@extends('admin_dash.header')
@if(!isset(Auth::user()->id))
    <script>
        window.location.href = '{{url("login")}}';
    </script>
@endif
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('css/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                @if(isset(Auth::user()->id))
                    <p>{{ isset(Auth::user()->first_name) ? Auth::user()->first_name : Auth::user()->email }}</p>
            @endif
            <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
        </div>
        <!-- search form -->
        <!--  <form action="#" method="get" class="sidebar-form">
           <div class="input-group">
             <input type="text" name="q" class="form-control" placeholder="Search...">
             <span class="input-group-btn">
                   <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                   </button>
                 </span>
           </div>
         </form> -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <li class=" @if(request()->route()->getName()=='home') active @endif">
                <a href="{{route('home')}}">
                    <i class="fa fa-tachometer" aria-hidden="true"></i> <span>Dashboard</span>

                </a>
            </li>
        <!--            <li  class=" @if(request()->route()->getName()=='user_list') active @endif">
                <a href="{{route('user_list')}}">
                    <i class="fa fa-list"></i> <span>Welcome User List</span>

                </a>
            </li>-->

        <!--   <li  class=" @if(request()->route()->getName()=='become_patener') active @endif">
                <a href="{{route('become_patener')}}">
                    <i class="fa fa-list"></i> <span>Become Patner</span>

                </a>
            </li> -->

            <li class=" @if(request()->route()->getName()=='customerlist') active @endif">
                <a href="{{route('customerlist')}}">
                    <i class="fa fa-list"></i> <span>Customer Management</span>
                    <span class="pull-right-container">
                      <!-- <small class="label pull-right bg-green">new</small> -->
                    </span>
                </a>
            </li>
            <li class=" @if(request()->route()->getName()=='userCheckInDetail') active @endif">
                <a href="{{route('userCheckInDetail')}}">
                    <i class="fa fa-list"></i> <span>Customer Check Ins</span>
                    <span class="pull-right-container">
                      <!-- <small class="label pull-right bg-green">new</small> -->
                    </span>
                </a>
            </li>

            <li class=" @if(request()->route()->getName()=='facilities') active @endif">
                <a href="{{route('facilities')}}">
                    <i class="fa fa-list"></i> <span>Facilities</span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>

            <li class=" @if(request()->route()->getName()=='gymlist') active @endif">
                <a href="{{route('gymlist')}}">
                    <i class="fa fa-list"></i> <span>Gym-Owner Management</span>
                </a>
            </li>


            <li class=" @if(request()->route()->getName()=='gym_plan') active @endif">
                <a href="{{route('gym_plan')}}">
                    <i class="fa fa-list"></i> <span>Gym Plan Management</span>
                </a>
            </li>


            <li class="@if(request()->route()->getName()=='plan_list') active @endif">
                <a href="{{route('plan_list')}}">
                    <i class="fa fa-file-text"></i> <span>Plans</span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-credit-card"></i>
                    <span>Corporate Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class=" @if(request()->route()->getName()=='corporatelist') active @endif">
                        <a href="{{route('corporatelist')}}">
                            <i class="fa fa-list"></i> <span>Corporate List</span>
                        </a>
                    </li>
                    <li class=" @if(request()->route()->getName()=='corporateCheckIn') active @endif">
                        <a href="{{route('corporateCheckIn')}}">
                            <i class="fa fa-list"></i> <span>Corporate Check Ins</span>
                        </a>
                    </li>
                </ul>
            </li>

            @if(request()->route()->getName()=='subscription_list' || request()->route()->getName()=='subscriberlist')
                <li class="treeview menu-open">
                    <style type="text/css">
                        .treeview-menu {
                            display: block;
                        }
                    </style>
            @else
                <li class="treeview">
                    @endif
                    <a href="#">
                        <i class="fa fa-credit-card"></i>
                        <span>Subscription Management</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if(request()->route()->getName()=='subscription_list') active @endif"><a href="{{route('subscription_list')}}"><i class="fa fa-bars"></i> Subscription</a></li>
                        <li class="@if(request()->route()->getName()=='subscriberlist') active @endif"><a href="{{route('subscriberlist')}}"><i class="fa fa-bars"></i> Subscriber List</a></li>
                    </ul>
                </li>

                <li class="@if(request()->route()->getName()=='coupon_list') active @endif">
                    <a href="{{route('coupon_list')}}">
                        <i class="fa fa-file-text"></i> <span>Coupon Code</span>
                        <span class="pull-right-container">
                    </span>
                    </a>
                </li>

                <li class="@if(request()->route()->getName()=='notification') active @endif">
                    <a href="{{route('notification')}}">
                        <i class="fa fa-file-text"></i> <span>Notifications</span>
                        <span class="pull-right-container">
                </span>
                    </a>
                </li>

                <li class="@if(request()->route()->getName()=='content') active @endif">
                    <a href="{{route('content')}}">
                        <i class="fa fa-file-text"></i> <span>Content Management</span>
                        <span class="pull-right-container">
                    </span>
                    </a>
                </li>

                <li class="@if(request()->route()->getName()=='blogs') active @endif">
                    <a href="{{route('blogs')}}">
                        <i class="fa fa-file-text"></i> <span>Blog</span>
                        <span class="pull-right-container">
                    </span>
                    </a>
                </li>
                <li class="@if(request()->route()->getName()=='news') active @endif">
                    <a href="{{route('news')}}">
                        <i class="fa fa-file-text"></i> <span>News</span>
                        <span class="pull-right-container">
                    </span>
                    </a>
                </li>
                <li class=" @if(request()->route()->getName()=='userBankDetail') active @endif">
                    <a href="{{route('userBankDetail')}}">
                        <i class="fa fa-list"></i> <span>User Bank Detail</span>
                        <span class="pull-right-container">
                      <!-- <small class="label pull-right bg-green">new</small> -->
                    </span>
                    </a>
                </li>


                <li class=" @if(request()->route()->getName()=='revenueHistory') active @endif">
                    <a href="{{route('revenueHistory')}}">
                        <i class="fa fa-list"></i> <span>Revenue History</span>
                        <span class="pull-right-container">
                      <!-- <small class="label pull-right bg-green">new</small> -->
                    </span>
                    </a>
                </li>
                <li class=" @if(request()->route()->getName()=='paymentsHistory') active @endif">
                    <a href="{{route('paymentsHistory')}}">
                        <i class="fa fa-list"></i> <span>Payments History</span>
                        <span class="pull-right-container">
                      <!-- <small class="label pull-right bg-green">new</small> -->
                    </span>
                    </a>
                </li>
            <!--            <li class="@if(request()->route()->getName()=='rate') active @endif">
                <a href="{{route('rate')}}">
                    <i class="fa fa-file-text"></i> <span>Rates
                        <span class="pull-right-container">
                        </span>
                </a>
            </li>-->
            <!--            <li class="@if(request()->route()->getName()=='uploadreport') active @endif">
                <a href="{{route('uploadreport')}}">
                    <i class="fa fa-file-text"></i> <span>Upload Report
                        <span class="pull-right-container">
                        </span>
                </a>
            </li> -->

                <li class="@if(request()->route()->getName()=='contact_us_admin') active @endif">
                    <a href="{{route('contact_us_admin')}}">
                        <i class="fa fa-file-text"></i> <span>Contact us
                        <span class="pull-right-container">
                        </span>
                    </a>
                </li>

                <li class="">
                    <a href="{{url('admin-change-password')}}"><!-- userpassword -->
                        <i class="fa fa-unlock-alt"></i> <span>Change Password</span>
                        <span class="pull-right-container">
            </span>
                    </a>
                </li>
                <!-- <li class="treeview">
                  <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>UI Elements</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                    <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                    <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                    <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                    <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                    <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
                  </ul>
                </li> -->
                <!--  <li class="treeview">
                   <a href="#">
                     <i class="fa fa-edit"></i> <span>Forms</span>
                     <span class="pull-right-container">
                       <i class="fa fa-angle-left pull-right"></i>
                     </span>
                   </a>
                   <ul class="treeview-menu">
                     <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
                     <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
                     <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
                   </ul>
                 </li> -->

                <!--   <li>
                    <a href="pages/mailbox/mailbox.html">
                      <i class="fa fa-envelope"></i> <span>Mailbox</span>
                      <span class="pull-right-container">
                        <small class="label pull-right bg-yellow">12</small>
                        <small class="label pull-right bg-green">16</small>
                        <small class="label pull-right bg-red">5</small>
                      </span>
                    </a>
                  </li> -->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
