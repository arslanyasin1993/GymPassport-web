@extends('corporate_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Employee Check In List
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('corporate-home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Employee Check In List</li>
            </ol>
        </section>
        <style type="text/css">
            i.fa {
                font-size: 19px;
            }

            .plan {
                font-weight: bold;
                float: right;
                margin-bottom: 14px;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs pull-left" id="myTab">
                            <!-- <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li> -->
                            <li class="active"><a href="#list" class="usertab" data="1" data-toggle="tab">Check In List</a></li>
                            <li><a href="#performers" class="usertab" data="2" data-toggle="tab">Top Performer</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="box">
                        <style type="text/css">
                            img.user_image {
                                height: auto;
                                width: 79px;
                                border-radius: 15px;
                            }
                        </style>
                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active" id="list">
                                <div class="box-header with-border">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-4"></div>
                                            <label for="inputEmail3" class="col-sm-1 control-label" style="width: 98px;">From-Date</label>
                                            <div class="col-sm-2">
                                                <input type='date' class="form-control" id='startdate' placeholder='From Date'>
                                            </div>
                                            <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                            <div class="col-sm-2">
                                                <input type="date" name="" id="enddate" class="form-control" placeholder="End Date">
                                            </div>
                                            <div class="col-sm-1">
                                                <button class="btn btn-primary" id="submit">Apply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="dataTable-example" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Employee name</th>
                                                <th>Employee email</th>
                                                <th>Check-in date</th>
                                                <th>Check-in Time</th>
                                                <th>Gym name</th>
                                                <th>User Plan</th>
                                                <th>Gym Plan</th>

                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="chart tab-pane" id="performers">
                                <div class="box-body box-profile">
                                    @if($top_performers)
                                        <img class="profile-user-img img-responsive img-circle" src="{{ $top_performers->user_name->user_image ? \Config::get('values.app_url') . $top_performers->user_name->user_image : asset('image/user_image/default.png') }}" alt="{{ $top_performers->user_name->first_name . ' ' . $top_performers->user_name->last_name }}">
                                        <h3 class="profile-username text-center">
                                            {{ $top_performers->user_name->first_name . ' ' . $top_performers->user_name->last_name }}
                                        </h3>
                                        <p class="text-muted text-center"> <span class="label label-success">{{ $top_performers->check_in_count }}</span></p>
                                        <p class="text-muted text-center"> <small class="">Total Check Ins</small></p>
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b class="key">User Id</b> <a class="pull-right value">{{ $top_performers->user_name->user_ufp_id }}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b class="key">First Name </b> <a class="pull-right value">{{ $top_performers->user_name->first_name }}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b class="key">Last Name</b> <a class="pull-right value"> {{ $top_performers->user_name->last_name }} </a>
                                            </li>
                                            <li class="list-group-item">
                                                <b class="key">Email</b> <a class="pull-right value"> {{ $top_performers->user_name->email }} </a>
                                            </li>
                                        </ul>
                                    @else
                                        No Check Ins In this Month
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    var dataTable = $('#dataTable-example').DataTable({
                        'processing': true,
                        'serverSide': true,
                        // 'searching': false, // Remove default Search Control
                        'ajax': {
                            'url': "{{route('employee-checkin-list')}}",
                            'data': function (data) {
                                // Read values
                                var enddate = $('#enddate').val();
                                var startdate = $('#startdate').val();
                                //var searchdata = $('#searchdata').val();

                                // Append to data
                                data.startdate = startdate;
                                data.enddate = enddate;
                                //data.searchdata = searchdata;
                            }
                        },
                        'columns': [
                            {data: 'username'},
                            {data: 'email'},
                            {data: 'check_in_date'},
                            {data: 'check_in_time'},
                            {data: 'gym_name'},
                            {data: 'plan_name'},
                            {data: 'gym_plan' , orderable: false},

                        ],
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'pdf', 'pageLength'
                        ],
                        "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000 , 5000]],
                    });

                    $('#searchdata').keyup(function () {
                        dataTable.draw();
                    });

                    $('#submit').click(function () {
                        dataTable.draw();
                    });
                });

            </script>
            <script type="text/javascript">
                $(function () {
                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear();
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    $('#startdate').attr('max', maxDate);
                    $('#enddate').attr('max', maxDate);
                });
            </script>
        </section>
        <!-- /.content -->
    </div>
@endsection

