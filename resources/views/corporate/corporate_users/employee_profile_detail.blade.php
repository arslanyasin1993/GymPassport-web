@extends('corporate_dash.design')
@section('title','Corporate | Employee Check Ins')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Employee CheckIns
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('corporate-home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('employee-list')}}">Employee List</a></li>
                <li class="active">Check Ins</li>
            </ol>
        </section>
        <style type="text/css">
            .timeline:before {
                display: none !important;
            }

            .timeline > li > .timeline-item {
                margin-left: 15px !important;
            }

            .first-box {
                width: 28%;
                padding: 0px 2px 11px 10px;
            }

            .second-box {
                width: 72%;
            }

            a.pull-right.value {
                margin: -1px 4px;
            }

            b.key {
                width: 100%;
            }

            @media (max-width: 768px) {
                .first-box {
                    width: 100%;
                }

                .second-box {
                    width: 100%;
                }
            }
        </style>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                @if(Session::has('message'))
                    <div class="alert msg_show @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible" style="margin:10px 16px 10px 10px;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {!! session('msg') !!}
                    </div>
            @endif
            <!-- /.col -->
                <div class="col-md-9 second-box">
                    <div class="nav-tabs-custom">
                    {{--                        <ul class="nav nav-tabs">--}}
                    {{--                            <li class="active"><a href="#checkin" data-toggle="tab">Check in details</a></li>--}}
                    {{--                        </ul>--}}
                    <!-- /.tab-pane -->

                        <ul class="timeline timeline-inverse" style="margin:5px!important; padding: 18px 0px 5px 0px">
                            @if(!empty($check_in_details))
                                @foreach($check_in_details as $data)
                                    <li class="active">
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{date('d/m/Y h:m a',strtotime($data->user_check_in))}}</span>
                                            <h3 class="timeline-header"><a href="#"> {{$data->gym_name->gym_name}} </a></h3>
                                            <div class="timeline-body">
                                                <b>Gym Address</b> : {{$data->gym_name->gym_address}} <br>
                                                <b>Check-In TimeLine </b>: {{date('d/m/Y h:m a',strtotime($data->user_check_in))}}<br>
                                                @php
                                                    if($data->plan_detail->visit_pass=='month'){
                                                    $pass = 'Monthly Subscription Plan';
                                                    }else{
                                                    $pass = $data->plan_detail->visit_pass.' Subscription Plan';
                                                    }

                                                @endphp
                                                <b>User Plan </b>: {{$data->plan_detail->plan_category->plan_cat_name}} ({{$pass}})
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @endif
                        </ul>

                    {{--                        </div>--}}


                    <!--  <div class="tab-pane" id="settings">
                 
                         </div> -->
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->

            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>

    <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <style type="text/css">
        .fontsize {
            font-size: 23px;
            text-align: center;
            display: block;
            margin-right: -42px;
            margin-top: -26px;
            cursor: pointer;
        }

        .star {
            color: red;
        }
    </style>
@endsection

