<html>
<body>
{{--<h1>PayFast Example Code For Redirection Payment Request</h1>--}}

@if (count($_GET) > 0)
    @if(isset($_GET['submit']))
        {{--        @php \App\Http\Controllers\API\PaymentController::payfastprocessed($merchant_id, $basket_id, $trans_amount,$_GET) @endphp--}}
        {{--        @php   processResponse($merchant_id, $basket_id, $trans_amount, $_GET) @endphp--}}

    @endif
@endif

@php
    /**
     * get access token with merchant id, secured key, basket id, transaction amount
     *
     */

    /**
     * process response coming from PayFast
     *
     */
    function processResponse($merchant_id, $original_basket_id, $txnamt, $response)
    {
        /**
         * following parameters sent from PayFast after success/failed transaction
         *
         */
    // Notify PayFast that information has been received - this is required
        header('HTTP/1.0 200 OK');
        flush();

    // Posted variables from ITN -the return variables
        $pfData = $_POST;

    // Update db
        switch ($pfData['payment_status']) {
            case 'COMPLETE':
                // If complete, update your application
                break;
            case 'FAILED':
                // There was an error, update your application
                break;
            default:
                // If unknown status, do nothing (safest course of action)
                break;
        }
        $trans_id = $response['transaction_id'];
        $err_code = $response['err_code'];
        $err_msg = $response['err_msg'];
        $basket_id = $response['basket_id'];
        $order_date = $response['order_date'];
        $response_key = $response['Response_Key'];
        $payment_name = $response['PaymentName'];
        $secretword = ''; // No secret code defined for merchant id 102,  secret code can be entered in merchant portal.
        $response_string = sprintf("%s%s%s%s%s", $merchant_id, $original_basket_id, $secretword, $txnamt, $err_code);
        $response_hash = hash('MD5', $response_string);

        if (strtolower($response_hash) != strtolower($response_key)) {
            echo "<br/>Transaction could not be varified<br/>";
            return;
        }
        if ($err_code == '000' || $err_code == '00') {
            echo "<strong>Transaction Successfully Completed. Transaction ID: " . $trans_id . "</strong><br/>";
            echo "<br/>Date: " . $order_date;
            return;
        }
        echo "<br/>Transaction Failed. Message: " . $err_msg;
    }

@endphp
<!-- For data integrity purpose, transaction amount and basket_id should be the same as the ones sent in token request -->
<!--
    Actual Payment Request
-->
<form id='PayFast_payment_form' name='PayFast-payment-form' method='post' style="display: none" action="https://ipg1.apps.net.pk/Ecommerce/api/Transaction/PostTransaction">
    Currency Code: <input type="TEXT" name="CURRENCY_CODE" value="PKR"/><br/>
    User Id: <input type="TEXT" name="user_id" value="{{$user_id}}"/><br/>
    Merchant ID: <input type="TEXT" name="MERCHANT_ID" value="{{$merchant_id}}"/><br/>
    Merchant Name: <input type="TEXT" name="MERCHANT_NAME" value="Gym Passport"/><br/>
    Token: <input type="TEXT" name="TOKEN" value="{{$token}}"/><br/>
    Success URL: <input type="TEXT" name="SUCCESS_URL" value="{{route('payfastprocessed',['user_id'=>$user_id,'merchant_id'=>$merchant_id,'token'=>$token,'email'=>$email,'mobile'=>$mobile,'trans_amount'=>$trans_amount,'basket_id'=>$basket_id,'subscription_id'=>$subscription_id,'plan_id'=>$plan_id,'gst'=>$gst,'coupon_id'=>$coupon_id,'coupon_code'=>$coupon_code,'counpon_discount'=>$counpon_discount])}}"/><br/>
    Failure URL: <input type="TEXT" name="FAILURE_URL" value="{{route('payFastFailPage',['amount'=>$trans_amount,'user_id'=>$user_id,'email'=>$email,'mobile'=>$mobile,'subscription_id'=>$subscription_id,'plan_id'=>$plan_id,'gst'=>$gst,'coupon_id'=>$coupon_id,'coupon_code'=>$coupon_code,'counpon_discount'=>$counpon_discount])}}"/><br/>
    Checkout URL: <input type="TEXT" name="CHECKOUT_URL" value="{{route('payfastprocessed',['user_id'=>$user_id,'merchant_id'=>$merchant_id,'token'=>$token,'email'=>$email,'mobile'=>$mobile,'trans_amount'=>$trans_amount,'basket_id'=>$basket_id,'subscription_id'=>$subscription_id,'plan_id'=>$plan_id,'gst'=>$gst,'coupon_id'=>$coupon_id,'coupon_code'=>$coupon_code,'counpon_discount'=>$counpon_discount])}}"/><br/>
    Customer Email: <input type="TEXT" name="CUSTOMER_EMAIL_ADDRESS" value="{{$email}}"/>
    Customer mobile: <input type="TEXT" name="CUSTOMER_MOBILE_NO" value="{{$mobile}}"/><br/>
    Transaction Amount: <input type="TEXT" name="TXNAMT" value="{{$trans_amount}}"/><br/>
    Basket ID: <input type="TEXT" name="BASKET_ID" value="{{$basket_id}}"/><br/>
    Transaction Date: <input type="TEXT" name="ORDER_DATE" value="<?php echo date('Y-m-d H:i:s', time()); ?>"/><br/>
    Signature: <input type="TEXT" name="SIGNATURE" value="SOME-RANDOM-STRING"/><br/>
    Version: <input type="TEXT" name="VERSION" value="MERCHANT-CART-0.1"/><br/>
    Item Description: <input type="TEXT" name="TXNDESC" value="Gympassport Subscription"/><br/>
    Proccode: <input type="TEXT" name="PROCCODE" value="00"/><br/>
    Transaction Type: <input type="TEXT" name="TRAN_TYPE" value='ECOMM_PURCHASE'/><br/>
    Store ID/Terminal ID (optional): <input type="TEXT" name="STORE_ID" value=''/><br/>
    <input type="SUBMIT" value="SUBMIT" id="submit" name="submit">
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(window).on('load', function () {
        @if($token)
        $("#submit").click();
        @endif
    });
</script>
</body>
</html>

