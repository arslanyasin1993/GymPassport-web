<header class="head">
    <style type="text/css">
        img.logo-image.img-fluid {
    height: 66px !important;
}
    .pro-image{
      width: 75px;
      height: 68px;
      border-radius: 10px;
      margin-top: -1px;
    }
    @media(max-width: 1282px){
    .pro-image{
      margin-top: 0px;
    }
    }
  </style>
  @if(request()->route()->getName()=='h_i_w')
    <style>
      .popup-paymentaa-second::after {
    top: 16px;
    }
    .popup-paymentaa-second {
   margin-top: 10px;
    }
    @media(max-width: 1281px){
      .popup-paymentaa-second {
    margin-top: 65px;
    }
    }
     @media(max-width: 1200px){
    .popup-paymentaa-second::after {
    top: 9px;
    }
    }
     @media(max-width: 768px){
    .popup-paymentaa-second::after {
    top: 19px;
     }
    }
    @media(max-width: 400px){
    .popup-paymentaa-second {
    max-width: 21%;
    padding: 3px;
    }
    }
    </style>
  @endif
  @if(request()->route()->getName() =='contactus')
    <style>
       .popup-paymentaa-second::after {
        top: 14px;
       }
       .profile-paymentaa-second a {
       padding: 27px 0px 14px 11px;
     }
     @media(max-width: 1200px){
     .popup-paymentaa-second::after {
    top: 2px;
     }
   }
    @media(max-width: 575px){
     .popup-paymentaa-second::after {
    top: 7px;
     }
   }
     </style>
     @endif
   <div class="container-fluid">
     @if(request()->route()->getName() =='paymenthistory')
          <div class="icon-pay-hist">
     @else
          <div class="icon">
     @endif
     @if(Auth::user()->user_type !='3')
          <a href="{{url('Home')}}"><img src="{{ asset('landing_page/images/Website_Logo-1.png')}}" alt="icon" class="logo-image img-fluid"></a>
     @else
          <a href="{{url('User-Home')}}"><img src="{{ asset('landing_page/images/Website_Logo-1.png')}}" alt="icon" class="logo-image img-fluid"></a>
     @endif
      </div>

 @if(request()->route()->getName() =='subscriptionplan' || request()->route()->getName() =='contactus' || request()->route()->getName() =='UserPasswordChange')
    @if(Auth::user()->user_type !='3')
      <div class="custom-navbar-pay-hist">
    @else
<!--      <div class="custom-navbar">-->
 <div class="custom-navbar-three">
    @endif
 @elseif(request()->route()->getName() =='sub_plan' || request()->route()->getName() =='sub_plan_detail')
<!--      <div class="custom-navbar-book-det">-->
 <div class="custom-navbar-three">
 @elseif(request()->route()->getName() =='paymenthistory')
      <div class="custom-navbar-pay-hist">
 @elseif(request()->route()->getName() =='ufppatnerhome' || request()->route()->getName() ==' checkin')
      <div class="custom-navbar-pay-hist">
 @else

      <div class="custom-navbar-three">
 @endif
         <ul>
          @if(Auth::user()->user_type=='3')
            <li class="@if(request()->route()->getName()=='userhome' || request()->route()->getName()=='UserPasswordChange') active @endif">
              <a href="{{route('userhome')}}">Home</a>
            </li>
            <li class="@if(request()->route()->getName()=='findgym')active @endif">
              <a href="{{route('findgym')}}">Find Gym</a>
            </li>
           <!--  <li><a href="#">Become a Partner</a></li> -->
            <li class="@if(request()->route()->getName()=='subscriptionplan' || request()->route()->getName()=='sub_plan' || request()->route()->getName()=='sub_plan_detail' || request()->route()->getName()=='card_detail' || request()->route()->getName()=='apply_coupon_card_detail')active @endif">
<!--              <a href="{{route('subscriptionplan')}}">Membership Plans</a>-->
            </li>
           @else
             <li class="@if(request()->route()->getName()=='ufppatnerhome' || request()->route()->getName()=='owner_edit_gym')active @endif">
              <a href="{{route('ufppatnerhome')}}">Home</a>
             </li>
             <li class="@if(request()->route()->getName()=='checkin')active @endif">
                <a href="{{route('checkin')}}">Check-in</a></li>
             @if(Auth::user()['user_type'] != '4')
             <li class="@if(request()->route()->getName()=='paymenthistory')active @endif">
                <a href="{{route('paymenthistory')}}">Payments </a>
             </li>
             @endif
            @endif
             <li class="@if(request()->route()->getName()=='contactus')active @endif">
              <a href="{{route('contactus')}}">Contact us</a>
            </li>
             @if(Auth::user()->user_type=='3')
             <li class="@if(request()->route()->getName()=='h_i_w')active @endif">
              <a href="{{route('h_i_w')}}">How It Works</a>
            </li>
            @endif
         </ul>
      </div>
      <div class="trigger-navbar">
         <span></span>
      </div>
      <div class="image">
         @if(isset(Auth::user()->user_image))
           <img src="https://gympassport.pk{{Auth::user()->user_image}}" class="pro-image" alt="profile-image">
         @else
         <img src="{{ asset('website_file/images/defalut.jpg')}}" alt="profile-image" class="pro-image">
<!--             @if(Auth::user()->user_type=='3')
                 <img src="{{ asset('website_file/images/defalut.jpg')}}" alt="profile-image" class="pro-image">
             @else
                 <img src="https://gympassport.pk/{{session()->get('gym_img')}}" alt="profile-image" class="pro-image">
             @endif-->
         @endif
      </div>
       @if(Auth::user()->user_type=='3')
           @if(request()->route()->getName() =='subscriptionplan')
              <div class="popup-paymentaa for-margin-membership">
                 <div class="profile-paymentaa"><a href="{{route('userhome')}}">Profile</a></div>
                 <div class="logout-paymentaa"><a href="{{ route('signout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" >Log out</a><form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form></div>
            </div>
              @elseif(request()->route()->getName() =='contactus')
                  <div class="popup-paymentaa-second for-padding-contact">
                     <div class="profile-paymentaa-second">
                      <a href="{{route('userhome')}}">Profile</a>
                     </div>
                     <div class="logout-paymentaa-second">
                      <a href="{{ route('signout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" >Log out</a><form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </div>
                  </div>
              @elseif(request()->route()->getName() =='UserPasswordChange' || request()->route()->getName() =='card_detail' || request()->route()->getName() =='apply_coupon_card_detail')
                  <div class="popup-paymentaa-second home-ch-pas">
                     <div class="profile-paymentaa-second">
                      <a href="{{route('userhome')}}">Profile</a>
                     </div>
                     <div class="logout-paymentaa-second">
                      <a href="{{ route('signout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" >Log out</a><form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </div>
                  </div>
               @elseif(request()->route()->getName() =='plan_wise_gym_list')
                  <div class="popup-paymentaa-second home-ch-pas ">
                     <div class="profile-paymentaa-second">
                      <a href="{{route('userhome')}}">Profile</a>
                     </div>
                     <div class="logout-paymentaa-second">
                      <a href="{{ route('signout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" >Log out</a><form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </div>
                  </div>
                  <style>
                    @media(max-width: 575px){
                    .profile-paymentaa-second a {
                      font-size: 12px;
                      padding: 10px 0px 5px 8px;
                  }
                }
                  </style>
              @else
                   <div class="popup-paymentaa-second">
                     <div class="profile-paymentaa-second">
                        <a href="{{route('userhome')}}">Profile</a>
                     </div>
                       @if(session()->has('last_url'))
                           <div class="profile-paymentaa-second"><a href="{{ route('login_account' , 1) }}">Back To Admin</a></div>
                       @endif
                     <div class="logout-paymentaa-second">
                      <a href="{{ route('signout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" >Log out</a><form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </div>
                  </div>
              @endif
      @else
         <div class="popup-paymentaa-second">
            <div class="profile-paymentaa-second"><a href="#">Profile</a></div>
             @if(session()->has('last_url'))
                <div class="profile-paymentaa-second"><a href="{{ route('login_account' , 1) }}">Back To Admin</a></div>
             @endif
         <div class="logout-paymentaa-second">
            <a href="{{ route('signout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" >Log out</a><form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form></div>
         </div>
      @endif
   </div>
</header>
   <script>
        $(document).ready(function() {
            $(".trigger-navbar").click(function() {
                $(".custom-navbar-three").slideToggle();
            });
        });

        $(document).ready(function() {
            $(".trigger-navbar").click(function() {
                $(".custom-navbar-pay-hist").slideToggle();
            });
        });

         $(document).ready(function() {
            $(".image").click(function() {
                $(".popup-paymentaa-second").toggle();
            });
        });

        $('.three-cancel').click(function() {
            $(".bord").css("background-color", "transparent");
            $(".enable-class").prop("disabled", true);
             $(".three-cancel").hide();
        });

        $('.three-edit-profile').click(function() {
            $(".bord").css("background-color", "#fff");
             $(".bord").css("padding", "0 5px");
            $(".enable-class").prop("disabled", false);
              $(".three-cancel").show();
        });
    </script>