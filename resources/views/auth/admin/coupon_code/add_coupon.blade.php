@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Coupon
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class=""><a href="{{route('coupon_list')}}">Coupon-List</a></li>
            <li class="active">Add-Coupon</li>
        </ol>
    </section>
    <style type="text/css">
        input[type=radio] {
    height: 19px;
    width: 100%;
}
        .rediobox{
            padding: 0px 0px 0px 12px;
        }
        .time{
            width: 99%;
            margin-left: 1px !important;
        }
        .star{
            color: red;
        }
        .help-block{
            color: red;
            border-color: red;
            line-height: 0px;
        }
        .help-block-text{
            color: red;
            border-color: red;
        }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }
        .location{
            margin-top: 10px;
        }
        .note{
            color: red;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #3c8dbc !important;
            border: 1px solid #3c8dbc !important;
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">     
                @if(Session::has('message'))
                <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {!! session('msg') !!}
                </div>
                @endif 
                <div class="box">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form action="{{route('store_coupon')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="user_type" value="2">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">For Plan<span class="star">*</span></label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2 @if($errors->has('plan_cat_id')) help-block @endif" id="plan_cat_id" name="plan_cat_id[]" multiple="multiple" data-placeholder="Select Plan">
                                            @if(!empty($plan_cat))
                                             @php $get = (old('plan_cat_id')=='6') ? "selected" :""; @endphp
                                            <option value="6" @php echo $get; @endphp>Apply to all Plans</option>
                                            @foreach($plan_cat as $key=>$value)
                                            @php $get = $value->id==old('plan_cat_id') ? "selected" :"";@endphp
                                            <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->plan_cat_name}}</option>
                                            @endforeach
                                            @else
                                            <option value=""> Plan Unavailable</option>
                                            @endif
                                        </select>
                                        @if ($errors->has('plan_cat_id'))
                                        <strong class="help-block"> {{ $errors->first('plan_cat_id') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Coupon Code<span class="star">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control @if($errors->has('coupon_code')) help-block @endif" id="coupon_code" name="coupon_code" value="{{ old('coupon_code') }}" placeholder="Enter Coupon code">
                                        @if ($errors->has('coupon_code'))
                                        <strong class="help-block"> {{ $errors->first('coupon_code') }}</strong>
                                        @endif
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Discount Percentage<span class="star">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control @if($errors->has('discount')) help-block @endif" id="discount" name="discount" placeholder="Enter discount percentage" value="{{ old('discount') }}">
                                        @if ($errors->has('discount'))
                                        <strong class="help-block"> {{ $errors->first('discount') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Valid From<span class="star">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="date" class="form-control @if($errors->has('valid_from')) help-block @endif" id="valid_from" name="valid_from" placeholder="Valid from" value="{{ old('valid_from') }}">
                                        @if ($errors->has('valid_from'))
                                        <strong class="help-block"> {{ $errors->first('valid_from') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Valid To<span class="star">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="date" class="form-control @if($errors->has('valid_to')) help-block @endif" id="valid_to" name="valid_to" placeholder="Valid to" value="{{ old('valid_to') }}">
                                        @if ($errors->has('valid_to'))
                                        <strong class="help-block"> {{ $errors->first('valid_to') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Description<span class="star">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control @if($errors->has('description')) help-block-text @endif"  name="description" id="description" placeholder="Enter Coupon description">{{ old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                        <strong class="help-block"> {{ $errors->first('description') }}</strong>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Coupon use to<span class="star"></span></label>
                                    <div class="col-sm-9" style="margin-top: 7px;">
                                        <label class="rediobox" id="forever">
                                            <input type="radio" name="valid_for" class="" value="1" > &nbsp; <span class="valid">Forever</span>
                                        </label>
                                        <label class="rediobox" id="openinput">
                                            <input type="radio" name="valid_for"  value="0" class=""> &nbsp; Enter manually
                                         </label>
                                        <label>
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label for="inputEmail3" class="col-sm-3 control-label"><span class="star"></span></label>
                                    <div class="col-sm-4" id="showcoupon" style="display:none;" >
                                        <input type="number" name="total_use_coupon" id="custom" class="form-control" placeholder="Enter number of use coupon">
                                    </div>
                                </div>

<!--                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Image<span class="star">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="file" name="image" class="form-control @if($errors->has('image')) help-block @endif">
                                        @if ($errors->has('image'))
                                        <strong class="help-block"> {{ $errors->first('image') }}</strong>
                                        @endif
                                    </div>
                                </div>-->
                            </div>


                            <!-- /.box-body -->
                            <div class="box-footer">
                                <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                <div class="col-sm-offset-1 col-sm-4"></div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script>
    $(document).ready(function(){
       $('#openinput').click(function(){
           $('#showcoupon').css("display", "block");
       }); 
       
       $('#forever').click(function(){
           $('#showcoupon').css("display", "none");
       });
    });
</script>
@endsection
