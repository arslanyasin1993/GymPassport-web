@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Listing
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User List</li>
        </ol>
    </section>
    <style type="text/css">
        i.fa {
            font-size: 19px;
        }
        .plan{
            font-weight: bold;
            float: right;
            margin-bottom: 14px;
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <a href="{{route('download_current_plan')}}"><button class="btn btn-info ">Download customer current plan</button></a>
                <div class="box">
                   
                    <div class="box-header with-border">
                        <div class="col-sm-12">
                            <!-- <div class="col-sm-4"></div> -->
                            <div class="form-group">
                                <div class="col-sm-4">
                                 <!-- <input type='text' class="form-control" id='searchdata' placeholder='Search By First Name ,Last Name,Email'> -->
                                </div> 
                                <label for="inputEmail3" class="col-sm-1 control-label" style="width: 98px;">From-Date</label>
                                <div class="col-sm-2">
                                    <input type='date' class="form-control" id='startdate' placeholder='From Date'>
                                </div>
                                <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                <div class="col-sm-2">
                                    <input type="date" name="" id="enddate" class="form-control" placeholder="End Date">
                                </div>
                                <div class="col-sm-1">
                                    <button class="btn btn-primary" id="submit">Apply</button>
                                </div>
                            </div>
                        </div>
                    </div>
<style type="text/css">
    img.user_image {
    height: auto;
    width: 79px;
    border-radius: 15px;
}
</style>
                    <div class="box-body">
                        <table id="dataTable-example" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>User Image</th>
                                    <th>Username</th>
                                    <th>Registration date</th>
                                    <th>View Information</th>
                                    <th>User status</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script type="text/javascript">
$(document).ready(function () {
    var dataTable = $('#dataTable-example').DataTable({
        'processing': true,
        'serverSide': true,
        // 'searching': false, // Remove default Search Control
        'ajax': {
            'url': "{{route('cus_man_list')}}",
            'data': function (data) {
                // Read values
                var enddate = $('#enddate').val();
                var startdate = $('#startdate').val();
                //var searchdata = $('#searchdata').val();

                // Append to data
                data.startdate = startdate;
                data.enddate = enddate;
                //data.searchdata = searchdata;
            },
           
        },
        'columns': [
            {data: 'user_image'},
            {data: 'username'},
            {data: 'registerd_date'},
            {data: 'view_info'},
            {data: 'user_status'},
        ]
    });

    $('#searchdata').keyup(function () {
        dataTable.draw();
    });

    $('#submit').click(function () {
        dataTable.draw();
    });
});

        </script>
        <script type="text/javascript">
            $(function () {
                var dtToday = new Date();
                var month = dtToday.getMonth() + 1;
                var day = dtToday.getDate();
                var year = dtToday.getFullYear();
                if (month < 10)
                    month = '0' + month.toString();
                if (day < 10)
                    day = '0' + day.toString();
                var maxDate = year + '-' + month + '-' + day;
                $('#startdate').attr('max', maxDate);
                $('#enddate').attr('max', maxDate);
            });
        </script>
    </section>
    <!-- /.content -->
</div>
@endsection

