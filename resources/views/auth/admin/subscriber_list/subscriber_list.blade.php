@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Subscriber List
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Subscription Plan List</li>
        </ol>
    </section>
    <style type="text/css">
        i.fa {
            font-size: 19px;
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box-header with-border">
                    <div class="col-sm-12">
                        <!-- <div class="col-sm-4"></div> -->
                        <div class="form-group">
                            <div class="col-sm-2">
                              <!-- <select name="country" class="form-control" id="country">
                                <option value="">Select Country</option>
                              </select> -->
                            </div>
                            <div class="col-sm-2">
                                <select name="visit_pass" class="form-control" id="visit_pass">
                                    <option  value="">Select No.Of Pass</option>
                                    <option value="{{trans('constants.one_visit')}}" @php echo $get = '1'==old('visit_pass') ? "selected" :"";@endphp>Casual Pass</option>
                                    <option value="{{trans('constants.five_visit')}}" @php echo $get = '5'==old('visit_pass') ? "selected" :"";@endphp>5 Visit Pass</option>
                                    <option value="{{trans('constants.ten_visit')}}" @php echo $get = '10'==old('visit_pass') ? "selected" :"";@endphp>10 Visit Pass</option>
                                    <option value="{{trans('constants.monthly_visit')}}" @php echo $get = 'month'==old('visit_pass') ? "selected" :"";@endphp>Monthly</option>
                                </select>
                            </div>
                            <label for="inputEmail3" class="col-sm-2 control-label" style="width: 98px;">From-Date</label>
                            <div class="col-sm-2">
                                <input type='date' class="form-control" id='startdate' placeholder='From Date'>
                            </div>
                            <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                            <div class="col-sm-2">
                                <input type="date" name="" id="enddate" class="form-control" placeholder="End Date">
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-primary" id="submit">Apply</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-left" id="myTab">
                      <!-- <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li> -->
                        <li class="active"><a href="#saver_pass" class="usertab" data="1" data-toggle="tab">{{trans('constants.saver_pass')}}</a></li>
                        <li><a href="#light_pass" class="usertab" data="2" data-toggle="tab">{{trans('constants.light_pass')}}</a></li>
                        <li><a href="#gym_fit_pass" class="usertab" data="3" data-toggle="tab">{{trans('constants.gym_fit_pass')}}</a></li>
                        <li><a href="#body_fit_pass" class="usertab" data="4" data-toggle="tab">{{trans('constants.body_fit_pass')}}</a></li>
                        <li><a href="#ultimate_fit_pass" class="usertab" data="5" data-toggle="tab">{{trans('constants.Ultimate_fit_pass')}}</a></li>
                    </ul>
                    <div class="tab-content no-padding">
                        <!-- Morris chart - Sales -->
                        <div class="chart tab-pane active" id="saver_pass" >
                            <div class="box-body">
                                <table id="gym_saver_pass" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Subscriber Name</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="chart tab-pane " id="light_pass" >
                            <div class="box-body">
                                <table id="gym_light_pass" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Subscriber Name</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="chart tab-pane " id="gym_fit_pass" >
                            <div class="box-body">
                                <table id="gymfit_pass" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Subscriber Name</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="chart tab-pane " id="body_fit_pass" >
                            <div class="box-body">
                                <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
                                <table id="bodyfit_pass" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Subscriber Name</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <div class="chart tab-pane " id="ultimate_fit_pass" >
                            <div class="box-body">

                                <table id="Ultimatepass" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Subscriber Name</th>
                                            <th>View Details</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body" id="subscriptiondata">

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript">
//$(document).ready(function(){
//$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
//localStorage.setItem('activeTab', $(e.target).attr('href'));
//});
//var activeTab = localStorage.getItem('activeTab');
//if(activeTab){
//$('#myTab a[href="' + activeTab + '"]').tab('show');
// }
//});
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                var plan_cat = '1';
                fetchusersubscription(plan_cat);
            });
            $('.usertab').click(function () {
                var plan_cat = $(this).attr('data');
                //  alert(plan_cat);
                fetchusersubscription(plan_cat);
            });
            function fetchusersubscription(plan_cat) {
                // alert(plan_cat);
                if (plan_cat == '1') {
                    var table_id = 'gym_saver_pass';
                } else if (plan_cat == '2') {
                    var table_id = 'gym_light_pass';
                } else if (plan_cat == '3') {
                    var table_id = 'gymfit_pass';
                } else if (plan_cat == '4') {
                    var table_id = 'bodyfit_pass';
                } else if (plan_cat == '5') {
                    var table_id = 'Ultimatepass';
                }
                $('#' + table_id).DataTable().destroy();
                var dataTable = $('#' + table_id).DataTable({
                    'processing': true,
                    'serverSide': true,
                    //'searching': false, // Remove default Search Control
                    'ajax': {
                        'url': "{{route('ajaxsubscriberlist')}}",
                        'data': function (data) {
                            // Read values
                            var enddate = $('#enddate').val();
                            var startdate = $('#startdate').val();
                            var visit_pass = $('#visit_pass').val();
                            //var searchdata = $('#searchdata').val();

                            // Append to data
                            data.startdate = startdate;
                            data.enddate = enddate;
                            data.visit_pass = visit_pass;
                            data.plan_cat = plan_cat;
                            //data.searchdata = searchdata;
                        }
                    },
                    'columns': [
                        {data: 'subscriber_name'},
                        {data: 'details'},
                    ]
                });
                $('#searchdata').keyup(function () {
                    dataTable.draw();
                });
                $('#visit_pass').change(function () {
                    dataTable.draw();
                });

                $('#submit').click(function () {
                    dataTable.draw();
                });
            }
        </script>

        <script type="text/javascript">
            function userview(id) {
                //alert(id);
                $.ajax({
                    url: "{{url('getsubscribe_user')}}/" + id,
                    type: 'GET',
                    dataType: 'html',
                    success: function (data) {
                        //alert(data);
                        $('#subscriptiondata').html(data);
                        $('#myModal').modal('show');
                    }
                });
            }
        </script>
        <script type="text/javascript">
            $(function () {
                var dtToday = new Date();
                var month = dtToday.getMonth() + 1;
                var day = dtToday.getDate();
                var year = dtToday.getFullYear();
                if (month < 10)
                    month = '0' + month.toString();
                if (day < 10)
                    day = '0' + day.toString();
                var maxDate = year + '-' + month + '-' + day;
                $('#startdate').attr('max', maxDate);
                $('#enddate').attr('max', maxDate);
            });
        </script>
    </section>
</div>
@endsection