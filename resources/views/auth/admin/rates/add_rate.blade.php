@extends('admin_dash.design')  
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(empty($rates))
        Create Rate
        @else
        Update Rate
        @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
       @if(empty($rates))
        <li class="active"> Create Rate</li>
        @else
        <li class="active"> Update Rate</li>
        @endif
        
        
      </ol>
    </section>
<style type="text/css">
  .time{
    width: 99%;
    margin-left: 1px !important;
  }
  .star{
    color: red;
  }
  .help-block{
    color: red;
    border-color: red;
    line-height: 0px;
  }
  .help-block-text{
    color: red;
    border-color: red;
  }
  .select2-container .select2-selection--single {
    height: 35px !important;
  }
</style>
  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-offset-2 col-xs-12 col-sm-6 col-md-6">   
         @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
            @endif  
          <div class="box">
              <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
@if(empty($rates))
            <form action="{{route('add_rate')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
               {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Country<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <select class="form-control  @if($errors->has('country_id')) help-block @endif" id="country_id" name="country_id">
                      @if(!empty($country))
                        <option value=""> Country</option>
                        @foreach($country as $key=>$value)
                          @php $get = $value->id==old('country_id') ? "selected" :"";@endphp
                           <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->nicename}}</option>
                        @endforeach
                      @else
                        <option value=""> Country Unavailable</option>
                      @endif
                    </select>
                     @if ($errors->has('country_id'))
                         <strong class="help-block"> {{ $errors->first('country_id') }}</strong>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">First Visit<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control @if($errors->has('first_visit')) help-block @endif" id="first_visit" name="first_visit" placeholder="Pay on 1st visit" value="{{ old('first_visit') }}" step="0.01">
                     @if ($errors->has('first_visit'))
                         <strong class="help-block"> {{ $errors->first('first_visit') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Second Visit<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control @if($errors->has('second_visit')) help-block @endif" id="second_visit" name="second_visit" placeholder="Pay on 2nd visit" value="{{ old('second_visit') }}" step="0.01">
                     @if ($errors->has('second_visit'))
                         <strong class="help-block"> {{ $errors->first('second_visit') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Third Visit<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control @if($errors->has('third_visit')) help-block @endif" id="third_visit" name="third_visit" placeholder="Pay on 3rd visit" value="{{ old('third_visit') }}" step="0.01">
                     @if ($errors->has('third_visit'))
                         <strong class="help-block"> {{ $errors->first('third_visit') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Further Visit<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control @if($errors->has('more_visit')) help-block @endif" id="more_visit" name="more_visit" placeholder="Pay on further visit" value="{{ old('more_visit') }}" step="0.01">
                     @if ($errors->has('more_visit'))
                         <strong class="help-block"> {{ $errors->first('more_visit') }}</strong>
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
               <div class="col-sm-offset-1 col-sm-4"></div>
                <button type="submit" class="btn btn-info">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
              </div>
              <!-- /.box-footer -->
            </form>
@else
       <form action="{{route('update_rate',['id'=>$rates->id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
               {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Country<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <select class="form-control  @if($errors->has('country_id')) help-block @endif" id="country_id" name="country_id">
                      @if(!empty($country))
                        <option value=""> Country</option>
                        @foreach($country as $key=>$value)
                          @php $get = $value->id==$rates->country_id ? "selected" :"";@endphp
                           <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->nicename}}</option>
                        @endforeach
                      @else
                        <option value=""> Country Unavailable</option>
                      @endif
                    </select>
                     @if ($errors->has('country_id'))
                         <strong class="help-block"> {{ $errors->first('country_id') }}</strong>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">First Visit<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control @if($errors->has('first_visit')) help-block @endif" id="first_visit" name="first_visit" placeholder="Pay on 1st visit" value="{{$rates->first_visit}}" step="0.01">
                     @if ($errors->has('first_visit'))
                         <strong class="help-block"> {{ $errors->first('first_visit') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Second Visit<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control @if($errors->has('second_visit')) help-block @endif" id="second_visit" name="second_visit" placeholder="Pay on 2nd visit" value="{{$rates->second_visit}}" step="0.01">
                     @if ($errors->has('second_visit'))
                         <strong class="help-block"> {{ $errors->first('second_visit') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Third Visit<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control @if($errors->has('third_visit')) help-block @endif" id="third_visit" name="third_visit" placeholder="Pay on 3rd visit" value="{{$rates->third_visit}}" step="0.01">
                     @if ($errors->has('third_visit'))
                         <strong class="help-block"> {{ $errors->first('third_visit') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Further Visit<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control @if($errors->has('more_visit')) help-block @endif" id="more_visit" name="more_visit" placeholder="Pay on further visit" value="{{$rates->more_visit}}" step="0.01">
                     @if ($errors->has('more_visit'))
                         <strong class="help-block"> {{ $errors->first('more_visit') }}</strong>
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
               <div class="col-sm-offset-1 col-sm-4"></div>
                <button type="submit" class="btn btn-info">Update</button>
                <a href="{{route('home')}}"><button type="button" class="btn btn-danger">Cancle</button></a>
              </div>
              <!-- /.box-footer -->
            </form>
@endif
          </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

var geocoder = new google.maps.Geocoder();
var address = "noida";

geocoder.geocode( { 'address': address}, function(results, status) {

  if (status == google.maps.GeocoderStatus.OK) {
    var latitude = results[0].geometry.location.lat();
    var longitude = results[0].geometry.location.lng();
    alert(latitude);
  } 
}); 
</script>
@endsection

