@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->
<style>
    table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting {
    padding-right: 0px !important;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Gym-Owner Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Gym-List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content"> 
        <div class="row">
             @if(Session::has('message'))
                <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {!! session('msg') !!}
                </div>
                @endif 
            <div class="col-xs-2" style="    margin-bottom: 5px;">
                <a href="{{route('gym_partner_add')}}"><button type="button" class="btn btn-block btn-warning"><i class="fa fa-edit"></i> Add Gyms Partner</button></a>
            </div>
            <div class="col-xs-2" style="    margin-bottom: 5px;">
                <a href="{{route('add_gym')}}"><button type="button" class="btn btn-block btn-info"><i class="fa fa-edit"></i> Create Gyms</button></a>
            </div> 
             <div class="col-xs-2" style="    margin-bottom: 5px;">
                <a href="{{route('resetpassword')}}"><button type="button" class="btn btn-block btn-success"><i class="fa fa-edit"></i> Reset Password</button></a>
            </div> 
            <div class="col-xs-12">
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-left" id="myTab">
                       <!-- <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li> -->
                        <li class="active"><a href="#Request" data-toggle="tab">Request</a></li>
                        <li><a href="#Registered" data-toggle="tab">Registered</a></li>
                    </ul> 
                    <div class="tab-content no-padding">
                        <div class="chart tab-pane active" id="Request" >  
                            <div class="box-header with-border">
                                <div class="col-sm-12">
                                    <!-- <div class="col-sm-4"></div> -->
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                         <!-- <input type='text' class="form-control" id='searchdata' placeholder='Search By First Name ,Last Name,Email'> -->
                                        </div> 
                                        <label for="inputEmail3" class="col-sm-1 control-label" style="width: 98px;">From-Date</label>
                                        <div class="col-sm-2">
                                            <input type='date' class="form-control" id='startdate_first' placeholder='From Date'>
                                        </div>
                                        <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                        <div class="col-sm-2">
                                            <input type="date" name="" id="enddate_first" class="form-control" placeholder="End Date">
                                        </div>
                                        <div class="col-sm-1">
                                            <button class="btn btn-primary" id="submit_first">Apply</button>
                                        </div>
                                    </div>
                                </div>
                            </div>        
                            <div class="box-body" style="overflow-x: auto;">
                                <table id="request_list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th style="width:50px !important;">Email</th>
                                            <th>Fitness Facility</th>
                                            <th>Phone Number</th>
                                            <th>Country</th>
                                            <th>State</th>
                                            <th>Postal Code</th>
                                            <th>Suburb</th>
                                            <th>Registration Fees</th>
                                            <th>Monthly Fees</th>
                                            <!-- <th>Check-in's</th> -->
                                            <th>Registered date</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                            <th>Delete Stripe Account</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="chart tab-pane" id="Registered" >    
                            <div class="box-header with-border">
                                <div class="col-sm-12">
                                    <!-- <div class="col-sm-4"></div> -->
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                         <!-- <input type='text' class="form-control" id='searchdata' placeholder='Search By First Name ,Last Name,Email'> -->
                                        </div> 
                                        <label for="inputEmail3" class="col-sm-1 control-label" style="width: 98px;">From-Date</label>
                                        <div class="col-sm-2">
                                            <input type='date' class="form-control" id='startdate' placeholder='From Date'>
                                        </div>
                                        <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                        <div class="col-sm-2">
                                            <input type="date" name="" id="enddate" class="form-control" placeholder="End Date">
                                        </div>
                                        <div class="col-sm-1">
                                            <button class="btn btn-primary" id="submit">Apply</button>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <div class="box-body">
                                <table id="example_registered" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
<!--                                            <th>Id</th>-->
                                            <th>Registered Owners</th>
                                            <th>Registration date</th>
                                            <th>Gym Name</th>
                                            <th>Gym Price Per Visit</th>
                                            <th>Email</th>
                                            <th>Phone number</th>
                                            <th>Country</th>
                                            <th>View Details</th>
                                            <th>Check-in's</th>
                                            <th>Status</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script type="text/javascript">
$(document).ready(function () {
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if (activeTab) {
        $('#myTab a[href="' + activeTab + '"]').tab('show');
    }
});
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                var dataTable_first = $('#request_list').DataTable({
                    'processing': true,
                    'serverSide': true,
                    // 'searching': false, // Remove default Search Control
                    'ajax': {
                        'url': "{{route('request_user_list',['type'=>'2'])}}",
                        'data': function (data) {
                            // Read values
                            var enddate = $('#enddate_first').val();
                            var startdate = $('#startdate_first').val();
                            //var searchdata = $('#searchdata').val();

                            // Append to data
                            data.startdate = startdate;
                            data.enddate = enddate;
                            //data.searchdata = searchdata;
                        }
                    },
                    'columns': [
                        {data: 'first_name'},
                        {data: 'email'},
                        {data: 'fitness_facility'},
                        {data: 'phone_number'},
                        {data: 'country'},
                        {data: 'state'},
                        {data: 'postal_code'},
                        {data: 'suburb'},
                        {data: 'registration_fees'},
                        {data: 'monthly_fees'},
                        {data: 'registration_date'}, 
                        {data: 'edit'}, 
                        {data: 'delete'}, 
                        {data: 'account_delete'}, 

                    ]
                });

                $('#searchdata').keyup(function () {
                    dataTable_first.draw();
                });

                $('#submit_first').click(function () {
                    dataTable_first.draw();
                });
            });


            $(document).on('click' , '#delete_gym_owner' , function (){
                var url = $(this).attr('data-href');
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (response){
                        if(response){
                            dataTable_first.draw();
                            toastr.success(response.message);
                        }else{
                            toastr.error(response.message);
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                var dataTable = $('#example_registered').DataTable({
                    'processing': true,
                    'serverSide': true,
                    // 'searching': false, // Remove default Search Control
                    'ajax': {
                        'url': "{{route('gym_owner_list')}}",
                        'data': function (data) {
                            // Read values
                            var enddate = $('#enddate').val();
                            var startdate = $('#startdate').val();
                            //var searchdata = $('#searchdata').val();

                            // Append to data
                            data.startdate = startdate;
                            data.enddate = enddate;
                            //data.searchdata = searchdata;
                        }
                    },
                    'columns': [
//                        {data: 'inc_id'},
                        {data: 'reg_owner'},
                        {data: 'registration_date'},
                        {data: 'gym_name'},
                        {data: 'gym_price_per_visit'},
                        {data: 'email'},
                        {data: 'phone_number'},
                        {data: 'country'},
                        {data: 'view_detail'},
                        {data: 'check_in'},
                        {data: 'status'},
                        {data: 'edit'},
                        {data: 'delete'},
                    ]
                });

                $('#searchdata').keyup(function () {
                    dataTable.draw();
                });

                $('#submit').click(function () {
                    dataTable.draw();
                });
            });

            // Change Status
            $(document).on('click' , '.change_gym_status' , function (){
                var url = $(this).attr('data-href');
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (response){
                        if(response){
                            dataTable.draw();
                            toastr.success(response.message);
                        }else{
                            toastr.error(response.message);
                        }
                    }
                });
            });

            // Delete Gym
            $(document).on('click' , '#delete_gym' , function (){
                var url = $(this).attr('data-href');
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (response){
                        if(response){
                            dataTable.draw();
                            toastr.success(response.message);
                        }else{
                            toastr.error(response.message);
                        }
                    }
                });
            })
        </script>
        <script type="text/javascript">
            $(function () {
                var dtToday = new Date();
                var month = dtToday.getMonth() + 1;
                var day = dtToday.getDate();
                var year = dtToday.getFullYear();
                if (month < 10)
                    month = '0' + month.toString();
                if (day < 10)
                    day = '0' + day.toString();
                var maxDate = year + '-' + month + '-' + day;
                $('#startdate').attr('max', maxDate);
                $('#enddate').attr('max', maxDate);
                $('#startdate_first').attr('max', maxDate);
                $('#enddate_first').attr('max', maxDate);
            });
        </script>
        <!-- /.row -->
    </section> 
    <!-- /.content -->
</div>
@endsection

