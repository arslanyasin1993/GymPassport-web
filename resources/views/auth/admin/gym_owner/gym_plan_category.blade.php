@extends('admin_dash.design')  
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gym Plan
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gym Plan</li>
       
        
        
      </ol>
    </section>
<style type="text/css">
  .time{
    width: 99%;
    margin-left: 1px !important;
  }
  .star{
    color: red;
  }
  .help-block{
    color: red;
    border-color: red;
    line-height: 0px;
  }
  .help-block-text{
    color: red;
    border-color: red;
  }
/*  .select2-container .select2-selection--single {
    height: 35px !important;
  }*/
   .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: #3c8dbc !important;
    border: 1px solid #3c8dbc !important;
  }
</style>
  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-offset-2 col-xs-12 col-sm-6 col-md-6">   
         @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
            @endif  
          <div class="box">
              <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{route('add_gym_plan')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
               {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Gym Name<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <select class="form-control  select2 @if($errors->has('gym_name')) help-block @endif" id="gym_name" name="gym_name[]" multiple="multiple" data-placeholder="Select a gym name">
                      @if(!empty($gym))
                        <option value=""> Gym Name</option>
                        @foreach($gym as $key=>$value)
                          @if(!in_array($value->id, $added_gym))
                           <option value="{{$value->id}}">{{$value->gym_name}}</option>
                           @endif
                        @endforeach
                      @endif
                    </select>
                     @if ($errors->has('gym_name'))
                         <strong class="help-block"> {{ $errors->first('gym_name') }}</strong>
                    @endif
                  </div>
                </div>  

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Plan Name<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <select class="form-control  @if($errors->has('plan_name')) help-block @endif" id="plan_name" name="plan_name">
                      @if(!empty($plan))
                        <option value="">Plan Name</option>
                        @foreach($plan as $key=>$value)
                          @php $get = $value->id==old('plan_name') ? "selected" :"";@endphp
                           <option value="{{$value->id}}">{{$value->plan_cat_name}}</option>
                        @endforeach
                      @else
                        <option value=""> Country Unavailable</option>
                      @endif
                    </select>
                     @if ($errors->has('plan_name'))
                         <strong class="help-block"> {{ $errors->first('plan_name') }}</strong>
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
               <div class="col-sm-offset-1 col-sm-4"></div>
                <button type="submit" class="btn btn-info">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          </div>


          <!-- /.box -->
        </div>
         <div class="col-xs-12 col-sm-10 col-md-10">   
            <div class="box">
            <div class="box-body">
                  <table id="dataTable-example" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                    <th>Gym name</th>
                    <th>Plan Name</th>
                    <th>Added Date</th>
                    <th>Action</th>
                    </tr>
                  </thead>
                  </table>
            </div>
          </div>

         </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
  var dataTable = $('#dataTable-example').DataTable({
   'processing': true,
   'serverSide': true,
  // 'searching': false, // Remove default Search Control
    'ajax': {
       'url':"{{route('gym_plan_list')}}",
       'data': function(data){
        
       }
    },
    'columns': [
        { data: 'gym_name' } ,
        { data: 'plan_name' },
        { data: 'date' },
        { data: 'action' },
       
    ]
  });

  $('#searchdata').keyup(function(){
    dataTable.draw();
  });
  
   $('#submit').click(function(){
    dataTable.draw();
   });
});

  $(document).on('click' , '#delete_gym_plan' , function (){
      var url = $(this).attr('data-href');
      $.ajax({
          url: url,
          type: 'GET',
          success: function (response){
              if(response){
                  dataTable.draw();
                  toastr.success(response.message);
              }else{
                  toastr.error(response.message);
              }
          }
      });
  });

</script>

@endsection

