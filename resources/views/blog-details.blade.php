@extends('web.layouts.master')
@section('title' , 'Gym Passport - Blog Details')
@section('style')
    <style>
        .post-card__image :before, .widget-posts__image :before {
            content: "";
            display: block;
            position: absolute;
            left: 0;
            right: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.2);
            opacity: 0;
            transition: opacity 0.6s;
            z-index: 1;
        }

        .widget__title {
            margin-bottom: 20px;
            font-size: 28px;
        }

        .widget-posts {
            box-shadow: 0 1px 3px rgba(0, 0, 0, .1);
            border: 2px solid #f0f0f0;
            border-radius: 2px;
            padding: 28px;
        }

        .widget-posts__name a {
            color: inherit;
            transition: color 0.2s;
            line-height: normal;
            text-decoration: none;
            font-size: 26px;
        }

        .widget-posts__name a:hover {
            color: #be0027;
        }

        .widget-posts__date {
            margin-top: 5px;
            font-size: 16px;
            color: #999;
        }

        .widget-posts__item {
            display: -ms-flexbox;
            display: flex;
        }

        .widget-posts__image {
            width: 175px;
            -ms-flex-negative: 0;
            flex-shrink: 0;
            border-radius: 1.5px;
            overflow: hidden;
            -ms-flex-item-align: self-start;
            align-self: self-start;
            position: relative;
            margin-right: 16px;
        }

        .widget-posts__item + .widget-posts__item {
            margin-top: 22px;
        }

        .post-header--layout--classic .post-header__meta {
            margin-bottom: 30px;
        }

        .post-header__meta {
            font-size: 16px;
            letter-spacing: 0.02em;
            color: #999;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
        }

        .post__featured img {
            max-width: 100%;
        }

        .post__featured {
            text-align: center;
            position: relative;
            overflow: hidden;
            border-radius: 2px;
            margin-bottom: 40px;
        }

        .typography {
            font-family: 'Poppins' !important;
        }

            /*.typography p {*/
            /*    line-height: 1.625;*/
            /*    font-size: 17px;*/
            /*}*/

        .post-author {
            border: 2px solid #f0f0f0;
            border-radius: 2px;
            padding: 22px;
            display: -ms-flexbox;
            display: flex;
            margin-top: 25px;
            align-items: center;
        }

        .post-author__avatar {
            width: 70px;
            -ms-flex-negative: 0;
            flex-shrink: 0;
            overflow: hidden;
            border-radius: 2px;
            margin-right: 18px;
        }

        .post-author__name {
            margin-top: 1px;
            font-size: 15px;
            font-weight: 500;
        }

        .post-author__name a {
            color: black !important;
        }

        .post__content.typography > * {
            font-family: Poppins !important;
        }

        .typography > * {
            font-family: Poppins !important;
        }

        .post__content.typography li {
            list-style: disc;
        }

        .post__content.typography ul {
            list-style: disc;
        }

        .post__content.typography > p,
        .post__content.typography > ul,
        .post__content.typography > li{
            font-size: 20px !important;
            color: #515151 !important;
        }
    </style>
@endsection
@section('content')
    <!-- GYM-PARTNER-HERO-AREA START -->
    <section class="gym-partner-hero-area blogs-hero">
        <div class="container">
            <div class="gym-partner-hero-content text-center">
                <h1>One Platform to share hundreds of fitness stories</h1>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <section class="terms-area">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="block post post--layout--classic" style="box-shadow: 0 1px 3px rgba(0,0,0,.1);border-radius: 2px;border: 2px solid #f0f0f0">
                            <div class="post__header post-header post-header--layout--classic">
                                <div class="post-header__meta text-center justify-content-center p-5 mb-0 flex-column">
                                    <h1 style="color: #000;font-size: 48px">{{$blog->title}}</h1>
                                    <div class="post-header__meta-item">By {{$blog->author}} {{(date_create_from_format('Y-m-d H:i:s',$blog->updated_at?:$blog->created_at)->format('F d, Y'))}}</div>
                                </div>
                            </div>
                            <div class="post__featured">
                                <img class="lazyload" data-src="{{$blog->image?asset($blog->image):"https://ui-avatars.com/api/?name=". $blog->title}}" alt="">
                            </div>
                            <div class="post__content typography px-5">
                                {!! $blog->description !!}
                            </div>
                            <div class="post__footer">
                                <div class="post-author">
                                    <div class="post-author__avatar">
                                        <h4>Author</h4>
                                    </div>
                                    <div class="post-author__info">
                                        <div class="post-author__name"><a href="https://www.linkedin.com/in/yousafaliofficial">{{$blog->author}}</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="block block-sidebar block-sidebar--position--end">
                            <div class="block-sidebar__item">
                                <div class="widget-posts widget">
                                    <h1 class="widget__title">Recent Blogs</h1>
                                    <div class="widget-posts__list">
                                        @foreach($recentBlogs as $recnetBlog)
                                            <div class="widget-posts__item">
                                                <div class="widget-posts__image">
                                                    <a href="{{route('blog-detail', $recnetBlog->slug) }}">
                                                        <img style="max-width:100%; max-height: 100%;" class="lazyload" data-src="{{$recnetBlog->image?asset($recnetBlog->image):"https://ui-avatars.com/api/?name=".$recnetBlog->title}}" alt="">
                                                    </a>
                                                </div>
                                                <div class="widget-posts__info">
                                                    <div class="widget-posts__name">
                                                        <a href="{{route('blog-detail', $recnetBlog->slug) }}">
                                                            {{ \Illuminate\Support\Str::limit($recnetBlog->title,40)}}
                                                        </a>
                                                    </div>
                                                    <div class="widget-posts__date">{{(date_create_from_format('Y-m-d H:i:s',$recnetBlog->updated_at?:$recnetBlog->created_at)->format('M d, Y'))}}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
