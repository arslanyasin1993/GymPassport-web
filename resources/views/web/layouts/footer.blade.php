<!-- FOOTER AREA START -->
<footer class="footer-area">
    <div class="container">
        <div class="footer-logo text-center">
            <a href="#">
                <img src="{{ asset('assets/images/footer-logo.svg') }}" alt="" class="img-fluid">
            </a>
        </div>
        <div class="footer-nav">
            <ul>
{{--                <li><a href="{{ url('/') }}" class="{{ Request::is('/') ? 'active' : '' }}">HOME</a></li>--}}
                <li><a href="{{ route('aboutus') }}" class="{{ request()->route()->getName() == 'aboutus' ? 'active' : '' }}">ABOUT US</a></li>
                <li><a href="{{ route('faq') }}" class="{{ request()->route()->getName() == 'faq' ? 'active' : '' }}">FAQS</a></li>
                <li><a href="{{ route('terms_conditions') }}" class="{{ request()->route()->getName() == 'terms_conditions' ? 'active' : '' }}">TERMS & CONDITIONS</a></li>
                <li><a href="{{ route('refund_policy') }}" class="{{ request()->route()->getName() == 'refund_policy' ? 'active' : '' }}">REFUND POLICY</a></li>
                <li><a href="{{ route('privacy_policy') }}" class="{{ request()->route()->getName() == 'privacy_policy' ? 'active' : '' }}">Privacy Policy</a></li>
{{--                <li><a href="{{route('becomepatner')}}" class="{{ request()->route()->getName() == 'becomepatner' ? 'active' : '' }}">PARTNERSHIPS</a></li>--}}
            </ul>
        </div>
        <div class="copy-area row justify-content-md-between justify-content-sm-center align-items-center">
            <div class="col-md-6 col-sm-12 text-center mb-2 mb-md-0">
                <p class="float-md-start">© {{ date('Y') }}, copyrights @ Gym Passport</p>
            </div>
            <div class="col-md-6 col-sm-12 text-center">
                <p class="float-md-end">Designed and Developed by <a href="https://softzee.com/" target="blank" class="text-danger">Softzee</a></p>
            </div>
        </div>
    </div>
</footer>
<!-- FOOTER AREA END -->

<!--MAIN-JQUERY-->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

{{--<script src="{{ asset('assets/js/jquery-3.4.1.min.js') }}"></script>--}}

<!--BOOTSTRAP-JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
{{--<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>--}}

<!-- flickity js -->
{{--<script src="{{ asset('assets/js/flickity.js') }}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.pkgd.min.js" integrity="sha512-Y1SAZrLDgz5+PyKBPpLPkEh5hlh1FvTmyNdR+PfbTweGjIB0XgJ65YiKobYLtRTpoz8pWfMJBxmJ8JDRLyJGjw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

{{--<script src="{{ asset('assets/js/owl.carousel.js') }}"></script>--}}

<!-- accordian-sldscript -->
<script src="{{ asset('assets/js/accordian-sldscript.js') }}"></script>

<!--CUSTOM-JQUERY-->
<script src="{{ asset('assets/js/custom.min.js') }}"></script>

<!--Toastr-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!--Lazy loading	-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.3.2/lazysizes.min.js" integrity="sha512-q583ppKrCRc7N5O0n2nzUiJ+suUv7Et1JGels4bXOaMFQcamPk9HjdUknZuuFjBNs7tsMuadge5k9RzdmO+1GQ==" crossorigin="anonymous" referrerpolicy="no-referrer" async></script>

<!--SCROLL-TOP	-->
<a href="#" class="scrolltotop"><i class="fa-solid fa-angle-up"></i></a>
