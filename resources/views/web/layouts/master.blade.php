<!DOCTYPE html>
<html lang="en-US">
<head>
    <link rel="canonical" href="{{url()->current()}}"/>
    <!--META-SETUP-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@yield('meta')
<!-- TITLE -->
    <title>@yield('title')</title>

    <!-- FAV-ICON -->
    <link rel="icon" href="{{ asset('landing_page/images/favicon32x32.png')}}"/>

    <!-- Flickity CSS -->
    {{--    <link rel="stylesheet" href="{{ asset('assets/css/flickity.css') }}"/>--}}
    <link rel="stylesheet" media="print" onload="this.onload=null;this.removeAttribute('media');" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.2.0/flickity.min.css" integrity="sha512-5/vljg0R8jZWzpN8BkuFi1wZIHFuX0wAbPyNazWGc7u+rTrOKW9Y+PfeLRx7fJ1DNGgkzSaiD/xshyT9yJKRjw==" crossorigin="anonymous" referrerpolicy="no-referrer"/>

    <!-- Owl CSS -->
    {{--    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.css') }}"/>--}}
    <link rel="stylesheet" media="print" onload="this.onload=null;this.removeAttribute('media');" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer"/>

    <!-- BOOTSTRAP-CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- SOCIAL-ICON -->
    <link rel="stylesheet" media="print" onload="this.onload=null;this.removeAttribute('media');" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>

    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>

    {{--    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;1,100;1,200;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">--}}
<!-- MIN-STYLESHEET -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.min.css') }}"/>

    <!-- RESPONSIVE-CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.min.css') }}"/>

    <link rel="stylesheet" media="print" onload="this.onload=null;this.removeAttribute('media');" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer"/>

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-81YTDYWY8M"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-81YTDYWY8M');
    </script>


    {{--  Custom Style  --}}
    @yield('style')
</head>
<body>
{{--  Header  --}}
@include('web.layouts.header')
{{--  Content  --}}
@yield('content')
{{--  Footer  --}}
@include('web.layouts.footer')
{{--  Custom Scripts  --}}
@yield('scripts')
</body>
</html>
