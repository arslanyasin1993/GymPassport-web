<!-- HEADER-SECTION -->

<header>

    <div class="header-area pt-0 position-fixed w-100">
        <div class="header-topbar" style="background: rgb(0,0,0.98);z-index: 20;margin-bottom: 20px">
            <div class="container-fluid">
                <div class="d-flex justify-content-end align-items-center py-3">
                    <a href="https://www.facebook.com/GymPassportPk/"><i class="fa-brands fa-facebook text-white fs-5 me-3"></i></a>
                    <a href="https://www.instagram.com/gym.passport/"><i class="fa-brands fa-instagram text-white fs-4 me-3"></i></a>
                    <a href="https://pk.linkedin.com/company/gym-passport"><i class="fa-brands fa-linkedin-in text-white fs-4"></i></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-2 col-7">
                    <div class="logo">
                        <a href="{{ url('/') }}"><img class="img-fluid" src="{{ asset('assets/images/logo.png') }}" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-8 col-1">
                    <nav class="main-nav text-end">
                        <ul>
                            @if(isset(Auth::user()->id))
                                @if(Auth::user()->user_type=='3')
                                    <li>
                                        <a href="{{route('userhome')}}" class="{{ request()->route()->getName() == 'userhome' ? 'active-nav' : '' }}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="{{route('subscriptionplan')}}" class="{{ request()->route()->getName() == 'subscriptionplan' ? 'active-nav' : '' }}">Membership Plan</a>
                                    </li>
                                    <li>
                                        <a href="{{url('/')}}" class="{{ Request::is('/') ? 'active-nav' : '' }}">Home</a>
                                    </li>
                                    <li>
                                        <a href="{{route('becomepatner')}}" class="{{ request()->route()->getName() == 'becomepatner' ? 'active-nav' : '' }}">Become a Gym Partner</a>
                                    </li>
                                    <li>
                                        <a href="{{route('corporate-partner')}}" class="{{ request()->route()->getName() == 'corporate-partner' ? 'active-nav' : '' }}">Corporate Partners</a>
                                    </li>
                                    <li>
                                        <a href="{{route('all-blog')}}" class="{{ request()->route()->getName() == 'all-blog' ? 'active-nav' : '' }}">Blogs</a>
                                    </li>
                                    <li>
                                        <a href="{{route('all-news')}}" class="{{ request()->route()->getName() == 'all-news' ? 'active-nav' : '' }}">News</a>
                                    </li>
                                    <li>
                                        <a href="{{route('contactus')}}" class="{{ request()->route()->getName() == 'contactus' ? 'active-nav' : '' }}">Contact Us</a>
                                    </li>
                                @elseif(Auth::user()->user_type=='2')
                                    <li>
                                        <a href="{{route('ufppatnerhome')}}" class="{{ request()->route()->getName() == 'ufppatnerhome' ? 'active-nav' : '' }}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="{{url('/')}}" class="{{ Request::is('/') ? 'active-nav' : '' }}">Home</a>
                                    </li>
                                    <li>
                                        <a href="{{route('becomepatner')}}" class="{{ request()->route()->getName() == 'becomepatner' ? 'active-nav' : '' }}">Become a Gym Partner</a>
                                    </li>
                                    <li>
                                        <a href="{{route('corporate-partner')}}" class="{{ request()->route()->getName() == 'corporate-partner' ? 'active-nav' : '' }}">Corporate Partners</a>
                                    </li>
                                    <li>
                                        <a href="{{route('all-blog')}}" class="{{ request()->route()->getName() == 'all-blog' ? 'active-nav' : '' }}">Blogs</a>
                                    </li>
                                    <li>
                                        <a href="{{route('all-news')}}" class="{{ request()->route()->getName() == 'all-news' ? 'active-nav' : '' }}">News</a>
                                    </li>
                                    <li>
                                        <a href="{{route('contactus')}}" class="{{ request()->route()->getName() == 'contactus' ? 'active-nav' : '' }}">Contact Us</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{route('home')}}" class="{{ request()->route()->getName() == 'home' ? 'active-nav' : '' }}">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="{{url('/')}}" class="{{ Request::is('/') ? 'active-nav' : '' }}">Home</a>
                                    </li>
                                    <li>
                                        <a href="{{route('becomepatner')}}" class="{{ request()->route()->getName() == 'becomepatner' ? 'active-nav' : '' }}">Become a Gym Partner</a>
                                    </li>
                                    <li>
                                        <a href="{{route('corporate-partner')}}" class="{{ request()->route()->getName() == 'corporate-partner' ? 'active-nav' : '' }}">Corporate Partners</a>
                                    </li>
                                    <li>
                                        <a href="{{route('all-blog')}}" class="{{ request()->route()->getName() == 'all-blog' ? 'active-nav' : '' }}">Blogs</a>
                                    </li>
                                    <li>
                                        <a href="{{route('all-news')}}" class="{{ request()->route()->getName() == 'all-news' ? 'active-nav' : '' }}">News</a>
                                    </li>
                                    <li>
                                        <a href="{{route('contactus')}}" class="{{ request()->route()->getName() == 'contactus' ? 'active-nav' : '' }}">Contact Us</a>
                                    </li>
                                @endif
                            @else
                                <li>
                                    <a href="{{url('/')}}" class="{{ Request::is('/') ? 'active-nav' : '' }}">Home</a>
                                </li>
                                <li>
                                    <a href="{{route('becomepatner')}}" class="{{ request()->route()->getName() == 'becomepatner' ? 'active-nav' : '' }}">Become a Gym Partner</a>
                                </li>
                                <li>
                                    <a href="{{route('corporate-partner')}}" class="{{ request()->route()->getName() == 'corporate-partner' ? 'active-nav' : '' }}">Corporate Partners</a>
                                </li>
                                <li>
                                    <a href="{{route('all-blog')}}" class="{{ request()->route()->getName() == 'all-blog' ? 'active-nav' : '' }}">Blogs</a>
                                </li>
                                <li>
                                    <a href="{{route('all-news')}}" class="{{ request()->route()->getName() == 'all-news' ? 'active-nav' : '' }}">News</a>
                                </li>
                                <li>
                                    <a href="{{route('contactus')}}" class="{{ request()->route()->getName() == 'contactus' ? 'active-nav' : '' }}">Contact Us</a>
                                </li>
                            @endif
                        </ul>
                    </nav>
                </div>
                @if(!isset(Auth::user()->id) || isset(Auth::user()->id) && Auth::user()->user_type != '2')
                    <div class="col-lg-2 col-4">
                        <div class="partner-btn text-end">
                            <a href="{{ route('singin') }}">Partner login</a>
                        </div>
                        <div class="menu-bar text-end" data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling" aria-controls="offcanvasScrolling">
                            <i class="fa-solid fa-bars"></i>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</header>

<!--MOBIL-MENU-START-->
<div class="offcanvas offcanvas-start" data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
    <div class="offcanvas-header">
        <h5 class="offcanvas-title" id="offcanvasScrollingLabel">
            <div class="logo">
                <a href="index.blade.php"><img class="img-fluid" src="{{ asset('assets/images/logo.png') }}" alt=""></a>
            </div>
        </h5>
        <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
        <nav class="main-nav">
            <ul>
                @if(isset(Auth::user()->id))
                    @if(Auth::user()->user_type=='3')
                        <li>
                            <a href="{{route('userhome')}}" class="{{ request()->route()->getName() == 'userhome' ? 'active-nav' : '' }}">Home</a>
                        </li>
                        <li>
                            <a href="{{route('subscriptionplan')}}" class="{{ request()->route()->getName() == 'subscriptionplan' ? 'active-nav' : '' }}">Membership Plan</a>
                        </li>
                    @elseif(Auth::user()->user_type=='2')
                        <li>
                            <a href="{{route('ufppatnerhome')}}" class="{{ request()->route()->getName() == 'ufppatnerhome' ? 'active-nav' : '' }}">Home</a>
                        </li>
                    @else
                        <li>
                            <a href="{{route('home')}}" class="{{ request()->route()->getName() == 'home' ? 'active-nav' : '' }}">Home</a>
                        </li>
                    @endif
                @else
                    <li>
                        <a href="{{url('/')}}" class="{{ request()->url() == '/' ? 'active-nav' : '' }}">Home</a>
                    </li>
                    <li>
                        <a href="{{route('becomepatner')}}" class="{{ request()->route()->getName() == 'becomepatner' ? 'active-nav' : '' }}">Become a Gym Partner</a>
                    </li>
                    <li>
                        <a href="{{route('corporate-partner')}}" class="{{ request()->route()->getName() == 'corporate-partner' ? 'active-nav' : '' }}">Corporate Partners</a>
                    </li>
                    <li>
                        <a href="{{route('all-blog')}}" class="{{ request()->route()->getName() == 'all-blog' ? 'active-nav' : '' }}">Blogs</a>
                    </li>
                    <li>
                        <a href="{{route('all-news')}}" class="{{ request()->route()->getName() == 'all-news' ? 'active-nav' : '' }}">News</a>
                    </li>
                    <li>
                        <a href="{{route('contactus')}}" class="{{ request()->route()->getName() == 'contactus' ? 'active-nav' : '' }}">Contact Us</a>
                    </li>
                @endif
            </ul>
        </nav>
        @if(!isset(Auth::user()->id))
            <div class="partner-btn text-end">
                <a href="{{ route('singin') }}">Partner login</a>
            </div>
        @endif
    </div>
</div>
