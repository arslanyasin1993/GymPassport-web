@if(!empty($side_gym_data))
@foreach($side_gym_data as $key=>$value)
   @if($value['gym_cat_id'] == '1')
    <div class="box-one class_blue">
    @elseif($value['gym_cat_id'] == '2')
    <div class="box-one class_green">
    @elseif($value['gym_cat_id'] == '3')
    <div class="box-one class_yellow">
    @elseif($value['gym_cat_id'] == '4')
    <div class="box-one class_orange">
    @elseif($value['gym_cat_id'] == '5')
    <div class="box-one class_red">
    @else
      <div class="box-one">
    @endif
      <div class="row">
         <div class="col-7 col-sm-7 col-md-7 col-lg-7">
            <div class="left-part">
               <h2>{{$value['gym_name']}}</h2>
               <h3>{{substr($value['gym_address'],0,30)}}</h3>
               <button type="button" class="distance-button">
                  <p>{{number_format((float) $value['distance'], 1, '.', '')}} Km Away</p>
               </button>
            </div>
         </div>
         <div class="col-5 col-sm-5 col-md-5 col-lg-5">
            <div class="image-man"><img src="https://gympassport.pk/{{$value['gym_logo']}}" alt="man-image">
            </div>
            <div class="view-det"><a href="#" onclick="getGymdetail('{{$value["gym_id"]}}')">View Details</a>
            </div><!-- <div class="view-det" data-toggle="modal" data-target="#myModal"><a href="#">View Details</a>
            </div> -->
         </div>
      </div>
   </div>
   @endforeach
@else
<div class="box-one">
   <div class="row">
       <h2 class="gym_unv">Gym Unavailable</h2>
   </div>
</div>
@endif
                      