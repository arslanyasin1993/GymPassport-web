@extends('web.layouts.master')
@section('title' , 'Gym Passport - Become a Gym Partner')
@section('style')
    <style>
        .register-area {
            margin-top: 150px !important;
        }
    </style>
@endsection

@section('meta')
    <meta name="title" content="Become a Partner and Expand Your Business Nationwide with the Best Gym Passport">
    <meta name="description" content="Join the ultimate business partnership program and expand your reach across Pakistan with access to top gyms. Apply to become a partner now and grow your business today!">
@endsection
@section('content')
    <!-- GYM-PARTNER-HERO-AREA START -->
    <section class="gym-partner-hero-area">
        <div class="container">
            <div class="gym-partner-hero-content text-center">
                <h1>Expand your business with the <span>Ultimate Partnership!</span></h1>
                <h1>Our Gym Passport is <span>Pakistan wide</span> with some of the top gyms on our roster!</h1>
                <div class="gym-hero-btn">
                    <a href="#submit_formss">Register now</a>
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <!-- PARTNERING AREA END -->
        <section class="partnering-area overflow-hidden">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid lazyload"> <span>parterning</span> <img data-src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid lazyload"></h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/partnering-title-bg.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2>Benefits of <span>Parterning</span> with us</h2>
                </div>
                <div class="partnering-main">
                    <div class="row g-5">
                        <div class="col-lg-4 col-md-6">
                            <div class="partnering-main-content">
                                <div class="partnering-header">
                                    <div class="free-img">
                                        <img data-src="{{ asset('assets/images/free-img.png') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                    <div class="number-img">
                                        <img data-src="{{ asset('assets/images/01.svg') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                </div>
                                <div class="partner-main-title">
                                    <h2>It's FREE!</h2>
                                    <p>Joining and staying to be our
                                        partner will cost you nothing.
                                        <span> EVER!</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="partnering-main-content partnering-main-content2">
                                <div class="partnering-header">
                                    <div class="free-img">
                                        <img data-src="{{ asset('assets/images/customer.svg') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                    <div class="number-img">
                                        <img data-src="{{ asset('assets/images/02.svg') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                </div>
                                <div class="partner-main-title">
                                    <h2>new customers</h2>
                                    <p>We target people outside of your <span>target area</span> and bring in more users to your Gym that otherwise would not visit. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="partnering-main-content">
                                <div class="partnering-header">
                                    <div class="free-img">
                                        <img data-src="{{ asset('assets/images/more-income.svg') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                    <div class="number-img">
                                        <img data-src="{{ asset('assets/images/03.svg') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                </div>
                                <div class="partner-main-title">
                                    <h2>more income</h2>
                                    <p>Create a <span>new revenue stream</span> with every member that visits your fitness center.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- STEPS AREA START -->
        <section class="steps-area">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-2.svg') }}" alt="" class="img-fluid lazyload"> <span>steps</span> <img data-src="{{ asset('assets/images/right-icon-2.svg') }}" alt="" class="img-fluid lazyload"></h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/steps-title-bg.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2>5 Simple <span>Steps</span></h2>
                </div>
                <div class="steps-main">
                    <ul>
                        <li><span><img data-src="{{ asset('assets/images/arrow-red.svg') }}" alt="" class="img-fluid lazyload"></span>
                            Register to get started
                        </li>
                        <li><span><img data-src="{{ asset('assets/images/arrow.svg') }}" alt="" class="img-fluid lazyload"></span>
                            A member of our team will
                            contact you and assist you
                            in the partnership process
                        </li>
                        <li><span><img data-src="{{ asset('assets/images/arrow.svg') }}" alt="" class="img-fluid lazyload"></span>
                            Your gym will be added onto our platform. We will also update our users that you are now a Gym Passport partner
                        </li>
                        <li><span><img data-src="{{ asset('assets/images/arrow.svg') }}" alt="" class="img-fluid lazyload"></span>
                            Allow Gym Passport members to visit your facility during staffed hours. Validate their check-in and let them enjoy their workout!
                        </li>
                        <li><span><img data-src="{{ asset('assets/images/arrow.svg') }}" alt="" class="img-fluid lazyload"></span>
                            Receive more income with every Gym Passport member that visits.
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- BEST FITNESS AREA START -->
        <section class="best-fitness-area mb-5 best-fitness-area2">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid lazyload"> <span>BEST FITNESS CLUBS</span> <img data-src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid lazyload"></h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/gym-partners-icon.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2>Some of our <span>Gym Partners</span> across Pakistan</h2>
                </div>
                <div class="main-content4">
                    <div id="owl-csel4" class="owl-carousel owl-theme">
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-f  itness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                    </div>
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="custom-nav owl-nav"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- REGISTER-AREA START -->
        <section class="register-area">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid lazyload"> <span>register</span> <img data-src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid lazyload"></h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/register-title-bg.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2>Register Your <span>Interest!</span></h2>
                </div>
                <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                    </symbol>
                </svg>
                @if(session()->has('msg'))
                    <div class="d-flex ms-auto me-auto" id="form-alert" style="max-width: 1370px">
                        <div class="col-12">
                            <div class="alert {{ session()->get('message') }} d-flex align-items-center justify-content-between" role="alert">
                                <div class="d-flex align-items-center">
                                    <svg class="bi flex-shrink-0 me-3" width="24" height="24" role="img" aria-label="Success:">
                                        <use xlink:href="#check-circle-fill"/>
                                    </svg>
                                    <div class="fs-4 lh-1">
                                        {{ session()->get('msg') }}
                                    </div>
                                </div>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="register-main">
                    <form action="{{ route('add_become_patner') }}" method="POST" id="submit_formss">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="text" name="first_name" value="{{ old('first_name') }}" placeholder="First Name">
                                    @if ($errors->has('first_name'))<strong class="text">{{ $errors->first('first_name') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="text" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name">
                                    @if ($errors->has('last_name'))<strong class="text">{{ $errors->first('last_name') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="name-input">
                                    <input type="text" name="fitness_facility" value="{{ old('fitness_facility') }}" placeholder="Name of your fitness facility">
                                    @if ($errors->has('fitness_facility'))<strong class="text">{{ $errors->first('fitness_facility') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="email" name="email" value="{{ old('email') }}" placeholder="email">
                                    @if ($errors->has('email'))<strong class="text">{{ $errors->first('email') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="text" name="phone_number" value="{{ old('phone_number') }}" placeholder="Phone Number">
                                    @if ($errors->has('phone_number'))<strong class="text">{{ $errors->first('phone_number') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="number" name="registration_fees" value="{{ old('registration_fees') }}" placeholder="Your clubs registration fees (Rs)">
                                    @if ($errors->has('registration_fees'))<strong class="text">{{ $errors->first('registration_fees') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="number" name="monthly_fees" value="{{ old('monthly_fees') }}" placeholder="Your clubs monthly fees (Rs)">
                                    @if ($errors->has('monthly_fees'))<strong class="text">{{ $errors->first('monthly_fees') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="text" value="{{ $country->nicename }}" placeholder="{{ $country->nicename }}" readonly>
                                    @if ($errors->has('country'))<strong class="text">{{ $errors->first('country') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <select name="state">
                                        <option value="" disabled selected hidden>State</option>
                                        @foreach($state as $key => $value)
                                            <option value="{{ $value->id }}">{{ $value->state }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('state'))<strong class="text">{{ $errors->first('state') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="name-input">
                                    <input type="text" name="address" value="{{ old('address') }}" placeholder="Address">
                                    @if ($errors->has('address'))<strong class="text">{{ $errors->first('address') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="submit-input">
                                    <input type="submit" value="Register Now">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    @if(Session::has('message'))
        <script>
            $("html, body").animate({ scrollTop: $("#form-alert").offset().top }, "slow");
        </script>
    @endif
    <script>
        $("#owl-csel4").owlCarousel({
            items: 4,
            autoplay: true,
            autoplayTimeout: 3000,
            startPosition: 0,
            rtl: false,
            loop: true,
            margin: 15,
            // groupCells: true,
            // groupCells: 6,
            dots: true,
            nav: true,
            navText: [
                '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ],
            navContainer: '.main-content4 .custom-nav',
            responsive: {
                0: {
                    items: 3,
                },
                767: {
                    items: 3,
                },
                768: {
                    items: 6,
                },
                992: {
                    items: 6,
                },
                1200: {
                    items: 6,
                }
            }

        });
    </script>
@endsection
