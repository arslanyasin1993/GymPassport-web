@extends('web.layouts.master')
@section('title' , 'Gym Passport - About Us')
@section('meta')
    <meta name="title" content="Get to Know Gym Passport - The Ultimate Business Partnership Program">
    <meta name="description" content="Discover the mission and values behind Gym Passport and learn why we're the go-to choice for business owners looking to expand their reach nationwide">
@endsection
@section('content')
    <!-- GYM-PARTNER-HERO-AREA START -->
    <section class="gym-partner-hero-area blogs-hero">
        <div class="container">
            <div class="gym-partner-hero-content text-center">
                <h1>About Us</h1>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <!-- ABOUT US AREA START -->
        <section class="about-us-area">
            <div class="container">
                <div class="about-us-main">
                    <div class="about-us-inner">
                        <div class="row g-0">
                            <div class="col-lg-6">
                                <div class="about-us-left">
                                    <img data-src="{{ asset('assets/images/about-us.png') }}" alt="" class="img-fluid lazyload">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="about-us-right text-center">
                                    {!! html_entity_decode($about_us->description) !!}
                                    {{--<h2>About Us</h2>--}}
                                    {{--<p>Gym Passport is a health and fitness membership providing its members with access to a wide choice of fitness facilities close to their home and work.--}}
                                    {{--    We are driven by a purpose to help people lead a healthy and an active lifestyle through a service representing excellent value and convenience.--}}
                                    {{--    Gym Passport has been working in close collaboration with employers and fitness partners for over three years. Through our successful and rewarding partnerships, our network has grown to over 300 facilities across Pakistan.</p>--}}
                                    {{--<p>Gym Passport is registered with SECP in Pakistan as Gym Passport SMC-Pvt Ltd with NTN 7095374</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row g-5">
                        <div class="col-lg-6 col-md-6">
                            <div class="office-address text-center">
{{--                                <h2>Office Address in Pakistan:</h2>--}}
                                {!! html_entity_decode($about_us->address_1) !!}
{{--                                <p>Plot # 191 J-1 M-A Johar Town Lahore<br>--}}
{{--                                    Lahore, Punjab 54000,Pakistan</p>--}}
{{--                                <p>Contact: +92 3424267060</p>--}}
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="office-address text-center">
{{--                                <h2>Office Address in Australia:</h2>--}}
{{--                                <p>Unit 1a, 173 Power Street,Glendenning 2761,<br>Sydney Australia</p>--}}
{{--                                <p>Contact: +61 401431864</p>--}}
                                {!! html_entity_decode($about_us->address_2) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
