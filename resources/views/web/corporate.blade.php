@extends('web.layouts.master')
@section('title' , 'Gym Passport - Corporate Partner')

@section('meta')
    <meta name="title" content="Partner with Us and Elevate Your Business with Corporate Gym Access Nationwide!">
    <meta name="description" content="Take your business to the next level with our corporate partnership program. Enjoy unlimited gym access across Pakistan and enhance your employee wellness. Partner with us today!">
@endsection
@section('style')
    <style>
        .gympassport-img img {
            border-radius: 22px;
        }

        @media only screen and (max-width: 400px) {
            .gympassport-img img {
                max-width: 140px;
            }

            .gympassport-main-right h2 {
                font-size: 20px;
            }
        }

        .best-fitness-img {
            max-width: 245px;
        }
    </style>
@endsection
@section('content')
    <!-- GYM-PARTNER-HERO-AREA START -->
    <section class="gym-partner-hero-area corporate-hero">
        <div class="container">
            <div class="gym-partner-hero-content text-center">
                <h1>INTRODUCE <span>HEALTH</span> AND WELLBEING INTO YOUR <span>COMPANY</span></h1>
                <div class="gym-hero-btn">
                    <a href="#submit_formss">Get in Touch</a>
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <!-- GYMPASSPORT AREA START -->
        <section class="gympassport-area overflow-hidden">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-3.svg') }}" alt="" class="img-fluid lazyload"> <span>gympassport</span> <img data-src="{{ asset('assets/images/right-icon-3.svg') }}" alt="" class="img-fluid lazyload"></h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/partnering-title-bg.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2>HOW CAN <span>GYMPASSPORT</span> HELP YOUR COMPANY?</h2>
                </div>
                <div class="gympassport-main">
                    <div class="row g-5">
                        <div class="col-lg-4 col-md-6">
                            <div class="gympassport-main-content">
                                <div class="gympassport-img">
                                    <img data-src="{{ asset('assets/images/pass-1.png') }}" alt="" class="img-fluid lazyload">
                                </div>
                                <a href="https://linktr.ee/gympassport">
                                    <div class="gympassport-main-right">
                                         <h2>Unlimited Access to over {{ $total_gyms }}+ Gyms and <span>Fitness centers</span> across Pakistan.</h2>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="gympassport-main-content">
                                <div class="gympassport-img">
                                    <img data-src="{{ asset('assets/images/corporate_2.jpeg') }}" alt="" class="img-fluid lazyload">
                                </div>
                                <div class="gympassport-main-right" style="padding: 49px 40px 49px 100px">
                                    <h2>Track your employees <span>fitness and
                                            wellness</span> journey.
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="gympassport-main-content">
                                <div class="gympassport-img">
                                    <img data-src="{{ asset('assets/images/corporate_3.jpeg') }}" alt="" class="img-fluid lazyload">
                                </div>
                                <div class="gympassport-main-right">
                                    <h2>Access to <span>other health benefits</span> like doctors, specialists and nutritionists.
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- BENEFITS AREA START -->
        <section class="benefits-area overflow-hidden">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid lazyload"> <span>Benefits</span> <img data-src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid lazyload"></h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/benefits-title-bg.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2><span>BENEFITS</span> TO YOUR COMPANY?</h2>
                </div>
                <div class="partnering-main">
                    <div class="row g-5">
                        <div class="col-lg-4 col-md-6">
                            <div class="partnering-main-content">
                                <div class="partnering-header">
                                    <div class="free-img">
                                        <img data-src="{{ asset('assets/images/benefits-1.svg') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                    <div class="number-img">
                                        <img data-src="{{ asset('assets/images/01.svg') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                </div>
                                <div class="partner-main-title">
                                    <h2>wellness benefits</h2>
                                    <p>Show employees you care about their health and fitness by offering employees wellness benefits.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="partnering-main-content">
                                <div class="partnering-header">
                                    <div class="free-img">
                                        <img data-src="{{ asset('assets/images/customer.svg') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                    <div class="number-img">
                                        <img data-src="{{ asset('assets/images/02.svg') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                </div>
                                <div class="partner-main-title">
                                    <h2>productivity</h2>
                                    <p>Increase employee mental health and productivity of work by up to 30% with a company wide Gym membership.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="partnering-main-content">
                                <div class="partnering-header">
                                    <div class="free-img">
                                        <img data-src="{{ asset('assets/images/benefits-2.png') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                    <div class="number-img">
                                        <img data-src="{{ asset('assets/images/03.svg') }}" alt="" class="img-fluid lazyload">
                                    </div>
                                </div>
                                <div class="partner-main-title">
                                    <h2>unique benefits</h2>
                                    <p>Reduce your staff turnover by up to 20% by offering unique benefits.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- GYM PASSPORT DETAILS-AREA START -->
        <section class="gym-passport-details-area">
            <div class="container">
                <div class="gym-passport-details-main table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="th1" scope="col"></th>
                            <th class="th2" scope="col">
                                <img data-src="{{ asset('assets/images/logo.png') }}" alt="" class="img-fluid lazyload">
                            </th>
                            <th class="th3" scope="col" colspan="2">On-Site Gym</th>
                            <th class="th4" scope="col">Reimbursements</th>
                            <th class="th5" scope="col">Local Gym Access</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="td1" scope="row">
                                <img data-src="{{ asset('assets/images/doller.svg') }}" alt="" class="img-fluid lazyload">
                                <span>Price</span>
                            </td>
                            <td class="td2">$</td>
                            <td class="td3" colspan="2">$</td>
                            <td class="td4">$-$$</td>
                            <td class="td5">$$</td>
                        </tr>
                        <tr>
                            <td class="td1" scope="row">
                                <img data-src="{{ asset('assets/images/access.svg') }}" alt="" class="img-fluid lazyload">
                                <span>Access</span>
                            </td>
                            <td class="td2">Unlimited</td>
                            <td class="td3" colspan="2">Limited Hours</td>
                            <td class="td4">Unlimited</td>
                            <td class="td5">Unlimited</td>
                        </tr>
                        <tr>
                            <td class="td1" scope="row">
                                <img data-src="{{ asset('assets/images/cavareg.svg') }}" alt="" class="img-fluid lazyload">
                                <span>Coverage &<br>
                                                Loation</span>
                            </td>
                            <td class="td2">
                                <i class="fa-sharp fa-solid fa-location-dot"></i>
                                <i class="fa-sharp fa-solid fa-location-dot"></i>
                                <i class="fa-sharp fa-solid fa-location-dot"></i>
                                <i class="fa-sharp fa-solid fa-location-dot"></i>
                            </td>
                            <td class="td3" colspan="2"><i class="fa-sharp fa-solid fa-location-dot"></i></td>
                            <td class="td4"><i class="fa-sharp fa-solid fa-location-dot"></i></td>
                            <td class="td5"><i class="fa-sharp fa-solid fa-location-dot"></i></td>
                        </tr>
                        <tr>
                            <td class="td1" scope="row">
                                <img data-src="{{ asset('assets/images/user.svg') }}" alt="" class="img-fluid lazyload">
                                <span>In-Person <br>
                                                Experience</span>
                            </td>
                            <td class="td2">Full Service gym and <br>
                                studios
                            </td>
                            <td class="td3" colspan="2">Gym Only</td>
                            <td class="td4">Full Service gym and <br>
                                studios
                            </td>
                            <td class="td5">Gym Only</td>
                        </tr>
                        <tr>
                            <td class="td1" scope="row">
                                <img data-src="{{ asset('assets/images/digital.svg') }}" alt="" class="img-fluid lazyload">
                                <span>Digital Solution</span>
                            </td>
                            <td class="td2"><img data-src="{{ asset('assets/images/check.svg') }}" alt="" class="img-fluid lazyload"></td>
                            <td class="td3" colspan="2"><img data-src="{{ asset('assets/images/cross.svg') }}" alt="" class="img-fluid lazyload"></td>
                            <td class="td4"><img data-src="{{ asset('assets/images/cross.svg') }}" alt="" class="img-fluid lazyload"></td>
                            <td class="td5"><img data-src="{{ asset('assets/images/cross.svg') }}" alt="" class="img-fluid lazyload"></td>
                        </tr>
                        <tr>
                            <td class="td1" scope="row">
                                <img data-src="{{ asset('assets/images/heart.svg') }}" alt="" class="img-fluid lazyload">
                                <span>Other Wellness</span>
                            </td>
                            <td class="td2"><img data-src="{{ asset('assets/images/check.svg') }}" alt="" class="img-fluid lazyload"></td>
                            <td class="td3" colspan="2"><img data-src="{{ asset('assets/images/cross.svg') }}" alt="" class="img-fluid lazyload"></td>
                            <td class="td4"><img data-src="{{ asset('assets/images/cross.svg') }}" alt="" class="img-fluid lazyload"></td>
                            <td class="td5"><img data-src="{{ asset('assets/images/cross.svg') }}" alt="" class="img-fluid lazyload"></td>
                        </tr>
                        <tr>
                            <td class="td1" scope="row">
                                <img data-src="{{ asset('assets/images/refer-gym.svg') }}" alt="" class="img-fluid lazyload">
                                <span>Refer a Gym</span>
                            </td>
                            <td class="td2"><img data-src="{{ asset('assets/images/check.svg') }}" alt="" class="img-fluid lazyload"></td>
                            <td class="td3" colspan="2"><img data-src="{{ asset('assets/images/cross.svg') }}" alt="" class="img-fluid lazyload"></td>
                            <td class="td4"><img data-src="{{ asset('assets/images/cross.svg') }}" alt="" class="img-fluid lazyload"></td>
                            <td class="td5"><img data-src="{{ asset('assets/images/cross.svg') }}" alt="" class="img-fluid lazyload"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>

        <!-- BEST FITNESS AREA START -->
        <section class="best-fitness-area best-fitness-area2">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid lazyload"> <span>BEST FITNESS CLUBS</span> <img data-src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid lazyload"></h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/gym-partners-icon.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2>Our <span>Corporate Partners</span> across Pakistan</h2>
                </div>
                <div class="main-content4">
                    <div id="owl-csel4" class="owl-carousel owl-theme">
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/unilever.jpeg') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/afiniti.webp') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/Dastagyr.jpg') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/systems.jpg') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/fatima_group.jpeg') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/EjadLab.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/Fast.jpg') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/Emumba.jpg') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/Synergy.jpg') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/Addo.jpg') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/Conrad-labs.jpg') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/Calrom.jpg') }}" alt="" class="img-fluid lazyload">
                        </div>
                    </div>
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="custom-nav owl-nav"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ACCESS AREA START -->
        <section class="access-area">
            <div class="container">
                <div class="access-area-main">
                    <div class="row">
                        <div class="col-lg-5 col-md-5">
                            <div class="access-left">
                                <h3>Access fitness</h3>
                                <h2>WITH OUR <span>MOBILE APPLICATION</span></h2>
                                <p>Download the Gym Passport app and use
                                    our Free trial to see how it works.
                                </p>
                                <div class="acess-img">
                                    <a href="https://play.google.com/store/apps/details?id=com.ripenapps.gympassport"> <img data-src="{{ asset('assets/images/google-store-1.png') }}" alt="" class="img-fluid lazyload"></a>
                                    <a href="https://apps.apple.com/au/app/gym-passport/id1534995333"><img data-src="{{ asset('assets/images/app-store-1.png') }}" alt="" class="img-fluid lazyload"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7">
                            <div class="access-right">
                                <div class="access-right-img">
                                    <img data-src="{{ asset('assets/images/access-2.png') }}" alt="" class="img-fluid lazyload">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- REGISTER-AREA START -->
        <section class="register-area qoute-area">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid lazyload"> <span>QOUTE</span> <img data-src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid lazyload"></h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/qoute-title-bg.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2>GET A FREE QOUTE</h2>
                </div>
                <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                    </symbol>
                </svg>
                @if(session()->has('msg'))
                    <div class="d-flex ms-auto me-auto" id="alert-form" style="max-width: 1370px">
                        <div class="col-12">
                            <div class="alert {{ session()->get('message') }} d-flex align-items-center justify-content-between" role="alert">
                                <div class="d-flex align-items-center">
                                    <svg class="bi flex-shrink-0 me-3" width="24" height="24" role="img" aria-label="Success:">
                                        <use xlink:href="#check-circle-fill"/>
                                    </svg>
                                    <div class="fs-4 lh-1">
                                        {{ session()->get('msg') }}
                                    </div>
                                </div>
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="register-main">
                    <form action="{{ route('add_corporate_partner') }}" method="post" id="submit_formss">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="text" name="first_name" value="{{ old('first_name') }}" placeholder="Name">
                                    @if ($errors->has('first_name'))<strong class="text">{{ $errors->first('first_name') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="text" name="corporate_name" value="{{ old('corporate_name') }}" placeholder="Company">
                                    @if ($errors->has('corporate_name'))<strong class="text">{{ $errors->first('corporate_name') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="number" name="no_of_employees" value="{{ old('no_of_employees') }}" placeholder="Number of Employees">
                                    @if ($errors->has('no_of_employees'))<strong class="text">{{ $errors->first('corporate_name') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="email" name="email" value="{{ old('email') }}" placeholder="email">
                                    @if ($errors->has('email'))<strong class="text">{{ $errors->first('email') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="name-input">
                                    <input type="text" name="phone_number" value="{{ old('phone_number') }}" placeholder="Phone Number">
                                    @if ($errors->has('phone_number'))<strong class="text">{{ $errors->first('email') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="name-input">
                                    <input type="text" name="address" value="{{ old('address') }}" placeholder="Address">
                                    @if ($errors->has('address'))<strong class="text">{{ $errors->first('address') }}</strong><br>@endif
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="submit-input">
                                    <input type="submit" value="Register Now">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    @if(Session::has('message'))
        <script>
            $("html, body").animate({scrollTop: $("#alert-form").offset().top}, "slow");
        </script>
    @endif
    <script>
        $("#owl-csel4").owlCarousel({
            items: 4,
            autoplay: true,
            autoplayTimeout: 3000,
            startPosition: 0,
            rtl: false,
            loop: true,
            margin: 15,
            // groupCells: true,
            // groupCells: 6,
            dots: true,
            nav: true,
            navText: [
                '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ],
            navContainer: '.main-content4 .custom-nav',
            responsive: {
                0: {
                    items: 3,
                },
                767: {
                    items: 3,
                },
                768: {
                    items: 6,
                },
                992: {
                    items: 6,
                },
                1200: {
                    items: 6,
                }
            }

        });
    </script>
@endsection
