@extends('web.layouts.master')
@section('title' , 'Gym Passport - Return Policy')

@section('meta')
    <meta name="title" content="Understand Gym Passport's Refund Policy before Joining">
    <meta name="description" content="Make an informed decision about joining Gym Passport by thoroughly reading our refund policy. Get all the information you need to ensure a risk-free partnership.">
@endsection
@section('content')
    <!-- GYM-PARTNER-HERO-AREA START -->
    <section class="gym-partner-hero-area blogs-hero">
        <div class="container">
            <div class="gym-partner-hero-content text-center">
                <h1>Gym Passport Returns Policy</h1>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <!-- RETURNS POLICY AREA START -->
        <section class="terms-area">
            <div class="container">
                <div class="terms-main-area">
                    {!! html_entity_decode($refund->description) !!}
{{--                    <div class="policy-content">--}}
{{--                        <h3>Gym Passport Returns Policy</h3>--}}
{{--                        <p>Thank you for being part of the Gym Passport community. When you join Gym Passport you become part of a national network of Gyms and Facilities coming together to make finding and exercising at Fitness centers simple and fun. Our Terms of Service, Privacy Policy, and this Returns Policy contain important information. Please review them carefully.</p>--}}
{{--                        <h4>Return Policy</h4>--}}
{{--                        <h3>Last Updated December 2022</h3>--}}
{{--                        <p>Gym Passport offers a monthly subscription pass. This pass type is valid for a month and auto-renews upon the expiry of the month period.</p>--}}
{{--                        <p>Subscription Fee - The fee for a Gym Passport Subscription is set forth during the purchasing process. The Gym Passport Subscription fee is non-refundable except as expressly set forth in the Terms and Conditions or as required under applicable law. There are no partial refunds.</p>--}}
{{--                        <p>Gym Passport may change the price for any Subscription, including recurring Subscription fees, from time to time, and will communicate any such changes to you in advance. Subject to applicable law, you accept the new price by not cancelling your subscription after the price change takes effect. If you do not agree with a price change, you have the right to reject the change by unsubscribing to the Services prior to the price change going into effect.</p>--}}
{{--                        <p>Subscription Cancellation - You may cancel your subscription at any time by canceling under your profile/my account/ my plan after which Gym Passport will stop automatically renewing your subscription.</p>--}}
{{--                        <p>PLEASE NOTE THAT NO REFUNDS WILL BE ISSUED UPON CANCELLATION.</p>--}}
{{--                        <p>If you have cancelled your account or modified any data on your account by mistake, contact us immediately. We will try to help, but unfortunately, we can’t promise that we can recover or restore anything and shall not be liable to you for such accidental cancellation or modifications.</p>--}}
{{--                        <p class="last-details">For any further questions or queries on our terms and services please contact us on support@gympassport.pk</p>--}}
{{--                    </div>--}}
                </div>
            </div>
        </section>
    </div>
@endsection
