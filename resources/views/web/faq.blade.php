@extends('web.layouts.master')
@section('title' , 'Gym Passport - Faqs')

@section('meta')
    <meta name="title" content="Get Quick Answers to Your Questions with Gym Passport's FAQs">
    <meta name="description" content="Find answers to your questions about Gym Passport's partnership program and business expansion opportunities. Read our comprehensive FAQs for more information.">
@endsection
@section('content')
    <!-- GYM-PARTNER-HERO-AREA START -->
    <section class="gym-partner-hero-area blogs-hero">
        <div class="container">
            <div class="gym-partner-hero-content text-center">
                <h1>Frequently asked Questions</h1>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <!-- RETURNS POLICY AREA START -->
        <section class="terms-area faq-area">
            <div class="container">
                <div class="terms-main-area">
                    <div class="policy-content">
                        {!! html_entity_decode($faq->description) !!}
                        {{--<h3>1) What is the benefit of Gym Passport?</h3>--}}
                        {{--<p>Gym Passpor has partnered with hundreds of Gyms across Pakistan across multiple cities. If you purchase a Gym Passport membership through the Gym Passport app - you can access and train at all of the Gyms we have available.</p>--}}
                        {{--<p>Most of our members use Gym Passport to train with friends or train while they are travelling. Many members also visit different Gyms as they provide different facilities. Some Gyms may have better facilities for crossfit, and some may have better muscle building equipment - you can visit different Gyms on different days and enjoy the benefiits of multiple Gyms.</p>--}}
                        {{--<span>Our monthly membership price is quite affordable with no registration fees, making convenient access to fitness easy on<br> your wallet!</span>--}}
                        {{--<h4>2) How does Gym Passport work?</h4>--}}
                        {{--<p>Gym Passport has partnered with hundreds of Gyms across Pakistan across multiple cities. If you purchase Gym Passport membership through the Gym Passport app - you can access and train at all of the Gyms we have available. Each one of our available Gyms has a Gym Passport QR code displayed at the Gym. You need to simply visit the Gym, and tell the staff member that you are a Gym passport member. Gym staff will then show you the QR code to scan. Once you have scanned the QR code you can enjoy your workout.</p>--}}
                        {{--<h4>3) How much does Gym Passport membership cost?</h4>--}}
                        {{--<p>Gym Passport has three different membership passes. Each one of our pass is a monthly pass, providing you access to our available Gyms for 30 days. Our Gym Passport LITE pass is our cheapest pass valued at PKR 3500/month which provides you access to multiple low-tier Gyms. Our Gym Passport STANDARD pass is our middle range pass valued at PKR 4500/month which provides you access to both mid-tier and low-tier Gyms. Our Gym Passport PRO pass is our high range pass valued at PKR 7500/month which provides you access to all of our available Gyms including our premium Gyms.</p>--}}
                        {{--<h4>4) How many check-ins does your membership offer?</h4>--}}
                        {{--<p>Gym Passport offers 20 check-ins under each of our passes. Our membership pass is a monthly pass valid for 30 days. You receive 20 check-ins (20 visits) to Gyms available under your pass each month. The average person working out in Pakistan visits a Gym 13 times in a month, we provide 20 visits to make sure you have more than enough visits.</p>--}}
                        {{--<h4>5) Can we try Gym Passport before we purchase a membership?</h4>--}}
                        {{--<p>Yes! Defnitely! We understand this is a new concept in Pakistan, for this reason we offer a Free Trial for our members to visit and train at any of our available Gyms. Our Free Trial provides you 1 visit access for free to any Gym - no payment needed. After you have used your Free trial and understand how we work, you can easiily purchase a monthly pass and continue using our services.</p>--}}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
