@extends('web.layouts.master')
@section('title' , 'Gym Passport - Contact Us')

@section('meta')
    <meta name="title" content="Get in Touch with Us - Contact Gym Passport Today">
    <meta name="description" content="Reach out to us and take the first step towards expanding your business with our gym passport partnership program. Contact us today for more information.">
@endsection

@section('content')
    <!-- GYM-PARTNER-HERO-AREA START -->
    <section class="gym-partner-hero-area blogs-hero">
        <div class="container">
            <div class="gym-partner-hero-content text-center">
                <h1>Contact Us</h1>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <!-- CONTACT US AREA START -->
        <section class="contact-us-area">
            <div class="container">
                <div class="contact-us-main">
                    <div class="row g-5">
                        <div class="col-lg-6">
                            <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                                <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                                </symbol>
                            </svg>
                            @if(session()->has('msg'))
                                <div class="d-flex ms-auto me-auto" id="alert-form" style="max-width: 1370px">
                                    <div class="col-12">
                                        <div class="alert {{ session()->get('message') }} d-flex align-items-center justify-content-between" role="alert">
                                            <div class="d-flex align-items-center">
                                                <svg class="bi flex-shrink-0 me-3" width="24" height="24" role="img" aria-label="Success:">
                                                    <use xlink:href="#check-circle-fill"/>
                                                </svg>
                                                <div class="fs-4 lh-1">
                                                    {{ session()->get('msg') }}
                                                </div>
                                            </div>
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="contact-us-left">
                                <h2>Contact Us</h2>
                                <form action="{{ route('submit_contactus') }}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="contact-input-area">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="contact-input">
                                                    <input type="text" id="name" name="name" value="@if(isset(Auth::user()->first_name)){{Auth::user()->first_name}} {{Auth::user()->last_name}}@endif" placeholder="Enter Name">
                                                    @if ($errors->has('name'))<p class="error" for="name">{{ $errors->first('name') }}</p>@endif
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="contact-input">
                                                    <input type="email" name="email" value="@if(isset(Auth::user()->first_name)){{Auth::user()->email}}@endif" placeholder="Email Address">
                                                    @if ($errors->has('email'))<p class="error" for="email">{{ $errors->first('email') }}</p>@endif
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="contact-input">
                                                    <input type="text" name="contact" id="contact" value="@if(isset(Auth::user()->first_name)){{Auth::user()->phone_number}}@endif" placeholder="Contact Number">
                                                    @if ($errors->has('contact'))<p class="error" for="contact">{{ $errors->first('contact') }}</p>@endif
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="contact-input">
                                                    <textarea class="form-control" name="message" id="message" rows="3" placeholder="Message" required="">{{ old('message') }}</textarea>
                                                    @if ($errors->has('message')){{ $errors->first('message') }}@endif
                                                </div>
                                            </div>
                                            <div class="g-recaptcha" data-sitekey="6LeZNEAaAAAAAE2iHazV4XL_BsaxzZHWVzM1EKDa"></div>
{{--                                            <br/>--}}
                                            <div class="col-lg-12">
                                                <div class="contact-input-btn">
                                                    <input type="submit" value="Send Query">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="contact-us-right">
                                <div class="contact-us-img">
                                    <img src="{{ asset('assets/images/contact-us.jpg') }}" alt="" class="img-fluid">
                                </div>
                                <div class="office-address text-center">
                                    <h2>contact us</h2>
                                    @if(isset(Auth::user()->first_name))
                                        @if(Auth::user()->user_type==2)
                                            <p>Phone: <a href="tel:0342-4267060"><label class="value" style="color: rgb(107 , 107 , 107);">0342-4267060</label></a></p>
                                        @endif
                                    @else
                                        <p>Phone: <a href="tel:0342-4267060"><label class="value" style="color: rgb(107 , 107 , 107);">03424267060</label></a></p>
                                    @endif
                                    @if(isset(Auth::user()->first_name))
                                        @if(Auth::user()->user_type==2)
                                            <p>Email Address:  <label class="value email_value" style="cursor: pointer; color: rgb(107 , 107 , 107);">support@gympassport.pk</label></p>
                                        @endif
                                        @if(Auth::user()->user_type==3)
                                            <p>Email Address:  <label class="value email_value_user" style="cursor: pointer; color: rgb(107 , 107 , 107);">support@gympassport.pk</label></p>
                                        @endif
                                    @else
                                        <p>Email Address:  <label class="value email_value_user" style="cursor: pointer; color: rgb(107 , 107 , 107);">support@gympassport.pk</label></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            @if(isset(Auth::user()->first_name))
                                @if(Auth::user()->user_type==2)
                                    <div id="googleMap_old" style="width:100%;height:317px;"></div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- BEST FITNESS AREA START -->
        <section class="best-fitness-area best-fitness-area2 about-us-fitnss">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid">  <span>BEST FITNESS CLUBS</span> <img src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid"> </h3>
                    <div class="title-img">
                        <img src="{{ asset('assets/images/gym-partners-icon.svg') }}" alt="" class="img-fluid">
                    </div>
                    <h2>our <span>gym partners</span> across Pakistan</h2>
                </div>
                <div class="main-content6">
                    <div id="owl-csel7" class="owl-carousel owl-theme">
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="best-fitness-img">
                            <img src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="custom-nav owl-nav"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    @if(Session::has('message'))
        @if(session('message') == 'alert-danger')
            <script>
                $("html, body").animate({ scrollTop: $("#alert-form").offset().top }, "slow");
            </script>
        @endif
    @endif
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        $(document).ready(function () {
            $('.email_value').on('click', function () {
                window.location.href = "mailto:partner@ufitpass.com?subject=UFPSupport&body=Eneter your message here..";
            });

            $('.email_value_user').on('click', function () {
                window.location.href = "mailto:support@ufitpass.com?subject=UFPSupport&body=Eneter your message here..";
            });
        });

        function myMap() {
            var mapProp = {
                center: new google.maps.LatLng(-33.868820, 151.209290),
                zoom: 14,
                styles: [
                    {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                    {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                    {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                    {
                        featureType: 'administrative.locality',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#d59563'}]
                    },
                    {
                        featureType: 'poi',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#d59563'}]
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'geometry',
                        stylers: [{color: '#263c3f'}]
                    },
                    {
                        featureType: 'poi.park',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#6b9a76'}]
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry',
                        stylers: [{color: '#38414e'}]
                    },
                    {
                        featureType: 'road',
                        elementType: 'geometry.stroke',
                        stylers: [{color: '#212a37'}]
                    },
                    {
                        featureType: 'road',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#9ca5b3'}]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry',
                        stylers: [{color: '#746855'}]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'geometry.stroke',
                        stylers: [{color: '#1f2835'}]
                    },
                    {
                        featureType: 'road.highway',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#f3d19c'}]
                    },
                    {
                        featureType: 'transit',
                        elementType: 'geometry',
                        stylers: [{color: '#2f3948'}]
                    },
                    {
                        featureType: 'transit.station',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#d59563'}]
                    },
                    {
                        featureType: 'water',
                        elementType: 'geometry',
                        stylers: [{color: '#17263c'}]
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.fill',
                        stylers: [{color: '#515c6d'}]
                    },
                    {
                        featureType: 'water',
                        elementType: 'labels.text.stroke',
                        stylers: [{color: '#17263c'}]
                    }
                ]
            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        }

        $("#owl-csel7").owlCarousel({
            items: 4,
            autoplay: true,
            autoplayTimeout: 3000,
            startPosition: 0,
            rtl: false,
            loop: true,
            margin: 15,
            // groupCells: true,
            // groupCells: 6,
            dots: true,
            nav: true,
            navText: [
                '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ],
            navContainer: '.main-content7 .custom-nav',
            responsive:{
                0: {
                    items: 3,
                },
                767: {
                    items: 3,
                },
                768: {
                    items: 6,
                },
                992: {
                    items: 6,
                },
                1200: {
                    items: 6,
                }
            }

        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeKf5myvnzRx4e9fmgzzXGHgYSrJUjXuU&callback=myMap"></script>
@endsection
