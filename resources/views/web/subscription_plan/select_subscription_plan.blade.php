@extends('web_user_dash.design')
@section('content')
<section class="booking-details-wrapper">
   <div class="container-fluid">
      <div class="sub-plan-details-box1">
         <form action="{{route('sub_plan_detail')}}" method="post">
                {{ csrf_field() }}
            <input type="hidden" name="plan_cat_id" value="{{$membership_detail['plan_cat_id']}}">
            <div class="first-wrapper">
               <div class="key-value-div-sub-plan">
                  <h4 class="same-h4-sub-plan">Plan Name</h4>
               </div>
               <div class="key-value-div-sub-plan value-sub-plan">
                  <!--  <input type="text" name="" class="payable-value-sub-plan border-class" placeholder="Ultimate Fitness Pass"> -->
                  <label class="payable-value-sub-plan border-class">{{$membership_detail['plan_cat_name']}}</label>
               </div>
               <div class="key-value-div-sub-plan">
                  <h4 class="same-h4-sub-plan">Plan type</h4>
               </div>
               <div class="key-value-div-sub-plan value-sub-plan list-icon-sub-plan">
                  <select class="list-sub-plan" id="plan_type" name="plan_id">
                     <option class="master-card" value="0" >Select plan type</option>
                      @if($membership_detail['plan_detail'])
                        @foreach($membership_detail['plan_detail'] as $key=>$value)
                          <option value="{{$value['plan_id']}}" class="cards-sub-plan">{{$value['visit_pass']}}</option>
                        @endforeach
                      @endif
                  </select>
               </div>
               <div class="key-value-div-sub-plan">
                  <h4 class="same-h4-sub-plan">Plan Amount</h4>
               </div>
               <div class="key-value-div-sub-plan value-sub-plan">
                  <!-- <input type="text" name="" class="payable-value-sub-plan border-class" placeholder="$5" style="width: 30%;"> -->
                  <label class="payable-value-sub-plan border-class" style="width: 39%" id="amount">$0</label>
               </div>
               <div class="key-value-div-sub-plan">
                  <h4 class="same-h4-sub-plan">Plan Description</h4>
               </div>
               <!--  <div class="col-sm-12"> -->
               <!-- <div class="msg-box-sub-plan">
                  <textarea rows="4" cols="3">lorem ipsum sit amet lorem ipsum sit amet lorem ipsum sit amet lorem ipsum sit amet lorem ipsum sit amet lorem ipsum sit amet lorem ipsum sit amet lorem ipsum sit amet
                  </textarea>
                  </div> -->
               <div class="msg-box-sub-plan">
                  <label id="text">Plan description</label>
               </div>
               <!--  </div> -->
               <div class="purchase-plan-btn-box">
                  <button type="submit" class="btn btn-danger purchase-plan-btn">Purchase Plan</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</section>
  <script type="text/javascript">
    $('#plan_type').change(function(){
        var plan_id = this.value;
        
        if(plan_id =='0'){
            $('#amount').html('$0');
            $('#text').html('Plan description');
            $(':input[type="submit"]').prop('disabled', true); 
        }
      if(plan_id !='0'){
           $.ajax({
            url:"{{route('ajax_price')}}",
            data: {'plan_id':plan_id, '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              //alert(data);
              var value = JSON.parse(data);
              $('#amount').html(value.amount);
              $('#text').html(value.description);  
              $(':input[type="submit"]').prop('disabled', false);
            }
        });
      }
        
    });
   </script>



   <script type="text/javascript">
     $(document).ready(function() {
     $(':input[type="submit"]').prop('disabled', true);
     });

   </script>
@endsection