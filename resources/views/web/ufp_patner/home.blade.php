@extends('web_user_dash.design')
@section('content')
<style type="text/css">

    .box-collapsee {
        cursor: pointer;
    }
    #page-selection{
        float: right;
    }
    #checkinpage{
        float: right;
    }

    .dropdata{
        color: #9c9c9c;
        font-size: 20px;
        font-family: Raleway;
        font-weight: 600;
        margin-top: 9px;
        padding: 23px 7px;
    }
    .checkin-page-facilities{

        font-size: 16px;
    }
    @media(max-width: 1200px){
        .checkin-page-facilities{
            font-size: 13px;
        }
    }
    @media(max-width: 992px){
        .checkin-page-facilities{
            font-size: 11px;
        }
        .custom-form-key-value {
            margin: 0 auto;
            width: 100%;
            margin-right: 0px;
            max-width: 100%;

        }
    }
    @media(max-width: 768px){
        .custom-form-key-value {
            text-align: center;
            margin: 0 auto;
            /* display: flex; */
            justify-content: center;
            width: 66%;
            max-width: 83%;
        }
        .three-main-heading {
            margin-left: 22px;
        }
        .facilities-image-class{
            width: 100px;
        }
        .last-image-class{
            width: 200px;
            margin-bottom: 20px;

        }
        .three-two-btns{
            margin-left: 257px;
        }
    }
    @media(max-width: 700px){
        .three-two-btns {
            margin-left: 238px;
        }
    }
    @media(max-width: 630px){
        .three-two-btns {
            margin-left: 225px;
        }
    }
    /*@media(max-width: 768px){
    .custom-form-key-value {
       margin-right: 0px;
    }
        
    }
    @media(max-width: 575px){
    .custom-form-key-value {
       margin-right: -69px;
    }
        
    }*/
    @media(max-width: 575px){
        .three-main-heading {
            margin-left: -50px;
        }
        .facilities-image-class{
            width: 41px;
        }
        .last-image-class{
            width: 100px;
        }
        .three-two-btns{
            margin-left: 157px;
        }
    }
    @media (max-width: 530px){
        .three-two-btns {
            margin-left: 143px;
        }
    }
    @media (max-width: 450px){
        .three-two-btns {
            margin-left: 126px;
        }
        .checkin-page-facilities {

            padding-left: 5px;
            margin-top: 8px;
        }
    }
    @media(max-width: 430px){
        .three-main-heading {
            margin-left: -5px;
        }
    }
    @media(max-width: 375px){
        .three-two-btns {
            margin-left: 106px;
        }
    }
    @media(max-width: 350px){
        .three-two-btns {
            margin-left: 97px;
        }
    }
</style>
<section class="three-user-profile-wrapper">
    <div class="three-user-profile-part1">
        <div class="three-for-bg">
            @if(Session::has('message'))
            @if(session('message')=='alert-danger')
            <script type="text/javascript">
                swal({
                    title: "{!! session('msg') !!}",
                    text: "",
                    icon: 'error'
                });
            </script>
            @else
            <script type="text/javascript">
                swal({
                    title: "{!! session('msg') !!}",
                    text: "",
                    icon: 'success'
                });
            </script>
            @endif 
            <!-- <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               
               </div> -->
            @endif
            <!--         @if(Session::has('message'))
               <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   {!! session('msg') !!}
               </div>
               @endif-->
            <h3 class="three-main-heading">Partner Profile</h3>
            <form class="three-left-form" method="post" action="{{route('update_profile')}}">
                {{ csrf_field() }}
                <div class="custom-form-key-value">
{{--                    <label class="three-label">User Id</label>--}}
{{--                    <input type="number" class="three-values" placeholder="12345" disabled value="{{Auth::user()->user_ufp_id}}">--}}
                    <label class="three-label">First Name</label>
                    <input type="text" name="first_name" class="three-values bord enable-class" placeholder="John" disabled=""  value="{{Auth::user()->first_name}}" required>
                    @if ($errors->has('first_name'))<label class="error" for="">{{ $errors->first('first_name') }}</label>@endif
                    <label class="three-label">Last Name</label>
                    <input type="text" name="last_name" class="three-values bord enable-class" placeholder="Doe" disabled=""  value="{{Auth::user()->last_name}}" required>
                    @if ($errors->has('last_name'))<label class="error" for="">{{ $errors->first('last_name') }}</label>@endif
                    <label class="three-label">Email</label>
                    <input type="email" name="email" class="three-values bord" placeholder="abc@gmail.com" disabled="disabled" value="{{Auth::user()->email}}" required>
                    @if ($errors->has('email'))<label class="error" for="">{{ $errors->first('email') }}</label>@endif


                    @if ($errors->has('City'))<label class="error" for="">{{ $errors->first('city') }}</label>@endif
                    <label class="three-label">City</label>
                    <input type="text" name="city" class="three-values bord enable-class" placeholder="Enter City" disabled="disabled" value="{{Auth::user()->city}}" required>
                    @if ($errors->has('city'))<label class="error" for="">{{ $errors->first('city') }}</label>@endif

                    <!--               <label class="three-label">D.O.B</label>
                       <input type="date" name="d_o_b" class="three-values bord enable-class" placeholder="07/09/1995" disabled="" value="{{date('Y-m-d',strtotime(Auth::user()->d_o_b))}}" required>
                       @if ($errors->has('d_o_b'))<label class="error" for="">{{ $errors->first('d_o_b') }}</label>@endif-->
{{--                    <label class="three-label">Country</label>--}}
{{--                    <select class="three-values bord enable-class" name="country" disabled>--}}
{{--                        <option value="">Select Country</option>--}}
{{--                        @if(!empty($country))--}}
{{--                        @foreach($country as $key=>$value)--}}
{{--                        <option value="{{$value->id}}" class="options" @if($value->id==Auth::user()->country) selected @endif>{{$value->nicename}}</option>--}}
{{--                        @endforeach--}}
{{--                        @endif--}}
{{--                    </select>--}}
                    @if ($errors->has('country'))<label class="error" for="">{{ $errors->first('country') }}</label>@endif
                </div>
                <div class="three-two-btns">
                    <button type="button" class="btn btn-danger three-edit-profile edit">Edit Profile</button>
                    <button type="submit" class="btn btn-danger three-edit-profile update" style="display: none;">Update</button>
                    <button type="button" class="btn btn-danger three-cancel">Cancel</button>
                    <a href="{{route('UserPasswordChange')}}"><button type="button" class="btn btn-danger three-change-password">Change Password</button></a>
                    @if($user_type  != 4)
                        <a href="{{route('addstaff')}}"><button type="button" class="btn btn-danger three-change-password">{{ isset($staff)?"Edit ":"Add " }}Staff Account</button></a>
                    @endif
                    <?php /*
                    <a href="{{route('UserGymTiming')}}"><button type="button" class="btn btn-danger three-change-password">Gym Timing</button></a> */ ?>
                </div>
            </form>
        </div>
    </div>


    <div class="three-user-profile-part-twoo">
        <div class="three-user-profile-part2-collapsee">
            @if(!empty($gym_data))
            @foreach($gym_data as $key=>$val)
            <div class="row">
                <div class="col-sm-12" style="margin-bottom: 14px;">
                    <div class="box-collapsee" data-toggle="collapse" data-target="#demo-collapsee-page{{$val->id}}">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="collapse-page-img">
                                    <img src="{{trans('constants.image_url')}}{{$val->gym_logo}}" alt="image" class="img-fluid" style="border-radius: 50%;height: 78px;width: 78px; padding: 4px 2px;">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <p class="p-collapse-page">{{$val->gym_name}}</p>
                            </div>
                            <div class="col-sm-2">
                                <a href="{{route('owner_edit_gym',['gym_id'=>encrypt($val->id)])}}" class="p-collapse-page" style="margin-top: 25px;"><i class="fa fa-edit edit_custom_cls"></i></a>
                            </div>
                        </div>
                    </div>
                    <div id="demo-collapsee-page{{$val->id}}" class="dropdata collapse collapsible-element">
                        <p class="para-collaps-page">Gym Name :<span class="showgym_data">{{$val->gym_name}}</span> </p>
                        <p class="para-collaps-page">Address :<span class="showgym_data">{{$val->gym_address}}</span> </p>
                        <p class="para-collaps-page">Phone Number :<span class="showgym_data">{{$val->phone_number}}</span> </p>
       <!--                 <p class="para-collaps-page">Longitude and latitude :<span class="showgym_data">{{$val->gym_latitude}},{{$val->gym_longitude}}</span> </p>-->
                        <p class="para-collaps-page">Facilities :<span class="showgym_data"></span> </p>
                        @if(!empty($val->facilities))
                            @foreach($val->facilities as $key=>$fac)
                               @if(!empty($fac->gym_facilities))
                                    <div class="row">
                                        <div class="col-2 col-sm-2">
                                            <img src="{{ asset($fac->gym_facilities->imag_url) }}" alt="image" class="img-fluid facilities-image-class">
                                        </div>
                                        <div class="col-5 col-sm-5">
                                            <p class="checkin-page-facilities">{{$fac->gym_facilities->facilities}}</p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                        <p class="para-collaps-page">Images : <span class="showgym_data"></span> </p>
                        @if(!empty($val->gym_image))
                        @foreach($val->gym_image as $key=>$img)
                        <!--  <div class="row"> -->

                        <div class="col-sm-2">
                            <img src="{{trans('constants.image_url')}}{{$img->gym_image}}" alt="image" class="img-fluid last-image-class" data-toggle="modal" data-target="#myModal{{$img->id}}">

                            <div class="modal fade" id="myModal{{$img->id}}" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content home-change-modal-content">
                                        <!-- <div class="modal-header home-change-modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div> -->

                                        <!--  <div class="modal-body home-change-modal-body"> -->
                                        <div class="modal-image-home-change">
                                            <img src="{{trans('constants.image_url')}}{{$img->gym_image}}" alt="image" class="img-fluid" style="min-width:100%">
                                            <!--  </div> -->
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- </div> -->
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
    <!-- Modal -->
    @if(isset(Auth::user()->id) && Auth::user()->partner_first_login==1)
    <div class="container">
        <div id="myModalfirst" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content content-of-modal-custom">
                    <div class="modal-header header-of-modal-custom">
                        <button type="button" class="close modal-close-btn-custom" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body body-of-modal-custom">
                        <h4 class="login-act-heading">Update Password</h4>
                        <div class="content">
                            <div class="form loginBox">
                                <form method="post" action="{{route('update_pass')}}">
                                    {{ csrf_field() }}
                                    <div class="custom-input-fields-box">
                                        <input id="password" class="form-control modal-custom-input-fields" type="password" placeholder="Password" name="password" value="{{old('password')}}" required autocomplete="off">
                                        @if ($errors->has('password'))<label class="first_error" for="">{{ $errors->first('password') }}</label>@endif<br>
                                        <input id="password" class="form-control modal-custom-input-fields" type="password" placeholder="Confirm Password" name="confrimed_password" value="{{old('confrimed_password')}}" required autocomplete="off">
                                        @if ($errors->has('confrimed_password'))<label class="first_error" for="">{{ $errors->first('confrimed_password') }}</label>@endif<br>
                                    </div>
                                    <div class="submit-btn-login-box">
                                        <input class="btn btn-default btn-login-modal" type="submit" id="submit_form" value="Update">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <style type="text/css">
        label.first_error {
            color: red;
            text-align: left;
            float: left;
            padding-left: 107px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#myModalfirst').modal('toggle');
        });
    </script>
    <!-- @if ($errors->has('password') || $errors->has('confrimed_password'))
         <script type="text/javascript">
           $(document).ready(function() {
               $('#myModalfirst').modal('toggle');
           });
         </script>
    @endif -->
    @endif
</section>
<script>

//$('#myModalfirst').modal('show');
    $('.three-cancel').click(function () {
        $(".bord").css("background-color", "transparent");
        $(".enable-class").prop("disabled", true);
        $(".three-cancel").hide();
        $(".update").hide();
        $(".edit").show();
    });

    $('.edit').click(function () {
        $(".bord").css("background-color", "#fff");
        $(".bord").css("padding", "0 5px");
        $(".enable-class").prop("disabled", false);
        $(".update").show();
        $(".three-cancel").show();
        $(".edit").hide();
    });

    $(".three-cancel").hide();
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $.ajax({
            url: "{{route('billing_histroy')}}",
            data: {},
            type: "get",
            success: function (data) {
                $("#billing_detail").html(data);
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $.ajax({
            url: "{{route('check_in_histroy')}}",
            data: {},
            type: "get",
            success: function (data) {
                $("#gym_check_in_list").html(data);
            }
        });
    });
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
@endsection