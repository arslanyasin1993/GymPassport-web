@extends('web_user_dash.design')
@section('content')
    <section class="pay-hist-section">
        <style type="text/css">
            .custommsg {
                margin-top: 15px;
                width: 85%;
                margin-left: 53px;
            }

            .error {
                color: red;
                margin: 0px 0px 0px -17px;
            }

            .list-paym-hist-page {
                width: 43%;
            }

            .value-box-paym {
                padding: 0px 0px;
            }

            .star {
                color: red;
            }

            .file_msg {
                color: red;
            }


            #fakeDiv, #fakeDiv2, #fakeDiv3, #fakeDiv4 {
                position: relative;
                margin-top: 45px;
            }

            #selectedFile, #selectedFile2, #selectedFile3, #selectedFile4 {
                opacity: 0;
                position: absolute;
                left: 0;
                top: 0;
            }

            #loader {
                left: 49%;
                top: 50%;
            }

            .alert_msg {
                color: red;
                font-size: 16px;
            }

            .month-name.month-color {
                background-color: #036d03;
            }

            .month-name.month-color.active {
                background-color: #fff;
            }
        </style>
        <div class="pay-hist-menu-box">
            <ul class="nav nav-pills" role="tablist" id="myTab">
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#menu-one">Payment History</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#menu-two">Payment Accounts</a>
                </li>

            </ul>
        </div>
        <div class="tab-content">
            <div id="menu-one" class="container-fluid tab-pane ">
                <div class="d-flex justify-content-center">
                    <div class="select-box-wrapper" style="max-width: 35% !important; margin: 0 10px !important;">
                        <label style="padding: unset !important;" class="select-box-paym">Select Gym</label>
                        <select class="list-paym-hist-page" name="payment_gym_name" id="payment_gym_name">
                            @if(!empty($gym_list))
                                @foreach($gym_list as $key=>$value)
                                    <option class="options" value="{{$value->gym_name}}">{{$value->gym_name}}</option>
                                @endforeach
                                @if(count($gym_list) > 1)
                                    <option value="all">All</option>
                                @endif
                            @endif
                        </select>
                    </div>
                    <div class="select-box-wrapper" style="max-width: 35% !important; margin: 0 10px !important;">
                        <label style="padding: unset !important;" class="select-box-paym">Select Month</label>
                        <input type="month" class="list-paym-hist-page" name="date_filter" id="date_filter">
                    </div>
                </div>
                <a href="{{route('paymentDownload')}}"><button style="margin-top: 10px !important;" type="submit" class="btn btn-danger fixed-btn-checkins">Generate Report</button></a>
                {{--            <div class="owl-carousel owl-theme owl-loaded owl-drag" id="slider2">--}}

                {{--            </div>--}}
                <div class="earnings-heading">
                    <h5>Total Earnings <span id="total_earn">0</span> Rs</h5>
                    <!--                <p style="color:red;">Payment History will reflect in 24 hours after payout run</p>-->
                </div>
                <div class="pay-hist-wrapper">
                    <div class="row">
                        <div class="col-4 col-sm-6">
                            <p class="cus-details-paym cust-nam">Transaction id</p>
                        </div>
                        <div class="col-4 col-sm-3">
                            <p class="cus-details-paym date-nam">Date</p>
                        </div>
                        <div class="col-4 col-sm-3">
                            <p class="cus-details-paym earn-nam">Earnings</p>
                        </div>
                    </div>
                </div>
                <div class="four-rows-paym" id="month_wise_user">
                    <div class="row-fst-paym">
                        <div class="row">
                            <p class="row-one-details" style="text-align: center;">No record found/p>
                        </div>
                    </div>

                </div>
            </div>
            <div id="menu-two" class="tab-pane fade active">
                <div class="main-box-bank-det">
                    {{-- @if(Session::has('message'))
                    <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible custommsg">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {!! session('msg') !!}
                    </div>
                    @endif --}}
                    @if(Session::has('message'))
                        @if(session('message')=='alert-danger')
                            <script type="text/javascript">
                                swal({
                                    title: "{!! session('msg') !!}",
                                    text: "",
                                    icon: 'error'
                                });
                            </script>
                        @else
                            <script type="text/javascript">
                                swal({
                                    title: "{!! session('msg') !!}",
                                    text: "",
                                    icon: 'success'
                                });
                            </script>
                        @endif
                    @endif
                    <div id="loader" style="display: none;"></div>
                    @if(empty($bank_detail))
                        <form action="{{route('add_bank_detaiil')}}" method="post" enctype="multipart/form-data">
                            <div class="container-fluid tab-pane active" style="margin-top: 50px;">
                                <div class="bank-det value-box-paym" style="text-align: center; width: 100%;float: left;margin-top: 50px;">
                                    <label class="select-box-paym">Select Payment Account</label>
                                    <select class="list-paym-hist-page" name="account_type" id="choose_payment_type">
                                        <option class="options" value="">Select One Option</option>
                                        <option class="options" value="1">Jazz Cash</option>
                                        <option class="options" value="2">Bank Account</option>
                                    </select>
                                </div>
                            </div>
                        <!--<form action="{{route('add_account')}}" method="post" enctype="multipart/form-data">-->
                        {{ csrf_field() }}

                        <!--                    <div class="box-one-paym" style="width: 90%">
                        <p class="alert_msg">Note: Please check this form carefully before submitting.if this form is submitted incorrectly,you will need to contact us to update the form.</p>
                        <p class="alert_msg">Note: First name, Last name, Date of Birth can not be updated once this form is submitted.  </p>
                        <h4 class="key-paym-heading">Company and Director information :</h4>
                    </div>
                    

                    <div class="box-one-paym">
                        <div class="bank-det">
                            <h3 class="key-paym">First Name <span class="star">*</span></h3>
                        </div>
                        <div class="bank-det value-box-paym">
                            <input type="text" name="first_name" class="value-paym" placeholder="First Name" value="{{old('first_name')}}">
                            @if ($errors->has('first_name'))<br><span class="error" for="">{{ $errors->first('first_name') }}</span>@endif

                                </div>
                            </div>

                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">Last Name <span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <input type="text" name="last_name" class="value-paym" placeholder="Last Name" value="{{old('last_name')}}">
                            @if ($errors->has('last_name'))<br><span class="error" for="">{{ $errors->first('last_name') }}</span>@endif

                                </div>
                            </div>
                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">Email <span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <input type="email" name="email" class="value-paym" placeholder="Email Id" value="{{old('email')}}">
                            @if ($errors->has('email'))<br><span class="error" for="">{{ $errors->first('email') }}</span>@endif
                                </div>
                            </div>
                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">Phone Number <span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                <input type="number" name="phone" class="value-paym" placeholder="Phone Number" value="{{old('phone')}}">
                        <input type="number" name="phone" class="value-paym" placeholder="Phone Number" value="@if(empty(old('phone'))){{Auth::User()->phone_number}}@endif{{old('phone')}}">
                            @if ($errors->has('phone'))<br><span class="error" for="">{{ $errors->first('phone') }}</span>@endif
                                </div>
                            </div>
                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">D.O.B <span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <input type="date" name="dob" class="value-paym" placeholder="Date Of Birth" value="{{old('dob')}}">
                            @if ($errors->has('dob'))<br><span class="error" for="">{{ $errors->first('dob') }}</span>@endif
                                </div>
                            </div>
                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">Country <span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <select class="value-paym" name="country">
@if(!empty($country))
                            @foreach($country as $value)
                                <option value="{{$value->iso}}" @if(Auth::User()->country == $value->id) selected @endif>{{$value->nicename}}</option>
                                @endforeach
                        @endif
                                </select>
@if ($errors->has('country'))<br><span class="error" for="">{{ $errors->first('country') }}</span>@endif
                                </div>
                            </div>
                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">State <span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <select class="value-paym" name="state">
@if(!empty($state))
                            @foreach($state as $value)
                                <option value="{{$value->state_code}}" @if(Auth::User()->state == $value->id) selected @endif>{{$value->state}}</option>
                                @endforeach
                        @endif
                                </select>
@if ($errors->has('state'))<br><span class="error" for="">{{ $errors->first('state') }}</span>@endif
                                </div>
                            </div>
                             <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">Postal Code <span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <input type="number" name="postal_code" class="value-paym" placeholder="Postal Code" value="{{old('postal_code')}}">
                            @if ($errors->has('postal_code'))<br><span class="error" for="">{{ $errors->first('postal_code') }}</span>@endif
                                </div>
                            </div>
                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">City <span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <input type="text" name="city" class="value-paym" placeholder="City" value="{{old('city')}}">
                            @if ($errors->has('city'))<br><span class="error" for="">{{ $errors->first('city') }}</span>@endif
                                </div>
                            </div>

                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">Current Address <span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <textarea class="value-paym clss_for_mrgn_bttm" name="line1" placeholder="Current Address" rows="2" cols="1"></textarea>

@if ($errors->has('line1'))<br><span class="error" for="">{{ $errors->first('line1') }}</span>@endif
                                </div>
                            </div>

                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">Company name <span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <input type="text" name="company_name" class="value-paym" placeholder="Company name" value="{{old('company_name')}}">
                            @if ($errors->has('company_name'))<br><span class="error" for="">{{ $errors->first('company_name') }}</span>@endif
                                </div>
                            </div>

                             <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">ABN/ACN<span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <input type="number" name="tax_id" class="value-paym" placeholder="ABN/ACN" value="{{old('tax_id')}}">
                            @if ($errors->has('tax_id'))<br><span class="error" for="">{{ $errors->first('tax_id') }}</span>@endif
                                </div>
                            </div>

                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">ID Document Front (e.g Driver's Licence)<span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym" id="fakeDiv">
                                    <input type="file" id="selectedFile" name="document_front" class="value-paym" placeholder="ABC547865" value="{{old('document_front')}}">
                            <input type="text" id="fakeInput" disabled/>
                            <span class="file_msg">JPEG/PNG (Max file size 4 mb)</span>

                            <span onclick="document.getElementById('selectedFile').click();" class="buttonImage" >Browse</span>
                            @if ($errors->has('document_front'))<br><span class="error" for="">{{ $errors->first('document_front') }}</span>@endif
                                </div>
                            </div>
                            <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">ID Document Back (e.g Driver's Licence)<span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym" id="fakeDiv2">
                                    <input type="file" id="selectedFile2"  name="document_back" class="value-paym" placeholder="ABC547865" value="{{old('document_back')}}">
                            <input type="text" id="fakeInput2" disabled/> 
                            <span class="file_msg">JPEG/PNG (Max file size 4 mb)</span>
                            <span onclick="document.getElementById('selectedFile2').click();" class="buttonImage" >Browse</span>
                            @if ($errors->has('document_back'))<br><span class="error" for="">{{ $errors->first('document_back') }}</span>@endif
                                </div>
                            </div>
                              <div class="box-one-paym">
                                <div class="bank-det">
                                    <h3 class="key-paym">Business website<span class="star">*</span></h3>
                                </div>
                                <div class="bank-det value-box-paym">
                                    <input type="text" name="business_website" class="value-paym" placeholder="Business website" value="{{old('business_website')}}">
                            @if ($errors->has('business_website'))<br><span class="error" for="">{{ $errors->first('business_website') }}</span>@endif
                                </div>
                            </div>-->

                            <div class="bank-account-detail" style="display: none;">
                                <div class="box-one-paym" style="width: 90%">
                                    <h4 class="key-paym-heading">Bank Account :</h4>
                                </div>
                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Bank Name <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="bank_name" class="value-paym" placeholder="Bank Name" value="{{old('bank_name')}}">
                                        @if ($errors->has('bank_name'))<br><span class="error" for="">{{ $errors->first('bank_name') }}</span>@endif
                                    </div>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Branch Number/Code <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="branch_name" class="value-paym" placeholder="Branch Name" value="{{old('branch_name')}}">
                                        @if ($errors->has('branch_name'))<br><span class="error" for="">{{ $errors->first('branch_name') }}</span>@endif
                                    </div>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Account Name <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="account_name" class="value-paym" placeholder="Account Name" value="{{old('account_name')}}">
                                        @if ($errors->has('account_name'))<br><span class="error" for="">{{ $errors->first('account_name') }}</span>@endif
                                    </div>
                                </div>


                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Account Number <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="number" name="account_number" class="value-paym" placeholder="Account Number" value="{{old('account_number')}}">
                                        @if ($errors->has('account_number'))<br><span class="error" for="">{{ $errors->first('account_number') }}</span>@endif
                                    </div>
                                </div>

                            <!-- <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">Account Type <span class="star"></span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <input type="number" name="account_type" class="value-paym" placeholder="Account Type" value="{{old('account_type')}}">
                                @if ($errors->has('account_type'))<br><span class="error" for="">{{ $errors->first('account_type') }}</span>@endif
                                    </div>
                                </div> -->
                            </div>
                            <div class="jazzcash-account-detail" style="display: none;">
                                <div class="box-one-paym" style="width: 90%">
                                    <h4 class="key-paym-heading">Jazz Cash Account :</h4>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Account Name <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="jazz_account_name" class="value-paym" placeholder="Account Name" value="{{old('account_name')}}">
                                        @if ($errors->has('jazz_account_name'))<br><span class="error" for="">{{ $errors->first('jazz_account_name') }}</span>@endif
                                    </div>
                                </div>


                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Mobile Number <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="number" name="jazz_account_number" class="value-paym" placeholder="Account Number" value="{{old('account_number')}}">
                                        @if ($errors->has('jazz_account_number'))<br><span class="error" for="">{{ $errors->first('jazz_account_number') }}</span>@endif
                                    </div>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Cnic <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="number" name="cnic" class="value-paym" placeholder="Cnic number" value="{{old('cnic')}}">
                                        @if ($errors->has('cnic'))<br><span class="error" for="">{{ $errors->first('cnic') }}</span>@endif
                                    </div>
                                </div>
                            </div>
                            <div id="save-btn-paym" class="box-one-paym" style="width: 90%; text-align:center;display: none;">
                                <button type="submit" class="btn btn-danger save-btn-paym">Save</button>
                            </div>
                        </form>
                    @else
                        <form action="{{route('update_bank_detaiil',['user_id'=>$patner_id])}}" method="post" enctype="multipart/form-data">
                        <!--<form action="{{route('update_add_account',['user_id'=>$patner_id])}}" method="post" enctype="multipart/form-data">-->
                            {{ csrf_field() }}
                            <div class="bank-det value-box-paym" style="text-align: center; width: 100%;float: left;margin-top: 50px;">
                                <label class="select-box-paym">Select Payment Account</label>
                                <select class="list-paym-hist-page" name="account_type" id="choose_payment_type">
                                    <option class="options" value="">Select One Option</option>
                                    <option class="options" value="1" @if($bank_detail->account_type == 1) selected @endif>Jazz Cash</option>
                                    <option class="options" value="2" @if($bank_detail->account_type == 2) selected @endif>Bank Account</option>
                                </select>
                            </div>
                            <div class="bank-account-detail" @if($bank_detail->account_type == 1) style="display: none;" @endif>
                                <div class="box-one-paym" style="width: 90%">
                                    <h4 class="key-paym-heading">Bank Account :</h4>
                                </div>
                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Bank Name <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="bank_name" class="value-paym" placeholder="Bank Name" value="{{$bank_detail->bank_name}}">
                                        @if ($errors->has('bank_name'))<br><span class="error" for="">{{ $errors->first('bank_name') }}</span>@endif
                                    </div>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Branch Number/Code <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="branch_name" class="value-paym" placeholder="Branch Name" value="{{$bank_detail->branch_name}}">
                                        @if ($errors->has('branch_name'))<br><span class="error" for="">{{ $errors->first('branch_name') }}</span>@endif
                                    </div>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Account Name <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="account_name" class="value-paym" placeholder="Account Name" value="{{$bank_detail->account_name}}">
                                        @if ($errors->has('account_name'))<br><span class="error" for="">{{ $errors->first('account_name') }}</span>@endif
                                    </div>
                                </div>


                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Account Number <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="number" name="account_number" class="value-paym" placeholder="Account Number" value="{{$bank_detail->account_number}}">
                                        @if ($errors->has('account_number'))<br><span class="error" for="">{{ $errors->first('account_number') }}</span>@endif
                                    </div>
                                </div>

                            <!-- <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">Account Type <span class="star"></span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <input type="number" name="account_type" class="value-paym" placeholder="Account Type" value="{{$bank_detail->account_type}}">
                                @if ($errors->has('account_type'))<br><span class="error" for="">{{ $errors->first('account_type') }}</span>@endif
                                    </div>
                                </div> -->
                            <!--                     <input type="hidden" name="account_id" value="{{$bank_detail->account_id}}">
                         <input type="hidden" name="person_id" value="{{$bank_detail->individual_person_id}}">-->
                            <!--                    <div class="box-one-paym" style="width: 90%">
                            <p class="alert_msg">Note: Please check this form carefully before submitting.if this form is submitted incorrectly,you will need to contact us to update the form.</p>
                            <h4 class="key-paym-heading">Company and Director information :</h4>
                        </div>

                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">First Name <span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <input type="text" name="first_name" class="value-paym not-allow" placeholder="First Name" value="{{$bank_detail->individual_person_first_name}}" id="edit_first_name">
                                @if ($errors->has('first_name'))<br><span class="error" for="">{{ $errors->first('first_name') }}</span>@endif

                                    </div>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Last Name <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="last_name" class="value-paym not-allow" placeholder="Last Name" value="{{$bank_detail->individual_person_last_name}}" id="edit_last_name">
                                @if ($errors->has('last_name'))<br><span class="error" for="">{{ $errors->first('last_name') }}</span>@endif

                                    </div>
                                </div>
                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Email <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="email" name="email" class="value-paym" placeholder="Email Id" value="{{$bank_detail->email}}">
                                @if ($errors->has('email'))<br><span class="error" for="">{{ $errors->first('email') }}</span>@endif
                                    </div>
                                </div>
                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Phone Number <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                    <input type="number" name="phone" class="value-paym" placeholder="Phone Number" value="{{substr($bank_detail->company_phone,4)}}">
                            <input type="number" name="phone" class="value-paym" placeholder="Phone Number" value="@if(empty(old('phone'))){{Auth::User()->phone_number}}@endif{{old('phone')}}">
                                @if ($errors->has('phone'))<br><span class="error" for="">{{ $errors->first('phone') }}</span>@endif
                                    </div>
                                </div>
                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">D.O.B <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="date" name="dob" class="value-paym not-allow" placeholder="Date Of Birth" value="{{$bank_detail->d_o_b}}" id="edit_dob">
                                @if ($errors->has('dob'))<br><span class="error" for="">{{ $errors->first('dob') }}</span>@endif
                                    </div>
                                </div>
                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Country <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <select class="value-paym" name="country">
@if(!empty($country))
                                @foreach($country as $value)
                                    <option value="{{$value->iso}}" @if(Auth::User()->country == $value->id) selected @endif>{{$value->nicename}}</option>
                                    @endforeach
                            @endif
                                    </select>
@if ($errors->has('country'))<br><span class="error" for="">{{ $errors->first('country') }}</span>@endif
                                    </div>
                                </div>
                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">State <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <select class="value-paym" name="state">
@if(!empty($state))
                                @foreach($state as $value)
                                    <option value="{{$value->state_code}}" @if($bank_detail->state == $value->state_code) selected @endif>{{$value->state}}</option>
                                    @endforeach
                            @endif
                                    </select>
@if ($errors->has('state'))<br><span class="error" for="">{{ $errors->first('state') }}</span>@endif
                                    </div>
                                </div>
                                 <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Postal Code <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="number" name="postal_code" class="value-paym" placeholder="Postal Code" value="{{$bank_detail->postal_code}}">
                                @if ($errors->has('postal_code'))<br><span class="error" for="">{{ $errors->first('postal_code') }}</span>@endif
                                    </div>
                                </div>
                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">City <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="city" class="value-paym" placeholder="City" value="{{$bank_detail->city}}">
                                @if ($errors->has('city'))<br><span class="error" for="">{{ $errors->first('city') }}</span>@endif
                                    </div>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Current Address <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <textarea class="value-paym clss_for_mrgn_bttm" name="line1" placeholder="Current Address" rows="2" cols="1">{{$bank_detail->current_address}}</textarea>

                                @if ($errors->has('line1'))<br><span class="error" for="">{{ $errors->first('line1') }}</span>@endif
                                    </div>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Company name <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="company_name" class="value-paym" placeholder="Company name" value="{{$bank_detail->company_name}}" id="edit_company_name">
                                @if ($errors->has('company_name'))<br><span class="error" for="">{{ $errors->first('company_name') }}</span>@endif
                                    </div>
                                </div>
                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Role in company <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="role_in_company" class="value-paym" placeholder="Role in company" value="{{$bank_detail->role_in_company}}">
                                @if ($errors->has('role_in_company'))<br><span class="error" for="">{{ $errors->first('role_in_company') }}</span>@endif
                                    </div>
                                </div>
                                 <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">ABN/ACN<span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="number" name="tax_id" class="value-paym" placeholder="ABN/ACN" value="{{$bank_detail->tax_id}}">
                                @if ($errors->has('tax_id'))<br><span class="error" for="">{{ $errors->first('tax_id') }}</span>@endif
                                    </div>
                                </div>
                                  <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Business website<span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="business_website" class="value-paym" placeholder="Business website" value="{{$bank_detail->business_url}}">
                                @if ($errors->has('business_website'))<br><span class="error" for="">{{ $errors->first('business_website') }}</span>@endif
                                    </div>
                                </div>-->


                            <!-- <div class="box-one-paym" style="width: 90%">
                            <h4 class="key-paym-heading">Bank Account :</h4>
                        </div>
                        <div class="box-one-paym">
                            <div class="bank-det">
                                <h3 class="key-paym">Account Holder Name <span class="star">*</span></h3>
                            </div>
                            <div class="bank-det value-box-paym">
                                <input type="text" name="account_holder_name" class="value-paym" placeholder="Account Holder Name" value="{{$bank_detail->account_holder_name}}">
                                @if ($errors->has('account_holder_name'))<br><span class="error" for="">{{ $errors->first('account_holder_name') }}</span>@endif
                                    </div>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Swift/BIC Code <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="number" name="swift_code" class="value-paym" placeholder="Swift Code" value="{{$bank_detail->bsb_number}}">
                                @if ($errors->has('swift_code'))<br><span class="error" for="">{{ $errors->first('swift_code') }}</span>@endif
                                    </div>
                                </div>
                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Account Number <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="number" name="account_number" class="value-paym" placeholder="Account Number" value="{{$bank_detail->account_number}}">
                                @if ($errors->has('account_number'))<br><span class="error" for="">{{ $errors->first('account_number') }}</span>@endif
                                    </div>
                                </div> -->
                            </div>
                            <div class="jazzcash-account-detail" @if($bank_detail->account_type == 2) style="display: none;" @endif>
                                <div class="box-one-paym" style="width: 90%">
                                    <h4 class="key-paym-heading">Jazz Cash Account :</h4>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Account Name <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="text" name="jazz_account_name" class="value-paym" placeholder="Account Name" value="{{$bank_detail->account_name}}">
                                        @if ($errors->has('jazz_account_name'))<br><span class="error" for="">{{ $errors->first('jazz_account_name') }}</span>@endif
                                    </div>
                                </div>


                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Mobile Number <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="number" name="jazz_account_number" class="value-paym" placeholder="Account Number" value="{{$bank_detail->account_number}}">
                                        @if ($errors->has('jazz_account_number'))<br><span class="error" for="">{{ $errors->first('jazz_account_number') }}</span>@endif
                                    </div>
                                </div>

                                <div class="box-one-paym">
                                    <div class="bank-det">
                                        <h3 class="key-paym">Cnic <span class="star">*</span></h3>
                                    </div>
                                    <div class="bank-det value-box-paym">
                                        <input type="number" name="cnic" class="value-paym" placeholder="Cnic number" value="{{$bank_detail->cnic}}">
                                        @if ($errors->has('cnic'))<br><span class="error" for="">{{ $errors->first('cnic') }}</span>@endif
                                    </div>
                                </div>
                            </div>
                            <div id="update_btn" class="box-one-paym" style="width: 90%; text-align:center;">
                                <button type="submit" class="btn btn-danger save-btn-paym">Update</button>
                            </div>
                        </form>

                    @endif
                </div>
            </div>
        </div>

    </section>
    <style>
        .not-allow {
            cursor: not-allowed;
        }
    </style>
    <script>
        $("#edit_first_name").prop("disabled", true);
        $("#edit_last_name").prop("disabled", true);
        // $( "#edit_company_name" ).prop( "disabled", true );
        $("#edit_dob").prop("disabled", true);
    </script>
    <script>
        $(document).ready(function () {
            $('a[data-toggle="pill"]').on('show.bs.tab', function (e) {
                localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = localStorage.getItem('activeTab');
            if (activeTab) {
                $('#myTab a[href="' + activeTab + '"]').tab('show');
            }
        });
    </script>
    <script type="text/javascript">;
        $(document).ready(function () {
            var gym_name = $('#payment_gym_name').val();
            var date = $('#date_filter').val();
            //alert(gym_id);
            if (gym_name != '') {
                get_month_wise_data(gym_name , date);
            }

            $('#payment_gym_name, #date_filter').change(function (event) {
                event.preventDefault();
                $('#total_earn').html('0');
                var gym_name = $('#payment_gym_name').val();
                var date = $('#date_filter').val();
                if (gym_name != '') {
                    get_month_wise_data(gym_name , date);
                }
            });

            /* function get_payment_month(gym_id) {
                 // $("#slider2").html('');
                 $.ajax({
                     url: "{{route('get_payment_month')}}",
                data: {'gym_id': gym_id, '_token': "{{ csrf_token() }}"},
                type: "post",
                success: function (data) {
                    $("#slider2").html(data);
                    $('.owl-carousel').trigger('destroy.owl.carousel');
                    $('.owl-carousel').owlCarousel({
                        loop: false,
                        margin: 10,
                        nav: true,
                        dots: false,
                        responsive: {
                            0: {
                                items: 3
                            },
                            600: {
                                items: 3
                            },
                            1000: {
                                items: 6
                            }
                        }
                    });
                    $("#month_wise_user").html('');
                    let row = `<div class="row-fst-paym">
                           <div class="row">
                            <p class="row-one-details" style="text-align: center;">Please select month to see details</p>
                           </div>
                        </div>`;
                    $("#month_wise_user").append(row);
                }
            });
        }*/
        });
    </script>
    <script type="text/javascript">
        function get_month_wise_data(gym_name , date) {
            $.ajax({
                url: "{{route('month_wise_user')}}",
                data: {'gym_name': gym_name, 'date': date, '_token': "{{ csrf_token() }}"},
                type: "post",
                success: function (data) {
                    console.log(data);
                    if (data.status) {
                        $("#month_wise_user").html('');
                        $("#total_earn").html('');
                        $('#total_earn').html(data.total_earn_amount);
                        //$('#total_earn').html('0');
                        $.each(data.result, (key, val) => {
                            var d = new Date(val.date);
                            var year = d.getFullYear();
                            var month = d.getMonth() + 1;
                            var date = d.getDate();
                            // var finaldate = date + '/' + month + '/' + year;
                            let user_row = `<div class="row-fst-paym">
                                         <div class="row">
                                            <div class="col-4 col-sm-6">
                                               <p class="row-one-details" style="font-size: 20px">${val.trans_id}</p>
                                            </div>
                                            <div class="col-4 col-sm-3">
                                               <p class="row-one-details" style="font-size: 20px">${val.date}</p>
                                            </div>
                                            <div class="col-4 col-sm-3">
                                               <p class="row-one-details earnings-in-dollar" style="font-size: 20px">${val.amount} Rs</p>
                                            </div>
                                         </div>
                                      </div>`;
                            $("#month_wise_user").append(user_row);
                        });
                    } else {
                        $("#month_wise_user").html('');
                        let row = `<div class="row-fst-paym">
                           <div class="row">
                            <p class="row-one-details" style="text-align: center;">Payment not available of this month</p>
                           </div>
                        </div>`;
                        $("#month_wise_user").append(row);
                        //$("#month_wise_user").delay(500).slideUp(300);

                    }
                }
            });
        }
    </script>
    <script>
        $('#selectedFile').change(function () {
            var a = $('#selectedFile').val().toString().split('\\');
            $('#fakeInput').val(a[a.length - 1]);
        });

        $('#selectedFile2').change(function () {
            var a = $('#selectedFile2').val().toString().split('\\');
            $('#fakeInput2').val(a[a.length - 1]);
        });

        $('#selectedFile3').change(function () {
            var a = $('#selectedFile3').val().toString().split('\\');
            $('#fakeInput3').val(a[a.length - 1]);
        });

        $('#selectedFile4').change(function () {
            var a = $('#selectedFile4').val().toString().split('\\');
            $('#fakeInput4').val(a[a.length - 1]);
        });
        // $('.bank-account-detail').hide();
        // $('.jazzcash-account-detail').hide();
        // $('#update_btn').hide();

        $('#choose_payment_type').change(function () {
            var a = $('#choose_payment_type').val();
            if (a == 1) {
                $('.bank-account-detail').hide();
                $('.jazzcash-account-detail').show();
                $('#update_btn').show();

                @if(!empty($bank_detail))
                var bank_detail_account_type = {{$bank_detail->account_type}};
                @else
                var bank_detail_account_type = '';
                @endif
                if (bank_detail_account_type == 2) {
                    $('input[name=jazz_account_number]').val('');
                    $('input[name=jazz_account_name]').val('');
                    $('input[name=account_number]').val("@if(!empty($bank_detail)){{$bank_detail->account_number}}@endif");
                    $('input[name=account_name]').val("@if(!empty($bank_detail)){{$bank_detail->account_name}}@endif");

                }
            } else if (a == 2) {
                @if(!empty($bank_detail))
                var bank_detail_account_type = {{$bank_detail->account_type}};
                @else
                var bank_detail_account_type = '';
                @endif

                if (bank_detail_account_type == 1) {
                    $('input[name=jazz_account_number]').val("@if(!empty($bank_detail)){{$bank_detail->account_number}}@endif");
                    $('input[name=jazz_account_name]').val("@if(!empty($bank_detail)){{$bank_detail->account_name}}@endif");
                    $('input[name=account_number]').val('');
                    $('input[name=account_name]').val('');
                }
                $('.jazzcash-account-detail').hide();
                $('.bank-account-detail').show();
                $('#update_btn').show();
            } else {
                $('.bank-account-detail').hide();
                $('.jazzcash-account-detail').hide();
                $('#update_btn').hide();
            }
            $('#save-btn-paym').show();
        });
    </script>
    @if(!empty($errors->has('')))
        <script type="text/javascript">
            setTimeout(function () {
                $('.error').hide();
            }, 4000);
            $('.booking-details-wrapper').removeClass("fadace");
            document.getElementById('formpopup').classList.add('show');
            $("#loader").hide();
        </script>
    @endif
    @if(Session::has('message'))
        <script type="text/javascript">
            $('.booking-details-wrapper').removeClass("fadace");
            $("#loader").hide();
        </script>
    @endif
    <script>
        document.getElementById("loader").style.display = "none";
        $('.save-btn-paym').click(function () {
            var myVar;
            $('.booking-details-wrapper').addClass("fadace");
            $("#loader").show();
            myVar = setTimeout(showPage, 9000);
        });


        $('.month-color').on('click', function () {
            $('.month-color').removeClass('active');
            $(this).addClass('active');
        });

    </script>
@endsection