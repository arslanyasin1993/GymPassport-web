@extends('web_user_dash.design')
@section('content')
<style type="text/css">
   #checkin_user_page{
   float: right;
   }
   .pagination>li>a, .pagination>li>span {
    padding: 5px 13px;
    }
    li.prev.disabled a {
      border-radius: 5px 0 0 5px;
    }
    li.next a {
    border-radius: 0 5px 5px 0px;
    }
  ul.pagination.bootpag li a {
    color: #757373;
    }
    .pagination{
      margin: 20px 10px;
    }
    #bydate{
        color: #cec1bf;
    margin-left: 12px;
    }
    .show_date_msg{
      color: #cac0b3;
    }
    .width{
        width: 84%;
    max-width: 84%;
    }

</style>
<section class="pay-hist-section-checkins">
  <div class="checkins-div">
    <div class="billing-history-box-checkins">
      <!-- <div class="row">
        <div class="col-6 col-sm-6 col-md-3 col-lg-3">

        </div>
        </div> -->

      <div class="four-buttons-checkins">
        <div class="key-value-div-sub-plan value-sub-plan-checkins list-icon-sub-plan-checkins-select-gym common">
          <select class="list-sub-plan-checkins-select-gym" name="gym_name" id="gym_name">
            <!--  <option value="Master-card" class="master-card cards-sub-plan">Select Gym</option> -->
            @if(!empty($gym_list))
            @foreach($gym_list as $key=>$value)
            <option class="cards-sub-plan" value="{{$value->id}}">{{$value->gym_name}}</option>
            @endforeach
            @endif
            <!-- <option value="card-3" class="cards-sub-plan">B</option>
              <option value="card-4" class="cards-sub-plan">C</option> -->
          </select>
        </div>
        <div class="key-value-div-sub-plan value-sub-plan-checkins manual_date d-none">
            <input type="date" name="start_date" id="start_date" class="list-sub-plan-checkins-select-gym" style="max-width: 300px">
        </div>
          <div class="key-value-div-sub-plan value-sub-plan-checkins manual_date d-none mr-2">
              <input type="date" name="end_date"  id="end_date" value="{{ date('Y-m-d') }}" class="list-sub-plan-checkins-select-gym" style="max-width: 300px">
          </div>
        <div class="key-value-div-sub-plan value-sub-plan-checkins list-icon-sub-plan-checkins-weekly common">
          <select class="list-sub-plan-checkins-weekly" name="gym_type" id="gym_type">
            <option  class="master-card cards-sub-plan" value="{{trans('constants.daily')}}">Daily</option>
            <option  class="cards-sub-plan" value="{{trans('constants.weekly')}}" selected>Weekly</option>
            <option  class="cards-sub-plan" value="{{trans('constants.monthly')}}">Monthly</option>
            <option  class="cards-sub-plan" value="{{trans('constants.three_month')}}">3 Months</option>
            <option  class="cards-sub-plan" value="{{trans('constants.six_month')}}">6 Months</option>
            <option  class="cards-sub-plan" value="{{trans('constants.nine_month')}}">9 Months</option>
            <option  class="cards-sub-plan" value="{{trans('constants.yearly')}}">Yearly</option>
            <option  class="cards-sub-plan" value="manual">Manual Date</option>
<!--            <option  class="cards-sub-plan" value="advanced">Advanced</option>-->
            <!-- <option value="card-4" class="cards-sub-plan">C</option> -->
          </select>
          <div id="bydate"></div>
        </div>
        <div class="ababa common">
          <div class="estimated-payment-para">
              <p class="estimated-stmt-checkins">Revenue : <span class="estimated-value-checkins">Rs <span id="fetch_amount">0</span></span></p>
<!--              <p class="estimated-stmt-checkins">Monthly ∞ : <span class="estimated-value-checkins">($<span id="month_amount">0</span>)</span></p>
              <p class="estimated-stmt-checkins">Visit Passes : <span class="estimated-value-checkins">($<span id="daily_amount">0</span>)</span></p>-->
          
           </div>
<!--            <div class="show_date_msg">Next payment run <span id="payment_date"></span> <br> (For the period <span id="from_date"></span> - <span id="to_date"></span>)</div>-->
<!--        @if(date('d') < '05')
          <div class="show_date_msg">Next payment run 05-{{date('m-Y')}} <br> (For the period {{date('01/m/Y',strtotime('-1 month'))}} - {{date('t/m/Y',strtotime('-1 month'))}})</div>
        @else
          <div class="show_date_msg">Next payment run 05-{{date('m-Y',strtotime('first day of +1 month'))}} <br> (For the period {{date('01/m/Y')}} - {{date('t/m/Y')}})</div>
        @endif-->
             </div>
        <div class="add-gym-box">
          <!--  <button type="submit" class="btn btn-danger add-gym-btn-checkins">Add Gym</button>  -->
        </div>
      </div>
      
    </div>

    <div class="billing-history-box-checkins-scroll" id="check-in-history">
   
    </div>
    <a href="{{route('check_in_export')}}"><button type="submit" class="btn btn-danger fixed-btn-checkins">Generate Report</button></a>
  </div>
  </div>
  <div class="date-popup">
       <div class="modal fade custom_modal_fade_class" role="dialog">
        <div class="modal-dialog custom_dialog_cls">
            <div class="modal-content" style="margin-left: -19px;margin-top: 9rem;">
                <div class="modal-header" style=" border: none;background-color: #383838;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color: #fff;">&times;</span></button>
            </div>
                <div class="modal-body" style="background-color: #383838;">
                <div class="custom_body_div">
                    <div class="row_one"><p class="text_prd">Period From:</p><input type="date" class="form-control class_form" id="period_from"></div>
                    <div class="row_one"><p class="text_prd">Period To:</p><input type="date" class="form-control class_form" id="period_to"></div>
                    <div class="form_button"><button class="btn btn-primary custom_btn_filter">Filter</button></div>
                </div>
            </div>
<!--            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>-->
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function(){
    var gym_id = $('#gym_name').val();
    var gym_type = $('#gym_type').val();

    var user_id = '';
    if(gym_id !='' && gym_type !=''){
        getuser(gym_id , gym_type , null , null);
        gym_earn_amount(gym_id , gym_type , null , null);
        select_gym_wise_user_list(gym_id);
    }

      $('#gym_type').change(function(event) {
           event.preventDefault();
           var gym_type = $('#gym_type').val();
           if(gym_type == 'manual'){
               $('.manual_date').removeClass('d-none');
           }else{
               $('.manual_date').addClass('d-none');
               $('#start_date').val('');
               $('#end_date').val('');

               var gym_id = $('#gym_name').val();
               var gym_type = $('#gym_type').val();
               if(gym_id !='' && gym_type !=''){
                   getuser(gym_id,gym_type);
                   gym_earn_amount(gym_id,gym_type);
               }
               if(gym_id !='' && gym_type =='advanced'){
                   $('.modal').modal('show');
                   //getuser(gym_id,gym_type);
                   //gym_earn_amount(gym_id,gym_type);
               }
           }
      });

      $('#start_date').change(function(event) {
          event.preventDefault();
          var gym_id = $('#gym_name').val();
          var start_date = $('#start_date').val();
          var end_date = $('#end_date').val();
          if(gym_id !='' && start_date !='' && end_date != ''){
              getuser(gym_id , null , start_date , end_date);
              gym_earn_amount(gym_id , null , start_date , end_date);
          }
          if(gym_id !='' && gym_type =='advanced'){
              $('.modal').modal('show');
          }
      });

      $('#end_date').change(function(event) {
          event.preventDefault();
          var gym_id = $('#gym_name').val();
          var start_date = $('#start_date').val();
          var end_date = $('#end_date').val();
          if(gym_id !='' && start_date !='' && end_date != ''){
              getuser(gym_id , null , start_date , end_date);
              gym_earn_amount(gym_id ,null , start_date ,end_date);
          }
          if(gym_id !='' && gym_type =='advanced'){
              $('.modal').modal('show');
              //getuser(gym_id,gym_type);
              //gym_earn_amount(gym_id,gym_type);
          }
      });

      $('#gym_name').change(function(event) {
           event.preventDefault();
           var gym_id = $('#gym_name').val();
           // var gym_type = $('#gym_type').val();
          var start_date = $('#start_date').val();
          var end_date = $('#end_date').val();
           if(gym_id !='' && start_date !='' && end_date != ''){
              getuser(gym_id,start_date , end_date);
              gym_earn_amount(gym_id,start_date,end_date);
          }
      });

      $('#user_name').change(function(event) {
           event.preventDefault();
           var user_id = $('#user_name').val();
           var gym_id = $('#gym_name').val();
           var gym_type = $('#gym_type').val();
           if(gym_id !='' && gym_type !='' && user_id !=''){
              getuser(gym_id,gym_type);
              gym_earn_amount(gym_id,gym_type);
          }
      });

      function getuser(gym_id , gym_type = null , start_date = null , end_date = null){
         if(start_date > end_date){
             alert('Start date cannot be greater then End Date');
             return;
         }

         $.ajax({
            url:"{{route('user_check_in_histroy')}}",
            data: {gym_id , gym_type , start_date , end_date , '_token': "{{ csrf_token() }}"},
            type: "post",
            success: function(data){
              $("#check-in-history").html(data);
            }
         });
      }

      function gym_earn_amount(gym_id , gym_type = null ,  start_date = null , end_date = null){
            if(start_date > end_date){
                alert('Start date cannot be greater then End Date');
                return;
            }

            $.ajax({
                    url:"{{route('gym_earn_amount')}}",
                    data: {gym_id , gym_type , start_date , end_date , '_token': "{{ csrf_token() }}"},
                    type: "post",
                    success: function(data){
                      //console.log(data);
                        if(data.status){
                            $('#fetch_amount').html(data.amount);
                            $('#month_amount').html(data.monthly_amount);
                            $('#daily_amount').html(data.daily_amount);
                            $('#bydate').html(data.date);
                        }else{
                            var amount = '0';
                            $('#fetch_amount').html(amount);
                            $('#month_amount').html('0');
                            $('#daily_amount').html('0');
                            $('#bydate').html(data.date);
                        }
                      
                    }
             });  
        }
        
        function select_gym_wise_user_list(gym_id){
             $.ajax({
                    url:"{{route('select_box_user')}}",
                    data: {'gym_id':gym_id, '_token': "{{ csrf_token() }}"},
                    type: "post",
                    success: function(data){
                       // alert(data);
                       var obj =  jQuery.parseJSON(data);
                      $.each(obj, (key, val) => { 
                          let user_name = `<option class="cards-sub-plan" value="${val.id}">${val.first_name} ${val.last_name}</option>`;
                          $("#user_name").append(user_name);
                    });
                    }
             });
        }
  });
</script>
<script>
  $(document).ready(function() {
      $.ajax({
        url:"{{route('get_payment_period')}}",
        data: {'_token': "{{ csrf_token() }}"},
        type: "post",
        success: function(data){
           // alert(data);
           var value =  jQuery.parseJSON(data);
           console.log(value);
           if(value.status){
               $('#payment_date').html(value.payout_date);
               $('#from_date').html(value.start_date);
               $('#to_date').html(value.to_date);
           }
          
        }
      });
  });
</script>
<script>
  $(document).ready(function() {
      $(".image-pay-hist").click(function() {
          $(".popup-paymentaa-second").toggle();
      });
  });
  
  $(document).ready(function() {
      $(".trigger-navbar").click(function() {
          $(".custom-navbar-pay-hist").slideToggle();
      });
  });
</script>
@endsection