@extends('web.layouts.master')
@section('title' , 'Gym Passport - Privacy Policy')

@section('meta')
    <meta name="title" content="Our Privacy Matters - Read Gym Passport's Privacy Policy">
    <meta name="description" content="At Gym Passport, we take privacy seriously. Learn how we protect your information and ensure a secure partnership by reading our comprehensive privacy policy.">
@endsection
@section('content')
    <!-- GYM-PARTNER-HERO-AREA START -->
    <section class="gym-partner-hero-area blogs-hero">
        <div class="container">
            <div class="gym-partner-hero-content text-center">
                <h1>Gym Passport Privacy Policy</h1>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <section class="terms-area">
            <div class="container">
                <div class="terms-main-area">
                    <div class="policy-content">
                        {!! html_entity_decode($privacy->description) !!}
{{--                        <div class="privacy-content">--}}
{{--                            <P>Thank you for being part of the Gym Passport community. When you join Gym Passport you become part of a national network of Gyms and Facilities coming together to make finding and exercising at Fitness centres simple and fun. Our Terms of Service and this Privacy Policy, contain important information, and will help you have a great experience. Please review them carefully.</P>--}}
{{--                        </div>--}}
{{--                        <h3>Privacy Policy</h3>--}}
{{--                        <h4>Last Updated December 2022</h4>--}}
{{--                        <p>Gym Passport respects each individual’s right to personal privacy. This privacy policy is designed to assist you in understanding how UFP will collect, use and safeguard any personal information you provide. All user information contained within any form you may use or build is governed by our terms of service.</p>--}}
{{--                        <p>The Site is not intended for or directed to persons under the age of 13, and we will not knowingly collect information from such persons. Any person who provides information to us through registration or in any other manner on the Site represents to Gym Passport that they are 13 years of age or older. If we learn that a child under 13 has submitted personal information to us, we will attempt to delete the information as soon as possible.</p>--}}
{{--                        <p>For our platform to provide accurate data, we collect and store information you provide directly to us. We collect information you provide when you create an account, fill out a form, or communicate with us. The types of information we may collect include your name, email address, postal address, phone number, social network access data, photos, and any other information you choose to provide.</p>--}}
{{--                        <p>Some of the information we collect will include log in information such as the type of browser you use, access times, pages viewed and your IP address, device information including computer or mobile platform, model, hardware, operating system, version, unique identifiers and mobile network. So that you may use location features of our platform, we will also collect location data to include your device location through geographic/geolocation mapping services. With your permission we may access contact lists, photos and calendar information from your device.</p>--}}
{{--                        <p>We may collect information about you from other sources through certain features on our platform that you elect to use including social media services or other third party services.</p>--}}
{{--                        <p>We may use the information collected to provide, maintain and improve our platform and/or develop new products and services, personalise our platform, monitor and analyse trends, usage, and activities in connection with our platform, investigate and prevent fraudulent activities, abuse or illegal activities or to protect the rights of property or safety of other users and the public.</p>--}}
{{--                        <p>Information we collect from you may be used to provide advertisements, content or features that match your profile or location or facilitate promotions. Some of the information you provide is shared through our platform according to your member profile and account settings. Some information such as user name may be public, other information may be hidden from public view at your discretion and available to monitor within your account settings.</p>--}}
{{--                        <p>We may use service providers in connection with our platform to assist us in certain functions to maintain our service and functionality. We take measures to ensure that these service providers access, process and store information about you only for the purposes we authorise, and are subject to confidentiality obligations.</p>--}}
{{--                        <p>Our platform may offer social sharing features and other integrated tools which let you share actions you take on our platform with third-party services, and vice versa. Such features let you share information with your friends or the public, depending on the settings you have chosen with the service. The service’s use of the information will be governed by their privacy policies, and we do not control their use of the shared data. For more information about the purpose and scope of data collection and processing in connection with social sharing features, please review the privacy policies of the services that provide these features.</p>--}}
{{--                        <h4>Security</h4>--}}
{{--                        <p>We are committed to doing all things we reasonably can to protect Personal Information from loss, misuse, interference, disclosure, modification, unauthorised access, and destruction. Gym Passport takes reasonable precautions to safeguard the confidentiality of Personal Information. We protect the security of your Personal Information during transmission to us by using Secure Sockets Layer (SSL) software, which encrypts the information you input.</p>--}}
{{--                        <p>Your consent to this privacy policy</p>--}}
{{--                        <p>By using this Site, you agree to this Privacy Policy. We may occasionally update this Privacy Policy. The date of the most recent update of this document will always be displayed at the beginning of this page. If we change our Privacy Policy, we will post those changes on our website. If we decide to use Personal Information in a manner different from that stated at the time it was collected, we will notify users via email or display a notice on our website. We encourage you to periodically review this Privacy Policy to stay informed about how we are protecting the Personal Information we collect.</p>--}}
{{--                        <p>Your continued use of this Site constitutes your agreement to this Privacy Policy and any updates.</p>--}}
{{--                        <p class="last-details">For any further questions or queries on our terms and services please contact us on support@gympassport.pk</p>--}}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
