
<!DOCTYPE html>
<html>
<head>
  <title>signup</title>
 <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
  <meta charset="utf-8">
  <meta name="viewport" content="width=devic e-width, initial-scale=1">
  <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/media.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <header class="head-signup">
    <div class="container-fluid">
      <div class="icon-signup">
        <img src="{{ asset('website_file/images/logo.png')}}" alt="icon" class="img-fluid signup-icon">
      </div>
      </div>
  </header>
  <section class="newpass-left-part">
    <img src="{{ asset('website_file/images/new-pass.png')}}" alt="newpass-page-image" class="img-fluid signup-left-img">
  </section>
  <section class="newpass-right-part">
    @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
      @endif 
    <h5 class="newpass-heading">Change Password</h5>

    <form action="{{route('update_password')}}" method="post">
        {{ csrf_field() }}
      <input type="hidden" name="user_id" value="{{session()->get('user_id')}}">
      <input type="hidden" name="email" value="{{session()->get('email')}}">
    <label class="newpass-labels">Enter New Password</label>
      <div class="pass-box-wrapper">
        <input type="password" name="password" class="newpass-form" placeholder="*********" id="pwd2">
          <p class="show-hide-para-newpass" id="pwdbtn2">Show</p>
          @if ($errors->has('password'))<label class="error" for="">{{ $errors->first('password') }}</label>@endif
            </div>
            <label class="newpass-labels">Confirm New Password</label>
      <div class="pass-box-wrapper">
        <input type="password" name="confirm_password" class="newpass-form" placeholder="*********" id="pwd3">
            <p class="show-hide-para-newpass" id="pwdbtn3">Show</p>
            @if ($errors->has('confirm_password'))<label class="error" for="">{{ $errors->first('confirm_password') }}</label>@endif
            </div>
            <div class="newpass-btn-box">
            <button type="submit" class="btn btn-danger newpass-login-btn">Submit</button>
        </div>
        </form>
  </section>
  <script>
  $(document).ready(function(){
    $('#pwdbtn2').click(function(){
      if($('#pwd2').attr('type') == 'password'){
        $('#pwd2').attr("type","text");
        $(this).text("hide");
      }else {
        $('#pwd2').attr("type","password");
        $(this).text("show");
      }
    })
  });

 $(document).ready(function(){
    $('#pwdbtn3').click(function(){
      if($('#pwd3').attr('type') == 'password'){
        $('#pwd3').attr("type","text");
        $(this).text("hide");
      }else {
        $('#pwd3').attr("type","password");
        $(this).text("show");
      }
    })
  });
</script>
  <script type="text/javascript">
    $('.close').click(function(){
    $('.alert').hide();
});
  </script>
</body>
</html>