@extends('web.layouts.master')
@section('title' , 'Gym Passport - Partner Login')
@section('content')
    <!-- GYM-PARTNER-HERO-AREA START -->
    <section class="gym-partner-hero-area blogs-hero">
        <div class="container">
            <div class="gym-partner-hero-content text-center">
                <h1>Login</h1>
            </div>
        </div>
    </section>
    <div class="container-fluid">
        <!--LOGIN AREA START -->
        <section class="login-area">
            <div class="container">
                <div class="login-main">
                    <div class="row g-5">
                        <div class="col-lg-6">
                            <div class="login-left">
                                <img src="{{ asset('assets/images/login.png') }}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            @if(session()->has('msg'))
                                <div class="d-flex ms-auto me-auto" id="form-alert" style="max-width: 1370px">
                                    <div class="col-12">
                                        <div class="alert {{ session()->get('message') }} d-flex align-items-center justify-content-between" role="alert">
                                            <div class="d-flex align-items-center">
                                                <svg class="bi flex-shrink-0 me-3" width="24" height="24" role="img" aria-label="Success:">
                                                    <use xlink:href="#check-circle-fill"/>
                                                </svg>
                                                <div class="fs-4 lh-1">
                                                    {{ session()->get('msg') }}
                                                </div>
                                            </div>
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="login-right">
                                <h2>Login</h2>
                                <form action="{{ route('signin.submit')}}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="login-input-area">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="login-input">
                                                    <input type="email" id="email" name="email" value="{{ old('email') }}" placeholder="Email Address">
                                                    @if ($errors->has('email'))<p class="error">{{ $errors->first('email') }}</p>@endif
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="login-input login-input2">
                                                    <input type="password" name="password" value="{{ old('password') }}" placeholder="Password">
                                                    @if ($errors->has('password'))<p class="error">{{ $errors->first('password') }}</p>@endif
                                                    <div class="forget-pass text-end">
                                                        <a href="{{route('forgate_password')}}">Forgot Password?</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="remember-me">
                                                    <input type="checkbox" name="checked" value="1" id="remember">
                                                    <label for="remember">Remember Me</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="login-btn">
                                                    <input type="submit" value="Login">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
