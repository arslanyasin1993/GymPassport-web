<div>
    Hi, {{ $name['user_name'] }},<br/>
    Your login details to login to your corporate account on Gym Passport is <br/>
    Username: {{ $name['email'] }} and your password is : {{$name['password']}}.<br/><br/>
    You will be required to upload a photo of yourself after you login.<br/>
    For a guide to see how Gym Passport works or to see which Gyms are available on your platform, please use the below link<br/>
    <a href="https://linktr.ee/gympassport">https://linktr.ee/gympassport</a>
</div>