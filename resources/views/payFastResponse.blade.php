<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PayFast Message</title>
    <style>
        body {
            margin: 0;
            padding: 0;
            height: 100vh;
            display: flex;
            align-items: center;
            justify-content: center;
            font-family: Poppins, sans-serif;
            background-color: #f0f0f0;
        }
        div {
            text-align: center;
        }
    </style>
</head>
<body>
<div>
    <h1>{{ $message }}</h1>
</div>
</body>
</html>
