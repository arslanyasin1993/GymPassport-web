<div class="wrapper">
<style type="text/css">
  .logo_img{
    height: 48px;
  }
</style>
  <header class="main-header">
    <a href="{{route('corporate-home')}}" class="logo">
      <span class="logo-mini"><b></b></span>
      <span class="logo-lg"><b><img src="{{ asset('landing_page/images/Website_Logo-1.png')}}" alt="" class="logo_img"></b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ auth()->user()->user_image && file_exists(public_path(auth()->user()->user_image)) ? asset(auth()->user()->user_image) : asset('css/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                {{-- <span class="hidden-xs">{{{ isset(Auth::user()->first_name) ? Auth::user()->first_name : Auth::user()->email }}}</span> --}}
                @php
                    $corporate_name = \App\User::join('corporate_subscription as c', 'c.corporate_id', '=', 'users.id')->where('c.corporate_id', Auth::user()->id)->pluck("corporate_name")->get(0);
                @endphp
                <span class="hidden-xs">{{{ isset($corporate_name) ? $corporate_name : Auth::user()->email }}}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="{{ auth()->user()->user_image && file_exists(public_path(auth()->user()->user_image)) ? asset(auth()->user()->user_image) : asset('css/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                <p>
                  <small>Member since Nov. {{{ isset(Auth::user()->created_at) ? date('Y',strtotime(Auth::user()->created_at)) : "" }}}</small>
                </p>
              </li>
              <!-- Menu Body -->
<!--              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                 /.row 
              </li>-->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                </div>
                <div class="pull-right">
                    @if(session()->has('last_url'))
                        <a href="{{ route('login_account' , 1) }}" class="btn btn-default btn-flat">Back To Admin</a>
                    @endif
                    <a href="{{ route('signout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Sign out</a>
                    <form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li> -->
            <!--  <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> -->
          <!-- </li> -->
        </ul>
      </div>
    </nav>
  </header>