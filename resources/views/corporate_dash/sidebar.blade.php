@extends('corporate_dash.header')
@if(!isset(Auth::user()->id))
    <script>
        window.location.href = '{{url("login")}}';
    </script>
@endif
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('css/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                @if(isset(Auth::user()->id))
                    @php $corporate_name = \App\User::join('corporate_subscription as c', 'c.corporate_id', '=', 'users.id')->where('c.corporate_id', Auth::user()->id)->pluck("corporate_name")->get(0);
                    @endphp
                    <p>{{ isset($corporate_name) ? Illuminate\Support\Str::limit($corporate_name,20) : Auth::user()->email }}</p>
            @endif
            <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
        </div>
        <!-- search form -->
        <!--  <form action="#" method="get" class="sidebar-form">
           <div class="input-group">
             <input type="text" name="q" class="form-control" placeholder="Search...">
             <span class="input-group-btn">
                   <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                   </button>
                 </span>
           </div>
         </form> -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <li class=" @if(request()->route()->getName()=='corporate-home') active @endif">
                <a href="{{route('corporate-home')}}">
                    <i class="fa fa-tachometer" aria-hidden="true"></i> <span>Home</span>
                </a>
            </li>


            <li class=" @if(request()->route()->getName()=='corporatelist') active @endif">
                <a href="{{route('employee-list')}}">
                    <i class="fa fa-list"></i> <span>Employee Management</span>
                </a>
            </li>
            <li class=" @if(request()->route()->getName()=='employeeCheckInDetail') active @endif">
                <a href="{{route('employeeCheckInDetail')}}">
                    <i class="fa fa-list"></i> <span>Check-In Management</span>
                    <span class="pull-right-container">
                      <!-- <small class="label pull-right bg-green">new</small> -->
                    </span>
                </a>
            </li>
            <li class=" @if(request()->route()->getName()=='changeCorporatePassword') active @endif">
                <a href="{{route('changeCorporatePassword')}}">
                    <i class="fa fa-lock"></i> <span>Edit Profile</span>
                </a>
            </li>


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
