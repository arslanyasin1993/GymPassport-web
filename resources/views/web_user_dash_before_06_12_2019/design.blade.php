<!DOCTYPE html>
<html>
    <head>
        @if(!isset(Auth::user()->id))
        <script>
            window.location.href = '{{url("/")}}';
        </script>
        @endif
    <title>Ultimate Fitness</title>
     <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=devic e-width, initial-scale=1,  user-scalable=no">
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/owl.carousel.min.css')}}">
<!--     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
 -->    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="{{ asset('website_file/js/owl.carousel.min.js')}}"></script>
     <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/media.css')}}">
    <style type="text/css">
            #loader {
                  position: absolute;
                  left: 27%;
                  top: 50%;
                  z-index: 1;
                  width: 150px;
                  height: 150px;
                  margin: -75px 0 0 -75px;
                  border: 16px solid #f3f3f3;
                  border-radius: 50%;
                  border-top: 16px solid #3498db;
                  width: 120px;
                  height: 120px;
                  -webkit-animation: spin 2s linear infinite;
                  animation: spin 2s linear infinite;
                }

                @-webkit-keyframes spin {
                  0% { -webkit-transform: rotate(0deg); }
                  100% { -webkit-transform: rotate(360deg); }
                }

                @keyframes spin {
                  0% { transform: rotate(0deg); }
                  100% { transform: rotate(360deg); }
                }

                /* Add animation to "page content" */
                .animate-bottom {
                  position: relative;
                  -webkit-animation-name: animatebottom;
                  -webkit-animation-duration: 1s;
                  animation-name: animatebottom;
                  animation-duration: 1s
                }

                @-webkit-keyframes animatebottom {
                  from { bottom:-100px; opacity:0 } 
                  to { bottom:0px; opacity:1 }
                }

                @keyframes animatebottom { 
                  from{ bottom:-100px; opacity:0 } 
                  to{ bottom:0; opacity:1 }
                }

                #myDiv {
                  display: none;
                  text-align: center;
                  }
              </style>

</head>
    <body>
        @include('web_user_dash.header')

        @yield('content')

        @include('web_user_dash.footer')
    </body>
</html>