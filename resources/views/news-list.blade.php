@extends('web.layouts.master')
@section('title' , 'Gym Passport - News')
@section('style')
    <style>
        .blog-nav {
            padding-top: 88px;
            margin-right: 15px;
        }

        .blog-nav ul {
            display: flex;
            align-items: center;
            justify-content: flex-end;
        }

        .blog-nav ul li a img {
            display: none;
        }

        .blog-nav ul li a {
            font-size: 17px;
            font-weight: 400;
            color: #B9B9B9;
            transition: 0.2s;
            display: flex;
            align-items: center;
        }

        .blog-nav ul li {
            padding-left: 12px;
        }

        .blog-nav ul li .active img {
            display: block;
            margin-left: 6px;
        }

        .blog-nav ul li .active {
            color: #000000;
        }

        .blog-description-text > * {
            font-family: Poppins !important;
        }
    </style>
@endsection

@section('meta')
    <meta name="title" content="Explore the Benefits of Gym Passport Partnership on Our News">
    <meta name="description" content="Discover the endless opportunities for business growth with our gym passport partnership program. Stay informed with our informative news articles and join today.">
@endsection
@section('content')
    <!-- GYM-PARTNER-HERO-AREA START -->
    <section class="gym-partner-hero-area blogs-hero">
        <div class="container">
            <div class="gym-partner-hero-content text-center">
                <h1>Get Your Latest Gym Passport News</h1>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <section class="blog-area">
            <div class="container">
                <div class="blog-area-main">
                    <div class="row">
                        @foreach($news as $item)
                            <div class="col-12 col-sm-6 col-md-6 col-lg-4 d-flex mb-4">
                                <div class="card flex-fill" style="box-shadow: 0 1px 3px rgba(0,0,0,.1);border-radius: 20px;overflow: hidden">
                                    <div class="blog-img-wrapper" style="min-height: 300px">
                                        <a href="{{ route('news-detail', $item->slug) }}">
                                            <img class="card-img-top" height="300px" style="object-fit: cover" data-src="{{ $item->image ? asset($item->image) : "https://ui-avatars.com/api/?name=".$item->title }}" src="{{ $item->image ? asset($item->image) : "https://ui-avatars.com/api/?name=".$item->title }}" alt="News image">
                                        </a>
                                    </div>
                                    <div class="card-body p-4">
                                        <a href="{{ route('news-detail', $item->slug) }}">
                                            <h5 style="font-size: 34px;color: #bd0027">{{ \Illuminate\Support\Str::limit($item->title , 70)}}</h5>
                                        </a>
                                        <div class="blog-description-text-wrapper py-3 "    style="font-family: Poppins;min-height: 108px">
                                            <div class="blog-description-text d-none">
                                                {!! $item->description  !!}
                                            </div>
                                        </div>
                                        <a href="{{ route('news-detail', $item->slug) }}" class="btn" style="background: #bd0027;color: white;font-size: 18px;padding: 10px 25px">Read More</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    @if ($news->hasPages())
                        <div class="blog-nav">
                            <ul>
                                {{--<li class="{{ $news->onFirstPage() ? "disabled" : '' }}">--}}
                                {{--<a href="{{ $news->previousPageUrl() }}">Previous</a>--}}
                                {{--</li>--}}
                                @if($news->currentPage() > 3)
                                    <li><a href="{{ $news->url(1) }}">1<span><img data-src="{{ asset('assets/images/nav-icon-1.svg') }}" alt="" class="img-fluid lazyload"></span></a></li>
                                @endif
                                @foreach(range(1, $news->lastPage()) as $i)
                                    @if($i >= $news->currentPage() - 2 && $i <= $news->currentPage() + 2)
                                        @if ($i == $news->currentPage())
                                            <li><a href="{{ $news->url($i) }}" class="active">{{ $i }}<span><img data-src="{{ asset('assets/images/nav-icon-1.svg') }}" alt="" class="img-fluid lazyload"></span></a></li>
                                        @else
                                            <li><a href="{{ $news->url($i) }}">{{ $i }}<span><img data-src="{{ asset('assets/images/nav-icon-1.svg') }}" alt="" class="img-fluid lazyload"></span></a></li>
                                        @endif
                                    @endif
                                @endforeach
                                @if($news->currentPage() < $news->lastPage() - 3)
                                    <li><a href="{{ $news->url($news->lastPage()) }}">{{$news->lastPage()}}<span><img data-src="{{ asset('assets/images/nav-icon-1.svg') }}" alt="" class="img-fluid lazyload"></span></a></li>
                                @endif
                                {{--<li class="{{ !$news->hasMorePages() ? "disabled" : '' }}">--}}
                                {{--<a href="{{ $news->nextPageUrl() }}">Next</a>--}}
                                {{--</li>--}}
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </section>

        <!-- BEST FITNESS AREA START -->
        <section class="best-fitness-area best-fitness-area2">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid lazyload"> <span>BEST FITNESS CLUBS</span> <img data-src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid lazyload"></h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/gym-partners-icon.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2>our <span>gym partners</span> across Pakistan</h2>
                </div>
                <div class="main-content5">
                    <div id="owl-csel5" class="owl-carousel owl-theme">
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                    </div>
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="custom-nav owl-nav"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ACCESS AREA START -->
        <section class="access-area blog-access-area">
            <div class="container">
                <div class="access-area-main">
                    <div class="row">
                        <div class="col-lg-5 col-md-5">
                            <div class="access-left">
                                <h3>Access fitness</h3>
                                <h2>WITH OUR <span>MOBILE APPLICATION</span></h2>
                                <p>Download the Gym Passport app and <a href="#">get
                                        started</a> to your ultimate access to fitness!
                                </p>
                                <div class="acess-img">
                                    <a href="https://play.google.com/store/apps/details?id=com.ripenapps.gympassport&hl=en&gl=US"> <img data-src="{{ asset('assets/images/google-store-1.png') }}" alt="" class="img-fluid lazyload"></a>
                                    <a href="https://apps.apple.com/pk/app/gym-passport/id1534995333"><img data-src="{{ asset('assets/images/app-store-1.png') }}" alt="" class="img-fluid lazyload"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7">
                            <div class="access-right">
                                <div class="access-right-img">
                                    <img data-src="{{ asset('assets/images/access-3.png') }}" alt="" class="img-fluid lazyload">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script>
        $("#owl-csel5").owlCarousel({
            items: 4,
            autoplay: true,
            autoplayTimeout: 3000,
            startPosition: 0,
            rtl: false,
            loop: true,
            margin: 15,
            // groupCells: true,
            // groupCells: 6,
            dots: true,
            nav: true,
            navText: [
                '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ],
            navContainer: '.main-content5 .custom-nav',
            responsive: {
                0: {
                    items: 3,
                },
                767: {
                    items: 3,
                },
                768: {
                    items: 6,
                },
                992: {
                    items: 6,
                },
                1200: {
                    items: 6,
                }
            }

        });
    </script>
    <script>
        function truncateText(selector, maxLength) {
            var element = document.querySelectorAll(selector);
            $.each(element, function (i) {

                truncated = this.innerHTML;

                if (truncated.length > maxLength) {
                    truncated = truncated.substr(0, maxLength) + '...';
                }

                this.innerHTML = truncated;
            });
            $('.blog-description-text').removeClass('d-none')
        }

        $(document).ready(function () {
            truncateText('.blog-description-text', 200);
        });


    </script>



@endsection
