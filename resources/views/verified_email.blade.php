<!DOCTYPE html>
<html lang="en-US">
<head>
    <!--META-SETUP-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- TITLE -->
    <title>@yield('title')</title>

    <!-- FAV-ICON -->
    <link rel="icon" href="{{ asset('landing_page/images/favicon32x32.png')}}"/>

    <!-- Flickity CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/flickity.css') }}" />

    <!-- BOOTSTRAP-CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}"/>

    <!-- SOCIAL-ICON -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>

    <!-- MIN-STYLESHEET -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <div class="card mt-5">
                    <div class="card-body">
                        <p class="h4 text-center">{{ $message }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>