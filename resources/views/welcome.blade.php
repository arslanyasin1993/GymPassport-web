@extends('web.layouts.master')
@section('title' , 'Gym Passport')
    @section('style')
        <style>
            main .two .three{
                width: 15%;
                cursor: pointer;
                position: relative;
            }
            main .one{
                width: 65%;
                cursor: pointer;
                position: relative;
                margin-top: 0px;
            }
            .carousel-indicators{
                width: 65%;
            }
        </style>
    @endsection

@section('meta')
    <meta name="title" content="Unlimited Gym Access Across Pakistan with One Membership - Gym passport">
    <meta name="description" content="Get fit and achieve your fitness goals with our all-inclusive gym membership. Enjoy access to over 309 top-notch gyms across Pakistan. Sign up now">
@endsection
@section('content')
    <div class="container-fluid">
        <!-- HERO AREA START -->
        <section class="hero-area">
            <div class="row align-items-end g-0">
                <div class="col-lg-5 col-md-5">
                    <div class="hero-left-content">
                        <span></span>
                        <h1>one Membership and access <br>
                            over <span> <a href="https://linktr.ee/gympassport" style="color: rgb(189, 36, 38);">{{ $active_gym }} Gyms</a></span> across<br>
                            Pakistan</h1>
                        <p>Pakistan's Largest Gym Network.<br><br>

                            Access to the best gyms across<br>
                            lahore, karachi, islamabad and 11 other cities.
                        </p>
                        <div class="app-store-img">
                            <a href="https://play.google.com/store/apps/details?id=com.ripenapps.gympassport">
                                <img src="{{ asset('assets/images/google-store-1.png') }}" alt="" class="img-fluid">
                            </a>
                            <a href="https://apps.apple.com/au/app/gym-passport/id1534995333">
                                <img src="{{ asset('assets/images/app-store-1.png') }}" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7">
                    <!-- <div class="hero-slider-main">
                        <div class="main-carousel">
                            <div class="carousel-cell">
                                <div class="carousel-img">
                                    <img src="images/hero-slider-2.jpeg" alt="" class="img-fluid">
                                </div>
                                <div class="hero-slider-title">
                                    <h2>fitness trailes</h2>
                                </div>
                            </div>
                            <div class="carousel-cell">
                                <div class="carousel-img">
                                    <img src="images/hero-slider-1.png" alt="" class="img-fluid">
                                </div>
                            </div>
                            <div class="carousel-cell">
                                <div class="carousel-img">
                                    <img src="images/hero-slider-2.jpeg" alt="" class="img-fluid">
                                </div>
                                <div class="hero-slider-title">
                                    <h2>fitness trailes</h2>
                                </div>
                            </div>
                            <div class="carousel-cell">
                                <div class="carousel-img">
                                    <img src="images/gym-2.png" alt="" class="img-fluid">
                                </div>
                                <div class="hero-slider-title">
                                    <h2>top gyms with best trainers</h2>
                                </div>
                            </div>
                            <div class="carousel-cell">
                                <div class="carousel-img">
                                    <img src="images/hero-slider-1.png" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="hero-slider-main">
                        <section class="main-sldarea">
                            <main>
                                <div class="one a carousel slide" id="carousel" data-bs-ride="carousel">
                                    <div class="carousel-indicators">
                                        <button type="button" data-bs-target="#carousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                        <button type="button" data-bs-target="#carousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                        <button type="button" data-bs-target="#carousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                        <button type="button" data-bs-target="#carousel" data-bs-slide-to="3" aria-label="Slide 4"></button>
                                        <button type="button" data-bs-target="#carousel" data-bs-slide-to="4" aria-label="Slide 5"></button>
                                    </div>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img class="lazyload" data-src="{{ asset('assets/images/hero-slider-1.png') }}" alt="">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="lazyload" data-src="{{ asset('assets/images/slider_gym8.jpg') }}" alt="">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="lazyload" data-src="{{ asset('assets/images/slider_gym5.jpg') }}" alt="">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="lazyload" data-src="{{ asset('assets/images/slider_gym6.jpg') }}" alt="">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="lazyload" data-src="{{ asset('assets/images/slider_gym7.jpg') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="two">
                                    <img class="lazyload" data-src="{{ asset('assets/images/hero-slider-2.jpeg') }}" alt="">
                                    <h2>fitness trailes</h2>
                                </div>
                                <div class="three">
                                    <img class="lazyload" data-src="{{ asset('assets/images/slider-3.png') }}" alt="">
                                    <h2>top gyms with best trainers</h2>
                                </div>
                            </main>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <!-- GYM PASSPORT AREA START -->
        <section class="gym-passport-area">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid">  <span>GYM PASSPORT</span> <img src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid"> </h3>
                    <div class="title-img">
                        <img src="{{ asset('assets/images/title-main.svg') }}" alt="" class="img-fluid">
                    </div>
                    <h2>Start your <span>fitness</span> journey</h2>
                </div>
                <div class="gym-main">
                    <div class="fitness-journey-main">
                        <div class="fitness-journey-inner text-center">
                            <h2>Start your fitness journey with
                                The <span>GYM PASSPORT</span>
                            </h2>
                            <h3>Workout wherever, whenever!</h3>
                            <p>Gym Passport is Pakistan's Largest Premium
                                Gym Network</p>
                            <a href="https://play.google.com/store/apps/details?id=com.ripenapps.gympassport">Download Now</a>
                        </div>
                    </div>
                    <div class="main-content2">
                        <div id="owl-csel2" class="owl-carousel owl-theme">
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-1.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-2.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-3.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-4.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-1.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-2.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-3.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-4.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-1.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-2.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-3.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-4.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-1.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-2.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-3.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-4.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-1.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-2.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-3.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-4.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-1.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-2.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-3.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-4.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-1.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-2.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-3.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-4.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-1.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-2.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-3.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="gym-main-img">
                                <img data-src="{{ asset('assets/images/gym-4.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                        </div>
                        <div class="owl-theme">
                            <div class="owl-controls">
                                <div class="custom-nav owl-nav"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- BEST FITNESS AREA START -->
        <section class="best-fitness-area">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid">  <span>BEST FITNESS CLUBS</span> <img src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid"> </h3>
                    <div class="title-img">
                        <img src="{{ asset('assets/images/gym-partners-icon.svg') }}" alt="" class="img-fluid">
                    </div>
                    <h2>Some of our <span>gym partners</span> across Pakistan</h2>
                </div>
                <div class="main-content">
                    <div id="owl-csel1" class="owl-carousel owl-theme">
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-7.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                        <div class="best-fitness-img">
                            <img data-src="{{ asset('assets/images/gym-partner-8.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                    </div>
                    <div class="owl-theme">
                        <div class="owl-controls">
                            <div class="custom-nav owl-nav"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- PAYMENT AREA START -->
        <section class="payment-area">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid">  <span>PAYMENT</span> <img src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid"> </h3>
                    <div class="title-img">
                        <img src="{{ asset('assets/images/payment-title-bg.svg') }}" alt="" class="img-fluid">
                    </div>
                    <h2><span>PAYMENTS</span>  WE ACCEPT</h2>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-4">
                        <div class="payment-img">
                            <img data-src="{{ asset('assets/images/payment-1.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                    </div>
                    <div class="col-lg-2 col-4">
                        <div class="payment-img">
                            <img data-src="{{ asset('assets/images/payment-2.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                    </div>
                    <div class="col-lg-2 col-4">
                        <div class="payment-img">
                            <img data-src="{{ asset('assets/images/payment-3.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                    </div>
                    <div class="col-lg-2 col-4">
                        <div class="payment-img">
                            <img data-src="{{ asset('assets/images/payment-4.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                    </div>
                    <div class="col-lg-2 col-4">
                        <div class="payment-img">
                            <img data-src="{{ asset('assets/images/payment-5.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                    </div>
                    <div class="col-lg-2 col-4">
                        <div class="payment-img">
                            <img data-src="{{ asset('assets/images/payment-6.png') }}" alt="" class="img-fluid lazyload">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- HOWIT WORKS AREA START -->
        <section class="howit-works">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid">  <span>How It Works</span> <img src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid"> </h3>
                    <div class="title-img">
                        <img src="{{ asset('assets/images/howit-works-title.svg') }}" alt="" class="img-fluid">
                    </div>
                    <h2>How It <span>Works</span> </h2>
                </div>
                <div class="row workstp">
                    <div class="col-lg-4 col-md-6 actvtwrk">
                        <div class="howitworks-main">
                            <div class="howitwork-left">
                                <h2>01</h2>
                            </div>
                            <div class="howitwork-right">
                                <h3>First Download our app and sign
                                    up to get started!</h3>
                                <h3>your gym passport lives here!</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="howitworks-main howitworks-main2">
                            <div class="howitwork-left">
                                <h2>02</h2>
                            </div>
                            <div class="howitwork-right">
                                <h3>Purchase our Membership pass from the App</h3>
                                <h3>Our pass is a monthly subscription pass. You can cancel at anytime!</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="howitworks-main howitworks-main2 howitworks-main3">
                            <div class="howitwork-left">
                                <h2>03</h2>
                            </div>
                            <div class="howitwork-right">
                                <h3>Visit any fitness centre!</h3>
                                <h3>We have partnered with centers across Pakistan, so the last thing that will come between you and your fitness goals,will be an excuse!
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- DEMO CLIP AREA START -->
        <section class="demo-clip-area">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img src="{{ asset('assets/images/left-icon-2.svg') }}" alt="" class="img-fluid">  <span>Demo Clip</span> <img src="{{ asset('assets/images/right-icon-2.svg') }}" alt="" class="img-fluid"> </h3>
                    <div class="title-img">
                        <img src="{{ asset('assets/images/demo-clip-bg.svg') }}" alt="" class="img-fluid">
                    </div>
                    <h2>dEMO cLIP <span>aBOUT US</span> </h2>
                </div>
                <div class="demo-vedio-main">
                    <div class="demo-vedio">
                        <iframe src="https://www.youtube.com/embed/I2SFgjz1RAU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>

        <!-- CHECK OUT AREA START -->
        <section class="checkout-area">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid">  <span>Check out</span> <img data-src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid lazyload"> </h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/our-membership-plan.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2> OUR MEMBERSHIP PLAN </h2>
                </div>
                <div class="checkout-main-wrapper">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="checkout-img">
                                <img data-src="{{ asset('assets/images/checkout-2.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="checkout-content-main">
                                <div class="checkout-icon text-center">
                                    <a href="">
                                        <img data-src="{{ asset('assets/images/check-icon.svg') }}" alt="" class="img-fluid lazyload">
                                    </a>
                                </div>
                                <div class="checkout-inner-content checkout-inner-content2 text-center">
                                    <h2>Gym Passport-Standard</h2>
                                    <ul>
                                        <li><span><img data-src="{{asset('assets/images/check-mark.svg')}}" alt="" class="img-fluid lazyload"></span> number of gyms - {{ $silver_gym_count }} Gyms</li>
                                        <li><span><img data-src="{{asset('assets/images/check-mark.svg')}}" alt="" class="img-fluid lazyload"></span> check-ins- 20 visits a Month</li>
                                        <li><span><img data-src="{{asset('assets/images/check-mark.svg')}}" alt="" class="img-fluid lazyload"></span> price- rs {{ $standard_price }}/ month</li>
                                    </ul>
                                    <a href="https://linktr.ee/gympassport"><h3>no registration fees! you pay<br> nothing to gyms.</h3></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="checkout-img">
                                <img data-src="{{ asset('assets/images/checkout-3.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                            <div class="checkout-content-main">
                                <div class="checkout-icon text-center">
                                    <a href="">
                                        <img data-src="{{ asset('assets/images/check-icon.svg') }}" alt="" class="img-fluid lazyload">
                                    </a>
                                </div>
                                <div class="checkout-inner-content text-center">
                                    <h2>Gym Passport-Pro</h2>
                                    <ul>
                                        <li><span><img data-src="{{ asset('assets/images/check-mark.svg') }}" alt="" class="img-fluid lazyload"></span> number of gyms - {{ $active_gym }} Gyms</li>
                                        <li><span><img data-src="{{ asset('assets/images/check-mark.svg') }}" alt="" class="img-fluid lazyload"></span> check-ins- 20 visits a Month</li>
                                        <li><span><img data-src="{{ asset('assets/images/check-mark.svg') }}" alt="" class="img-fluid lazyload"></span> price- rs {{ $pro_price }}/ month</li>
                                    </ul>
                                    <a href="https://linktr.ee/gympassport"><h3>no registration fees! you pay<br> nothing to gyms.</h3></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ARE YOU AREA START -->
        <section class="are-you-area">
            <div class="container">
                <div class="gym-passport-title text-center">
                    <h3><img data-src="{{ asset('assets/images/left-icon-1.svg') }}" alt="" class="img-fluid lazyload">  <span>ARE YOU A</span> <img data-src="{{ asset('assets/images/right-icon-1.svg') }}" alt="" class="img-fluid lazyload"> </h3>
                    <div class="title-img">
                        <img data-src="{{ asset('assets/images/are-you-title-bg.svg') }}" alt="" class="img-fluid lazyload">
                    </div>
                    <h2>Are you a Gym owner?</h2>
                </div>
                <div class="areyou-main">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="areyou-left">
                                <img data-src="{{ asset('assets/images/are-you-main.png') }}" alt="" class="img-fluid lazyload">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="areyou-right">
                                <h3>ARE YOU A</h3>
                                <h2> <span>ARE YOU A GYM OWNER?</span> Want to partner with us?</h2>

                                <span class="span"></span>

                                <p>Expand your business and add a second revenue   <br>
                                    stream with the ULTIMATE PARTNERSHIP.<br>
                                    <span> Our partnership is absolutely free</span>
                                </p>
                                <a href="{{ route('becomepatner') }}">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- KEEPIN TOUCH AREA START -->
        {{--<section class="keepin-touch-area">--}}
        {{--<div class="container">--}}
        {{--<div class="keepin-main text-center">--}}
        {{--<h3>kEEP IN TOUCH</h3>--}}
        {{--<span class="span"></span>--}}
        {{--<h2>for further <span>inquires</span> please subscribe us!</h2>--}}
        {{--<form action="">--}}
        {{--<div class="email-input">--}}
        {{--<input type="email" placeholder="eNTER YOUR EMAIL ADDRESS">--}}
        {{--<div class="sub-btn">--}}
        {{--<a href="#">sUBSCRIBE</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</form>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</section>--}}

        <!-- ACCESS AREA START -->
        <section class="access-area">
            <div class="container">
                <div class="access-area-main">
                    <div class="row">
                        <div class="col-lg-5 col-md-5">
                            <div class="access-left">
                                <h3>Access fitness</h3>
                                <h2>WITH OUR <span>MOBILE APPLICATION</span></h2>
                                <p>Download the Gym Passport app and get <br>
                                        started to your ultimate access to fitness!
                                </p>
                                <div class="acess-img">
                                    <a href="https://play.google.com/store/apps/details?id=com.ripenapps.gympassport&hl=en&gl=US"> <img data-src="{{ asset('assets/images/google-store-1.png') }}" alt="" class="img-fluid lazyload"></a>
                                    <a href="https://apps.apple.com/pk/app/gym-passport/id1534995333"><img data-src="{{ asset('assets/images/app-store-1.png') }}" alt="" class="img-fluid lazyload"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7">
                            <div class="access-right">
                                <div class="access-right-img">
                                    <img data-src="{{ asset('assets/images/access-main.png') }}" alt="" class="img-fluid lazyload">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
