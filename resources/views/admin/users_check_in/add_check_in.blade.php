@extends('admin_dash.design')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Check In Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class=""><a href="{{route('userCheckInDetail')}}">User-Check-In-List</a></li>
                <li class="active">add-user-check-in</li>
            </ol>
        </section>
        <style type="text/css">
            .time {
                width: 99%;
                margin-left: 1px !important;
            }

            .star {
                color: red;
            }

            .help-block {
                color: red;
                border-color: red;
                line-height: 0px;
            }

            .help-block-text {
                color: red;
                border-color: red;
            }

            .select2-container .select2-selection--single {
                height: 35px !important;
            }

            .location {
                margin-top: 10px;
            }

            .note {
                color: red;
            }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                background-color: #3c8dbc !important;
                border: 1px solid #3c8dbc !important;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif


                    <div class="box">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="{{route('add_check_in')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}


                                <div class="box-body">


                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">User<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2 @if($errors->has('user_id')) help-block @endif" onchange="getUserData(this.value)" id="user_id" name="user_id" required>
                                                @if(!empty($users))
                                                    <option value=""> User</option>
                                                    @foreach($users as $key=>$value)
                                                        <option value="{{$value->id}}">{{$value->first_name. ' '.$value->last_name.' ('.$value->email.')'}}</option>
                                                    @endforeach
                                                @else
                                                    <option value=""> Users Unavailable</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('user_id'))
                                                <strong class="help-block"> {{ $errors->first('user_id') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Subscription<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            <input class="form-control @if($errors->has('subscription_plan')) help-block @endif" id="subscription_plan" name="subscription_plan" readonly/>
                                            @if ($errors->has('subscription_plan'))
                                                <strong class="help-block"> {{ $errors->first('subscription_plan') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Gym<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2 @if($errors->has('gym_id')) help-block @endif" id="gym_id" name="gym_id">
                                                <option value="">Gym</option>
                                            </select>
                                            @if ($errors->has('gym_id'))
                                                <strong class="help-block"> {{ $errors->first('gym_id') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="check_in_date" class="col-sm-2 control-label">Check In Date</label>
                                        <div class="col-sm-10">
                                            <input type="date" class="form-control @if($errors->has('check_in_date')) help-block @endif" name="check_in_date" id="check_in_date" max="{{date('Y-m-d')}}" value="{{date('Y-m-d')}}"/>
                                            @if ($errors->has('check_in_date'))
                                                <strong class="help-block"> {{ $errors->first('check_in_date') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                    <div class="col-sm-offset-1 col-sm-4"></div>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>

                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script type="text/javascript">


        function getUserData(v) {
            if (!v) {
                return false;
            }

            var url = "{{route('get_user_subscription')}}" + '/' + v;
            data = {user_id: v};
            $.ajax({
                url: url,
                data: data,
                success: function (response) {

                    $("#subscription_plan").val('');
                    if (response.userSubscription)
                        $("#subscription_plan").val(response.userSubscription.plan_cat_name);
                    html = '';

                    for (i = 0; i < response.userGyms.length; i++) {
                        html += "<option value='" + response.userGyms[i].gym_id + "'>" + response.userGyms[i].gym_name + "</option>";
                    }
                    $("#gym_id").html(html);
                }, error: function (err) {

                }

            });

        }

    </script>
@endsection
