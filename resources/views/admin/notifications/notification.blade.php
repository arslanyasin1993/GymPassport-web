@extends('admin_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Send Notification
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class=""><a href="{{route('coupon_list')}}">Notifications</a></li>
            </ol>
        </section>
        <style type="text/css">
            input[type=radio] {
                height: 19px;
                width: 100%;
            }
            .rediobox{
                padding: 0px 0px 0px 12px;
            }
            .time{
                width: 99%;
                margin-left: 1px !important;
            }
            .star{
                color: red;
            }
            .help-block{
                color: red;
                border-color: red;
                line-height: 0px;
            }
            .help-block-text{
                color: red;
                border-color: red;
            }
            .select2-container .select2-selection--single {
                height: 35px !important;
            }
            .location{
                margin-top: 10px;
            }
            .note{
                color: red;
            }
            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                background-color: #3c8dbc !important;
                border: 1px solid #3c8dbc !important;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="{{route('send_notification')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Notification Type<span class="star">*</span></label>
                                        <div class="col-sm-9">
                                            <select class="form-control select2 help-block" name="types"  data-placeholder="Select Notification Type">

                                                    <option value="1" selected>All Users</option>

                                            </select>
                                            @if ($errors->has('types'))
                                                <strong class="help-block"> {{ $errors->first('types') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Title<span class="star">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control @if($errors->has('title')) help-block @endif" id="title" name="title" placeholder="Enter notification title">
                                            @if ($errors->has('title'))
                                                <strong class="help-block"> {{ $errors->first('title') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Message<span class="star">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control @if($errors->has('message')) help-block @endif" id="message" name="message" placeholder="Enter Notification Message">
                                            @if ($errors->has('message'))
                                                <strong class="help-block"> {{ $errors->first('message') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                </div>


                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                    <div class="col-sm-offset-1 col-sm-4"></div>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('#openinput').click(function(){
                $('#showcoupon').css("display", "block");
            });

            $('#forever').click(function(){
                $('#showcoupon').css("display", "none");
            });
        });
    </script>
@endsection
