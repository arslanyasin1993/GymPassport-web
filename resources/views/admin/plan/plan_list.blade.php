@extends('admin_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Plan List
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Gym-List</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-2" style="    margin-bottom: 5px;">
                    <a href="{{route('add_plan')}}">
                        <button type="button" class="btn btn-block btn-info"><i class="fa fa-edit"></i> Add Plan</button>
                    </a>
                </div>
                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active" id="Request">
                                <div class="box-header with-border">

                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="plan_list" class="table table-bordered table-striped w-100">
                                            <thead>
                                            <tr>
                                                <th>Plan Name</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Created At</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($plan as $p)
                                                <tr>
                                                    <td>{{$p->plan_cat_name}}</td>
                                                    <td>{{$p->description}}</td>
                                                    <td>
                                                        @if($p->status == 1)
                                                            <a href="javascript:void(0)" data-href='{{ route('plan_list.status', ['id' => $p->id, 'status' => 0]) }}' class='btn {{ $p->status == 1 ? 'btn-success' : 'btn-danger' }} btn-xs change_plan_status' id='active'>{{ $p->status == 1 ? 'Active' : 'InActive' }}</a>
                                                        @else
                                                            <a href="javascript:void(0)" data-href='{{ route('plan_list.status', ['id' => $p->id, 'status' => 1]) }}' class='btn {{ $p->status == 1 ? 'btn-success' : 'btn-danger' }} btn-xs change_plan_status' id='active'>{{ $p->status == 1 ? 'Active' : 'InActive' }}</a>
                                                        @endif
                                                    </td>
                                                    <td>{{$p->created_at}}</td>
                                                    <td>
                                                        <a href='{{route('delete_plan', ['id' => $p->id])}}' class='btn btn-danger btn-xs' id='inactive' onclick='return confirm("Are you sure to delete this coupon")'>Delete</a>
                                                        <a href='{{route('edit_plan', ['id' => $p->id])}}' class='btn btn-info btn-xs' id='inactive'>Edit</a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                        localStorage.setItem('activeTab', $(e.target).attr('href'));
                    });
                    var activeTab = localStorage.getItem('activeTab');
                    if (activeTab) {
                        $('#myTab a[href="' + activeTab + '"]').tab('show');
                    }
                });

                $(document).on('click' , '.change_plan_status' , function (){
                    var url = $(this).attr('data-href');
                    if (confirm("Are you sure?")){
                        $.ajax({
                            url: url,
                            type: 'GET',
                            success: function (response){
                                if(response){
                                    toastr.success(response.message);
                                    setTimeout(function(){
                                        window.location.reload();
                                    }, 500);
                                }else{
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });
            </script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#plan_list').DataTable({
                        "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000 , 5000]],
                    });
                    {{--var dataTable_first = $('#plan_list').DataTable({--}}
                    {{-- 'processing': true,--}}
                    {{-- 'serverSide': true,--}}
                    {{--// 'searching': false, // Remove default Search Control--}}
                    {{--  'ajax': {--}}
                    {{--     'url':"{{route('ajax_plan_list')}}",--}}
                    {{--     'data': function(data){--}}
                    {{--     //    var enddate = $('#enddate_first').val();--}}
                    {{--     //    var startdate = $('#startdate_first').val();--}}
                    {{--     //    //var searchdata = $('#searchdata').val();--}}
                    {{--     //--}}
                    {{--     //    // Append to data--}}
                    {{--     //    data.startdate = startdate;--}}
                    {{--     //    data.enddate = enddate;--}}
                    {{--     //    //data.searchdata = searchdata;--}}
                    {{--     // }--}}
                    {{--  },--}}
                    {{--  'columns': [--}}
                    {{--              { data: 'plan_cat_name' },--}}
                    {{--              { data: 'description' },--}}
                    {{--              { data: 'created_at' },--}}
                    {{--              { data: 'plan_cat_name' },--}}
                    {{--           ]--}}
                    // });
                    //
                    // $('#searchdata').keyup(function(){
                    //   dataTable_first.draw();
                    // });
                    //
                    //  $('#submit_first').click(function(){
                    //   dataTable_first.draw();
                    //  });
                });
            </script>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

