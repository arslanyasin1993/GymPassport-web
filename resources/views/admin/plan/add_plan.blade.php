@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
             {{ isset($plan)?"Edit":"Add" }} Plan
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class=""><a href="{{route('plan_list')}}">Plan-List</a></li>
            <li class="active">Add-Plan</li>
        </ol>
    </section>
    <style type="text/css">
        input[type=radio] {
    height: 19px;
    width: 100%;
}
        .rediobox{
            padding: 0px 0px 0px 12px;
        }
        .time{
            width: 99%;
            margin-left: 1px !important;
        }
        .star{
            color: red;
        }
        .help-block{
            color: red;
            border-color: red;
            line-height: 0px;
        }
        .help-block-text{
            color: red;
            border-color: red;
        }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }
        .location{
            margin-top: 10px;
        }
        .note{
            color: red;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #3c8dbc !important;
            border: 1px solid #3c8dbc !important;
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">     
                @if(Session::has('message'))
                <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {!! session('msg') !!}
                </div>
                @endif 
                <div class="box">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form action="{{isset($plan)?route('update_plan',['id' => $plan->id]):route('store_plan')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="user_type" value="2">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Plan Name<span class="star">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" required class="form-control @if($errors->has('plan_cat_name')) help-block @endif" id="plan_cat_name" name="plan_cat_name" value="{{ isset($plan)?$plan->plan_cat_name:"" }}" placeholder="Enter Plan Name">
                                        @if ($errors->has('plan_cat_name'))
                                            <strong class="help-block"> {{ $errors->first('plan_cat_name') }}</strong>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Description</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control @if($errors->has('description')) help-block-text @endif"  name="description" id="description" placeholder="Enter Plan description">{{ isset($plan)?$plan->description:"" }}</textarea>
                                        @if ($errors->has('description'))
                                        <strong class="help-block"> {{ $errors->first('description') }}</strong>
                                        @endif
                                    </div>
                                </div>

                            </div>


                            <!-- /.box-body -->
                            <div class="box-footer">
                                <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                <div class="col-sm-offset-1 col-sm-4"></div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script>
    $(document).ready(function(){
       $('#openinput').click(function(){
           $('#showplan').css("display", "block");
       }); 
       
       $('#forever').click(function(){
           $('#showplan').css("display", "none");
       });
    });
</script>
@endsection
