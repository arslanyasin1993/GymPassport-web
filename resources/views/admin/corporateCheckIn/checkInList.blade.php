@extends('admin_dash.design')
@section('content')
    <style>
        table.dataTable thead > tr > th.sorting_asc, table.dataTable thead > tr > th.sorting_desc, table.dataTable thead > tr > th.sorting, table.dataTable thead > tr > td.sorting_asc, table.dataTable thead > tr > td.sorting_desc, table.dataTable thead > tr > td.sorting {
            padding-right: 0px !important;
        }

        img.user_image {
            height: auto;
            width: 79px;
            border-radius: 15px;
        }
    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Corporate Check Ins</h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Corporate Check Ins</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                @if(Session::has('message'))
                    <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {!! session('msg') !!}
                    </div>
                @endif

                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active">
                                <div class="box-header with-border">
                                    <div class="col-sm-12">
                                        <div class="col-sm-2" style="margin-bottom: 5px;">
                                            <a href="{{route('add-subscriber')}}">
                                                <button type="button" class="btn btn-block btn-success"><i class="fa fa-edit"></i> Add Subscriber</button>
                                            </a>
                                        </div>
                                        <!-- <div class="col-sm-4"></div> -->
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <select name="visit_pass" class="form-control" id="visit_pass">
                                                    <option value="">Select No.Of Pass</option>
                                                    <option value="{{trans('constants.lite')}}" @php echo $get = '1'==old('visit_pass') ? "selected" :"";@endphp>Gym Passport - Lite</option>
                                                    <option value="{{trans('constants.standard')}}" @php echo $get = '2'==old('visit_pass') ? "selected" :"";@endphp>Gym Passport - Standard</option>
                                                    <option value="{{trans('constants.pro')}}" @php echo $get = '3'==old('visit_pass') ? "selected" :"";@endphp>Gym Passport - Pro</option>
                                                </select>
                                            </div>
                                            <label for="inputEmail3" class="col-sm-2 control-label" style="width: 98px;">From-Date</label>
                                            <div class="col-sm-2">
                                                <input type='date' class="form-control" id='startdate' placeholder='From Date'>
                                            </div>
                                            <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                            <div class="col-sm-2">
                                                <input type="date" name="" id="enddate" class="form-control" placeholder="End Date">
                                            </div>
                                            <div class="col-sm-1">
                                                <button class="btn btn-primary" id="submit">Apply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="corporate_plan_table" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Company</th>
                                                    <th>No of Employees</th>
                                                    <th>Plan</th>
                                                    <th>Check Ins</th>
                                                    <th>Check In Amount</th>
                                                    <th>Details</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <td style="font-weight: bold">Total Amount</td>
                                                    <td colspan="4" style="font-weight: bold; text-align: end"></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script>
                $(document).ready(function () {
                    var plan_cat = '5';
                    fetchcorporatesubscription(plan_cat);
                });

                function fetchcorporatesubscription(plan_cat) {
                    // alert(plan_cat);

                    is_corporate = 1;
                    var table_id = 'corporate_plan_table';

                    $('#' + table_id).DataTable().destroy();
                    var dataTable = $('#' + table_id).DataTable({
                        'processing': true,
                        'serverSide': true,
                        //'searching': false, // Remove default Search Control
                        'ajax': {
                            'url': "{{route('ajaxsubscriberlistcorporate')}}",
                            'data': function (data) {
                                // Read values
                                var enddate = $('#enddate').val();
                                var startdate = $('#startdate').val();
                                var visit_pass = $('#visit_pass').val();
                                //var searchdata = $('#searchdata').val();

                                // Append to data
                                data.startdate = startdate;
                                data.enddate = enddate;
                                data.visit_pass = visit_pass;
                                data.plan_cat = plan_cat;
                                data.is_corporate = is_corporate;
                                //data.searchdata = searchdata;
                            }
                        },
                        "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000, 5000]],
                        "footerCallback": function( row, data, start, end, display ) {
                            var api = this.api();
                            $( api.column( 1 ).footer() ).html(api.ajax.json().revenue); // get the totalAmount from the JSON response
                        },
                        'columns': [

                            {data: 'corporate_name'},
                            {data: 'no_of_employee'},
                            {data: 'plan_name'},
                            {data: 'total_cehck_ins'},
                            {data: 'total_amount'},
                            {data: 'detail'},
                        ]
                    });
                    $('#searchdata').keyup(function () {
                        dataTable.draw();
                    });
                    $('#visit_pass').change(function () {
                        dataTable.draw();
                    });
                    $('#submit').click(function () {
                        dataTable.draw();
                    });
                }
            </script>
        </section>
    </div>
@endsection

