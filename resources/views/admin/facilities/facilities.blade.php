@extends('admin_dash.design')  
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      @if(empty($data_facilities))
        Create Facilities
      @else
         Update Facilities
      @endif
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         @if(empty($data_facilities))
         <li class="active"> Create Facilities</li>
      @else
         <li class="active"> Update Facilities</li>
      @endif
       
       
        
        
      </ol>
    </section>
<style type="text/css">
  .time{
    width: 99%;
    margin-left: 1px !important;
  }
  .star{
    color: red;
  }
  .help-block{
    color: red;
    border-color: red;
    line-height: 0px;
  }
  .help-block-text{
    color: red;
    border-color: red;
  }
  .select2-container .select2-selection--single {
    height: 35px !important;
  }
  .img_url{
    height: 30px;
    width: 30px;
  }
</style>
  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-offset-2 col-xs-12 col-sm-8 col-md-8">   
         @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
            @endif  
          <div class="box">
              <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
            </div>
        @if(empty($data_facilities))
            <form action="{{route('add_facilities')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
               {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Facilities<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @if($errors->has('facilities')) help-block @endif" id="facilities" name="facilities" placeholder="facilities Name" value="{{ old('facilities') }}">
                     @if ($errors->has('facilities'))
                         <strong class="help-block"> {{ $errors->first('facilities') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Facilities Icon<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="file" class="form-control @if($errors->has('facilities_icon')) help-block @endif" id="facilities_icon" name="facilities_icon" placeholder="Pay on 2nd visit" value="">
                     @if ($errors->has('facilities_icon'))
                         <strong class="help-block"> {{ $errors->first('facilities_icon') }}</strong>
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
               <div class="col-sm-offset-1 col-sm-4"></div>
                <button type="submit" class="btn btn-info">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
              </div>
              <!-- /.box-footer -->
            </form>
        @else

            <form action="{{route('update_faci',['id'=>$data_facilities->id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
               {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Facilities<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @if($errors->has('facilities')) help-block @endif" id="facilities" name="facilities" placeholder="facilities Name" value="{{$data_facilities->facilities}}">
                     @if ($errors->has('facilities'))
                         <strong class="help-block"> {{ $errors->first('facilities') }}</strong>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Facilities Icon<span class="star"></span></label>
                  <div class="col-sm-9">
                    <input type="file" class="form-control @if($errors->has('facilities_icon')) help-block @endif" id="facilities_icon" name="facilities_icon" placeholder="Pay on 2nd visit" value="">
                     @if ($errors->has('facilities_icon'))
                         <strong class="help-block"> {{ $errors->first('facilities_icon') }}</strong>
                    @endif

                    <img src="{{$data_facilities->imag_url}}"  style="margin-top: 12px;width: 52px;
    height: 52px;">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
               <div class="col-sm-offset-1 col-sm-4"></div>
                <button type="submit" class="btn btn-info">Update</button>
                <a href="{{route('home')}}"><button type="button" class="btn btn-danger">Cancel</button></a>
              </div>
              <!-- /.box-footer -->
            </form>
        @endif
          </div>
          </div>
@if(empty($data_facilities))
           <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tbody>
                  <tr>
                  <th>ID</th>
                  <th>Facilities</th>
                  <th>Facilities Icon</th>
                  <th>Action</th>
                </tr>
              @if(!empty($facilities))
                @foreach($facilities as $key=>$value)
                <tr>
                  <td>{{$value->id}}</td>
                  <td>{{$value->facilities}}</td>
                  <td><img src="{{$value->imag_url}}" class="img_url"></td>
                  <td>
                          <a href="{{route('edit_facilities',['id'=>$value->id])}}"><i class="fa fa-edit"></i></a>
                          <a href="{{route('delete_facilities',['id'=>$value->id])}}"><i class="fa fa-trash"></i></a>

                  </td>
                </tr>
                @endforeach
              @endif
              </tbody>
            </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
              @if(!empty($facilities))
                {{ $facilities->links() }}
              @endif
              </ul>
            </div>
          </div>
@endif
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

var geocoder = new google.maps.Geocoder();
var address = "noida";

geocoder.geocode( { 'address': address}, function(results, status) {

  if (status == google.maps.GeocoderStatus.OK) {
    var latitude = results[0].geometry.location.lat();
    var longitude = results[0].geometry.location.lng();
    alert(latitude);
  } 
}); 
</script>
@endsection

