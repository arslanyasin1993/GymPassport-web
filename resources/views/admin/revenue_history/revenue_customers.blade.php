@extends('admin_dash.design')
@section('content')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
    <style>
        table.dataTable thead > tr > th.sorting_asc, table.dataTable thead > tr > th.sorting_desc, table.dataTable thead > tr > th.sorting, table.dataTable thead > tr > td.sorting_asc, table.dataTable thead > tr > td.sorting_desc, table.dataTable thead > tr > td.sorting {
            padding-right: 0px !important;
        }

        img.user_image {
            height: auto;
            width: 79px;
            border-radius: 15px;
        }
    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Visitors Detail</h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Visitors Detail</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active">
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="Table_ID" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>User</th>
                                                    <th>Gym Name</th>
                                                    <th>Check In</th>
                                                    <th>Gym Earning</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($visitors as $key => $visitor)
                                                    <tr>
                                                        <td>{{ $key + 1 }}</td>
                                                        <td>{{ $visitor->user_detail->first()->first_name . ' ' . $visitor->user_detail->first()->last_name }}</td>
                                                        <td>{{ $visitor->gym_name->gym_name }}</td>
                                                        <td>{{ $visitor->user_check_in }}</td>
                                                        <td>{{ $visitor->gym_earn_amount }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#Table_ID').DataTable({
                        "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000 , 5000]],
                    });
                });
            </script>
        </section>
    </div>
@endsection

