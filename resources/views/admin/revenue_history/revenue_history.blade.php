@extends('admin_dash.design')
@section('content')
<script src="{{asset('js/jquery.js')}}"></script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            @if(empty($data_facilities))
                Revenue History
            @else
                Update Facilities
            @endif
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            @if(empty($data_facilities))
                <li class="active"> Revenue History</li>
            @else
                <li class="active"> Update Facilities</li>
            @endif
        </ol>
    </section>
    <link href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        .time {
            width: 99%;
            margin-left: 1px !important;
        }
        .star {
            color: red;
        }
        .help-block {
            color: red;
            border-color: red;
            line-height: 0px;
        }
        .help-block-text {
            color: red;
            border-color: red;
        }
        .select2-container .select2-selection--single {
            height: 35px !important;
        }
        .img_url {
            height: 30px;
            width: 30px;
        }
        .panel-primary{
            border: 1px solid #fff;
        }
        .content_cls {
            border-radius: 4px;
            width: 100%;
            padding: 10px 10px;
        }
        .panel-body.custom_panel_body {
            position: relative;
            background-color: #222d32;
            border-radius: 0;
        }
        h3.user_info {
            font-size: 17px;
            color: #fff;
        }
        .panel-body.custom_panel_body {
            position: relative;
            background-color: #222d32;
        }
        .row.panel {
            background-color: #dedede;
        }
    </style>
    <section class="content" style="min-height: 0px !important;">
        <div class=" panel panel-default">
            <div class=" panel-body">
                <div class=" row row-group">
                    <form action="{{ route('revenueHistory') }}" method="GET">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <select name="gym_type" id="gym_type" class=" form-control" >
                                    <option value="{{trans('constants.daily')}}" {{ request()->gym_type == trans('constants.daily') ? 'selected' : '' }}>Daily</option>
                                    <option value="{{trans('constants.weekly')}}" {{ request()->gym_type == trans('constants.weekly') ? 'selected' : '' }}>Weekly</option>
                                    <option value="{{trans('constants.monthly')}}" {{ request()->gym_type == trans('constants.monthly') ? 'selected' : '' }}>Monthly</option>
                                    <option value="{{trans('constants.three_month')}}" {{ request()->gym_type == trans('constants.three_month') ? 'selected' : '' }}>3 Months</option>
                                    <option value="{{trans('constants.six_month')}}" {{ request()->gym_type == trans('constants.six_month') ? 'selected' : '' }}>6 Months</option>
                                    <option value="{{trans('constants.nine_month')}}" {{ request()->gym_type == trans('constants.nine_month') ? 'selected' : '' }}>9 Months</option>
                                    <option value="{{trans('constants.yearly')}}" {{ request()->gym_type == trans('constants.yearly') ? 'selected' : '' }}>Yearly</option>
                                    <option value="custom_date" {{ request()->gym_type == 'custom_date' ? 'selected' : '' }}>Custom Date</option>
                                </select>
                                <p style="color:aliceblue ;font-weight:bold" id="start_date"></p>
                            </div>
                            <span id="custom_date" style="{{ request()->gym_type == 'custom_date' ? 'display: block' : 'display: none' }}">
                                <div class="col-sm-3">
                                    <input type="date" id="start_date" value="{{ request()->start_date ? request()->start_date : date('Y-m-d') }}" name="start_date" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                    <input type="date" id="end_date" name="end_date" value="{{ request()->end_date ? request()->end_date : date('Y-m-d') }}" class="form-control">
                                </div>
                            </span>
                            <button class="btn btn-primary">Apply</button>
                            <a href="{{ route('revenueHistory') }}" class="btn btn-danger">Reset</a>
                        </div>
                    </form>
                </div>
                <div class=" row" style="margin-top: 2%;" id="check-in-history"></div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <style type="text/css">
                        img.user_image {
                            height: auto;
                            width: 79px;
                            border-radius: 15px;
                        }
                    </style>
                    <div class="box-body">
                        <div id="expbuttons"></div>
                        <table id="Table_ID" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>GymName</th>
                                    <th>Per Visit Price</th>
                                    <th>Visited Users</th>
                                    <th>Revenue</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $total_revenue = 0; @endphp
                                @foreach($data as $result)
                                    <tr>
                                        <td>{{ $result['gym_name'] }}</td>
                                        <td>{{ $result['gym_price_per_visit'] }}</td>
                                        <td>{{ $result['visited_users'] }}</td>
                                        <td>{{ $result['revenue'] }}</td>
                                        <td>{!! $result['details'] !!}</td>
                                        @php $total_revenue = $total_revenue + $result['revenue']; @endphp
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td style="font-weight: bold">Total Revenue</td>
                                    <td colspan="3" style="text-align: end; font-weight: bold">{{ number_format($total_revenue) }} RS</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var date_range;
                $('#Table_ID').DataTable({
                    "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000 , 5000]],
                });

                $('#reservation').on('apply.daterangepicker', function(ev, picker){
                    date_range = $(this).val();
                });
            });

            $(document).on('change' , '#gym_type' , function (){
                if($(this).val() == 'custom_date'){
                    $('#custom_date').css("display", "block");
                }else{
                    $('#custom_date').css("display", "none");
                }
            });
        </script>
    </section>
</div>

</div>
</div>

<script>
    function get_data() {
        let gym_id = $('#gym_name').val();
        let gym_type = $('#gym_type').val();

        $.ajax({
            url: "{{route('gymrevenue.details')}}",
            type: "post",
            data: {
                _token: "{{csrf_token()}}",
                gym_id: gym_id,
                gym_type: gym_type
            },
            success: function(data) {
                console.log(data);
                $('#start_date').text(data.start_date + ' ' + 'to' + ' ' + data.end_date);
                $('#total_amount').text(data.total_amount);
                var details = '';
                $.each(data.visit_details, function(key, value) {
                    details += <div class=" panel panel-primary" style="margin-right:10px; margin-left:10px">
                        <div class="panel-body custom_panel_body" data-toggle="collapse" href="#custom_content_id" aria-expanded="false" aria-controls="footwear">
                            <div class="collapse content_cls" id="custom_content_id">
                                <p>Collapsible content...</p>
                                 
                                 
                            </div>
                            <div class="col-md-3">
                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                     
                            <div class="col-md-3">
                                <h3>${value.user_detail[0].first_name +' '+ value.user_detail[0].last_name}</h3>
                                <h3>Id: ${value.user_detail[0].user_ufp_id}</h3>
                            </div>
                            <div class="col-md-3">
                                <h3>check in counter</h3>
                                <h3>Status: ${value.user_detail[0].status==1?'Active': 'Expired'}</h3>
                            </div>
                            <div class="col-md-3">
                                <h3>check-In time: ${value.check_in_time}</h3>
                                <h3>check-In date: ${value.check_in_date}</h3>
                            </div>
                     
                        </div>

                    </div>
                });

                $('#visit_detail').append(details);


            }
        });

    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var gym_id = $('#gym_name').val();
        var gym_type = $('#gym_type').val();
        var user_id = '';
        if (gym_id != '' && gym_type != '') {
            //getuser(gym_id, gym_type);
            //gym_earn_amount(gym_id, gym_type);
            //select_gym_wise_user_list(gym_id);
        }

        $('#gym_type').change(function(event) {
            event.preventDefault();
            var gym_id = $('#gym_name').val();
            var gym_type = $('#gym_type').val();
            if (gym_id != '' && gym_type != 'advanced') {
                //getuser(gym_id, gym_type);
                //gym_earn_amount(gym_id, gym_type);
            }
            if (gym_id != '' && gym_type == 'advanced') {
                $('.modal').modal('show');
                //getuser(gym_id,gym_type);
                //gym_earn_amount(gym_id,gym_type);
            }
        });

        $('#gym_name').change(function(event) {
            event.preventDefault();
            var gym_id = $('#gym_name').val();
            var gym_type = $('#gym_type').val();
            if (gym_id != '' && gym_type != '') {
                getuser(gym_id, gym_type);
                gym_earn_amount(gym_id, gym_type);
            }
        });
        $('#user_name').change(function(event) {
            event.preventDefault();
            var user_id = $('#user_name').val();
            var gym_id = $('#gym_name').val();
            var gym_type = $('#gym_type').val();
            if (gym_id != '' && gym_type != '' && user_id != '') {
                getuser(gym_id, gym_type);
                gym_earn_amount(gym_id, gym_type);
            }
        });

        function getuser(gym_id, gym_type) {
            $.ajax({
                url: "{{route('user_check_in_histroy_admin')}}",
                data: {
                    'gym_id': gym_id,
                    'gym_type': gym_type,
                    '_token': "{{ csrf_token() }}"
                },
                type: "post",
                success: function(data) {
                    $("#check-in-history").html(data);
                }
            });
        }


        function gym_earn_amount(gym_id, gym_type) {
            $.ajax({
                url: "{{route('gym_earn_amount_admin')}}",
                data: {
                    'gym_id': gym_id,
                    'gym_type': gym_type,
                    '_token': "{{ csrf_token() }}"
                },
                type: "post",
                success: function(data) {
                    //console.log(data);
                    if (data.status) {
                        $('#fetch_amount').html(data.amount);
                        $('#month_amount').html(data.monthly_amount);
                        $('#daily_amount').html(data.daily_amount);
                        $('#bydate').html(data.date);
                    } else {
                        var amount = '0';
                        $('#fetch_amount').html(amount);
                        $('#month_amount').html('0');
                        $('#daily_amount').html('0');
                        $('#bydate').html(data.date);
                    }

                }
            });
        }

        function select_gym_wise_user_list(gym_id) {
            $.ajax({
                url: "{{route('select_box_user_admin')}}",
                data: {
                    'gym_id': gym_id,
                    '_token': "{{ csrf_token() }}"
                },
                type: "post",
                success: function(data) {
                    // alert(data);
                    var obj = jQuery.parseJSON(data);
                    $.each(obj, (key, val) => {
                        let user_name = `<option class="cards-sub-plan" value="${val.id}">${val.first_name} ${val.last_name}</option>`;
                        $("#user_name").append(user_name);
                    });
                }
            });
        }
    });
</script>
<script>
    $(document).ready(function() {
        $.ajax({
            url: "{{route('get_payment_period_admin')}}",
            data: {
                '_token': "{{ csrf_token() }}"
            },
            type: "post",
            success: function(data) {
                // alert(data);
                var value = jQuery.parseJSON(data);
                console.log(value);
                if (value.status) {
                    $('#payment_date').html(value.payout_date);
                    $('#from_date').html(value.start_date);
                    $('#to_date').html(value.to_date);
                }

            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $(".image-pay-hist").click(function() {
            $(".popup-paymentaa-second").toggle();
        });
    });

    $(document).ready(function() {
        $(".trigger-navbar").click(function() {
            $(".custom-navbar-pay-hist").slideToggle();
        });
    });
</script>

@endsection