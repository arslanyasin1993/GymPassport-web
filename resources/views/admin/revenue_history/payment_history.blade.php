@extends('admin_dash.design')
@section('content')
    <script src="{{asset('js/jquery.js')}}"></script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @if(empty($data_facilities))
                    Payment History
                @else
                    Update Facilities
                @endif

            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                @if(empty($data_facilities))
                    <li class="active"> Payment History</li>
                @else
                    <li class="active"> Update Facilities</li>
                @endif


            </ol>
            @if(Session::has('message'))
                <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {!! session('msg') !!}
                </div>
            @endif
            <div class="box">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Upload Payments CSV File.</h3>
                    </div>
                    <form action="{{route('upload_payments')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">CSV File<span class="star">*</span></label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control @if($errors->has('upload_payment')) help-block @endif" id="upload_payment" name="upload_payment" placeholder="Pay on 2nd visit" value="">
                                    @if ($errors->has('upload_gym_owner'))
                                        <strong class="help-block"> {{ $errors->first('upload_payment') }}</strong>
                                    @endif
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="col-sm-offset-1 col-sm-4"></div>
                                <button type="submit" name="submit" class="btn btn-info">Submit</button>
                                <input type="hidden" name="submit" value="import">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </section>
        <style type="text/css">
            .time {
                width: 99%;
                margin-left: 1px !important;
            }

            .star {
                color: red;
            }

            .help-block {
                color: red;
                border-color: red;
                line-height: 0px;
            }

            .help-block-text {
                color: red;
                border-color: red;
            }

            .select2-container .select2-selection--single {
                height: 35px !important;
            }

            .img_url {
                height: 30px;
                width: 30px;
            }

            .panel-primary {
                border: 1px solid #fff;
            }

            .content_cls {


                border-radius: 4px;

                width: 100%;
                padding: 10px 10px;
                /* margin-left: -14px; */
            }

            .panel-body.custom_panel_body {
                position: relative;
                background-color: #222d32;
                border-radius: 0;

            }

            h3.user_info {
                font-size: 17px;
                color: #fff;
            }

            .panel-body.custom_panel_body {
                position: relative;
                background-color: #222d32;
            }

            .row.panel {
                background-color: #dedede;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <form action="{{ route('paymentsHistory') }}" method="GET">
                                <div class="col-sm-3">
                                    <input type="text" value="{{ !empty(request()->dateFilter) ? request()->dateFilter : '' }}" name="dateFilter" id="reservation" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-primary" type="submit">Filter</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example_registered" class="table table-bordered w-100">
                                    <thead>
                                    <tr>
                                        <th>Gym Name</th>
                                        <th>Gym Email</th>
                                        <th>Transaction ID</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                        <th>Send Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($gym_list))
                                        @foreach($gym_list as $key=>$value)
                                            <tr>
                                                <td>{{$value->gym_name}}</td>
                                                <td>{{$value->gym_owner_email}}</td>
                                                <td>{{$value->trans_id}}</td>
                                                <td>{{$value->amount}}</td>
                                                <td>{{$value->date}}</td>
                                                <td>
                                                    @if($value->email_sent!=1)
                                                        <a href="{{route('send-transaction-email',$value->id)}}" class="btn btn-success btn-xs">Send Email</a>
                                                    @else
                                                        {{"Email Sent"}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                @if(!empty($user))
                                    {{ $user->links() }}
                                @endif
                            </ul>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            var dataTable = $('#example_registered').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'pageLength'
                ],
                "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000 , 5000]],
            });
            dataTable.draw();


        });
    </script>
@endsection
