<style type="text/css">
 .image_class{
      width: 105px;
    height: 105px;
    border-radius: 22px;

 }
 #checkin_user_page {
    float: right;
}
</style>

@if(!empty($check_in_detail))
@php $i=0; @endphp
@foreach($check_in_detail as $key=>$data)
    <div class=" panel panel-primary" style="margin-right:10px; margin-left:10px">
      <div class="panel-body custom_panel_body" data-toggle="collapse" href="#custom_content_id{{$i}}" aria-expanded="false" aria-controls="footwear">
          <div class="col-md-3">
              <img src="{{$data['image']}}" alt="..." class="margin image_class">
          </div>
          <div class="col-md-3">
              <h3 class="user_info">{{$data['name']}}</h3>
              <h3 class="user_info">Id: {{$data['user_ufp_id']}}</h3>
          </div>
          <div class="col-md-3">
              <h3 class="user_info">check in counter ({{$data['total_visit']}} of {{$data['other_gym_total_visit']}})</h3>
              <h3 class="user_info">Status: {{$data['plan_status']}}</h3>
          </div>
          <div class="col-md-3">
              <h3 class="user_info">check-In time: {{$data['check_in_time']}}</h3>
              <h3 class="user_info">check-In date: {{$data['check_in_date']}}</h3>
          </div>
          </div>

      

          <div class="collapse content_cls" id="custom_content_id{{$i}}">
              <div class="content-checkin">
                @if(!empty($data['user_all_visit']))
                @foreach($data['user_all_visit'] as $key=>$val)
                 <div class="abc-collapse">
                    <div class="row panel">
                          <div class="col-sm-6">
                            <p class="checkin-para">Gym Name3456790-:  <span class="gym_detail">{{$val->gym_name->gym_name}}</span></p>
                            @php
                              if($val->get_subscription->visit_pass=='month'){
                                  $gym_earn_amount_old = "(Final amount is calculated after one month cycle and paid during the next month payment cycle)";
                                  $gym_earn_amount_last = "(paid to Gym on 5th ".date('M Y',strtotime("+1 month",strtotime($val->get_subscription->expired_at))).')';
                                  $gym_earn_amount = "";
                              }else{
                                  $gym_earn_amount = '';
                              }
                            @endphp
                            @if($val->get_subscription->visit_pass=='month')
                            <p class="checkin-para">Estimated Amount:  <span class="gym_detail">{{$val->gym_earn_amount}} {{$gym_earn_amount}}</span></p>
                            @else
                            <p class="checkin-para">Total Amount:  <span class="gym_detail">{{$val->gym_earn_amount}} {{$gym_earn_amount}}</span></p>
                            @endif
                          </div>
                        <div class="col-sm-6">
                          <p class="checkin-para">Check-in Date:  {{date('d-m-Y', strtotime($val->user_check_in))}}</p>
                          <p class="checkin-para">Check-in Time:  {{date('h:i A', strtotime($val->user_check_in))}}</p>
                          @if($val->get_subscription->visit_pass=='month')
                          @php
                              $date_exp = date('Y-m-d', strtotime('-1 day', strtotime($val->get_subscription->purchased_at)));
                          @endphp
                          @endif
                             <p class="checkin-para">Address:  <span class="gym_detail">{{$val->gym_name->gym_address}}</span></p>
                        </div>
                    </div>

                  </div>
                @endforeach
               @endif
               </div>
          </div>
          </div>
       
<!--  <div class="billing-history-wrapper-checkins collapsible-checkin">
          <div class="row">
            <div class="col-12 col-sm-2">
              <div class="user_image">
                <img src="http://3.6.180.30/ultimateFitness/{{$data['image']}}" class="img-fluid check_image">
              </div>
            </div>
            <div class="col-12 col-sm-3">
              <div class="row">
                <div class="user-info">
                   <div class="user_name">{{$data['name']}}</div>
                </div>
             </div>
              <div class="row">
                <div class="user-info">
                <div class="user_ID">ID : </div>
                <div class="id-value"> {{$data['user_ufp_id']}}</div>
              </div>
              </div>
              
            </div>
            <div class="col-12 col-sm-3">
              <div class="user_check_in_counter">Check-in Counter : </div>
              <div class="user_check_in_counter_value">({{$data['total_visit']}} of {{$data['other_gym_total_visit']}})</div> 
              <div class="status_div">  
              <span class="user_check_in_counter">Status: </span>
              <span class="user_check_in_counter_value">{{$data['plan_status']}}</span>
              </div>
                 
            </div>
            <div class="col-12 col-sm-4">
              <div class="check_date_time">
                 <h4 class="time-date-box">
                <span class="purchased-payment-billing-checkins">Check-In Time</span><span class="purchased-payment-date-billing-checkins">{{$data['check_in_time']}}</span>
              </h4>
              <h4 class="time-date-box">
                <span class="purchased-payment-billing-checkins">Check-In-Date</span>
                <span class="purchased-payment-date-billing-checkins">{{$data['check_in_date']}}</span>
              </h4>
              </div>
            </div>
          </div>

    </div> -->


<!-- <div class="content-checkin">
  @if(!empty($data['user_all_visit']))
  @foreach($data['user_all_visit'] as $key=>$val)
   <div class="abc-collapse">
      <div class="row">
            <div class="col-sm-6">
              <p class="checkin-para">Gym Name:  <span class="gym_detail">{{$val->gym_name->gym_name}}</span></p>
              @php
                if($val->get_subscription->visit_pass=='month'){
                    $gym_earn_amount_old = "(Final amount is calculated after one month cycle and paid during the next month payment cycle)";
                    $gym_earn_amount_last = "(paid to Gym on 5th ".date('M Y',strtotime("+1 month",strtotime($val->get_subscription->expired_at))).')';
                    $gym_earn_amount = "";
                }else{
                    $gym_earn_amount = '';
                }
              @endphp
              @if($val->get_subscription->visit_pass=='month')
              <p class="checkin-para">Estimated Amount:  <span class="gym_detail">{{$val->gym_earn_amount}} {{$gym_earn_amount}}</span></p>
              @else
              <p class="checkin-para">Total Amount:  <span class="gym_detail">{{$val->gym_earn_amount}} {{$gym_earn_amount}}</span></p>
              @endif
            </div>
          <div class="col-sm-6">
            <p class="checkin-para">Check-in Date:  {{date('d-m-Y', strtotime($val->user_check_in))}}</p>
            <p class="checkin-para">Check-in Time:  {{date('h:i A', strtotime($val->user_check_in))}}</p>
            @if($val->get_subscription->visit_pass=='month')
            @php
                $date_exp = date('Y-m-d', strtotime('-1 day', strtotime($val->get_subscription->purchased_at)));
            @endphp
            @endif
               <p class="checkin-para">Address:  <span class="gym_detail">{{$val->gym_name->gym_address}}</span></p>
          </div>
      </div>

    </div>
  @endforeach
 @endif
 </div> --> 
 @php $i++; @endphp
 @endforeach
   <div id="checkin_user_page"></div>
@else

@endif
<style type="text/css">
 .user_image {
    height: 100px;
    padding: 12px 27px;
    /*margin-top: 20px;*/
}
.check_date_time {
    box-shadow: 0px 0px 12px #4e4e4e;
    border-radius: 16px;
    margin-top: 18px;
    padding: 15px 10px;
    width: 100%;
    max-width: 100%;
}
.user_name , .user_ID , .user_plan , .user_check_in_counter {
    color: #fff;
    font-size: 20px;
    font-weight: 600;
    font-family: Raleway;
    margin-top: 12px;
}
.id-value , .plan-value , .user_check_in_counter_value{
  color: #9c9c9c;
    font-weight: 600;
    font-family: Raleway;
    font-size: 18px;
    margin-top: 0px;
    padding: 0px 4px;
}
  .amt-subscription-billing-checkins {
    padding: 0px 22px;
  }
  .billing-history-wrapper-checkins.collapsible-checkin {
    padding-top: 26px;
}
  .user-info {
    display: inline-flex;
  }
  .id-value , .plan-value {
    margin-top: 10px;
}
  @media(max-width: 1200px){
   .user_name, .user_ID, .user_plan, .user_check_in_counter {
    font-size: 16px;
}
.id-value, .plan-value, .user_check_in_counter_value {
    font-size: 14px;
}
.purchased-payment-billing-checkins {
    font-size: 16px;
    }
    .purchased-payment-date-billing-checkins {
    font-size: 16px;
    margin-top: 13px;
    padding-right: 11px;
  }
  .check_date_time {
    margin-top: 22px;
    padding: 14px 2px;
}
.id-value, .plan-value, .user_check_in_counter_value {
    font-size: 12px;
}
.purchased-payment-billing-checkins {
    font-size: 11px;
}
.purchased-payment-date-billing-checkins {
    font-size: 14px;
}
}
@media (max-width: 992px){
.user_name, .user_ID, .user_plan, .user_check_in_counter {
    font-size: 14px;
}
.user_image {
    padding: 44px 13px;

}
.billing-history-wrapper-checkins {
    padding: 0 45px 0 0;
}
}
@media (max-width: 850px){
.box-one-for-bg-img {
    background-position: -17px 11px;
}
}
@media (max-width: 800px){
.billing-history-wrapper-checkins {
    padding: 21px 27px 0 0px;
}
.purchased-payment-date-billing-checkins {
    font-size: 12px;
}

}

@media (max-width: 768px){
  .billing-history-wrapper-checkins {
    background-image: none;
  }
  .custom-form-key-value {
    margin-right: 156px;
}
.three-two-btns {
    /* margin: 0 auto; */
    margin-left: 257px;
}
.user_name, .user_ID, .user_plan, .user_check_in_counter {
   margin-top: -2px;
  }
.id-value , .plan-value {
    margin-top: -2px;
}
}
@media (max-width: 740px){
  .check_date_time {
    margin-top: 0px;
   
}
     .purchased-payment-billing-checkins {
    display: flex;
    justify-content: center;
}
.purchased-payment-date-billing-checkins {
    text-align: center;
    margin: 0 auto;
    display: flex;
    justify-content: center;
    float: inherit;
    margin-top: 8px;
}
.time-date-box{
  margin-top: 15px;
}
.user_name, .user_ID, .user_plan, .user_check_in_counter {
    font-size: 13px;
}
.id-value, .plan-value, .user_check_in_counter_value {
    font-size: 10px;

}
}
@media (max-width: 680px){
.list-sub-plan-checkins-select-gym {
    font-size: 13px;
}
.list-sub-plan-checkins-weekly {
    font-size: 13px;
}
}
@media (max-width: 575px){
 .user_image {
    width: 100%;
    margin: 0 auto;
    padding: 0px 2px;
    max-width: 21%;
    margin-top: -2px;
    text-align: center;
}

.billing-history-wrapper-checkins {
  background-image: none;
    max-width: 91%;
    padding: 0 10px;
    padding-top: 23px;
    height: 350px;
  }
  .user_name, .user_ID, .user_plan, .user_check_in_counter {
    margin: 0 auto;
    text-align: center;
    margin-top: 0px;
    line-height: 18px;
    font-size: 15px;
}
.id-value, .plan-value, .user_check_in_counter_value {
    margin-top: 0px;
    line-height: 15px;
    font-size: 13px;
    margin: 0 auto;
    text-align: center;
}
/*.purchased-payment-date-billing-checkins {
    font-size: 13px;
    text-align: center;
    float: left;
}*/
.purchased-payment-billing-checkins {
    display: flex;
    font-size: 15px;
    justify-content: center;
    padding: 5px 7px;
}
.user_name {

    margin: 0 auto;
    }
    .user-info {
    text-align: center;
    margin: 0 auto;
    margin-bottom: 13px;
    margin-top: -7px;
}
.check_date_time {
    margin: 0 auto;
    display: flex;
    justify-content: center;
    box-shadow: none;
    padding: none;

}
}
@media(max-width: 450px){
  .user_image {
    max-width: 30%;
  }
}
@media(max-width: 400px)
{
   .user_image {
    max-width: 32%;
  }



  .list-sub-plan-checkins-select-gym {
    font-size: 9px;
}
.list-sub-plan-checkins-weekly {
    font-size: 10px;
  }
  .estimated-stmt-checkins {
    font-size: 9px;
}
}
@media (max-width: 330px){
.three-two-btns {
    margin-left: 101px;
}
}
<style>

</style>
<script src="{{ asset('website_file/jquery.bootpag.min.js')}}"></script>
<?php  $total_pages = ceil($total_rows / $limit); if($total_pages > 1): ?>
<?php endif; ?>
<script>         
  var numval = localStorage.getItem("check_in_user");
 var gym_id = $('#gym_name').val();
  var gym_type = $('#gym_type').val();
  if(numval != null){
    var nval = numval;
  }else{
    var nval = 1;
  } 
  $('#checkin_user_page').bootpag({
    total: '<?php if(!empty($total_pages)){ echo $total_pages; } ?>',
    page: nval,
    maxVisible: '<?php if(!empty($total_pages)){ echo $total_pages; } ?>',
    firstLastUse: false,
    wrapClass: 'pagination',
    activeClass: 'active',
    disabledClass: 'disabled',
  }).on("page", function(event, check_in_user, gym_id, gym_type){
      localStorage.setItem("check_in_user", check_in_user);
      $.ajax({
      type: 'post',
      url: "{{route('user_check_in_histroy_admin')}}",
      data: {"check_in_user" : check_in_user, '_token': "{{ csrf_token() }}"},
      beforeSend:function(){
      },
      complete:function(){
      },
      }).done(function(data) {
      $("#check-in-history").html(data);
         localStorage.setItem("check_in_user",1);
      });
  });

var coll = document.getElementsByClassName("collapsible-checkin");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}
</script>