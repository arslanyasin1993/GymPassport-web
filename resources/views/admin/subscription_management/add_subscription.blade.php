@extends('admin_dash.design')  
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        @if(isset($plan_id) && !empty($plan_id))
         Update Subscription
        @else
        Create Subscription
        @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="{{route('subscription_list')}}">subscription</a></li>
        @if(isset($plan_id) && !empty($plan_id))
         <li class="active">Update-subscription</li>
        @else
        <li class="active">create-subscription</li>
        @endif
        
      </ol>
    </section>
<style type="text/css">
  .time{
    width: 99%;
    margin-left: 1px !important;
  }
  .star{
    color: red;
  }
  .help-block{
    color: red;
    border-color: red;
    line-height: 0px;
  }
  .help-block-text{
    color: red;
    border-color: red;
  }
  .select2-container .select2-selection--single {
    height: 35px !important;
  }
</style>
  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">   
         @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
            @endif  
          <div class="box">
              <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
@if(isset($plan_id) && !empty($plan_id))
         <form action="{{route('update_subscription',['id'=>$plan_list->id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
               {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Plan Name<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <!-- <input type="text" class="form-control @if($errors->has('plan_name')) help-block @endif" id="plan_name" name="plan_name" value="{{$plan_list->plan_name}}" placeholder="Enter Plan Name"> -->
                     <select name="plan_name" id="plan_name" class="form-control @if($errors->has('plan_name')) help-block @endif">
                      <option value="">Select Plan</option>
                         @if(!empty($plan_cat))
                            @foreach($plan_cat as $key=>$val)
                              <option value="{{$val->id}}" @if($plan_list->plan_name==$val->id) selected @endif>{{$val->plan_cat_name}}</option>
                            @endforeach
                          @endif
                    </select>
                      @if ($errors->has('plan_name'))
                         <strong class="help-block"> {{ $errors->first('plan_name') }}</strong>
                      @endif
                  </div>
                </div> 

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">No.of visit<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <select name="visit_pass" id="visit_pass" class="form-control @if($errors->has('visit_pass')) help-block @endif">
                            <option value="">Select number of visit</option>

                            <option value="{{trans('constants.one_visit')}}" @if($plan_list->visit_pass=='1') selected @endif>Casual Pass</option>

                            <option value="{{trans('constants.five_visit')}}" @if($plan_list->visit_pass=='5') selected @endif>5 Visit Pass</option>

                            <option value="{{trans('constants.ten_visit')}}" @if($plan_list->visit_pass=='10') selected @endif>10 Visit Pass</option>

                            <!-- <option value="{{trans('constants.month_visit')}}" @if($plan_list->visit_pass=='30') selected @endif>30 Visit Pass</option> -->

                            <option value="{{trans('constants.monthly_visit')}}" @if($plan_list->visit_pass=='Monthly') selected @endif>Monthly</option>
                            <option value="{{trans('constants.3_monthly_visit')}}" @if($plan_list->visit_pass=='3 Month') selected @endif>3 Month</option>
                        <option value="{{trans('constants.6_month_visit')}}"  @if($plan_list->visit_pass=='6 Month') selected @endif>6 Month</option>
                        <option value="{{trans('constants.12_month_visit')}}"  @if($plan_list->visit_pass=='12 Month') selected @endif>12 Month</option>

                    </select>
                      @if ($errors->has('visit_pass'))
                         <strong class="help-block"> {{ $errors->first('visit_pass') }}</strong>
                      @endif
                  </div>
                </div> 

               <!--  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Plan Duration<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <select name="plan_duration" id="plan_duration" class="form-control  @if($errors->has('plan_duration')) help-block @endif">
                   
                     <option value="{{trans('constants.one_month')}}" @if($plan_list->plan_duration.' '.$plan_list->plan_time =='1 Month') selected @endif>{{trans('constants.one_month')}}</option>

                    <option value="{{trans('constants.7_day')}}" @if($plan_list->plan_duration.' '.$plan_list->plan_time =='7 Day') selected @endif>{{trans('constants.7_day')}}</option>

                    <option value="{{trans('constants.30_day')}}" @if($plan_list->plan_duration.' '.$plan_list->plan_time =='30 Day') selected @endif>{{trans('constants.30_day')}}</option>

                    <option value="{{trans('constants.60_day')}}" @if($plan_list->plan_duration.' '.$plan_list->plan_time =='60 Day') selected @endif>{{trans('constants.60_day')}}</option>
                    </select>
                   @if ($errors->has('plan_duration'))
                         <strong class="help-block"> {{ $errors->first('plan_duration') }}</strong>
                  @endif
                  </div>
                </div> -->

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Plan Amount<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @if($errors->has('amount')) help-block @endif" id="amount" name="amount" placeholder="Enter Amount" value="{{$plan_list->amount}}">
                     @if ($errors->has('amount'))
                         <strong class="help-block"> {{ $errors->first('amount') }}</strong>
                    @endif
                  </div>
                </div>
                <!-- select2 -->
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Country<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control  @if($errors->has('country_id')) help-block @endif" id="country_id" name="country_id">
                      @if(!empty($country))
                        <option value=""> Country</option>
                        @foreach($country as $key => $value)
                          @php $get = $value->id == $plan_list->country_id ? "selected" : "";@endphp
                           <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->nicename}}</option>
                        @endforeach
                      @else
                        <option value=""> Country Unavailable</option>
                      @endif
                    </select>
                     @if ($errors->has('country_id'))
                         <strong class="help-block"> {{ $errors->first('country_id') }}</strong>
                    @endif
                  </div>
                </div>

                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Plan Description<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <textarea  name="description" id="description" class="form-control @if($errors->has('description')) help-block-text @endif" placeholder="Enter Plan Description">{{$plan_list->description}}</textarea>
                     @if ($errors->has('description'))
                         <strong class="help-block"> {{ $errors->first('description') }}</strong>
                    @endif
                  </div>
                </div>

                  <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Status<span class="star">*</span></label>
                      <div class="col-sm-10">
                          <select class="form-control  @if($errors->has('status')) help-block @endif" id="status" name="status">
                              <option value="1" {{ $plan_list->status == '1' ? "selected" : "" }}>Active</option>
                              <option value="0" {{ $plan_list->status == '0' ? "selected" : "" }}>In Active</option>
                          </select>
                      </div>
                  </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
               <div class="col-sm-offset-1 col-sm-4"></div>
                <button type="submit" class="btn btn-info">Update</button>
                <a href="{{route('subscription_list')}}"><button type="button" class="btn btn-danger">Cancle</button></a>
              </div>
              <!-- /.box-footer -->
            </form>
@else
            <form action="{{route('add_sub')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
               {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Plan Name<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <!-- input type="text" class="form-control @if($errors->has('plan_name')) help-block @endif" id="plan_name" name="plan_name" value="{{ old('plan_name') }}" placeholder="Enter Plan Name"> -->
                    <select name="plan_name" id="plan_name" class="form-control @if($errors->has('plan_name')) help-block @endif">
                          <option value="">Select Plan</option>
                          @if(!empty($plan_cat))
                            @foreach($plan_cat as $key=>$val)
                              <option value="{{$val->id}}" @php echo $get = $val->id ==old('plan_name') ? "selected" :"";@endphp>{{$val->plan_cat_name}}</option>
                            @endforeach
                          @endif
                         <!--  <option value="{{trans('constants.light_fit')}}" @php echo $get = 'GymFit-Lite Pass'==old('plan_name') ? "selected" :"";@endphp>GymFit-Lite Pass</option>
                          <option value="{{trans('constants.gym_light')}}" @php echo $get = 'GymFit Pass'==old('plan_name') ? "selected" :"";@endphp>GymFit Pass</option>
                          <option value="{{trans('constants.premium_pass')}}" @php echo $get = 'Premium Pass'==old('plan_name') ? "selected" :"";@endphp>Premium Pass</option>
                          <option value="{{trans('constants.ultimate_pass')}}" @php echo $get = 'Ultimate Pass'==old('plan_name') ? "selected" :"";@endphp>Ultimate Pass</option> -->
                    </select>
                      @if ($errors->has('plan_name'))
                         <strong class="help-block"> {{ $errors->first('plan_name') }}</strong>
                      @endif
                  </div>
                </div> 

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">No.of visit<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <select name="visit_pass" id="visit_pass" class="form-control @if($errors->has('visit_pass')) help-block @endif">
                            <option value="">Select number of visit</option>

                            <option value="{{trans('constants.one_visit')}}" @php echo $get = '1'==old('visit_pass') ? "selected" :"";@endphp>Casual Pass</option>

                            <option value="{{trans('constants.five_visit')}}" @php echo $get = '5'==old('visit_pass') ? "selected" :"";@endphp>5 Visit Pass</option>

                            <option value="{{trans('constants.ten_visit')}}" @php echo $get = '10'==old('visit_pass') ? "selected" :"";@endphp>10 Visit Pass</option>

                            <!-- <option value="{{trans('constants.month_visit')}}" @php echo $get = '30'==old('visit_pass') ? "selected" :"";@endphp>30 Visit Pass</option> -->

                            <option value="{{trans('constants.monthly_visit')}}" @php echo $get = 'month'==old('visit_pass') ? "selected" :"";@endphp>Monthly</option>
                        <option value="{{trans('constants.3_monthly_visit')}}" @php echo $get = '3 month'==old('visit_pass') ? "selected" :"";@endphp>3 Month</option>
                        <option value="{{trans('constants.6_month_visit')}}" @php echo $get = '6_month'==old('visit_pass') ? "selected" :"";@endphp>6 Month</option>
                        <option value="{{trans('constants.12_month_visit')}}" @php echo $get = '12_month'==old('visit_pass') ? "selected" :"";@endphp>12 Month</option>
                    </select>
                      @if ($errors->has('visit_pass'))
                         <strong class="help-block"> {{ $errors->first('visit_pass') }}</strong>
                      @endif
                  </div>
                </div> 

               <!--  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Plan Duration<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <select name="plan_duration" id="plan_duration" class="form-control @if($errors->has('plan_duration')) help-block @endif">
                     <option value="">Select Duration</option>
                        

                          <option value="{{trans('constants.7_day')}}" @php echo $get = '7 Day'==old('plan_duration') ? "selected" :"";@endphp>{{trans('constants.7_day')}}</option>

                          <option value="{{trans('constants.30_day')}}" @php echo $get = '30 Day'==old('plan_duration') ? "selected" :"";@endphp>{{trans('constants.30_day')}}</option>

                          <option value="{{trans('constants.60_day')}}" @php echo $get = '60 Day'==old('plan_duration') ? "selected" :"";@endphp>{{trans('constants.60_day')}}</option>
                    </select>
                   @if ($errors->has('plan_duration'))
                         <strong class="help-block"> {{ $errors->first('plan_duration') }}</strong>
                  @endif
                  </div>
                </div> -->

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Plan Amount<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control @if($errors->has('amount')) help-block @endif" id="amount" name="amount" placeholder="Enter Amount" value="{{ old('amount') }}">
                     @if ($errors->has('amount'))
                         <strong class="help-block"> {{ $errors->first('amount') }}</strong>
                    @endif
                  </div>
                </div>
                <!-- select2 -->
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Country<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <select class="form-control  @if($errors->has('country_id')) help-block @endif" id="country_id" name="country_id">
                      @if(!empty($country))
                        <option value=""> Country</option>
                        @foreach($country as $key=>$value)
                          @php $get = $value->id==old('country_id') ? "selected" :"";@endphp
                           <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->nicename}}</option>
                        @endforeach
                      @else
                        <option value=""> Country Unavailable</option>
                      @endif
                    </select>
                     @if ($errors->has('country_id'))
                         <strong class="help-block"> {{ $errors->first('country_id') }}</strong>
                    @endif
                  </div>
                </div>

                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Plan Description<span class="star">*</span></label>
                  <div class="col-sm-10">
                    <textarea  name="description" id="description" class="form-control @if($errors->has('description')) help-block-text @endif" placeholder="Enter Plan Description">{{old('description')}}</textarea>
                     @if ($errors->has('description'))
                         <strong class="help-block"> {{ $errors->first('description') }}</strong>
                    @endif
                  </div>
                </div>

                  <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Status<span class="star">*</span></label>
                      <div class="col-sm-10">
                          <select class="form-control  @if($errors->has('status')) help-block @endif" id="status" name="status">
                              <option value="1">Active</option>
                              <option value="0">In Active</option>
                          </select>
                      </div>
                  </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
               <div class="col-sm-offset-1 col-sm-4"></div>
                <button type="submit" class="btn btn-info">Submit</button>
                <a href="{{route('subscription_list')}}"><button type="button" class="btn btn-danger">Cancle</button></a>
              </div>
              <!-- /.box-footer -->
            </form>
@endif
          </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

var geocoder = new google.maps.Geocoder();
var address = "noida";

geocoder.geocode( { 'address': address}, function(results, status) {

  if (status == google.maps.GeocoderStatus.OK) {
    var latitude = results[0].geometry.location.lat();
    var longitude = results[0].geometry.location.lng();
    alert(latitude);
  } 
}); 
</script>
@endsection

