@extends('admin_dash.design')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class=""><a href="{{route('subscription_list')}}">Edit User</a></li>
            </ol>
        </section>
        <style type="text/css">
            .time{
                width: 99%;
                margin-left: 1px !important;
            }
            .star{
                color: red;
            }
            .help-block{
                color: red;
                border-color: red;
                line-height: 0px;
            }
            .help-block-text{
                color: red;
                border-color: red;
            }
            .select2-container .select2-selection--single {
                height: 35px !important;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">
                    @if(Session::has('success'))
                        <div class="alert alert-success @if(Session::has('success')) {!! session('success') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('success') !!}
                        </div>
                    @endif
                    @if(Session::has('error_message'))
                        <div class="alert alert-danger @if(Session::has('error_message')) {!! session('error_message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('error_message') !!}
                        </div>
                    @endif
                    <div class="box" style="margin-top: 15px;">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                            </div>
                            <form action="{{route('update_customer',['id'=>$user->id])}}" method="POST" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">First Name<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('first_name')) help-block @endif" id="first_name" name="first_name" placeholder="Enter First Name" value="{{$user->first_name}}">
                                            @if ($errors->has('first_name'))
                                                <strong class="help-block"> {{ $errors->first('first_name') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Last Name<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('last_name')) help-block @endif" id="last_name" name="last_name" placeholder="Enter Last Name" value="{{$user->last_name}}">
                                            @if ($errors->has('last_name'))
                                                <strong class="help-block"> {{ $errors->first('last_name') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Email<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('email')) help-block @endif" id="email" name="email" placeholder="Enter Amount" value="{{$user->email}}">
                                            @if ($errors->has('email'))
                                                <strong class="help-block"> {{ $errors->first('email') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Password<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control @if($errors->has('password')) help-block @endif" id="password" name="password" placeholder="Enter Password" value="">
                                            @if ($errors->has('password'))
                                                <strong class="help-block"> {{ $errors->first('password') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Password Confirmation<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control @if($errors->has('password_confirmation')) help-block @endif" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password" value="">
                                            @if ($errors->has('password_confirmation'))
                                                <strong class="help-block"> {{ $errors->first('password_confirmation') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <div class="col-sm-offset-1 col-sm-4"></div>
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

