@extends('admin_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Subscription Plan List
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Subscription Plan List</li>
            </ol>
        </section>
        <style type="text/css">
            i.fa {
                font-size: 19px;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-3">
                    <a href="{{route('create_sub')}}">
                        <button type="button" class="btn bg-maroon btn-flat">
                            <i class="fa fa-edit"></i>Create Subscription Plan
                        </button>
                    </a>
                </div>
                <div class="col-xs-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title"></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-bordered w-100">
                                    <tbody>
                                    <tr>
                                        <th>Country</th>
                                        <th>Plan Name</th>
                                        <th>Visit Pass</th>
                                        <th>Duration</th>
                                        <th>Amount</th>
                                        <th style="width: 414px;">Description</th>
                                        <th>Status</th>
                                        <th>Created Date</th>
                                        <th>Action</th>
                                    </tr>

                                    @if(!empty($plan_list))
                                        @foreach($plan_list as $key=>$data)

                                            <tr>
                                                <td>{{$data->countryname->nicename}}</td>
                                                <td>{{$data->plan_category->plan_cat_name}}</td>
                                                <td>{{$data->visit_pass}}</td>
                                                <td>{{$data->plan_duration}}  {{$data->plan_time}}</td>
                                                <td>{{$data->amount}}</td>
                                                <td>{{$data->description}}</td>
                                                <td>
                                                    @if($data->status)
                                                        <a href="{{route('subscription-status',['id'=>$data->id,'status'=>0])}}" class='btn btn-success btn-xs' id='active'>Active</a>
                                                    @else
                                                        <a href="{{route('subscription-status',['id'=>$data->id,'status'=>1])}}" class='btn btn-danger btn-xs' id='in_active'>Inactive</a>
                                                    @endif

                                                    <span style="display: none">{{ ($data->status == 1) ? 'Active' : 'false' }}</span>
                                                </td>
                                                <td>{{date('d/m/y',strtotime($data->created_at))}}</td>
                                                <td>
                                                    <a href="{{route('update_sub',['id'=>$data->id])}}"><i class="fa fa-edit"></i></a>&nbsp;
                                                    <a href="{{route('delete_sub_plan',['id'=>$data->id])}}"><i class="fa fa-trash text-danger"></i></a>&nbsp;
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <ul class="pagination pagination-sm no-margin pull-right">
                                @if(!empty($plan_list))
                                    {{ $plan_list->links() }}
                                @endif
                            </ul>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

