@extends('admin_dash.design')
@section('content')
    @php  $url =  array_slice((explode('/',Request::url(),-1)), -1, true); @endphp
    @php  $status =  array_slice((explode('/',Request::url(),-2)), -1, true); @endphp
    @php  $status =  $status[0] @endphp
    @php  $url =  $url[0] @endphp

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Corporation Employee Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class=""><a href="#">Corporate-Employee-List</a></li>
                <li class="active">add-corporation-employee</li>
            </ol>
        </section>
        <style type="text/css">
            .time {
                width: 99%;
                margin-left: 1px !important;
            }

            .star {
                color: red;
            }

            .help-block {
                color: red;
                border-color: red;
                line-height: 0px;
            }

            .help-block-text {
                color: red;
                border-color: red;
            }

            .select2-container .select2-selection--single {
                height: 35px !important;
            }

            .location {
                margin-top: 10px;
            }

            .note {
                color: red;
            }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                background-color: #3c8dbc !important;
                border: 1px solid #3c8dbc !important;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif
                    @if($status==2)
                        <div class="box">
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Upload Employee CSV File.</h3>
                                </div>
                                <form action="{{route('upload_corporate_emplyee',$url)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    {{ csrf_field() }}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Sample CSV File<span class="star"></span></label>
                                            <div class="col-sm-9">
                                                <a href="{{asset('sample_csv_file.csv')}}" download="Sample CSV file.csv" class="btn btn-primary">View format</a>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">CSV File<span class="star">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control @if($errors->has('upload_corporate_employee')) help-block @endif" id="upload_gym_owner" name="upload_corporate_employee" placeholder="" value="">
                                                @if ($errors->has('upload_corporate_employee'))
                                                    <strong class="help-block"> {{ $errors->first('upload_corporate_employee') }}</strong>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="col-sm-offset-1 col-sm-4"></div>
                                            <button type="submit" name="submit" class="btn btn-info">Submit</button>
                                            <input type="hidden" name="submit" value="import">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endif
                    @if($status==1)

                        <div class="box">
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form action="{{route('store-corporation-employee',$url)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    {{ csrf_field() }}
                                    @php  $url1 =  array_slice((explode('/',Request::url())), -2, true); @endphp

                                    <input type="hidden" name="corporate_id" id="corporate_id" value="{{$url1[0]}}">
                                    <input type="hidden" name="is_email_verified" id="is_email_verified" value="1">
                                    <input type="hidden" name="is_corporate_user" id="is_corporate_user" value="1">
                                    <div class="box-body">


                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">First Name<span class="star">*</span></label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control @if($errors->has('first_name')) help-block @endif" id="first_name" name="first_name" value="{{ old('first_name') }}" placeholder="Enter first Name">
                                                @if ($errors->has('first_name'))
                                                    <strong class="help-block"> {{ $errors->first('first_name') }}</strong>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Last Name<span class="star">*</span></label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control @if($errors->has('last_name')) help-block @endif" id="last_name" name="last_name" placeholder="Enter Last Name" value="{{ old('last_name') }}" required>
                                                @if ($errors->has('last_name'))
                                                    <strong class="help-block"> {{ $errors->first('last_name') }}</strong>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Email<span class="star">*</span></label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control @if($errors->has('email')) help-block @endif" id="email" name="email" placeholder="Enter Email Address" value="{{ old('email') }}" required>
                                                @if ($errors->has('email'))
                                                    <strong class="help-block"> {{ $errors->first('email') }}</strong>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">City<span class="star"></span></label>
                                            <div class="col-sm-10">
                                                <select class="form-control select2 @if($errors->has('city')) help-block @endif" id="city" name="city">
                                                    @if(!empty($city))
                                                        <option value=""> City</option>
                                                        @foreach($city as $key=>$value)
                                                            @php $get = $value->name==old('city') ? "selected" :"";@endphp
                                                            <option value="{{$value->name}}" @php echo $get;@endphp>{{$value->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value=""> City Unavailable</option>
                                                    @endif
                                                </select>
                                                @if ($errors->has('city'))
                                                    <strong class="help-block"> {{ $errors->first('city') }}</strong>
                                                @endif
                                            </div>
                                        </div>


                                    </div>


                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                        <div class="col-sm-offset-1 col-sm-4"></div>
                                        <button type="submit" class="btn btn-info">Submit</button>
                                        <button type="reset" class="btn btn-danger">Reset</button>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                        </div>
                @endif
                <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script type="text/javascript">


        function planChange(v) {
            if (!v) {
                return false;
            }

            var url = "{{route('get-subscription')}}";
            data = {plan: v};
            $.ajax({
                url: url,
                data: data,
                success: function (response) {

                    html = '';
                    for (i = 0; i < response.length; i++) {
                        html += "<option value='" + response[i].id + "'>" + response[i].plan_duration + ' ' + response[i].plan_time + "</option>";
                    }
                    $("#subscription_id").html(html);
                }, error: function (err) {

                }

            });
        }

    </script>
@endsection
