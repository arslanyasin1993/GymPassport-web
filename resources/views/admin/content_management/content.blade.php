@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Content Management
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active"><a href="#">Content-Management</a></li>
      </ol>
   </section>
   <style type="text/css">
      .time{
      width: 99%;
      margin-left: 1px !important;
      }
      .star{
      color: red;
      }
      .help-block{
      color: red;
      border-color: red;
      line-height:16px;
      }
      .help-block-text{
      color: red;
      border-color: red;
      }
      .select2-container .select2-selection--single {
      height: 35px !important;
      }
   </style>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12">
           @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
            @endif
            <div class="nav-tabs-custom">
               <!-- Tabs within a box -->
               <ul class="nav nav-tabs pull-left" id="myTab">
                  <!-- <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li> -->
                  <li class="active"><a href="#aboutus" data-toggle="tab">About us</a></li>
                  <li><a href="#privacyPolicy" data-toggle="tab">Privacy Policy</a></li>
                   <li><a href="#refundPolicy" data-toggle="tab">Refund Policy</a></li>
                  <li><a href="#terms_condition" data-toggle="tab">Terms & Condition</a></li>
                  <li><a href="#HiW" data-toggle="tab">How it Works</a></li>
                  <li><a href="#use" data-toggle="tab">Usege Rules</a></li>
                  <li><a href="#faq" data-toggle="tab">FAQ</a></li>
               </ul>
               <div class="tab-content no-padding">
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane active" id="aboutus" >
                     <div class="box box-info">
                        <div class="box-body pad">
                          @if(empty($about_us))
                           <form action="{{route('add-content')}}" method="post">
                              {{ csrf_field() }}
                              <div class="row">
                                  <input type="hidden" name="type" value="1">
                                  <textarea id="about_us" name="about_us" rows="10" cols="80" required></textarea>
                                   @if ($errors->has('about_us'))
                                    <strong class="help-block"> {{ $errors->first('about_us') }}</strong>
                                  @endif
                              </div>
                               <div class="row">
                                   <div class="col-md-6">
                                       <label for="address_1">Address in Pakistan</label>
                                       <textarea id="address_1" name="address_1" rows="10" cols="80" required></textarea>
                                       @if ($errors->has('address_1'))
                                           <strong class="help-block"> {{ $errors->first('address_1') }}</strong>
                                       @endif
                                   </div>
                                   <div class="col-md-6">
                                       <label for="address_2">Address in Australia</label>
                                       <textarea id="address_2" name="address_2" rows="10" cols="80" required></textarea>
                                       @if ($errors->has('address_2'))
                                           <strong class="help-block"> {{ $errors->first('address_2') }}</strong>
                                       @endif
                                   </div>
                               </div>
                              <div class="box-footer">
                                 <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                           </form>
                          @else
                             <form action="{{route('update-content',['id'=>$about_us->id])}}" method="post">
                              {{ csrf_field() }}
                              <div class="row">
                                  <input type="hidden" name="type" value="1">
                                  <textarea id="about_us" name="about_us" rows="10" cols="80" required>{{$about_us->description}}</textarea>
                                  @if ($errors->has('about_us'))
                                      <strong class="help-block"> {{ $errors->first('about_us') }}</strong>
                                  @endif
                              </div>
                              <div class="row">
                                  <div class="col-md-6">
                                      <label for="address_1">Address in Pakistan</label>
                                      <textarea id="address_1" name="address_1" rows="10" cols="80" required>{{$about_us->address_1}}</textarea>
                                      @if ($errors->has('address_1'))
                                          <strong class="help-block"> {{ $errors->first('address_1') }}</strong>
                                      @endif
                                  </div>
                                  <div class="col-md-6">
                                      <label for="address_2">Address in Australia</label>
                                      <textarea id="address_2" name="address_2" rows="10" cols="80" required>{{$about_us->address_2}}</textarea>
                                      @if ($errors->has('address_2'))
                                          <strong class="help-block"> {{ $errors->first('address_2') }}</strong>
                                      @endif
                                  </div>
                              </div>
                              <div class="box-footer">
                                 <button type="submit" class="btn btn-primary">Update</button>
                              </div>
                           </form>
                          @endif
                        </div>
                     </div>
                  </div>
                  <div class="chart tab-pane" id="privacyPolicy">
                     <div class="box box-info">
                        <div class="box-body pad">
                          @if(empty($privacy))
                           <form action="{{route('add-content')}}" method="post">
                              {{ csrf_field() }}
                              <input type="hidden" name="type" value="2">
                              <textarea id="privacy_policy" name="privacy_policy" rows="10" cols="80" required>
                              </textarea>
                               @if ($errors->has('privacy_policy'))
                              <strong class="help-block"> {{ $errors->first('privacy_policy') }}</strong>
                            @endif
                               <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                           </form>
                          @else
                               <form action="{{route('update-content',['id'=>$privacy->id])}}" method="post">
                              {{ csrf_field() }}
                              <input type="hidden" name="type" value="2">
                              <textarea id="privacy_policy" name="privacy_policy" rows="10" cols="80" required>
                                {{$privacy->description}}
                              </textarea>
                               @if ($errors->has('privacy_policy'))
                              <strong class="help-block"> {{ $errors->first('privacy_policy') }}</strong>
                            @endif
                               <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Update</button>
                              </div>
                           </form>
                          @endif
                          
                        </div>
                     </div>
                  </div>
                   <div class="chart tab-pane" id="refundPolicy">
                       <div class="box box-info">
                           <div class="box-body pad">
                               @if(empty($refund))
                                   <form action="{{route('add-content')}}" method="post">
                                       {{ csrf_field() }}
                                       <input type="hidden" name="type" value="2">
                                       <textarea id="refund_policy" name="refund_policy" rows="10" cols="80" required>
                              </textarea>
                                       @if ($errors->has('refund_policy'))
                                           <strong class="help-block"> {{ $errors->first('refund_policy') }}</strong>
                                       @endif
                                       <div class="box-footer">
                                           <button type="submit" class="btn btn-primary">Submit</button>
                                       </div>
                                   </form>
                               @else
                                   <form action="{{route('update-content',['id'=>$refund->id])}}" method="post">
                                       {{ csrf_field() }}
                                       <input type="hidden" name="type" value="3">
                                       <textarea id="refund_policy" name="refund_policy" rows="10" cols="80" required>
                                {{$refund->description}}
                              </textarea>
                                       @if ($errors->has('refund_policy'))
                                           <strong class="help-block"> {{ $errors->first('refund_policy') }}</strong>
                                       @endif
                                       <div class="box-footer">
                                           <button type="submit" class="btn btn-primary">Update</button>
                                       </div>
                                   </form>
                               @endif

                           </div>
                       </div>
                   </div>
                  <div class="chart tab-pane" id="terms_condition" >
                     <div class="box box-info">
                        <div class="box-body pad">
                        @if(empty($terms_conditions))
                           <form action="{{route('add-content')}}" method="post">
                              {{ csrf_field() }}
                              <input type="hidden" name="type" value="5">
                              <textarea id="terms_conditions" name="terms_conditions" rows="10" cols="80" required>
                               </textarea>
                                @if ($errors->has('terms_conditions'))
                                   <strong class="help-block"> {{ $errors->first('terms_conditions') }}</strong>
                                @endif
                              <div class="box-footer">
                                 <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                           </form>
                        @else
                           <form action="{{route('update-content',['id'=>$terms_conditions->id])}}" method="post">
                              {{ csrf_field() }}
                              <input type="hidden" name="type" value="5">
                              <textarea id="terms_conditions" name="terms_conditions" rows="10" cols="80" required>
                                {{$terms_conditions->description}}
                               </textarea>
                                @if ($errors->has('terms_conditions'))
                                   <strong class="help-block"> {{ $errors->first('terms_conditions') }}</strong>
                                @endif
                              <div class="box-footer">
                                 <button type="submit" class="btn btn-primary">Update</button>
                              </div>
                           </form>
                        @endif
                        </div>
                     </div>
                  </div>
                  <div class="chart tab-pane" id="HiW" >
                     <div class="box-body pad">
                      @if(empty($hiw))
                        <form action="{{route('add-content')}}" method="post">
                           {{ csrf_field() }}
                           <input type="hidden" name="type" value="4">
                           <textarea id="how_its_work" name="how_its_work" rows="10" cols="80" required>
                           </textarea>
                            @if ($errors->has('how_its_work'))
                              <strong class="help-block"> {{ $errors->first('how_its_work') }}</strong>
                            @endif
                           <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Submit</button>
                           </div>
                        </form>
                      @else
                      <form action="{{route('update-content',['id'=>$hiw->id])}}" method="post">
                           {{ csrf_field() }}
                           <input type="hidden" name="type" value="4">
                           <textarea id="how_its_work" name="how_its_work" rows="10" cols="80" required>
                             {{$hiw->description}}
                           </textarea>
                            @if ($errors->has('how_its_work'))
                              <strong class="help-block"> {{ $errors->first('how_its_work') }}</strong>
                            @endif
                           <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Update</button>
                           </div>
                        </form>
                      @endif
                     </div>
                  </div>
                   <div class="chart tab-pane" id="use" >
                     <div class="box-body pad">
                      @if(empty($usege_rule))
                           <form action="{{route('add-content')}}" method="post">
                           {{ csrf_field() }}
                           <input type="hidden" name="type" value="6">

                           <textarea id="use_rule" name="use_rule" rows="10" cols="80" required>
                           </textarea>
                            @if ($errors->has('use_rule'))
                              <strong class="help-block"> {{ $errors->first('use_rule') }}</strong>
                            @endif
                           <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Submit</button>
                           </div>
                        </form>
                  @else
                      <form action="{{route('update-content',['id'=>$usege_rule->id])}}" method="post">
                           {{ csrf_field() }}
                           <input type="hidden" name="type" value="6">

                           <textarea id="use_rule" name="use_rule" rows="10" cols="80" required>
                            {{$usege_rule->description}}
                           </textarea>
                            @if ($errors->has('use_rule'))
                              <strong class="help-block"> {{ $errors->first('use_rule') }}</strong>
                            @endif
                           <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Update</button>
                           </div>
                        </form>
                  @endif
                     </div>
                  </div>
                  <div class="chart tab-pane" id="faq" >
                     <div class="box-body pad">
                    @if(empty($faq))
                          <form action="{{route('add-content')}}" method="post">
                           {{ csrf_field() }}
                           <input type="hidden" name="type" value="7">
                           <textarea id="faq_data" name="faq_data" rows="10" cols="80" required>
                           </textarea>
                            @if ($errors->has('faq_data'))
                              <strong class="help-block"> {{ $errors->first('faq_data') }}</strong>
                            @endif
                           <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Submit</button>
                           </div>
                        </form>
                    @else
                      <form action="{{route('update-content',['id'=>$faq->id])}}" method="post">
                           {{ csrf_field() }}
                           <input type="hidden" name="type" value="7">
                           <textarea id="faq_data" name="faq_data" rows="10" cols="80" required>
                            {{$faq->description}}
                           </textarea>
                            @if ($errors->has('faq_data'))
                              <strong class="help-block"> {{ $errors->first('faq_data') }}</strong>
                            @endif
                           <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Update</button>
                           </div>
                        </form>
                    @endif
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- /.box -->
      </div>
   </section>
   <!-- /.content -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
  });
  var activeTab = localStorage.getItem('activeTab');
  if(activeTab){
    $('#myTab a[href="' + activeTab + '"]').tab('show');
  }
});
</script>
<!--<script src="{{ asset('css/bower_components/ckeditor/ckeditor.js')}}"></script>-->
<script src="{{ asset('ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
   CKEDITOR.replace("about_us");
   CKEDITOR.replace("address_1");
   CKEDITOR.replace("address_2");
   CKEDITOR.replace("privacy_policy");
   CKEDITOR.replace("refund_policy");
   CKEDITOR.replace("terms_conditions");
   CKEDITOR.replace("how_its_work");
   CKEDITOR.replace("use_rule");
   CKEDITOR.replace("faq_data");
</script>
@endsection