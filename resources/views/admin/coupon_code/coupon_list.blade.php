@extends('admin_dash.design')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Coupon List</h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Gym-List</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-2" style="margin-bottom: 5px;">
                    <a href="{{route('add_coupon')}}">
                        <button type="button" class="btn btn-block btn-info"><i class="fa fa-edit"></i> Add Coupon</button>
                    </a>
                </div>
                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active" id="Request">
                                <div class="box-header with-border">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <!-- <input type='text' class="form-control" id='searchdata' placeholder='Search By First Name ,Last Name,Email'> -->
                                            </div>
                                            <label for="inputEmail3" class="col-sm-1 control-label" style="width: 98px;">From-Date</label>
                                            <div class="col-sm-2">
                                                <input type='date' class="form-control" id='startdate_first' placeholder='From Date'>
                                            </div>
                                            <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                            <div class="col-sm-2">
                                                <input type="date" name="" id="enddate_first" class="form-control" placeholder="End Date">
                                            </div>
                                            <div class="col-sm-1">
                                                <button class="btn btn-primary" id="submit_first">Apply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="coupon_list" class="table table-bordered table-striped w-100">
                                            <thead>
                                                <tr>
                                                    <th>Coupon Code</th>
                                                    <th>Discount Percentage</th>
                                                    <th>Valid From</th>
                                                    <th>Valid To</th>
                                                    <th>Description</th>
                                                    <th>Created Date</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                        localStorage.setItem('activeTab', $(e.target).attr('href'));
                    });
                    var activeTab = localStorage.getItem('activeTab');
                    if (activeTab) {
                        $('#myTab a[href="' + activeTab + '"]').tab('show');
                    }
                });
            </script>
            <script type="text/javascript">
                var dataTable_first;
                $(document).ready(function () {
                    dataTable_first = $('#coupon_list').DataTable({
                        'processing': true,
                        'serverSide': true,
                        // 'searching': false, // Remove default Search Control
                        'ajax': {
                            'url': "{{route('ajax_coupon_list')}}",
                            'data': function (data) {
                                // Read values
                                var enddate = $('#enddate_first').val();
                                var startdate = $('#startdate_first').val();
                                //var searchdata = $('#searchdata').val();

                                // Append to data
                                data.startdate = startdate;
                                data.enddate = enddate;
                                //data.searchdata = searchdata;
                            }
                        },
                        'columns': [
                            /* { data: 'plan_cat_name' }, */
                            {data: 'coupon_code'},
                            {data: 'discount'},
                            {data: 'valid_from'},
                            {data: 'valid_to'},
                            {data: 'description'},
                            {data: 'created_at'},
                            {data: 'delete'},
                        ],
                        "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000 , 5000]],
                    });

                    $('#searchdata').keyup(function () {
                        dataTable_first.draw();
                    });

                    $('#submit_first').click(function () {
                        dataTable_first.draw();
                    });
                });

                $(document).on('click' , '#delete_coupon' , function (){
                    var url = $(this).attr('data-href');
                    $.ajax({
                        url: url,
                        type: 'GET',
                        success: function (response){
                            if(response){
                                dataTable_first.draw();
                                toastr.success(response.message);
                            }else{
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            </script>
            <script type="text/javascript">
                $(function () {
                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear();
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    $('#startdate').attr('max', maxDate);
                    $('#enddate').attr('max', maxDate);
                    $('#startdate_first').attr('max', maxDate);
                    $('#enddate_first').attr('max', maxDate);
                });
            </script>
        </section>
    </div>
@endsection

