@extends('admin_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
              {{$corporateName}}  Employee Check In List
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Employee Check In List</li>
            </ol>
        </section>
        <style type="text/css">
            i.fa {
                font-size: 19px;
            }

            .plan {
                font-weight: bold;
                float: right;
                margin-bottom: 14px;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-xs-12">

                    <div class="box">

                        <div class="box-header with-border">
                            <div class="col-sm-12">
                                <!-- <div class="col-sm-4"></div> -->
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <!-- <input type='text' class="form-control" id='searchdata' placeholder='Search By First Name ,Last Name,Email'> -->
                                    </div>
                                    <label for="inputEmail3" class="col-sm-1 control-label" style="width: 98px;">From-Date</label>
                                    <div class="col-sm-2">
                                        <input type='date' class="form-control" value="{{ request()->startDate }}" id='startdate' placeholder='From Date'>
                                    </div>
                                    <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                    <div class="col-sm-2">
                                        <input type="date" name="" id="enddate" value="{{ request()->endDate }}" class="form-control" placeholder="End Date">
                                    </div>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary" id="submit">Apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <style type="text/css">
                            img.user_image {
                                height: auto;
                                width: 79px;
                                border-radius: 15px;
                            }
                        </style>
                        <div class="box-body">
                            <div class="table-responsive">
                            <table id="dataTable-example" class="table table-bordered table-striped w-100">
                                <thead>
                                <tr>
                                    <th>Employee name</th>
                                    <th>Employee email</th>
                                    <th>Check-in Date</th>
                                    <th>Check-in Time</th>
                                    <th>Gym name</th>
                                    <th>Gym Earn Amount</th>
                                    <th>User Plan</th>
                                    <th>Gym Plan</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td style="font-weight: bold;">Total Rvenue</td>
                                        <td colspan="4" style="font-weight: bold; text-align: end"></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script type="text/javascript">
                var dataTable;
                $(document).ready(function () {
                    var corporateId = "<?php echo $_GET['corporateId']; ?>";
                    dataTable = $('#dataTable-example').DataTable({
                        'processing': true,
                        'serverSide': true,
                        // 'searching': false, // Remove default Search Control
                        'ajax': {
                            'url': "{{route('corporate-employee-checkin-list')}}"+'/'+corporateId,
                            'data': function (data) {
                                // Read values
                                var enddate = $('#enddate').val();
                                var startdate = $('#startdate').val();
                                //var searchdata = $('#searchdata').val();

                                // Append to data
                                data.startdate = startdate;
                                data.enddate = enddate;
                                //data.searchdata = searchdata;
                            }
                        },
                        'columns': [
                            {data: 'username'},
                            {data: 'email'},
                            {data: 'check_in_date'},
                            {data: 'check_in_time'},
                            {data: 'gym_name'},
                            {data: 'amount'},
                            {data: 'plan_name'},
                            {data: 'gym_plan_name'},
                            {data: 'action'},

                        ],
                        "footerCallback": function( row, data, start, end, display ) {
                            var api = this.api();
                            $( api.column( 1 ).footer() ).html(api.ajax.json().revenue); // get the totalAmount from the JSON response
                        },
                        dom: 'Bfrtip',
                        order: [[2, "desc"]],
                        buttons: [
                            'csv', 'pdf', 'pageLength'
                        ],
                        "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000 , 5000]],
                        colReorder: true,
                    });

                    $('#searchdata').keyup(function () {
                        dataTable.draw();
                    });

                    $('#submit').click(function () {
                        dataTable.draw();
                    });
                });

            </script>
            <script type="text/javascript">
                $(function () {
                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear();
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    $('#startdate').attr('max', maxDate);
                    $('#enddate').attr('max', maxDate);
                });

                $(document).on('click' , '.del_btn' , function (e){
                    e.preventDefault();
                    if (confirm("Are you sure to delete this record?")){
                        var url = $(this).attr('data-url');
                        $.ajax({
                            url: url,
                            type: 'GET',
                            success: function (response){
                                if(response.status == true){
                                    dataTable.draw();
                                    toastr.success(response.message);
                                }else{
                                    toastr.success(response.message);
                                }
                            }
                        });
                    }
                })
            </script>
        </section>
        <!-- /.content -->
    </div>
@endsection

