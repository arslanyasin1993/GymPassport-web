@extends('admin_dash.design')
@section('content')
    <style>
        table.dataTable thead > tr > th.sorting_asc, table.dataTable thead > tr > th.sorting_desc, table.dataTable thead > tr > th.sorting, table.dataTable thead > tr > td.sorting_asc, table.dataTable thead > tr > td.sorting_desc, table.dataTable thead > tr > td.sorting {
            padding-right: 0px !important;
        }

        img.user_image {
            height: auto;
            width: 79px;
            border-radius: 15px;
        }
    </style>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Corporate Management</h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Corporate-List</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                @if(Session::has('message'))
                    <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {!! session('msg') !!}
                    </div>
                @endif

                <div class="col-xs-2" style="    margin-bottom: 5px;">
                    <a href="{{route('add_corporation')}}">
                        <button type="button" class="btn btn-block btn-info"><i class="fa fa-edit"></i> Create Corporation</button>
                    </a>
                </div>

                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active">
                                <div class="box-header with-border">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <!-- <input type='text' class="form-control" id='searchdata' placeholder='Search By First Name ,Last Name,Email'> -->
                                            </div>
                                            <label for="inputEmail3" class="col-sm-1 control-label" style="width: 98px;">From-Date</label>
                                            <div class="col-sm-2">
                                                <input type='date' class="form-control" id='startdate' placeholder='From Date'>
                                            </div>
                                            <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                            <div class="col-sm-2">
                                                <input type="date" name="" id="enddate" class="form-control" placeholder="End Date">
                                            </div>
                                            <div class="col-sm-1">
                                                <button class="btn btn-primary" id="submit">Apply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example_registered" class="table table-bordered table-striped w-100">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Company</th>
                                                    <th>Email</th>
                                                    <th>Total Employees</th>
                                                    <th>Registered Employees</th>
                                                    <th>Active Employees</th>
                                                    <th>Subscribed</th>
                                                    <th>Monthly invoice</th>
                                                    <th>Status Updated At</th>
                                                    <th>Created At</th>
                                                    <th>Status</th>
                                                    <th>Login</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                        localStorage.setItem('activeTab', $(e.target).attr('href'));
                    });
                    var activeTab = localStorage.getItem('activeTab');
                    if (activeTab) {
                        $('#myTab a[href="' + activeTab + '"]').tab('show');
                    }
                });
            </script>

            <script type="text/javascript">
                var dataTable;
                $(document).on('click', '.btn_no_subscription', function () {
                    alert("Subscribe a plan first");
                });

                $(document).ready(function () {
                    dataTable = $('#example_registered').DataTable({
                        'processing': true,
                        'serverSide': true,
                        // 'searching': false, // Remove default Search Control
                        'ajax': {
                            'url': "{{route('corporate_list')}}",
                            'data': function (data) {
                                // Read values
                                var enddate = $('#enddate').val();
                                var startdate = $('#startdate').val();
                                var searchdata = $('#searchdata').val();

                                // Append to data
                                data.startdate = startdate;
                                data.enddate = enddate;
                                data.searchdata = searchdata;
                            }
                        },
                        'columns': [

                            {data: 'username'},
                            {data: 'corporate_name'},
                            {data: 'email'},
                            {data: 'no_of_employee'},
                            {data: 'employees'},
                            {data: 'active_employees'},
                            {data: 'subscribed'},
                            {data: 'monthly_invoice'},
                            {data: 'status_updated_at'},
                            {data: 'created_at'},
                            {data: 'status'},
                            {data: 'login'},
                            {data: 'edit'},
                            {data: 'delete'}
                        ],
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'excel', 'pdf', 'pageLength'
                        ],
                        "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000 , 5000]],
                    });

                    $('#searchdata').keyup(function () {
                        dataTable.draw();
                    });

                    $('#submit').click(function () {
                        dataTable.draw();
                    });
                });

                $(document).on('click' , '.change_corporate_status' , function (e){
                    e.preventDefault();
                    if (confirm("Are you sure to delete this record?")){
                        var url = $(this).attr('data-href');
                        $.ajax({
                            url: url,
                            type: 'GET',
                            success: function (response){
                                if(response){
                                    dataTable.draw();
                                    toastr.success(response.message);
                                }else{
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                $(document).on('click' , '#delete_corporate' , function (){
                    var url = $(this).attr('data-href');
                    $.ajax({
                        url: url,
                        type: 'GET',
                        success: function (response){
                            if(response){
                                dataTable.draw();
                                toastr.success(response.message);
                            }else{
                                toastr.error(response.message);
                            }
                        }
                    });
                });
            </script>
            <script type="text/javascript">
                $(function () {
                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear();
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    $('#startdate').attr('max', maxDate);
                    $('#enddate').attr('max', maxDate);
                    $('#startdate_first').attr('max', maxDate);
                    $('#enddate_first').attr('max', maxDate);
                });
            </script>
        </section>
    </div>
@endsection

