@extends('admin_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Corporation Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class=""><a href="#">Corporate-List</a></li>
                <li class="active">add-corporation</li>
            </ol>
        </section>
        <style type="text/css">
            .time {
                width: 99%;
                margin-left: 1px !important;
            }

            .star {
                color: red;
            }

            .help-block {
                color: red;
                border-color: red;
                line-height: 0px;
            }

            .help-block-text {
                color: red;
                border-color: red;
            }

            .select2-container .select2-selection--single {
                height: 35px !important;
            }

            .location {
                margin-top: 10px;
            }

            .note {
                color: red;
            }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                background-color: #3c8dbc !important;
                border: 1px solid #3c8dbc !important;
            }
            .rediobox{
                padding: 0px 0px 0px 12px;
            }
            .time{
                width: 99%;
                margin-left: 1px !important;
            }
            .star{
                color: red;
            }
            .help-block{
                color: red;
                border-color: red;
                line-height: 0px;
            }
            .help-block-text{
                color: red;
                border-color: red;
            }
            .select2-container .select2-selection--single {
                height: 35px !important;
            }
            .location{
                margin-top: 10px;
            }
            .note{
                color: red;
            }
            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                background-color: #3c8dbc !important;
                border: 1px solid #3c8dbc !important;
            }
            .choose_input {
                opacity: 0;
                z-index: 1000;
                position: absolute;
                left: 3px;
                /* bottom: 10px; */
                height: 30px;
                width: 100%;
            }
            .upload_span {
                position: absolute;
                right: 0;
                cursor: pointer;
                width: 100%;
                margin-top: 6px;
                /* height: 10px; */
                background-color: #d7d7d7;
                border-radius: 4px;
            }

            .imageDiv {
                width: 220px;
                height: 200px;
                /* height: 180px; */
                /* margin: auto auto 35px auto; */
                margin: auto auto 35px auto;
                position: relative;
            }

            .imageDiv img {
                max-width: 100%;
                max-height: 100%;
                border-radius: 4px;
                width: 98%;
                height: 95%;
                /*max-width: 320px;*/
                /*max-height: 320px;*/
                background-color: #f2f2f2;
            }

            .photoUpload:hover {
                pointer-events: auto !important;
                cursor: pointer !important;
                color: #6B943C;
            }

            .image_remove {
                position: absolute;
                top: 0;
                right: 2px;
                /*float: right;*/
                opacity: 0.8;
                display: none;
            }

            .overlay {
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                height: 100%;
                width: 100%;
                opacity: 0;
                transition: .3s ease;
                background-color: transparent;
                border-radius: 50%;

            }

            .imageDiv:hover .overlay {
                opacity: 1;
            }


            .custom-file input[type='file'] {
                display: none
            }

            .custom-file label {
                cursor: pointer;
                color: #000;
                text-align: center;
                display: table;
                margin: auto;
                margin-top: 20px;
            }

            .photoUpload strong.help-block {
                position: absolute;
                bottom: -45px;
                left: 1px;
            }

            .photoUpload span.responseError {
                position: absolute;
                bottom: -45px;
                left: 1px;
            }
            .photoUpload span.help-block {
                position: absolute;
                bottom: -45px;
                left: 1px;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="{{route('store-corporation')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{ csrf_field() }}

                            <input type="hidden" name="is_email_verified" value="1">
                                <div class="box-body">

                                    <div class="gray-bg imageDiv">
                                        <a type="button" class="btn btn-danger btn-sm image_remove">
                                            <i class="fa fa-trash text-white"></i>
                                        </a>
                                        <img src="{{asset('placeholder.png')}}" alt="Blog">

                                        <div class="photoUpload">
                                                <span class="upload_span">
                                                    <i class="fa fa-upload mr-2"></i>
                                                    Upload Corporate Logo
                                                </span>
                                            <input type="file" name="image" id="image" value="" onchange="imagePreview(this);" class="choose_input @if($errors->has('image')) help-block @endif" accept="image/png,image/jpg,image/jpeg">
                                            @if ($errors->has('image'))
                                                <strong class="help-block"> {{ $errors->first('image') }}</strong>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="form-group mt-5">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Name<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('first_name')) help-block @endif" id="first_name" name="first_name" value="{{ old('first_name') }}" placeholder="Enter Corporate Name">
                                            @if ($errors->has('first_name'))
                                                <strong class="help-block"> {{ $errors->first('first_name') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Company<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('corporate_name')) help-block @endif" id="corporate_name" name="corporate_name" placeholder="Enter Company Name" value="{{ old('corporate_name') }}">
                                            @if ($errors->has('corporate_name'))
                                                <strong class="help-block"> {{ $errors->first('corporate_name') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Employees<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('no_of_employees')) help-block @endif" id="no_of_employees" name="no_of_employees" placeholder="Enter No of Employees" value="{{ old('no_of_employees') }}">
                                            @if ($errors->has('no_of_employees'))
                                                <strong class="help-block"> {{ $errors->first('no_of_employees') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Per employee rate<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control @if($errors->has('per_employee_rate')) help-block @endif" id="per_employee_rate" name="per_employee_rate" placeholder="Per Employee Rate" value="{{ old('per_employee_rate') }}">
                                            @if ($errors->has('per_employee_rate'))
                                                <strong class="help-block">{{ $errors->first('per_employee_rate') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Email<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control @if($errors->has('email')) help-block @endif" id="email" name="email" placeholder="Enter Email Address" value="{{ old('email') }}">
                                            @if ($errors->has('email'))
                                                <strong class="help-block"> {{ $errors->first('email') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Password<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control @if($errors->has('password')) help-block @endif" id="password" name="password" placeholder="Enter password" value="{{ old('password') }}">

                                            @if ($errors->has('password'))
                                                <strong class="help-block"> {{ $errors->first('password') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Phone Number<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control @if($errors->has('phone_number')) help-block @endif" id="phone_number" name="phone_number" placeholder="Enter Phone Number" value="{{ old('phone_number') }}">
                                            @if ($errors->has('phone_number'))
                                                <strong class="help-block"> {{ $errors->first('phone_number') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Address<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control @if($errors->has('address')) help-block-text @endif" name="address" id="address" placeholder="Enter address">{{ old('address') }}</textarea>
                                            @if ($errors->has('address'))
                                                <strong class="help-block"> {{ $errors->first('address') }}</strong>
                                            @endif

                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Plan<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2 @if($errors->has('plan_id')) help-block @endif" onchange="planChange(this.value)" id="plan_id" name="plan_id">
                                                @if(!empty($city))
                                                    <option value=""> Plan</option>
                                                    @foreach($plan as $key=>$value)
                                                        @php $get = $value->plan_cat_name==old('plan_id') ? "selected" :"";@endphp
                                                        <option value="{{$value->id}}" @php echo $get;@endphp>{{$value->plan_cat_name}}</option>
                                                    @endforeach
                                                @else
                                                    <option value=""> Plan Unavailable</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('plan_id'))
                                                <strong class="help-block"> {{ $errors->first('plan_id') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Subscription<span class="star">*</span></label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2 @if($errors->has('subscription_id')) help-block @endif" id="subscription_id" name="subscription_id">
                                                <option value=""> Select Subscription</option>
                                            </select>
                                            @if ($errors->has('subscription_id'))
                                                <strong class="help-block"> {{ $errors->first('subscription_id') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                </div>


                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                    <div class="col-sm-offset-1 col-sm-4"></div>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script type="text/javascript">

        // Show selected Image
        function imagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(input).closest('.imageDiv').find('img').attr('src', e.target.result);
                    // $(input).closest('.imageDiv').find('a').removeClass('hidden');
                    $(input).closest('.imageDiv').find('a').css('display', 'block');

                }

                reader.readAsDataURL(input.files[0]);
            } else {
                $(input).closest('.imageDiv').find('img').attr('src', 'placeholder.png');
            }
        }

        function planChange(v) {
            if (!v) {
                return false;
            }

            var url = "{{route('get-subscription')}}";
            data = {plan: v};
            $.ajax({
                url: url,
                data: data,
                success: function (response) {

                    html = '';
                    for (i = 0; i < response.length; i++) {
                        html += "<option value='" + response[i].id + "'>" + response[i].plan_duration + ' ' + response[i].plan_time + "</option>";
                    }
                    $("#subscription_id").html(html);
                }, error: function (err) {

                }

            });
        }

    </script>
@endsection
