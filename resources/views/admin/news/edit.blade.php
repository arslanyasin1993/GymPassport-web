@extends('admin_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit News
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class=""><a href="{{route('news')}}">News-List</a></li>
                <li class="active">Edit-News</li>
            </ol>
        </section>
        <style type="text/css">
            input[type=radio] {
                height: 19px;
                width: 100%;
            }
            .rediobox{
                padding: 0px 0px 0px 12px;
            }
            .time{
                width: 99%;
                margin-left: 1px !important;
            }
            .star{
                color: red;
            }
            .help-block{
                color: red;
                border-color: red;
                line-height: 0px;
            }
            .help-block-text{
                color: red;
                border-color: red;
            }
            .select2-container .select2-selection--single {
                height: 35px !important;
            }
            .location{
                margin-top: 10px;
            }
            .note{
                color: red;
            }
            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                background-color: #3c8dbc !important;
                border: 1px solid #3c8dbc !important;
            }
            .choose_input {
                opacity: 0;
                z-index: 1000;
                position: absolute;
                left: 3px;
                /* bottom: 10px; */
                height: 30px;
                width: 100%;
            }
            .upload_span {
                position: absolute;
                right: 0;
                cursor: pointer;
                width: 100%;
                margin-top: 6px;
                /* height: 10px; */
                background-color: #d7d7d7;
                border-radius: 4px;
            }

            .imageDiv {
                width: 220px;
                height: 200px;
                /* height: 180px; */
                /* margin: auto auto 35px auto; */
                margin: auto auto 0px auto;
                position: relative;
            }

            .imageDiv img {
                max-width: 100%;
                max-height: 100%;
                border-radius: 4px;
                width: 98%;
                height: 95%;
                /*max-width: 320px;*/
                /*max-height: 320px;*/
                background-color: #f2f2f2;
            }

            .photoUpload:hover {
                pointer-events: auto !important;
                cursor: pointer !important;
                color: #6B943C;
            }

            .image_remove {
                position: absolute;
                top: 0;
                right: 2px;
                /*float: right;*/
                opacity: 0.8;
                display: none;
            }

            .overlay {
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                height: 100%;
                width: 100%;
                opacity: 0;
                transition: .3s ease;
                background-color: transparent;
                border-radius: 50%;

            }

            .imageDiv:hover .overlay {
                opacity: 1;
            }


            .custom-file input[type='file'] {
                display: none
            }

            .custom-file label {
                cursor: pointer;
                color: #000;
                text-align: center;
                display: table;
                margin: auto;
                margin-top: 20px;
            }

            .photoUpload strong.help-block {
                position: absolute;
                bottom: -45px;
                left: 1px;
            }

            .photoUpload span.responseError {
                position: absolute;
                bottom: -45px;
                left: 1px;
            }
            .photoUpload span.help-block {
                position: absolute;
                bottom: -45px;
                left: 1px;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif
                    <div class="box">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="{{route('news-update',$news->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}
                                <div class="row justify-content-center mb-5">
                                    <div class="form-group text-center">
                                        {{--                                    <span class="text-info small-text p-1" style="background-color: #f4f5f7">* Image dimension 1920 x 1380</span>--}}

                                        <div class="gray-bg imageDiv">
                                            <a type="button" class="btn btn-danger btn-sm image_remove">
                                                <i class="fa fa-trash text-white"></i>
                                            </a>
                                            @if($news->image)
                                                <img src="{{asset($news->image)}}" alt="Profile">
                                            @else
                                                <img src="{{asset('placeholder.png')}}" alt="Profile">
                                            @endif

                                            <div class="photoUpload">
                                                <span class="upload_span">
                                                    <i class="fa fa-upload mr-2"></i>
                                                    Upload News Image
                                                </span>
                                                <input type="file" name="image" id="image" value="" onchange="imagePreview(this);" class="choose_input @if($errors->has('image')) help-block @endif" accept="image/png,image/jpg,image/jpeg">
                                                @if ($errors->has('image'))
                                                    <strong class="help-block"> {{ $errors->first('image') }}</strong>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Author <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control @if($errors->has('author')) help-block @endif" name="author" id="author" value="{{ $news->author }}" placeholder="John Doe">
                                            @if ($errors->has('author'))
                                                <strong class="help-block"> {{ $errors->first('author') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Title <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control item-name @if($errors->has('title')) help-block @endif" name="title" id="title" value="{{ $news->title }}" placeholder="Be a Great Human">
                                            @if ($errors->has('title'))
                                                <strong class="help-block"> {{ $errors->first('title') }}</strong>
                                            @endif
                                        </div>
                                    </div>
{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-sm-3 control-label">Slug<span class="text-danger">*</span></label>--}}
{{--                                        <div class="col-sm-9">--}}
                                            <input type="hidden" class="form-control @if($errors->has('slug')) help-block @endif" name="slug" id="slug" value="{{ $news->slug }}" placeholder="Slug">
{{--                                            @if ($errors->has('slug'))--}}
{{--                                                <strong class="help-block"> {{ $errors->first('slug') }}</strong>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Description <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control ckeditor @if($errors->has('description')) help-block @endif" name="description" id="description">{{$news->description}}</textarea>
                                            @if ($errors->has('description'))
                                                <strong class="help-block"> {{ $errors->first('description') }}</strong>
                                            @endif

                                        </div>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                    <div class="col-sm-offset-1 col-sm-4"></div>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('ckeditor/ckeditor.js')}}"></script>
    <script>
        CKEDITOR.replace( 'editor1' );
        // Show selected Image
        function imagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(input).closest('.imageDiv').find('img').attr('src', e.target.result);
                    // $(input).closest('.imageDiv').find('a').removeClass('hidden');
                    $(input).closest('.imageDiv').find('a').css('display', 'block');

                }

                reader.readAsDataURL(input.files[0]);
            } else {
                $(input).closest('.imageDiv').find('img').attr('src', 'placeholder.png');
            }
        }

        $('.item-name').on('keyup', function () {
            let $this = $(this);
            let str = $this.val().replace(/[0-9`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '-').replace(/ /g, '-');
            $('#slug').val(str);
        });
    </script>
@endsection
