@extends('admin_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                News List
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">News-List</li>
            </ol>
        </section>
        <style type="text/css">
            img.user_image {
                height: auto;
                width: 79px;
                border-radius: 15px;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-2" style="    margin-bottom: 5px;">
                    <a href="{{route('add-news')}}">
                        <button type="button" class="btn btn-block btn-info"><i class="fa fa-edit"></i> Add News
                        </button>
                    </a>
                </div>
                <div class="col-xs-12">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif
                    <div class="nav-tabs-custom">
                        <div class="tab-content no-padding">
                            <div class="chart tab-pane active" id="Request">
                                <div class="box-header with-border">
                                    <div class="col-sm-12">
                                        <!-- <div class="col-sm-4"></div> -->
                                        <div class="form-group">
                                            <div class="row filters-section mt-3 mb-5 align-items-end">
                                                <div class="col-md-3">
                                                    <label for="">Active</label>
                                                    <select class="form-control" name="status" id="inputStateRes">
                                                        <option value="All">All</option>
                                                        <option value="Active">Active</option>
                                                        <option value="InActive">In Active</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="coupon_list" class="table table-bordered table-striped w-100">
                                            <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Detail</th>
                                                <th>Author</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($news as $item)

                                                <tr>
                                                    <td>
                                                        {{$loop->iteration}}
                                                    </td>
                                                    <td>
                                                        <img src="{{asset($item->image?:"https://ui-avatars.com/api/?rounded=true&name=".$item->title)}}"
                                                             class="user_image" alt="News Image">
                                                    </td>
                                                    <td>
                                                        {{$item->title}}
                                                    </td>

                                                    <td class="blog-description"
                                                        title="{!!strip_tags( $item->description )!!}">{!! strip_tags( $item->description) !!}</td>


                                                    <td>{{ $item->author }}</td>
                                                    <td>
                                                        {{date('d M Y',strtotime($item->updated_at?:$item->created_at))}}
                                                    </td>


                                                    <td>
                                                        @if($item->is_active)
                                                            <a href="{{route('news-status',['id'=>$item->id,'status'=>0])}}"
                                                               class='btn btn-success btn-xs' id='active'>Active</a>
                                                        @else
                                                            <a href="{{route('news-status',['id'=>$item->id,'status'=>1])}}"
                                                               class='btn btn-danger btn-xs' id='active'>Inactive</a>
                                                        @endif

                                                        <span style="display: none">{{ ($item->is_active == 1) ? 'satuts_enabled' : 'satuts_disabled' }}</span>
                                                     </td>

                                                    <td>
                                                        <div class="actions">
                                                            <a href="{{route('news-edit',$item->id)}}"
                                                               class="btn btn-info btn-xs bg-olive">
                                                                <i class="fe fe-pencil"></i> Edit
                                                            </a>

                                                            <a href="{{route('news-delete',$item->id)}}"
                                                               onclick='return confirm("Are you sure to delete this record")'
                                                               class="btn btn-danger btn-xs">
                                                                <i class="fe fe-trash"></i> Delete
                                                            </a>

                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script>
                $(document).ready(function () {
                    $("#coupon_list").DataTable();
                });
                $('#inputStateRes').on('change', function () {
                    if (this.value == "Active") {
                        $("#coupon_list").DataTable().column(6).search('satuts_enabled').draw();
                    } else if (this.value == "InActive") {
                        $("#coupon_list").DataTable().column(6).search('satuts_disabled').draw();
                    } else if (this.value == 'All') {
                        $("#coupon_list").DataTable().column(6).search('').draw();
                    } else {
                        $("#coupon_list").DataTable().search().draw();
                    }
                });
            </script>
            <script>
                function truncateText(selector, maxLength) {
                    var element = document.querySelectorAll(selector);
                    $.each(element, function (i) {

                        truncated = this.innerHTML;

                        if (truncated.length > maxLength) {
                            truncated = truncated.substr(0, maxLength) + '...';
                        }

                        this.innerHTML = truncated;
                    });
                }

                $(document).ready(function () {
                    truncateText('.blog-description', 200);
                });


            </script>

            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

@endsection

