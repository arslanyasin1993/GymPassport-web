@extends('admin_dash.design')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Subscription Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class=""><a href="{{route('subscriberlist')}}">Subscriber-List</a></li>
                <li class="active">add-user-subscription</li>
            </ol>
        </section>
        <style type="text/css">
            .time {
                width: 99%;
                margin-left: 1px !important;
            }

            .star {
                color: red;
            }

            .help-block {
                color: red;
                border-color: red;
                line-height: 0px;
            }

            .help-block-text {
                color: red;
                border-color: red;
            }

            .select2-container .select2-selection--single {
                height: 35px !important;
            }

            .location {
                margin-top: 10px;
            }

            .note {
                color: red;
            }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                background-color: #3c8dbc !important;
                border: 1px solid #3c8dbc !important;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif


                    <div class="box">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="{{route('addUserSubscription')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}


                                <div class="box-body">


                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">User<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2 @if($errors->has('user_id')) help-block @endif" id="user_id" name="user_id" required>
                                                @if(!empty($users))
                                                    <option value=""> User</option>
                                                    @foreach($users as $key=>$value)
                                                        <option value="{{$value->id}}">{{$value->first_name. ' '.$value->last_name.' ('.$value->email.')'}}</option>
                                                    @endforeach
                                                @else
                                                    <option value=""> Users Unavailable</option>
                                                @endif
                                            </select>
                                            @if ($errors->has('user_id'))
                                                <strong class="help-block"> {{ $errors->first('user_id') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Subscription Plan<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            {{--                                            <input class="form-control @if($errors->has('subscription_plan')) help-block @endif" id="subscription_plan" name="subscription_plan" readonly/>--}}
                                            <select class="form-control select2 @if($errors->has('subscription_plan')) help-block @endif" id="subscription_plan" name="subscription_plan" onchange="getSubscriptions(this.value)" required>
                                                @if(!empty($plan_category))
                                                    <option value=""> Select Subscription Plan</option>
                                                    @foreach($plan_category as $key=>$value)
                                                        <option value="{{$value->id}}">{{$value->plan_cat_name}}</option>
                                                    @endforeach
                                                @else
                                                    <option value=""> Subscription Plan Unavailable</option>
                                                @endif
                                            </select>


                                            @if ($errors->has('subscription_plan'))
                                                <strong class="help-block"> {{ $errors->first('subscription_plan') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="subscription_id" class="col-sm-2 control-label">Subscription<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2 @if($errors->has('subscription_id')) help-block @endif" onchange="getSubscriptionAmount(this.value)" id="subscription_id" name="subscription_id">
                                                <option value="">Select subscription</option>
                                            </select>
                                            @if ($errors->has('subscription_id'))
                                                <strong class="help-block"> {{ $errors->first('subscription_id') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="subscription_amount" class="col-sm-2 control-label">Amount<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            <input class="form-control @if($errors->has('subscription_amount')) help-block @endif" name="subscription_amount" id="subscription_amount" value=""/>

                                            @if ($errors->has('subscription_amount'))
                                                <strong class="help-block"> {{ $errors->first('subscription_amount') }}</strong>
                                            @endif
                                        </div>
                                    </div>


                                </div>


                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                    <div class="col-sm-offset-1 col-sm-4"></div>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>

                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script type="text/javascript">

        subscriptionResponse = [];

        function getSubscriptions(v) {
            if (!v) {
                return false;
            }

            var url = "{{route('getSubscriptions')}}" + '/' + v;
            data = {user_id: v};
            $.ajax({
                url: url,
                data: data,
                success: function (response) {
                    html = '';
                    for (i = 0; i < response.length; i++) {
                        html += "<option value='" + response[i].id + "'>" + response[i].visit_pass + "</option>";
                    }
                    $("#subscription_id").html(html);
                    subscriptionResponse = response;
                    if (response.length > 0) {
                        getSubscriptionAmount(response[0].id);
                    }
                }, error: function (err) {

                }

            });

        }

        function getSubscriptionAmount(v) {
            $.each(subscriptionResponse, function () {
                if (this.id == v) {
                    $('#subscription_amount').val(this.amount);
                }
            });
        }

    </script>
@endsection
