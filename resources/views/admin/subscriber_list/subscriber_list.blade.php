@extends('admin_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Subscriber List
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Subscription Plan List</li>
            </ol>
        </section>
        <style type="text/css">
            i.fa {
                font-size: 19px;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12" style="text-align: end; margin-bottom: 15px;">
                    <a href="{{ route('inactiveSubscription') }}" class="btn btn-danger" id="deactivateSubscription">Deactivate Subscription</a>
                    <a href="{{ route('activeSubscription') }}" class="btn btn-primary" id="deactivateSubscription">Activate Subscription</a>
                </div>
                <div class="col-xs-12">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif
                    <div class="box-header with-border">
                        <div class="col-sm-12">
                            <div class="col-sm-2" style="margin-bottom: 5px;">
                                <a href="{{route('add-subscriber')}}">
                                    <button type="button" class="btn btn-block btn-success"><i class="fa fa-edit"></i> Add Subscriber</button>
                                </a>
                            </div>
                            <!-- <div class="col-sm-4"></div> -->
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <select name="visit_pass" class="form-control" id="visit_pass">
                                        <option value="">Select No.Of Pass</option>
                                        <option value="{{trans('constants.lite')}}" @php echo $get = '1'==old('visit_pass') ? "selected" :"";@endphp>Gym Passport - Lite</option>
                                        <option value="{{trans('constants.standard')}}" @php echo $get = '2'==old('visit_pass') ? "selected" :"";@endphp>Gym Passport - Standard</option>
                                        <option value="{{trans('constants.pro')}}" @php echo $get = '3'==old('visit_pass') ? "selected" :"";@endphp>Gym Passport - Pro</option>
                                    </select>
                                </div>
                                <label for="inputEmail3" class="col-sm-2 control-label" style="width: 98px;">From-Date</label>
                                <div class="col-sm-2">
                                    <input type='date' class="form-control" id='startdate' placeholder='From Date'>
                                </div>
                                <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                <div class="col-sm-2">
                                    <input type="date" name="" id="enddate" class="form-control" placeholder="End Date">
                                </div>
                                <div class="col-sm-1">
                                    <button class="btn btn-primary" id="submit">Apply</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-tabs-custom">
                        <!-- Tabs within a box -->
                        <ul class="nav nav-tabs pull-left" id="myTab">
                            <!-- <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li> -->
                            <li class="active"><a href="#lite_pass" class="usertab" data="1" data-toggle="tab">{{trans('constants.lite_pass')}}</a></li>
                            <li><a href="#standard_pass" class="usertab" data="2" data-toggle="tab">{{trans('constants.standard_pass')}}</a></li>
                            <li><a href="#pro_pass" class="usertab" data="3" data-toggle="tab">{{trans('constants.pro_pass')}}</a></li>
                            <li><a href="#trial_pass" class="usertab" data="4" data-toggle="tab">{{trans('constants.trial_pass')}}</a></li>
                            <li><a href="#corporate_plan" class="corporatetab" data="5" data-toggle="tab">{{trans('constants.corporate_plan')}}</a></li>
                        </ul>
                        <div class="tab-content no-padding">
                            <!-- Morris chart - Sales -->
                            <div class="chart tab-pane active" id="lite_pass">
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="gym_pass_lite" class="table table-bordered table-striped w-100">
                                            <thead>
                                                <tr>
                                                    <th>Subscription Date</th>
                                                    <th>Subscriber Name</th>
                                                    <th>Subscriber Email</th>
                                                    <th>Membership Fee</th>
                                                    <th>Coupon</th>
                                                    <th>CheckIns</th>
                                                    <th>Check In Amount</th>
                                                    <th>View Details</th>
                                                    <th>Plan Status</th>
                                                    <th>Subscription Status</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <td colspan="3"></td>
                                                <th></th>
                                                <td></td>
                                                <th style="text-align: start"></th>
                                                <th colspan="4"></th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="chart tab-pane " id="standard_pass">
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="gym_pass_standard" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Subscription Date</th>
                                            <th>Subscriber Name</th>
                                            <th>Subscriber Email</th>
                                            <th>Membership Fee</th>
                                            <th>Coupon</th>
                                            <th>CheckIns</th>
                                            <th>Check In Amount</th>
                                            <th>View Details</th>
                                            <th>Plan Status</th>
                                            <th>Subscription Status</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <td colspan="3"></td>
                                            <th></th>
                                            <td></td>
                                            <th style="text-align: start"></th>
                                            <th colspan="4"></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    </div>
                                </div>
                            </div>
                            <div class="chart tab-pane " id="pro_pass">
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="gym_pass_pro" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Subscription Date</th>
                                            <th>Subscriber Name</th>
                                            <th>Subscriber Email</th>
                                            <th>Membership Fee</th>
                                            <th>Coupon</th>
                                            <th>CheckIns</th>
                                            <th>Check In Amount</th>
                                            <th>View Details</th>
                                            <th>Plan Status</th>
                                            <th>Subscription Status</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <td colspan="3"></td>
                                            <th></th>
                                            <td></td>
                                            <th style="text-align: start"></th>
                                            <th colspan="4"></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    </div>
                                </div>
                            </div>

                            <div class="chart tab-pane " id="trial_pass">
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="gym_pass_trial" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Subscription Date</th>
                                                <th>Subscriber Name</th>
                                                <th>Subscriber Email</th>
                                                <th>Membership Fee</th>
                                                <th>Coupon</th>
                                                <th>CheckIns</th>
                                                <th>Check In Amount</th>
                                                <th>View Details</th>
                                                <th>Plan Status</th>
                                                <th>Subscription Status</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <td colspan="3"></td>
                                                <th></th>
                                                <td></td>
                                                <th style="text-align: start"></th>
                                                <th colspan="4"></th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="chart tab-pane " id="corporate_plan">
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="corporate_plan_table" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Company</th>
                                                <th>No. of Employees</th>
                                                <th>Plan</th>
                                                <th>Checkins</th>
                                                <th>View Details</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body" id="subscriptiondata">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script type="text/javascript">
                //$(document).ready(function(){
                //$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                //localStorage.setItem('activeTab', $(e.target).attr('href'));
                //});
                //var activeTab = localStorage.getItem('activeTab');
                //if(activeTab){
                //$('#myTab a[href="' + activeTab + '"]').tab('show');
                // }
                //});
            </script>
            <script type="text/javascript">
                var dataTable;
                $(document).ready(function () {
                    var plan_cat = '1';
                    fetchusersubscription(plan_cat);
                });
                $('.usertab').click(function () {
                    var plan_cat = $(this).attr('data');
                    //  alert(plan_cat);
                    fetchusersubscription(plan_cat);
                });
                $('.corporatetab').click(function () {
                    var plan_cat = $(this).attr('data');
                    fetchcorporatesubscription(plan_cat);
                });

                function fetchusersubscription(plan_cat) {
                    // alert(plan_cat);
                    is_corporate = 0;
                    if (plan_cat == '1') {
                        var table_id = 'gym_pass_lite';
                    } else if (plan_cat == '2') {
                        var table_id = 'gym_pass_standard';
                    } else if (plan_cat == '3') {
                        var table_id = 'gym_pass_pro';
                    } else if (plan_cat == '4') {
                        var table_id = 'gym_pass_trial';
                    } else {
                        is_corporate = 1;
                        var table_id = 'corporate_plan_table';
                    }
                    $('#' + table_id).DataTable().destroy();
                    dataTable = $('#' + table_id).DataTable({
                        'processing': true,
                        'serverSide': true,
                        //'searching': false, // Remove default Search Control
                        'ajax': {
                            'url': "{{route('ajaxsubscriberlist')}}",
                            'data': function (data) {
                                // Read values
                                var enddate = $('#enddate').val();
                                var startdate = $('#startdate').val();
                                var visit_pass = $('#visit_pass').val();
                                //var searchdata = $('#searchdata').val();

                                // Append to data
                                data.startdate = startdate;
                                data.enddate = enddate;
                                data.visit_pass = visit_pass;
                                data.plan_cat = plan_cat;
                                data.is_corporate = is_corporate;
                                //data.searchdata = searchdata;
                            }
                        },
                        "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000, 5000]],
                        "footerCallback": function(row, data, start, end, display) {
                            var api = this.api();
                            $(api.column(3).footer()).html(api.ajax.json().total_earning_amount);
                            $(api.column(5).footer()).html(api.ajax.json().total_checkins);
                            $(api.column(6).footer()).html(api.ajax.json().total_checkin_amount);
                        },
                        'columns': [
                            {
                                data: 'created_at'
                            },
                            {data: 'subscriber_name'},
                            {data: 'subscriber_email'},
                            {data: 'amount'},
                            {data: 'coupon_code'},
                            {data: 'total_checkin'},
                            {data: 'total_earning'},
                            {data: 'details'},
                            {data: 'plan_status'},
                            {data: 'subscription_status'},
                            {data: 'edit'},
                            {data: 'delete'},
                        ]
                    });
                    $('#searchdata').keyup(function () {
                        dataTable.draw();
                    });
                    $('#visit_pass').change(function () {
                        dataTable.draw();
                    });

                    $('#submit').click(function () {
                        dataTable.draw();
                    });
                }


                function fetchcorporatesubscription(plan_cat) {
                    // alert(plan_cat);

                    is_corporate = 1;
                    var table_id = 'corporate_plan_table';

                    $('#' + table_id).DataTable().destroy();
                    var dataTable = $('#' + table_id).DataTable({
                        'processing': true,
                        'serverSide': true,
                        //'searching': false, // Remove default Search Control
                        'ajax': {
                            'url': "{{route('ajaxsubscriberlistcorporate')}}",
                            'data': function (data) {
                                // Read values
                                var enddate = $('#enddate').val();
                                var startdate = $('#startdate').val();
                                var visit_pass = $('#visit_pass').val();
                                //var searchdata = $('#searchdata').val();

                                // Append to data
                                data.startdate = startdate;
                                data.enddate = enddate;
                                data.visit_pass = visit_pass;
                                data.plan_cat = plan_cat;
                                data.is_corporate = is_corporate;
                                //data.searchdata = searchdata;
                            }
                        },
                        "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000 , 5000]],
                        'columns': [

                            {data: 'corporate_name'},
                            {data: 'no_of_employee'},
                            {data: 'plan_name'},
                            {data: 'total_cehck_ins'},
                            {data: 'detail'},
                        ]
                    });
                    $('#searchdata').keyup(function () {
                        dataTable.draw();
                    });
                    $('#visit_pass').change(function () {
                        dataTable.draw();
                    });
                    $('#submit').click(function () {
                        dataTable.draw();
                    });
                }

                $(document).on('click' , '.delete_subscription' , function (e){
                    e.preventDefault();
                    if (confirm("Are you sure to delete this record?")){
                        var url = $(this).attr('data-href');
                        $.ajax({
                            url: url,
                            type: 'GET',
                            success: function (response){
                                if(response){
                                    dataTable.draw();
                                    toastr.success(response.message);
                                }else{
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                });

                $(document).on('click' , '#change_sub_status' , function(e){
                    e.preventDefault();
                    var url = $(this).attr('data-url');
                    if (confirm("Are you sure to change subscription status?")){
                        $.ajax({
                            url: url,
                            type: 'GET',
                            success: function (response){
                                if(response.status){
                                    dataTable.draw();
                                    toastr.success(response.message);
                                }else{
                                    toastr.error(response.error_message);
                                }
                            }
                        })
                    }
                })
            </script>

            <script type="text/javascript">
                function userview(id) {
                    //alert(id);
                    $.ajax({
                        url: "{{url('getsubscribe_user')}}/" + id,
                        type: 'GET',
                        dataType: 'html',
                        success: function (data) {
                            //alert(data);
                            $('#subscriptiondata').html(data);
                            $('#myModal').modal('show');
                        }
                    });
                }

            </script>
            <script type="text/javascript">
                $(function () {
                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear();
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    $('#startdate').attr('max', maxDate);
                    $('#enddate').attr('max', maxDate);
                });
            </script>
        </section>
    </div>
@endsection
