@extends('admin_dash.design')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Subscription Management
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class=""><a href="{{route('subscriberlist')}}">Subscriber-List</a></li>
                <li class="active">edit-user-subscription</li>
            </ol>
        </section>
        <style type="text/css">
            .time {
                width: 99%;
                margin-left: 1px !important;
            }

            .star {
                color: red;
            }

            .help-block {
                color: red;
                border-color: red;
                line-height: 0px;
            }

            .help-block-text {
                color: red;
                border-color: red;
            }

            .select2-container .select2-selection--single {
                height: 35px !important;
            }

            .location {
                margin-top: 10px;
            }

            .note {
                color: red;
            }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                background-color: #3c8dbc !important;
                border: 1px solid #3c8dbc !important;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-sm-offset-1 col-xs-12 col-sm-10 col-md-10">
                    @if(Session::has('message'))
                        <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! session('msg') !!}
                        </div>
                    @endif


                    <div class="box">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form action="{{route('update_user_subscription',$id)}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                {{ csrf_field() }}


                                <div class="box-body">

                                    <div class="form-group">
                                        <label for="amount" class="col-sm-2 control-label">User<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            <input class="form-control"  value="{{$userSubscription->username->first_name.' '.$userSubscription->username->last_name}}" readonly/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="amount" class="col-sm-2 control-label">Amount<span class="star"></span></label>
                                        <div class="col-sm-10">
                                            <input class="form-control @if($errors->has('amount')) help-block @endif" name="amount" id="amount" min="0" value="{{$userSubscription->total_payable_amount}}"/>
                                            @if ($errors->has('amount'))
                                                <strong class="help-block"> {{ $errors->first('amount') }}</strong>
                                            @endif
                                        </div>
                                    </div>


                                </div>


                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
                                    <div class="col-sm-offset-1 col-sm-4"></div>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                                <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>

                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>

    <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script type="text/javascript">

        subscriptionResponse = [];

        function getSubscriptions(v) {
            if (!v) {
                return false;
            }

            var url = "{{route('getSubscriptions')}}" + '/' + v;
            data = {user_id: v};
            $.ajax({
                url: url,
                data: data,
                success: function (response) {
                    html = '';
                    for (i = 0; i < response.length; i++) {
                        html += "<option value='" + response[i].id + "'>" + response[i].visit_pass + "</option>";
                    }
                    $("#subscription_id").html(html);
                    subscriptionResponse = response;
                    if (response.length > 0) {
                        getSubscriptionAmount(response[0].id);
                    }
                }, error: function (err) {

                }

            });

        }

        function getSubscriptionAmount(v) {
            $.each(subscriptionResponse, function () {
                if (this.id == v) {
                    $('#subscription_amount').val(this.amount);
                }
            });
        }

    </script>
@endsection
