<div class="box-body box-profile">
  <div class="col-sm-6">
    <ul class="list-group list-group-unbordered">
      <li class="list-group-item">
        <b>User Id</b> <a class="pull-right">{{$data->user_id}}</a>
      </li>
      <li class="list-group-item">
        <b>Name</b> <a class="pull-right">
        @if($data->username->first_name)
          {{$data->username->first_name}} 
          {{$data->username->last_name}}
        @endif
        </a>
      </li>
      <li class="list-group-item">
        <b>Email Address</b> 
          @if($data->username->email)
           <a >{{$data->username->email}}
          @endif
        </a>
      </li>
      <li class="list-group-item">
        <b>Country</b> 
        <a class="pull-right">
        @if($data->username->countryname)
          {{$data->username->countryname->nicename}}
        @endif
        </a>
      </li>
    </ul>
  </div>
  <div class="col-sm-6">
    <ul class="list-group list-group-unbordered">
      <p>Currently using {{$data->plan_duration}} {{$data->plan_time}} subscription plan.</p>
      <p>Amount paid for subscription : Rs. {{$data->total_payable_amount}}</p>
      <p>Purchase on : {{date('d/m/Y',strtotime($data->purchased_at))}}</p>
      <p>Next Payment date :  {{date('d/m/Y',strtotime("+$data->plan_duration $data->plan_time",strtotime($data->purchased_at)))}}</p>
      @if($data->status == 1)
        <p>Plan Status : <span class="badge badge-success">Active Plan</span></p>
      @elseif($data->status == 0)
        <p>Plan Status : <span class="badge badge-danger">Expired Plan</span></p>
      @else
        <p>Plan Status : <span class="badge badge-warning">Payment Pending</span></p>
      @endif
    </ul>
  </div>
</div>