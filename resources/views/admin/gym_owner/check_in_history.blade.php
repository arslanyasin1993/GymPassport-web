@extends('admin_dash.design')
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Check-in History
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="{{route('gymlist')}}">Gym-List</li>
        <li class="active">Check-in History</li>
      </ol>
    </section>
<style type="text/css">
  .table{
    width: 100% !important;
  }
  .etimate_amt{
    font-size: 22px;
    font-weight: bold;
  }
</style>
  <!-- Main content -->
    <section class="content">
       <button class="btn btn-info" style="margin-bottom: 5px;">Total Estimated Payment:</button> <span class="etimate_amt">${{$total_etimate_amt}}</span>
      <div class="row"> 
        <div class="col-xs-12">
    <div class="nav-tabs-custom">
       <!-- Tabs within a box -->
       <ul class="nav nav-tabs pull-left" id="myTab">
          <!-- <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li> -->
          <li class="active"><a href="#daily" class='activetab' data-toggle="tab" value="1">Daily</a></li>
          <li><a href="#weekly" data-toggle="tab" class='activetab' value="2">Weekly</a></li>
          <li><a href="#monthly" data-toggle="tab" class='activetab' value="3"> Monthly</a></li>
          <li><a href="#year" data-toggle="tab"  class='activetab' value="4">Year</a></li>
       </ul> 
      <div class="tab-content no-padding">
         <div class="chart tab-pane active" id="daily" >         
            <div class="box-body">
              <table id="daily_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User Name</th>
                  <th>Check-In</th>
                <!--   <th>Check-Out</th> -->
                </tr>
                </thead>
              </table>
            </div>
        </div>
        <div class="chart tab-pane" id="weekly" >       
            <div class="box-body">
              <table id="weekly_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User Name</th>
                  <th>Check-In</th>
                 <!--  <th>Check-Out</th> -->
                </tr>
                </thead>
              </table>
            </div>
        </div>
         <div class="chart tab-pane" id="monthly" >       
            <div class="box-body">
              <table id="monthly_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User Name</th>
                  <th>Check-In</th>
                  <!-- <th>Check-Out</th> -->
                </tr>
                </thead>
              </table>
            </div>
        </div>
         <div class="chart tab-pane" id="year" >       
            <div class="box-body">
              <table id="year_list" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>User Name</th>
                  <th>Check-In</th>
                 <!--  <th>Check-Out</th> -->
                </tr>
                </thead>
              </table>
            </div>
        </div>
      </div>
    </div>

          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
<script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
  });
  var activeTab = localStorage.getItem('activeTab');
  if(activeTab){
    $('#myTab a[href="' + activeTab + '"]').tab('show');
  }
});
</script>
<script type="text/javascript">
 $(document).ready(function(){
  var dataTable_first = $('#daily_list').DataTable({
   'processing': true,
   'serverSide': true,
   'searching': false, // Remove default Search Control
    'ajax': {
       'url':"{{route('check_in_history',['type'=>'1','gym_id'=>$gym_id])}}",
       'data': function(data){
          // Read values
          //var enddate = $('#enddate_first').val();
          //var startdate = $('#startdate_first').val();
          //var searchdata = $('#searchdata').val();

          // Append to data
          //data.startdate = startdate;
          //data.enddate = enddate;
          //data.searchdata = searchdata;
       }
    },
    'columns': [
              { data: 'username' }, 
              { data: 'check_in' }, 
              // { data: 'check_out' }, 
             
       
    ]
  });

  $('#searchdata').keyup(function(){
    dataTable_first.draw();
  });
  
   $('#submit_first').click(function(){
    dataTable_first.draw();
   });
});
</script>
<script type="text/javascript">
   $(document).ready(function(){
       
        var dataTable_second = $('#weekly_list').DataTable({
        'processing': true,
        'serverSide': true,
        'searching': false, // Remove default Search Control
        'ajax': {
        'url':"{{route('check_in_history',['type'=>'2','gym_id'=>$gym_id])}}",
        'data': function(data){
        }
        },
        'columns': [
        { data: 'username' }, 
        { data: 'check_in' }, 
        // { data: 'check_out' }, 
        ]
        });
});
    // $('.activetab').click(function(){
    //       var val = $(this).attr('value')
    //       alert(val);
    //     });
</script>
<script type="text/javascript">
   $(document).ready(function(){
       
        var dataTable_third = $('#monthly_list').DataTable({
        'processing': true,
        'serverSide': true,
        'searching': false, // Remove default Search Control
        'ajax': {
        'url':"{{route('check_in_history',['type'=>'3','gym_id'=>$gym_id])}}",
        'data': function(data){
        }
        },
        'columns': [
        { data: 'username' }, 
        { data: 'check_in' }, 
        // { data: 'check_out' }, 
        ]
        });
});
</script>
<script type="text/javascript">
   $(document).ready(function(){
       
        var dataTable_third = $('#year_list').DataTable({
        'processing': true,
        'serverSide': true,
        'searching': false, // Remove default Search Control
        'ajax': {
        'url':"{{route('check_in_history',['type'=>'4','gym_id'=>$gym_id])}}",
        'data': function(data){
        }
        },
        'columns': [
        { data: 'username' }, 
        { data: 'check_in' }, 
        // { data: 'check_out' }, 
        ]
        });
});
</script>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

