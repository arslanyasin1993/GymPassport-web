@extends('admin_dash.design')  
@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Gym-Partner
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="{{route('gymlist')}}">Gym-Owner Management</a></li>
        <li class="active">Edit Gym-Partner</li>
        
      </ol>
    </section>
<style type="text/css">
  .time{
    width: 99%;
    margin-left: 1px !important;
  }
  .star{
    color: red;
  }
  .help-block{
    color: red;
    border-color: red;
    line-height: 0px;
  }
  .help-block-text{
    color: red;
    border-color: red;
  }
  .select2-container .select2-selection--single {
    height: 35px !important;
  }
</style>
  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-offset-1 col-xs-12 col-sm-9 col-md-10">   
         @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
            @endif  
          <div class="box">
              <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">NOTE : All <span class="star">*</span> fileds is Required</h3>
            </div>

            <form action="{{route('update_gym_owner',['id'=>$id])}}" method="post" enctype="multipart/form-data" class="form-horizontal">
               {{ csrf_field() }}
               <input type="hidden" name="created_from" value="admin_side">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">First Name<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @if($errors->has('first_name')) help-block @endif" id="first_name" name="first_name" value="{{$user_data->first_name}}" placeholder="Enter First Name">
                      @if ($errors->has('first_name'))
                         <strong class="help-block"> {{ $errors->first('first_name') }}</strong>
                      @endif
                  </div>
                </div> 

                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Last Name<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @if($errors->has('last_name')) help-block @endif" id="last_name" name="last_name" value="{{$user_data->last_name}}" placeholder="Enter Last Name">
                      @if ($errors->has('last_name'))
                         <strong class="help-block"> {{ $errors->first('last_name') }}</strong>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Name of your fitness facility<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @if($errors->has('fitness_facility')) help-block @endif" id="fitness_facility" name="fitness_facility" value="{{$user_data->fitness_facility}}" placeholder="Enter Name of your fitness facility">
                      @if ($errors->has('fitness_facility'))
                         <strong class="help-block"> {{ $errors->first('fitness_facility') }}</strong>
                      @endif
                  </div>
                </div> 

<!--                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Email<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @if($errors->has('email')) help-block @endif" id="email" name="email" value="{{ old('email') }}" placeholder="Enter Email Id">
                      @if ($errors->has('email'))
                         <strong class="help-block"> {{ $errors->first('email') }}</strong>
                      @endif
                  </div>
                </div>-->

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Phone Number<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @if($errors->has('phone_number')) help-block @endif" id="phone_number" name="phone_number" value="{{$user_data->phone_number}}" placeholder="Enter Phone Number">
                      @if ($errors->has('phone_number'))
                         <strong class="help-block"> {{ $errors->first('phone_number') }}</strong>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Country<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <select class="form-control" name="country" id="country"> 
                      <option value="">Select Country</option>
                      @if(!empty($country))
                        @foreach($country as $key=>$value)
                          <option value="{{$value->id}}" @if($user_data->country == $value->id) selected @endif >{{$value->nicename}}</option>
                        @endforeach
                      @endif
                    </select>
                      @if ($errors->has('country'))
                         <strong class="help-block"> {{ $errors->first('country') }}</strong>
                      @endif
                  </div>
                </div>

                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">State<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <select class="form-control" name="state" id="state"> 
                        <option value="">Select State</option>
                        @if(!empty($state))
                        @foreach($state as $key=>$value)
                          <option value="{{$value->id}}" @if($user_data->state ==$value->id) selected @endif>{{$value->state}}</option>
                        @endforeach
                      @endif
                    </select>
                      @if ($errors->has('state'))
                         <strong class="help-block"> {{ $errors->first('state') }}</strong>
                      @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Suburb<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @if($errors->has('suburb')) help-block @endif" id="suburb" name="suburb" value="{{$user_data->suburb}}" placeholder="Enter Suburb">
                      @if ($errors->has('suburb'))
                         <strong class="help-block"> {{ $errors->first('suburb') }}</strong>
                      @endif
                  </div>
                </div>

                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Postcode<span class="star">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @if($errors->has('postal_code')) help-block @endif" id="postal_code" name="postal_code" value="{{$user_data->postal_code}}" placeholder="Enter Postcode">
                      @if ($errors->has('postal_code'))
                         <strong class="help-block"> {{ $errors->first('postal_code') }}</strong>
                      @endif
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <!--  <button type="submit" class="btn btn-default">Cancel</button> -->
               <div class="col-sm-offset-1 col-sm-4"></div>
                <button type="submit" class="btn btn-info">Update</button>
                <a href="{{route('gymlist')}}"><button type="button" class="btn btn-danger">Cancel</button></a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">

var geocoder = new google.maps.Geocoder();
var address = "noida";

geocoder.geocode( { 'address': address}, function(results, status) {

  if (status == google.maps.GeocoderStatus.OK) {
    var latitude = results[0].geometry.location.lat();
    var longitude = results[0].geometry.location.lng();
    alert(latitude);
  } 
}); 
</script>
@endsection

