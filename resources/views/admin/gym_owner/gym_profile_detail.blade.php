@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Gym Details
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{route('gymlist')}}">Gym Listing</a></li>
      <li class="active">Gym Details</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-3">
        <!-- Profile Image -->
<style type="text/css">
  .profile-user-img{
        width: 130px !important;
        height: 126px !important;
  }
</style>
        <div class="box box-primary">
          <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="{{Url('/')}}/{{$gym_data->gym_logo}}" alt="@if(!empty($gym_data->gym_name)){{$gym_data->gym_name}}@endif profile picture">
            <h3 class="profile-username text-center">@if(!empty($gym_data->gym_name)){{$gym_data->gym_name}}@endif</h3>
           <!--  <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Followers</b> <a class="pull-right">1,322</a>
              </li>
              <li class="list-group-item">
                <b>Following</b> <a class="pull-right">543</a>
              </li>
              <li class="list-group-item">
                <b>Friends</b> <a class="pull-right">13,287</a>
              </li>
            </ul> -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- About Me Box -->
        <!-- <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">About Me</h3>
          </div>
          <div class="box-body">
            <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
            <p class="text-muted">
              B.S. in Computer Science from the University of Tennessee at Knoxville
            </p>
            <hr>
            <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
            <p class="text-muted">Malibu, California</p>
            <hr>
            <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
            <p>
              <span class="label label-danger">UI Design</span>
              <span class="label label-success">Coding</span>
              <span class="label label-info">Javascript</span>
              <span class="label label-warning">PHP</span>
              <span class="label label-primary">Node.js</span>
            </p>
            <hr>
            <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
          </div>
        </div> -->
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#aboutgym" data-toggle="tab">About Gym</a></li>
            <li><a href="#opentime" data-toggle="tab">Open Time</a></li>
            <li><a href="#settings" data-toggle="tab">Images</a></li>
          </ul>
          <div class="tab-content">
            <div class="active tab-pane" id="aboutgym">
              <form class="form-horizontal">
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Owner Name :</label>
                <div class="col-sm-10">
                 @if(!empty($gym_data->gym_owner_detail->first_name))
                    {{$gym_data->gym_owner_detail->first_name}}
                 @endif 
                 @if(!empty($gym_data->gym_owner_detail->last_name))
                    {{$gym_data->gym_owner_detail->last_name}}
                 @endif
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Owner Email :</label>
                <div class="col-sm-10">
                   @if(!empty($gym_data->gym_owner_detail->email))
                    {{$gym_data->gym_owner_detail->email}}
                   @endif
                </div>
              </div>
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Gym Name :</label>
                <div class="col-sm-10">
                   @if(!empty($gym_data->gym_name))
                    {{$gym_data->gym_name}}
                   @endif
                </div>
              </div>
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Gym Price Per Visit :</label>
                <div class="col-sm-10">
                   @if(!empty($gym_data->gym_price_per_visit))
                    {{$gym_data->gym_price_per_visit}}
                   @endif
                </div>
              </div>
              <div class="form-group">
                <label for="inputExperience" class="col-sm-2 control-label">Phone Number :</label>
                <div class="col-sm-10">
                  @if(!empty($gym_data->phone_number))
                    {{$gym_data->phone_number}}
                   @endif
                </div>
              </div>
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">Gym Activities :</label>
                <div class="col-sm-10">
                  @if(!empty($gym_data->gym_activities))
                    {{$gym_data->gym_activities}}
                   @endif
                </div>
              </div>
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">About Gym :</label>
                <div class="col-sm-10">
                    @if(!empty($gym_data->about_gym))
                    {{$gym_data->about_gym}}
                   @endif
                </div>
              </div> 
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">About Address :</label>
                <div class="col-sm-10">
                    @if(!empty($gym_data->gym_address))
                    {{$gym_data->gym_address}}
                   @endif
                </div>
              </div>
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">Latitude :</label>
                <div class="col-sm-10">
                    @if(!empty($gym_data->gym_latitude))
                    {{$gym_data->gym_latitude}}
                   @endif
                </div>
              </div>
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">Longitude :</label>
                <div class="col-sm-10">
                    @if(!empty($gym_data->gym_longitude))
                    {{$gym_data->gym_longitude}}
                   @endif
                </div>
              </div>
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">Country :</label>
                <div class="col-sm-10">
                   @if(!empty($gym_data->countryname->nicename))
                    {{$gym_data->countryname->nicename}}
                   @endif
                </div>
              </div> 
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">City :</label>
                <div class="col-sm-10" id="city">
                  
                </div>
              </div>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="opentime">
                <div class="row">
                  @if($gym_data->is_all_day_open == 1)
                    <div class="col-12">
                      <p class="text-center" style="font-weight: 900">GYM is open 24*7</p>
                    </div>
                  @else
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped">
                        <thead>
                        <tr>
                          <th>S.N</th>
                          <th>Days</th>
                          <th>Open Time</th>
                          <th>Close Time</th>

                        </tr>
                        </thead>
                        <tbody>
                    @if(!empty($gym_data->gym_time))
                    @php $i=1; @endphp
                    @foreach($gym_data->gym_time as $key=>$time)
                        <tr>
                          <td>@php echo $i++; @endphp</td>
                          @if($time->gym_open_days=='0')
                          <td>Sunday</td>
                          @endif
                          @if($time->gym_open_days=='1')
                          <td>Monday</td>
                          @endif
                          @if($time->gym_open_days=='2')
                          <td>Tuesday</td>
                          @endif
                          @if($time->gym_open_days=='3')
                          <td>Wednesday</td>
                          @endif
                          @if($time->gym_open_days=='4')
                          <td>Thursday</td>
                          @endif
                          @if($time->gym_open_days=='5')
                          <td>Friday</td>
                          @endif
                          @if($time->gym_open_days=='6')
                          <td>Saturday</td>
                          @endif
                          <td>{{date('h:i A', strtotime($time->gym_open_timing))}}</td>
                          <td>{{date('h:i A', strtotime($time->gym_close_time))}}</td>
                        </tr>
                      @endforeach
                    @endif
                        </tbody>
                      </table>
                    </div>
                  @endif
                </div>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="settings">
              <div class="post">
                <div class="user-block">
                  <img class="img-circle img-bordered-sm" src="{{Url('/')}}/{{$gym_data->gym_logo}}" alt="User Image">
                  <span class="username">
                  <a href="#"> @if(!empty($gym_data->gym_name))
                    {{$gym_data->gym_name}}
                   @endif</a>
                <!--   <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a> -->
                  </span>
                  <span class="description">
                    @if(!empty($gym_data->created_at))
                    Posted date {{date('d/m/Y',strtotime($gym_data->created_at))}}
                   @endif
                  </span>
                </div>
                <!-- /.user-block -->
                <div class="row margin-bottom">
                 <!--  <div class="col-sm-6">
                    <img class="img-responsive" src="{{ asset('css/dist/img/photo1.png')}}" alt="Photo">
                  </div> -->
                  <!-- /.col -->
                  <div class="col-sm-12">
                    <div class="row">
                      @if(!empty($gym_data->gym_image))
                        @foreach($gym_data->gym_image as $key=>$img)
                        <div class="col-sm-4">
                          <img class="img-responsive" src="{{ Url('/')}}/{{$img->gym_image}}" alt="Photo">
                        </div>
                        @endforeach
                      @endif
                     <!--  <div class="col-sm-4">
                        <img class="img-responsive" src="{{ asset('css/dist/img/photo4.jpg')}}" alt="Photo">
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
  var gym_id = "{{collect(request()->segments())->last()}}";
  //alert(gym_id)
  $.ajax({  
    type: "GET",  
    url: "{{url('test')}}/"+gym_id, 
    //data: { gym_id: gym_id },
    success: function(response) {
      $('#city').html(response[0].city);
    }
});
</script>
</div>
@endsection