@extends('admin_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User Listing
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">User List</li>
            </ol>
        </section>
        <style type="text/css">
            i.fa {
                font-size: 19px;
            }

            .plan {
                font-weight: bold;
                float: right;
                margin-bottom: 14px;
            }
        </style>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                @if(Session::has('message'))
                    <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {!! session('msg') !!}
                    </div>
                @endif
                <div class="col-xs-12">
                    <a href="{{route('download_current_plan')}}">
                        <button class="btn btn-info ">Download customer current plan</button>
                    </a>
                    <a href="{{route('add_customer')}}" class="pull-right">
                        <button class="btn btn-primary ">Add New User</button>
                    </a>

                    <div class="box">

                        <div class="box-header with-border">
                            <div class="col-sm-12">
                                <!-- <div class="col-sm-4"></div> -->
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <!-- <input type='text' class="form-control" id='searchdata' placeholder='Search By First Name ,Last Name,Email'> -->
                                    </div>
                                    <label for="inputEmail3" class="col-sm-1 control-label" style="width: 98px;">From-Date</label>
                                    <div class="col-sm-2">
                                        <input type='date' class="form-control" id='startdate' placeholder='From Date'>
                                    </div>
                                    <label for="inputEmail3" class="col-sm-1 control-label">To-Date</label>
                                    <div class="col-sm-2">
                                        <input type="date" name="" id="enddate" class="form-control" placeholder="End Date">
                                    </div>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary" id="submit">Apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <style type="text/css">
                            img.user_image {
                                height: auto;
                                width: 79px;
                                border-radius: 15px;
                            }
                        </style>
                        <div class="box-body">
                            <div id="expbuttons"></div>
                            <div class="table-responsive">
                                <table id="dataTable-example" class="table table-bordered table-striped w-100">
                                    <thead>
                                    <tr>
                                        <th>User Image</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>City</th>
                                        <th>Phone</th>
                                        <th>User Type</th>
                                        <th>Registration date</th>
                                        <th>View Information</th>
                                        <th>User status</th>
                                        <th>Login</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="{{ asset('css/bower_components/jquery/dist/jquery.min.js')}}"></script>


            <script type="text/javascript">
                var dataTable;
                $(document).ready(function () {
                    dataTable = $('#dataTable-example').DataTable({
                        'processing': true,
                        'serverSide': true,
                        // 'searching': false, // Remove default Search Control
                        'ajax': {
                            'url': "{{route('cus_man_list')}}",
                            'data': function (data) {
                                // Read values
                                var enddate = $('#enddate').val();
                                var startdate = $('#startdate').val();
                                //var searchdata = $('#searchdata').val();

                                // Append to data
                                data.startdate = startdate;
                                data.enddate = enddate;
                                //data.searchdata = searchdata;
                            },

                        },
                        'columns': [
                            {data: 'user_image'},
                            {data: 'username'},
                            {data: 'email'},
                            {data: 'city'},
                            {data: 'phone'},
                            {data: 'user_type'},
                            {data: 'registerd_date'},
                            {data: 'view_info'},
                            {data: 'user_status'},
                            {
                                data: 'login' , render(data , type , row) {
                                    let url = "{{ route('login_account', ':id') }}";
                                    url = url.replace(':id', row.id);
                                    return `<a href='${url}' class='btn btn-xs bg-success'>Login</a>`;
                                }
                            },
                            {data: 'action'},
                        ],
                        dom: 'Bfrtip',
                        "lengthMenu": [[10, 25, 50, 100, 1000 , 5000], [10, 25, 50, 100, 1000 , 5000]],
                        buttons: [
                            'csv', 'excel', 'pdf', 'pageLength'
                        ]
                    });

                    $('#searchdata').keyup(function () {
                        dataTable.draw();
                    });

                    $('#submit').click(function () {
                        dataTable.draw();
                    });
                });

                // Change Status
                $(document).on('click' , '.change_status' , function (){
                    var url = $(this).attr('data-href');
                    $.ajax({
                       url: url,
                       type: 'GET',
                       success: function (response){
                           console.log(response);
                           if(response.status){
                               toastr.success(response.message);
                           }else{
                               toastr.error(response.message);
                           }
                           dataTable.ajax.reload();
                       }
                    });
                });

                // Delete Customer
                $(document).on('click' , '#inactive' , function(){
                    var url = $(this).attr('data-url');
                    $.ajax({
                        url: url,
                        type: 'GET',
                        success: function (response){
                            if(response.status == true){
                                dataTable.draw();
                                toastr.success(response.message);
                            }else{
                                toastr.success(response.message);
                            }
                        }
                    });
                });
            </script>
            <script type="text/javascript">
                $(function () {
                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear();
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    $('#startdate').attr('max', maxDate);
                    $('#enddate').attr('max', maxDate);
                });
            </script>
        </section>
    </div>
@endsection

