@extends('web_user_dash.design')
@section('content')
	  {!! $map['js'] !!}
	<section class="scc">
		<div class="container-fluid">
	        <div class="row">
	    		<div class="col-sm-2">
		           <div class="list-view"><a href="
		           	map-coll-page2.html"><button class="btn-list-view">List View</button><span class="gt-symbol">&gt</span></a></div>
                </div>
            </div>
         {!! $map['html'] !!}

         <div id="directionsDiv"></div>
        </div>
    </section>
    <script>
    	$(document).ready(function(){
			$(".image").click(function(){
				$(".popup-paymentaa").toggle();
			});
			});

		$(document).ready(function(){
			$(".trigger-navbar").click(function(){
				$(".custom-navbar").slideToggle();
			});
			});
    </script>
@endsection