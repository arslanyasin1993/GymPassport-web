@extends('web_user_dash.design')
@section('content')
<style type="text/css">
  .booking-details-wrapper {
    height: 100vh;
}
</style>
<section class="booking-details-wrapper">
   <div class="container-fluid">
      <div class="booking-details-box1">
         <div class="first-wrapper">
            <!-- <div id="demo" class="collapse"> -->
              @php
                  $plan_id ='';
                  $base_amount ='';
                  $gst ='';
                  $total_payable ='';
                if(!empty($plan_detail)){
                  $plan_id = $plan_detail['plan_id'];
                  $base_amount = $plan_detail['base_amount'];
                  $gst = $plan_detail['gst'];
                  $total_payable = $plan_detail['total_payable'];
                }
                
              @endphp
            <div class="key-value-div">
               <h4 class="same-h4">Base Amount</h4>
            </div>
            <div class="key-value-div">
               <input type="text" name="" class="payable-value" placeholder="$50" value="${{$base_amount}}" disabled>
               <!-- <h2 class="payable-value">$50</h2> -->
            </div>
           <!--  <div class="key-value-div">
               <h4 class="same-h4">Internet handling fees</h4>
            </div>
            <div class="key-value-div">
               <span class="qqq"><input type="text" name="" class="payable-value" placeholder="$30"></span>
            </div>
            <div class="key-value-div">
               <h4 class="same-h4">Base Amount</h4>
            </div>
            <div class="key-value-div">
               <input type="text" name="" class="payable-value" placeholder="$10">
            </div>-->
            <div class="key-value-div">
               <h4 class="same-h4">Integrated GST @ 10%</h4>
            </div>
            <div class="key-value-div">
               <input type="text" name="" class="payable-value" placeholder="$10" value="${{$gst}}" disabled>
            </div>
            <!--   <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo"><span class="span-icon"><i class="fa">&#xf106;</i></span></button> -->
            <!-- </div> -->
            <!--   <span class="span-icon"><i class="fa" data-toggle="collapse" data-target="#demo">&#xf106;</i></span> -->
            <div class="pol-tag"> <a href="#" class="cancellation-pol">Cancellation Policy</a></div>
         </div>
         <div class="payable-amt">
            <p class="payable-stmt">Total Payable Amount</p>
            <input type="text" name="" class="payable-value one-fifty" placeholder="$150" value="${{$total_payable}}" disabled>
         </div>
      </div>
     <!--  <div class="booking-details-box2">
         <h3>Apply Promo Code</h3>
         <div class="second-box">
          -- <p class="enter-code-box">Enter Code</p> --
            <input type="text" name="" class="enter-code-box" placeholder="Enter code">
            --  <input type="text" name="" class="apply-box" placeholder="Apply"> --
            <button type="submit" class="apply-box">Apply</button>
         </div>
      </div> -->
      <div class="pay-btn-box">
         <a href="{{route('card_detail',['plan_id'=>encrypt($plan_id),'base_amount'=>encrypt($base_amount),'gst'=>encrypt($gst),'total_payable'=>encrypt($total_payable)])}}"><button type="button" class="btn btn-danger pay-btn">Pay $ {{$total_payable}}</button></a>
      </div>
   </div>
</section>
<script>
   $(document).ready(function(){
     $(".trigger-navbar").click(function(){
       $(".custom-navbar-book-det").slideToggle();
     });
     });
   
</script>
@endsection