@if(!empty($check_in_detail)) 
   @foreach($check_in_detail as $key=>$value)
      <div class="checkin-wrapper">
            <h2>{{$value->gym_name->gym_name}}</h2>
            <span class="address-checkin">{{$value->gym_name->gym_address}}</span><span class="get-direction">Get Direction <span class="gt-symbol-checkin">&gt</span></span>
            <div class="date-time-box">
             <!--  {{$value->gym_name->gym_latitude}}{{$value->gym_name->gym_longitude}} -->
              <h4><span class="date-caption-box">Date:</span><span class="date-value-box">{{date('d/m/Y',strtotime($value->user_check_in))}}</span>
                <span class="time-caption-box">Time:</span><span class="time-value-box">{{date('h:i A',strtotime($value->user_check_in))}}</span>
              </h4>
            </div>
          </div>
  @endforeach
  <div id="checkinpage"></div>
@else
   <div class="billing-history-box">
    <div class="billing-history-wrapper">
      <h2>Billing detail not available</h2>
    </div>
  </div>
@endif

<script src="{{ asset('website_file/jquery.bootpag.min.js')}}"></script>
<?php  $total_pages = ceil($total_rows / $limit); if($total_pages > 1): ?>
<?php endif; ?>
<script>         
  var numval = localStorage.getItem("check_in_val");
  if(numval != null){
    var nval = numval;
  }else{
    var nval = 1;
  } 
  $('#checkinpage').bootpag({
    total: '<?php if(!empty($total_pages)){ echo $total_pages; } ?>',
    page: nval,
    maxVisible: '<?php if(!empty($total_pages)){ echo $total_pages; } ?>',
    firstLastUse: true,
    // first: '←',
    // last: '→',
    wrapClass: 'pagination',
    activeClass: 'active',
    disabledClass: 'disabled',
    // nextClass: 'next',
    // prevClass: 'prev',
    // lastClass: 'last',
    // firstClass: 'first'
  }).on("page", function(event, check_in_val){
      localStorage.setItem("check_in_val", check_in_val);
      $.ajax({
      type: 'get',
      url: "{{route('check_in_histroy')}}",
      data: {"check_in_val" : check_in_val},
      beforeSend:function(){
      },
      complete:function(){
      },
      }).done(function(data) {
      $("#gym_check_in_list").html(data);
         localStorage.setItem("check_in_val",1);

      });
  });
</script>