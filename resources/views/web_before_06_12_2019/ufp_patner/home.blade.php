@extends('web_user_dash.design')
@section('content')
<style type="text/css">
  #page-selection{
  float: right;
  }
  #checkinpage{
  float: right;
  }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<section class="three-user-profile-wrapper">
  <div class="three-user-profile-part1">
    <div class="three-for-bg">
        @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
         @endif
      <h3 class="three-main-heading">User Profile</h3>
      <form class="three-left-form" method="post" action="{{route('update_profile')}}">
          {{ csrf_field() }}
        <label class="three-label">User Id</label>
        <input type="number"  class="three-values enable-class" placeholder="98754" disabled="" value="">
        <label class="three-label">First Name</label>
        <input type="text" name="first_name" class="three-values bord enable-class" placeholder="John" disabled="" value="" required>
        @if ($errors->has('first_name'))<label class="error" for="">{{ $errors->first('first_name') }}</label>@endif
        <label class="three-label">Last Name</label>
        <input type="text" name="last_name" class="three-values bord enable-class" placeholder="Doe" disabled="" value="" required>
         @if ($errors->has('last_name'))<label class="error" for="">{{ $errors->first('last_name') }}</label>@endif
        <label class="three-label">Email</label>
        <input type="text" name="email" class="three-values bord enable-class" placeholder="abc@gmail.com" disabled="" value="" required>
        @if ($errors->has('email'))<label class="error" for="">{{ $errors->first('email') }}</label>@endif
        <label class="three-label">D.O.B</label>
        <input type="date" name="d_o_b" class="three-values bord enable-class" placeholder="07/09/1995" disabled="" value="" required>
          @if ($errors->has('d_o_b'))<label class="error" for="">{{ $errors->first('d_o_b') }}</label>@endif
        <label class="three-label">Country</label>
        <!-- <input type="tetx" name="" class="three-values" placeholder="India" id="one"> -->
         <select name="country" class="list-three-pages three-label bord" required>
          <option value="">Select Country</option>
        </select>
         @if ($errors->has('country'))<label class="error" for="">{{ $errors->first('country') }}</label>@endif
        <div class="three-two-btns">
          <button type="button" class="btn btn-danger three-edit-profile edit">Edit Profile</button>
          <button type="submit" class="btn btn-danger three-edit-profile update" style="display: none;">Update</button>
          <button type="button" class="btn btn-danger three-edit-profile cancle" style="display: none;">Cancle</button>
          <a href="{{route('UserPasswordChange')}}"><button type="button" class="btn btn-danger three-change-password">Change Password</button></a>
        </div>
      </form>
    </div>
  </div>
  <div class="three-user-profile-part2">
    <div class="three-user-profile-menu-box">
      <ul class="nav nav-pills">
        <li class="active"><a data-toggle="pill" href="#menu1">Current Plan</a></li>
        <li><a data-toggle="pill" href="#menu2">Billing History</a></li>
        <li><a data-toggle="pill" href="#menu3">Check-In Details</a></li>
      </ul>
    </div>
    <div class="tab-content">
      <div id="menu1" class="tab-pane fade in active" >
        <div class="plan-box">
          <h5>You are currently using </h5>
          <span class="amt-subscription">Amount paid for Subscription:</span><span class="value-subscription">$ </span>
          <div class="purchased-box"><span class="purchased-payment">Purchased on:</span><span class="purchased-payment-date"></span></div>
          <div class="payment-box">
            <h3><span class="purchased-payment">Next Payment Date:</span><span class="purchased-payment-date"></span></h3>
          </div>
          <span class="subscription">Subscription</span>
          <label class="switch ">
          <input type="checkbox">
          <span class="slider round"></span>
          </label>
          <span class="learn-more"><a href="#">Learn More <i class="fa fa-chevron-right chevron-right-symbol"></i></a></span>
        </div>
      </div>
      <div id="menu2" class="tab-pane fade">
        <div class="right-box-for-scroll-billing-history" id="billing_detail">
       <!--    <div class="billing-history-box">
          <div class="billing-history-wrapper">
              <h2>Ultimate</h2>
              <span class="amt-subscription-billing">5 visit Pass</span>
              <div class="purchased-box-billing">
                <h4>
                  <span class="purchased-payment-billing">Purchased on:</span><span class="purchased-payment-date-billing">07/010/2018</span>
                </h4>
                <h4>
                  <span class="purchased-payment-billing">Expired on:</span>
                  <span class="purchased-payment-date-billing">07/010/2018</span>
                </h4>
              </div>
            </div>
          </div>
          <div class="billing-history-box">
            <div class="billing-history-wrapper">
              <h2>GymFit - Lite Pass</h2>
              <span class="amt-subscription-billing">5 visit Pass</span>
              <div class="purchased-box-billing">
                <h4>
                  <span class="purchased-payment-billing">Purchased on:</span><span class="purchased-payment-date-billing">07/010/2018</span>
                </h4>
                <h4>
                  <span class="purchased-payment-billing">Expired on:</span>
                  <span class="purchased-payment-date-billing">07/010/2018</span>
                </h4>
              </div>
            </div>
          </div> -->
        </div>
      </div>
      <div id="menu3" class="tab-pane fade">
        <div class="right-box-for-scroll"  id="gym_check_in_list">
         <!--  <div class="checkin-wrapper">
            <h2>Cross Fit</h2>
            <span class="address-checkin">Sector 63, Noida</span><span class="get-direction">Get Direction <span class="gt-symbol-checkin">&gt</span></span>
            <div class="date-time-box">
              <h4><span class="date-caption-box">Date:</span><span class="date-value-box">07/08/2019</span>
                <span class="time-caption-box">Time:</span><span class="time-value-box">08:00 AM</span>
              </h4>
            </div>
          </div>
          <div class="checkin-wrapper">
            <h2>Cross Fit</h2>
            <span class="address-checkin">Sector 63, Noida</span><span class="get-direction">Get Direction <span class="gt-symbol-checkin">&gt</span></span>
            <div class="date-time-box">
              <h4><span class="date-caption-box">Date:</span><span class="date-value-box">08/08/2019</span>
                <span class="time-caption-box">Time:</span><span class="time-value-box">08:00 AM</span>
              </h4>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</section>
<script>
  $(document).ready(function(){
    $('.checked').click(function(){
        alert('hdsfhsdkj')
    });

    $(".trigger-navbar").click(function(){
      $(".custom-navbar-three").slideToggle();
    });
    });
  
      $('.three-edit-profile').click(function(){
         $(".bord").css("background-color", "#be0027");
         $(".enable-class").prop( "disabled", false );
         $(".update").css("display", "block");
         $(".cancle").css("display", "block");
         $(".edit").css("display", "none");

         });  

      $('.cancle').click(function(){
         $(".bord").css("background-color", "#383838");
        $(".enable-class").prop( "disabled", true );
         $(".update").css("display", "none");
         $(".cancle").css("display", "none");
         $(".edit").css("display", "block");

         });
  
  
  
  
</script>
<script>
  $(document).ready(function(){
    // $(".trigger-navbar").click(function(){
    //   $(".custom-navbar-three").slideToggle();
    // });
    });
</script>
@endsection