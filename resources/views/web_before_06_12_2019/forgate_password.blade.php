
<!DOCTYPE html>
<html>
<head>
  <title>signup</title>
 <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
  <meta charset="utf-8">
  <meta name="viewport" content="width=devic e-width, initial-scale=1">
  <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/media.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
   <style type="text/css">
      .error{
            padding: 0px 0px 0px 10px;
            color: red;
            font-size: 14px;
      }
    </style>

    <body>
    <header class="head-signup">
    <div class="container-fluid">
      <div class="icon-signup">
        <img src="{{ asset('website_file/images/logo.png')}}" alt="icon" class="img-fluid signup-icon">
      </div>
      </div>
  </header>
  <section class="forgot-pass-left-part">
    <img src="{{ asset('website_file/images/forgot-pass-image.png')}}" alt="forgot-pass-page-image" class="img-fluid signup-left-img">
  </section>
  <section class="forgot-pass-right-part">
     @if(Session::has('message'))
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
      @endif 
    <form  action="{{route('forgate_pass')}}" method="post">
       {{ csrf_field() }}
      <div class="for-bg-image">
      <h5 class="forgot-pass-heading">Forgot Password</h5>
      <label class="forgot-pass-labels">Enter Registered Email Address</label>
      <input type="email" name="email" class="forgot-pass-form" placeholder="ultimatefitness@gmail.com" value="{{ old('email') }}">
       @if ($errors->has('email'))<label class="error" for="">{{ $errors->first('email') }}</label>@endif
      <div class="forgot-pass-btn-box">
      <button type="submit" class="btn btn-danger forgot-pass-next-btn">Next</button>
      </div>
      </div>
    </form>
   
  </section>
  <script type="text/javascript">
    $('.close').click(function(){
    $('.alert').hide();
});
  </script>
</body>
</html>