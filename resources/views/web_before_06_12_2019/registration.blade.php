
<!DOCTYPE html>
<html>
<head>
  <title>signup</title>
 <link rel="shortcut icon" href="{{ asset('landing_page/images/favicon32x32.png')}}">
  <meta charset="utf-8">
  <meta name="viewport" content="width=devic e-width, initial-scale=1">
  <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/style.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('website_file/css/media.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
      .error{
            padding: 0px 0px 0px 2px;
            color: red;
            font-size: 14px;
      }
    </style>
    <style type="text/css">
            #loader {
                  position: absolute;
                  left: 68%;
                  top: 85%;
                  z-index: 1;
                  width: 150px;
                  height: 150px;
                  margin: -75px 0 0 -75px;
                  border: 16px solid #f74d4d80;
                  border-radius: 50%;
                  border-top: 16px solid #bf0606;
                  width: 120px;
                  height: 120px;
                  -webkit-animation: spin 2s linear infinite;
                  animation: spin 2s linear infinite;

                }

                @-webkit-keyframes spin {
                  0% { -webkit-transform: rotate(0deg); }
                  100% { -webkit-transform: rotate(360deg); }
                }

                @keyframes spin {
                  0% { transform: rotate(0deg); }
                  100% { transform: rotate(360deg); }
                }

                /* Add animation to "page content" */
                .animate-bottom {
                  position: relative;
                  -webkit-animation-name: animatebottom;
                  -webkit-animation-duration: 1s;
                  animation-name: animatebottom;
                  animation-duration: 1s
                }

                @-webkit-keyframes animatebottom {
                  from { bottom:-100px; opacity:0 } 
                  to { bottom:0px; opacity:1 }
                }

                @keyframes animatebottom { 
                  from{ bottom:-100px; opacity:0 } 
                  to{ bottom:0; opacity:1 }
                }
                .alert-success {
                  font-size: 18px;
                  font-weight: 700;
                  margin-top: 10px;
               }
               .alert-danger {
                  font-size: 18px;
                  font-weight: 700;
                  margin-top: 10px;
               }
        </style>
</head>
<body>
    <header class="head-signup">
    <div class="container-fluid">
      <div class="icon-signup">
        <img src="{{ asset('website_file/images/logo.png')}}" alt="icon" class="img-fluid signup-icon">
      </div>
      </div>
  </header>
  <section class="signup-left-part">
    <img src="{{ asset('website_file/images/signup.png')}}" alt="signup-page-image" class="img-fluid signup-left-img">
    <div class="stores-icon">
      <img src="{{ asset('website_file/images/app-store.png')}}" alt="app-store-image" class="img-fluid app-store-signup">
      <img src="{{ asset('website_file/images/google-store.png')}}" alt="google-store-image" class="img-fluid google-store-signup">
      </div>
  </section>
  <section class="signup-right-part">
    <div class="successmsg-from-popup-wraper" id="myPopup" style="display: none;">
      @if(Session::has('message'))
            <script>
                $("#myPopup").show();
            </script>
            <div class="alert @if(Session::has('message')) {!! session('message') !!} @endif alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {!! session('msg') !!}
            </div>
            @endif  
  <!--   <div class="successmsg-popup">
        <div class="close-btnbx">
            <button type="button" class="close close-popup" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @if(Session::has('message'))
            <script>
                $("#myPopup").show();
            </script>
            <p>{!! session('msg') !!}</p>
      
       @endif
    </div> -->
</div>
    <div class="bg-image-signup">
       <div class="form-popup-wraper" id="formpopup">
         <div id="loader"></div> </div>
    <h5 class="sign-up-heading">Sign Up</h5>
   <form id="reg_form" method="post" action="{{route('user_submit')}}" enctype="multipart/form-data">
      {{ csrf_field() }}
    <input type="hidden" name="user_type" id="user_type" value="3">
       <div class="col-one">
            <label class="signup-labels">First Name</label>
        <input type="text" name="first_name" id="first_name" class="signup-form" placeholder="John" value="{{ old('first_name') }}">
          @if ($errors->has('first_name'))<label class="error" for="">{{ $errors->first('first_name') }}</label>@endif
      </div>
      <div class="col-one">
            <label class="signup-labels">Last Name</label>
          <input type="text" name="last_name" id="last_name" class="signup-form" placeholder="Doe"  value="{{ old('last_name') }}">
          @if ($errors->has('last_name'))<label class="error" for="">{{ $errors->first('last_name') }}</label>@endif
      </div>
      <div class="col-one">
        <label class="signup-labels">Email Address</label>
        <input type="email" name="email" id="email" class="signup-form" placeholder="ultimatefitness@gmail.com"  value="{{ old('email') }}">
         @if ($errors->has('email'))<label class="error" for="">{{ $errors->first('email') }}</label>@endif
      </div>
      <div class="col-one-hide">
        <label class="signup-labels">Email Address</label>
        <input type="email" class="signup-form" placeholder="ultimatefitness@gmail.com">
      </div>
      <div class="for-margin">
        <div class="col-one">
        <label class="signup-labels">Password</label>
        <div class="pass-box-wrapper">
          <input type="password" name="password" class="signup-form" placeholder="*********" id="pwd" value="{{ old('password') }}">
            <p class="show-hide-para-signup" id="pwdbtn">Show</p>
              </div>
          @if ($errors->has('password'))<label class="error" for="">{{ $errors->first('password') }}</label>@endif
          </div>
          <div class="col-one">
              <label class="signup-labels">Confirm Password</label>
        <div class="pass-box-wrapper">
          <input type="password" name="confirm_password" class="signup-form" placeholder="*********" id="pwd1">
          <p class="show-hide-para-signup" id="pwdbtn1" value="{{ old('confirm_password') }}">Show</p>
              </div>
            @if ($errors->has('confirm_password'))<label class="error" for="">{{ $errors->first('confirm_password') }}</label>@endif
          </div>

      <div class="col-one">
        <label class="signup-labels">Country</label>
        <!-- <input type="text" name="country" id="country" class="signup-form" placeholder="India"> -->
        <select name="country" id="country" class="signup-form">
          <option value="">Select Country</option>
          @if(!empty($country))
            @foreach($country as $key=>$val)
              <option  value="{{$val->id}}" @if($val->id==old('country')) selected @endif> {{$val->nicename}}</option>
            @endforeach
          @endif
        </select>
        @if ($errors->has('country'))<label class="error" for="">{{ $errors->first('country') }}</label>@endif
      </div>
      <div class="col-one">
        <label class="signup-labels">State</label>
        <!-- <input type="text" name="state" id="state"  class="signup-form" placeholder="Uttar Pradesh"> -->
        <select name="state" id="state" class="signup-form">
          <option value="">Select Country</option>
          @if(!empty($country))
            @foreach($state as $key=>$val)
              <option  value="{{$val->id}}" @if($val->id==old('state')) selected @endif>{{$val->state}}</option>
            @endforeach
          @endif
        </select>
         @if ($errors->has('state'))<label class="error" for="">{{ $errors->first('state') }}</label>@endif
      </div>
      <div class="col-one">
        <label class="signup-labels">City</label>
        <input type="text" name="city" id="city" class="signup-form" placeholder="Noida" value="{{old('city')}}">
        @if ($errors->has('city'))<label class="error" for="">{{ $errors->first('city') }}</label>@endif
      </div>
      <div class="col-one">
        <label class="signup-labels">Postal Code</label>
        <input type="number" name="postal_code" id="postal_code" class="signup-form" placeholder="201301" value="{{old('postal_code')}}">
        @if ($errors->has('postal_code'))<label class="error" for="">{{ $errors->first('postal_code') }}</label>@endif
      </div>
        <div class="col-one">
        <label class="signup-labels">Address</label>
        <input type="text" name="address"  id="address" class="signup-form" placeholder="Sector 63" value="{{old('address')}}">
        @if ($errors->has('address'))<label class="error" for="">{{ $errors->first('address') }}</label>@endif
      </div>
    
         
      
    </div>
    <div class="tnc">
      <div class="signup-checkbx">
        <input type="checkbox" id="signupcheckbx" checked name="checked" value="1">
        @if ($errors->has('checked'))<label class="error" for="">{{ $errors->first('checked') }}</label>@endif
        <label for="signupcheckbx">
           <span></span>
        </label>
         <h4>I agree to <a href="{{route('terms_conditions')}}" target="_blank"><span class="tnc-stmt">Terms & Conditions</span></a></h4>
        </div>
        </div>
    <!-- <div class="tnc">
      <label class="container-signup">
        <input type="checkbox" checked="checked">
        <span class="checkmark"></span>
      </label>
        <h4>I agree to <span class="tnc-stmt">Terms & Conditions</span></h4>
    </div> -->
    <div class="signup-btn-div">
      <button type="submit" class="btn btn-danger signup-btn submit">Sign Up</button>
    </div>
      </form>
  </div>
          <div class="store-signup">
                <img src="{{ asset('website_file/images/app-store.png')}}" alt="newpass-page-image" class="img-fluid apple-store-image">
              <img src="{{ asset('website_file/images/google-store.png')}}" alt="newpass-page-image" class="img-fluid google-store-image">
            </div>
  </section>
   <script src="{{ asset('website_file/validate.js')}}"></script>
  <script>
 $(document).ready(function(){
    $('#pwdbtn').click(function(){
      if($('#pwd').attr('type') == 'password'){
        $('#pwd').attr("type","text");
        $(this).text("hide");
      }else {
        $('#pwd').attr("type","password");
        $(this).text("show");
      }
    })
  });

 $(document).ready(function(){
    $('#pwdbtn1').click(function(){
      if($('#pwd1').attr('type') == 'password'){
        $('#pwd1').attr("type","text");
        $(this).text("hide");
      }else {
        $('#pwd1').attr("type","password");
        $(this).text("show");
      }
    })
  });
</script>
@if($errors->has('first_name') || $errors->has('last_name') || $errors->has('email') || $errors->has('postal_code') || $errors->has('state'))
<script type="text/javascript">
    document.getElementById('formpopup').classList.add('show');
    document.getElementById("loader").style.display = "none";
</script>
@endif
 @if(Session::has('message'))
    <script type="text/javascript">
        document.getElementById("loader").style.display = "none";
   //document.getElementById('formpopup').classList.add('show');
   </script>
 @endif
 <script>
document.getElementById("loader").style.display = "none";
$('.submit').click(function(){
  //alert('ds');
    var myVar;
    document.getElementById("loader").style.display = "block";
    myVar = setTimeout(showPage, 9000);
});

function showPage() {
 // document.getElementById("loader").style.display = "none";
}
</script>
    <script type="text/javascript">
        // $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        //         $(".alert").slideUp(500);
        //    });

        function showpopup(){
            document.getElementById('formpopup').classList.add('show');
        }  


$(document).ready(function(){
    $(".custom-btn").click(function(){
    $('#formpopup').addClass("show");
});

$(document).on("click", function(event){
    // var trigger = $('.custom-btn, #formpopup');
    // console.log(trigger);
     if (!$(event.target).closest(".custom-btn, #formpopup").length) {
        $('#formpopup').removeClass('show');
     }

});

$('.close').click(function(){
    $('#myPopup').hide();
});



});


    </script>
<script type="text/javascript">
  $().ready(function() {                 
    // $("#reg_form").validate({
    //     rules:{
    //         first_name:{
    //             required: true,
    //             minlength: 2,
    //             maxlength: 100,
    //         }, 
    //         last_name:{
    //             required: true,
    //             minlength: 2,
    //             maxlength: 100,
    //         }, 
    //         email:{
    //             required: true,
    //             minlength: 5,
    //         }, 
    //         password:{
    //             required: true,
    //             minlength: 8,
    //         }, 
    //         confirm_password:{
    //             required: true,
    //             minlength: 8,
    //         }, 
    //         address:{
    //             required: true,
    //         },
    //         country:{
    //             required: true,
    //         },
    //         state:{
    //             required: true,
    //         },
    //         city:{
    //             required: true,
    //         },
    //         postal_code:{
    //             required: true,
    //             minlength: 4,
    //         },
    //     },
    //     highlight: function (element) {
    //         $(element).closest('.form-group').addClass('has-error');
    //     },
    //     messages:{
    //         first_name:{
    //             required: "First name is required",
    //             minlength: "First name must be at least 2 characters",
    //             maxlength: "Maximum number of characters - 100",
    //         },
    //         last_name:{
    //             required: "Last name is required",
    //             minlength: "Last name must be at least 2 characters",
    //             maxlength: "Maximum number of characters - 100",
    //         }, 
    //         email:{
    //             required: "Email is required",
    //             minlength: "Email must be at least 5 characters",
    //         }, 
    //         password:{
    //             required: "Password is required",
    //             minlength: "Password must be at least 8 characters",
    //         }, 
    //         confirm_password:{
    //             required: "Confirm password is required",
    //             minlength: "Confirm password must be at least 8 characters",
    //         }, 
    //         address:{
    //             required: "Address is required",
    //         }, 
    //         country:{
    //             required: "Country is required",
    //         }, 
    //         state:{
    //             required: "State is required",
    //         },
    //         city:{
    //             required: "City is required",
    //         }, 
    //         postal_code:{
    //             required: "Postal code is required",
    //         },
    //     },
    //     submitHandler: function(form) { 
    //         $("#reg_form").submit();
    //         return false;
    //           $.ajax({
    //             url:"{{route('user_submit')}}",
    //             type:'POST',
    //             dataType: 'html',
    //             success: function(data) {
    //                 console.log(data);
    //                 $("#block").html(data);
    //             }
    //          });
    //          return true; 
    //      }
    //  });
});
</script>
</body>
</html>