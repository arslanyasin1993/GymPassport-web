@extends('corporate_dash.design')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{$total_active_user}}</h3>

                            <p>Active Employees</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users" aria-hidden="true"></i>
                        </div>
                        <a href="{{route('employee-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{{$total_inactive_user}}<sup style="font-size: 20px"></sup></h3>

                            <p>In Active Employees</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-users" aria-hidden="true"></i>
                        </div>
                        <a href="{{route('employee-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>{{$total_users}}</h3>

                            <p>Total Employees</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-universal-access" aria-hidden="true"></i>
                        </div>
                        <a href="{{route('employee-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red" style="padding: 20px">
                        <h3 style="font-size: 24px">Company Details</h3>
                        <div class="inner" style="font-size: 15px; padding: 10px 0 20px 15px">
                            <div class="row">
                                <label>Company Name:</label>
                                <span>{{$corporate_name}}</span>
                            </div>
                            <div class="row">
                                <label>Corporate Plan:</label>
                                <span>{{$corporate_plan}}</span>
                            </div>
                            <div class="row">
                                <label>Expiry Date:</label>
                                <span>{{$expired_at}}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-navy" style="padding: 20px">
                        <h3 style="font-size: 24px">Contact us Here!</h3>
                        <div class="inner" style="font-size: 15px; padding: 10px 0 20px 15px">
                            <div class="row">
                                <label>For any assistance contact Gym Passport team:</label>
                            </div>
                            <div class="row">
                                <label>Phone:</label>
                                <span>03424267060</span>
                            </div>
                            <div class="row">
                                <label>Email:</label>
                                <span>partner@gympassport.pk</span>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- ./col -->
            </div>
        </section>

        <!-- /.content -->
    </div>
@endsection

