<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('registration', 'API\UserController@registration');
Route::post('validate_registration', 'API\UserController@validate_registration');
Route::any('country', 'API\UserController@country');
Route::any('state', 'API\UserController@state');
Route::post('forgot-password', 'API\UserController@forgot_password');
Route::post('otp-verification', 'API\UserController@verify_otp');
Route::post('login', 'API\UserController@login');
Route::post('update-password', 'API\UserController@update_password');
Route::post('GetVersion', 'API\UserController@app_version');
Route::post('getContant', 'API\ContantController@getContant');
Route::post('VerifyEmail', 'API\UserController@email_send_opt');
Route::group(['middleware' => ['validToken']], function () {
    Route::post('ChangePassword', 'API\UserController@ChangePassword');
    Route::post('SubscriptionPlan', 'API\SubscriptionController@getSubscription');
    Route::post('PackageDetails', 'API\SubscriptionController@SubscriptionDetail');
    Route::post('AccountOverView', 'API\SubscriptionController@account_over_view');
    Route::post('UpdateProfile', 'API\UserController@UpdateProfile');
    Route::post('Subscription', 'API\SubscriptionController@subscription');
    Route::post('CancelSubscription', 'API\SubscriptionController@CancelSubscription');
    Route::post('BookingDetails', 'API\SubscriptionController@booking_detail');
    Route::post('BillingDetails', 'API\SubscriptionController@BillingDetails');
    Route::post('ManualRenewPlan', 'API\SubscriptionController@ManualRenewPlan');
    Route::post('delteSelfAccount', 'API\UserController@delteSelfAccount');
    Route::namespace('API')->group(function () {
        Route::any('FindGym', 'FindgymController@gymlist');
        Route::any('FindGymDetails', 'FindgymController@gymdetails');
        Route::any('CheckInGymlist', 'FindgymController@check_in_gymlist');
        Route::any('GymCheckIn', 'FindgymController@gym_check_in');
        Route::any('HomeScreen', 'SubscriptionController@homescreen');
        //21-10-2019
        Route::post('Payment', 'PaymentController@save_payment');
        Route::post('FreePayment', 'PaymentController@save_payment_without_card');
        Route::post('PaymentJazzcash', 'PaymentController@save_payment_jazzcash');
        Route::post('Trial', 'PaymentController@enableTrail');
        Route::post('payfast', 'PaymentController@payfast');

        Route::post('token', 'PaymentController@generate_token');
        //11-11-2019
        Route::post('UpdateCard', 'PaymentController@update_card');
        //28-11-2019
        Route::post('CouponCode', 'CouponController@coupon');
    });

});
//06-1-20

Route::post('payout', 'API\PayoutController@index');
Route::post('LastDayPlan', 'API\AutorenewalController@fetch_last_day_subscription_plan');
//Route::get('lastsub','API\CanclesubscriptionController@auto_cancle_subscription');
Route::post('pay_out', 'Payout\PayoutController@index');
Route::post('payoutcharge', 'Payout\PayoutController@payoutcharge');
Route::get('TransferToPayout', 'Payout\PayoutController@TransferToPayout');
Route::get('balance', 'Payout\PayoutController@retrive_stripe_balance');
Route::post('update_account', 'Payout\PayoutController@update_account');
Route::post('account_delete', 'Payout\PayoutController@account_delete');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
