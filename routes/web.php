<?php


//use DB;
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

use App\User;
use Illuminate\Support\Facades\DB;

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('user_logout', '\App\Http\Controllers\UserAuth\LoginController@logout')->name('signout');
Route::get('account/verify/{token}', function ($token) {
    $verifyUser = DB::table('users_verify')->where('token', $token)->first();
    $message = 'Sorry your email cannot be identified.';

    if(!is_null($verifyUser) ){
        $user = User::where('email' , $verifyUser->user_email)->first();

        if($user) {
            if (!$user->is_email_verified) {
                $user->is_email_verified = 1;
                $user->save();
                $message = "Your e-mail is verified. You can now login.";
            } else {
                $message = "Your e-mail is already verified. You can now login.";
            }
        }else{
            $message = 'User not Found! Please contact Administrator';
        }
    }
    return view('verified_email' , get_defined_vars());
});
Route::get('/', function () {
    $db['country'] = DB::table('country')->select('id', 'nicename')->where('id', 162)->get();
    $db['state'] = DB::table('state')->select('id', 'state')->where('country_id', 162)->orderBy('state', 'ASC')->get();
    // Pro Pass
    $db['active_gym'] = DB::table('gymcategories')->join('users_gym', 'users_gym.id', '=', 'gymcategories.user_gym_id')
        ->where(function ($query) {
            $query->where([['gymcategories.status', 1], ['users_gym.status', 1], ['users_gym.is_delete', 1]])->whereIN('gymcategories.plan_category_id', [1, 2, 3, 4]);
        })->groupBy('gymcategories.user_gym_id')->get()->count();

    // Standard Pass
    $db['silver_gym_count'] = DB::table('gymcategories')->join('users_gym', 'users_gym.id', '=', 'gymcategories.user_gym_id')
        ->where(function ($query) {
            $query->where([['gymcategories.status', 1], ['users_gym.status', 1], ['users_gym.is_delete', 1]])->whereIN('gymcategories.plan_category_id', [1, 2]);
        })->groupBy('gymcategories.user_gym_id')->get()->count();

    // Lite Pass
    $db['gold_gym_count'] = DB::table('gymcategories')->join('users_gym', 'users_gym.id', '=', 'gymcategories.user_gym_id')
        ->where(function ($query) {
            $query->where([['gymcategories.status', 1], ['users_gym.status', 1], ['users_gym.is_delete', 1]])->whereIN('gymcategories.plan_category_id', [1]);
        })->groupBy('gymcategories.user_gym_id')->get()->count();

    $db['lite_price'] = DB::table('subscriptions')->where('id', '1')->first()->amount;
    $db['standard_price'] = DB::table('subscriptions')->where('id', '2')->first()->amount;
    $db['pro_price'] = DB::table('subscriptions')->where('id', '3')->first()->amount;
//     echo '<pre>';  print_r( $db);die;
    return view('welcome', $db);
});

//Route::get('/payfast','Admin\TestController@payfast')->name('payfastPage') ;
Route::get('/pay-fast', 'API\PaymentController@payfast')->name('payfastPage');
Route::get('/pay-fast-fail', 'API\PaymentController@payFastFail')->name('payFastFailPage');
Route::get('/pay-fast/processed', 'API\PaymentController@payfastprocessed')->name('payfastprocessed');

//Route::get('/payfast', function () {
//    return view('web.payfast');
//});


Route::get('/storage-link', function () {
    Artisan::call('storage:link');
});

Route::get('/all-clear', function () {
    Artisan::call('optimize:clear');
    return "Compiled views cleared!<br/>
            Application cache cleared!<br/>
            Route cache cleared!<br/>
            Configuration cache cleared!<br/>
            Compiled services and packages files removed!<br/>
            Caches cleared successfully!";
});

Route::get('/cache-clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    return "Cache and Config cache is cleared";
});
Route::get('/cache-config', function () {

    Artisan::call('config:clear');

    return "Config Cache is cleared";
});

Route::get('/view-clear', function () {

    Artisan::call('view:clear');

    return "View Cache is cleared";
});

Route::get('/qrcode', function () {

    return view('qr_code');
});

Route::get('page-unavailable', function () {
    return view('notfound');
})->name('notfound');

Route::get('blogs-list', function () {
    return view('blogs-list');
})->name('blogs-list');

Route::get('blog-details', function () {
    return view('blog-details');
})->name('blog-details');

Route::get('corporate-partner', 'CorporatePartnerController@index')->name('corporate-partner');
Route::post('corporate-partner', 'CorporatePartnerController@store')->name('add_corporate_partner');

Route::get('terms-conditions', 'ContantmanagmentController@getterms_conditions')->name('terms_conditions');
Route::get('privacy-policy', 'ContantmanagmentController@getprivacy_policy')->name('privacy_policy');
Route::get('refund-policy', 'ContantmanagmentController@getrefund_policy')->name('refund_policy');

//09-01-2020
Route::get('about-us', 'ContantmanagmentController@get_about_us')->name('aboutus');
Route::get('faq', 'ContantmanagmentController@get_faq')->name('faq');
Route::get('how-its-work', 'ContantmanagmentController@get_how_its_work')->name('h_i_w');
//end
Route::post('add-user', 'Admin\LandinguserController@store')->name('adduser');
Auth::routes();
//Admin Routes

Route::namespace('Admin')->middleware('admin')->group(function () {
    //gym owner rooute
    Route::get('gym-list', 'GymownerController@gymlist')->name('gymlist');

    //corporate rooute
    Route::get('corporate', 'CorporateController@corporatelist')->name('corporatelist');
    Route::get('corporatestatus/{id}/{status}', 'CorporateController@corporate_status')->name('corporate_status');
    Route::get('registered-corporate', 'CorporateController@corporate_list')->name('corporate_list');
    Route::get('corporateCheckIn', 'CorporateController@checkInList')->name('corporateCheckIn');


    Route::get('blog-list','BlogController@index')->name('blogs');
    Route::get('blog-status/{id}/{status}','BlogController@updateStatus')->name('blog-status');
    Route::get('add-blog','BlogController@create')->name('add-blog');
    Route::post('create-blog','BlogController@store')->name('blog-store');
    Route::get('blog-edit/{id}','BlogController@edit')->name('blog-edit');
    Route::post('blog-update/{id}','BlogController@update')->name('blog-update');
    Route::get('blog-delete/{id}','BlogController@destroy')->name('blog-delete');

    Route::get('news-list','NewsController@index')->name('news');
    Route::get('news-status/{id}/{status}','NewsController@updateStatus')->name('news-status');
    Route::get('add-news','NewsController@create')->name('add-news');
    Route::post('create-news','NewsController@store')->name('news-store');
    Route::get('news-edit/{id}','NewsController@edit')->name('news-edit');
    Route::post('news-update/{id}','NewsController@update')->name('news-update');
    Route::get('news-delete/{id}','NewsController@destroy')->name('news-delete');

    Route::get('add-corporation', 'CorporateController@create')->name('add_corporation');
    Route::post('insert-corporation', 'CorporateController@store')->name('store-corporation');

    Route::get('edit-corporation/{id}', 'CorporateController@edit')->name('edit_corporation');
    Route::post('update-corporation/{id}', 'CorporateController@update')->name('update-corporation');

    Route::get('delete-corporate/{id}', 'CorporateController@delete_corporate')->name('delete_corporate');


    Route::get('get-subscription', 'CorporateController@getSubscription')->name('get-subscription');

    Route::get('corporate-employees/{id}/{name}', 'CorporateEmployeeController@employeelist')->name('corporate-employee-list');
    Route::get('corporate-employee-status/{id}/{status}', 'CorporateEmployeeController@corporate_employee_status')->name('corporate_employee_status');
    Route::get('registered-corporate-employee/{id}/{name}', 'CorporateEmployeeController@corporate_employee_list')->name('corporate_employee_list_name');

    Route::get('add-corporation-employee/{status?}/{corporateId}/{name}', 'CorporateEmployeeController@create')->name('add_corporation_employee');

    Route::post('insert-corporation-employee', 'CorporateEmployeeController@store')->name('store-corporation-employee');

    Route::get('edit-corporation-employee/{id}', 'CorporateEmployeeController@edit')->name('edit_corporation_employee');
    Route::post('update-corporation-employee/{id}', 'CorporateEmployeeController@update')->name('update-corporation-employee');

    Route::get('delete-corporate-employee/{id}/{cId}/{cName}', 'CorporateEmployeeController@delete_corporate_employee')->name('delete_corporate_employee');
    Route::get('send-email/{id}', 'CorporateEmployeeController@sendCredentials')->name('send_email');


    Route::get('pdf/{id}', 'TestController@generate_qr_code_pdf')->name('pdf');

//    Route::get('pdf_download', 'TestController@run_all');


    Route::get('add-gym', 'GymownerController@create')->name('add_gym');
    Route::post('insert-gym', 'GymownerController@store')->name('store');
    Route::get('registered-gym-owner', 'GymownerController@gym_owner_list')->name('gym_owner_list');
    Route::get('gym-details/{id}', 'GymownerController@gym_profile_detail')->name('gym_details');
    Route::get('gym-details/{id}/{status}', 'GymownerController@gym_status')->name('gym_status');
    //29-02-2020
    Route::get('delete-gym/{id}', 'GymownerController@delete_gym')->name('delete_gym');
    //04-06-2020
    Route::get('reset-gym-owner-password', 'GymownerController@resetpassword')->name('resetpassword');
    Route::post('change-gym-owner-password', 'GymownerController@changepassword')->name('change_owner_password');

    Route::get('gym-edit/{id}', 'GymownerController@edit_gym')->name('edit_gym');
    Route::get('check-in-history/{id}', 'GymownerController@check_in_history')->name('check_in');
    Route::get('check-in-history/{id}', 'GymownerController@check_in_history')->name('check_in_history_data');
    Route::get('CheckInHistory/{type}/{gym_id}', 'GymownerController@GetCheckInHistory')->name('check_in_history'); //30-10-2019
    Route::post('update-gym/{id}', 'GymownerController@update')->name('update_gym');
    Route::get('delete-image/{id}', 'GymownerController@delete_image');
    Route::get('request-gym-list/{type}', 'GymownerController@gym_owner_list')->name('request_user_list');

    //06-11-2019
    Route::get('gym-plan', 'GymownerController@gym_plan')->name('gym_plan');
    Route::post('add-gym-plan', 'GymownerController@add_gym_plan')->name('add_gym_plan');
    Route::get('gym-plan-list', 'GymownerController@gym_plan_list')->name('gym_plan_list');
    Route::get('delete/{id}', 'GymownerController@delete_plan')->name('delete_gym_plan');

    //add gym owner in admin panel route 14-01-2020
    //Route::get('add-gym-partner', 'GymownerController@gym_partner_add')->name('gym_partner_add');
    Route::get('add-gym-partner', 'TestController@index')->name('gym_partner_add');

    //Landing user list
    Route::get('user-list', 'LandinguserController@index')->name('user_list');
    Route::get('Become/Partner', 'LandinguserController@becomepatner')->name('become_patener');
    Route::get('Contact-us', 'LandinguserController@contact_us')->name('contact_us_admin');
    Route::get('Contact-us-delete/{id?}', 'LandinguserController@contact_us_delete')->name('contact_us_delete_admin');
    //Route::post('add-user','LandinguserController@store')->name('content');
    //Change Password
    Route::get('admin-change-password', 'LandinguserController@ChangePassword');
    Route::post('ChangePassword', 'LandinguserController@update_password')->name('change_password');


    //add subscription
    Route::get('subscription-list', 'SubscriptionController@index')->name('subscription_list');
    Route::get('create-subscription', 'SubscriptionController@create')->name('create_sub');
    Route::get('subscription-status/{id}/{status}','SubscriptionController@updateStatus')->name('subscription-status');
    Route::get('edit-subscription/{id}', 'SubscriptionController@edit')->name('update_sub');
    Route::get('delete-subscription/{id}', 'SubscriptionController@delete_customer')->name('delete_sub');
    Route::get('edit-customer/{id}', 'SubscriptionController@edit_customer')->name('edit_customer');
    Route::post('update-customer/{id}', 'SubscriptionController@update_customer')->name('update_customer');
    Route::get('delete-subscription-plan/{id}', 'SubscriptionController@delete_sub_plan')->name('delete_sub_plan');
    Route::post('add-subscription', 'SubscriptionController@store')->name('add_sub');
    Route::post('update-subscription/{id}', 'SubscriptionController@update')->name('update_subscription');

    //User Subscription route
    Route::get('subscriber-list/', 'SubscriptionController@subscriberlist')->name('subscriberlist');
    Route::get('inactiveSubscription/', 'SubscriptionController@inactiveSubscription')->name('inactiveSubscription');
    Route::get('activeSubscription/', 'SubscriptionController@activeSubscription')->name('activeSubscription');
    Route::get('changeSubscriptionStatus/{subscriptionId}/{status}', 'SubscriptionController@changeSubscriptionStatus')->name('changeSubscriptionStatus');
    Route::get('subscriberlist', 'SubscriptionController@ajaxsubscriber')->name('ajaxsubscriberlist');
    Route::get('subscribercorporatelist', 'SubscriptionController@corporate_list')->name('ajaxsubscriberlistcorporate');
    //Corporate user CheckIn detail
    Route::get('corporateEmployeeCheckInDetail/{corporateId?}', 'CorporateEmployeeCheckInDetailController@index')->name('corporateEmployeeCheckInDetail');
    Route::get('corporate-employee-checkin-list/{corporateId?}', 'CorporateEmployeeCheckInDetailController@checkInDetailList')->name('corporate-employee-checkin-list');
    Route::get('delete-corporate-employee-check-in/{id}', 'CorporateEmployeeCheckInDetailController@delete_check_in')->name('delete_corporate_employee_check_in');

    Route::get('getsubscribe_user/{id}', 'SubscriptionController@getsubscribe_user');
    Route::get('edit_user_subscription/{id}', 'SubscriptionController@EditUserSubscription')->name('edit_user_subscription');
    Route::post('update_user_subscription/{id}', 'SubscriptionController@UpdateUserSubscription')->name('update_user_subscription');
    Route::get('delete_user_subscription/{id}', 'SubscriptionController@DeleteUserSubscription')->name('delete_user_subscription');

    Route::get('add-subscriber', 'SubscriptionController@addSubscriber')->name('add-subscriber');
    Route::post('addUserSubscription', 'SubscriptionController@addUserSubscription')->name('addUserSubscription');
    Route::get('getSubscriptions/{plan_category?}', 'SubscriptionController@getSubscriptions')->name('getSubscriptions');

    //////cancel user subscription 03-11-2021
    Route::get('cancel-user-subscription/{uId?}', 'SubscriptionController@CancelSubscription')->name('cancel-user-subscription');

    //Customer Management
    Route::get('customer-management', 'CustomermanagementController@index')->name('customerlist');
    Route::get('customer-management-list', 'CustomermanagementController@cus_man_list')->name('cus_man_list');
    Route::get('userstatus/{id}/{status}', 'CustomermanagementController@user_status')->name('user_status');
    Route::get('user-info/{id}', 'CustomermanagementController@user_info')->name('user_detail');
    Route::post('update-image/{id}', 'CustomermanagementController@update_image')->name('update_image');
    Route::get('download-user-report', 'CustomermanagementController@download_user_report')->name('download_user_report');
    Route::post('set-user-report', 'CustomermanagementController@set_user_report')->name('set_user_report');
    Route::get('download-plan', 'CustomermanagementController@download_current_plan')->name('download_current_plan');

    ///Add Customer User
    Route::get('add-customer', 'UserController@create')->name('add_customer');

    Route::post('insert-customer', 'UserController@store')->name('store-customer');

    //user Bank detail
    Route::get('userBankDetail', 'UserBankDetailController@index')->name('userBankDetail');
    Route::get('bank-detail-list', 'UserBankDetailController@bankDetailList')->name('bank-detail-list');

    //user CheckIn detail
    Route::get('userCheckInDetail', 'UserCheckInDetailController@index')->name('userCheckInDetail');
    Route::get('user-checkin-list', 'UserCheckInDetailController@checkInDetailList')->name('user-checkin-list');
    Route::get('delete-check-in/{id}', 'UserCheckInDetailController@delete_check_in')->name('delete_check_in');

    Route::get('create-check-in', 'UserCheckInDetailController@create')->name('create_check_in');
    Route::post('add-check-in', 'UserCheckInDetailController@store')->name('add_check_in');

    Route::get('get-user-subscription/{id?}', 'UserCheckInDetailController@getUserSubscription')->name('get_user_subscription');
    Route::get('get-user-gyms/{id?}', 'UserCheckInDetailController@getUserGyms')->name('get_user_gyms');

    //Payment History
    Route::get('paymentsHistory', 'TestController@paymentHistory')->name('paymentsHistory');

    Route::get('send-transaction-email/{id}', 'TestController@paymentHistoryEmail')->name('send-transaction-email');


    //Revenue History
    Route::get('revenueHistory', 'RevenueHistoryController@index')->name('revenueHistory');
    //Route::post('gym-revenue-detail', 'RevenueHistoryController@getrevenueData')->name('gymrevenue.details');
    Route::get('gym-revenue-detail', 'RevenueHistoryController@newGetrevenueData')->name('gymrevenue.details');

    Route::post('gym_payment_period', 'RevenueHistoryController@payment_period')->name('gym_payment_period');

    Route::post('get_payment_period_admin', 'RevenueHistoryController@payment_period')->name('get_payment_period_admin');
    Route::post('select_box_user_id_admin', 'RevenueHistoryController@select_box_user')->name('select_box_user_admin');
    Route::post('gym_earn_amount_admin', 'RevenueHistoryController@gym_earn_amount')->name('gym_earn_amount_admin');
    Route::get('crevenue/{id}/{start_date}/{end_date}', 'RevenueHistoryController@check_in_user_histroy')->name('crevenue');
    Route::post('UserCheckInAdmin', 'RevenueHistoryController@check_in_histroy')->name('user_check_in_histroy_admin');

    //Rates Management
    Route::get('rates', 'RateController@index')->name('rate');
    Route::post('add-rate', 'RateController@store')->name('add_rate');
    Route::post('update-rate/{id}', 'RateController@update')->name('update_rate');

    //Facility Management
    Route::get('facilities', 'FacilityController@index')->name('facilities');
    Route::get('facilities/{id}', 'FacilityController@index')->name('edit_facilities');
    Route::get('delete-facilities/{id}', 'FacilityController@destroy')->name('delete_facilities');
    Route::post('update-facilities/{id}', 'FacilityController@update')->name('update_faci');
    Route::post('add-facilities', 'FacilityController@store')->name('add_facilities');
    //add plan
    Route::get('plan-list', 'PlanController@index')->name('plan_list');
    Route::get('plan-list/{plan}/{status}', 'PlanController@change_status')->name('plan_list.status');
    Route::get('delete-plan/{id}', 'PlanController@delete_plan')->name('delete_plan');
    Route::get('add-plan', 'PlanController@create')->name('add_plan');
    Route::post('store-plan', 'PlanController@store')->name('store_plan');
    Route::post('update-plan/{id}', 'PlanController@update')->name('update_plan');
    Route::get('edit-plan/{id}', 'PlanController@edit')->name('edit_plan');

    //coupon routes 28-11-2019
    Route::get('coupon-code-list', 'CouponController@index')->name('coupon_list');
    Route::get('delete-coupon/{id}', 'CouponController@delete_coupon')->name('delete_coupon');
    Route::get('add-coupon-code', 'CouponController@create')->name('add_coupon');
    Route::post('store-coupon-code', 'CouponController@store')->name('store_coupon');
    Route::get('ajax-coupon-list', 'CouponController@coupon_list')->name('ajax_coupon_list');
    Route::get('inactive-status/{id}', 'CouponController@inactive_status')->name('inactive_status');

    Route::get('notification', 'CouponController@get_Notifi_index')->name('notification');
    Route::post('send-notification', 'CouponController@send_notification')->name('send_notification');


    //Upload Report 31-12-2019
    Route::get('upload-reports', 'UploadreportController@index')->name('uploadreport');
    Route::post('save-reports', 'UploadreportController@importFileIntoDB')->name('savereport');

    //delete gym partner and also gyms
    Route::get('delete_gym_owner/{id}', 'GymownerController@delete_gym_owner')->name('delete_gym_owner');
    Route::get('edit-gym-owner/{id}', 'GymownerController@edit_gym_owner')->name('edit_gym_owner');
    Route::post('update-gym-owner/{id}', 'GymownerController@update_gym_owner')->name('update_gym_owner');

    //delete user 17-08-2021
    Route::get('delete-user-data/{email?}', 'UserController@deleteUserData');

});


Route::namespace('Corporate')->middleware('webcorporate')->group(function () {

    Route::get('employees', 'EmployeeController@employeelist')->name('employee-list');
    Route::get('employee-status/{id}/{status}', 'EmployeeController@employee_status')->name('employee_status');
    Route::get('registered-employee', 'EmployeeController@employee_list')->name('employee_list_name');

    Route::get('add-employee', 'EmployeeController@create')->name('add_employee');

    Route::post('insert-employee', 'EmployeeController@store')->name('store-employee');

    Route::get('employee-info/{id}/{name}', 'EmployeeController@user_info')->name('employee_detail');

    Route::get('edit-employee/{id}', 'EmployeeController@edit')->name('edit_employee');
    Route::post('update-employee/{id}', 'EmployeeController@update')->name('update-employee');

//    Route::get('delete-employee/{id}/{cId}/{cName}', 'EmployeeController@delete_employee')->name('delete_employee');

    Route::get('send-email-employee/{id}', 'EmployeeController@sendCredentials')->name('send_email_employee');

    //user CheckIn detail
    Route::get('employeeCheckInDetail', 'EmployeeCheckInDetailController@index')->name('employeeCheckInDetail');
    Route::get('employee-checkin-list', 'EmployeeCheckInDetailController@checkInDetailList')->name('employee-checkin-list');
    Route::get('delete-employee-check-in/{id}', 'EmployeeCheckInDetailController@delete_check_in')->name('delete_employee_check_in');

    Route::get('create-employee-check-in', 'EmployeeCheckInDetailController@create')->name('create_employee_check_in');
    Route::post('add-employee-check-in', 'EmployeeCheckInDetailController@store')->name('add_employee_check_in');

    Route::get('change-corporate-password', 'EmployeeController@changeCorporatePassword')->name('changeCorporatePassword');
    Route::post('change-corporate-password', 'EmployeeController@updateCorporatePassword')->name('updateCorporatePassword');


});

Route::group(['middleware' => ['webcorporate']], function () {
    Route::get('/corporate-home', 'HomeController@show')->name('corporate-home');
});

Route::group(['middleware' => ['admin']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
});

// Login any User
Route::get('login_account/{id}' , 'HomeController@login_account')->name('login_account');


Route::get('/blogs', 'BlogController@index')->name('all-blog');
Route::get('/blogs/{slug}', 'BlogController@detail')->name('blog-detail');

Route::get('/news', 'NewsController@index')->name('all-news');
Route::get('/news/{slug}', 'NewsController@detail')->name('news-detail');

//Content
Route::get('content-list', 'ContantmanagmentController@index')->name('content');
Route::post('add-content', 'ContantmanagmentController@store')->name('add-content');
Route::post('update-content/{id}', 'ContantmanagmentController@update')->name('update-content');

//14-10-2019
Route::get('Become-Partner', 'BecomepatnerController@index')->name('becomepatner');
Route::post('Add-Become-Patner', 'BecomepatnerController@store')->name('add_become_patner');
Route::post('Admin-Add-Become-Patner', 'Admin\TestController@store')->name('admin_add_become_patner');
// 25-10-2020
// upload cvs file for gym
Route::post('upload-gym-partner', 'Admin\TestController@upload_gym_partner_file')->name('upload_gym_partner');
Route::post('upload-gym-owner', 'Admin\TestController@uploadgymownercsv')->name('upload_gym_owner');
Route::post('upload-corporate-emplyee/{id}', 'Admin\TestController@uploadCorporateEmployeeCsv')->name('upload_corporate_emplyee');
Route::post('upload-payments', 'Admin\TestController@uploadPayments')->name('upload_payments');
Route::post('upload-gymplans', 'Admin\TestController@uploadGymPlans')->name('upload_gymplans');

//01-11-2019
//website route start here...
Route::namespace('Web')->group(function () {
    Route::get('sign-in', 'UserController@index')->name('singin');
    Route::get('sign-up', 'UserController@create')->name('singup');
    Route::post('sing-up-user', 'UserController@store')->name('user_submit');
    Route::get('forgate-password', 'UserController@forgate_password')->name('forgate_password');
    Route::post('forgate', 'UserController@forgate_pass')->name('forgate_pass');
    //Route::get('forgate-otp','UserController@forgate_')->name('forgate_pass');
    Route::post('otp-check', 'UserController@otp_check')->name('otp_check');
    Route::get('reset-password', 'UserController@new_password')->name('new_password');
    Route::get('otp', 'UserController@otp')->name('otp');
    Route::post('update_password', 'UserController@update_password')->name('update_password');
});

Route::post('sing-in', 'UserAuth\LoginController@login')->name('signin.submit');
Route::get('user-define', 'UserAuth\UserdefineController@index');


Route::namespace('WebGymUser')->group(function () {
    //user home
    Route::get('User-Home', 'UsergymhomeController@index')->name('userhome');
    Route::get('billingDetail', 'UsergymhomeController@billing_histroy')->name('billing_histroy');
    Route::get('CheckInHistroy', 'UsergymhomeController@check_in_histroy')->name('check_in_histroy');
    Route::post('update_profile', 'UsergymhomeController@update_profile')->name('update_profile');
    Route::get('change-password', 'UsergymhomeController@change_password')->name('UserPasswordChange');
    Route::post('change-password', 'UsergymhomeController@password')->name('UserPassword');

    //find gym
    Route::get('find-gym', 'FindgymController@index')->name('findgym');
    Route::get('find-gym-new', 'FindgymController@index_new')->name('findgymnew');
    Route::get('find-gym-by-plan', 'FindgymController@find_gym_by_plan')->name('find_gym_by_plan');
    //Route::get('googlemap', 'FindgymController@map');
    Route::get('direction', 'FindgymController@direction');

    //subscription plan
    Route::get('membership-plan', 'SubscriptionController@index')->name('subscriptionplan');
    //Route::post('plan-wise_gym', 'SubscriptionController@plan_wise_gym')->name('plan_wise_gym_list');
    Route::get('gyms/{plan_id}', 'SubscriptionController@plan_wise_gym')->name('plan_wise_gym_list');
    Route::post('ajax-gym-list', 'SubscriptionController@plan_wise_gym_list')->name('ajax_plan_wise_gym_list');

    Route::get('subscription-plan/{plan_id}', 'SubscriptionController@subscription_plan')->name('sub_plan');
    Route::any('subscription-plan-detail', 'SubscriptionController@subscription_plan_detail')->name('sub_plan_detail');
    Route::post('get-price-detail', 'SubscriptionController@ajax_price')->name('ajax_price');
    Route::get('card-detail', 'SubscriptionController@card_detail')->name('card_detail');
    // Route::get('card-detail/{plan_id}/{base_amount}/{gst}/{total_amount}/{total_payable_amount}/{coupon_id}/{coupon_name}/{coupon_discount}/{coupon_discount_amount}', 'SubscriptionController@card_detail')->name('apply_coupon_card_detail');
    Route::post('make-payment', 'PaymentController@make_payment')->name('make_payment');
    Route::post('apply-promo-code', 'SubscriptionController@apply_promo_code')->name('apply_promo_code');
    //10-12-19
    Route::post('get_lat_long', 'FindgymController@getlatlong')->name('get_lat_long');
    Route::post('ajax_find_gym', 'FindgymController@ajax_find_gym')->name('ajax_find_gym');
    Route::post('ajax_find_sidebar', 'FindgymController@find_sidebar')->name('ajax_find_side_gym_list');
    Route::post('ajax_find_detail', 'FindgymController@find_gym_detail')->name('ajax_find_gym_detail');
    Route::post('check_in', 'FindgymController@gym_check_in')->name('check_in');

    //contact us
    Route::get('contact-us', 'ContactusController@index')->name('contactus');
    Route::post('add-contact-us', 'ContactusController@store')->name('submit_contactus');

    //cancel subscription
    Route::post('cancel_subscription', 'SubscriptionController@CancelSubscription')->name('cancel_subscription');

    // gym owner timing
    Route::get('gym-timing', 'UsergymhomeController@edit_gymtiming')->name('UserGymTiming');
});

//gym patner routes  //22-11-2019
Route::namespace('WebGymOwner')->middleware('webuser')->group(function () {
    Route::get('Home', 'PatnerhomeController@index')->name('ufppatnerhome');
    Route::get('add-staff', 'PatnerhomeController@addstaff')->name('addstaff');
    Route::post('store-staff', 'PatnerhomeController@store_staff')->name('store_staff');
    Route::post('update-staff', 'PatnerhomeController@update_staff')->name('update_staff');
    Route::post('updatepasswordpartner', 'PatnerhomeController@update_pass')->name('update_pass'); //first login
    Route::get('payment-history', 'PaymenthistoryController@index')->name('paymenthistory');
    Route::get('payment-download', 'PaymenthistoryController@paymentDownload')->name('paymentDownload');

    //add bank detail
    Route::post('add-bank-detail', 'PaymenthistoryController@add_bank_detail')->name('add_bank_detaiil');
    Route::post('update_bank_detaiil/{id}', 'PaymenthistoryController@update_bank_detail')->name('update_bank_detaiil');

    //check in detail
    Route::get('check-in-detail', 'CheckinController@index')->name('checkin');
    Route::post('UserCheckIn', 'CheckinController@check_in_histroy')->name('user_check_in_histroy');
    Route::get('download', 'CheckinController@exportFile')->name('check_in_export');
    Route::post('gym_earn_amount', 'CheckinController@gym_earn_amount')->name('gym_earn_amount');
    Route::post('select_box_user_id', 'CheckinController@select_box_user')->name('select_box_user');
    //payment period
    Route::post('get_payment_period', 'CheckinController@payment_period')->name('get_payment_period');
    //payment history
    Route::post('get_payment_month', 'PaymenthistoryController@get_payment_month')->name('get_payment_month');
    //Route::post('month_wise_user', 'PaymenthistoryController@month_wise_user')->name('month_wise_user');
    Route::post('month_wise_user', 'PaymenthistoryController@month_wise_user_manual')->name('month_wise_user');

    //edit update gym
    Route::get('edit-gym/{gym_id}', 'EditgymController@index')->name('owner_edit_gym');
    Route::post('update/{gym_id}', 'EditgymController@update')->name('owner_update_gym');

    //delete image
    Route::get('delete-gym-image/{id}', 'EditgymController@delete_image');

});

/*Route::namespace('WebGymOwner')->middleware('webstaff')->group(function () {

    Route::get('Home', 'PatnerhomeController@index')->name('ufppatnerhome');
    Route::post('updatepasswordpartner', 'PatnerhomeController@update_pass')->name('update_pass'); //first login
    Route::get('payment-history', 'PaymenthistoryController@index')->name('paymenthistory');

    //add bank detail
    Route::post('add-bank-detail', 'PaymenthistoryController@add_bank_detail')->name('add_bank_detaiil');
    Route::post('update_bank_detaiil/{id}', 'PaymenthistoryController@update_bank_detail')->name('update_bank_detaiil');

    //check in detail
    Route::get('check-in-detail', 'CheckinController@index')->name('checkin');
    Route::post('UserCheckIn', 'CheckinController@check_in_histroy')->name('user_check_in_histroy');
    Route::get('download', 'CheckinController@exportFile')->name('check_in_export');
    Route::post('gym_earn_amount', 'CheckinController@gym_earn_amount')->name('gym_earn_amount');
    Route::post('select_box_user_id', 'CheckinController@select_box_user')->name('select_box_user');
    //payment period
    Route::post('get_payment_period', 'CheckinController@payment_period')->name('get_payment_period');
    //payment history
    Route::post('get_payment_month', 'PaymenthistoryController@get_payment_month')->name('get_payment_month');
    //Route::post('month_wise_user', 'PaymenthistoryController@month_wise_user')->name('month_wise_user');
    Route::post('month_wise_user', 'PaymenthistoryController@month_wise_user_manual')->name('month_wise_user');

    //edit update gym
    Route::get('edit-gym/{gym_id}','EditgymController@index')->name('owner_edit_gym');
    Route::post('update/{gym_id}','EditgymController@update')->name('owner_update_gym');

    //delete image
    Route::get('delete-gym-image/{id}', 'EditgymController@delete_image');

});*/


Route::post('add-account', 'Payout\PayoutController@SaveAccount')->name('add_account');
Route::post('update-account/{user_id}', 'Payout\PayoutController@update_add_account')->name('update_add_account');
Route::get('transfer-to-account', 'Payout\PayoutController@TransferToPayout');
Route::get('delete-stripe-account/{id}', 'Payout\PayoutController@account_delete')->name('delete_stripe_account');
Route::any('test/{id}', function ($id) {
    $select = "select city from users_gym where id=$id";
    return $data = DB::select($select);
});



