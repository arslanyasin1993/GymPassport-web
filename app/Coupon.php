<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Couponplan;
use DB;
class Coupon extends Model {

    protected $fillable = ['coupon_code', 'no_of_coupon', 'discount', 'valid_from', 'valid_to', 'image', 'description', 'status', 'offline_coupon','valid_for','total_use_coupon', 'is_one_time_use_coupon' , 'corporate_id'];

    public function select_data($order_by, $offset, $limit_t, $startdate, $enddate, $search) {
        $query = self::query();
        $query->where('status', 1);
        $query->where('is_delete', 1);
        $query->with('coupon_plan');
        if (!empty($startdate) && !empty($enddate)) {
            $query->where('valid_from','<=',$startdate);
            $query->where('valid_to','>=',$enddate);
        }
        $query->Where(function($q) use ($search) {
            $q->orWhere('coupon_code', 'like', '%' . $search . '%');
        });
        $query->skip($offset);
        $query->take($limit_t);
        $query->orderBy('coupon_code', $order_by);
        //$data = $query->toSql();
        $data = $query->get();
        return $data;
    }

    public function select_data_count($startdate, $enddate, $search) {
        $query = self::query();
        $query->where('status', 1);
        $query->where('is_delete', 1);
        $query->with('coupon_plan');
        if (!empty($startdate) && !empty($enddate)) {
            $query->where('valid_from','<=',$startdate);
            $query->where('valid_to','>=',$enddate);
        }
        $query->Where(function($q) use ($search) {
            $q->orWhere('coupon_code', 'like', '%' . $search . '%');
        });
        //$data = $query->toSql();
        $data = $query->get();
        $count = $data->count();
        return $count;
    }

    public function coupon_plan() {
        return $this->hasMany(Couponplan::class, 'coupon_id', 'id');
    }
    
    public function api_coupon($plan_cat_id,$search,$offset,$limit){
        $data = DB::table('coupons as c')
                ->leftjoin('coupon_plan as cp','c.id','=','cp.coupon_id')
                ->leftjoin('plan_category as pc','pc.id','cp.plan_cat_id')
                ->where('c.status','1')
                ->where('cp.plan_cat_id',$plan_cat_id)
                ->where('c.valid_from','<=',date('Y-m-d'))
                ->where('c.valid_to','>=',date('Y-m-d'))
                ->where('c.coupon_code', 'like', '%' .$search. '%')
                ->select('c.id as coupon_id','c.coupon_code','c.discount','c.valid_from','c.valid_to','c.image','c.description','c.status','pc.plan_cat_name')
                ->skip($offset)
                ->take($limit) 
                ->get();
        return $data;
                
    }

}
