<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table = "payments";
    protected $fillable = [
        'date', 'trans_id','amount','gym_name','gym_owner_email','email_sent'
    ];
    public $timestamps = false;
    //
}
