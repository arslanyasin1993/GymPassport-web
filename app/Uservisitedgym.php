<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Usergym;
use App\User;
use App\API\Usersubscription;
use App\Payoutdate;
use Carbon\Carbon;

class Uservisitedgym extends Model {

    protected $table = "user_visited_gym";
    protected $fillable = ['user_id', 'unique_id' , 'gym_id', 'user_check_in', 'user_check_out', 'usersubscription_id', 'check_in_status', 'gym_earn_amount'];

//11-10-2019
    public function homescreendata($user_id, $user_sub_id) {
        // echo $user_id.$user_sub_id;die;
        $sql = 'SELECT uvg.user_check_in,ug.gym_name,ug.gym_address FROM user_visited_gym uvg LEFT JOIN users_gym ug ON ug.id=uvg.gym_id WHERE uvg.user_id="' . $user_id . '" AND uvg.usersubscription_id="' . $user_sub_id . '" ORDER BY uvg.id DESC LIMIT 1';
        $data = DB::select(DB::raw($sql));
        return $data;
    }

    //25-10-2019

    public function check_in_details($user_id = null, $startdate = null, $enddate = null, $search = null, $offset = null, $limit = null, $columnName=null,$orderBy=null)
    {
        if ($user_id) {
            if ($startdate && $enddate){
                $data = self::with('gym_name')->with('plan_detail.plan_category')->where('user_id', $user_id)->whereBetween('user_check_in', [$startdate.' 00:00:00', $enddate.' 23:59:59'])->distinct()->orderBy('id', 'desc')->get();//->toArray();
            }else{
                $data = self::with('gym_name')->with('plan_detail.plan_category')->where('user_id', $user_id)->distinct()->orderBy('id', 'desc')->get();//->toArray();
            }
        } else {
            $data = DB::table('user_visited_gym as a')
                ->select('a.*', 'u.first_name', 'u.last_name', 'u.user_ufp_id', 'u.email', 'u.phone_number', 'g.gym_name',
                    'p.plan_cat_name');
            if ($startdate && $enddate) {
                $data = $data->whereBetween('a.user_check_in', [$startdate.' 00:00:00', $enddate.' 23:59:59']);
            }

            if ($search) {
//                $data = $data->orwhere('u.first_name', 'LIKE', '%' . $search . '%');
                $data = $data->where(function ($q) use ($search) {
                    $q->whereRaw("CONCAT(u.first_name, ' ', u.last_name) LIKE ?", ['%'.$search.'%'])
                        ->orWhere('u.email', 'like', '%' . $search . '%')
                        ->orWhere('u.phone_number', 'like', '%' . $search . '%')
                        ->orWhere('u.user_ufp_id', 'like', '%' . $search . '%')
                        ->orWhere('g.gym_name', 'like', '%' . $search . '%');
                });
            }
            $data = $data->leftjoin('users as u', 'u.id', '=', 'a.user_id')
                ->leftjoin('users_gym as g', 'g.id', '=', 'a.gym_id')
                ->leftjoin('usersubscriptions as us', 'us.id', '=', 'a.usersubscription_id')
                ->leftjoin('subscriptions as s', 's.id', '=', 'us.subscription_id')
                ->leftjoin('plan_category as p', 'p.id', '=', 's.plan_name')
               ->orderBy($columnName,$orderBy)
                ->offset($offset)
                ->limit($limit)
                ->get();

        }
        return $data;
    }

    public function check_in_details_amount($user_id = null, $startdate = null, $enddate = null , $search = null){
        if ($user_id) {
            $data = self::with('gym_name')->with('plan_detail.plan_category')->where('user_id', $user_id)->distinct()->orderBy('id', 'desc')->get();//->toArray();
        } else {
            $data = DB::table('user_visited_gym as a')
                ->select('a.*', 'u.first_name', 'u.last_name', 'u.user_ufp_id', 'u.email', 'u.phone_number', 'g.gym_name',
                    'p.plan_cat_name');
            if ($startdate && $enddate) {
                $data = $data->whereBetween('a.user_check_in', [$startdate.' 00:00:00', $enddate.' 23:59:59']);
            }
            if ($search) {
//                $data = $data->orwhere('u.first_name', 'LIKE', '%' . $search . '%');
                $data = $data->where(function ($q) use ($search) {
                    $q->whereRaw("CONCAT(u.first_name, ' ', u.last_name) LIKE ?", ['%'.$search.'%'])
                        ->orWhere('u.email', 'like', '%' . $search . '%')
                        ->orWhere('u.phone_number', 'like', '%' . $search . '%')
                        ->orWhere('u.user_ufp_id', 'like', '%' . $search . '%')
                        ->orWhere('g.gym_name', 'like', '%' . $search . '%');
                });
            }

            $data = $data->leftjoin('users as u', 'u.id', '=', 'a.user_id')
                ->leftjoin('users_gym as g', 'g.id', '=', 'a.gym_id')
                ->leftjoin('usersubscriptions as us', 'us.id', '=', 'a.usersubscription_id')
                ->leftjoin('subscriptions as s', 's.id', '=', 'us.subscription_id')
                ->leftjoin('plan_category as p', 'p.id', '=', 's.plan_name')
                ->sum('a.gym_earn_amount');

        }
        return $data;
    }

    public function gym_name() {
        return $this->belongsTo(Usergym::class, 'gym_id', 'id')->select(['id', 'gym_name', 'gym_logo', 'gym_address']);
    }

    //06-03-2020 for use month list user in payment page

     public function check_in_subscription() {
        return $this->belongsTo(Usersubscription::class, 'usersubscription_id', 'id');
    }

//30-10-2019
    public function user_name() {
        return $this->belongsTo(User::class, 'user_id', 'id')->select(['id', 'first_name', 'last_name','user_ufp_id' , 'email', 'user_image']);
    }

    //30-10-2019

    public function get_check_in_date($type, $gym_id, $offset, $limit_t) {
        //echo $type;die;
        //DB::enableQueryLog();
        $query = self::query();
        $query->with('user_name');
        $query->where('gym_id', $gym_id);
        if ($type == '1') { //daily base
            //echo 'hee';die;
            $query->whereDate('user_check_in', '=', date('Y-m-d'));
        }
        if ($type == '2') { //weekly based
           //$week = date("W");
            $startdate = date('Y-m-d',strtotime('- 1 week'));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
        }
        if ($type == '3') { //month based
            //$month = date("m");
            $startdate = date('Y-m-d',strtotime("- 1 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
        }
        if ($type == '4') { //Year based
           // $year = date("Y");
            $startdate = date('Y-m-d',strtotime("- 1 year"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
        }
         if ($type == '5') { //3 month based
            //$month = date("m");
            $startdate = date('Y-m-d',strtotime("- 3 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
        }
         if ($type == '6') { //6 month based
            //$month = date("m");
            $startdate = date('Y-m-d',strtotime("- 6 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
        }
         if ($type == '7') { //9 month based
            //$month = date("m");
            $startdate = date('Y-m-d',strtotime("- 9 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
        }
        $query->orderBy('user_check_in', 'DESC');
        $query->skip($offset);
        $query->take($limit_t);
        $data = $query->get();//->toArray();
      // $rquery = DB::getQueryLog();
       //print_r($rquery);exit;
        //$data = $query->toSql();
        return $data;
    }

    public function get_check_in_date_total($type, $gym_id) {
        $query = self::query();
        $query->with('user_name');
        $query->where('gym_id', $gym_id);
        if ($type == '1') {
            $query->whereDate('user_check_in', '=', date('Y-m-d'));
            //die;
        }
        if ($type == '2') { //weekly based
            $startdate = date('Y-m-d',strtotime('- 1 week'));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
           // $query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '3') { //month based
            $startdate = date('Y-m-d',strtotime("- 1 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '4') { //Year based
            $startdate = date('Y-m-d',strtotime("- 1 year"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '5') { //3 month based
            $startdate = date('Y-m-d',strtotime("- 3 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '6') { //6 month based
            $startdate = date('Y-m-d',strtotime("- 6 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '7') { //9 month based
            $startdate = date('Y-m-d',strtotime("- 9 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        $query->orderBy('id', 'DESC');
        $data = $query->get();
        $count = $data->count();
        return $count;
    }

    //16-12-2019 for exel download
    public function get_excel_check_in($gym_id , $gym_type , $startdate , $end_date) {
        //echo $type;die;
        $query = self::query();
        $query->with('user_name');
        $query->where('gym_id', $gym_id);
        if($gym_type){
            if ($gym_type == '1') {
                $query->whereDate('user_check_in', '=', date('Y-m-d'));
                //die;
            }
            if ($gym_type == '2') { //weekly based
                $startdate = date('Y-m-d',strtotime('- 1 week'));
                $end_date = date('Y-m-d');
                $query->Where(function($q) use ($startdate,$end_date) {
                     $q->whereDate('user_check_in', '>=', $startdate);
                     $q->whereDate('user_check_in', '<=', $end_date);
                 });
                //$query->whereBetween('user_check_in', [$startdate, $end_date]);
            }
            if ($gym_type == '3') { //month based
                $startdate = date('Y-m-d',strtotime("- 1 month"));
                $end_date = date('Y-m-d');
                $query->Where(function($q) use ($startdate,$end_date) {
                     $q->whereDate('user_check_in', '>=', $startdate);
                     $q->whereDate('user_check_in', '<=', $end_date);
                 });
                //$query->whereBetween('user_check_in', [$startdate, $end_date]);
            }
            if ($gym_type == '4') { //Year based
                $startdate = date('Y-m-d',strtotime("- 1 year"));
                $end_date = date('Y-m-d');
                $query->Where(function($q) use ($startdate,$end_date) {
                     $q->whereDate('user_check_in', '>=', $startdate);
                     $q->whereDate('user_check_in', '<=', $end_date);
                 });
                //$query->whereBetween('user_check_in', [$startdate, $end_date]);
            }
             if ($gym_type == '5') { //3 month based
                $startdate = date('Y-m-d',strtotime("- 3 month"));
                $end_date = date('Y-m-d');
                $query->Where(function($q) use ($startdate,$end_date) {
                     $q->whereDate('user_check_in', '>=', $startdate);
                     $q->whereDate('user_check_in', '<=', $end_date);
                 });
                //$query->whereBetween('user_check_in', [$startdate, $end_date]);
            }
            if ($gym_type == '6') { //6 month based
                $startdate = date('Y-m-d',strtotime("- 6 month"));
                $end_date = date('Y-m-d');
                $query->Where(function($q) use ($startdate,$end_date) {
                     $q->whereDate('user_check_in', '>=', $startdate);
                     $q->whereDate('user_check_in', '<=', $end_date);
                 });
                //$query->whereBetween('user_check_in', [$startdate, $end_date]);
            }
            if ($gym_type == '7') { //9 month based
                $startdate = date('Y-m-d',strtotime("- 9 month"));
                $end_date = date('Y-m-d');
                $query->Where(function($q) use ($startdate,$end_date) {
                     $q->whereDate('user_check_in', '>=', $startdate);
                     $q->whereDate('user_check_in', '<=', $end_date);
                 });
                //$query->whereBetween('user_check_in', [$startdate, $end_date]);
            }
        }else{
            $query->Where(function($q) use ($startdate,$end_date) {
                $q->whereDate('user_check_in', '>=', $startdate);
                $q->whereDate('user_check_in', '<=', $end_date);
            });
        }
        $query->orderBy('user_check_in', 'DESC');
        $data = $query->get(); //->toArray();
        //$data = $query->toSql();
        return $data;
    }

    //17-12-2019 for exel download
    public function total_gym_earn_amount($gym_id , $gym_type , $startdate , $end_date , $payout_date=NULL) {
//        $query = self::query();
//        $query->where('gym_id', $gym_id);
//        if ($type == '1') {
//            $query->whereDate('user_check_in', '=', date('Y-m-d'));
//        }
//        if ($type == '2') { //weekly based
//            $startdate = date('Y-m-d',strtotime('- 1 week'));
//            $end_date = date('Y-m-d');
//            $query->Where(function($q) use ($startdate,$end_date) {
//                 $q->whereDate('user_check_in', '>=', $startdate);
//                 $q->whereDate('user_check_in', '<=', $end_date);
//             });
//        }
//        if ($type == '3') { //month based
//            $startdate = date('Y-m-d',strtotime("- 1 month"));
//            $end_date = date('Y-m-d');
//            $query->Where(function($q) use ($startdate,$end_date) {
//                 $q->whereDate('user_check_in', '>=', $startdate);
//                 $q->whereDate('user_check_in', '<=', $end_date);
//             });
//        }
//        if ($type == '4') { //Year based
//            $startdate = date('Y-m-d',strtotime("- 1 year"));
//            $end_date = date('Y-m-d');
//            $query->Where(function($q) use ($startdate,$end_date) {
//                 $q->whereDate('user_check_in', '>=', $startdate);
//                 $q->whereDate('user_check_in', '<=', $end_date);
//             });
//        }
        //$data = $query->sum('gym_earn_amount'); //->toArray();
        //$data = $query->toSql();

        if($gym_type){
            if ($gym_type == '1') {
                $startdate = date('Y-m-d');
            }
            if($gym_type == '2'){
                $startdate = date('Y-m-d',strtotime('- 1 week'));
            }
            if($gym_type == '3'){
                $startdate = date('Y-m-d',strtotime("- 1 month"));
            }
            if($gym_type == '4'){
                $startdate = date('Y-m-d',strtotime("- 1 year"));
            }
            if($gym_type == '5'){
                $startdate = date('Y-m-d',strtotime("- 3 month"));
            }
            if($gym_type == '6'){
                $startdate = date('Y-m-d',strtotime("- 6 month"));
            }
            if($gym_type == '7'){
                $startdate = date('Y-m-d',strtotime("- 9 month"));
            }
            $end_date = date('Y-m-d');
        }

        $last_month = date('m',strtotime("-1 month"));
        $current_month = date('m');
        // if($payout_date > date('Y-m-d')){
        //    // echo 'cddcfddffd';die;
        //     $pay_out_month = date('m', strtotime($payout_date));
        //     if($pay_out_month == $current_month){
        //        //echo 'dffdfdffd';die;
        //         $select = "select SUM(uvg.gym_earn_amount) as total_amount from user_visited_gym uvg LEFT JOIN usersubscriptions us ON us.id=uvg.usersubscription_id where uvg.gym_id = '$gym_id' and (date(uvg.user_check_in) >= '$startdate' and date(uvg.user_check_in) <= '$end_date') AND us.visit_pass !='month'";// AND (MONTH(uvg.user_check_in)='$last_month')";// AND (MONTH(us.expired_at)='$last_month')
        //     }else{
                
        //         $select = "select SUM(uvg.gym_earn_amount) as total_amount from user_visited_gym uvg LEFT JOIN usersubscriptions us ON us.id=uvg.usersubscription_id where uvg.gym_id = '$gym_id' and (date(uvg.user_check_in) >= '$startdate' and date(uvg.user_check_in) <= '$end_date') AND us.visit_pass !='month'";// AND (MONTH(uvg.user_check_in)='$current_month')";//AND (MONTH(us.expired_at)='$current_month')
        //     }
        //     $data = DB::select($select);
        // }else{
        //     //echo 'fdf';die;
        //     $select = "select SUM(uvg.gym_earn_amount) as total_amount from user_visited_gym uvg LEFT JOIN usersubscriptions us ON us.id=uvg.usersubscription_id where uvg.gym_id = '$gym_id' and (date(uvg.user_check_in) >= '$startdate' and date(uvg.user_check_in) <= '$end_date') AND us.visit_pass !='month'";// AND (MONTH(uvg.user_check_in)='$last_month')";// AND (MONTH(us.expired_at)='$last_month')
        //     $data = DB::select($select);
        // }
        $select = "select SUM(uvg.gym_earn_amount) as total_amount from user_visited_gym uvg LEFT JOIN usersubscriptions us ON us.id=uvg.usersubscription_id where uvg.gym_id = '$gym_id' and (date(uvg.user_check_in) >= '$startdate' and date(uvg.user_check_in) <= '$end_date')";// AND (MONTH(uvg.user_check_in)='$last_month')";// AND (MONTH(us.expired_at)='$last_month')
        $data = DB::select($select);
        return ['data' => $data , 'startdate' => $startdate , 'enddate' => $end_date];
    }

    //17-12-2019 for select month and month total amount
    public function get_month_and_amount($gym_id) {
        $month = (date('d-m') > date("05-m")) ? date('m',strtotime('-1 month')) :date('m',strtotime('-2 month')); 
        $select = 'SELECT DISTINCT month(user_check_in) as month,MONTHNAME(user_check_in) as month_name,RIGHT(YEAR(user_check_in), 2) as year,(SELECT SUM(gym_earn_amount) FROM user_visited_gym WHERE month(user_check_in)= month) as total_earn_amount FROM user_visited_gym WHERE gym_id=' . $gym_id . ' AND month(user_check_in) <=' . $month . ' ORDER BY month DESC';
        $data = DB::select($select);
        return $data;
    }

    public function get_month_wise_user($gym_id, $month) {
        //DB::enableQueryLog();
        $current_month = date('m');
        $query = self::query();
        $query->with('user_name');
        $query->where('gym_id', $gym_id);
        $query->whereMonth('user_check_in', $month);
        //$query->whereMonth('user_check_in', '<',$current_month);
        $query->WhereHas('check_in_subscription',function($q) use ($month){
            $q->where('visit_pass','!=','month');
        });
        $query->orderBy('user_check_in', 'DESC');
        $data = $query->get()->toArray();
       // dd(DB::getQueryLog()); 
        return $data;
    }
     public function get_month_wise_plan_user($gym_id, $month) {
        //DB::enableQueryLog();
        $current_month = date('m');
        $query = self::query();
        $query->with('user_name');
        $query->where('gym_id', $gym_id);
        $query->WhereHas('check_in_subscription',function($q) use ($month){
            $q->whereMonth('expired_at',$month);
        });
        $query->orderBy('user_check_in', 'DESC');
        $data = $query->get()->toArray();
       // dd(DB::getQueryLog()); 
        return $data;
    }
    
    
    //06-1-2020
    public function user_total_visit($user_id, $gym_id, $type){
        $query = self::query();
        $query->where('gym_id', $gym_id);
        $query->where('user_id', $user_id);
        if ($type == '1') { //daily base
            //echo 'hee';die;
            $query->whereDate('user_check_in', '=', date('Y-m-d'));
        }
        if ($type == '2') { //weekly based
           //$week = date("W");
            $startdate = date('Y-m-d',strtotime('- 1 week'));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
        }
        if ($type == '3') { //month based
            //$month = date("m");
            $startdate = date('Y-m-d',strtotime("- 1 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
        }
        if ($type == '4') { //Year based
           // $year = date("Y");
            $startdate = date('Y-m-d',strtotime("- 1 year"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
        }
         if ($type == '5') { //3 month based
            $startdate = date('Y-m-d',strtotime("- 3 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '6') { //6 month based
            $startdate = date('Y-m-d',strtotime("- 6 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '7') { //9 month based
            $startdate = date('Y-m-d',strtotime("- 9 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        $data = $query->get();
        return $total = $data->count();
    }
    
    //08-1-2020
    
    public function get_month_and_gym_amount($gym_id, $month){
        $current_month = date('m');
        
        //$select = "select SUM(gym_earn_amount) as total from `user_visited_gym` where `gym_id` = '$gym_id' and month(`user_check_in`) = '$month'";
        //$select = "select SUM(uvg.gym_earn_amount) as total from user_visited_gym uvg LEFT JOIN usersubscriptions us ON us.id=uvg.usersubscription_id where uvg.gym_id = '$gym_id' AND MONTH(uvg.user_check_in) = '$month' AND MONTH(us.expired_at) = '$month'";
        //$select = "select SUM(uvg.gym_earn_amount) as total from user_visited_gym uvg LEFT JOIN usersubscriptions us ON us.id=uvg.usersubscription_id where uvg.gym_id = '$gym_id' AND MONTH(uvg.user_check_in) = '$month' AND MONTH(us.expired_at) = '$month' AND MONTH(uvg.user_check_in) < '$current_month'";
        $select = "SELECT pt.amount_transfer as total FROM payout_transfer pt LEFT JOIN users ON users.id=pt.gym_partner_id LEFT JOIN users_gym ug ON ug.users_id=users.id WHERE ug.id='$gym_id' AND (MONTH(pt.created_at)-1) = '$month'";
        $data = DB::select($select);
        return $data;
    }


    // 15-1-2019 for for use check in history on partner side.

//    public function total_visit_user_wise_check_page($type, $gym_id, $offset, $limit_t){
//        if ($type == '1') { //daily base
//            $startdate = date('Y-m-d');
//        }
//        if($type == '2'){
//            $startdate = date('Y-m-d',strtotime('- 1 week'));
//        }
//        if($type == '3'){
//            $startdate = date('Y-m-d',strtotime("- 1 month"));
//        } 
//        if($type == '4'){
//            $startdate = date('Y-m-d',strtotime("- 1 year"));
//        }
//        $end_date = date('Y-m-d');
//         echo $select = 'SELECT DISTINCT uvg.user_id,COUNT(uvg.user_id) as total_visit,u.user_ufp_id,u.first_name,u.last_name,u.user_image,(SELECT COUNT(id) FROM user_visited_gym WHERE gym_id != "'.$gym_id.'" AND user_id = uvg.user_id) as other_gym_visit FROM user_visited_gym uvg LEFT JOIN users u ON u.id=uvg.user_id where uvg.gym_id="'.$gym_id.'" and (date(uvg.user_check_in) >= "'.$startdate.'" and date(uvg.user_check_in) <= "'.$end_date.'") GROUP BY uvg.user_id LIMIT '.$offset.','.$limit_t.'';die;
//        $data = DB::select($select);
//        return $data;
//    }
    
    // 20-2-20 for use checkin page in use gym patner side
    public function total_visit_user_wise_check_page($gym_id , $gym_type , $startdate , $end_date , $offset , $limit_t){
        if($gym_type){
            if ($gym_type == '1') {
                $startdate = date('Y-m-d');
            }
            if($gym_type == '2'){
                $startdate = date('Y-m-d',strtotime('- 1 week'));
            }
            if($gym_type == '3'){
                $startdate = date('Y-m-d',strtotime("- 1 month"));
            }
            if($gym_type == '4'){
                $startdate = date('Y-m-d',strtotime("- 1 year"));
            }
            if($gym_type == '5'){
                $startdate = date('Y-m-d',strtotime("- 3 month"));
            }
            if($gym_type == '6'){
                $startdate = date('Y-m-d',strtotime("- 6 month"));
            }
            if($gym_type == '7'){
                $startdate = date('Y-m-d',strtotime("- 9 month"));
            }
            $end_date = date('Y-m-d');
        }

        $select = 'SELECT us.user_id,u.first_name,u.last_name,u.user_ufp_id,u.user_image, us.id as subscription_id,uvg.user_check_in,pc.plan_cat_name,us.visit_pass,us.status,(SELECT COUNT(id) FROM user_visited_gym '
                 . 'WHERE user_visited_gym.usersubscription_id=us.id AND user_visited_gym.gym_id="'.$gym_id.'") as this_gym_total_visit,'
                 . '(SELECT COUNT(id) FROM user_visited_gym WHERE user_visited_gym.usersubscription_id=us.id) as other_gym_total_visit'
                 . ',(SELECT user_check_in FROM user_visited_gym WHERE user_visited_gym.usersubscription_id=us.id AND user_visited_gym.gym_id ="'.$gym_id.'" AND user_visited_gym.user_id=us.user_id ORDER BY user_check_in DESC LIMIT 1) as last_visit_date FROM usersubscriptions us LEFT JOIN user_visited_gym uvg ON us.id=uvg.usersubscription_id '
                 . 'LEFT JOIN plan_category pc ON pc.id=us.plan_name LEFT JOIN users u ON u.id=uvg.user_id WHERE uvg.gym_id="'.$gym_id.'" AND (date(uvg.user_check_in) >= "'.$startdate.'" and date(uvg.user_check_in) <= "'.$end_date.'") ORDER BY uvg.user_check_in DESC LIMIT '.$offset.','.$limit_t.'';
        $data = DB::select($select);
        return $data;
    }

    public function total_visit_user_wise_check_page_count($gym_id , $gym_type , $startdate , $end_date){
        if($gym_type){
            if ($gym_type == '1') {
                $startdate = date('Y-m-d');
            }
            if($gym_type == '2'){
                $startdate = date('Y-m-d',strtotime('- 1 week'));
            }
            if($gym_type == '3'){
                $startdate = date('Y-m-d',strtotime("- 1 month"));
            }
            if($gym_type == '4'){
                $startdate = date('Y-m-d',strtotime("- 1 year"));
            }
            if($gym_type == '5'){
                $startdate = date('Y-m-d',strtotime("- 3 month"));
            }
            if($gym_type == '6'){
                $startdate = date('Y-m-d',strtotime("- 6 month"));
            }
            if($gym_type == '7'){
                $startdate = date('Y-m-d',strtotime("- 9 month"));
            }
            $end_date = date('Y-m-d');
        }

        $select = 'SELECT uvg.user_id,u.user_ufp_id,u.first_name,u.last_name,u.user_image FROM user_visited_gym uvg LEFT JOIN users u ON u.id=uvg.user_id where uvg.gym_id="'.$gym_id.'" and (date(uvg.user_check_in) >= "'.$startdate.'" and date(uvg.user_check_in) <= "'.$end_date.'") ';
        $data = DB::select($select);
        return $data;
    }

    public function user_all_visit_list($gym_id , $gym_type , $startdate , $end_date , $user_id , $subscription_id=NULL){
          if($gym_type){
            if ($gym_type == '1') {
                $startdate = date('Y-m-d');
            }
            if($gym_type == '2'){
                $startdate = date('Y-m-d',strtotime('- 1 week'));
            }
            if($gym_type == '3'){
                $startdate = date('Y-m-d',strtotime("- 1 month"));
            }
            if($gym_type == '4'){
                $startdate = date('Y-m-d',strtotime("- 1 year"));
            }
            if($gym_type == '5'){
                $startdate = date('Y-m-d',strtotime("- 3 month"));
            }
            if($gym_type == '6'){
                $startdate = date('Y-m-d',strtotime("- 6 month"));
            }
            if($gym_type == '7'){
                $startdate = date('Y-m-d',strtotime("- 9 month"));
            }
            $end_date = date('Y-m-d');
          }
        $query = self::query();
        $query->with('gym_name');
        $query->with('get_subscription.plan_category');
        $query->where('gym_id', $gym_id);
        $query->where('user_id', $user_id);
        $query->where('usersubscription_id', $subscription_id);
        $query->Where(function($q) use ($startdate,$end_date) {
             $q->whereDate('user_check_in', '>=', $startdate);
             $q->whereDate('user_check_in', '<=', $end_date);
         });
        $query->orderBy('user_check_in', 'DESC');
        $data = $query->get();
        return $data;
    }

    //end 15-01-20 code

    public function last_visit_user_wise($gym_id,$user_id){
        $data = self::select('user_check_in')->where('gym_id',$gym_id)->where('user_id',$user_id)->orderBy('id','DESC')->first();
        return $data;
    }

    //23-1-2020
    public function total_estimate_amt_admin($gym_id){
        $data = self::where('gym_id',$gym_id)->sum('gym_earn_amount');
        return $data;
    }

    //27-01-2020 get user_visit_plan for checkin history

    public function get_subscription() {
        return $this->belongsTo(Usersubscription::class, 'usersubscription_id', 'id')->select(['id', 'plan_name', 'visit_pass','purchased_at','expired_at']);
    }
    
     //10-02-2020 for select record for transfer payout amount
    //rechange 04-03-220

    public function get_for_TarnsferPayout($gym_owner_id, $month, $payout_date){
      //echo  $select = "select SUM(uvg.gym_earn_amount) as total from user_visited_gym uvg INNER JOIN users_gym ug ON ug.id=uvg.gym_id WHERE ug.users_id='$gym_owner_id' AND month(uvg.user_check_in) = '$month'";die;
       $select = "select SUM(uvg.gym_earn_amount) as total from user_visited_gym uvg INNER JOIN users_gym ug ON ug.id=uvg.gym_id LEFT JOIN usersubscriptions us on us.id=uvg.usersubscription_id WHERE
ug.users_id='$gym_owner_id' AND month(uvg.user_check_in) = '$month' AND us.visit_pass !='month'";// AND MONTH(us.expired_at) = '$month'
       $data = DB::select($select);
        return $data;
    }
    
    //04-03-2020 for month pass user get amount to use payout
    public function get_for_TarnsferPayout_month($gym_owner_id, $month, $payout_date){
      //echo  $select = "select SUM(uvg.gym_earn_amount) as total from user_visited_gym uvg INNER JOIN users_gym ug ON ug.id=uvg.gym_id WHERE ug.users_id='$gym_owner_id' AND month(uvg.user_check_in) = '$month'";die;
       $select = "select SUM(uvg.gym_earn_amount) as total from user_visited_gym uvg INNER JOIN users_gym ug ON ug.id=uvg.gym_id LEFT JOIN usersubscriptions us on us.id=uvg.usersubscription_id WHERE
        ug.users_id='$gym_owner_id' AND MONTH(us.expired_at) = '$month' AND us.visit_pass ='month'";
       $data = DB::select($select);
        return $data;
    }
    
    //12-02-2020
    
    public function get_plan_name_for_excel($usersubscription_id){
        $select = "SELECT DISTINCT pc.id as plan_cat_id,pc.plan_cat_name FROM plan_category pc LEFT JOIN usersubscriptions us ON pc.id=us.plan_name LEFT JOIN user_visited_gym uvg ON uvg.usersubscription_id= us.id and uvg.user_id=us.user_id WHERE uvg.usersubscription_id='$usersubscription_id'";
        $data = DB::select($select);
        return $data;
        
    }
    //17-02-2020 for use user detail page on admin side 
    public function plan_detail() {
        return $this->belongsTo(Usersubscription::class, 'usersubscription_id', 'id')->select(['id', 'plan_name', 'visit_pass', 'plan_duration','plan_time']);
    }
    
    //19-02-2020 for user report from admin panel
    
     public function check_in_user_report_from_admin($type, $user_id) {
        //echo $type;die;
        $query = self::query();
        $query->with('user_name');
        $query->with('gym_name');
        $query->where('user_id', $user_id);
        if ($type == '1') { //weekly based
            $startdate = date('Y-m-d',strtotime('- 1 week'));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '2') { //month based
            $startdate = date('Y-m-d',strtotime("- 1 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '3') { //last 6 month based
            $startdate = date('Y-m-d',strtotime("- 6 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '4') { //Year based
            $startdate = date('Y-m-d',strtotime("- 1 year"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
         if ($type == '5') { //3 month based
            $startdate = date('Y-m-d',strtotime("- 3 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '6') { //6 month based
            $startdate = date('Y-m-d',strtotime("- 6 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        if ($type == '7') { //9 month based
            $startdate = date('Y-m-d',strtotime("- 9 month"));
            $end_date = date('Y-m-d');
            $query->Where(function($q) use ($startdate,$end_date) {
                 $q->whereDate('user_check_in', '>=', $startdate);
                 $q->whereDate('user_check_in', '<=', $end_date);
             });
            //$query->whereBetween('user_check_in', [$startdate, $end_date]);
        }
        $query->orderBy('user_check_in', 'DESC');
        $data = $query->get(); //->toArray();
        //$data = $query->toSql();
        return $data;
    }
    
    //04-03-2020 for estimated plan
    public function total_gym_earn_amount_month($type, $gym_id, $payout_date=NULL) {
       
        if ($type == '1') {
            $startdate = date('Y-m-d');
        }
        if($type == '2'){
            $startdate = date('Y-m-d',strtotime('- 1 week'));
        }
        if($type == '3'){
            $startdate = date('Y-m-d',strtotime("- 1 month"));
        }
        if($type == '4'){
            $startdate = date('Y-m-d',strtotime("- 1 year"));
        }
         if($type == '5'){
            $startdate = date('Y-m-d',strtotime("- 3 month"));
        }
        if($type == '6'){
            $startdate = date('Y-m-d',strtotime("- 6 month"));
        }
        if($type == '7'){
            $startdate = date('Y-m-d',strtotime("- 9 month"));
        }
        $end_date = date('Y-m-d');
        $last_month = date('m',strtotime("-1 month"));
        $current_month = date('m');
        if($payout_date > date('Y-m-d')){
            $pay_out_month = date('m', strtotime($payout_date));
            if($pay_out_month == $current_month){
                $select = "select SUM(uvg.gym_earn_amount) as total_amount from user_visited_gym uvg LEFT JOIN usersubscriptions us ON us.id=uvg.usersubscription_id where uvg.gym_id = '$gym_id' and (date(uvg.user_check_in) >= '$startdate' and date(uvg.user_check_in) <= '$end_date') AND us.visit_pass ='month'";// AND (MONTH(us.expired_at)='$last_month')";
            }else{
                
                $select = "select SUM(uvg.gym_earn_amount) as total_amount from user_visited_gym uvg LEFT JOIN usersubscriptions us ON us.id=uvg.usersubscription_id where uvg.gym_id = '$gym_id' and (date(uvg.user_check_in) >= '$startdate' and date(uvg.user_check_in) <= '$end_date') AND us.visit_pass ='month'"; //AND (MONTH(us.expired_at)='$current_month')";
            }
            $data = DB::select($select);
        }else{
            $select = "select SUM(uvg.gym_earn_amount) as total_amount from user_visited_gym uvg LEFT JOIN usersubscriptions us ON us.id=uvg.usersubscription_id where uvg.gym_id = '$gym_id' and (date(uvg.user_check_in) >= '$startdate' and date(uvg.user_check_in) <= '$end_date') AND us.visit_pass ='month'";// AND (MONTH(us.expired_at)='$last_month')";
            $data = DB::select($select);
        }
        
        return $data;
    }
    
    
    //05-05-2020 for listing payment
     public function payment_total_earning_amount($gym_id, $month){
       $select = "select SUM(uvg.gym_earn_amount) as total from user_visited_gym uvg INNER JOIN users_gym ug ON ug.id=uvg.gym_id LEFT JOIN usersubscriptions us on us.id=uvg.usersubscription_id WHERE
ug.id='$gym_id' AND month(uvg.user_check_in) = '$month' AND us.visit_pass !='month'";// AND MONTH(us.expired_at) = '$month'
       $data = DB::select($select);
        return $data;
    }
    
    public function payment_total_earning_amount_month($gym_id, $month){
       $select = "select SUM(uvg.gym_earn_amount) as total from user_visited_gym uvg INNER JOIN users_gym ug ON ug.id=uvg.gym_id LEFT JOIN usersubscriptions us on us.id=uvg.usersubscription_id WHERE
        ug.id='$gym_id' AND MONTH(us.expired_at) = '$month' AND us.visit_pass ='month'";
       $data = DB::select($select);
        return $data;
    }

    public function user_detail() {

        return $this->hasMany('App\User', 'id', 'user_id')->select('id','first_name','last_name','user_ufp_id','user_image','status');
    }


}
