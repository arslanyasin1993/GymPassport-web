<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $primaryKey = 'id';
    public $timestamp = true;
    protected $table = 'news';
    protected $fillable = ['title', 'author', 'slug' , 'image', 'description', 'is_active', 'is_deleted'];


}
