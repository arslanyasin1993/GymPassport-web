<?php

namespace App;

use App\API\Usersubscription;
use Illuminate\Database\Eloquent\Model;

class Userusecoupon extends Model
{
    protected $table = 'user_use_coupon';
    protected $fillable = ['usersubscription_id','coupon_id','coupon_code','counpon_discount','user_id','coupon_discount_price'];

//    public function user_subscription()
//    {
//        return $this->hasOne(Usersubscription::class, 'usersubscription_id')->select(array('coupon_code', 'created_at'));
//    }
}
