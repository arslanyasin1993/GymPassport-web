<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facilities extends Model
{
    use SoftDeletes;

    protected $table = 'facilities';

    protected $dates = ['deleted_at'];
    
    protected $fillable = ['facilities','imag_url']; 
    
    public function get_facility(){
        self::select('id','facilities','imag_url')->orderBy('id','DESC')->get();
    }
}
