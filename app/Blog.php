<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public $primaryKey = 'id';
    public $timestamp = true;
    protected $table = 'blogs';
    protected $fillable = ['title', 'author', 'slug' , 'image', 'description', 'is_active', 'is_deleted'];


}
