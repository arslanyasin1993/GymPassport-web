<?php

namespace App;

use App\API\Plancategory;
use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
class Gymcategory extends Model
{
    protected $fillable = ['user_gym_id','plan_category_id'];

    public function get_gym_plan_id($user_plan_category_id){
        $data = self::select('user_gym_id')
				->where('plan_category_id','<=',$user_plan_category_id)
				->get()->toArray();
		return $data;
	} 
        
    //18-11-2019

    public function get_gymname_plan_wise($plan_id,$search){
        $sql = "SELECT a.id as gym_id,a.gym_name,a.gym_logo,a.gym_address FROM users_gym a LEFT JOIN gymcategories b ON a.id=b.user_gym_id WHERE b.plan_category_id <='".$plan_id."' AND a.status='1' AND a.gym_name LIKE '%".$search."%'";
        $data = DB::select($sql);
        return $data;
    }

    //23-01-2020

    public function get_gym_plan($gym_id){
        $select = "SELECT p.plan_cat_name FROM plan_category p LEFT JOIN gymcategories g ON g.plan_category_id=p.id WHERE g.user_gym_id='".$gym_id."'";
        $data = DB::select($select);
        return $data;
    }
    
    //13-02-2019 for subscription plan count gyms
    public function count_total_gym($plan_cat_id){
        $select = "SELECT COUNT(gc.user_gym_id) as total_gym FROM gymcategories gc LEFT JOIN users_gym ug ON gc.user_gym_id=ug.id WHERE gc.plan_category_id <='$plan_cat_id' AND ug.status='1'";
        $data = DB::select($select);
        return $data;
    }

    public function planCategory(){
        return $this->belongsTo(Plancategory::class , 'plan_category_id' , 'id');
    }
}
