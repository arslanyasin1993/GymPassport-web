<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
class Bankdetail extends Model
{
    protected $table ='user_bank_detail';

    protected $fillable = ['user_id','account_id','bank_name','branch_name','account_number','account_name','cnic','business_mcc','business_name','business_product_description'
,'business_url','business_type','capabilities_card_payments','capabilities_transfers','charges_enabled','country','created','default_currency','details_submitted','email','external_accounts_bank_id','external_accounts_bank_account','account_holder_name'
,'account_holder_type','external_accounts_bank_name','external_accounts_fingerprint','external_accounts_last4','external_accounts_routing_number','individual_person_id','individual_person_account','individual_person_first_name','individual_person_last_name','individual_person_phone','additional_document_front_id','additional_document_back_id'
,'document_front_id','document_back_id','account_type','status','external_account_url','company_name','company_phone','role_in_company'
,'d_o_b','state','postal_code','city','current_address','acn_abn','bsb_number','account_number','tax_id'];


public function SaveUserAccountDetail($account_create){
    
    $get_data = self::where('user_id',Auth::User()->id)->first();
    $data['user_id']= Auth::User()->id;
        $data['account_id']= $account_create->id;
        $data['business_mcc']= $account_create->business_profile->mcc;
        $data['business_name']= $account_create->business_profile->name;
        $data['business_product_description']= $account_create->business_profile->product_description;
       // $data['business_support_address']= $account_create->business_profile->support_address;
        //$data['business_support_email']= $account_create->business_profile->support_email;
        //$data['business_support_phone']= $account_create->business_profile->support_phone;
        //$data['business_support_url']= $account_create->business_profile->support_url;
        $data['business_url']= $account_create->business_profile->url;
        $data['business_type']= $account_create->business_type;
        $data['capabilities_card_payments']= $account_create->capabilities->card_payments;
        $data['capabilities_transfers']= $account_create->capabilities->transfers;
        $data['charges_enabled']= $account_create->charges_enabled;
        $data['country']= $account_create->country;
        $data['created']= date('Y-m-d H:i:s',strtotime($account_create->created));
        $data['default_currency']= $account_create->default_currency;
        $data['details_submitted']= $account_create->details_submitted;
        $data['email']= $account_create->email;
        $data['external_accounts_bank_id']= $account_create->external_accounts->data[0]->id;
        $data['external_accounts_bank_account']= $account_create->external_accounts->data[0]->account;
        $data['account_holder_name']= $account_create->external_accounts->data[0]->account_holder_name;
        $data['account_holder_type']= $account_create->external_accounts->data[0]->account_holder_type;
        $data['external_accounts_bank_name']= $account_create->external_accounts->data[0]->bank_name;
        $data['external_accounts_fingerprint']= $account_create->external_accounts->data[0]->fingerprint;
        $data['external_accounts_last4']= $account_create->external_accounts->data[0]->last4;
        $data['external_accounts_routing_number']= $account_create->external_accounts->data[0]->routing_number;
        $data['external_account_url']= $account_create->external_accounts->url;
        $data['document_front_id']= $account_create->company->verification->document->front;
        $data['document_back_id']= $account_create->company->verification->document->back;
        $data['company_name']= $account_create->company->name;
        $data['company_phone']= $account_create->company->phone;
        $data['account_type']= $account_create->type;
    if(!empty($get_data)){
        $insert = self::where('user_id',Auth::User()->id)->update($data);
        Session::flash('msg', 'Your account updated successful');
    }else{
        $insert = self::create($data);
        Session::flash('msg', 'Your account created successful');
    }
    

    if($insert){
        Session::flash('message', 'alert-success');
        return;
    }else{
        Session::flash('msg', 'Something went wrong to save account detail');
        Session::flash('message', 'alert-danger');
        return;
    }

}

//26-02-2020
public function updateUserAccountDetail($create_person){
    $user_id = Auth::User()->id;
    $update = self::where('user_id',$user_id)->update([
        'individual_person_id'=>$create_person->id,
        'individual_person_account'=>$create_person->account,
        'individual_person_first_name'=>$create_person->first_name,
        'individual_person_last_name'=>$create_person->last_name,
        'individual_person_phone'=>$create_person->phone,
        'additional_document_front_id'=>$create_person->verification->additional_document->front,
        'additional_document_back_id'=>$create_person->verification->additional_document->back,
        'role_in_company'=>$create_person->relationship->title,
    ]);
}

//29-02-2020

public function updateUserUserDetail($r){
    $user_id = Auth::User()->id;
    $input = $r->all();
    if(isset($input['dob'])){
       $update['d_o_b']= trim($input['dob']); 
    }
    $update['state']= trim($input['state']);
    $update['postal_code']= trim($input['postal_code']);
    $update['city']= trim($input['city']);
    $update['current_address']= trim($input['line1']);
    $update['acn_abn']= trim($input['tax_id']);
    $update['bsb_number']= trim($input['routing_number']);
    $update['account_number']= trim($input['account_number']);
    $update['tax_id']= trim($input['tax_id']);
    $update_data = self::where('user_id',$user_id)->update($update);
}

//10-2-20

public function selecttoPayout(){
    $data = self::select('user_id','account_id','email','external_accounts_bank_id','external_accounts_bank_account')->get();//->toArray();
    return $data;
}

    public function getbankdetail($start_date, $end_date, $search, $offset, $limit){
        $query = self::query();
        $query->where(function($q) use($search){
            $q->where('bank_name','LIKE', '%'.$search.'%')
            ->orwhere('branch_name','LIKE', '%'.$search.'%')
            ->orwhere('account_number','LIKE', '%'.$search.'%')
            ->orwhere('account_name','LIKE', '%'.$search.'%');
        });
        if(!empty($start_date) && !empty($end_date)){
         $query->whereBetween('created_at',[$start_date,$end_date]);

        }
        $query->orwhereHas('username',function($q) use($search){
            $q->where('first_name','LIKE', '%'.$search.'%');
        });
        $query->orwhereHas('gym_name',function($q) use($search){
            $q->where('gym_name','LIKE', '%'.$search.'%');
        });
       // $query->with('username');
        $query->offset($offset);
        $query->limit($limit);
        return $query->get();
    
    }

    public function username()
    {
        return $this->belongsTo(User::class,'user_id','id')->select('id','first_name','last_name');
       // return $this->belongsToMany(Plancategory::class, 'coupon_plan');
    }
    public function gym_name()
    {
        return $this->belongsTo(Usergym::class,'user_id','id')->select('id','gym_name');
       // return $this->belongsToMany(Plancategory::class, 'coupon_plan');
    }


}
