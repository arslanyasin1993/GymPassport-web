<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorporateSubscription extends Model
{
    protected $table = 'corporate_subscription';

    protected $fillable = ['subscription_id', 'corporate_id', 'corporate_name', 'no_of_employees','per_employee_rate', 'status', 'is_delete'];

    public function corporatesubscription($corporate_id)
    {
        $data = self::select('id', 'corporate_id', 'subscription_id')->where('corporate_id', $corporate_id)->orderBy('id', 'desc')->first();
        return $data;
    }
}
