<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use  App\API\Plancategory;
class Couponplan extends Model
{
    protected $table= 'coupon_plan';

    protected $fillable = ['coupon_id','plan_cat_id'];

    public function plan_category()
    {
        return $this->belongsTo(Plancategory::class, 'plan_cat_id','id')->select(array('id', 'plan_cat_name'));
       // return $this->belongsToMany(Plancategory::class, 'coupon_plan');
    }
}
