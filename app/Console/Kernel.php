<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        '\App\Console\Commands\AutoRenewal',
        '\App\Console\Commands\CancleSubscription',
//        '\App\Console\Commands\PayoutTransfer',
//        '\App\Console\Commands\MonthlyAmount',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('Daily:AutoRenewalPayment')->everyMinute();
        $schedule->command('Daily:CancleSubscription')->everyMinute();
        $schedule->command('Daily:LatestFileSend')->everyMinute();
//        $schedule->command('Monthly:TransferFundPartnerAccount')
//                ->monthlyOn(5, '04:00')->timezone('Australia/Sydney'); 
//        $schedule->command('Monthly:MonthlyAmountUpdateForGym')
//                ->monthlyOn(5, '20:00')->timezone('Australia/Sydney');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
