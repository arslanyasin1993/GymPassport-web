<?php

namespace App\Console\Commands;

use App\Http\Controllers\API\LatestFileSendController;
use Illuminate\Console\Command;

class LatestFileSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Daily:LatestFileSend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send latest DB backup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->latest_file = new LatestFileSendController();


    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->latest_file->send_latest_file();
    }
}
