<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Payout\PayoutController;

class PayoutTransfer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Monthly:TransferFundPartnerAccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transfer fund to Gym partner account which is based on total earn amount on gyms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->payout = new PayoutController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->payout->TransferToPayout();
        $this->info('Transfer amount to gym partner account throught stripe transfer!');
    }
}
