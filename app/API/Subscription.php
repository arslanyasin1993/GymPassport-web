<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;
use Session;
use Illuminate\Support\Facades\Validator;
use App\Country;
use App\API\Plancategory;
class Subscription extends Model
{
    protected  $fillable = ['plan_name','plan_duration','plan_time','amount','description','status','country_id','visit_pass'];

    public function selectdata(){
    	$data = self::select('id','plan_name','plan_duration','plan_time','amount','description','status','country_id','created_at','visit_pass')
            ->with('countryname')
            ->with('plan_category')
                ->orderBy('created_at','DESC')->paginate(10);
        return $data;
    }

    public function for_edit_select($id){
    	$data = self::select('id','plan_name','plan_duration','plan_time','amount','description','status','country_id','created_at','visit_pass')->with('countryname')
    	->where('id',$id)->first();
        return $data;
    }

    public function countryname()
    {
        return $this->belongsTo(Country::class, 'country_id','id')->select(array('id', 'nicename'));
    }

    public function savedata($request){
        $formdata = $request->all();
//        $explode = explode(" ",$formdata['plan_duration']);
//        unset($formdata['plan_duration']);
//        $formdata['plan_duration'] = $explode[0];
//        $formdata['plan_time'] = $explode[1];
        if($formdata['visit_pass']=='10'){
            $formdata['plan_duration'] = '10';//$explode[0];
            $formdata['plan_time'] = 'Day';//$explode[1];
        }else if($formdata['visit_pass']=='6 Month'){
            $formdata['plan_duration'] = '6';//$explode[0];
            $formdata['plan_time'] = 'month';//$explode[1];
        }else if($formdata['visit_pass']=='12 Month'){
            $formdata['plan_duration'] = '12';//$explode[0];
            $formdata['plan_time'] = 'month';//$explode[1];
        }else if($formdata['visit_pass']=='3 Month'){
            $formdata['plan_duration'] = '3';//$explode[0];
            $formdata['plan_time'] = 'month';//$explode[1];
        }else{
            $formdata['plan_duration'] = '30';//$explode[0];
            $formdata['plan_time'] = 'Day';//$explode[1];
        }
        $exits_data = self::where('plan_name',$formdata['plan_name'])
                        ->where('visit_pass',$formdata['visit_pass'])
                        ->where('plan_duration',$formdata['plan_duration'])
                        ->where('plan_time',$formdata['plan_time'])
            ->first();
        //echo '<pre>';print_r($formdata);die;
        if(empty($exits_data)){
                if(self::create($formdata)){
                $msg = trans('lang_data.subscription');
                    Session::flash('msg',$msg);
                    Session::flash('message','alert-success');
                }else{
                $msg = trans('lang_data.error');
                        Session::flash('msg',$msg);
                        Session::flash('message','alert-danger');
            }
        }else{
            $msg = "Plan already exists";
            Session::flash('msg',$msg);
            Session::flash('message','alert-danger');
        }
    }

   public function updatedata($request,$id){
        $formdata = $request->all();
        unset($formdata['_token']);
//        $explode = explode(" ",$formdata['plan_duration']);
//        unset($formdata['plan_duration']);
//        $formdata['plan_duration'] = $explode[0];
//        $formdata['plan_time'] = $explode[1];
        if($formdata['visit_pass']=='10'){
            $formdata['plan_duration'] = '10';//$explode[0];
            $formdata['plan_time'] = 'Day';//$explode[1];
        } else if ($formdata['visit_pass'] == '6 Month') {
            $formdata['plan_duration'] = '6';//$explode[0];
            $formdata['plan_time'] = 'month';//$explode[1];
        } else if ($formdata['visit_pass'] == '3 Month') {
            $formdata['plan_duration'] = '3';//$explode[0];
            $formdata['plan_time'] = 'month';//$explode[1];
        } else if ($formdata['visit_pass'] == '12 Month') {
            $formdata['plan_duration'] = '12';//$explode[0];
            $formdata['plan_time'] = 'month';//$explode[1];
        }else{
            $formdata['plan_duration'] = '30';//$explode[0];
            $formdata['plan_time'] = 'Day';//$explode[1];
        }
        $exits_data = self::where('plan_name',$formdata['plan_name'])
                        ->where('visit_pass',$formdata['visit_pass'])
                        ->where('plan_duration',$formdata['plan_duration'])
                        ->where('plan_time',$formdata['plan_time'])
                        ->where('id','!=',$id)
            ->first();
        // print_r($exits_data);die();
                if($exits_data){
            $msg = "Plan already exists";
                    Session::flash('msg',$msg);
                    Session::flash('message','alert-danger');
                }else{
                    $subscription = self::whereid($id)->update($formdata);
                    if($subscription){
                $msg = trans('lang_data.sub_update');
                        Session::flash('msg',$msg);
                        Session::flash('message','alert-success');
                    }else{
                $msg = trans('lang_data.error');
                            Session::flash('msg',$msg);
                            Session::flash('message','alert-danger');
            }
        }

    }

    //******* 07-10-2019

    public function plan_category()
    {
        return $this->belongsTo(Plancategory::class, 'plan_name','id');
    }
}
