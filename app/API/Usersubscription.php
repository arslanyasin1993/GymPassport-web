<?php

namespace App\API;

use App\API\Plancategory;
use App\CorporateUsers;
use App\Userusecoupon;
use Illuminate\Database\Eloquent\Model;
use  App\User;
use  App\Uservisitedgym;
use DB;

class Usersubscription extends Model
{
    protected $fillable = [
        'customer_id',
        'user_id',
        'subscription_id',
        'transaction_id',
        'status',
        'purchased_at',
        'visit_pass',
        'expired_at',
        'plan_name',
        'plan_duration',
        'plan_time',
        'amount',
        'description',
        'payment_method',
        'currency',
        'charge_id',
        'basket_id',
        'integrated_gst',
        'internet_base_amount',
        'internet_handling_fees',
        'total_payable_amount',
        'payment_status',
        'failure_code',
        'failure_message',
        'btn_status'
    ];


    public function selectdata(){
    	$data = self::select('id','user_id','subscription_id','transaction_id','purchased_at','expired_at','plan_name','plan_duration','plan_time','amount','description')->with('username')
    	->where('status',1)->orderBy('created_at','DESC')->paginate(3);
        return $data;
    }

    public function select_month_year($status){
        $time = ($status !=4) ? 'Month' : 'Year';
        if($status==1){
            $duration=1;
        }elseif($status==2){
             $duration=3;
        }elseif ($status==3) {
            $duration=6;
        }else{
            $duration=1;
        }
        $data = self::select('id','user_id','subscription_id','transaction_id','purchased_at','expired_at','plan_name','plan_duration','plan_time','amount','description')->with('username','username.countryname')->where('status',1)->where('plan_duration',$duration)->where('plan_time',$time)->orderBy('created_at','DESC')->paginate(15);
        return $data;
    }

    public function username()
    {
        return $this->belongsTo(User::class, 'user_id','id')->select(array('id', 'first_name','last_name','email','d_o_b','country','is_corporate_user'));
    }
//20-9-19
     public function user_subscription($id){
        //$data = self::select('id','user_id','subscription_id','transaction_id','purchased_at','expired_at','plan_name','plan_duration','plan_time','amount','description','status')->where('user_id',$id)->orderBy('status','DESC')->get();//paginate(3);
        $data = self::select('id','user_id','visit_pass','subscription_id','transaction_id','purchased_at','expired_at','plan_name','plan_duration','plan_time','amount','description','status')->with('plan_category')->where('user_id',$id)->orderBy('id','DESC')->get();
        return $data;
    }
//10-10-2019

    public function plan_category()
    {
        return $this->belongsTo(Plancategory::class, 'plan_name','id')->select(array('id', 'plan_cat_name'));
    }

//    public function user_use_coupon()
//    {
//        return $this->belongsToMany(Userusecoupon::class,'user_use_coupon','usersubscription_id','id','id')->select(array('coupon_code'));;
//    }

    public function user_use_coupon()
    {
        return $this->hasOne(Userusecoupon::class)->select(array('coupon_code'));
    }

    public function my_visit($user_id, $user_subscription_id)
    {
        $sql ='SELECT ug.gym_name,CAST(uvg.gym_id as CHAR) as gym_id,date_format(uvg.user_check_in,"%d/%m/%Y") as check_in_date FROM user_visited_gym uvg LEFT JOIN users_gym ug ON uvg.gym_id=ug.id WHERE uvg.usersubscription_id="'.$user_subscription_id.'" AND uvg.user_id="'.$user_id.'" order by uvg.user_check_in desc';
        $data = DB::select($sql);
        return $data;
    }

    //14-10-2019
    public function select_subscriber_list($plan_cat, $startdate, $enddate, $search, $offset, $limit_t, $country, $visit_pass,$iscorporate=null){
        //echo $enddate;die;
        $query = self::query();
        $query->with('plan_category');
        $query->where('status' , '!=' , 3);
        $query->where(function($q) use ($search) {
            $q->orWhere('amount' , 'like' , '%' .$search. '%');
            $q->orWhereHas('username', function($q) use ($search) {
                return $q->whereRaw("CONCAT(first_name, ' ', last_name) LIKE ?", ['%'.$search.'%'])
                         ->orWhere('email', 'like', '%' .$search. '%');
            });
        });
        $corporateIds = CorporateUsers::groupBy('corporate_id')->pluck('corporate_id')->all();
//        $corporateUserIds = CorporateUsers::pluck('user_id')->all();
        if (!$iscorporate) {
//            $data_filter = array_merge($corporateUserIds, $corporateIds);
            $query->where('plan_name', $plan_cat);
            $query->whereHas('username', function($q)  {
                return $q->where('is_corporate_user' , 0);
            });
        } else {
            $query->whereIn('user_id', $corporateIds);
        }
        if(!empty($visit_pass)){
            $query->where('visit_pass',$visit_pass);
        }
        if(!empty($startdate) && !empty($enddate)){
            $query->whereBetween('created_at',[$startdate,$enddate]);
        }
        $query->skip($offset);
        $query->take($limit_t);
        $query->orderBy('created_at','DESC');
        $data = $query->get();

        foreach ($data as $item) {
            $userVisitedgym = DB::select("select count(*) as total_checkin, sum(gym_earn_amount) as total_earning from user_visited_gym where check_in_status=1 and user_id = $item->user_id  and usersubscription_id=$item->id group by user_id");

            if ($userVisitedgym) {
                $item->total_checkin = $userVisitedgym[0]->total_checkin;
                $item->total_earning = $userVisitedgym[0]->total_earning;
            } else {
                $item->total_checkin = 0;
                $item->total_earning = 0;
            }
        }
        return $data;
    }

    public function calculate_amounts($plan_cat, $startdate, $enddate, $search, $visit_pass,$iscorporate=null){
        //echo $enddate;die;
        $query = self::query();
//        ->join('user_use_coupon','user_use_coupon.usersubscription_id','usersubscriptions.id');
        $query->with('plan_category');
        //$query->with('username');
        //$query->where('status',1);
        $query->where('status' , '!=' , 3);
        $query->where(function($q) use ($search) {
            $q->orWhere('amount' , 'like' , '%' .$search. '%');
            $q->orWhereHas('username', function($q) use ($search) {
                return $q->whereRaw("CONCAT(first_name, ' ', last_name) LIKE ?", ['%'.$search.'%'])
                         ->orWhere('email', 'like', '%' .$search. '%');
            });
        });
        $corporateIds = CorporateUsers::groupBy('corporate_id')->pluck('corporate_id')->all();
//        $corporateUserIds = CorporateUsers::pluck('user_id')->all();
        if (!$iscorporate) {
//            $data_filter = array_merge($corporateUserIds, $corporateIds);
            $query->where('plan_name', $plan_cat);
            $query->whereHas('username', function($q)  {
                return $q->where('is_corporate_user' , 0);
            });
        } else {
            $query->whereIn('user_id', $corporateIds);
        }
        if(!empty($visit_pass)){
            $query->where('visit_pass',$visit_pass);
        }
        if(!empty($startdate) && !empty($enddate)){
            $query->whereBetween('created_at',[$startdate,$enddate]);
        }
        $data = $query->get();

        $total_checkins = 0;
        $total_checkin_amount = 0;
        foreach ($data as $item) {
            $userVisitedgym = DB::select("select count(*) as total_checkin, sum(gym_earn_amount) as total_earning from user_visited_gym where check_in_status=1 and user_id = $item->user_id  and usersubscription_id=$item->id group by user_id");
            if ($userVisitedgym) {
                $total_checkin_amount = $total_checkin_amount + $userVisitedgym[0]->total_earning;
                $total_checkins = $total_checkins + $userVisitedgym[0]->total_checkin;
            }
        }
        return ['total_earning_amount' => number_format($query->sum('total_payable_amount')) , 'total_checkins' => $total_checkins , 'total_checkin_amount' => number_format($total_checkin_amount)];
    }

    public function count_subscriber_list($plan_cat, $startdate, $enddate, $search, $country, $visit_pass, $iscorporate = null){
        $query = self::query();
        $query->with('plan_category');
        $query->where('status' , '!=' , 3);
        $query->where(function($q) use ($search) {
            $q->orWhere('amount' , 'like' , '%' .$search. '%');
            $q->orWhereHas('username', function($q) use ($search) {
                return $q->whereRaw("CONCAT(first_name, ' ', last_name) LIKE ?", ['%'.$search.'%'])
                    ->orWhere('email', 'like', '%' .$search. '%');
            });
        });
        $query->where('plan_name',$plan_cat);
        if(!empty($visit_pass)){
            $query->where('visit_pass',$visit_pass);
        }
        if(!empty($startdate) && !empty($enddate)){
           $query->whereBetween('created_at',[$startdate,$enddate]);
        }
        $corporateIds = CorporateUsers::groupBy('corporate_id')->pluck('corporate_id')->all();
        if (!$iscorporate) {
            $query->where('plan_name', $plan_cat);
            $query->whereHas('username', function($q)  {
                return $q->where('is_corporate_user' , 0);
            });
        } else {
            $query->whereIn('user_id', $corporateIds);
        }
        $query->orderBy('created_at','DESC');
        $data = $query->get();
        $count = $data->count();
        return $count;
    }

    public function user_current_active_plan($id){
        $data = self::with('plan_category')->with('username','username.countryname')->where('id',$id)->first();
        return $data;
    }

    //18-10-2019
    public function is_subscription($user_id){
        $data = self::select('id','status','purchased_at','expired_at','visit_pass')->where('user_id',$user_id)->where('status','1')->first()->toArray();
        return $data;
    }

    //30-10-2019
    public function user_card(){
        return $this->hasMany('App\Usercard','usersubscription_id','id');
    }

    //05-10-2019

    public function is_subscribe($user_id){
        $data = self::select('id','subscription_id')->where('user_id',$user_id)->where('status','1')->first();
        return $data;
    }
    public function is_subscribe_trail($user_id){
        $data = self::select('id','subscription_id')->where('user_id',$user_id)->where('subscription_id','4')->first();
        return $data;
    }

    //select data for autopayment

    public function getautorenewaldata($date){
        $data = self::with('user_card')->whereDate('expired_at','<',$date)->where('plan_time','month')->where('status',1)->get();//->toArray();
        return $data;
    }

    //manual renew option add 28-09-20
    public function getmanualrenewaldata($user_id){
        //DB::enableQueryLog();
        $data = self::with('user_card')->where('user_id',$user_id)->where('plan_time','month')->where('status',1)->first();//->toArray();
        // dd(DB::getQueryLog());
        return $data;
    }

    //23-10-2019

    public function get_customer_id($user_id){
        $customer_id_exits = self::select('customer_id')->where('user_id',$user_id)->orderBy('id','desc')->first();
        if(isset($customer_id_exits->customer_id)){
            $customer = $customer_id_exits->customer_id;
        }else{
            $customer = '';
        }
        return $customer;
        //return $customer_id_exits;
    }


    //16-1-2020 for use checking controller
    public function plan_cat_name_fetch($user_id){
        $data = self::with('plan_category')->where('user_id',$user_id)->where('status','1')->first();
        return $data;
    }
    //11-02-2020
    public function last_plan_cat_name_fetch($user_id){
        $data = self::with('plan_category')->where('user_id',$user_id)->where('status','0')->orderBy('id','desc')->first();
        return $data;
    }

    //auto cancle subscription 17-1-2020

    public function select_subscription($date){
        $data = self::where('status','1')->whereDate('expired_at','<',$date)->where('visit_pass','!=','month')->get();
        return $data;
    }

    //10-02-2020
    public function Last_Month_user_data(){
        $month = date('m',strtotime("- 1 month"));
        $select = "SELECT COUNT(uvg.id) as total_visit,us.id,us.user_id,us.total_payable_amount FROM usersubscriptions us LEFT JOIN user_visited_gym uvg ON uvg.usersubscription_id=us.id WHERE month(us.expired_at)=".$month." AND us.visit_pass='month' GROUP BY us.id ORDER BY us.id DESC";
        $data = DB::select($select);
        return $data;
    }


    //05-03-2020 select subscription plan besed on checkin date for upload report
    public function select_month_plan_checkin_date($user_id, $check_in_date){
        $query = self::query();
        $query->where('user_id', $user_id);
        $query->where('visit_pass', 'month');
        $query->Where(function($q) use ($check_in_date) {
            $q->whereDate('purchased_at', '<', $check_in_date);
            $q->whereDate('expired_at', '>', $check_in_date);
        });
        $data = $query->first();//->toArray();
//        $check_in_date = date('Y-m-d', strtotime($check_in_date));
//        echo $select = "SELECT * FROM usersubscriptions WHERE user_id = '$user_id' AND visit_pass ='month' AND date(purchased_at) < '$check_in_date' AND date(expired_at) > '$check_in_date'";die;
//        $data = DB::select($select);
        return $data;

    }

}
