<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Contantmanagment extends Model
{
    protected $table="contantmanagments";
    protected $fillable = ['description','type','address_1','address_2'];
}
