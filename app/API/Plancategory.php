<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Plancategory extends Model
{
    protected $table = 'plan_category';

    protected $fillable = ['plan_cat_name','status','description'];

    public function plan_detail(){
 
    	return $this->hasMany('App\API\Subscription','plan_name','id')->where('status',1);
    }

    public function get_subscription_data(){
    	$data = self::select('id','plan_cat_name','description')->with('plan_detail')->where('id','!=',4)->where('status' , 1)->get();
    	return $data;
    }
    public function get_subscription_data_flag($subscribe){
        $data = "";
        if($subscribe->subscription_id == 4){
            $data = self::select('id', 'plan_cat_name', 'description')->with('plan_detail')->where('id','!=',4)->get();
        }else {
            $data = self::select('id', 'plan_cat_name', 'description')->with('plan_detail')->get();
        }
        return $data;
    }

    public function get_plan_category(){
    	$query = self::select('id','plan_cat_name')->get();
    	return $query;
    }
    
    //19-11-2019
    public function web_get_subscription_data($plan_cat_id){
        $data = self::select('id','plan_cat_name','description')->with('plan_detail')->where('id',$plan_cat_id)->first();
        return $data;
    }
}
