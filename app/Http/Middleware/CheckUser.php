<?php

namespace App\Http\Middleware;

use App\CorporateUsers;
use Closure;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\ApiCommanFunctionController as ApiCommanFunctionController;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     function __construct(){
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        //dd($data);
        $this->form = $data;
        $form['user_id'] = $this->form->user_id;
        $form['token'] = $this->form->token;
        $this->return = new ApiCommanFunctionController;
    }

    public function handle($form, Closure $next)
    {
        $check['user_id'] = $form->user_id;
        $check['token'] = $form->token;
        $rules = [
            'user_id' => 'required',
            'token' => 'required',
        ];
        $message = [
            'user_id.required' => 'User Id is required',
            'token.required' => 'Token is required',
        ];
        $validator = Validator::make($check, $rules, $message );
        if($validator->fails()){
            return $this->return->sendError_obj($validator->errors()->first());       
        }else{
            $user = User::whereid($form->user_id)->first();
            if(!empty($user)){
                if($user->status == 0 || $user->is_delete == 0){
                    $allData = (object)[];
                    return $this->return->sendResponse(133,$allData, 'User does not exist please check login credentials');
                }
                $corporateUser = CorporateUsers::where('user_id',$user->id)->first();
                if($corporateUser) {
                    $checkCorporate = User::find($corporateUser->corporate_id);
                    if(!$checkCorporate->status){
                        $allData = (object)[];
                        return $this->return->sendResponse(0, $allData, 'Your account has been blocked please contact GymPassport Admin');
                    }
                }
                if($form->token == $user->user_token){
                    $tokenTime_db = new \DateTime($user->user_token_time);
                    $current_time = new \DateTime();
                    $time = $current_time->diff($tokenTime_db);
                    if ($time->i <= 30000) {
                            $user->user_token_time = $current_time;
                            $user->update();
                        return $next($form);
                    }else{
                       $allData = (object)[];
                       return $this->return->sendResponse(133,$allData, 'Session time out, Please login again');
                    }
                }else{
                    $allData = (object)[];
                    return $this->return->sendResponse(133,$allData, 'Session time out, Please login again');
                }
                
            }else{
                $allData = (object)[];
                return $this->return->sendResponse(0,$allData, 'Something went wrong please try again');
            }
           
        }
        
    }
}
