<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;

class Corporatecheck
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
 
        if (isset(Auth::user()->id)) {
            if (Auth::user()->user_type == 5) {
                return $next($request);
            } else {
                $msg = "Invalid user please login valid credentials";
                Auth::logout();
                Session::flush();
                Session::flash('msg', $msg);
                Session::flash('message', 'alert-danger');
                return redirect()->route('login');
                //return redirect()->route('notfound');
            }

        } else {

            $msg = "Session time out please login again";
            Auth::logout();
            Session::flush();
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
            return redirect()->route('login');

        }

    }

}
