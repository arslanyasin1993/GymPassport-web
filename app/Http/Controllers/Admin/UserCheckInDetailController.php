<?php

namespace App\Http\Controllers\Admin;

use App\API\Usersubscription;
use App\Http\Controllers\Controller;
use App\User;
use App\Usergym;
use App\Uservisitedgym;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class UserCheckInDetailController extends Controller
{
    public function index()
    {
        return view('admin.users_check_in.users_check_in');
    }

    public function checkInDetailList(Request $req)
    {
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = $_GET['columns'][$columnIndex]['data'];


        if ($columnName == 'username') {
            $columnName = ($columnName == 'username') ? 'u.first_name' : $columnName;
        } elseif ($columnName == 'user_id') {
            $columnName = ($columnName == 'user_id') ? 'u.user_ufp_id' : $columnName;
        } elseif ($columnName == 'email') {
            $columnName = ($columnName == 'email') ? 'u.email' : $columnName;
        } else {
            $columnName = ($columnName == 'gym_name') ? 'g.gym_name' : 'a.user_check_in';
        }

        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        //$offset = $_GET['iDisplayStart'] ? $_GET['iDisplayStart'] :"0";
        //$limit_t = ($_GET['iDisplayLength'] !='-1') ? $_GET['iDisplayLength'] :"";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';

        $draw = $_GET['draw'];
        $search = $_GET['search']['value'];
        $obj = new Uservisitedgym;
        $details = $obj->check_in_details('', $startdate, $enddate, $search, $offset, $limit_t, $columnName, $order_by);
        $total_amount = $obj->check_in_details_amount('',$startdate, $enddate , $search);

        //return $details;
        $array = [];
        foreach ($details as $detail) {
            $sql = 'SELECT plan_category.plan_cat_name,gymcategories.plan_category_id FROM plan_category LEFT JOIN gymcategories ON gymcategories.plan_category_id=plan_category.id WHERE gymcategories.user_gym_id="' . $detail->gym_id . '"';
            $plan_name_by_gym_id = DB::select($sql);

            if (count($plan_name_by_gym_id) > 1) {

                $result = array_filter($plan_name_by_gym_id, function ($value) {
                    return $value->plan_category_id < 4;
                });

                $result = collect($result)->sortBy('plan_category_id')->reverse()->toArray();

                $plan_name = array_values($result);;

            }else{

                $plan_name = $plan_name_by_gym_id;
            }
//            $memberShip = Plancategory::join('subscription as s','s.id','=',$detail->)
            $arr = [];
            $delete = " <button data-url='" . route('delete_check_in', ['id' => $detail->id]) . "' class='btn btn-danger btn-xs' id='inactive' onclick='return confirm(\"Are you sure to delete this checkIn\")'>Delete</button>";
            $array[] = array(
                "username" => $detail->first_name . " " . $detail->last_name,
                "user_id" => $detail->user_ufp_id,
                "email" => $detail->email,
                "phone_number" => $detail->phone_number,
                "gym_plan_name" =>$plan_name?$plan_name[0]->plan_cat_name:"",
                "check_in_date" => date('d-m-Y' , strtotime($detail->user_check_in)),
                "check_in_time" => date('H:i:s' , strtotime($detail->user_check_in)),
                "gym_name" => $detail->gym_name,
                "amount" => $detail->gym_earn_amount,
                'plan_name' => $detail->plan_cat_name,
                'delete' => $delete,
            );
        }

        $totalRecord = count($details);

        $totalRecord = Uservisitedgym::select('user_visited_gym.id');

        if ($search) {
            $totalRecord = $totalRecord->where(function ($q) use ($search) {
                $q->where('u.first_name', 'like', '%' . $search . '%')
                    ->orWhere('u.last_name', 'like', '%' . $search . '%')
                    ->orWhere('u.email', 'like', '%' . $search . '%')
                    ->orWhere('g.gym_name', 'like', '%' . $search . '%')
                    ->orWhere('user_visited_gym.gym_earn_amount', 'like', '%' . $search . '%');
            });
        }
        if ($startdate && $enddate) {
            $totalRecord = $totalRecord->whereBetween('user_visited_gym.user_check_in', [$startdate . ' 00:00:00', $enddate . ' 23:59:59']);
        }
        $totalRecord = $totalRecord
            ->leftjoin('users as u', 'u.id', '=', 'user_visited_gym.user_id')
            ->leftjoin('users_gym as g', 'g.id', '=', 'user_visited_gym.gym_id')
            ->leftjoin('usersubscriptions as us', 'us.id', '=', 'user_visited_gym.usersubscription_id')
            ->leftjoin('subscriptions as s', 's.id', '=', 'us.subscription_id')
            ->leftjoin('plan_category as p', 'p.id', '=', 's.plan_name')->get()->count();
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $array,
            "totalAmount" => $total_amount // add totalAmount to the response array
        );
        echo json_encode($response);
        exit;
    }


    public function create()
    {
        $data['users'] = User::where('user_type', 3)->where('is_delete', 1)->where('status', 1)->get();

        return view('admin.users_check_in.add_check_in', $data);
    }


    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'user_id' => 'required',
            'gym_id' => 'required',
            'subscription_plan' => 'required',
            'check_in_date' => "required|date_format:Y-m-d|before_or_equal:" . date('Y-m-d')
        ], [
            'check_in_date.before_or_equal' => "The :attribute must be a date before or equal to " . date('d-m-Y') . ".",
        ]);

        $insert = $request->all();
        $user_plan = Usersubscription::select('id', 'plan_name', 'expired_at', 'visit_pass', 'plan_duration', 'plan_time', 'created_at', 'total_payable_amount', 'amount')->where('status', 1)->where('user_id', $insert['user_id'])->first();

        $gym_price_per_visit = Usergym::select('id', 'gym_price_per_visit')->where('id', $insert['gym_id'])->first();

        if ($user_plan) {
            $insert['usersubscription_id'] = $user_plan->id;
            $insert['check_in_status'] = 1;
            $insert['user_check_in'] = $request->check_in_date;
            $insert['gym_earn_amount'] = $gym_price_per_visit->gym_price_per_visit;
            $add = Uservisitedgym::create($insert);
            if ($add) {
                $msg = "Check in added successfully";
                Session::flash('msg', $msg);
                Session::flash('message', 'alert-success');

            } else {
                $msg = trans('lang_data.error');
                Session::flash('msg', $msg);
                Session::flash('message', 'alert-success');
            }
            return redirect()->back();
        } else {
            $msg = "User Subscription is Expired";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function getUserSubscription($id)
    {

        $data['userSubscription'] = Usersubscription::select('p.plan_cat_name', 'p.id')->where('usersubscriptions.user_id', $id)->join('subscriptions as s', 's.id', '=', 'usersubscriptions.subscription_id')
            ->join('plan_category as p', 'p.id', '=', 's.plan_name')->orderBy('usersubscriptions.id', 'desc')->first();

        $data['userGyms'] = '';

        if ($data['userSubscription']) {
            $plan_id = $data['userSubscription']->id;
            $data['userGyms'] = DB::select(DB::raw("SELECT users_gym.id as gym_id,users_gym.gym_name,is_all_day_open,gym_logo,gym_address,users_gym.status FROM users_gym 
                INNER JOIN gymcategories AS gc ON users_gym.id = gc.user_gym_id AND gc.plan_category_id  <= $plan_id
                WHERE users_gym.status = 1"));
        }

        return $data;
    }


    public function delete_check_in($id)
    {
        $updateUser = Uservisitedgym::whereid($id)->delete();
        if ($updateUser) {
            return response()->json(['status' => true , 'message' => 'Customer Check In deleted Successfuly']);
        } else {
            return response()->json(['status' => false , 'message' => 'Something went wrong']);
        }
    }
}
