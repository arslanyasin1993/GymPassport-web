<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\API\Subscription;
use App\API\Usersubscription;
use App\Country;
use App\User;
use App\Uservisitedgym;
use Session;
use DB;
use Illuminate\Support\Facades\Config;
class CustomermanagementController_old extends Controller
{
	    public function __construct(){
	        $this->subscription = new Subscription;
	        $this->country = new Country;
	        $this->U_Sub = new Usersubscription;
	        $this->user = new User;
                $this->user_visit_gym = new Uservisitedgym;
	    }


	 public function index(){
	 	return view('admin.customer_management.customer_list');
	 }

	 public function cus_man_list(Request $re){
	 	//echo'<pre>';print_r($_GET);die;

	 	//echo'<pre>';print_r($re->all());die;
                $order_by = $_GET['order'][0]['dir'];
		$columnIndex = $_GET['order'][0]['column'];
		$columnName = $_GET['columns'][$columnIndex]['data'];
		$columnName =  ($columnName=='username') ? 'first_name' : 'created_at';

		$offset = $_GET['start'] ? $_GET['start'] :"0";
		$limit_t = ($_GET['length'] !='-1') ? $_GET['length'] :"";
		//$offset = $_GET['iDisplayStart'] ? $_GET['iDisplayStart'] :"0";
		//$limit_t = ($_GET['iDisplayLength'] !='-1') ? $_GET['iDisplayLength'] :"";
		$startdate  = ($_GET['startdate']) ? date('Y-m-d',strtotime($_GET['startdate'])) : '';
		$enddate  = ($_GET['enddate']) ? date('Y-m-d',strtotime($_GET['enddate'])) : '';
		$draw = $_GET['draw'];
		$search = $_GET['search']['value'];
		//$search = $_GET['searchdata'] ? $_GET['searchdata'] :'';
		//$search = ($_GET['sSearch'] =='') ? "" : $_GET['sSearch'];
		$array = array('id','email','first_name','last_name','d_o_b','country','state','city','phone_number','address','postal_code','user_type','user_image','created_at','status');
		if(empty($startdate) && empty($enddate)){
			$userdata = User::select($array)
			->where('user_type',3)
			->where(function($q) use ($search) {
			      $q->where('first_name', 'like', '%' .$search. '%')
			        ->orWhere('last_name', 'like', '%' .$search. '%')
			        ->orWhere('email', 'like', '%' .$search. '%');
			  })
			->skip($offset)->take($limit_t)
			->orderBy($columnName,$order_by)
			->get();
		}else{
			$userdata = User::select($array)
			->where('user_type',3)
			->whereBetween('created_at',[$startdate,$enddate])
			->where(function($q) use ($search) {
			      $q->where('first_name', 'like', '%' .$search. '%')
			        ->orWhere('last_name', 'like', '%' .$search. '%')
			        ->orWhere('email', 'like', '%' .$search. '%');
			  })
			->skip($offset)->take($limit_t)
			->orderBy('created_at','DESC')
			->get();
		}
			//print_r($userdata);die;
			$data = $this->getuserdata($userdata);
		//print_r($data);die;
		if(empty($startdate) && empty($enddate)){
			$data_gg = User::select('id')->where('user_type',3)
						->where(function($q) use ($search) {
					          $q->where('first_name', 'like', '%' .$search. '%')
					            ->orWhere('last_name', 'like', '%' .$search. '%')
					            ->orWhere('email', 'like', '%' .$search. '%');
					      })
						->get();
		}else{
			$data_gg = User::select('id')->where('user_type',3)
						->whereBetween('created_at',[$startdate,$enddate])
						->where(function($q) use ($search) {
					          $q->where('first_name', 'like', '%' .$search. '%')
					            ->orWhere('last_name', 'like', '%' .$search. '%')
					            ->orWhere('email', 'like', '%' .$search. '%');
					      })
						->get();
		}
		$totalRecord = count($data_gg);
	 	$results = ["draw" => intval($draw),
          "iTotalRecords" => $totalRecord,
          "iTotalDisplayRecords" => $totalRecord,
          "aaData" => $data ];
         echo json_encode($results);
	 }

	 public function getuserdata($userdata){
	 		$data = array();
	 		$i=0;
	 		foreach ($userdata as $key => $value) {
	 			$data[$i]['user_image']= \Config::get('values.app_url') .$value->user_image;
	 			$data[$i]['username']= $value->first_name.' '.$value->last_name;
	 			$data[$i]['email'] = $value->email;
	 			$data[$i]['city'] = $value->city;
	 			$data[$i]['phone'] = $value->phone_number;
	 			$data[$i]['registerd_date']= date('Y-m-d',strtotime($value->created_at));
	 			$user_view = "<a href='".route('user_detail',['id'=>$value->id])."' class='btn btn-info btn-xs bg-olive'>View</a>";
	 			$check_in_details = "<a href='".route('update_sub',['id'=>$value->id])."' class='btn btn-info btn-xs bg-purple'>History</a>";
	 			$subscription_details = "<a href='".route('update_sub',['id'=>$value->id])."' class='btn btn-info btn-xs bg-navy'>View</a>";
	 			$subscription_details .= "    <a href='".route('update_sub',['id'=>$value->id])."' class='btn btn-danger btn-xs bg-navy'>Delete</a>";
	 			$data[$i]['view_info']= $user_view;
	 			$data[$i]['check_in_details']= $check_in_details;
	 			$data[$i]['subscription_details']= $subscription_details;
	 			if($value->status == 1){
	 				$active_inactive="<a href='".route('user_status',['id'=>$value->id,'status'=>$value->status])."' class='btn btn-success btn-xs' id='active' >Active</a>";
	 			}else{
	 				$active_inactive=" <a href='".route('user_status',['id'=>$value->id,'status'=>$value->status])."' class='btn btn-danger btn-xs' id='inactive'>Inactive</a>";
	 			}
	 			$data[$i]['user_status']= $active_inactive;
	 			$i++;
	 		}

	 		return $data;
	 }

	 public function user_status($id,$status){
	 	//echo $id.' staus '.$status;die;
	 	$update['status'] = ($status==1) ? 0 : 1;
	 	$update = User::whereid($id)->update($update);
	 	if($update){
	 		return redirect()->route('customerlist');
	 	}else{
	 		return redirect()->route('customerlist');
	 	}
	 }
//20-9-19
	 public function user_info($id){
	 	//echo $id;
		 $data['user_details'] = $this->user->user_details($id);

	 	$data['subscription_details'] = $this->U_Sub->user_subscription($id);
                $data['check_in_details'] = $this->user_visit_gym->check_in_details($id);
	 	//echo '<pre>';print_r($data['subscription_details']);die;
	 	//$data['url'] = "http://localhost/ultimateFitness/";
	 	$data['url'] =\Config::get('values.app_url') ;
	 	return view('admin.customer_management.customer_profile_detail',$data);
	 }
}
