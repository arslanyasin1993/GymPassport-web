<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Support\Facades\Hash;
use Session;
use App\User;
use App\Country;
use App\Usercard;
use Carbon\Carbon;
use App\Userusecoupon;
use App\Uservisitedgym;
use App\CorporateUsers;
use App\API\Plancategory;
use App\Autorenewalfaild;
use App\API\Subscription;
use Illuminate\Http\Request;
use App\API\Usersubscription;
use App\CorporateSubscription;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{

    function __construct()
    {
        $this->subscription = new Subscription;
        $this->plan_cat = new Plancategory;
        $this->U_Sub = new Usersubscription;
        $this->country = new Country;
        // $this->usercard = new Usercard;
    }

    public function index()
    {
        $data['plan_list'] = $this->subscription->selectdata();
        //$data = $data['plan_list'] ? $data['plan_list']:array();
        //echo '<pre>';print_r($data['plan_list']);die();
        return view('admin.subscription_management.subscription_list', $data);
    }

    public function create()
    {
        $data['country'] = $this->country->select('id', 'nicename')->get();
        $data['plan_cat'] = $this->plan_cat->get_plan_category();
        return view('admin.subscription_management.add_subscription', $data);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'plan_name' => 'required|string|max:255',
            //'plan_duration' => 'required',
            'visit_pass' => 'required',
            //'plan_time' => 'required|string',
            'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'country_id' => 'required|integer',
            'description' => 'required|string',
        ]);
        $this->subscription->savedata($request);
        return redirect()->back();
    }

    public function edit($id)
    {
        $data['country'] = $this->country->get();
        $data['plan_id'] = $this->subscription->find($id);
        $data['plan_cat'] = $this->plan_cat->get_plan_category();
        $data['plan_list'] = $this->subscription->for_edit_select($id);
        return view('admin.subscription_management.add_subscription', $data);
    }

    public function delete_customer($id)
    {
        if(request()->ajax()){
            Usercard::where('user_id', $id)->delete();
            CorporateUsers::where('user_id', $id);
            Uservisitedgym::where('user_id', $id)->delete();
            Autorenewalfaild::where('user_id', $id)->delete();
            Userusecoupon::where('user_id', $id)->delete();

            $bank = DB::table('user_bank_detail')->where('user_id', $id)->get();
            if (!empty($bank)) {
                DB::table('user_bank_detail')->where('user_id', $id)->delete();
            }

            Usersubscription::where('user_id', $id)->delete();
            $user = User::where('id', $id)->delete();

            if($user){
                return response()->json(['status' => true , 'message' => 'Customer deleted Successfuly']);
            }else{
                return response()->json(['status' => false , 'message' => 'Something went wrong']);
            }
        }
    }

    public function edit_customer($id){
        $user = User::find($id);
        return view('admin.subscription_management.edit_customer', get_defined_vars());
    }

    public function update_customer(Request $request , $id){
        $required = $request->password ? 'required|min:8|max:25|confirmed' : '';
        $this->validate($request , [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => $required,
        ]);

        $data = $request->all();
        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;

        if($request->password){
            $user->password = Hash::make($request->password);
        }
        $user->save();

        if($user){
            return redirect()->back()->with('success' , 'Customer Updated Sauccessfuly');
        }
        return redirect()->back()->with('error_message' , 'Something went wrong');
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            // 'plan_cat_id' => 'required|string|max:255',
            //'plan_duration' => 'required',
            'plan_name' => 'required',
            'visit_pass' => 'required',
            // 'plan_time' => 'required|string',
            'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'country_id' => 'required|integer',
            'description' => 'required|string',
        ]);
        $this->subscription->updatedata($request, $id);
        return redirect()->back();
    }

    public function subscriberlist()
    {

        // $status1 = 1;
        // $status2 = 2;
        // $status3 = 3;
        // $status4 = 4;
        // $data['one_month'] =  $this->U_Sub->select_month_year($status1);
        // $data['three_month'] =  $this->U_Sub->select_month_year($status2);
        // $data['six_month'] =  $this->U_Sub->select_month_year($status3);
        // $data['one_year'] =   $this->U_Sub->select_month_year($status4);
        $data['country'] = $this->country->select('id', 'nicename')->where('id', 13)->get();
        // echo '<pre>';print_r($data['one_month']);die;
        return view('admin.subscriber_list.subscriber_list', $data);
    }

    //14-10-2019
    public function ajaxsubscriber()
    {
        $plan_cat = $_GET['plan_cat'];
        // echo'<pre>';print_r($_GET);die();
        $plan_cat = $plan_cat ? $plan_cat : '';
        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
        $draw = $_GET['draw'];
        $search = $_GET['search']['value'];
        $country = '';
        $visit_pass = $_GET['visit_pass'] ? $_GET['visit_pass'] : "";

        $FetchSubscriberData = $this->U_Sub->select_subscriber_list($plan_cat, $startdate, $enddate, $search, $offset, $limit_t, $country, $visit_pass);


        $totalFetchUser = $this->U_Sub->count_subscriber_list($plan_cat, $startdate, $enddate, $search, $country, $visit_pass);
        $data = $this->createfetchuserdata($FetchSubscriberData);
        $amounts = $this->U_Sub->calculate_amounts($plan_cat, $startdate, $enddate, $search, $visit_pass);

        $results = ["draw" => intval($draw),
            "iTotalRecords" => $totalFetchUser,
            "iTotalDisplayRecords" => $totalFetchUser,
            'total_earning_amount' => $amounts['total_earning_amount'] . '  Rs',
            'total_checkin_amount' => $amounts['total_checkin_amount'] . '  Rs',
            'total_checkins' => $amounts['total_checkins'],
            "aaData" => $data];
        echo json_encode($results);
    }

    public function createfetchuserdata($FetchSubscriberData)
    {
        $i = 0;
        $data = array();
        if (!empty($FetchSubscriberData)) {
            foreach ($FetchSubscriberData as $key => $value) {
                $coupon = Userusecoupon::where('usersubscription_id', $value->id)->first();
                if ($coupon) {
                    $data[$i]['coupon_code'] = $coupon->coupon_code;
                } else {
                    $data[$i]['coupon_code'] = '';
                }
                $data[$i]['total_checkin'] = $value->total_checkin;
                $data[$i]['total_earning'] = $value->total_earning;
                $data[$i]['subscriber_name'] = $value->username->first_name . ' ' . $value->username->last_name;
                $data[$i]['subscriber_email'] = $value->username->email;
                $data[$i]['plan_status'] = $value->status == 0 ? "<span class='text text-danger fw-900'>Expired Plan</span>" : "<span class='text text-success fw-900'>Active Plan</span>";

                if (strtotime($value->expired_at) > time()) {
                    $data[$i]['subscription_status'] = $value->status == 0 ? "<button id='change_sub_status' data-url='". route('changeSubscriptionStatus', ['subscriptionId' => $value->id, 'status' => 1]) ."' class='btn btn-sm btn-danger'>Active</button>" : "<button id='change_sub_status' data-url='". route('changeSubscriptionStatus', ['subscriptionId' => $value->id, 'status' => 0]) ."' class='btn btn-sm btn-success'>Inactive</button>";
                }else{
                    $data[$i]['subscription_status'] = '';
                }

                $data[$i]['amount'] = number_format( $value->total_payable_amount,2);
                $data[$i]['created_at'] = date('d M Y',strtotime($value->created_at));
                $active_inactive = "<a href='#' onclick='userview(" . $value->id . ")' class='btn btn-success btn-xs' id='active' >View</a>";
                $data[$i]['details'] = $active_inactive;
                if ($value->total_payable_amount==0){
                    $edit = " <a href='" . route('edit_user_subscription', ['id' => $value->id]) . "' class='btn btn-danger btn-xs' onclick='return confirm(\"Are you sure to update this subscription\")'>Edit</a>";
                }
                else{
                    $edit='';
                }

                $data[$i]['edit'] = $edit;
                $delete = " <a href='javascript:void(0)' data-href='" . route('delete_user_subscription', ['id' => $value->id]) . "' class='btn btn-danger btn-xs delete_subscription'>Delete</a>";
                $data[$i]['delete'] = $delete;
                $i++;
            }
        }
        return $data;
    }

    public function corporate_list(Request $r, $type = null)
    {
        // based on tab request 2 for pending user and 0,1 for active user
        $where = ($type) ? [0, 1] : [2];
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = ($_GET['columns'][$columnIndex]['data'] == 'first_name') ? 'first_name' : 'created_at';

        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
        $search = $_GET['search']['value'];

        $select = array('id', 'first_name', 'email', 'phone_number', 'status', 'created_at');
        $get_data = $this->select_user_data($select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName);
        $revenue = $this->select_revenue_data($select, $search, $where, $startdate, $enddate);

        $totalRecord = $this->selectdata_count($search, $where, $startdate, $enddate);
        $results = [
            "draw" => intval($_GET['draw']),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $get_data,
            "revenue" => number_format($revenue) .' '. 'Rs'
            ];
        echo json_encode($results);
    }

    public function select_user_data($select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName)
    {
        $query = User::query($select);
        $query->whereNotIn('status', $where);
        $query->where(['is_delete' => '1', 'is_corporate' => '1']);

        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

        $query->with('countryname');
        $query->with('cityname');
        $query->skip($offset);
        $query->take($limit_t);
        $query->orderBy($columnName, $order_by);
        $user_data = $query->get();

        $data = array();
        $i = 0;
        $j = 1;
        foreach ($user_data as $key => $value) {
            $employees = CorporateUsers::where(['corporate_id' => $value->id, 'is_delete' => 1])->count();
            $employeeIds = CorporateUsers::where(['corporate_id' => $value->id, 'is_delete' => 1])->pluck('user_id');

            $corporate = CorporateSubscription::where('corporate_id', $value->id)->first();
            $plan = Plancategory::select('plan_cat_name')->join('subscriptions', 'subscriptions.plan_name', '=', 'plan_category.id')->where('subscriptions.id', $corporate->subscription_id)->first();

            $total_check_in = DB::table('user_visited_gym')->whereIn('user_id' , $employeeIds)->count();
            $total_amount = DB::table('user_visited_gym')->whereIn('user_id' , $employeeIds)->sum('gym_earn_amount');
            if($startdate && $enddate){
                $total_check_in = DB::table('user_visited_gym')->whereIn('user_id' , $employeeIds)->whereBetween('user_check_in' , [$startdate . ' 00:00:00', $enddate . ' 23:59:59'])->count();
                $total_amount = DB::table('user_visited_gym')->whereIn('user_id' , $employeeIds)->whereBetween('user_check_in' , [$startdate . ' 00:00:00', $enddate . ' 23:59:59'])->sum('gym_earn_amount');
            }

            $detail = "<a href='" . route('corporateEmployeeCheckInDetail' ).'?corporateId='. $value->id . "' class='btn btn-primary btn-xs'>View</a>";
            if($startdate && $enddate){
                $detail = "<a href='" . route('corporateEmployeeCheckInDetail' ).'?corporateId='. $value->id . '&startDate='. $startdate.'&endDate='. $enddate. "' class='btn btn-primary btn-xs'>View</a>";
            }

            $data[$i]['id'] = $value->id;
            $data[$i]['corporate_name'] = $corporate->corporate_name;
            $data[$i]['no_of_employee'] = $employees;
            $data[$i]['plan_name'] = $plan ? $plan->plan_cat_name : '';
            $data[$i]['total_cehck_ins'] = $total_check_in;
            $data[$i]['total_amount'] = $total_amount;
            $data[$i]['detail'] = $detail;
            $i++;
        }
        return $data;
    }

    public function changeSubscriptionStatus($subscriptionId , $status){
        $userSubscription = Usersubscription::where('id' , $subscriptionId)->update([
           'status' => $status
        ]);

        if($userSubscription){
            return response()->json(['status' => true , 'message' => 'User subscription status changed Successfuly!']);
        }
        return response()->json(['status' => false , 'error_message' => 'Something went Wrong!']);
    }

    public function check_in_details($user_id, $startdate = null, $enddate = null)
    {
        $data = DB::table('user_visited_gym as a')->select('a.*', 'u.first_name', 'u.last_name', 'u.user_ufp_id', 'u.email', 'u.phone_number', 'g.gym_name', 'p.plan_cat_name' , 'p2.plan_cat_name AS gym_plan_name');
        if ($startdate && $enddate) {
            $data = $data->whereBetween('a.user_check_in', [$startdate . ' 00:00:00', $enddate . ' 23:59:59']);
        }
        $user_id_arr = CorporateUsers::where('corporate_id', $user_id)->where('is_delete' , 1)->pluck('user_id');

        $data = $data->whereIn('a.user_id', $user_id_arr);
        $data = $data->leftjoin('users as u', 'u.id', '=', 'a.user_id')
        ->leftjoin('users_gym as g', 'g.id', '=', 'a.gym_id')
        ->leftjoin('usersubscriptions as us', 'us.id', '=', 'a.usersubscription_id')
        ->leftjoin('gymcategories as gc', 'gc.user_gym_id', '=', 'g.id')
        ->leftjoin('plan_category as p2', 'gc.plan_category_id', '=', 'p2.id')
        ->leftjoin('subscriptions as s', 's.id', '=', 'us.subscription_id')
        ->leftjoin('plan_category as p', 'p.id', '=', 's.plan_name')
        ->where('p2.id' , '!=' , 4);

        return ['checkins' => $data->count() , 'gym_earn_amount' => $data->sum('a.gym_earn_amount')];
    }

    public function select_revenue_data($select, $search, $where, $startdate, $enddate){

        $query = User::query($select);
        $query->whereNotIn('status', $where);
        $query->where(['is_delete' => '1', 'is_corporate' => '1']);

        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

        $query->with('countryname');
        $query->with('cityname');
        $user_data = $query->get();

        $data = array();
        $total_revenue = 0;
        foreach ($user_data as $key => $value) {
            $data = DB::table('user_visited_gym as a');
            if ($startdate && $enddate) {
                $data = $data->whereBetween('a.user_check_in', [$startdate . ' 00:00:00', $enddate . ' 23:59:59']);
            }
            $user_id_arr = CorporateUsers::where('corporate_id', $value->id)->where('is_delete' , 1)->pluck('user_id');

            $data = $data->whereIn('a.user_id', $user_id_arr);
            $data = $data->leftjoin('users as u', 'u.id', '=', 'a.user_id')
                ->leftjoin('users_gym as g', 'g.id', '=', 'a.gym_id')
                ->leftjoin('usersubscriptions as us', 'us.id', '=', 'a.usersubscription_id')
                ->leftjoin('subscriptions as s', 's.id', '=', 'us.subscription_id')
                ->leftjoin('plan_category as p', 'p.id', '=', 's.plan_name')
                ->sum('a.gym_earn_amount');
            $total_revenue = $total_revenue + $data;
        }
        return $total_revenue;
    }

    public function selectdata_count($search, $where, $startdate, $enddate)
    {

        $query = User::query();
        $query->whereNotIn('status', $where);
        $query->where('is_delete', '=', '1');
        $query->where('is_corporate', '=', '1');
//        if (!empty($startdate) && !empty($enddate)) {
//            $query->whereDate('created_at', '>=', $startdate);
//            $query->whereDate('created_at', '<=', $enddate);
//        }
        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

//        $query->with('cityname');
        $query->with('countryname');
        //$query->with('cityname');
        $query->orderBy('created_at', 'DESC');
        $gym_data = $query->get();
        return $gym_data->count();
    }

    public function getsubscribe_user($id)
    {
        $data['data'] = $this->U_Sub->user_current_active_plan($id);
        // return json_encode($data);
        return view('admin.subscriber_list.get_user_subscribe_model', $data);
    }


    public function addSubscriber()
    {
        $users = User::where('user_type', 3)->where('is_delete', 1)->where('status', 1)->get();
        $plan_category = Plancategory::all();

        return view('admin.subscriber_list.add_subscriber', compact('users', 'plan_category'));
    }

    public function getSubscriptions($id)
    {
        return $this->subscription->where('plan_name', $id)->get();
    }

    public function addUserSubscription(Request $request)
    {

        $validatedData = $request->validate([
            'user_id' => 'required',
            'subscription_id' => 'required',
            'subscription_plan' => 'required'
        ]);


        $input = $request->all();

        $userId = $input['user_id'];
        $oldSubscription = Usersubscription::where(['user_id' => $input['user_id'], 'status' => 1])->get();

        if ($oldSubscription) {
            $status = DB::statement("UPDATE usersubscriptions SET status = 0  where status =1 and user_id = $userId");
        }

        $subscribed['user_id'] = $input['user_id'];

        User::updateOrCreate(
            [
                'id' => $input['user_id']
            ],
            [
                'partner_first_login' => 1
            ]
        );

        $subscribed['customer_id'] = $input['user_id'];
        $subscribed['subscription_id'] = $input['subscription_id'];
        $subscribed['total_payable_amount'] = $input['subscription_amount'];


        $get_subscription_data = Subscription::whereid($input['subscription_id'])->first();

        $subscribed['transaction_id'] = 12345;
        $subscribed['purchased_at'] = Carbon::now();

        $subscribed['plan_name'] = $get_subscription_data->plan_name;
        $subscribed['visit_pass'] = $get_subscription_data->visit_pass;
        $subscribed['plan_duration'] = $get_subscription_data->plan_duration;
        $subscribed['plan_time'] = $get_subscription_data->plan_time;
        $subscribed['amount'] = $get_subscription_data->amount;
        $subscribed['description'] = $get_subscription_data->description ? $get_subscription_data->description : '';
        $plan_duration = $subscribed['plan_duration'];
        $plan_time = $subscribed['plan_time'];
        $subscribed['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($subscribed['purchased_at'])));


        $subscribed['payment_method'] = 'Foree';
        $subscribed['currency'] = 'PKR';
        $subscribed['charge_id'] = 12345;
//        $subscribed['total_payable_amount'] = 0;

        $corporateSubscriptionAdded = Usersubscription::create($subscribed);

        if ($corporateSubscriptionAdded) {
            $msg = "Subscription added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');

        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        }
        return redirect()->back();
    }


///// Cancle user subscription 03-11-2021
    public function CancelSubscription($uId)
    {
        $response = array();
        $user = User::find($uId);

        $date = date('Y-m-d H:i:s');
        $getdata = Usersubscription::where('user_id', $user->id)->where('status', 1)->update(['status' => 0, 'expired_at' => $date]);

        if ($getdata) {
            $msg = "Successfully cancelled subscription plan";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
            Session::flash('flag', true);

        } else {
            $msg = "Something went wrong to cancel subscription";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
            Session::flash('flag', false);
//            $response['message'] = 'Something went wrong to cancel subscription';
        }

        return redirect()->back();
    }
    ////////Edit user subscription   25-07-2022/////
    public function EditUserSubscription(Request $request, $id)
    {
        $userSubscription = Usersubscription::find($id);
        return view('admin.subscriber_list.edit_subscriber', compact('userSubscription','id'));
    }
    public function UpdateUserSubscription(Request $request, $id)
    {

        $userSubscription = Usersubscription::find($id);
        $userSubscription->total_payable_amount = $request->amount;
        $userSubscription->save();

        if ($userSubscription) {

            Session::flash('msg', 'User Subscription amount Successfully updated');
            Session::flash('message', 'alert-success');
        } else {
            Session::flash('msg', 'Cannot update right now!');
            Session::flash('message', 'alert-success');
        }
        return view('admin.subscriber_list.edit_subscriber',compact('userSubscription','id'));

    }
    ////////Delete user subscription   02-02-2022/////
    public function DeleteUserSubscription(Request $request, $id)
    {
        $userSubscription = Usersubscription::find($id)->delete();
        if ($userSubscription) {
            return response()->json(['status' => true , 'message' => 'Susbscriber deleted Successfuly!']);
        } else {
            return response()->json(['status' => false , 'message' => 'Something went Wrong!']);
        }
        return redirect()->back();
    }

    public function delete_sub_plan($id){
        $subscription = Subscription::find($id)->delete();

        if ($subscription) {
            Session::flash('msg', 'Subscription Deleted Successfully');
            Session::flash('message', 'alert-success');
        } else {
            Session::flash('msg', 'Cannot delete this subscription right now!');
            Session::flash('message', 'alert-success');
        }
        return redirect()->back();
    }

    public function updateStatus($id , $status){
        $subscription = Subscription::find($id);
        $subscription->status = $status;
        $check = $subscription->save();
        if ($check) {
            $msg = "Status updated successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function inactiveSubscription(){
        Usersubscription::where('status' , 1)->where('expired_at' , '>' , Carbon::now())->update([
            'status' => 0,
            'btn_status' => 0,
        ]);

        Session::flash('msg', 'Users Subscription Deactivated Successfully');
        Session::flash('message', 'alert-success');
        return redirect()->back();
    }

    public function activeSubscription(){
        Usersubscription::where('status' , 0)->where('btn_status' , 0)->where('expired_at' , '>' , Carbon::now())->update([
            'status' => 1,
            'btn_status' => 1
        ]);

        Session::flash('msg', 'Users Subscription Activated Successfully');
        Session::flash('message', 'alert-success');
        return redirect()->back();
    }
}
