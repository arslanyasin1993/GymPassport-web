<?php

namespace App\Http\Controllers\Admin;

use DB;
use Session;
use App\User;
use App\City;
use App\Country;
use App\Usergym;
use App\Gymimage;
use App\Facilities;
use App\Bankdetail;
use App\Gymcategory;
use App\Uservisitedgym;
use App\Gym_facilities;
use App\API\Plancategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class GymownerController extends Controller {

    function __construct() {
        $this->country = new Country;
        $this->gym_mod = new Usergym;
        $this->user_visit_gym = new Uservisitedgym;
    }

    public function gymlist() {
        return view('admin.gym_owner.gymlist');
    }

    public function create() {
        $data['gym_owner'] = User::select('id','first_name','last_name','email')
                                ->where('user_type','2')
                                ->where('status',1)
                                ->orderBy('first_name','asc')->get();
        $data['facities'] = Facilities::orderBy('facilities', 'ASC')->get();
        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['city']=City::select('id','name')->where('country','Pakistan')->get();
        //dd($data['city']);
        //return $data['city'];
        return view('admin.gym_owner.add_gym', $data);
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
//            'first_name' => 'required|string|max:255',
//            'last_name' => 'required|string|max:255',
//            'email' => 'required|string|email|max:255|unique:users',
            'gym_owner' => 'required',
            'gym_name' => 'required|string|max:255',
            'gym_price_per_visit' => 'required|numeric',
            //'gym_activities' => 'required|string',
            'gym_address' => 'required|string',
            //'about_gym' => 'required|string',
            'gym_latitude' => 'required|string',
            'gym_longitude' => 'required|string',
            'phone_number' => 'required',
            'country' => 'required|integer',
            //'password' => 'required|string|min:8',
            'gym_image.*' => 'required_without_all|image|mimes:jpeg,png,jpg',
            'gym_logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
//            'gym_open_days' => 'required',
//            'gym_open_timing' => 'required',
            'gym_pin_code' => 'required',
            'facilities_id' => 'required',
        ]);

        $this->gym_mod->user_gym_registerd($request);
        return redirect()->back();
    }

    public function gym_owner_list(Request $r, $type = null) {
        //echo '<pre>';print_r($_GET);die();
        $where = ($type) ? [0, 1] : [2]; // based on tab request 2 for pending user and 0,1 for active user
        //print_r($where);die;
        // $offset = $_GET['iDisplayStart'] ? $_GET['iDisplayStart'] :"0";
        // $limit_t = ($_GET['iDisplayLength'] !='-1') ? $_GET['iDisplayLength'] :"";
        // $search = ($_GET['sSearch'] =='') ? "" : $_GET['sSearch'];
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName =  ($_GET['columns'][$columnIndex]['data']=='gym_name') ? 'gym_name' : 'created_at';
        
        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
        $status = isset($_GET['status']) ? $_GET['status'] : null;

        $search = $_GET['search']['value'];
        //$search = $_GET['searchdata'] ? $_GET['searchdata'] :'';
        if(empty($type)){
            $gymPlan = $r->gymPlan;
            $select = array('id', 'users_id', 'gym_name','gym_price_per_visit', 'gym_logo', 'phone_number', 'gym_activities', 'about_gym', 'gym_address',
                'gym_latitude', 'gym_longitude', 'country','city','qr_code_image', 'accept_terms_condition', 'status', 'created_at');
            $get_data = $this->select_gym_data($select, $search, $offset, $limit_t, $where, $startdate, $enddate,$order_by,$columnName ,$status,$gymPlan);
            //echo '<pre>';print_r($get_data);die;
            $totalRecord = $this->selectdata_count($search, $where, $startdate, $enddate,$status,$gymPlan);
            $results = ["draw" => intval($_GET['draw']),
                "iTotalRecords" => $totalRecord,
                "iTotalDisplayRecords" => $totalRecord, 
                "aaData" => $get_data];
            echo json_encode($results);
        }else{
            //echo 'sdfkj';die;
            $BecomePatnerUser = $this->becomepatneruser($search, $offset, $limit_t, $where, $startdate, $enddate, $order_by,$columnName);
            $totalRecord = $this->becomepatneruser_total($search, $where, $startdate, $enddate);
            $results = ["draw" => intval($_GET['draw']),
                "iTotalRecords" => $totalRecord,
                "iTotalDisplayRecords" => $totalRecord,
                "aaData" => $BecomePatnerUser];
            echo json_encode($results);
            //echo '<pre>';print_r($BecomePatnerUser);die();
        }
     
    }

    public function select_gym_data($select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by,$columnName,$status,$gymPlan) {
            $query = Usergym::join('gymcategories' , 'gymcategories.user_gym_id' , '=' , 'users_gym.id');
            $query->whereNotIn('users_gym.status',$where);
            $query->where('users_gym.is_delete','1');
            if(!empty($startdate) && !empty($enddate)){
                $query->whereDate('users_gym.created_at','>=',$startdate);
                $query->whereDate('users_gym.created_at','<=',$enddate);
            }
            if ($status) {
                $status_value = ($_GET['status'] === 'active') ? 1 : 0;
                $query->where('users_gym.status', $status_value);
            }
            $query->Where(function($q) use ($search) {
                $q->orWhere('users_gym.gym_name', 'like', '%' .$search. '%')
                ->orWhere('users_gym.gym_price_per_visit', 'like', '%' .$search. '%')
                 ->orWhere('users_gym.phone_number', 'like', '%' .$search. '%');
            });

            if($gymPlan){
                $uniqueUserGymIds = DB::table('gymcategories')->whereIn('user_gym_id', function ($query) {
                    $query->select('user_gym_id')->from('gymcategories')->groupBy('user_gym_id')->having(DB::raw('COUNT(*)'), '>', 1);
                })->pluck('user_gym_id')->toArray();

                $query->whereNotIn('gymcategories.user_gym_id' , $uniqueUserGymIds)->where('gymcategories.plan_category_id' , $gymPlan);
            }

          $query->with('gym_owner_detail');
          $query->with('countryname');
          $query->skip($offset);
          $query->take($limit_t);
          $query->groupBY('gymcategories.user_gym_id');
          $query->orderBy('gymcategories.'.$columnName,$order_by);

          $gym_data = $query->get();

        $data = array();
        $i = 0;
        $j = 1;
        foreach ($gym_data as $key => $value) {
            if (in_array(2, $where)) {  //where base on tab request data
                if ($value->status == 1) {
                    $status = "<a href='javascript:void(0)' data-href='" . route('gym_status', ['id' => $value->id, 'status' => $value->status]) . "' class='btn btn-success btn-xs change_gym_status' id='active' onclick='return confirm(\"Are you sure to inactive this recode\")'>Active</a>";
                } else {
                    $status = "<a href='javascript:void(0)' data-href='" . route('gym_status', ['id' => $value->id, 'status' => $value->status]) . "' class='btn btn-danger btn-xs change_gym_status' id='inactive' onclick='return confirm(\"Are you sure to active this recode\")'>Inactive</a>";
                }
            } else {
                $status = "<a href='javascript:void(0)' data-href='" . route('gym_status', ['id' => $value->id, 'status' => $value->status]) . "' class='btn btn-xs bg-orange change_gym_status' id='active' onclick='return confirm(\"Are you sure to confirm this request\")'>Pending</a>";
            }
            $login = "<a href='" . route('login_account', ['id' => $value->users_id]) . "' class='btn btn-xs bg-success'>Login</a>";
            $delete = " <a href='javascript:void(0)' data-href='" . route('delete_gym', ['id' => $value->id]) . "' class='btn btn-danger btn-xs' id='delete_gym' onclick='return confirm(\"Are you sure to delete this gym\")'>Delete</a>";
            $check_in = "<a href='" . route('check_in_history_data', ['id' => $value->id]) . "' class='btn btn-xs bg-purple'>History</a>";
            $view_detail = "<a href='" . route('gym_details', ['id' => $value->id]) . "' class='btn btn-xs bg-navy'>View</a>";
            $edit = "<a href='" . route('edit_gym', ['id' => $value->id]) . "' class='btn btn-xs bg-orange'>Edit</a>";
            $qrcode = "<a href='" . asset('image/qr_codes/' . $value->qr_code_image) . "' download><img src='" . asset('image/qr_codes/' . $value->qr_code_image) . "' width='30px;'/></a>";
            $data[$i]['reg_owner'] = $value->gym_owner_detail->first_name . ' ' . $value->gym_owner_detail->last_name;

            $date = date('d/m/Y', strtotime($value->created_at));
            $data[$i]['registration_date'] = $date;
            $data[$i]['gym_name'] = $value->gym_name;
            $data[$i]['gym_price_per_visit'] = $value->gym_price_per_visit;
            $data[$i]['email'] = $value->gym_owner_detail->email;
            $data[$i]['phone_number'] = $value->phone_number;
            $data[$i]['country'] = ($value->countryname) ? $value->countryname->nicename  : '';
            $cat = Gymcategory::where('user_gym_id' , $value->user_gym_id)->get();
            $test = '';
            foreach ($cat as $category){
                if($category->planCategory){
                    $test .= '<span class="badge badge-pill badge-primary">'. $category->planCategory->plan_cat_name .'</span>';
                }
            }
            $data[$i]['categories'] = $test;
            $data[$i]['city'] = ($value->city) ? $value->city : '';
            $data[$i]['view_detail'] = $view_detail;
            $data[$i]['check_in'] = $check_in;
            $data[$i]['status'] = $status;
            $data[$i]['qr_code_image'] = $qrcode;
            $data[$i]['login'] = $login;
            $data[$i]['edit'] = $edit;
            $data[$i]['delete'] = $delete;
            $i++;
            // }
        }
        return $data;
    }

    public function selectdata_count($search, $where, $startdate, $enddate,$status , $gymPlan) {
        
        $query = Usergym::join('gymcategories' , 'gymcategories.user_gym_id' , '=' , 'users_gym.id');
        $query->whereNotIn('users_gym.status',$where);
        $query->where('users_gym.is_delete','1');
        if ($status) {
            $status_value = ($_GET['status'] === 'active') ? 1 : 0;
            $query->where('users_gym.status', $status_value);
        }
        if(!empty($startdate) && !empty($enddate)){
            $query->whereDate('users_gym.created_at','>=',$startdate);
            $query->whereDate('users_gym.created_at','<=',$enddate);
        }
        $query->Where(function($q) use ($search) {
            $q->orWhere('users_gym.gym_name', 'like', '%' .$search. '%')
             ->orWhere('users_gym.phone_number', 'like', '%' .$search. '%');
        });

        if($gymPlan){
            $uniqueUserGymIds = DB::table('gymcategories')->whereIn('user_gym_id', function ($query) {
                $query->select('user_gym_id')->from('gymcategories')->groupBy('user_gym_id')->having(DB::raw('COUNT(*)'), '>', 1);
            })->pluck('user_gym_id')->toArray();

            $query->whereNotIn('gymcategories.user_gym_id' , $uniqueUserGymIds)->where('gymcategories.plan_category_id' , $gymPlan);
        }

        $query->with('countryname');
        $query->with('gym_owner_detail');
        $query->groupBY('gymcategories.user_gym_id');
        $query->orderBy('users_gym.created_at','DESC');

        $gym_data = $query->get();
        return $gym_data->count();
    }

    public function gym_profile_detail($gym_id) {
       // echo $gym_id;die;
        $select = array('id', 'users_id', 'gym_name', 'gym_price_per_visit','gym_logo', 'phone_number', 'gym_activities', 'about_gym', 'gym_address',
            'gym_latitude', 'city','gym_longitude', 'country', 'accept_terms_condition', 'status', 'created_at','staff_hours','is_staff_hours', 'is_all_day_open');
        $data['gym_data'] = $this->gym_mod->gym_details($gym_id, $select);
        //echo '<pre>';print_r($data['gym_data']);die;
        return view('admin.gym_owner.gym_profile_detail', $data);
    }

    public function gym_status($gym_id, $status) {
        $update['status'] = ($status == 1) ? 0 : 1;
        $update = Usergym::whereid($gym_id)->update($update);

        if ($update) {
            return response()->json(['status' => true , 'message' => 'Gym Status updated Successfuly!']);
        } else {
            return response()->json(['status' => true , 'message' => 'Something went Wrong!']);
        }
    }

    public function edit_gym($gym_id) {
        $day = array();
        $staff_day = array();
        $select = array('id', 'users_id', 'gym_name','gym_price_per_visit', 'gym_logo', 'phone_number', 'gym_activities', 'about_gym', 'gym_address',
            'gym_latitude', 'gym_longitude', 'country', 'accept_terms_condition', 'status', 'created_at','is_all_day_open','gym_pin_code','is_staff_hours','staff_hours' , 'city');
        $data['gym_data'] = $this->gym_mod->gym_details($gym_id, $select);
        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['facities'] = Facilities::orderBy('facilities','ASC')->get();
        foreach ($data['gym_data']->gym_time as $key => $value) {
            $day[$key] = $value->gym_open_days;
        }
        foreach ($data['gym_data']->staff_timing as $key => $value) {
            $staff_day[$key] = $value->gym_open_days;
        }
        $facility = Gym_facilities::select('facilities_id')->where('gym_id', $gym_id)->get()->toArray();
        $facility_name = array_column($facility, 'facilities_id');

        $data['facility_id'] = $facility_name;
         // echo '<pre>';print_r($day); die;

        $data['days'] = $day;
	$data['staff_days'] = $staff_day;
	
	return view('admin.gym_owner.edit_gym', $data);

    }

    public function check_in_history($gym_id) {
        //echo $gym_id;die;
        //return redirect()->route('notfound');
        $data['gym_id'] = $gym_id;
        $total_amt = $this->user_visit_gym->total_estimate_amt_admin($gym_id);
       // echo'<pre>';print_r($total_amt);die();
        //echo $total_amt;die();
        $data['total_etimate_amt'] = $total_amt;
        return view('admin.gym_owner.check_in_history', $data);
    }

    public function delete_image($id) {
        $gym_image_path = Gymimage::select('gym_image')->whereid($id)->first();
        $path = 'https://'.$_SERVER['HTTP_HOST'].'/'.$gym_image_path->gym_image;
        //unlink($path);
        $delete = Gymimage::whereid($id)->delete();
        
        echo ($delete == true) ? 1 : 0;
    }

    public function update(Request $request, $id) {
       $userGym =  Usergym::find($id);


        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email,'.$userGym->users_id,
            'gym_name' => 'required|string|max:255',
            'gym_price_per_visit' => 'required|numeric',
            //'gym_activities' => 'required|string',
            'gym_address' => 'required|string',
            //'about_gym' => 'required|string',
            'gym_latitude' => 'required|string',
            'gym_longitude' => 'required|string',
            'phone_number' => 'required',
            'country' => 'required|integer',
            // 'gym_image.*' => 'required_without_all|image|mimes:jpeg,png,jpg',
            // 'gym_logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
//            'gym_open_days' => 'required',
//            'gym_open_timing' => 'required',
//            'gym_close_time' => 'required',
            'gym_pin_code' => 'required',
            'facilities_id' => 'required',
        ]);

        $this->gym_mod->user_gym_update($request, $id);
        return redirect()->back();
    }

//30-10-2019
    public function GetCheckInHistory($type, $gym_id) {
        // echo $type.$gym_id;
        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        //$search = $_GET['search']['value'];

        if (isset($_GET['type'])) {
            $type = $_GET['type'];
        }
        //echo $type;die;
        $userdata = $this->user_visit_gym->get_check_in_date($type, $gym_id, $offset, $limit_t);
        $total_record = $this->user_visit_gym->get_check_in_date_total($type, $gym_id);
        // echo'<pre>';print_r($userdata);die;
        $data = $this->get_check_in_history($userdata);
        $results = ["draw" => intval($_GET['draw']),
            "iTotalRecords" => $total_record,
            "iTotalDisplayRecords" => $total_record,
            "aaData" => $data];
        echo json_encode($results);
    }

    public function get_check_in_history($userdata) {
        $data = array();
        $i = 0;
        $j = 1;
        foreach ($userdata as $key => $value) {
            $name = $value->user_name->first_name . ' ' . $value->user_name->last_name;
            $data[$i]['username'] = ucfirst($name);
            $data[$i]['check_in'] = date('d/m/Y h:i A', strtotime($value->user_check_in));
            $data[$i]['check_out'] = date('d/m/Y h:i A', strtotime($value->user_check_out));
            $i++;
        }
        return $data;
    }
    
       //06-11-2019

    public function gym_plan(){
        $data['plan'] = Plancategory::select('id','plan_cat_name')->orderBy('id','asc')->get();
        $data['gym'] = Usergym::select('id','gym_name')->orderBy('id','asc')->get();
        $added_gym = Gymcategory::select('user_gym_id')->get()->toArray();
        $data['added_gym']=array_column($added_gym, 'user_gym_id');
       //echo '<pre>'; print_r($data); die;
        return view('admin.gym_owner.gym_plan_category',$data);
    }

    public function add_gym_plan(Request $r){
        $validatedData = $r->validate([
            'gym_name' => 'required',
            'plan_name' => 'required',
        ]);
       // print_r($r->plan_name);die();
        foreach ($r['gym_name'] as $key => $value) {
            $add = Gymcategory::create([
                'user_gym_id'=>$value,
                'plan_category_id'=>$r->plan_name,
            ]);
        }
        if (isset($add->id) && !empty($add->id)) {
            $msg = "Gym plan added successfull";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function gym_plan_list(){
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName =  ($_GET['columns'][$columnIndex]['data']=='gym_name') ? 'users_gym.gym_name' : 'plan_category.plan_cat_name';
        $offset = $_GET['start'] ? $_GET['start'] :"0";
        $limit_t = ($_GET['length'] !='-1') ? $_GET['length'] :"";
        $draw = $_GET['draw'];
        $search = $_GET['search']['value'];
        $gym_data = DB::table('gymcategories')
            ->leftjoin('users_gym','users_gym.id','=','gymcategories.user_gym_id')
            ->leftjoin('plan_category','plan_category.id','=','gymcategories.plan_category_id')
            ->select('gymcategories.*','users_gym.gym_name','plan_category.plan_cat_name')
            ->where('gymcategories.status','1')
            ->Where('users_gym.gym_name', 'LIKE', '%' . $search . '%')
            ->orWhere('plan_category.plan_cat_name', 'LIKE', '%' . $search . '%')
            ->orderBy($columnName,$order_by)
            ->skip($offset)
            ->take($limit_t)
            ->get();
        $data = array();$i=0;
        foreach ($gym_data as $key => $value) {
                $action=" <a href='javascript:void(0)' data-href='".route('delete_gym_plan',['id'=>$value->id,'status'=>$value->status])."' class='btn btn-danger btn-xs' id='delete_gym_plan' onclick='return confirm(\"Are you sure to delete this recode\")'>Delete</a>";
                $data[$i]['gym_name'] = $value->gym_name;
                $data[$i]['plan_name'] = $value->plan_cat_name;
                $data[$i]['date'] = date('d-m-Y',strtotime($value->created_at));
                $data[$i]['action'] = $action;
                $i++;
        }
       $totalRecord = DB::table('gymcategories')
            ->leftjoin('users_gym','users_gym.id','=','gymcategories.user_gym_id')
            ->leftjoin('plan_category','plan_category.id','=','gymcategories.plan_category_id')
            ->select('gymcategories.*','users_gym.gym_name','plan_category.plan_cat_name')
            ->where('gymcategories.status','1')
            ->Where('users_gym.gym_name', 'LIKE', '%' . $search . '%')
            ->orWhere('plan_category.plan_cat_name', 'LIKE', '%' . $search . '%')
            ->get();
        $totalRecord = $totalRecord->count();

        $results = ["draw" => intval($_GET['draw']),
          "iTotalRecords" => $totalRecord,
          "iTotalDisplayRecords" => $totalRecord,
          "aaData" => $data ];
         echo json_encode($results);

    } 

    public function delete_plan($id){
         $delete = Gymcategory::whereid($id)->delete();
         if ($delete) {
             return response()->json(['status' => true , 'message' => 'Gym Category deleted Successfuly!']);
         } else {
             return response()->json(['status' => false , 'message' => 'Something went Wrong!']);
         }
    }


    public function becomepatneruser($search, $offset, $limit_t, $where, $startdate, $enddate, $order_by,$columnName){
       // echo 'sdhcvs';die();
        $query = User::query();
        $query->where('user_type','2')->where('is_delete' , 1);
            if(!empty($startdate) && !empty($enddate)){
                $query->whereDate('created_at','>=',$startdate);
                $query->whereDate('created_at','<=',$enddate);
            }
            $query->Where(function($q) use ($search) {
                $q->orWhere('first_name', 'like', '%' .$search. '%')
                 ->orWhere('email', 'like', '%' .$search. '%')
                 ->orWhere('fitness_facility', 'like', '%' .$search. '%')
                 ->orWhere('last_name', 'like', '%' .$search. '%');
            });
          $query->with('countryname');
          $query->with('state_name');
          $query->skip($offset);
          $query->take($limit_t);
          $query->orderBy($columnName,$order_by);
          $gym_data = $query->get();
          $data = array();
        $i = 0;
        $j = 1;
        foreach ($gym_data as $key => $value) {
            // if($value->status != 2){ //only select registerd gym  
            // $data[$i]['inc_id'] = $j++;
            $delete = " <a href='javascript:void(0)' data-href='" . route('delete_gym_owner', ['id' => $value->id]) . "' class='btn btn-danger btn-xs' id='delete_gym_owner' onclick='return confirm(\"Are you sure to delete this user\")'>Delete</a>";
            $Account_delete = '';
            $get_bank_detail = Bankdetail::where('user_id',$value->id)->first();
            if(!empty($get_bank_detail)){
                 $Account_delete = " <a href='" . route('delete_stripe_account', ['id' => $value->id]) . "' class='btn btn-danger btn-xs' id='inactive' onclick='return confirm(\"Are you sure to want delete stripe account of this gym owner\")'>Account Delete</a>";
            }
 
            $edit = "<a href='" . route('edit_gym_owner', ['id' => $value->id]) . "' class='btn btn-xs bg-orange'>Edit</a>";
            $data[$i]['first_name'] = $value->first_name . ' ' . $value->last_name;
            $data[$i]['email'] = $value->email;
            $data[$i]['fitness_facility'] = $value->fitness_facility;
            $data[$i]['phone_number'] = $value->phone_number ? $value->phone_number :'';
            $data[$i]['country'] = $value->country ? $value->countryname->nicename:'';
            $data[$i]['state'] = $value->state ? $value->state_name['state']:'';
            $data[$i]['postal_code'] = $value->postal_code ? $value->postal_code:'';
            $data[$i]['suburb'] = $value->suburb ? $value->suburb:'';
            $data[$i]['registration_fees'] = $value->registration_fees ? $value->registration_fees:'';
            $data[$i]['monthly_fees'] = $value->monthly_fees ? $value->monthly_fees:'';
            $date = date('d/m/Y', strtotime($value->created_at));
            $data[$i]['registration_date'] = $date;
            $data[$i]['edit'] = $edit;
            $data[$i]['delete'] = $delete;
            $data[$i]['account_delete'] = $Account_delete;
            $i++;
            // }
        }

        return $data;
    }

     public function becomepatneruser_total($search, $where, $startdate, $enddate){
           $query = User::query();
           $query->where('user_type','2')->where('is_delete' , 1);
            if(!empty($startdate) && !empty($enddate)){
                $query->whereDate('created_at','>=',$startdate);
                $query->whereDate('created_at','<=',$enddate);
            }
            $query->Where(function($q) use ($search) {
                $q->orWhere('first_name', 'like', '%' .$search. '%')
                 ->orWhere('email', 'like', '%' .$search. '%')
                 ->orWhere('last_name', 'like', '%' .$search. '%');
            });
          $query->with('countryname');
          $query->with('state_name');
          $gym_data = $query->get();
          return $total = $gym_data->count();
     }


     // add gym partner from admin side 14-01-2020

     public function gym_partner_add(){
        $db['country'] = DB::table('country')->select('id','nicename')->where('id',162)->get();
        $db['state'] = DB::table('state')->select('id','state')
                        ->where('country_id',162)->orderBy('state','ASC')->get();
        //dd($db);
        return view('admin.gym_owner.add_gym_partners',$db);
     }
     
     public function delete_gym($gym_id){
        $update['status'] = 0;
        $update['is_delete'] = 0;
        $update = Usergym::whereid($gym_id)->update($update);

        if ($update) {
            return response()->json(['status' => true , 'message' => 'Gym deleted Successfuly']);
        } else {
            return response()->json(['status' => false , 'message' => 'Something went Wrong!']);
        }
     }
     
     public function delete_gym_owner($owner_id){
        $owner['status'] = '0';
        $update['status'] = 0;
        $update['is_delete'] = 0;
        $owner['is_delete'] = '0';

        $update_user = User::whereid($owner_id)->update($owner);
        $update_gym = Usergym::where('users_id',$owner_id)->update($update);

        if ($update_user && $update_gym) {
            return response()->json(['status' => true , 'message' => 'Gym Owner Deleted Successfuly!']);
        } else {
            return response()->json(['status' => false , 'message' => 'Something went Wrong!']);
        }
     }
     
     
      public function edit_gym_owner($user_id){
        $db['country'] = DB::table('country')->select('id','nicename')->where('id',13)->get();
        $db['state'] = DB::table('state')->select('id','state')
                        ->where('country_id',13)->orderBy('state','ASC')->get();
        $db['user_data'] = User::where('id',$user_id)->first();
       // echo '<pre>';        print_r($db['user_data']);die;
        $db['id'] = $user_id;
        return view('admin.gym_owner.edit_gym_partners',$db); 
     }
     
     public function update_gym_owner(Request $r, $user_id){
          $validatedData = $r->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'state' => 'required|string|max:10',
            'suburb' => 'required|string',
            'postal_code' => 'required|string|max:10',
            'phone_number' => 'required',
            'fitness_facility' => 'required',
            'country' => 'required',
        ]);
         $input = $r->all();
         unset($input['_token']);
         unset($input['created_from']);
    	 $data = User::where('id',$user_id)->update($input);

        if($data){
                $msg = "Gym Partner Added Successfully!";
                Session::flash('msg',$msg);
                Session::flash('message','alert-success');
        }else{
                $msg = trans('lang_data.error');
                Session::flash('msg',$msg);
                Session::flash('message','alert-danger');
        }
         return redirect()->back();
     }
     
  //04-06-2020
     public function resetpassword(){
         $data['gym_owner'] = User::select('id','first_name','last_name','email')
                                ->where('user_type','2')
                                ->where('status',1)
                                ->orderBy('first_name','asc')->get();
         return view('admin.gym_owner.reset_password',$data);
     }
     public function changepassword(Request $r){
         $validatedData = $r->validate([
            'gym_owner' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password|min:8'
        ]);
        $user_id = $r->gym_owner;
        $update = User::whereid($user_id)->update(['password'=>Hash::make($r->confirm_password)]);
        if($update){
           $msg = trans('lang_data.pass_success');
            Session::flash('msg',$msg);
            Session::flash('message','alert-success'); 
        }else{
            $msg = "Something went wrong please try again";
            Session::flash('msg',$msg);
            Session::flash('message','alert-danger');
        }
        return redirect()->route('resetpassword');
     }

}
