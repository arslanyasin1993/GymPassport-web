<?php

namespace App\Http\Controllers\Admin;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
private $_viewPath ;

    public function __construct()
    {
        $this->middleware('auth');
        $this->_viewPath = 'admin.news.';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $news = News::where(['is_deleted' => 0])->orderby('created_at', 'desc')->get();
        return view($this->_viewPath.'index', compact('news'));
    }

    public function create()
    {
        return view($this->_viewPath.'add');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg',
            'author' => 'required|string|max:255',
            'description' => 'required|string',
            'slug' => 'required|unique:news,slug|regex:/^[a-zA-Z0-9-]+$/',
        ]);
        if ($request->hasFile('image')) {
            $sDirPath = public_path('storage/news');
            $path = 'storage/news';
            if (!$sDirPath) {
                mkdir($sDirPath, 0777, true);
            }
            $news_image = date('Ymdhis') . '.' . $request->image->getClientOriginalExtension();

            $request->image->move($sDirPath, $news_image);
            $input['image'] = $news_image ? $path . '/' . $news_image : "";
        }

        $news = News::create($input);
        if ($news) {

            $msg = "News added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');

        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }

        return redirect()->back();

    }

    public function edit($id)
    {
        $news = News::find($id);
        return view($this->_viewPath.'edit', compact('news'));
    }

    public function update(Request $request, $id)
    {
        $news = News::find($id);
        $input = $request->all();
        unset($input['image']);
        $validatedData = $request->validate([
            'author' => 'required|string|max:255',
            'description' => 'required|string',
            'slug' => 'required|regex:/^[a-zA-Z0-9-]+$/|unique:news,slug,' . $id
        ]);

        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'nullable|image|mimes:jpeg,png,jpg',
            ]);
        }

        if ($request->hasFile('image')) {
            $sDirPath = public_path('storage/news');
            $path = 'storage/news';
            if (!$sDirPath) {
                mkdir($sDirPath, 0777, true);
            }
            $news_image = date('Ymdhis') . '.' . $request->image->getClientOriginalExtension();

            $request->image->move($sDirPath, $news_image);
            $input['image'] = $news_image ? $path . '/' . $news_image : "";
        }


        $check = $news->update($input);
        if ($check) {

            $msg = "News Updated successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');

        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        $news = News::find($id);
        $input['is_deleted'] = 1;
        $news->update($input);
        $check = $news->save();
        if ($check) {
            $msg = 'News deleted successfully';
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();

    }

    public function updateStatus($id, $status)
    {
        $news = News::find($id);
        $news->is_active = $status;
        $check = $news->save();
        if ($check) {
            $msg = "Status updated successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }
}

