<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use Session;
use Validator;
use App\Usergym;
use App\Bankdetail;
use App\Payoutdate;
use App\Uservisitedgym;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use App\API\Usersubscription;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class RevenueHistoryController extends Controller
{
     function __construct() {

        $this->gym = new Usergym;
        $this->uvg = new Uservisitedgym;
    }

    public function index(Request $request)
    {
        $i = 0;
        $data = [];
        $gym_type = empty($request->gym_type) ? 1 : $request->gym_type;

        $usersgym = Usergym::select('id', 'gym_name' , 'gym_price_per_visit')->where('status', 1);
        if(!empty($dateFrom) && !empty($dateTo)){
            $usersgym->whereDate('created_at' , '>=' , $dateFrom)->whereDate('created_at' , '<=' , $dateTo);
        }
        $usersgym = $usersgym->get();

        foreach($usersgym as $row) {
            // Daily
            if ($gym_type == 1) {
                $startdate = date('Y-m-d');
                $end_date = date('Y-m-d');
            }
            // Weekly
            elseif ($gym_type == 2) {
                $startdate = date('Y-m-d', strtotime('-1 week'));
                $end_date = date('Y-m-d');
            }
            // Monthly
            elseif ($gym_type == 3) {
                $startdate = date('Y-m-d', strtotime("- 1 month"));
                $end_date = date('Y-m-d');
            }
            // 3 Month
            elseif ($gym_type == 5) {
                $startdate = date('Y-m-d', strtotime("- 3 month"));
                $end_date = date('Y-m-d');
            }
            // 6 Month
            elseif ($gym_type == 6) {
                $startdate = date('Y-m-d', strtotime("- 6 month"));
                $end_date = date('Y-m-d');
            }
            // 9 Month
            elseif ($gym_type == 7) {
                $startdate = date('Y-m-d', strtotime("- 9 month"));
                $end_date = date('Y-m-d');
            }
            // Year
            elseif($gym_type == 4) {
                $startdate = date('Y-m-d', strtotime("- 1 year"));
                $end_date = date('Y-m-d');
            }
            // Custom Date
            else{
                if(!empty($request->start_date) && !empty(!empty($request->end_date)) && $request->gym_type== 'custom_date' ){
                    $startdate = date('Y-m-d' , strtotime($request->start_date));
                    $end_date = date('Y-m-d' , strtotime($request->end_date));
                }else{
                    $startdate = date('Y-m-d');
                    $end_date = date('Y-m-d');
                }
            }
            $total_amount = Uservisitedgym::select('gym_earn_amount')->where('gym_id', $row->id)->whereBetween('user_check_in', [$startdate.' 00:00:00', $end_date.' 23:59:59'])->sum('gym_earn_amount');
            $visit_details = Uservisitedgym::select('id', 'user_id', 'gym_id', 'check_in_status', 'user_check_in')
                ->with('user_detail')
                ->where('gym_id', $row->id)
                ->whereBetween('user_check_in', [$startdate.' 00:00:00', $end_date.' 23:59:59'])
                ->get();

            foreach ($visit_details as $visit_detail) {
                $visit_detail->check_in_date = date('Y-m-d', strtotime($visit_detail->user_check_in));
                $visit_detail->check_in_time = date('H:i:s', strtotime($visit_detail->user_check_in));
            }
            $data[$i]['gym_name'] = $row->gym_name;
            $data[$i]['gym_price_per_visit'] = $row->gym_price_per_visit;
            $data[$i]['visited_users'] = count($visit_details);
            $data[$i]['revenue'] = $total_amount;
            $data[$i]['details'] = '<a href="' . route("crevenue", ["id" => $row->id, "type" => $gym_type, 'start_date' => $startdate , 'end_date' => $end_date]) . '" class="btn btn-info btn-xs bg-olive">View</a>';
            $i++;
        }
        return view('admin.revenue_history.revenue_history' , get_defined_vars());
    }
    public function getrevenueData(Request $request)
    {
        $gym_type=$request->gym_type;
        $gym_id=$request->gym_id;
        $startdate='';
        $end_date='';
        if($gym_type==1){//daily
            $startdate = date('d-m-Y');
            $end_date = date('d-m-Y');
        }elseif ($gym_type==2) {//weekly
            $startdate = date('d-m-Y',strtotime('-1 week'));
            $end_date = date('d-m-Y');
        }elseif($gym_type==3){//monthly
            $startdate = date('d-m-Y',strtotime("- 1 month"));
            $end_date = date('d-m-Y');
        }elseif($gym_type==5){//3 month
            $startdate = date('d-m-Y',strtotime("- 3 month"));
            $end_date = date('d-m-Y');
        }elseif($gym_type==6){//6 month
            $startdate = date('d-m-Y',strtotime("- 6 month"));
            $end_date = date('d-m-Y');
        }elseif($gym_type==7){//9 month
            $startdate = date('d-m-Y',strtotime("- 9 month"));
            $end_date = date('d-m-Y');
        }else {//year
            $startdate = date('d-m-Y',strtotime("- 1 year"));
            $end_date = date('d-m-Y');
        }

        // $total_amount = Uservisitedgym::select(DB::raw('sum(quantity) as order_product_count'))
        // ->groupBy('vendor_Order_id')
        // ->having('vendor_Order_id', $order->id)

        // ->first();
        $total_amount= Uservisitedgym::select('gym_earn_amount')->where('gym_id',$gym_id)->sum('gym_earn_amount');
       // dd($total_amount);
        $visit_details = Uservisitedgym::select('id','user_id','gym_id','check_in_status','user_check_in')
                     ->with('user_detail')
                     ->where('gym_id',$gym_id)
                     ->get();
                    foreach($visit_details as $visit_detail){
                        $visit_detail->check_in_date = date('Y-m-d', strtotime($visit_detail->user_check_in));
                        $visit_detail->check_in_time = date('H:i:s', strtotime($visit_detail->user_check_in));
                    }

        return ['start_date'=>$startdate,'end_date'=>$end_date,'total_amount'=>$total_amount,'visit_details'=>$visit_details];



    }

    public function newGetrevenueData(Request $request)
    {
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = $_GET['columns'][$columnIndex]['data'];

        $offset = $_GET['start'] ? $_GET['start'] :"0";
        $limit_t = ($_GET['length'] !='-1') ? $_GET['length'] :"";

        $gym_type=$_GET['gym_type'];
        //$gym_id=$_GET['gym_id'];

        $draw = $_GET['draw'];
        $search = $_GET['search']['value'];
        $data = array();
        $i=0;

        $usersgym = Usergym::select('id', 'gym_name')->where('status', 1)
            ->where(function($q) use ($search) {
                $q->where('gym_name', 'like', '%' .$search. '%');
            })->skip($offset)->take($limit_t)->orderBy($columnName,$order_by);

        if(!empty($dateFrom) && !empty($dateTo)){
            $usersgym->whereDate('created_at' , '>=' , $dateFrom)->whereDate('created_at' , '<=' , $dateTo);
        }
        $usersgym = $usersgym->get();

        foreach($usersgym as $row) {

            $gym_type = $gym_type;
            //$gym_id=$request->gym_id;
            $startdate = '';
            $end_date = '';
            if ($gym_type == 1) {//daily
                $startdate = date('Y-m-d');
                $end_date = date('Y-m-d');
            } elseif ($gym_type == 2) {//weekly
                $startdate = date('Y-m-d', strtotime('-1 week'));
                $end_date = date('Y-m-d');
            } elseif ($gym_type == 3) {//monthly
                $startdate = date('Y-m-d', strtotime("- 1 month"));
                $end_date = date('Y-m-d');
            } elseif ($gym_type == 5) {//3 month
                $startdate = date('Y-m-d', strtotime("- 3 month"));
                $end_date = date('Y-m-d');
            } elseif ($gym_type == 6) {//6 month
                $startdate = date('Y-m-d', strtotime("- 6 month"));
                $end_date = date('Y-m-d');
            } elseif ($gym_type == 7) {//9 month
                $startdate = date('Y-m-d', strtotime("- 9 month"));
                $end_date = date('Y-m-d');
            } elseif($gym_type == 4) {//year
                $startdate = date('Y-m-d', strtotime("- 1 year"));
                $end_date = date('Y-m-d');
            }else{
                if(!empty($request->date_range) && $request->gym_type== 'custom_date' ){
                    $startdate = date('Y-m-d' , strtotime(explode('-' , $request->date_range)[0]));
                    $end_date = date('Y-m-d' , strtotime(explode('-' , $request->date_range)[1]));
                }else{
                    $startdate = date('Y-m-d');
                    $end_date = date('Y-m-d');
                }
            }

            // $total_amount = Uservisitedgym::select(DB::raw('sum(quantity) as order_product_count'))
            // ->groupBy('vendor_Order_id')
            // ->having('vendor_Order_id', $order->id)

            // ->first();
            $total_amount = Uservisitedgym::select('gym_earn_amount')->where('gym_id', $row->id)->whereBetween('user_check_in', [$startdate, $end_date])->sum('gym_earn_amount');

            $visit_details = Uservisitedgym::select('id', 'user_id', 'gym_id', 'check_in_status', 'user_check_in')
                ->with('user_detail')
                ->where('gym_id', $row->id)
                ->whereBetween('user_check_in', [$startdate, $end_date])
                ->get();

            foreach ($visit_details as $visit_detail) {
                $visit_detail->check_in_date = date('Y-m-d', strtotime($visit_detail->user_check_in));
                $visit_detail->check_in_time = date('H:i:s', strtotime($visit_detail->user_check_in));
            }
            $data[$i]['gym_name'] = $row->gym_name;
            $data[$i]['visited_users'] = count($visit_details);
            $data[$i]['revenue'] = $total_amount;
            $data[$i]['details'] = '<a href="/crevenue/'.$row->id.'/'.$gym_type.'" class="btn btn-info btn-xs bg-olive">View</a>';
             //return ['start_date' => $startdate, 'end_date' => $end_date, 'total_amount' => $total_amount, 'visit_details' => $visit_details];
            $i++;
        }
        $count = Usergym::select('id', 'gym_name')
            ->where('status', 1)
            ->where(function($q) use ($search) {
                $q->where('gym_name', 'like', '%' .$search. '%');
            });
//            if(!empty($dateFrom) && !empty($dateTo) &&$request->gym_type== 'custom_date' ){
//                $count->whereDate('created_at' , '>=' , $dateFrom)->whereDate('created_at' , '<=' , $dateTo);
//            }
        $count = $count->get();
        $totalRecord = count($count);
        $results = ["draw" => intval($draw),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $data ];
        echo json_encode($results);

    }


        public function gym_earn_amount(Request $r) {
        $input = $r->all();
        $response = [];
        $gym_type = $input['gym_type'];
        $gym_id = $input['gym_id'];
        $get_payout_date = Payoutdate::select('payout_date')->first(); //select date from payout table where store payout date
        $payout_date = "";//$get_payout_date->payout_date;
        $total_amount_visit = $this->uvg->total_gym_earn_amount($gym_type, $gym_id, $payout_date);
        $total_amount_month = $this->uvg->total_gym_earn_amount_month($gym_type, $gym_id, $payout_date);
        $final_amount = 0;
//        echo '<pre>';print_r($total_amount_visit);
//        echo '<pre>';print_r($total_amount_month);die;
        $daily_amount = '0';
        $month_amount = '0';
        // if(isset($total_amount_visit[0]->total_amount) && isset($total_amount_month[0]->total_amount)){ //if not null both value
        //    $final_amount = ($total_amount_month[0]->total_amount + $total_amount_visit[0]->total_amount);
        //     $daily_amount = $total_amount_visit[0]->total_amount;
        //     $month_amount = $total_amount_month[0]->total_amount;
        // }else
        if(isset($total_amount_visit[0]->total_amount)){  //if not null visit pass value
            $final_amount = $total_amount_visit[0]->total_amount;
            // $daily_amount = $total_amount_visit[0]->total_amount;
         }//else if(isset($total_amount_month[0]->total_amount)){ // if not null monthly pass value
        //     $final_amount = $total_amount_month[0]->total_amount;
        //      $month_amount = $total_amount_month[0]->total_amount;
        // }
      // echo'<pre>';print_r($final_amount);die;
        if($gym_type==1){//daily
            $startdate = date('d-m-Y');
            $end_date = date('d-m-Y');
        }elseif ($gym_type==2) {//weekly
            $startdate = date('d-m-Y',strtotime('-1 week'));
            $end_date = date('d-m-Y');
        }elseif($gym_type==3){//monthly
            $startdate = date('d-m-Y',strtotime("- 1 month"));
            $end_date = date('d-m-Y');
        }elseif($gym_type==5){//3 month
            $startdate = date('d-m-Y',strtotime("- 3 month"));
            $end_date = date('d-m-Y');
        }elseif($gym_type==6){//6 month
            $startdate = date('d-m-Y',strtotime("- 6 month"));
            $end_date = date('d-m-Y');
        }elseif($gym_type==7){//9 month
            $startdate = date('d-m-Y',strtotime("- 9 month"));
            $end_date = date('d-m-Y');
        }else {//year
            $startdate = date('d-m-Y',strtotime("- 1 year"));
            $end_date = date('d-m-Y');
        }
        if ($final_amount) {
            $response['status'] = true;
            $response['message'] = 'Total Earn Amount';
            $response['amount'] = round($final_amount,2);//$total_amount;
            $response['daily_amount'] = "";//round($daily_amount,2);//$total_amount;
            $response['monthly_amount'] = "";//round($month_amount,2);//$total_amount;
            $response['date'] = $startdate.' - '.$end_date;
        } else {
            $response['status'] = false;
            $response['message'] = 'Nothing Fetch Amount';
            $response['amount'] = '';
            $response['daily_amount'] = '';
            $response['monthly_amount'] = '';
            $response['date'] = $startdate.' - '.$end_date;
        }

        return $response;
    }

    //13-05-2020
    public function payment_period(){
        $data = Payoutdate::select('period_from','period_to','payout_date')->first();
        if(!empty($data)){
            $arr['status'] = true;
            $arr['payout_date'] = date('d-m-Y', strtotime($data->payout_date));
            $arr['start_date'] = date('d/m/Y', strtotime($data->period_from));
            $arr['to_date'] = date('d/m/Y', strtotime($data->period_to));
        }else{
            $arr['status'] = false;
        }

        echo json_encode($arr);
    }
    public function check_in_user_histroy($gymid , $start_date , $end_date) {
        $visitors = Uservisitedgym::with('user_detail')
        ->where('gym_id' , $gymid)
        ->whereBetween('user_check_in', [$start_date, $end_date])
        ->get();
        return view('admin.revenue_history.revenue_customers' , get_defined_vars());
    }

    public function check_in_histroy(Request $request) {

        if (isset($request['gym_id']) && isset($request['gym_type'])) {
            $gym_id = trim($request['gym_id']);
            $gym_type = trim($request['gym_type']);
            session()->put('gym_id', $gym_id);
            session()->put('gym_type', $gym_type);
        } else {
            $gym_type = session()->get('gym_type');
            $gym_id = session()->get('gym_id');
        }
        // echo 'gym_id '.$gym_id.' type '.$gym_type;die;
        $authuser = Auth::User();
        $patner_id = $authuser->id;
        if (isset($request['check_in_user'])) {
            $num = $request['check_in_user'];
        } else {
            $num = '';
        }
        if (!empty($num) && $num != null) {
            $data['page'] = $num;
        } else {
            $data['page'] = (request()->segment(2)) ? request()->segment(2) : 1;
        }
        $data['limit'] = 10;
        $limit = 10;
        $offset = ($data['page'] - 1) * $data['limit'];

        $check_in_detail= $this->uvg->total_visit_user_wise_check_page($gym_type, $gym_id, $offset, $limit);
       // echo '<pre>';print_r($check_in_detail);die;
        $arr['array_data'] = [];
        foreach($check_in_detail as $key=>$value){
            $user_id = $value->user_id;
            $subscription_id = $value->subscription_id;
            $arr['array_data'][$key]['gym_id'] = $gym_id;
            $arr['array_data'][$key]['user_id'] = $user_id;
            $arr['array_data'][$key]['image'] = \Config::get('values.app_url') .$value->user_image;
            $arr['array_data'][$key]['name'] = $value->first_name.' '.$value->last_name;
            $arr['array_data'][$key]['user_ufp_id'] = $value->user_ufp_id;
           // $last_visit = $this->uvg->last_visit_user_wise($gym_id,$user_id); // last visit fetch
            $user_all_visit = $this->uvg->user_all_visit_list($gym_type,$gym_id,$user_id,$subscription_id); //all visit fetch
           // $plan_name = $this->user_sub->plan_cat_name_fetch($user_id); //user plan cat name fetch
           // $last_plan_name = $this->user_sub->last_plan_cat_name_fetch($user_id); //user plan cat name fetch
            $arr['array_data'][$key]['check_in_time'] = date('h:i A', strtotime($value->last_visit_date));
            $arr['array_data'][$key]['check_in_date'] = date('d-m-Y', strtotime($value->last_visit_date));
            $arr['array_data'][$key]['total_visit'] = $value->this_gym_total_visit;
            $arr['array_data'][$key]['other_gym_total_visit'] = $value->other_gym_total_visit;
            $arr['array_data'][$key]['plan_status'] = ($value->status=='1') ? 'Active':'Expired';
            $arr['array_data'][$key]['current_visit'] = ($value->visit_pass == 'month') ? " &#8734;" : $value->visit_pass;//$plan_name->plan_category->plan_cat_name.
            $arr['array_data'][$key]['current_visit_pass'] = ($value->visit_pass == 'month') ? " &#8734; Pass" : $value->visit_pass.' visit';//$plan_name->plan_category->plan_cat_name.
            $arr['array_data'][$key]['current_paln_cat_name'] = $value->plan_cat_name;
            $arr['array_data'][$key]['user_all_visit'] = $user_all_visit;
        }
      //echo '<pre>';print_r($user_all_visit);die;
        $data['check_in_detail'] = $arr['array_data'];
        $total_rows = $this->uvg->total_visit_user_wise_check_page_count($gym_type, $gym_id);
        if(!empty($total_rows)){
            $total_rows = count($total_rows);
        }else{
            $total_rows = 0;
        }
        $data['total_rows'] = $total_rows;
        return view('admin.revenue_history.check_in_details', $data);
    }

      //28-02-2020
    public function select_box_user(Request $r){
        $input = $r->all();
        $gym_id = $input['gym_id'];
        $select = 'SELECT DISTINCT u.id,u.first_name,u.last_name FROM users u LEFT JOIN user_visited_gym uvg ON u.id=uvg.user_id WHERE uvg.gym_id='.$gym_id.'';
        $get_data = DB::select($select);
        echo json_encode($get_data);
    }


}
