<?php

namespace App\Http\Controllers\Admin;

use DB;
use Session;
use App\City;
use App\User;
use App\Country;
use Carbon\Carbon;
use App\CorporateUsers;
use App\API\Plancategory;
use App\API\Subscription;
use Illuminate\Http\Request;
use App\API\Usersubscription;
use App\CorporateSubscription;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class CorporateController extends Controller
{

    function __construct()
    {
        $this->country = new Country;
    }

    public function corporatelist()
    {
        return view('admin.corporates.corporatelist');
    }

    public function create()
    {

        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['city'] = City::select('id', 'name')->where('country', 'Pakistan')->get();
        $data['plan'] = Plancategory::select('id', 'plan_cat_name')->where('id', '!=', '4')->get();
        $data['subscription'] = Subscription::select('id', 'description')->get();

        return view('admin.corporates.add_corporation', $data);
    }

    public function getSubscription()
    {
        $v = $_REQUEST['plan'];
        $subscription = Subscription::select('id', 'visit_pass', 'plan_duration', 'plan_time', 'description')->where('plan_name', $v)->get();

        return $subscription;
    }

    public function store(Request $request)
    {


        $r = $request->all();
        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone_number' => 'required',
            'password' => 'required|string|min:8',
            'no_of_employees' => 'required|integer',
            'per_employee_rate' => 'required|integer',
            'subscription_id' => 'required',
            'corporate_name' => 'required',
            'plan_id' => 'required',
        ]);

        $r['user_type'] = 5;

//        $imageName = date('Ymdhis') . '.' . request()->user_image->getClientOriginalExtension();
//        request()->user_image->move(public_path('image/user_image'), $imageName);
        if (isset($r['firebase_token'])) {
            $firebase_token = $r['firebase_token'];
        } else {
            $firebase_token = "";
        }
        $r['firebase_token'] = $firebase_token;
        $r['password'] = Hash::make($r['password']);
        $r['user_ufp_id'] = mt_rand(100000, 999999);//$input['country'].rand(pow(10, 3-1), pow(10, 3)-1);
        $r['user_token'] = $this->generateRandomString();
        $r['user_token_time'] = new \DateTime();
        $r['is_corporate'] = 1;
        $r['partner_first_login'] = 1;
        $r['is_email_verified'] = 1;

        if ($request->hasFile('image')) {

            $sDirPath = public_path('image/user_image');
            $path = 'image/user_image';
            if (!$sDirPath) {
                mkdir($sDirPath, 0777, true);
            }
            $image = date('Ymdhis') . '.' . $request->image->getClientOriginalExtension();


            $request->image->move($path, $image);
            $r['user_image'] = $image ? $path . '/' . $image : "";
        }
//        $r['user_image'] = '/public/image/user_image/' . $imageName;


        $user = User::Create($r);
        if ($user) {
            $subscription['subscription_id'] = $request->subscription_id;
            $subscription['no_of_employees'] = $request->no_of_employees;
            $subscription['corporate_name'] = $request->corporate_name;
            $subscription['corporate_id'] = $user->id;
            $subscription['per_employee_rate'] = $request->per_employee_rate;

            $corporateSubscription = CorporateSubscription::create($subscription);

            $subscribed['user_id'] = $user->id;
            $subscribed['customer_id'] = $user->id;
            $subscribed['subscription_id'] = $request->subscription_id;

            $get_subscription_data = Subscription::whereid($request->subscription_id)->first();


            $subscribed['transaction_id'] = 12345;
            $subscribed['purchased_at'] = Carbon::now();

            $subscribed['plan_name'] = $get_subscription_data->plan_name;
            $subscribed['visit_pass'] = $get_subscription_data->visit_pass;
            $subscribed['plan_duration'] = $get_subscription_data->plan_duration;
            $subscribed['plan_time'] = $get_subscription_data->plan_time;
            $subscribed['amount'] = 0;
            $subscribed['description'] = $get_subscription_data->description ? $get_subscription_data->description : '';
            $plan_duration = $subscribed['plan_duration'];
            $plan_time = $subscribed['plan_time'];
            $subscribed['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($subscribed['purchased_at'])));


            $subscribed['payment_method'] = 'Foree';
            $subscribed['currency'] = 'PKR';
            $subscribed['charge_id'] = 12345;
            $subscribed['total_payable_amount'] = 0;

            $corporateSubscriptionAdded = Usersubscription::create($subscribed);
            $msg = "Corporate added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }


    function generateRandomString($length = 200)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function corporate_list(Request $r, $type = null)
    {
        $where = ($type) ? [0, 1] : [2]; // based on tab request 2 for pending user and 0,1 for active user
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = ($_GET['columns'][$columnIndex]['data'] == 'first_name') ? 'first_name' : 'created_at';

        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
        $search = $_GET['search']['value'];

        $select = array('id', 'first_name', 'email', 'phone_number', 'status', 'created_at' , 'status_updated_at');
        $get_data = $this->select_user_data($select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName);

        $totalRecord = $this->selectdata_count($search, $where, $startdate, $enddate);
        $results = ["draw" => intval($_GET['draw']),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $get_data];
        echo json_encode($results);


    }

    public function select_user_data($select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName)
    {

        $query = User::query($select);
        $query->whereNotIn('status', $where);
        $query->where(['is_delete' => '1', 'is_corporate' => '1']);

        if (!empty($startdate) && !empty($enddate)) {
            $query->whereDate('created_at', '>=', $startdate);
            $query->whereDate('created_at', '<=', $enddate);
        }
        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

        $query->with('countryname');
        $query->with('cityname');
        $query->skip($offset);
        $query->take($limit_t);
        $query->orderBy($columnName, $order_by);

        $user_data = $query->get();

        $data = array();
        $i = 0;
        $j = 1;
        foreach ($user_data as $key => $value) {

            $employees = CorporateUsers::where(['corporate_id' => $value->id, 'is_delete' => 1])->count();
            $corporate_users = CorporateUsers::where('corporate_id' , $value->id)->pluck('user_id')->toArray();
            $active_employees = User::whereIn('id' , $corporate_users)->where('status' , 1)->where('is_delete' , 1)->count();
            $monthly_invoice = CorporateUsers::where('corporate_id' , $value->id)->where('is_delete' , 1)->where('status' , 1)->count();
            $corporate = CorporateSubscription::where('corporate_id', $value->id)->first();
            $corporate_subscription = Usersubscription::where('user_id', $value->id)->where('status',1)->first();

            $image = \Config::get('values.app_url') . $value->user_image;
            $data[$i]['user_image'] = '<img src="' . $image . '" class="user_image" alt="' . $value->first_name . '">';

            if ($value->status == 1) {
                $status = "<a href='javascript:void(0)' data-href='" . route('corporate_status', ['id' => $value->id, 'status' => $value->status]) . "' class='btn btn-success btn-xs change_corporate_status' id='active'>Active</a>";
            } else {
                $status = "<a href='javascript:void(0)' data-href='" . route('corporate_status', ['id' => $value->id, 'status' => $value->status]) . "' class='btn btn-danger btn-xs change_corporate_status' id='inactive'>Inactive</a>";
            }
            if ($corporate->subscription_id)
                $employeeCount = " <a href='" . route('corporate-employee-list', ['id' => $value->id, 'company' => $value->first_name]) . "' class='btn btn-success btn-xs' >$employees</a>";
            else
                $employeeCount = " <a href='javascript:void(0)' class='btn btn-success btn-xs btn_no_subscription' >$employees</a>";

            $login = "<a href='" . route('login_account', ['id' => $value->id]) . "' class='btn btn-xs bg-success'>Login</a>";
            $delete ="<a href='javascript:void(0)' data-href='" . route('delete_corporate', ['id' => $value->id]) . "' class='btn btn-danger btn-xs' id='delete_corporate' onclick='return confirm(\"Are you sure to delete this corporate\")'>Delete</a>";
            $edit ="<a href='" . route('edit_corporation', ['id' => $value->id]) . "' class='btn btn-xs bg-orange'>Edit</a>";

//            $data[$i]['reg_owner'] = $value->first_name . ' ' . $value->last_name;

            $date = date('d/m/Y', strtotime($value->created_at));
            $data[$i]['id'] = $value->id;
            $data[$i]['username'] = $value->first_name;
            $data[$i]['corporate_name'] = $corporate->corporate_name;
            $data[$i]['no_of_employee'] = $corporate->no_of_employees;
            $data[$i]['email'] = $value->email;
            $data[$i]['address'] = $value->address;
            $data[$i]['country'] = ($value->countryname) ? $value->countryname->nicename : '';
            $data[$i]['city'] = ($value->cityname) ? $value->cityname->name : '';
            $data[$i]['subscribed'] = ($corporate_subscription) ? 'Yes' : 'No';
            $data[$i]['monthly_invoice'] = number_format($monthly_invoice * $corporate->per_employee_rate) . ' ' . 'Rs';
            $data[$i]['created_at'] = date('Y-m-d' , strtotime($corporate->created_at));
            $data[$i]['status_updated_at'] = $value->status_updated_at;
            $data[$i]['status'] = $status;
            $data[$i]['employees'] = $employeeCount;
            $data[$i]['active_employees'] = $active_employees;
            $data[$i]['login'] = $login;
            $data[$i]['edit'] = $edit;
            $data[$i]['delete'] = $delete;
            $i++;
            // }
        }
        return $data;
    }

    public function corporate_status($id, $status)
    {
        $up['status'] = ($status == 1) ? 0 : 1;
        $input['user_token'] = null;
        $input['user_token_time'] = null;
        $input['status'] = ($status == 1) ? 0 : 1;
        $input['status_updated_at'] = date('Y-m-d');
        $update = User::whereid($id)->update($input);

        if ($update) {
            return response()->json(['status' => true , 'message' => 'Corporate Status Updated Successfuly!']);
        } else {
            return response()->json(['status' => false , 'message' => 'Something went Wrong!']);
        }
    }

    public function selectdata_count($search, $where, $startdate, $enddate)
    {

        $query = User::query();
        $query->whereNotIn('status', $where);
        $query->where('is_delete', '=', '1');
        $query->where('is_corporate', '=', '1');
        if (!empty($startdate) && !empty($enddate)) {
            $query->whereDate('created_at', '>=', $startdate);
            $query->whereDate('created_at', '<=', $enddate);
        }
        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

//        $query->with('cityname');
        $query->with('countryname');
        //$query->with('cityname');
        $query->orderBy('created_at', 'DESC');
        $gym_data = $query->get();
        return $gym_data->count();
    }


    public function edit($id)
    {

        $data['user'] = User::where('id', $id)->first();
        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['city'] = City::select('id', 'name')->where('country', 'Pakistan')->get();
        $data['plan'] = Plancategory::select('id', 'plan_cat_name')->where('id', '!=', '4')->get();


        $data['usersubscription'] = Usersubscription::where('user_id', $id)->orderBy('id', 'desc')->first();
        $data['corporate'] = CorporateSubscription::where('corporate_id', $id)->first();

        $data['subscription'] = '';
        if ($data['usersubscription'])
            $data['subscription'] = Subscription::select('id', 'plan_duration', 'plan_time')->where('plan_name', $data['usersubscription']->plan_name)->get();
        else {
            $data['subscription'] = Subscription::select('id', 'plan_duration', 'plan_time')->get();
        }

        return view('admin.corporates.edit_corporation', $data);

    }


    public function update(Request $request, $id)
    {
        $user = User::findOrfail($id);


        $r = $request->all();

        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'phone_number' => 'required',
            'email' => ['required', 'string', 'max:255', Rule::unique('users')->ignore($user->id)],
            'corporate_name' => 'required|string|max:255',
            'no_of_employees' => 'required|integer',
            'per_employee_rate' => 'required|integer',
            'subscription_id' => 'required',
            'plan_id' => 'required',
        ]);

        if (request()->user_image) {
            $imageName = date('Ymdhis') . '.' . request()->user_image->getClientOriginalExtension();
            request()->user_image->move(public_path('image/user_image'), $imageName);
            $r['user_image'] = '/image/user_image/' . $imageName;
        } else {
            unset($r['user_image']);
        }
        if ($r['password']) {
            $r['password'] = Hash::make($r['password']);
        } else {
            unset($r['password']);
        }
//        unset($r['email']);
        $r['is_email_verified'] = 1;
        $user = $user->update($r);

        if ($user) {
            $corporateSubscription = CorporateSubscription::where('corporate_id', $id)->orderBy('id', 'desc')->first();
            if ($corporateSubscription->subscription_id != $r['subscription_id'] || $r['renew'] == 1) {
                $subscription['subscription_id'] = $request->subscription_id;
                $subscription['corporate_id'] = $id;
                $subscription['no_of_employees'] = $request->no_of_employees;
                $subscription['corporate_name'] = $request->corporate_name;
                $subscription['per_employee_rate'] = $request->per_employee_rate;
                $corporateSubscription = $corporateSubscription->update($subscription);

                $subscribed['user_id'] = $id;
                $subscribed['customer_id'] = $id;
                $subscribed['subscription_id'] = $request->subscription_id;

                $get_subscription_data = Subscription::whereid($request->subscription_id)->first();


                $subscribed['purchased_at'] = Carbon::now();

                $subscribed['plan_name'] = $get_subscription_data->plan_name;
                $subscribed['visit_pass'] = $get_subscription_data->visit_pass;
                $subscribed['plan_duration'] = $get_subscription_data->plan_duration;
                $subscribed['plan_time'] = $get_subscription_data->plan_time;
                $subscribed['amount'] = 0;
                $subscribed['description'] = $get_subscription_data->description ? $get_subscription_data->description : '';
                $plan_duration = $subscribed['plan_duration'];
                $plan_time = $subscribed['plan_time'];
                $subscribed['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($subscribed['purchased_at'])));


                $subscribed['payment_method'] = 'Foree';
                $subscribed['currency'] = 'PKR';
                $subscribed['charge_id'] = 12345;
                $subscribed['transaction_id'] = 12345;
                $subscribed['total_payable_amount'] = 0;

                $corporateSubscriptionAdded = Usersubscription::create($subscribed);

                if ($corporateSubscriptionAdded) {
                    $corporateUser = CorporateUsers::where('corporate_id', $id)->get();

                    $eS['status'] = 0;
                    foreach ($corporateUser as $employee) {
                        $employees = Usersubscription::where('user_id', $employee->user_id)->get();
                        foreach ($employees as $item) {
                            $employeeStatus = Usersubscription::find($item->id);
                            $employeeStatus->status = 0;
                            $employeeStatus->save();
                        }
                    }
//                    $employees = substr($employees, 0, -1);
//                    Usersubscription::whereIn('user_id', [$employees])->update(['status' => 0]);

                    foreach ($corporateUser as $employee) {
                        $subscribed['user_id'] = $employee->user_id;
                        $subscribed['customer_id'] = $employee->user_id;
                        $employeeSubscriptionAdded = Usersubscription::create($subscribed);

                    }

                }

            } else {
                $subscription['no_of_employees'] = $request->no_of_employees;
                $subscription['corporate_name'] = $request->corporate_name;
                $subscription['per_employee_rate'] = $request->per_employee_rate;
                $corporateSubscription = $corporateSubscription->update($subscription);
            }
            if ($user) {
                $msg = "Corporate updated successfully";
                Session::flash('msg', $msg);
                Session::flash('message', 'alert-success');
            } else {
                $msg = trans('lang_data.error');
                Session::flash('msg', $msg);
                Session::flash('message', 'alert-danger');
            }
        }
        return redirect()->back();
    }

    public function delete_corporate($corporate_id){
        $update['status'] = 0;
        $update['is_delete'] = 0;
        $updateUser = User::whereid($corporate_id)->update($update);

        if ($updateUser) {
            $updateSubscription = CorporateSubscription::where('corporate_id', $corporate_id)->update($update);
            $updateCorporateUsers = CorporateUsers::where('corporate_id', $corporate_id)->update($update);
            $update['corporate_users.status'] = 0;
            $update['corporate_users.is_delete'] = 0;
            $cTime = Carbon::now();
            $updateUsers = DB::statement("Update users u inner join corporate_users c on c.user_id = u.id set u.status=0,u.is_delete=0 where c.corporate_id=" . $corporate_id);

            return response()->json(['status' => true , 'message' => 'Corporate Deleted Successfuly!']);
        } else {
            return response()->json(['status' => false , 'message' => 'Something went Wrong!']);
        }
    }

    public function checkInList(){
        return view('admin.corporateCheckIn.checkInList');
    }

}
