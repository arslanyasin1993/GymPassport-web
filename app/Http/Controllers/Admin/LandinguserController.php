<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Landinguser;
use Illuminate\Support\Facades\Validator;
use App\Country;
use App\User;
use App\Contactus;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class LandinguserController extends Controller
{
//    echo 'hello World!';exit;
	function __construct(){
		$this->country = new Country;
		$this->user = new Landinguser;
	}

    public function index(){
    	$data['user'] = Landinguser::orderBy('id','DESC')->paginate(15);
    	$data = $data ? $data :array();
        //echo '<pre>';        print_r($data['user']);die;
    	return view('admin.landing_user.user_list',$data);
    }

    public function store(Request $r){
     //   print_r($r); die;
    	 $validatedData = $r->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'state' => 'required|string|max:255',
            'suburb' => 'required|string|max:255',
            'postal_code' => 'required|string|max:255',
            'country' => 'required',
        ]);
         // $r['password'] = Hash::make('12345678');
         // $r['user_type'] = '2';
    	 $this->user->savedata($r);
        // $insert = User::create($r);
    	 return redirect()->back();
    }


    public function ChangePassword(){
        return view('admin.changepassword');
    }

    public function update_password(Request $r){
        $user_id = Auth::user()->id;
        $getpass = Auth::user()->password;;
        $validatedData = $r->validate([
            'current_password' => 'required',
            'new_password' => 'required|min:8',
            'password' => 'required|same:new_password|min:8'
        ]);
        $password = $r->current_password;
        if(Hash::check($password, $getpass)){
                User::whereid($user_id)->update(['password'=>Hash::make($r->password)]);
                $msg = trans('lang_data.pass_success');
                Session::flash('msg',$msg);
                Session::flash('message','alert-success');
        }else{
                $msg = trans('lang_data.matchpass');
                Session::flash('msg',$msg);
                Session::flash('message','alert-danger');
        }
        return redirect('admin-change-password');
    }

   //15-10-2019

    public function becomepatner(){
        $data['user'] = User::with('countryname')->with('state_name')->where('user_type',2)->orderBy('id','DESC')->paginate(15);
        return view('admin.landing_user.become_patner_list',$data);
    }

    public function contact_us(){
        $select = 'SELECT * FROM `contact_us` ORDER BY id DESC';
        //DB::select($select);
        $data['contact_us'] = Contactus::orderBy('id','desc')->paginate(20);
         return view('admin.landing_user.contact_us_admin',$data);
    }

    public function contact_us_delete($id)
    {

        $deleteContact = Contactus::whereid($id)->delete();

        if ($deleteContact) {
            $msg = trans('lang_data.delete');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
//        dd(Session::get('msg'));
        return redirect()->route('contact_us_admin');
    }
}
