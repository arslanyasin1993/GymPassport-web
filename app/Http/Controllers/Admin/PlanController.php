<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Coupon;
use App\Couponplan;
use App\API\Plancategory;
use Session;
use DB;

class PlanController extends Controller {

    function __construct() {
        $this->coupon = new Coupon;
        $this->couponplan = new Couponplan;
    }

    public function index() {
        $plan['plan'] = Plancategory::all();

        return view('admin.plan.plan_list',$plan);
    }

    public function edit($id) {
        $plan['plan'] = Plancategory::find($id);

        return view('admin.plan.add_plan',$plan);
    }



    public function create() {
        return view('admin.plan.add_plan');
    }

    public function store(Request $r) {
        $validatedData = $r->validate([
            'plan_cat_name' => 'required',
        ]);
        $input = $r->all();

        $insert = Plancategory::create($input);
        
        if ($insert) {
            $msg = "Plan added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function update(Request $r,$id) {
        $validatedData = $r->validate([
            'plan_cat_name' => 'required',
        ]);

        $plan = Plancategory::find($id);

        $plan->plan_cat_name = $r->plan_cat_name;

        $plan->description = $r->description;

        try {

            $plan->save();
            $msg = "Plan Updated successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        }catch(\Exception $e){
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }

        return redirect('plan-list');
    }

    public function plan_list(Request $r) {
//        $order_by = $_GET['order'][0]['dir'];
//        $columnIndex = $_GET['order'][0]['column'];
//        $columnName = $_GET['columns'][$columnIndex]['data'];
//        // $columnName =  ($columnName=='username') ? 'first_name' : 'created_at';
//        $offset = $_GET['start'] ? $_GET['start'] : "0";
//        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
//        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
//        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
//        $draw = $_GET['draw'];
//        $search = $_GET['search']['value'];
//        $fetch_data = $this->coupon->select_data($order_by, $offset, $limit_t, $startdate, $enddate, $search);
        //echo '<pre>';print_r($fetch_data);die();
//        $data = $this->fetch_user_data_loop($fetch_data);
//        $totalRecord = $this->coupon->select_data_count($startdate, $enddate, $search);
        $data = Plancategory::all();
        $totalRecord = count($data);
        $results = ["draw" => intval(1),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $data];
        echo json_encode($results);
    }

    public function fetch_user_data_loop($fetch_data) {
        $data = array();
        $i = 0;
        foreach ($fetch_data as $key => $value) {
            //$plan_name = \App\API\Plancategory::where('id',$plan_id)->first();
            //$data[$i]['plan_cat_name']= $plan_id;//$plan_name->plan_cat_name;
             $delete = " <a href='" . route('delete_coupon', ['id' => $value->id]) . "' class='btn btn-danger btn-xs' id='inactive' onclick='return confirm(\"Are you sure to delete this coupon\")'>Delete</a>";
            $data[$i]['coupon_code'] = $value->coupon_code;
            $data[$i]['discount'] = $value->discount;
            $data[$i]['valid_from'] = date('d-m-Y', strtotime($value->valid_from));
            $data[$i]['valid_to'] = date('d-m-Y', strtotime($value->valid_to));
            $data[$i]['description'] = $value->description;
            $data[$i]['created_at'] = date('d-m-Y', strtotime($value->created_at));
            $data[$i]['delete'] = $delete;
            $i++;
        }

        return $data;
    }
    
    public function delete_plan($plan_id){
        //echo $coupon_id;die;
        $delete = Plancategory::destroy($plan_id);
        if ($delete) {
            return redirect()->route('plan_list');
        } else {
            return redirect()->route('plan_list');
        }
    }

    public function change_status($plan_id , $status){
        $planCategory = Plancategory::find($plan_id);
        $planCategory->status = $status;
        $planCategory->save();

        if ($planCategory) {
            return response()->json(['status' => true , 'message' => 'Plan Category Status updated Successfuly!']);
        } else {
            return response()->json(['status' => true , 'message' => 'Something went Wrong!']);
        }
    }
}
