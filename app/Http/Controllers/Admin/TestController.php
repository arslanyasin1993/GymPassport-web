<?php

namespace App\Http\Controllers\Admin;

use App\API\Subscription;
use App\API\Usersubscription;
use App\CorporateSubscription;
use App\CorporateUsers;
use App\Gym_facilities;
use App\Gymcategory;
use App\Gymtiming;
use App\Http\Controllers\Controller;
use App\Mail\SendMailable;
use App\Mail\SendTransactionMail;
use App\Payments;
use App\User;
use App\Usergym;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TestController extends Controller
{
    public function index()
    {
        $db['country'] = DB::table('country')->select('id', 'nicename')->where('id', 162)->get();
        $db['state'] = DB::table('state')->select('id', 'state')
            ->where('country_id', 162)->orderBy('state', 'ASC')->get();
        return view('admin.gym_owner.add_gym_partners', $db);
    }

    public function payfast()
    {
        return view('web.payfast');
    }
   public function payfastprocessed(Request $request )
    {
        dd($_REQUEST);

    }


    public function generate_qr_code_pdf($id)
    {

        if ($data = Usergym::where('id', $id)->first()) {

            $image_path = $data->qr_code_image;
            $gym_name = $data->gym_name;

            $pdf = PDF::loadView('qr_code', ['qr_image' => $image_path, 'gym_name' => $gym_name]);
            return $pdf->stream($gym_name . '.pdf');
        }
    }

    public function make_qr_code_pdf_to_zip($id)
    {

        if ($data = Usergym::where('id', $id)->first()) {

            $image_path = $data->qr_code_image;
            $gym_name = str_replace('/', '', $data->gym_name);
            $gym_pin_code = $data->gym_pin_code;

            $pdf_name = public_path('/pdf/' . $gym_name . '_' . $id . '_' . $gym_pin_code . '.pdf');

            $pdf = PDF::loadView('qr_code', ['qr_image' => $image_path, 'gym_name' => $gym_name])->save($pdf_name);

        }
    }

    public function run_all()
    {
        $query = Usergym::all();

        foreach ($query as $key => $value)
            $this->make_qr_code_pdf_to_zip($value->id);
        return "done";
    }


    public function store(Request $r)
    {
        $insert = $r->all();

        //unset($insert['form_source'])
        $insert['password'] = Hash::make('123456789');
        $insert['user_type'] = '2';
        $insert['partner_first_login'] = '1';
        $insert['user_ufp_id'] = mt_rand(10000, 99999);//$input['country'].rand(pow(10, 3-1), pow(10, 3)-1);
        unset($insert['_token']);
        //$validator = Validator::make($r->all(), [
        $validatedData = $r->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'state' => 'required',
            //'address' => 'required',
            'phone_number' => 'required',
            'fitness_facility' => 'required',
            'country' => 'required',
        ]);
        // dd($insert);
        $data = User::create($insert);
        $LastId = $data->id;
        if ($LastId > 0) {
            if (isset($r->created_from)) {
                $msg = "Gym Partner Added Successfully!";
            } else {
                $msg = "Thank you for registering your interest. A member of our team will contact you within 24 hours to continue the partnership process";
            }

            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function upload_gym_partner_file(Request $request)
    {
        $validatedData = $request->validate([
            'upload_gym_partner' => 'required'
        ]);

        $registeredEmails = User::select('email')->get()->toArray();
        $emaildata = array();
        foreach ($registeredEmails as $registeredemail) {
            $emaildata[] = $registeredemail['email'];
        }

        $total = 0;
        $nocreated = 0;
        if ($request->input('submit') != null) {

            $file = $request->file('upload_gym_partner');

            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv", "xlsx");
            // 2MB in Bytes
            $maxFileSize = 2097152;

            // Check file extension
            if (in_array(strtolower($extension), $valid_extension)) {

                // Check file size
                if ($fileSize <= $maxFileSize) {

                    // File upload location
                    $location = 'uploads_partner_files';
                    // Upload file
                    $file->move(public_path($location), $filename);

                    // Import CSV to Database
                    $filepath = public_path($location . "/" . $filename);
                    // Reading file
                    $file = fopen($filepath, "r");

                    $importData_arr = array();
                    $i = 0;

                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);

                        // Skip first row (Remove below comment if you want to skip the first row)
                        /*if($i == 0){
                            $i++;
                            continue;
                        }*/
                        for ($c = 0; $c < $num; $c++) {
                            // if(in_array($filedata[6],$valid_extension)){

                            // }
                            if (!in_array($filedata[2], $emaildata)) {
                                $importData_arr[$i][] = $filedata [$c];
                            }
                        }
                        $i++;
                    }
                    fclose($file);

                    if (empty($importData_arr)) {
                        Session::flash('msg', 'Something went wrong.');
                        Session::flash('message', 'alert-danger');
                    }

                    // Insert to MySQL database
                    foreach ($importData_arr as $importData) {

                        $insertData = array(

                            'first_name' => $importData[0],
                            'last_name' => $importData[1],
                            'email' => $importData[2],
                            'country' => $importData[3],
                            'state' => $importData[4],
                            'district' => $importData[5],
                            'city' => $importData[6],
                            'phone_number' => $importData[7],
                            'fitness_facility' => $importData[8],
                            'postal_code' => $importData[9],
                            'password' => Hash::make('123456789'),
                            'user_type' => '2',
                            'partner_first_login' => '1',
                            'user_ufp_id' => mt_rand(10000, 99999),
                            'created_from' => 'admin_side',
                            'created_at' => date('d-m-Y h:i A'),
                            'is_delete' => 1,
                            'status' => 1
                        );
                        $data = User::create($insertData);
                        $LastId = $data->id;
                        if ($LastId > 0) {
                            $total++;
                        } else {
                            $nocreated++;
                        }
                        if ($total > 0) {
                            Session::flash('msg', $total . " Gym Partners have been successfully created");
                            Session::flash('message', 'alert-success');
                        }

                        if ($nocreated > 0) {
                            Session::flash('msg', $nocreated . " Gym Partners have not been created");
                            Session::flash('message', 'alert-danger');
                        }
                    }

                } else {
                    Session::flash('message', 'File too large. File must be less than 2MB.');
                }

            } else {
                Session::flash('message', 'Invalid File Extension.');
            }
        }

        return redirect()->back();
    }

    public function uploadgymownercsv(Request $request)
    {
        $this->gym_mod = new Usergym;
        $validatedData = $request->validate([
            'upload_gym_owner' => 'required'
        ]);


        $total = 0;
        $nocreated = 0;
        if ($request->input('submit') != null) {

            $file = $request->file('upload_gym_owner');

            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv", "xlsx");
            // 2MB in Bytes
            $maxFileSize = 2097152;

            // Check file extension
            if (in_array(strtolower($extension), $valid_extension)) {

                // Check file size
                if ($fileSize <= $maxFileSize) {

                    // File upload location
                    $location = 'uploads_gym_files';
                    // Upload file
                    $file->move(public_path($location), $filename);

                    // Import CSV to Database
                    $filepath = public_path($location . "/" . $filename);
                    // Reading file
                    $file = fopen($filepath, "r");

                    $importData_arr = array();
                    $i = 0;

                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);

                        // Skip first row (Remove below comment if you want to skip the first row)
                        /*if($i == 0){
                            $i++;
                            continue;
                        }*/
                        for ($c = 0; $c < $num; $c++) {
                            // if(in_array($filedata[6],$valid_extension)){

                            // }
                            $email = User::select('id')
                                ->where('email', $filedata[0])
                                ->get();
                            if (isset($email[0]->id)) {
                                $importData_arr[$i][] = $filedata[$c];
                            }
                        }
                        if (isset($email[0]->id))
                            $importData_arr[$i][0] = $email[0]->id;
                        $i++;
                    }
                    fclose($file);

                    if (empty($importData_arr)) {
                        Session::flash('msg', 'Something went wrong.');
                        Session::flash('message', 'alert-danger');
                    }

                    // Insert to MySQL database
                    foreach ($importData_arr as $importData) {

                        $password = '';
                        if (isset($importData[2]) && $importData[2] != '') {
                            $password = Hash::make($importData[2]);
                            $user['password'] = $password;
                        }

                        if ($password != '') {
                            User::where('id', $importData[0])->update($user);
                        }

                        $is_all_day_open_array = json_decode($importData[11]);
                        $is_all_day_open = $is_all_day_open_array[0]->is_all_day_open;
                        $facilities = isset($importData[12]) ? json_decode($importData[12]) : '';
                        $staffhours = $importData[14] == 1 ? $importData[15] : null;
                        if ($is_all_day_open != 1) {
                            $workingdays = $is_all_day_open_array[1]->days;
                            $open_time = $is_all_day_open_array[2]->open_time;
                            $close_time = $is_all_day_open_array[3]->close_time;
                        }
                        $insertData = array(
                            'users_id' => $importData[0],
                            'gym_name' => $importData[1],
                            'gym_price_per_visit' => $importData[3],
                            'phone_number' => $importData[4],
                            'about_gym' => $importData[5],
                            'gym_address' => $importData[6],
                            'gym_latitude' => $importData[7],
                            'gym_longitude' => $importData[8],
                            'country' => $importData[9],
                            'city' => $importData[10],
                            'accept_terms_condition' => 1,
                            'status' => 1,
                            'is_all_day_open' => $is_all_day_open,
                            'is_delete' => 1,
                            'created_at' => date('d-m-Y h:i A'),
                            'gym_pin_code' => $importData[13],
                            'gym_logo' => '',
                            'is_staff_hours' => $importData[14],
                            'staff_hours' => $staffhours
                        );
                        $insertData = $insertData;
                        $data = Usergym::create($insertData);
                        $this->gen_qr_image($data->id);
                        $LastId = $data->id;
                        if ($LastId > 0) {
                            $total++;
                        } else {
                            $nocreated++;
                        }

                        if ($is_all_day_open != 1) //if not open 24 hr
                        {
                            if ($workingdays) {
                                foreach ($workingdays as $key => $val) {
                                    if ($val == 1) {
                                        Gymtiming::create([
                                            'users_gym_id' => $LastId,
                                            'gym_open_days' => $key,
                                            'gym_open_timing' => date("H:i", strtotime($open_time->{$key})),
                                            'gym_close_time' => date("H:i", strtotime($close_time->{$key})),
                                        ]); //insert gym time and open days
                                    }
                                }
                            }
                        }

                        if (!empty($facilities)) {
                            foreach ($facilities as $facility) {
                                $facilities['gym_id'] = $LastId;
                                $facilities['facilities_id'] = $facility;
                                Gym_facilities::create($facilities);
                            }
                        }

                        if ($total > 0) {
                            Session::flash('msg', $total . " Gyms have been successfully created");
                            Session::flash('message', 'alert-success');
                        }

                        if ($nocreated > 0) {
                            Session::flash('msg', $nocreated . " Gyms have not been created");
                            Session::flash('message', 'alert-danger');
                        }
                    }

                } else {
                    Session::flash('message', 'File too large. File must be less than 2MB.');
                }

            } else {
                Session::flash('message', 'Invalid File Extension.');
            }
        }

        return redirect()->back();
    }

    public function uploadCorporateEmployeeCsv(Request $request, $id)
    {

        $this->user = User::join('corporate_subscription as c', 'c.corporate_id', '=', 'users.id')->where('users.id', $id)->get();

        $validatedData = $request->validate([
            'upload_corporate_employee' => 'required'
        ]);
        if ($this->user->isEmpty()) {
            Session::flash('msg', 'There is an error in uploading file.');
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        }

        $total = 0;
        $nocreated = 0;
        if ($request->input('submit') != null) {

            $file = $request->file('upload_corporate_employee');

            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv", "xlsx");
            // 2MB in Bytes
            $maxFileSize = 2097152;

            // Check file extension
            if (in_array(strtolower($extension), $valid_extension)) {

                // Check file size
                if ($fileSize <= $maxFileSize) {

                    // File upload location
                    $location = 'uploads_corporate_employee_files';
                    // Upload file
                    $file->move(public_path($location), $filename);

                    // Import CSV to Database
                    $filepath = public_path($location . "/" . $filename);
                    // Reading file
                    $file = fopen($filepath, "r");

                    $importData_arr = array();
                    $i = 0;
                    $no_of_employees = $this->user[0]->no_of_employees;

                    $already_employeed = User::join('corporate_users as c', 'c.corporate_id', '=', 'users.id')->where('users.is_delete',1)->where('c.is_delete', 1)->where('c.corporate_id', $id)->get()->count();

                    if ($no_of_employees == 0) {
                        Session::flash('msg', 'You cannot add employees.');
                        Session::flash('message', 'alert-danger');
                        return redirect()->back();
                    }
                    if ($no_of_employees <= $already_employeed) {
                        Session::flash('msg', 'Max no of employees already added.');
                        Session::flash('message', 'alert-danger');
                        return redirect()->back();
                    }
                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        ///total columns///
                        $num = count($filedata);

                        ///total rows
                        $numRows = count(file($filepath, FILE_SKIP_EMPTY_LINES));

                        $remaining = $no_of_employees - $already_employeed;
                        if ($remaining < $numRows) {

                            Session::flash('msg', "Your remaining limit to add employees is $remaining.");
                            Session::flash('message', 'alert-danger');
                            return redirect()->back();
                        }


                        // Skip first row (Remove below comment if you want to skip the first row)
//                        if ($i == 0) {
//                            $i++;
//                            continue;
//                        }
                        for ($c = 0; $c < $num; $c++) {
                            // if(in_array($filedata[6],$valid_extension)){

                            // }
//                            $2y$10$7e05eUz6GkyW/u7s2oeVwudLlySwwS/bkhl/mX4bLYKx.v7hIkwuq
                            $email = User::select('id','is_corporate')
                                ->where('email', trim($filedata[2]))
                                ->get();
                            if (isset($email[0]->id)) {
                                if (!$email[0]->is_corporate) {
                                    $is_corporate_employee = CorporateUsers::select('id')
                                        ->where('user_id', $email[0]->id)
                                        ->get();
                                    if (!isset($is_corporate_employee[0]->id)) {
                                        $importData_arr[$i][] = trim($filedata[$c]);
                                    }
                                }
                            }

                            if (!isset($email[0]->id)) {
//                                dd($i.'in');
                                $importData_arr[$i][] = trim($filedata[$c]);
                            }
                        }
//                        if (isset($email[0]->id))
//                            $importData_arr[$i][0] = $email[0]->id;
                        $i++;
                    }
                    fclose($file);

                    if (empty($importData_arr)) {
                        Session::flash('msg', 'Something went wrong.');
                        Session::flash('message', 'alert-danger');
                    }

                    // Insert to MySQL database
                    foreach ($importData_arr as $importData) {


                        $email = User::select('id')
                            ->where('email', $importData[2])
                            ->get();
                        if (!isset($email[0]->id)) {

                            $password = mt_rand(10000000,99999999);
                            $r['password'] = Hash::make($password);

                            $r['user_ufp_id'] = mt_rand(100000, 999999);//$input['country'].rand(pow(10, 3-1), pow(10, 3)-1);
                            $r['user_token'] = $this->generateRandomString();
                            $r['user_token_time'] = new \DateTime();
                            $r['user_image'] ="/public/image/user_image/default.png";
                            $insertData = array(
                                'first_name' => $importData[0],
                                'last_name' => $importData[1],
                                'email' => $importData[2],
                                'password' => $r['password'],
                                'user_image' => $r['user_image'],
                                'user_ufp_id' => $r['user_ufp_id'],
                                'user_token' => $r['user_token'],
                                'user_token_time' => $r['user_token_time'],
                                'plain_password' => $password,
                                'partner_first_login'=>1,
                                'user_type' => 3,
                                'is_email_verified' => 1,
                                'is_corporate_user' => 1,
                            );

                            $user = User::create($insertData);

                            if ($user) {

                                $message = ['user_name' => $importData[0] . ' ' . $importData[1], 'email' => $importData[2], 'password' => $password];
                                \Mail::to($importData[2])->send(new SendMailable($message, 2));

                                $subscription['user_id'] = $user->id;
                                $subscription['corporate_id'] = $id;
                                $corporateUser = CorporateUsers::create($subscription);

                                $subscription_id = CorporateSubscription::select('subscription_id')->where('corporate_id', $id)->orderBy('id', 'desc')->first();

                                $subscribed['user_id'] = $user->id;
                                $subscribed['customer_id'] = $user->id;
                                $subscribed['subscription_id'] = $subscription_id->subscription_id;

                                $get_subscription_data = Subscription::whereid($subscription_id->subscription_id)->first();

                                $subscribed['transaction_id'] = 12345;
                                $subscribed['purchased_at'] = Carbon::now();

                                $subscribed['plan_name'] = $get_subscription_data->plan_name;
                                $subscribed['visit_pass'] = $get_subscription_data->visit_pass;
                                $subscribed['plan_duration'] = $get_subscription_data->plan_duration;
                                $subscribed['plan_time'] = $get_subscription_data->plan_time;
                                $subscribed['amount'] = 0;
                                $subscribed['description'] = $get_subscription_data->description ? $get_subscription_data->description : '';
                                $plan_duration = $subscribed['plan_duration'];
                                $plan_time = $subscribed['plan_time'];
                                $subscribed['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($subscribed['purchased_at'])));


                                $subscribed['payment_method'] = 'Foree';
                                $subscribed['currency'] = 'PKR';
                                $subscribed['charge_id'] = 12345;
                                $subscribed['total_payable_amount'] = 0;

                                $corporateSubscriptionAdded = Usersubscription::create($subscribed);

                                $total++;
                            } else {
                                $nocreated++;
                            }

                        } else {
                            $is_corporate_employee = CorporateUsers::select('user_id')
                                ->where(['corporate_id' => $id, 'user_id' => $email[0]->id])
                                ->get();
                            if (!isset($is_corporate_employee[0]->user_id)) {

                                $subscription['user_id'] = $email[0]->id;
                                $subscription['corporate_id'] = $id;
                                $corporateUser = CorporateUsers::create($subscription);

                                $subscription_id = CorporateSubscription::select('subscription_id')->where('corporate_id', $id)->orderBy('id', 'desc')->first();

                                $subscribed['user_id'] = $email[0]->id;
                                $subscribed['customer_id'] = $email[0]->id;
                                $subscribed['subscription_id'] = $subscription_id->subscription_id;

                                $update_subscription = Usersubscription::where(['user_id' => $email[0]->id, 'status' => 1])->get();

                                if ($update_subscription) {
                                    foreach ($update_subscription as $item) {
                                        $subsc = Usersubscription::find($item->id);
                                        $subsc->status = 0;
                                        $subsc->save();

                                    }
                                }

                                $get_subscription_data = Subscription::whereid($subscription_id->subscription_id)->first();

                                $subscribed['transaction_id'] = 12345;
                                $subscribed['purchased_at'] = Carbon::now();

                                $subscribed['plan_name'] = $get_subscription_data->plan_name;
                                $subscribed['visit_pass'] = $get_subscription_data->visit_pass;
                                $subscribed['plan_duration'] = $get_subscription_data->plan_duration;
                                $subscribed['plan_time'] = $get_subscription_data->plan_time;
                                $subscribed['amount'] = 0;
                                $subscribed['description'] = $get_subscription_data->description ? $get_subscription_data->description : '';
                                $plan_duration = $subscribed['plan_duration'];
                                $plan_time = $subscribed['plan_time'];
                                $subscribed['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($subscribed['purchased_at'])));


                                $subscribed['payment_method'] = 'Foree';
                                $subscribed['currency'] = 'PKR';
                                $subscribed['charge_id'] = 12345;
                                $subscribed['total_payable_amount'] = 0;

                                $corporateSubscriptionAdded = Usersubscription::create($subscribed);

                                $total++;
                            } else {
                                $nocreated++;
                            }

                        }

                        if ($total > 0) {
                            Session::flash('msg', $total . " Users have been successfully created");
                            Session::flash('message', 'alert-success');
                        } else {
                            Session::flash('msg', $nocreated . " Users have not been created");
                            Session::flash('message', 'alert-danger');
                        }
                    }

                } else {
                    Session::flash('message', 'File too large. File must be less than 2MB.');
                }

            } else {
                Session::flash('message', 'Invalid File Extension.');
            }
        }

        return redirect()->back();
    }

    function generateRandomString($length = 200)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function gen_qr_image($gym_id)
    {
        $file_formate = 'svg';
        $qr_code_image["qr_code_image"] = $gym_id . '.svg';

        if (!file_exists(base_path() . '/public/image/qr_codes/' . $qr_code_image["qr_code_image"])) {
            QrCode::format($file_formate)->size(250)
                ->generate($gym_id, public_path('/image/qr_codes/' . $qr_code_image["qr_code_image"]));

            DB::table('users_gym')
                ->where('id', $gym_id)
                ->update($qr_code_image);

        }
    }

    public function paymentHistory()
    {
        if(!empty(request()->dateFilter)){
            $dateFrom = date('Y-m-d' , strtotime(explode('-' , request()->dateFilter)[0]));
            $dateTo = date('Y-m-d' , strtotime(explode('-' , request()->dateFilter)[1]));

            $data['gym_list'] = Payments::whereDate('date' , '>=' , $dateFrom)->wheredate('date' , '<=' , $dateTo)->get();
        }else{
            $data['gym_list'] = Payments::get();
        }
        return view('admin.revenue_history.payment_history', $data);
    }

    public function paymentHistoryEmail($id)
    {
        $history = Payments::find($id);

        $message = ['gym_name' => $history->gym_name, 'email' => $history->gym_owner_email, 'trans_id' => $history->trans_id, 'amount' => $history->amount];
        \Mail::to($history->gym_owner_email)->send(new SendTransactionMail($message));

        $gym_owner = User::where('email', $history->gym_owner_email)->first();

        $smsMessage = "Dear Gym Owner,\n";
        $smsMessage .= "The payment for the previous month check-ins has been processed into your account.\n";
        $smsMessage .= "Amount:  $history->amount  PKR \n";
        $smsMessage .= "Transaction ID:  $history->trans_id \n \n";
        $smsMessage .= "Regards,\n";
        $smsMessage .= "Gym Passport";

        if ($gym_owner->phone_number)
            $this->sendSMS($smsMessage, $gym_owner->phone_number);

        Payments::updateOrCreate(
            [
                'id' => $id
            ], [
            'email_sent' => 1
        ]);

        $msg = "Transaction email has been sent successfully!";
        Session::flash('msg', $msg);
        Session::flash('message', 'alert-success');

        return redirect()->back();

    }

    public function sendSMS($message, $PhoneNo)
    {
        $message = strtr($message, array('<br>' => PHP_EOL));
        $message = strip_tags($message);
        $message = urlencode($message);
        $PhoneNo = strstr($PhoneNo, "0")?$PhoneNo:'0'.$PhoneNo;

        $phn = '92' . substr(strstr($PhoneNo, "0"), 1);
//        $_url = 'http://api.bizsms.pk/api-send-branded-sms.aspx?username=gympassport@bizsms.pk&pass=gympas386&text=' . $message . '&masking=GymPassport&destinationnum=' . $phn . '&language=English';
         $_url = "https://secure.m3techservice.com/GenericService/webservice_4_0.asmx/SendSMS?UserId=TotalSoft@Gym%20Passport&Password=G%23M9@$$90RT&MobileNo=$phn&MsgId=1234&SMS=$message&MsgHeader=939397&SMSType=0&HandsetPort=0&SMSChannel=info&Telco=";

        if ($_result = file_get_contents($_url)) {
            $_result_f = json_decode($_result);
            return 1;
        }
        return 0;
    }

    public function uploadPayments(Request $request)
    {

        $this->gym_mod = new Usergym;
        $validatedData = $request->validate([
            'upload_payment' => 'required'
        ]);


        $total = 0;
        $nocreated = 0;
        if ($request->input('submit') != null) {

            $file = $request->file('upload_payment');

            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv", "xlsx");
            // 2MB in Bytes
            $maxFileSize = 2097152;

            // Check file extension
            if (in_array(strtolower($extension), $valid_extension)) {

                // Check file size
                if ($fileSize <= $maxFileSize) {

                    // File upload location
                    $location = 'uploads_gym_files';
                    // Upload file
                    $file->move(public_path($location), $filename);

                    // Import CSV to Database
                    $filepath = public_path($location . "/" . $filename);
                    // Reading file
                    $file = fopen($filepath, "r");

                    $importData_arr = array();
                    $i = 0;

                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);
                        for ($c = 0; $c < $num; $c++) {
                            $email = DB::table('users')
                                ->leftJoin('users_gym', 'users.id', '=', 'users_gym.users_id')
                                ->where('users_gym.gym_name', $filedata[0])
                                ->where('users.email', $filedata[1])
                                ->get();

                            if (isset($email[0]->id)) {
                                $importData_arr[$i][] = $filedata[$c];
                            }
                        }
                        $i++;
                    }
                    fclose($file);

                    if (empty($importData_arr)) {
                        Session::flash('msg', 'Something went wrong.');
                        Session::flash('message', 'alert-danger');
                    }

                    // Insert to MySQL database
                    foreach ($importData_arr as $importData) {
                        $string = $importData[3];
                        $insertData = array(
                            'date' => date("Y-m-d", strtotime(substr($string, -4) . "-" . substr($string, 3, 2) . "-" . substr($string, 0, 2))),
                            'trans_id' => $importData[2],
                            'amount' => $importData[4],
                            'gym_name' => $importData[0],
                            'gym_owner_email' => $importData[1]
                        );

                        $data = Payments::create($insertData);
                        $LastId = $data->id;
                        if ($LastId > 0) {
                            $total++;
                        } else {
                            $nocreated++;
                        }
                        if ($total > 0) {
                            Session::flash('msg', $total . " Payments have been successfully created");
                            Session::flash('message', 'alert-success');
                        }

                        if ($nocreated > 0) {
                            Session::flash('msg', $nocreated . " Payments have not been created");
                            Session::flash('message', 'alert-danger');
                        }
                    }

                } else {
                    Session::flash('message', 'File too large. File must be less than 2MB.');
                }

            } else {
                Session::flash('message', 'Invalid File Extension.');
            }
        }

        return redirect()->back();
    }

    public function uploadGymPlans(Request $request)
    {

        $this->gym_mod = new Usergym;
        $validatedData = $request->validate([
            'upload_gym_owner' => 'required'
        ]);


        $total = 0;
        $nocreated = 0;
        if ($request->input('submit') != null) {

            $file = $request->file('upload_gym_owner');

            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv", "xlsx");
            // 2MB in Bytes
            $maxFileSize = 2097152;

            // Check file extension
            if (in_array(strtolower($extension), $valid_extension)) {

                // Check file size
                if ($fileSize <= $maxFileSize) {

                    // File upload location
                    $location = 'uploads_gym_files';
                    // Upload file
                    $file->move(public_path($location), $filename);

                    // Import CSV to Database
                    $filepath = public_path($location . "/" . $filename);
                    // Reading file
                    $file = fopen($filepath, "r");

                    $importData_arr = array();
                    $i = 0;

                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);
                        for ($c = 0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata[$c];
                        }
                        $i++;
                    }
                    fclose($file);

                    if (empty($importData_arr)) {
                        Session::flash('msg', 'Something went wrong.');
                        Session::flash('message', 'alert-danger');
                    }
                    // Insert to MySQL database
                    foreach ($importData_arr as $importData) {

                        $insertData = array(
                            'user_gym_id' => $importData[0],
                            'plan_category_id' => $importData[1]
                        );

                        $data = Gymcategory::create($insertData);
                        $LastId = $data->id;
                        if ($LastId > 0) {
                            $total++;
                        } else {
                            $nocreated++;
                        }
                        if ($total > 0) {
                            Session::flash('msg', $total . " Gym Plan have been successfully created");
                            Session::flash('message', 'alert-success');
                        }

                        if ($nocreated > 0) {
                            Session::flash('msg', $nocreated . " Gym Plan have not been created");
                            Session::flash('message', 'alert-danger');
                        }
                    }

                } else {
                    Session::flash('message', 'File too large. File must be less than 2MB.');
                }

            } else {
                Session::flash('message', 'Invalid File Extension.');
            }
        }

        return redirect()->back();
    }


}
