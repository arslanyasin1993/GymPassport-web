<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\API\Subscription;
use App\API\Usersubscription;
use App\Country;
use App\User;
use App\Uservisitedgym;
use Session;
use DB;
use Illuminate\Support\Facades\Config;
use App\Exports\CheckinExport;
use App\Exports\Usercurrentplan;
use Maatwebsite\Excel\Facades\Excel;
class CustomermanagementController extends Controller
{
	    public function __construct(){
	        $this->subscription = new Subscription;
	        $this->country = new Country;
	        $this->U_Sub = new Usersubscription;
	        $this->user = new User;
                $this->user_visit_gym = new Uservisitedgym;

	    }



	 public function index(){
	 	return view('admin.customer_management.customer_list');
	 }

	 public function cus_man_list(Request $re){
	 	// echo'<pre>';print_r($_GET);die;

	 	//echo'<pre>';print_r($re->all());die;
                $order_by = $_GET['order'][0]['dir'];
		$columnIndex = $_GET['order'][0]['column'];
		$columnName = $_GET['columns'][$columnIndex]['data'];
		$columnName =  ($columnName=='username') ? 'first_name' : 'created_at';

		$offset = $_GET['start'] ? $_GET['start'] :"0";
		$limit_t = ($_GET['length'] !='-1') ? $_GET['length'] :"";
		//$offset = $_GET['iDisplayStart'] ? $_GET['iDisplayStart'] :"0";
		//$limit_t = ($_GET['iDisplayLength'] !='-1') ? $_GET['iDisplayLength'] :"";
		$startdate  = ($_GET['startdate']) ? date('Y-m-d',strtotime($_GET['startdate'])) : '';
		$enddate  = ($_GET['enddate']) ? date('Y-m-d',strtotime($_GET['enddate'])) : '';
		$draw = $_GET['draw'];
		$search = $_GET['search']['value'];
		//$search = $_GET['searchdata'] ? $_GET['searchdata'] :'';
		//$search = ($_GET['sSearch'] =='') ? "" : $_GET['sSearch'];
		$array = array('id','email','first_name','last_name','d_o_b','country','state','city','phone_number','address','postal_code','user_type','user_image','created_at','status','is_corporate','is_corporate_user');
		if(empty($startdate) && empty($enddate)){
			$userdata = User::select($array)
			->where('user_type',3)
			->where('is_delete',1)
			->where(function($q) use ($search) {
                $q->whereRaw("CONCAT(first_name, ' ', last_name) LIKE ?", ['%'.$search.'%'])
                    ->orWhere('city', 'like', '%' .$search. '%')
                    ->orWhere('email', 'like', '%' .$search. '%')
                    ->orWhere('phone_number', 'like', '%' .$search. '%')
                    ->orWhere('created_at', 'like', '%' .$search. '%');
			  })
			->skip($offset)->take($limit_t)
			->orderBy($columnName,$order_by)
			->get();
		}else{
			$userdata = User::select($array)
			->where('user_type',3)
            ->where('is_delete',1)
			->whereBetween('created_at',[$startdate,$enddate])
			->where(function($q) use ($search) {
                $q->whereRaw("CONCAT(first_name, ' ', last_name) LIKE ?", ['%'.$search.'%'])
                    ->orWhere('city', 'like', '%' .$search. '%')
                    ->orWhere('email', 'like', '%' .$search. '%')
                    ->orWhere('phone_number', 'like', '%' .$search. '%')
                    ->orWhere('created_at', 'like', '%' .$search. '%');
			  })
			->skip($offset)->take($limit_t)
			->orderBy('created_at','DESC')
			->get();
		}
			//print_r($userdata);die;
			$data = $this->getuserdata($userdata);
		// print_r($data);die;
		if(empty($startdate) && empty($enddate)){
			$data_gg = User::select('id')->where('user_type',3)->where('is_delete',1)
						->where(function($q) use ($search) {
                            $q->whereRaw("CONCAT(first_name, ' ', last_name) LIKE ?", ['%'.$search.'%'])
                                ->orWhere('city', 'like', '%' .$search. '%')
                                ->orWhere('email', 'like', '%' .$search. '%')
                                ->orWhere('phone_number', 'like', '%' .$search. '%')
                                ->orWhere('created_at', 'like', '%' .$search. '%');
					      })
						->get();
		}else{
			$data_gg = User::select('id')->where('user_type',3)->where('is_delete',1)
						->whereBetween('created_at',[$startdate,$enddate])
						->where(function($q) use ($search) {
                            $q->whereRaw("CONCAT(first_name, ' ', last_name) LIKE ?", ['%'.$search.'%'])
                                ->orWhere('city', 'like', '%' .$search. '%')
                                ->orWhere('email', 'like', '%' .$search. '%')
                                ->orWhere('phone_number', 'like', '%' .$search. '%')
                                ->orWhere('created_at', 'like', '%' .$search. '%');
					      })
						->get();
		}
		$totalRecord = count($data_gg);
	 	$results = ["draw" => intval($draw),
          "iTotalRecords" => $totalRecord,
          "iTotalDisplayRecords" => $totalRecord,
          "aaData" => $data ];
         echo json_encode($results);
	 }

	 public function getuserdata($userdata){
	 		$data = array();
	 		$i=0;
	 		foreach ($userdata as $key => $value) {
	 			$image = \Config::get('values.app_url') .$value->user_image;
	 			$data[$i]['id']= $value->id;
	 			$data[$i]['user_image']= '<img src="'.$image.'" class="user_image" alt="'.$value->first_name.'">';
	 			$data[$i]['username']= $value->first_name.' '.$value->last_name;
                $data[$i]['email']= $value->email;
                $data[$i]['city'] = $value->city;
                $data[$i]['phone'] = $value->phone_number;
                $data[$i]['user_type'] = $value->is_corporate == 1 | $value->is_corporate_user == 1 ? '<span class="text text-info" style="font-weight: 900">Corprate User</span>' : '<span class="text text-success" style="font-weight: 900">General User</span>';
	 			$data[$i]['registerd_date']= date('Y-m-d',strtotime($value->created_at));
	 			$user_view = "<a href='".route('user_detail',['id'=>$value->id])."' class='btn btn-info btn-xs bg-olive'>View</a>";
	 			$check_in_details = "<a href='".route('update_sub',['id'=>$value->id])."' class='btn btn-info btn-xs bg-purple'>History</a>";
	 			$subscription_details = "<a href='".route('update_sub',['id'=>$value->id])."' class='btn btn-info btn-xs bg-navy'>View</a>";
	 			$data[$i]['view_info']= $user_view;
	 			$data[$i]['check_in_details']= $check_in_details;
	 			$data[$i]['subscription_details']= $subscription_details;
	 			if($value->status == 1){
	 				$active_inactive="<a href='javascript:void(0)' data-href='".route('user_status',['id'=>$value->id,'status'=>$value->status])."' class='btn btn-success btn-xs change_status'>Active</a>";
	 			}else{
	 				$active_inactive="<a href='javascript:void(0)' data-href='".route('user_status',['id'=>$value->id,'status'=>$value->status])."' class='btn btn-danger btn-xs change_status'>Inactive</a>";
	 			}
	 			$data[$i]['user_status']= $active_inactive;
	 			$data[$i]['action'] = "
	 			    <button data-url='".route('delete_sub',['id'=>$value->id])."' class='btn btn-danger btn-xs bg-danger' id='inactive' onclick='return confirm(\"Are you sure to delete this!\")'>Delete</button>
	 			    <a href='".route('edit_customer',['id'=>$value->id])."' class='btn btn-primary btn-xs bg-info'>Edit</a>
	 			";
	 			$i++;
	 		}

	 		return $data;
	 }

	 public function user_status($id,$status)
     {
         $update['user_token'] = null;
         $update['user_token_time'] = null;
         $update['status'] = ($status == 1) ? 0 : 1;
         $update = User::whereid($id)->update($update);

         if ($update) {
             return response()->json(['status' => true , 'message' => 'User status updatde Successfuly!']);
         } else {
             return response()->json(['status' => false , 'message' => 'Something went Wrong!']);
         }
     }

	 public function user_info($id){
	 	//echo $id;
	 	$data['user_details'] = $this->user->user_details($id);
	 	$data['subscription_details'] = $this->U_Sub->user_subscription($id);
                $data['check_in_details'] = $this->user_visit_gym->check_in_details($id);
	 	//echo '<pre>';print_r($data['check_in_details']);die;
	 	//$data['url'] = "http://localhost/ultimateFitness/";
	 	$data['url'] = \Config::get('values.app_url');
	 	$data['user_id'] = $id;
	 	return view('admin.customer_management.customer_profile_detail',$data);
	 }

	//23-01-2020
	 public function update_image(Request $r, $id){
	 	//print_r($r->all());die();
	 	$this->validate($r, ['image' => 'image|mimes:jpeg,png,jpg|max:2048',]);
	 	$imageName = date('Ymdhis') . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('image/user_image'), $imageName);
		$update = User::whereid($id)->update([
		    'user_image' => '/public/image/user_image/' . $imageName,
		]);
		if ($update) {
            $msg = "Image updated successfull";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }

        return redirect()->back();
	 }


   //19-02-2020
    public function set_user_report(Request $r){
         $input = $r->all();
         session()->put('report_val',$input['report_val']);
         session()->put('report_user_id',$input['user_id']);
    }
   public function download_user_report(Request $r){
       //$input = $r->all();
      // print_r($input);die;
        $report_val = session()->get('report_val');
        $report_user_id = session()->get('report_user_id');
       $report_data = $this->user_visit_gym->check_in_user_report_from_admin($report_val, $report_user_id);
       //echo'<pre>';print_r($report_data);die;
        $i = 1;
        $data = array();
        foreach ($report_data as $key => $value) {
            $data[$key]['se_no'] = $i;
            $data[$key]['name'] = $value->user_name->first_name . ' ' . $value->user_name->last_name;
            $plan_name = $this->user_visit_gym->get_plan_name_for_excel($value->usersubscription_id);
            $data[$key]['check_in_date'] = date('d-m-Y', strtotime($value->user_check_in));
            $data[$key]['check_in_time'] = date('h:i A', strtotime($value->user_check_in));
            $data[$key]['check_in_gym'] = $value->gym_name->gym_name;
             //echo '<pre>';print_r($plan_name);die;
            if(!empty($plan_name)){
                 $data[$key]['plan_name'] = $plan_name[0]->plan_cat_name;
                // $data[$key]['plan_cat_id'] = $plan_name[0]->plan_cat_id;
            }else{
                $data[$key]['plan_name'] = '';
            }
            //$data[$key]['check_in_amount'] = $value->gym_earn_amount;
            $i++;
        }
        session()->forget('report_val');
        session()->forget('report_user_id');
        return Excel::download(new CheckinExport($data), 'check_in_report.xlsx');
         //echo'<pre>';print_r($data);die;
   }

   //20-2-2020 for download customer plan
   public function download_current_plan() {
       $user_list = $this->user->get_current_plan();
       //echo'<pre>';print_r($user_list);die;
        $i = 1;
        $data = array();
        foreach ($user_list as $key => $value) {
            $data[$key]['se_no'] = $i;
            $data[$key]['ufp_id'] = $value->user_ufp_id;
            $data[$key]['name'] = $value->first_name . ' ' . $value->last_name;
            $data[$key]['email'] = $value->email;
            if(isset($value->last_subscription) && !empty($value->last_subscription)){
                $data[$key]['plan_name'] = $value->last_subscription->plan_category->plan_cat_name;
                $data[$key]['visit_pass'] =  ($value->last_subscription->visit_pass=='month') ? 'Monthly Pass': $value->last_subscription->visit_pass.' Visit Pass';
                $data[$key]['status'] = ($value->last_subscription->status=='1') ? 'Active':'Expire';
            }else{
                $data[$key]['plan_name'] ='';
                $data[$key]['visit_pass'] ='';
                $data[$key]['status'] ='';
            }
            $i++;
        }
       // echo '<pre>';print_r($data);die;
         return Excel::download(new Usercurrentplan($data), 'customer_current_plan.xlsx');

   }
}
