<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\User;
use App\Usergym;
use App\CorporateUsers;
use App\Uservisitedgym;
use Illuminate\Http\Request;
use App\API\Usersubscription;
use App\CorporateSubscription;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CorporateEmployeeCheckInDetailController extends Controller
{
    public function index()
    {
        $corporate_id = $_GET['corporateId'];
        $this->data['corporateName'] = CorporateSubscription::where('corporate_id', $corporate_id)->orderBy('id','desc')->pluck('corporate_name')->first();

        return view('admin.corporates.employee_check_in.users_check_in',$this->data);
    }

    public function checkInDetailList(Request $req, $id)
    {
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = $_GET['columns'][$columnIndex]['data'];

        if ($columnName == 'username') {
            $columnName = ($columnName == 'username') ? 'u.first_name' : $columnName;
        } elseif ($columnName == 'user_id') {
            $columnName = ($columnName == 'user_id') ? 'u.user_ufp_id' : $columnName;
        } elseif ($columnName == 'email') {
            $columnName = ($columnName == 'email') ? 'u.email' : $columnName;
        } else {
            $columnName = ($columnName == 'gym_name') ? 'g.gym_name' : 'a.user_check_in';
        }

        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';

        $draw = $_GET['draw'];
        $search = $_GET['search']['value'];

        $details = $this->check_in_details($id, $startdate, $enddate, $search, $offset, $limit_t, $columnName, $order_by);
        $array = [];
        foreach ($details['data'] as $detail) {
            $arr = [];
            $array[] = array(
                "username" => $detail->first_name . " " . $detail->last_name,
                "user_id" => $detail->user_ufp_id,
                "email" => $detail->email,
                "phone_number" => $detail->phone_number,
                "check_in_date" => date('d-m-Y' , strtotime($detail->user_check_in)),
                "check_in_time" => date('H:i:s' , strtotime($detail->user_check_in)),
                "gym_name" => $detail->gym_name,
                "amount" => $detail->gym_earn_amount,
                'plan_name' => $detail->plan_cat_name,
                'gym_plan_name' => $detail->gym_plan_name,
                'action' => "<a href='javascript:void(0);' data-url='" . route('delete_corporate_employee_check_in', ['id' => $detail->id]) . "' class='btn btn-xs bg-red del_btn'>Delete</a>",
            );
        }
        $totalRecord = Uservisitedgym::select('user_visited_gym.id');

        if ($search) {
            $totalRecord = $totalRecord->where(function ($q) use ($search) {
                $q->where('u.first_name', 'like', '%' . $search . '%')
                    ->orWhere('u.last_name', 'like', '%' . $search . '%')
                    ->orWhere('u.email', 'like', '%' . $search . '%')
                    ->orWhere('g.gym_name', 'like', '%' . $search . '%');
            });
        }
        if ($startdate && $enddate) {
            $totalRecord = $totalRecord->whereBetween('user_visited_gym.user_check_in', [$startdate . ' 00:00:00', $enddate . ' 23:59:59']);
        }
        $user_id_arr = CorporateUsers::select('user_id')->where('corporate_id', $id)->get()->pluck('user_id');

        $totalRecord = $totalRecord->whereIn('u.id', $user_id_arr)
            ->leftjoin('users as u', 'u.id', '=', 'user_visited_gym.user_id')
            ->leftjoin('users_gym as g', 'g.id', '=', 'user_visited_gym.gym_id')
            ->leftjoin('usersubscriptions as us', 'us.id', '=', 'user_visited_gym.usersubscription_id')
            ->leftjoin('subscriptions as s', 's.id', '=', 'us.subscription_id')
            ->leftjoin('plan_category as p', 'p.id', '=', 's.plan_name')->count();
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "revenue" => $details['revenue'] . ' ' . 'Rs',
            "aaData" => $array
        );
        echo json_encode($response);
        exit;
    }

    public function check_in_details($user_id, $startdate = null, $enddate = null, $search = null, $offset = null, $limit = null, $columnName = null, $orderBy = null)
    {
        $data = DB::table('user_visited_gym as a')->select('a.*', 'u.first_name', 'u.last_name', 'u.user_ufp_id', 'u.email', 'u.phone_number', 'g.gym_name', 'p.plan_cat_name');
        if ($startdate && $enddate) {
            $data = $data->whereBetween('a.user_check_in', [$startdate . ' 00:00:00', $enddate . ' 23:59:59']);
        }
        $user_id_arr = CorporateUsers::where('corporate_id', $user_id)->pluck('user_id');

        $data = $data->whereIn('a.user_id', $user_id_arr);
        if ($search) {
            $data = $data->where(function ($q) use ($search) {
                $q->where('u.first_name', 'like', '%' . $search . '%')
                    ->orWhere('u.last_name', 'like', '%' . $search . '%')
                    ->orWhere('u.email', 'like', '%' . $search . '%')
                    ->orWhere('g.gym_name', 'like', '%' . $search . '%');
            });
        }

        $revenue = $this->check_in_detail_amount($user_id , $startdate , $enddate , $search);

        $data = $data->leftjoin('users as u', 'u.id', '=', 'a.user_id')
        ->leftjoin('users_gym as g', 'g.id', '=', 'a.gym_id')
        ->leftjoin('usersubscriptions as us', 'us.id', '=', 'a.usersubscription_id')
        ->leftjoin('subscriptions as s', 's.id', '=', 'us.subscription_id')
        ->leftjoin('plan_category as p', 'p.id', '=', 's.plan_name')
        ->orderBy($columnName, $orderBy)
        ->offset($offset)
        ->limit($limit)
        ->get();

        foreach ($data as $record){
            $gym_plan_name = DB::table('gymcategories')->join('plan_category' , 'plan_category.id' , '=' , 'gymcategories.plan_category_id')
            ->where('user_gym_id' , $record->gym_id)->where('gymcategories.plan_category_id' , '!=' , 4)->first(['plan_category.plan_cat_name']);
            $record->gym_plan_name = $gym_plan_name->plan_cat_name;
        }
        return ['data' => $data , 'revenue' => $revenue];
    }

    public function check_in_detail_amount($user_id, $startdate = null, $enddate = null , $search = null)
    {
        $data = DB::table('user_visited_gym as a');
        if ($startdate && $enddate) {
            $data = $data->whereBetween('a.user_check_in', [$startdate . ' 00:00:00', $enddate . ' 23:59:59']);
        }
        $user_id_arr = CorporateUsers::where('corporate_id', $user_id)->where('is_delete' , 1)->pluck('user_id');

        $data = $data->whereIn('a.user_id', $user_id_arr);
        if ($search) {
            $data = $data->where(function ($q) use ($search) {
                $q->where('u.first_name', 'like', '%' . $search . '%')
                    ->orWhere('u.last_name', 'like', '%' . $search . '%')
                    ->orWhere('u.email', 'like', '%' . $search . '%')
                    ->orWhere('g.gym_name', 'like', '%' . $search . '%');
            });
        }

        $data = $data->leftjoin('users as u', 'u.id', '=', 'a.user_id')
        ->leftjoin('users_gym as g', 'g.id', '=', 'a.gym_id')
        ->leftjoin('usersubscriptions as us', 'us.id', '=', 'a.usersubscription_id')
        ->leftjoin('subscriptions as s', 's.id', '=', 'us.subscription_id')
        ->leftjoin('plan_category as p', 'p.id', '=', 's.plan_name')
        ->sum('a.gym_earn_amount');

        return $data;
    }

    public function getUserSubscription($id)
    {

        $data['userSubscription'] = Usersubscription::select('p.plan_cat_name', 'p.id')->where('usersubscriptions.user_id', $id)->join('subscriptions as s', 's.id', '=', 'usersubscriptions.subscription_id')
            ->join('plan_category as p', 'p.id', '=', 's.plan_name')->orderBy('usersubscriptions.id', 'desc')->first();

        $data['userGyms'] = '';

        if ($data['userSubscription']) {
            $plan_id = $data['userSubscription']->id;
            $data['userGyms'] = DB::select(DB::raw("SELECT users_gym.id as gym_id,users_gym.gym_name,is_all_day_open,gym_logo,gym_address,users_gym.status FROM users_gym 
                INNER JOIN gymcategories AS gc ON users_gym.id = gc.user_gym_id AND gc.plan_category_id  = $plan_id
                WHERE users_gym.status = 1"));
        }

        return $data;
    }

    public function delete_check_in($id){
        $checkIn = Uservisitedgym::find($id);
        $checkIn->delete();

        if ($checkIn) {
            return response()->json(['status' => true , 'message' => 'CheckIn deleted Successfuly!']);
        } else {
            return response()->json(['status' => false , 'message' => 'Something went Wrong!']);
        }
        return redirect()->back();
    }
}
