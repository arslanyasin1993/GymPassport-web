<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Usergym;
use App\Gymimage;
use App\Uservisitedgym;
use App\API\Usersubscription;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Gymcategory;
use Session;
use Illuminate\Support\Facades\Validator;

//use Excel;

class UploadreportController extends Controller {

    function __construct() {
        $this->gym_mod = new Usergym;
        $this->user_visit_gym = new Uservisitedgym;
        $this->gym_plan_cat = new Gymcategory;
        $this->user = new User;
        $this->us = new Usersubscription;
    }

    public function index() {

        //$data['gym'] = Usergym::select('id', 'gym_name')->orderBy('gym_name', 'asc')->get();
        $gym = Usergym::select('id', 'gym_name')->where('status', 1)->orderBy('gym_name', 'asc')->get();
        $arr['arr'] = [];
        foreach ($gym as $key => $value) {
            $plan_cat_name = $this->gym_mod->plan_name_by_gym_id($value->id);
            if (!empty($plan_cat_name)) {
                $arr['arr'][$key]['id'] = $value->id;
                $arr['arr'][$key]['gym_name'] = $value->gym_name;
            }
        }
        $data['gym'] = $arr['arr'];
        return view('admin.upload_report.uploadreport', $data);
    }

    public function importFileIntoDB(Request $request) {
        $validatedData = $request->validate([
            'gym_name' => 'required',
            'check_in_report' => 'required',
        ]);
        $file = $request->file('check_in_report');
        // File Details 
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $tempPath = $file->getRealPath();
        $fileSize = $file->getSize();
        $mimeType = $file->getMimeType();
        // Valid File Extensions
        $valid_extension = array("csv");
        // 2MB in Bytes
        $maxFileSize = 2097152;
        // Check file extension
        if (in_array(strtolower($extension), $valid_extension)) {
            // Check file size
            if ($fileSize <= $maxFileSize) {
                $filename = $_FILES["check_in_report"]["tmp_name"];
                if ($_FILES["check_in_report"]["size"] > 0) {
                    $valid_extension = array("csv");
                    $maxFileSize = 2097152;
                    $file = fopen($filename, "r");
                    $importData_arr = array();
                    $i = 0;
                    $array_key = array('check_in_date', 'check_in_time', 'member_name', 'ufp_id');
                    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
                        if ($i == 0) {
                            $i++;
                            continue;
                        }
                        $num = count($getData);
                        for ($c = 0; $c < $num; $c++) {
                            $importData_arr[$i][] = $getData[$c];
                        }
                        $i++;
                    }
                    fclose($file);
                    foreach ($importData_arr as $data) {
                        $da = explode('/', $data[0]);
                        // $check_in = $da[2].'-'.$da[1].'-'.$da[0] . ' ' . $data[1];
                        $check_in = $data[0] . ' ' . $data[1];
                        $check_in_date = date('Y-m-d H:i:s', strtotime($check_in));
                        $user_name = $data[2];
                        $ufp_id = $data[3];
                        $user_current_plan = $this->user->select_plan_from_ufpid($ufp_id);
                        //echo'<pre>';print_r($user_current_plan);
                        $gym_id = $request->gym_name;
                        if (!empty($user_current_plan)) {
                            $user_id = trim($user_current_plan['id']);
                            $UserCheckinPlanBetweenDate = $this->us->select_month_plan_checkin_date($user_id, $check_in_date);
                            if (!empty($UserCheckinPlanBetweenDate)) {
                                $final_divided_amount = $this->given_amount_patner_after_visit($UserCheckinPlanBetweenDate); //amount divide
                                $user_subscription_id = $UserCheckinPlanBetweenDate->id;
                                $user_have_visit_pass = $UserCheckinPlanBetweenDate->visit_pass;
                                $user_plan_category_id = $UserCheckinPlanBetweenDate->plan_name;
                                $insert['user_id'] = $user_id;
                                $insert['gym_id'] = $gym_id;
                                $insert['usersubscription_id'] = $user_subscription_id;
                                $insert['check_in_status'] = 1;
                                $insert['user_check_in'] = $check_in_date;
                                $insert['gym_earn_amount'] = $final_divided_amount;
                                $amount_update['gym_earn_amount'] = $final_divided_amount;

                                $get_plan_wise_gym = $this->gym_plan_cat->get_gym_plan_id($user_plan_category_id);
                                $gym_array = array_column($get_plan_wise_gym, 'user_gym_id');
                                if (in_array($gym_id, $gym_array)) { //if user plan and gym plan match
                                    if ($user_have_visit_pass == 'month') { // if user monthly plan
                                        $insert_data = Uservisitedgym::create($insert);
                                         Uservisitedgym::where('usersubscription_id',$user_subscription_id)->update($amount_update);
                                       // echo '<pre>';print_r($insert);die;
                                    }
                                    Session::flash('msg', 'Upload Report Import Successful.');
                                    Session::flash('message', 'alert-success');
                                }else{
                                    Session::flash('msg', 'Gym not available on user plan');
                                    Session::flash('message', 'alert-danger');
                                }
                            }else{
//                                 Session::flash('msg', 'Please check visited date in sheet');
//                                 Session::flash('message', 'alert-danger');
                            }
                            //echo '<pre>';print_r($UserCheckinPlanBetweenDate);die;
                        } else {
                           Session::flash('msg', 'User not found.');
                           Session::flash('message', 'alert-danger');
                        }
                    }
                    //die();
                }
            } else {
                Session::flash('msg', 'File too large. File must be less than 2MB.');
                Session::flash('message', 'alert-danger');
            }
        } else {
            Session::flash('msg', 'Invalid File Extension. please upload file in cvv format');
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function given_amount_patner_after_visit($user_subscription) {
        //dd($user_subscription);die;
        $for_patner_percentage = 81;
       $user_payable_amount = $user_subscription->amount;
        $total_visit_pass = $user_subscription->visit_pass;
        if ($total_visit_pass != 'month') {
            $for_final_distribute_amount = ($for_patner_percentage / 100) * $user_payable_amount;
            $amount_for_patner = ($for_final_distribute_amount / $total_visit_pass);
            $final_amount_for_gym_patner = number_format((float) $amount_for_patner, 2, '.', '');
        } else {
            $visit = Uservisitedgym::select('id')->where('usersubscription_id', $user_subscription->id)->get();
            $total_visit_pass = $visit->count();
            if ($total_visit_pass > 0) {
                $total_visit_pass = ($total_visit_pass + 1);
                $for_final_distribute_amount = ($for_patner_percentage / 100) * $user_payable_amount;
                $amount_for_patner = ($for_final_distribute_amount / $total_visit_pass);
            } else {
                $total_visit_pass = 1;
                $for_final_distribute_amount = ($for_patner_percentage / 100) * $user_payable_amount;
                $amount_for_patner = ($for_final_distribute_amount / $total_visit_pass);
            }
            $final_amount_for_gym_patner = number_format((float) $amount_for_patner, 2, '.', '');
            //$final_amount_for_gym_patner = '00.00';
        }

        return $final_amount_for_gym_patner;
    }

}
