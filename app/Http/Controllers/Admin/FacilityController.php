<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Facilities;
use Session;

class FacilityController extends Controller
{
    function __construct(){

    }

    public function index($id=null){
    	//https://ufitpass.com/public/gym_images/facility_icon/spa.png
    	if(empty($id)){
    		$data['facilities'] = Facilities::orderBy('id','ASC')->paginate(10);
    	}else{
    		$data['data_facilities'] = Facilities::whereid($id)->first();
    	}
    	
    	return view('admin.facilities.facilities',$data);
    }

    public function store(Request $r){
    	$validatedData = $r->validate([
            'facilities' => 'required|string|unique:facilities',
            'facilities_icon.*' => 'required_without_all|image|mimes:jpeg,png,jpg',
            'facilities_icon' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $input = $r->all();
        if ($r->hasFile('facilities_icon')) {
	    		$sDirPath = public_path('gym_images/facility_icon');
	    		$path = trans('constants.image_url').'public/gym_images/facility_icon';
	    		if(!$sDirPath){
	    			$oldmask = umask(0);
	    			mkdir($sDirPath,0777,true);
	    			umask($oldmask);
	    		}

	    		$icon = request()->facilities_icon->getClientOriginalName();
	            request()->facilities_icon->move($sDirPath, $icon);
	            $input['imag_url'] = $icon ? $path.'/'.$icon :"";
			}
		unset($input['facilities_icon']);
		unset($input['_token']);
		//echo '<pre>';print_r($input);die;
		 if(Facilities::create($input)){
				$msg = "facilities added successfully";
				Session::flash('msg',$msg);
				Session::flash('message','alert-success');
   		 }else{
				$msg = trans('lang_data.error');
				Session::flash('msg',$msg);
				Session::flash('message','alert-danger');
   		 }
   		 return redirect()->back();

    }

    public function update(Request $r,$id){
    	$validatedData = $r->validate([
            //'facilities' => 'required|string|unique:facilities',
            'facilities' => 'required|string',
            // 'facilities_icon.*' => 'required_without_all|image|mimes:jpeg,png,jpg',
            // 'facilities_icon' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        $input = $r->all();
        if ($r->hasFile('facilities_icon')) {
	    		$sDirPath = public_path('gym_images/facility_icon');
	    		$path = trans('constants.image_url').'public/gym_images/facility_icon';
	    		if(!$sDirPath){
	    			$oldmask = umask(0);
	    			mkdir($sDirPath,0777,true);
	    			umask($oldmask);
	    		}

	    		$icon = request()->facilities_icon->getClientOriginalName();
	            request()->facilities_icon->move($sDirPath, $icon);
	            $input['imag_url'] = $icon ? $path.'/'.$icon :"";
			}
		unset($input['facilities_icon']);
		unset($input['_token']);
		//echo '<pre>';print_r($input);die;
		 if(Facilities::whereid($id)->update($input)){
				$msg = "facilities updated successfully";
				Session::flash('msg',$msg);
				Session::flash('message','alert-success');
   		 }else{
				$msg = trans('lang_data.error');
				Session::flash('msg',$msg);
				Session::flash('message','alert-danger');
   		 }
   		 return redirect()->back();
    }

    public function destroy($id){
        $data = Facilities::find($id);
        $check = $data->delete();
        if ($check) {
            $msg = 'Record deleted successfully';
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }


}
