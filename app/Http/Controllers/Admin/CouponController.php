<?php

namespace App\Http\Controllers\Admin;

use App\CorporateSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Coupon;
use App\Couponplan;
use App\API\Plancategory;
use Session;
use DB;
use App\User;


class CouponController extends Controller {

    function __construct() {
        $this->coupon = new Coupon;
        $this->couponplan = new Couponplan;
    }

    public function index() {
        return view('admin.coupon_code.coupon_list');
    }

    public function create() {
        $data['plan_cat'] = Plancategory::select('id', 'plan_cat_name')->get();
        $data['corporates'] = CorporateSubscription::where('status' , 1)->where('is_delete' , 1)->get();
        return view('admin.coupon_code.add_coupon', $data);
    }

    public function store(Request $r) {
        $validatedData = $r->validate([
            'plan_cat_id' => 'required_without_all',
            'coupon_code' => 'required|string|max:255',
            'discount' => 'required',
            'valid_from' => 'required',
            'valid_to' => 'required',
            'description' => 'required',
            //'image' => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);
        $input = $r->all();
        // unset($input['image']);
        unset($input['plan_cat_id']);
//        if ($r->hasFile('image')) {
//            $sDirPath = public_path('coupon_image');
//            $path = 'public/coupon_image';
//            $image = date('Ymdhis') . '.' . request()->image->getClientOriginalExtension();
//            request()->image->move($sDirPath, $image);
//            $input['image'] = $image ? $path . '/' . $image : "";
//        }
        
        // if(!isset($input['is_one_time_use_coupon'])){
            
        // }
        $insert = Coupon::create($input);
                
        if ($insert) {
            $insert_id = $insert->id;
             if($r->plan_cat_id[0]=='6'){
                $array = array('1','2','3');
              }else{
                $array = $r->plan_cat_id;
              }
            foreach ($array as $key => $value) {
                $add_plan['coupon_id'] = $insert_id;
                $add_plan['plan_cat_id'] = $value;
                Couponplan::create($add_plan);
            }
            $msg = "Coupon added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function coupon_list(Request $r) {
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = $_GET['columns'][$columnIndex]['data'];
        // $columnName =  ($columnName=='username') ? 'first_name' : 'created_at';
        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
        $draw = $_GET['draw'];
        $search = $_GET['search']['value'];
        $fetch_data = $this->coupon->select_data($order_by, $offset, $limit_t, $startdate, $enddate, $search);
        //echo '<pre>';print_r($fetch_data);die();
        $data = $this->fetch_user_data_loop($fetch_data);
        $totalRecord = $this->coupon->select_data_count($startdate, $enddate, $search);

        $results = ["draw" => intval($draw),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $data];
        echo json_encode($results);
    }

    public function fetch_user_data_loop($fetch_data) {
        $data = array();
        $i = 0;
        foreach ($fetch_data as $key => $value) {
            //$plan_name = \App\API\Plancategory::where('id',$plan_id)->first();
            //$data[$i]['plan_cat_name']= $plan_id;//$plan_name->plan_cat_name;
             $delete = " <a href='javascript:void(0)' data-href='" . route('delete_coupon', ['id' => $value->id]) . "' class='btn btn-danger btn-xs' id='delete_coupon' onclick='return confirm(\"Are you sure to delete this coupon\")'>Delete</a>";
            $data[$i]['coupon_code'] = $value->coupon_code;
            $data[$i]['discount'] = $value->discount;
            $data[$i]['valid_from'] = date('d-m-Y', strtotime($value->valid_from));
            $data[$i]['valid_to'] = date('d-m-Y', strtotime($value->valid_to));
            $data[$i]['description'] = $value->description;
            $data[$i]['created_at'] = date('d-m-Y', strtotime($value->created_at));
            $data[$i]['delete'] = $delete;
            $i++;
        }

        return $data;
    }
    
    public function delete_coupon($coupon_id){
        $update['status'] = 0;
        $update['is_delete'] = 0;
        $update = Coupon::whereid($coupon_id)->update($update);

        if ($update) {
            return response()->json(['status' => true , 'message' => 'Coupon deleted Successfuly!']);
        } else {
            return response()->json(['status' => false , 'message' => 'Something went Wrong!']);
        }
    }

    public function get_Notifi_index() {
        return view('admin.notifications.notification');
    }

    public function send_notification(Request $r)
    {
        $validatedData = $r->validate([
            'types' => 'required',
            'title' => 'required',
            'message' => 'required',
        ]);
        $input = $r->all();
        if($input['types'] == 1) {
            $users = User::get();
            $user_tokens = array();
            foreach ($users as $key => $value) {
                if(!empty($value->firebase_token)) {
                    array_push($user_tokens, $value->firebase_token);
                }
            }
            $this->fire_single_notification($input['title'], $input['message'], '',$user_tokens);
        }
        return redirect()->back();
    }

    public function fire_single_notification($title = '', $description = '', $url = '', $firebase_token) {
        $usestokens = json_encode($firebase_token);
        $fields = '{
            "registration_ids" : '.$usestokens.',
            "notification" : {
				"title" : "' . $title . '",
				"body" : "' . $description . '",
				"url" : "' . $url . '",
				"type" : "simple",
				"sound" : "default",
            },
            "data" : {
				"title" : "' . $title . '",
				"body" : "' . $description . '",
				"url" : "' . $url . '",
				"type" : "simple",
				"sound" : "default",
             }
        }';

        $url = 'https://fcm.googleapis.com/fcm/send';
        $setting = "AAAAYJ_iTiA:APA91bH8yDT-0do3nTZNawiSpkhV50t_XJgU4Tbysd8T51T7KaqXdFkdIfG5x-5Ws-WEDX_4Whn-xOGPbr1tL2exXUPmwzMT_kufTBuYaru8R-6UULusKBBaWfE7rEtQCYhgABKK07Hj";
        $headers = array(
            'Authorization: key=' . $setting,
            'Content-Type: application/json',
        );

        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        curl_close($ch);

    }

}
