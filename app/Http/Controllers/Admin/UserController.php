<?php

namespace App\Http\Controllers\Admin;

use App\API\Plancategory;
use App\API\Subscription;
use App\API\Usersubscription;
use App\Autorenewalfaild;
use App\City;
use App\CorporateSubscription;
use App\CorporateUsers;
use App\Country;
use App\Http\Controllers\Controller;
use App\User;
use App\Usercard;
use App\Userusecoupon;
use App\Uservisitedgym;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Session;

class UserController extends Controller
{
    function __construct()
    {
        $this->country = new Country();
    }

    public function deleteUserData($email)
    {

        $user = User::where('email', $email)->first();
        if ($user) {
            if ($user->is_corporate) {
                $corporateUsers = CorporateUsers::select('user_id')->where('corporate_id', $user->id)->get();
                $userSubscription = Usersubscription::where('user_id', $user->id)->get();
                foreach ($corporateUsers as $corporateUser) {
                    $userSubscription = Usersubscription::where('user_id', $corporateUser->user_id)->delete();
                    $userVisitedGym = Uservisitedgym::where('user_id', $corporateUser->user_id)->delete();
                    $autoRenewalFaild = Autorenewalfaild::where('user_id', $user->id)->delete();
                    $userUseCoupon = Userusecoupon::where('user_id', $user->id)->delete();
                    if (DB::table('user_bank_detail')->where('user_id', $user->id)->get()) {
                        DB::table('user_bank_detail')->where('user_id', $user->id)->delete();
                    }
                    User::find($corporateUser->user_id)->delete();
                }
                CorporateUsers::where('corporate_id', $user->id)->delete();
                CorporateSubscription::where('corporate_id', $user->id)->delete();
                Usersubscription::where('user_id', $user->id)->delete();
                $user->delete();
                return 'This corporate user and its employees deleted Successfully';

            } else {
                $userCard = Usercard::where('user_id', $user->id)->delete();
                $corporateUsers = CorporateUsers::where('user_id', $user->id)->delete();
                $userVisitedGym = Uservisitedgym::where('user_id', $user->id)->delete();
                $autoRenewalFaild = Autorenewalfaild::where('user_id', $user->id)->delete();
                $userUseCoupon = Userusecoupon::where('user_id', $user->id)->delete();
                if (DB::table('user_bank_detail')->where('user_id', $user->id)->get()) {
                    DB::table('user_bank_detail')->where('user_id', $user->id)->delete();
                }
                $userSubscription = Usersubscription::where('user_id', $user->id)->delete();
                $user->delete();
            }
            return 'User Deleted Successfully';

        }
        return 'There is no user.';

    }

    public function create()
    {
        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['city'] = City::select('id', 'name')->where('country', 'Pakistan')->get();
        $data['plan'] = Plancategory::select('id', 'plan_cat_name')->where('id', '!=', '4')->get();
        $data['subscription'] = Subscription::select('id', 'description')->get();
        return view('admin.customer_management.add_customer', $data);
    }


    public function store(Request $request)
    {


        $r = $request->all();
        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone_number' => 'required|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'subscription_id' => 'nullable',
            'plan_id' => 'nullable',
        ]);

        $r['user_type'] = 3;

//        $imageName = date('Ymdhis') . '.' . request()->user_image->getClientOriginalExtension();
//        request()->user_image->move(public_path('image/user_image'), $imageName);
        if (isset($r['firebase_token'])) {
            $firebase_token = $r['firebase_token'];
        } else {
            $firebase_token = "";
        }
        $r['firebase_token'] = $firebase_token;
        $r['password'] = Hash::make($r['password']);
        $r['user_ufp_id'] = mt_rand(100000, 999999);//$input['country'].rand(pow(10, 3-1), pow(10, 3)-1);
        $r['user_token'] = $this->generateRandomString();
        $r['user_token_time'] = new \DateTime();
        $r['partner_first_login'] = 1;


        //        $r['user_image'] = '/public/image/user_image/' . $imageName;


        $user = User::Create($r);
        if ($user) {
            if (isset($request->subscription_id)) {

                $subscribed['user_id'] = $user->id;
                $subscribed['customer_id'] = $user->id;
                $subscribed['subscription_id'] = $request->subscription_id;

                $get_subscription_data = Subscription::whereid($request->subscription_id)->first();


                $subscribed['transaction_id'] = 12345;
                $subscribed['purchased_at'] = Carbon::now();

                $subscribed['plan_name'] = $get_subscription_data->plan_name;
                $subscribed['visit_pass'] = $get_subscription_data->visit_pass;
                $subscribed['plan_duration'] = $get_subscription_data->plan_duration;
                $subscribed['plan_time'] = $get_subscription_data->plan_time;
                $subscribed['amount'] = 0;
                $subscribed['description'] = $get_subscription_data->description ? $get_subscription_data->description : '';
                $plan_duration = $subscribed['plan_duration'];
                $plan_time = $subscribed['plan_time'];
                $subscribed['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($subscribed['purchased_at'])));


                $subscribed['payment_method'] = 'Foree';
                $subscribed['currency'] = 'PKR';
                $subscribed['charge_id'] = 12345;
                $subscribed['total_payable_amount'] = 0;

                $corporateSubscriptionAdded = Usersubscription::create($subscribed);
            }
            $msg = "User added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    function generateRandomString($length = 200)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}
