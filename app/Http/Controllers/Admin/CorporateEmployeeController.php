<?php

namespace App\Http\Controllers\Admin;

use DB;
use Session;
use App\City;
use App\User;
use App\Country;
use App\Usercard;
use Carbon\Carbon;
use App\Userusecoupon;
use App\Uservisitedgym;
use App\CorporateUsers;
use App\Autorenewalfaild;
use App\API\Subscription;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use App\API\Usersubscription;
use App\CorporateSubscription;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class CorporateEmployeeController extends Controller
{

    function __construct()
    {
        $this->country = new Country;

    }

    public function employeelist()
    {
        return view('admin.corporate_users.employeelist');
    }

    public function create()
    {

//        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['city'] = City::select('id', 'name')->where('country', 'Pakistan')->get();
//        $data['plan'] = Plancategory::select('id', 'plan_cat_name')->where('id', '!=', '4')->get();
        $data['subscription'] = Subscription::select('id', 'description')->get();

        return view('admin.corporate_users.add_employee', $data);
    }

    public function getSubscription()
    {
        $v = $_REQUEST['plan'];
        $subscription = Subscription::select('id', 'visit_pass', 'plan_duration', 'plan_time', 'description')->where('plan_name', $v)->get();

        return $subscription;
    }

    public function store(Request $request)
    {
        $r = $request->all();

        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
//            'phone_number' => 'required',
//            'country' => 'required|integer',
//            'password' => 'required|string|min:8',
//            'user_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $already_employeed = User::join('corporate_users as c', 'c.corporate_id', '=', 'users.id')->where('users.is_delete', 1)->where('c.is_delete', 1)->where('c.corporate_id', $r['corporate_id'])->get()->count();
        $CorporateSubscription = CorporateSubscription::where('corporate_id', $r['corporate_id'])->first();
        $no_of_employees = $CorporateSubscription->no_of_employees;
        if ($no_of_employees == 0) {
            Session::flash('msg', 'You cannot add employees.');
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        }
        if ($no_of_employees <= $already_employeed) {
            Session::flash('msg', 'Max no of employees already added.');
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        }
        $password = mt_rand(10000000,99999999);
        $r['plain_password'] = $password;
        $r['password'] = Hash::make($password);
        $message = ['user_name' => $r['first_name'] . ' ' . $r['last_name'], 'email' => $r['email'], 'password' => $password];
        \Mail::to($r['email'])->send(new SendMailable($message, 2));

        $r['user_type'] = 3;
        $r['partner_first_login'] = 1;


        if (isset($r['firebase_token'])) {
            $firebase_token = $r['firebase_token'];
        } else {
            $firebase_token = "";
        }
        $r['firebase_token'] = $firebase_token;

        $r['user_ufp_id'] = mt_rand(100000, 999999);//$input['country'].rand(pow(10, 3-1), pow(10, 3)-1);
        $r['user_token'] = $this->generateRandomString();
        $r['user_token_time'] = new \DateTime();
        $r['user_image'] ="/public/image/user_image/default.png";

        $user = User::Create($r);

        if ($user) {

            $subscription['user_id'] = $user->id;
            $subscription['corporate_id'] = $request->corporate_id;
            $corporateUser = CorporateUsers::create($subscription);

            $subscription_data = Usersubscription::where('user_id',$request->corporate_id)->where('status',1)->first();
            if ($subscription_data)
            {
                $subscribed['user_id'] = $user->id;
                $subscribed['customer_id'] = $user->id;
                $subscribed['subscription_id'] = $subscription_data->subscription_id;
                $subscribed['transaction_id'] = $subscription_data->transaction_id;
                $subscribed['purchased_at'] = $subscription_data->purchased_at;

                $subscribed['plan_name'] = $subscription_data->plan_name;
                $subscribed['visit_pass'] = $subscription_data->visit_pass;
                $subscribed['plan_duration'] = $subscription_data->plan_duration;
                $subscribed['plan_time'] = $subscription_data->plan_time;
                $subscribed['amount'] = $subscription_data->amount;
                $subscribed['description'] = $subscription_data->description;
                $subscribed['expired_at'] = $subscription_data->expired_at;


                $subscribed['payment_method'] = $subscription_data->payment_method;
                $subscribed['currency'] = $subscription_data->currency;
                $subscribed['charge_id'] = $subscription_data->charge_id;
                $subscribed['total_payable_amount'] = $subscription_data->total_payable_amount;

                $corporateSubscriptionAdded = Usersubscription::create($subscribed);
            }
            $msg = "Corporate employee added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }


    function generateRandomString($length = 200)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function corporate_employee_list(Request $r, $id = null, $corporate_name = null)
    {
        $where = ($id) ? $id : ''; // based on tab request 2 for pending user and 0,1 for active user
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = ($_GET['columns'][$columnIndex]['data'] == 'first_name') ? 'first_name' : 'users.created_at';

        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $status = $_GET['status'] == 'All' ? '' : ($_GET['status']=='Active'?'active':'inactive');
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
        $filter_type = ($_GET['filter_type']);
        $search = $_GET['search']['value'];

        $select = array('users.id', 'first_name', 'email', 'phone_number', 'status', 'users.created_at');
        $get_data = $this->select_user_data($id, $corporate_name, $select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName , $status ,$filter_type);

        $totalRecord = $this->selectdata_count($search, $where, $startdate, $enddate , $status , $filter_type);

        $results = ["draw" => intval($_GET['draw']),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $get_data];
        echo json_encode($results);


    }

    public function select_user_data($id, $corporate_name, $select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName , $status,$filter_type)
    {
        $query = User::query($select);
        $query->where('corporate_users.corporate_id', $where);
        $query->where(['users.is_delete' => '1']);
        $query->join('corporate_users', 'corporate_users.user_id', '=', 'users.id');

        if($filter_type == 1){
            if (!empty($startdate) && !empty($enddate)) {
                $query->whereDate('users.created_at', '>=', $startdate);
                $query->whereDate('users.created_at', '<=', $enddate);
            }
        }elseif($filter_type == 3){
            if (!empty($startdate) && !empty($enddate)) {
                $query->whereHas('userCheckIns' , function ($q) use ($startdate , $enddate){
                    $q->whereBetween('user_check_in', [$startdate.' 00:00:00', $enddate.' 23:59:59']);
                });
            }
        }

        if(!empty($status)){
            if($status == 'active'){
                $query->where('users.status' , 1);
            }else{
                $query->where('users.status' , 0);
            }
        }
        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

        $query->with('countryname');
        $query->with('cityname');
        $query->skip($offset);
        $query->take($limit_t);
        $query->orderBy($columnName, $order_by);

        $user_data = $query->get();

        $data = array();
        $i = 0;
        $j = 1;
        foreach ($user_data as $key => $value) {
            if($filter_type == 1 || $filter_type == 2 || $filter_type == 3){
                $total_checkin = DB::table('user_visited_gym')->where('user_id' , $value->user_id)->whereDate('user_check_in' , '>=' , $startdate)->whereDate('user_check_in' ,'<=', $enddate)->count();
            }
            else{
                $total_checkin = DB::table('user_visited_gym')->where('user_id' , $value->user_id)->count();
            }
            $image = \Config::get('values.app_url') . $value->user_image;
            $data[$i]['user_image'] = '<img src="' . $image . '" class="user_image" alt="' . $value->first_name . '">';

            if ($value->status == 1) {
                $status = "<a href='javascript:void(0)' data-href='" . route('corporate_employee_status', ['id' => $value->user_id, 'status' => $value->status]) . "' class='btn btn-success btn-xs change_status'>Active</a>";
            } else {
                $status = "<a href='javascript:void(0)' data-href='" . route('corporate_employee_status', ['id' => $value->user_id, 'status' => $value->status]) . "' class='btn btn-danger btn-xs change_status'>Inactive</a>";
            }

            $login = "<a href='" . route('login_account', ['id' => $value->user_id]) . "' class='btn btn-xs bg-success'>Login</a>";
            $delete = " <button data-url='" . route('delete_corporate_employee', ['id' => $value->user_id, 'cId' => $id, 'cName' => $corporate_name]) . "' class='btn btn-danger btn-xs' id='inactive' onclick='return confirm(\"Are you sure to delete this employee\")'>Delete</button>";
            $edit = "<a href='" . route('edit_corporation_employee', ['id' => $value->user_id]) . "' class='btn btn-xs bg-orange'>Edit</a>";
            $sendEmail = "<a href='" . route('send_email', ['id' => $value->user_id]) . "' class='btn btn-xs bg-black'>Send</a>";

//            $data[$i]['reg_owner'] = $value->first_name . ' ' . $value->last_name;

            $date = date('d/m/Y', strtotime($value->created_at));

            $data[$i]['username'] = $value->first_name . ' ' . $value->last_name;
            $data[$i]['email'] = $value->email;
            $data[$i]['address'] = $value->address;
            $data[$i]['monthly_checkin'] = $total_checkin;
            $data[$i]['plain_password'] = $value->plain_password;
            $data[$i]['country'] = ($value->countryname) ? $value->countryname->nicename : '';
            $data[$i]['city'] = ($value->cityname) ? $value->cityname->name : '';
            $data[$i]['status'] = $status;
            $data[$i]['registered_at'] = date('Y-m-d' , strtotime($value->created_at));
            $data[$i]['login'] = $login;
            $data[$i]['edit'] = $edit;
            $data[$i]['delete'] = $delete;
            $data[$i]['send_email'] = $sendEmail;
            $i++;
            // }
        }
        return $data;
    }

    public function corporate_employee_status($id, $status)
    {
        $update['status'] = ($status == 1) ? 0 : 1;
        $input['status'] = ($status == 1) ? 0 : 1;
        $input['user_token'] = null;
        $input['user_token_time'] = null;
        $updateStatus = User::whereid($id)->update($input);

        if ($updateStatus) {
            CorporateUsers::where('user_id', $id)->update($update);
            return response()->json(['status' => true , 'message' => 'Corporate Employee Status Updated Successfuly!']);
        } else {
            $msg = trans('lang_data.error');
            return response()->json(['status' => false , 'message' => 'Something went Wrong!']);
        }
    }

    public function selectdata_count($search, $where, $startdate, $enddate , $status , $filter_type)
    {

        $query = User::query();
        $query->where('users.is_delete', '=', '1');
        $query->where('corporate_users.corporate_id', $where);
        $query->join('corporate_users', 'corporate_users.user_id', '=', 'users.id');

        if($filter_type == 1){
            if (!empty($startdate) && !empty($enddate)) {
                $query->whereDate('users.created_at', '>=', $startdate);
                $query->whereDate('users.created_at', '<=', $enddate);
            }
        }elseif($filter_type == 3){
            if (!empty($startdate) && !empty($enddate)) {
                $query->whereHas('userCheckIns' , function ($q) use ($startdate , $enddate){
                    $q->whereBetween('user_check_in', [$startdate.' 00:00:00', $enddate.' 23:59:59']);
                });
            }
        }

        if(!empty($status)){
            if($status == 'active'){
                $query->where('users.status' , 1);
            }else{
                $query->where('users.status' , 0);
            }
        }
        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

//        $query->with('cityname');
        $query->with('countryname');
        //$query->with('cityname');
        $query->orderBy('users.created_at', 'DESC');

        $gym_data = $query->get();
        return $gym_data->count();
    }


    public function edit($id)
    {

        $data['user'] = User::where('id', $id)->first();
//        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['city'] = City::select('id', 'name')->where('country', 'Pakistan')->get();

        return view('admin.corporate_users.edit_employee', $data);

    }


    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user = User::findOrfail($id);
        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => ['required', 'string', 'max:255', Rule::unique('users')->ignore($user->id)],
        ]);

        if(!empty($input['plain_password'])){
            $input['password'] = Hash::make($input['plain_password']);
        }

        $user = $user->update($input);
        if ($user) {
            $msg = "Corporate Employee updated successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }

        return redirect()->back();
    }

    public function delete_corporate_employee($employee_id, $corporate_id, $corporate_name)
    {
//        $userCard = Usercard::where('user_id', $employee_id)->delete();
        $corporateUsers = CorporateUsers::where('user_id', $employee_id)->update(['is_delete' => '0']);
//        $userVisitedGym = Uservisitedgym::where('user_id', $employee_id)->delete();
//        $autoRenewalFaild = Autorenewalfaild::where('user_id', $employee_id)->delete();
//        $userUseCoupon = Userusecoupon::where('user_id', $employee_id)->delete();
//        if (DB::table('user_bank_detail')->where('user_id', $employee_id)->get()) {
//            DB::table('user_bank_detail')->where('user_id', $employee_id)->delete();
//        }
//        $userSubscription = Usersubscription::where('user_id', $employee_id)->delete();
        $user = User::where('id', $employee_id)->update(['is_delete' => '0']);
        if ($user) {
            return response()->json(['status' => true , 'message' => 'Customer Check In deleted Successfuly']);
        } else {
            return response()->json(['status' => false , 'message' => 'Something went wrong']);
        }

        return redirect()->route('corporate-employee-list', [$corporate_id, $corporate_name]);
    }

    public function sendCredentials($id)
    {
        $r = User::find($id);
        $message = ['user_name' => $r->first_name . ' ' . $r->last_name, 'email' => $r->email, 'password' => $r->plain_password];

        \Mail::to($r->email)->send(new SendMailable($message, 2));
        $msg = "Email has been sent successfully";
        Session::flash('msg', $msg);
        Session::flash('message', 'alert-success');


        return redirect()->back();

    }
}
