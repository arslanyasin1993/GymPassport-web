<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $blogs = Blog::where(['is_deleted' => 0])->orderby('created_at', 'desc')->get();
        return view('admin.blog.index', compact('blogs'));
    }

    public function create()
    {
        return view('admin.blog.add');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg',
            'author' => 'required|string|max:255',
            'description' => 'required|string',
            'slug' => 'required|unique:blogs,slug|regex:/^[a-zA-Z0-9-]+$/',
        ]);
        if ($request->hasFile('image')) {
            $sDirPath = public_path('storage/blog');
            $path = 'storage/blog';
            if (!$sDirPath) {
                mkdir($sDirPath, 0777, true);
            }
            $blog_image = date('Ymdhis') . '.' . $request->image->getClientOriginalExtension();

            $request->image->move($sDirPath, $blog_image);
            $input['image'] = $blog_image ? $path . '/' . $blog_image : "";
        }

        $blog = Blog::create($input);
        if ($blog) {

            $msg = "Blog added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');

        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }

        return redirect()->back();

    }

    public function edit($id)
    {
        $blog = Blog::find($id);


        return view('admin.blog.edit', compact('blog'));
    }

    public function update(Request $request, $id)
    {
        $blog = Blog::find($id);
        $input = $request->all();
        unset($input['image']);
        $validatedData = $request->validate([
            'author' => 'required|string|max:255',
            'description' => 'required|string',
            'slug' => 'required|regex:/^[a-zA-Z0-9-]+$/|unique:blogs,slug,' . $id
        ]);

        if ($request->hasFile('image')) {
            $validatedData = $request->validate([
                'image' => 'nullable|image|mimes:jpeg,png,jpg',
            ]);
        }

        if ($request->hasFile('image')) {
            $sDirPath = public_path('storage/blog');
            $path = 'storage/blog';
            if (!$sDirPath) {
                mkdir($sDirPath, 0777, true);
            }
            $blog_image = date('Ymdhis') . '.' . $request->image->getClientOriginalExtension();

            $request->image->move($sDirPath, $blog_image);
            $input['image'] = $blog_image ? $path . '/' . $blog_image : "";
        }


        $check = $blog->update($input);
        if ($check) {

            $msg = "Blog Updated successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');

        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        $blog = Blog::find($id);
        $input['is_deleted'] = 1;
        $blog->update($input);
        $check = $blog->save();
        if ($check) {
            $msg = 'Blog deleted successfully';
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();

    }

    public function updateStatus($id, $status)
    {
        $blog = Blog::find($id);
        $blog->is_active = $status;
        $check = $blog->save();
        if ($check) {
            $msg = "Status updated successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }
}

