<?php

namespace App\Http\Controllers;

use App\CorporateSubscription;
use App\Country;
use App\User;
use App\Usergym;
use DB;
use Illuminate\Http\Request;
use Session;

class CorporatePartnerController extends Controller
{
    protected $data = array();
    function __construct()
    {
        $this->country = new Country;
    }

    public function index()
    {
        $this->data['total_gyms'] = DB::table('gymcategories')->join('users_gym', 'users_gym.id', '=', 'gymcategories.user_gym_id')
            ->where(function ($query) {
                $query->where([['gymcategories.status', 1], ['users_gym.status', 1], ['users_gym.is_delete', 1]])->whereIN('gymcategories.plan_category_id', [1, 2, 3, 4]);
            })->groupBy('gymcategories.user_gym_id')->get()->count();
        return view('web.corporate',$this->data);
    }

    public function store(Request $request)
    {


        $r = $request->all();
        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'corporate_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone_number' => 'required',
            'no_of_employees' => 'required',
//            'password' => 'required|string|min:8',
//            'password_confirmation' => 'required|same:password',
            'address' => 'required',
        ]);
//        $r['password'] = Hash::make($r['password']);
        $r['user_type'] = 5;


        if (isset($r['firebase_token'])) {
            $firebase_token = $r['firebase_token'];
        } else {
            $firebase_token = "";
        }
        $r['firebase_token'] = $firebase_token;
//        $r['password'] = Hash::make($r['password']);
        $r['user_ufp_id'] = mt_rand(100000, 999999);//$input['country'].rand(pow(10, 3-1), pow(10, 3)-1);
        $r['user_token'] = $this->generateRandomString();
        $r['user_token_time'] = new \DateTime();
        $r['is_corporate'] = 1;

        $user = User::Create($r);

        if ($user) {

            $input['corporate_id'] = $user->id;
            $input['corporate_name'] = $r['corporate_name'];
            $input['no_of_employees'] = $r['no_of_employees'];

            $userSubscription = CorporateSubscription::Create($input);

            $msg = "Thank you for your submission. A member of our team will reach out to you within 48 hours";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }


    function generateRandomString($length = 200)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}
