<?php

namespace App\Http\Controllers\WebGymOwner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Uservisitedgym;
use App\Usergym;
use App\API\Usersubscription;
use Auth;
use phpDocumentor\Reflection\DocBlock\Tags\Formatter\PassthroughFormatter;
use Session;
use DB;
use Illuminate\Support\Facades\Hash;
//use Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\UserController as ApiUserController;
use function GuzzleHttp\Promise\all;

class PatnerhomeController extends Controller
{
    function __construct() {
       $this->api_user_controller = new ApiUserController;
       $this->gym_data = new Usergym;
    }

    public function index(){
    	$GymPatner = Auth::User();
		$current_time = date('Ymdhis');
		$generateRefreshToken = $this->api_user_controller->generateRandomString();
		$GymPatner->user_token = $generateRefreshToken;
		$GymPatner->user_token_time = new \DateTime();
		$GymPatner->update();
    	$user_id = $GymPatner->id;
    	if($GymPatner->user_type == '4'){
            $gym_image = Usergym::select('gym_logo')->where('users_id', $GymPatner->gym_user_id)->where('status', '1')->first();
        }else {
            $gym_image = Usergym::select('gym_logo')->where('users_id', $user_id)->where('status', '1')->first();
        }
        // echo'<pre>';print_r($gym_image);die;
        if(!empty($gym_image)){
        session()->put('gym_img',$gym_image->gym_logo);
        }
       // echo session()->get('gym_img');die;
        session()->put('user_id',$user_id);
    	session()->put('user_token',$GymPatner->user_token);
        $data['country'] = DB::table('country')->select('id', 'nicename')->where('id',162)
        					->orderBy('nicename', 'ASC')->get();
       //echo '<pre>';print_r($data['country']);die();
        if($GymPatner->user_type == '4'){
            $data['gym_data'] = $this->gym_data->partner_gym_detail($GymPatner->gym_user_id);
        }else{
            $data['gym_data'] = $this->gym_data->partner_gym_detail($user_id);
        }
        // return $data['gym_data'];
        $data['user_type'] = $GymPatner->user_type;
        $data['staff'] = User::where('gym_user_id',Auth::id())->where('user_type',4)->select('id')->first();
       // echo '<pre>';print_r($data['gym_data']);die();
    	return view('web.ufp_patner.home',$data);
    }

    public function update_pass(Request $r){
     // print_r($r->all());die();
        $validatedData = $r->validate([
            'password' => 'required|min:8',
            'confrimed_password' => 'required|min:8|required_with:password|same:password',
        ]);
        $user_id = session()->get('user_id');
        $update['partner_first_login'] = '0';
        $update['password'] = Hash::make(trim($r->confrimed_password));
        $update_password = User::whereid($user_id)->update($update);
        if($update_password){
            $msg = "Password changed successfully";
            Session::flash('message','alert-success');
        }else{
            $msg = "Something went wrong please try again!";
            Session::flash('message','alert-danger');
        }
          Session::flash('msg',$msg);
          return redirect()->back();

    }
    
    public function addstaff(){
        $staff['staff'] = User::where('gym_user_id',Auth::id())
            ->where('user_type',4)
            ->select('first_name','last_name','email','phone_number','password')
            ->first();
//        return isset($staff)?$staff->first_name: 'a';
        return view('web.add_staff.add_staff',$staff);
    }

    public function store_staff(Request $r){

        $validatedData = $r->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8|confirmed',

        ]);

        $r['gym_user_id'] = Auth::id();
        $r['user_type'] = '4';
        $r['password'] = Hash::make(trim($r->password));
        $r['user_ufp_id'] = mt_rand(10000, 99999);

        $insert = User::create($r->all());

        if($insert){
            $msg = "User Created successfully";
            Session::flash('message','alert-success');
        }else{
            $msg = "Something went wrong please try again!";
            Session::flash('message','alert-danger');
        }
        Session::flash('msg',$msg);
        return redirect()->back();
    }

    public function update_staff(Request $r){
        $vali = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255'//|unique:users,email,'.$r->email,
        ];
        if (isset($r->password))
            $vali['password'] = 'required|min:8|confirmed';

        $validatedData = $r->validate($vali);

//        return $r;
//        $r['gym_user_id'] = Auth::id();
//        $r['user_type'] = '4';
//        $r['user_ufp_id'] = mt_rand(10000, 99999);
        if (isset($r->password))
            $r['password'] = Hash::make(trim($r->password));

        $staff = User::where('gym_user_id',Auth::id())
            ->where('user_type',4)->first();
//        ->Update($r->all())
        $staff->email = $r->email;
        $staff->first_name = $r->first_name;
        $staff->last_name = $r->last_name;
        if (isset($r->password))
            $staff->password = $r->password;

        try {

            $staff->save();
            $msg = "User Updated successfully";
            Session::flash('message','alert-success');
        }catch(\Exception $e){
            $msg = "Something went wrong please try again!";//.$e->getMessage();
            Session::flash('message','alert-danger');
        }

//        if($staff){
//
//        }else{
//
//        }
        Session::flash('msg',$msg);
        return redirect()->back();
    }

}
