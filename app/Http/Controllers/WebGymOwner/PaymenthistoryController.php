<?php

namespace App\Http\Controllers\WebGymOwner;

use App\Exports\PaymentExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bankdetail;
use App\Usergym;
use App\Payments;
use App\Uservisitedgym;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use DB;
use Validator;
use App\Country;
use App\State;

class PaymenthistoryController extends Controller {

    function __construct() {

        $this->gym = new Usergym;
        $this->uvg = new Uservisitedgym;
    }

    public function index() {
        $GymUser = Auth::User();
        $patner_id = $GymUser->id;
        $data['country'] = Country::where('id', '13')->get();
        $data['state'] = State::where('country_id', '13')->get();
        $data['bank_detail'] = Bankdetail::where('user_id', $patner_id)->first();
        $data['gym_list'] = Usergym::select('id', 'gym_name')->where('users_id', $patner_id)->where('status', 1)->get();
        //echo '<pre>';        print_r($data['bank_detail']);die;
        $data['patner_id'] = $patner_id;
        return view('web.ufp_patner.payment.payments', $data);
    }

    public function add_bank_detail(Request $r) {
        if($r->account_type == 1){
            $validatedData = $r->validate([
                'jazz_account_name' => 'required',
                'jazz_account_number' => 'required',
                'cnic' => 'required'
            ]);    
            $r['account_name'] = $r->jazz_account_name;
            $r['account_number'] = $r->jazz_account_number;
            $r['bank_name'] = '';
            $r['branch_name'] = '';
        }else{
            $validatedData = $r->validate([
                'bank_name' => 'required',
                'branch_name' => 'required',
                'account_number' => 'required',
                'account_name' => 'required|string|max:255',
            ]);
        }
        unset($r['jazz_account_name']);
        unset($r['jazz_account_number']);
        // $validatedData = $r->validate([
        //     'bank_name' => 'required',
        //     'branch_name' => 'required',
        //     'account_number' => 'required',
        //     'account_name' => 'required|string|max:255',
        // ]);
        $input = $r->all();
        $GymUser = Auth::User();
        $user_id = $GymUser->id;
        //$input['bsb_number']=$r->swift_code;
        unset($input['swift_code']);
        $input['user_id'] = $user_id;
        $insert = Bankdetail::create($input);
        if ($insert) {
            $msg = "Payment detail added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function update_bank_detail(Request $r, $id) {
        if($r->account_type == 1){
            $validatedData = $r->validate([
                'jazz_account_name' => 'required',
                'jazz_account_number' => 'required',
                'cnic' => 'required'
            ]);    
            $r['account_name'] = $r->jazz_account_name;
            $r['account_number'] = $r->jazz_account_number;
            $r['bank_name'] = '';
            $r['branch_name'] = '';
        }else{
            $validatedData = $r->validate([
                'bank_name' => 'required',
                'branch_name' => 'required',
                'account_number' => 'required',
                'account_name' => 'required|string|max:255',
            ]);
        }
        unset($r['jazz_account_name']);
        unset($r['jazz_account_number']);
        $input = $r->all();
        $GymUser = Auth::User();
        $user_id = $GymUser->id;
        $input['user_id'] = $user_id;
        //$input['bsb_number']=$r->swift_code;
        unset($input['swift_code']);
        unset($input['_token']);

      //  dd($input);
        //echo $id;die;
        $insert = Bankdetail::where('user_id',$id)->update($input);
        if ($insert) {
            $msg = "Payment detail update successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function get_payment_month(Request $r) {
        $input = $r->all();
        $gym_id = $input['gym_id'];
        $data['get_month'] = $this->uvg->get_month_and_amount($gym_id);
        $data['month_name'] = array_column($data, 'month');
        $get_month = $this->uvg->get_month_and_amount($gym_id);
        $arr['data'] = [];
        $data['current_month'] = date("m");
        $data['current_year'] = date("y");
        $data['static_month'] = array_column($get_month, 'month'); // getonly month
        // echo '<pre>';print_r($get_month);die();
        foreach ($get_month as $key => $val) {
            $month = $val->month;
            $amount = $this->uvg->get_month_and_gym_amount($gym_id, $month);
            // print_r($amount);
            $arr['data'][$key]['month_name'] = $val->month_name;
            $arr['data'][$key]['year'] = $val->year;
            $arr['data'][$key]['month'] = $val->month;
            if (!empty($amount)) {
                $arr['data'][$key]['total_earn_amount'] = $amount[0]->total ? number_format((float) $amount[0]->total, 2, '.', '') : '00.00';
            } else {
                $arr['data'][$key]['total_earn_amount'] = '00.00';
            }
        }
        $data['month_data'] = $arr['data'];
        // return $data;
        //echo '<pre>';print_r($arr);die();
        // echo '<pre>';print_r($data['get_month']);die();
        $data['gym_id'] = $gym_id;
        return view('web.ufp_patner.ajax_page.month_list', $data);
    }

    public function month_wise_user(Request $r) {
        $response = array();
        $input = $r->all();
        $gym_id = $input['gym_id'];
        $month = $input['month'];
        $data = $this->uvg->get_month_wise_user($gym_id, $month); //visit wise
        $data_second = $this->uvg->get_month_wise_plan_user($gym_id, $month); //month plan wise
        // get two array from diffrect condtion then both array is marge in one array 
        $final_data = array_merge($data, $data_second); //marge
        $new = array();
        foreach ($final_data as $value) {
            $new[serialize($value)] = $value; //removing array of object dupliacte data in array;
        }
        $final_data = array_values($new);
        //$user_data = array_unique($final_data);
        //echo '<pre>';print_r($data);
        //echo '<pre>';print_r($final_data);die;
        $AmountForTransfer_visit = $this->uvg->payment_total_earning_amount($gym_id, $month); // select previous month amount on based on month and partner id
        $AmountForTransfer_month = $this->uvg->payment_total_earning_amount_month($gym_id, $month);
        $TotalAmountForGymPatner = ($AmountForTransfer_visit[0]->total + $AmountForTransfer_month[0]->total);
        //echo '<pre>';print_r($data);die();
        if ($final_data) {
            //$total_amount_array = array_column($data, 'gym_earn_amount');
            //$total_earn_amount = array_sum($total_amount_array);
            $response['status'] = true;
            $response['message'] = 'user daily visit data';
            $response['total_earn_amount'] = number_format((float) $TotalAmountForGymPatner, 2, '.', '');
            $response['result'] = $final_data;
        } else {
            $response['status'] = false;
        }
        return $response;
    }
    public function month_wise_user_manual(Request $r) {
        $response = array();
        $input = $r->all();
        $gym_name = $input['gym_name'];
        $date = $input['date'];
        $GymUser = Auth::User();
        $user_email = $GymUser->email;

        session()->put('gym_name', $gym_name);
        session()->put('date', $date);

        if($gym_name == 'all'){
            $payments = Payments::where('gym_owner_email', $user_email)->select(DB::raw('DATE_FORMAT(date, "%Y/%m/%d") as date'), 'trans_id', 'amount', 'gym_name', 'gym_owner_email', 'email_sent');
            if ($date) {
                $formattedDate = date('Y-m', strtotime($date));
                $payments = $payments->where(DB::raw('DATE_FORMAT(date, "%Y-%m")'), $formattedDate);
            }
            $payments = $payments->orderBy('date', 'desc')->get();
        }else{
            $payments = Payments::where('gym_owner_email', $user_email)->where('gym_name',$gym_name)->select(DB::raw('DATE_FORMAT(date, "%Y/%m/%d") as date'), 'trans_id', 'amount', 'gym_name', 'gym_owner_email', 'email_sent');
            if ($date) {
                $formattedDate = date('Y-m', strtotime($date));
                $payments = $payments->where(DB::raw('DATE_FORMAT(date, "%Y-%m")'), $formattedDate);
            }
            $payments = $payments->orderBy('date', 'desc')->get();
        }

        if($payments) {
            $total_amount_array = array_column(json_decode($payments), 'amount');
            $total_earn_amount = array_sum($total_amount_array);
            $response['status'] = true;
            $response['message'] = 'user daily visit data';
            $response['total_earn_amount'] = number_format((float) $total_earn_amount, 2, '.', '');
            $response['result'] = $payments;
        } else {
            $response['status'] = false;
        }
        return $response;
    }

    public function paymentDownload(Request $r){
        $gym_name = session()->get('gym_name');
        $date = session()->get('date');
        $gymUser = Auth::user();

        if($gym_name == 'all'){
            $payments = Payments::where('gym_owner_email', $gymUser->email)->select(DB::raw('DATE_FORMAT(date, "%Y/%m/%d") as date'), 'trans_id', 'amount', 'gym_name', 'gym_owner_email', 'email_sent');
            if ($date) {
                $formattedDate = date('Y-m', strtotime($date));
                $payments = $payments->where(DB::raw('DATE_FORMAT(date, "%Y-%m")'), $formattedDate);
            }
            $payments = $payments->orderBy('date', 'desc')->get();
        }else{
            $payments = Payments::where('gym_owner_email', $gymUser->email)->where('gym_name',$gym_name)->select(DB::raw('DATE_FORMAT(date, "%Y/%m/%d") as date'), 'trans_id', 'amount', 'gym_name', 'gym_owner_email', 'email_sent');
            if ($date) {
                $formattedDate = date('Y-m', strtotime($date));
                $payments = $payments->where(DB::raw('DATE_FORMAT(date, "%Y-%m")'), $formattedDate);
            }
            $payments = $payments->orderBy('date', 'desc')->get();
        }

        $data = array();
        foreach ($payments as $key => $payment) {
            $data[$key]['sr_no'] = $key + 1;
            $data[$key]['gym_name'] = $payment->gym_name;
            $data[$key]['trans_id'] = $payment->trans_id;
            $data[$key]['date'] = $payment->date;
            $data[$key]['earning'] = $payment->amount;

        }
        return Excel::download(new PaymentExport($data), 'payment_history.xlsx');
    }

}
