<?php

namespace App\Http\Controllers\WebGymOwner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;
use App\User;
use App\Usergym;
use App\Gymimage;
use App\Uservisitedgym;
use App\Gym_facilities;
use App\Facilities;
use App\Gymcategory;
use App\API\Plancategory;
use Session;
use DB;
use Illuminate\Support\Facades\Validator;

class EditgymController extends Controller
{
      function __construct() {
        $this->country = new Country;
        $this->gym_mod = new Usergym;
        $this->user_visit_gym = new Uservisitedgym;
    }
    
    public function index($gym_id){
        $gym_id = decrypt($gym_id);
        $day = array();
        $select = array('id', 'users_id', 'gym_name', 'gym_logo', 'phone_number', 'gym_activities', 'about_gym', 'gym_address',
            'gym_latitude', 'gym_longitude', 'country', 'accept_terms_condition', 'status', 'created_at','is_all_day_open','gym_pin_code' , 'is_staff_hours' ,'staff_hours');
        $data['gym_data'] = $this->gym_mod->gym_details($gym_id, $select);
        $data['country'] = $this->country->select('id', 'nicename')->where('id', 13)->get();
        $data['facities'] = Facilities::orderBy('facilities','ASC')->get();
        foreach ($data['gym_data']->gym_time as $key => $value) {
            $day[$key] = $value->gym_open_days;
        }
        $facility = Gym_facilities::select('facilities_id')->where('gym_id', $gym_id)->get()->toArray();
        $facility_name = array_column($facility, 'facilities_id');
        $data['facility_id'] = $facility_name;
        $data['days'] = $day;
       // dd($data);die;
        //echo '<pre>';print_r((array)$data);die;
        return view('web.ufp_patner.edit_gym', $data);
        //echo decrypt($gym_id);
    }
    
    public function update(Request $r,$gym_id){
        $validatedData = $r->validate([
            'gym_name' => 'required|string|max:255',
            //'gym_address' => 'required|string',
            //'gym_latitude' => 'required|string',
            //'gym_longitude' => 'required|string',
            'phone_number' => 'required',     
            //'gym_pin_code' => 'required',
            'facilities_id' => 'required',
        ]);
        $select = array('id', 'users_id', 'gym_name', 'gym_logo', 'phone_number', 'gym_activities', 'about_gym', 'gym_address',
            'gym_latitude', 'gym_longitude', 'country', 'accept_terms_condition', 'status', 'created_at','is_all_day_open','gym_pin_code', 'gym_price_per_visit');
        $gymdata = $this->gym_mod->gym_details($gym_id,$select);
        $r->gym_price_per_visit = $gymdata->gym_price_per_visit;
        $gymdata->phone_number = $r->phone_number;
        $gymdata->facilities_id = $r->facilities_id;
        // return $gymdata;
        $this->gym_mod->user_gym_update($r, $gym_id);
        return redirect()->back();
    }

    public function delete_image($id) {
        $gym_image_path = Gymimage::select('gym_image')->whereid($id)->first();
        $path = 'https://'.$_SERVER['HTTP_HOST'].'/'.$gym_image_path->gym_image;
        //unlink($path);
        $delete = Gymimage::whereid($id)->delete();
        
        echo ($delete == true) ? 1 : 0;
    }
}
