<?php

namespace App\Http\Controllers\WebGymOwner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bankdetail;
use App\Usergym;
use App\Uservisitedgym;
use App\Payoutdate;
use Auth;
use Session;
use DB;
use Validator;
use App\API\Usersubscription;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class CheckinController extends Controller {

    function __construct() {

        $this->gym = new Usergym;
        $this->uvg = new Uservisitedgym;
        $this->excel = new UsersExport;
        $this->user_sub = new Usersubscription;
    }

    public function index() {
        $authuser = Auth::User();
        $patner_id = $authuser->id;
        if($authuser->user_type == '4'){
            $data['gym_list'] = Usergym::select('id', 'gym_name')->where('users_id', $authuser->gym_user_id)->where('status', 1)->get();

        }else{
            $data['gym_list'] = Usergym::select('id', 'gym_name')->where('users_id', $patner_id)->where('status', 1)->get();

        }
        //echo '<pre>';print_r($data['gym_list']);die();
        return view('web.ufp_patner.check_in.check_in', $data);
    }

    public function check_in_histroy(Request $request) {
        if (isset($request['gym_id'])) {
            $gym_id = trim($request['gym_id']);
            $gym_type = trim($request['gym_type']);
            $start_date = $request['start_date'];
            $end_date = $request['end_date'];

            session()->put('gym_id', $gym_id);
            session()->put('gym_type', $gym_type);
            session()->put('end_date', $end_date);
            session()->put('start_date', $start_date);
        } else {
            $gym_id = session()->get('gym_id');
            $gym_type = session()->get('gym_type');
            $end_date = session()->get('end_date');
            $start_date = session()->get('start_date');
        }
        $authuser = Auth::User();
        $patner_id = $authuser->id;
        if (isset($request['check_in_user'])) {
            $num = $request['check_in_user'];
        } else {
            $num = '';
        }
        if (!empty($num) && $num != null) {
            $data['page'] = $num;
        } else {
            $data['page'] = (request()->segment(2)) ? request()->segment(2) : 1;
        }
        $data['limit'] = 10;
        $limit = 10;
        $offset = ($data['page'] - 1) * $data['limit'];

        $check_in_detail= $this->uvg->total_visit_user_wise_check_page($gym_id , $gym_type , $start_date , $end_date , $offset , $limit);
        $arr['array_data'] = [];

        foreach($check_in_detail as $key=>$value){
            $user_id = $value->user_id;
            $subscription_id = $value->subscription_id;
            $arr['array_data'][$key]['gym_id'] = $gym_id;
            $arr['array_data'][$key]['user_id'] = $user_id;
            $arr['array_data'][$key]['image'] = $value->user_image;
            $arr['array_data'][$key]['name'] = $value->first_name.' '.$value->last_name;
            $arr['array_data'][$key]['user_ufp_id'] = $value->user_ufp_id;
           // $last_visit = $this->uvg->last_visit_user_wise($gym_id,$user_id); // last visit fetch
            $user_all_visit = $this->uvg->user_all_visit_list($gym_id , $gym_type , $start_date , $end_date , $user_id , $subscription_id);
            // $plan_name = $this->user_sub->plan_cat_name_fetch($user_id); //user plan cat name fetch
            // $last_plan_name = $this->user_sub->last_plan_cat_name_fetch($user_id); //user plan cat name fetch
            $arr['array_data'][$key]['check_in_time'] = date('h:i A', strtotime($value->user_check_in));
            $arr['array_data'][$key]['check_in_date'] = date('d-m-Y', strtotime($value->user_check_in));
            $arr['array_data'][$key]['total_visit'] = $value->this_gym_total_visit;
            $arr['array_data'][$key]['other_gym_total_visit'] = $value->other_gym_total_visit;
            $arr['array_data'][$key]['plan_status'] = ($value->status=='1') ? 'Active':'Expired';
            $arr['array_data'][$key]['current_visit'] = ($value->visit_pass == 'month') ? " &#8734;" : $value->visit_pass;//$plan_name->plan_category->plan_cat_name.
            $arr['array_data'][$key]['current_visit_pass'] = ($value->visit_pass == 'month') ? " &#8734; Pass" : $value->visit_pass.' visit';//$plan_name->plan_category->plan_cat_name.
            $arr['array_data'][$key]['current_paln_cat_name'] = $value->plan_cat_name;
            $arr['array_data'][$key]['user_all_visit'] = $user_all_visit;
        } 
      //echo '<pre>';print_r($user_all_visit);die; 
        $data['check_in_detail'] = $arr['array_data'];
        $total_rows = $this->uvg->total_visit_user_wise_check_page_count($gym_id , $gym_type , $start_date , $end_date);
        if(!empty($total_rows)){
            $total_rows = count($total_rows);
        }else{
            $total_rows = 0;
        }
        $data['total_rows'] = $total_rows;
        return view('web.ufp_patner.ajax_page.check_in_details', $data);
    }

    public function exportFile(Request $r) {
        $gym_type = session()->get('gym_type');
        $gym_id = session()->get('gym_id');
        $start_date = session()->get('start_date');
        $end_date = session()->get('end_date');
        $get = $this->uvg->get_excel_check_in($gym_id , $gym_type , $start_date , $end_date);
        $i = 1;
        $data = array();
        foreach ($get as $key => $value) {
            $data[$key]['se_no'] = $i;
            $data[$key]['name'] = $value->user_name->first_name . ' ' . $value->user_name->last_name;
            $plan_name = $this->uvg->get_plan_name_for_excel($value->usersubscription_id);
            $data[$key]['check_date'] = date('d-m-Y', strtotime($value->user_check_in));
            $data[$key]['check_in'] = date('h:i A', strtotime($value->user_check_in));
            
             //echo '<pre>';print_r($plan_name);die;
            if(!empty($plan_name)){
                 $data[$key]['plan_name'] = $plan_name[0]->plan_cat_name;
                // $data[$key]['plan_cat_id'] = $plan_name[0]->plan_cat_id;
            }else{
                $data[$key]['plan_name'] = ''; 
            }
            $data[$key]['check_in_amount'] = $value->gym_earn_amount;
            $i++;
        }
       // echo '<pre>';print_r($data);die;
        return Excel::download(new UsersExport($data), 'user_check_in_history.xlsx'); //xlsx //csv
    }

    public function gym_earn_amount(Request $r) {
        $response = [];
        $input = $r->all();

        $gym_id = $input['gym_id'];
        $gym_type = $input['gym_type'];
        $end_date = $input['end_date'];
        $start_date = $input['start_date'];

        $get_payout_date = Payoutdate::select('payout_date')->first(); //select date from payout table where store payout date
        $payout_date = "";//$get_payout_date->payout_date;
        $total_amount_visit = $this->uvg->total_gym_earn_amount($gym_id , $gym_type , $start_date , $end_date , $payout_date);
//        $total_amount_month = $this->uvg->total_gym_earn_amount_month($start_date,$end_date, $gym_id, $payout_date);
        $final_amount = 0;
//        echo '<pre>';print_r($total_amount_visit);
//        echo '<pre>';print_r($total_amount_month);die;
        $daily_amount = '0';
        $month_amount = '0';
        // if(isset($total_amount_visit[0]->total_amount) && isset($total_amount_month[0]->total_amount)){ //if not null both value 
        //    $final_amount = ($total_amount_month[0]->total_amount + $total_amount_visit[0]->total_amount);
        //     $daily_amount = $total_amount_visit[0]->total_amount;
        //     $month_amount = $total_amount_month[0]->total_amount;
        // }else 
        if(isset($total_amount_visit['data'][0]->total_amount)){  //if not null visit pass value
            $final_amount = $total_amount_visit['data'][0]->total_amount;
            // $daily_amount = $total_amount_visit[0]->total_amount;
         }//else if(isset($total_amount_month[0]->total_amount)){ // if not null monthly pass value
        //     $final_amount = $total_amount_month[0]->total_amount;
        //      $month_amount = $total_amount_month[0]->total_amount;
        // }
      // echo'<pre>';print_r($final_amount);die;
//        if($gym_type==1){//daily
//            $startdate = date('d-m-Y');
//            $end_date = date('d-m-Y');
//        }elseif ($gym_type==2) {//weekly
//            $startdate = date('d-m-Y',strtotime('-1 week'));
//            $end_date = date('d-m-Y');
//        }elseif($gym_type==3){//monthly
//            $startdate = date('d-m-Y',strtotime("- 1 month"));
//            $end_date = date('d-m-Y');
//        }elseif($gym_type==5){//3 month
//            $startdate = date('d-m-Y',strtotime("- 3 month"));
//            $end_date = date('d-m-Y');
//        }elseif($gym_type==6){//6 month
//            $startdate = date('d-m-Y',strtotime("- 6 month"));
//            $end_date = date('d-m-Y');
//        }elseif($gym_type==7){//9 month
//            $startdate = date('d-m-Y',strtotime("- 9 month"));
//            $end_date = date('d-m-Y');
//        }else {//year
//            $startdate = date('d-m-Y',strtotime("- 1 year"));
//            $end_date = date('d-m-Y');
//        }
        if ($final_amount) {
            $response['status'] = true;
            $response['message'] = 'Total Earn Amount';
            $response['amount'] = round($final_amount,2);//$total_amount;
            $response['daily_amount'] = "";//round($daily_amount,2);//$total_amount;
            $response['monthly_amount'] = "";//round($month_amount,2);//$total_amount;
            $response['date'] = $total_amount_visit['startdate'].' - '.$total_amount_visit['enddate'];
        } else {
            $response['status'] = false;
            $response['message'] = 'Nothing Fetch Amount';
            $response['amount'] = '';
            $response['daily_amount'] = '';
            $response['monthly_amount'] = '';
            $response['date'] = $total_amount_visit['startdate'].' - '.$total_amount_visit['enddate'];
        }
        return $response;
    }
    
    //28-02-2020
    public function select_box_user(Request $r){
        $input = $r->all();
        $gym_id = $input['gym_id'];
        $select = 'SELECT DISTINCT u.id,u.first_name,u.last_name FROM users u LEFT JOIN user_visited_gym uvg ON u.id=uvg.user_id WHERE uvg.gym_id='.$gym_id.'';
        $get_data = DB::select($select);
        echo json_encode($get_data);
    }
    
    //13-05-2020
    public function payment_period(){
        $data = Payoutdate::select('period_from','period_to','payout_date')->first();
        if(!empty($data)){
            $arr['status'] = true;
            $arr['payout_date'] = date('d-m-Y', strtotime($data->payout_date));
            $arr['start_date'] = date('d/m/Y', strtotime($data->period_from));
            $arr['to_date'] = date('d/m/Y', strtotime($data->period_to));
        }else{
            $arr['status'] = false;
        }
        
        echo json_encode($arr);
    }

}
