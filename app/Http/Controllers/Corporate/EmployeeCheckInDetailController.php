<?php

namespace App\Http\Controllers\Corporate;

use Illuminate\Support\Facades\Auth;
use Session;
use App\User;
use App\Usergym;
use App\CorporateUsers;
use App\Uservisitedgym;
use App\API\Usersubscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EmployeeCheckInDetailController extends Controller
{
    public function index()
    {
        $corporate_users = CorporateUsers::where('corporate_id' , Auth::id())->pluck('user_id')->toArray();
        $top_performers = Uservisitedgym::with('user_name')
            ->whereIn('user_id' , $corporate_users)
            ->whereYear('user_check_in' , date('Y'))
            ->whereMonth('user_check_in' , date('m'))
            ->groupBy('user_id')
            ->selectRaw('user_id, COUNT(*) as check_in_count') // Select user_id and the check-in count
            ->orderByRaw('check_in_count DESC')
            ->first();
        return view('corporate.employee_check_in.users_check_in' , get_defined_vars());
    }

    public function checkInDetailList(Request $req)
    {
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = $_GET['columns'][$columnIndex]['data'];

        if ($columnName == 'username') {
            $columnName = ($columnName == 'username') ? 'u.first_name' : $columnName;
        } elseif ($columnName == 'user_id') {
            $columnName = ($columnName == 'user_id') ? 'u.user_ufp_id' : $columnName;
        } elseif ($columnName == 'email') {
            $columnName = ($columnName == 'email') ? 'u.email' : $columnName;
        } else {
            $columnName = ($columnName == 'gym_name') ? 'g.gym_name' : 'a.user_check_in';
        }

        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';

        $draw = $_GET['draw'];
        $search = $_GET['search']['value'];

        $details = $this->check_in_details('', $startdate, $enddate, $search, $offset, $limit_t, $columnName, $order_by);
        $array = [];
        foreach ($details as $detail) {
            $arr = [];
             $array[] = array(
                "username" => $detail->first_name . " " . $detail->last_name,
                "user_id" => $detail->user_ufp_id,
                "email" => $detail->email,
                "phone_number" => $detail->phone_number,
                "check_in_date" => date_create_from_format('Y-m-d H:i:s', $detail->user_check_in)->format('d-m-Y'),
                "check_in_time" => date('H:i:s', strtotime($detail->user_check_in)),
                "gym_name" => $detail->gym_name,
                "amount" => $detail->gym_earn_amount,
                'plan_name' => $detail->plan_cat_name,
                'gym_plan' => $detail->gym_plan_name,
             );
        }

        $totalRecord = Uservisitedgym::select('user_visited_gym.id');
        if ($search) {
            $totalRecord = $totalRecord->where(function ($q) use ($search) {
                $q->where('u.first_name', 'like', '%' . $search . '%')
                    ->orWhere('u.last_name', 'like', '%' . $search . '%')
                    ->orWhere('u.email', 'like', '%' . $search . '%')
                    ->orWhere('g.gym_name', 'like', '%' . $search . '%');
            });
        }
        if ($startdate && $enddate) {
            $totalRecord = $totalRecord->whereBetween('user_visited_gym.user_check_in', [$startdate . ' 00:00:00', $enddate . ' 23:59:59']);
        }
        $user_id_arr = CorporateUsers::select('user_id')->where('corporate_id', auth()->user()->id)->get();
        $totalRecord = $totalRecord->whereIn('u.id', $user_id_arr)
        ->leftjoin('users as u', 'u.id', '=', 'user_visited_gym.user_id')
        ->leftjoin('users_gym as g', 'g.id', '=', 'user_visited_gym.gym_id')
        ->leftjoin('usersubscriptions as us', 'us.id', '=', 'user_visited_gym.usersubscription_id')
        ->leftjoin('subscriptions as s', 's.id', '=', 'us.subscription_id')
        ->leftjoin('plan_category as p', 'p.id', '=', 's.plan_name')->count();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $array
        );
        echo json_encode($response);
        exit;
    }

    public function check_in_details($user_id = null, $startdate = null, $enddate = null, $search = null, $offset = null, $limit = null, $columnName = null, $orderBy = null)
    {
        if ($user_id) {
            $data = self::with('gym_name')->with('plan_detail.plan_category')->where('user_id', $user_id)->distinct()->orderBy('id', 'desc')->get();//->toArray();
        } else {
            $data = DB::table('user_visited_gym as a')->select('a.*', 'u.first_name', 'u.last_name', 'u.user_ufp_id', 'u.email', 'u.phone_number', 'g.gym_name', 'p.plan_cat_name');
            if ($startdate && $enddate) {
                $data = $data->whereBetween('a.user_check_in', [$startdate . ' 00:00:00', $enddate . ' 23:59:59']);
            }
            $user_id_arr = CorporateUsers::select('user_id')->where('corporate_id', auth()->user()->id)->get();

            $data = $data->whereIn('a.user_id', $user_id_arr);
            if ($search) {
                $data = $data->where(function ($q) use ($search) {
                    $q->where('u.first_name', 'like', '%' . $search . '%')
                        ->orWhere('u.last_name', 'like', '%' . $search . '%')
                        ->orWhere('u.email', 'like', '%' . $search . '%')
                        ->orWhere('g.gym_name', 'like', '%' . $search . '%');
                });
            }
            $data = $data->leftjoin('users as u', 'u.id', '=', 'a.user_id')
            ->leftjoin('users_gym as g', 'g.id', '=', 'a.gym_id')
            ->leftjoin('usersubscriptions as us', 'us.id', '=', 'a.usersubscription_id')
            ->leftjoin('subscriptions as s', 's.id', '=', 'us.subscription_id')
            ->leftjoin('plan_category as p', 'p.id', '=', 's.plan_name')
            ->orderBy($columnName, $orderBy)
            ->offset($offset)
            ->limit($limit)
            ->get();

            foreach ($data as $record){
                $gym_plan_name = DB::table('gymcategories')->join('plan_category' , 'plan_category.id' , '=' , 'gymcategories.plan_category_id')->where('user_gym_id' , $record->gym_id)->where('gymcategories.plan_category_id' , '!=' , 4)->first(['plan_category.plan_cat_name']);
                $record->gym_plan_name = $gym_plan_name ? $gym_plan_name->plan_cat_name : '';
            }
        }
        return $data;
    }


    public function getUserSubscription($id)
    {

        $data['userSubscription'] = Usersubscription::select('p.plan_cat_name', 'p.id')->where('usersubscriptions.user_id', $id)->join('subscriptions as s', 's.id', '=', 'usersubscriptions.subscription_id')
            ->join('plan_category as p', 'p.id', '=', 's.plan_name')->orderBy('usersubscriptions.id', 'desc')->first();

        $data['userGyms'] = '';

        if ($data['userSubscription']) {
            $plan_id = $data['userSubscription']->id;
            $data['userGyms'] = DB::select(DB::raw("SELECT users_gym.id as gym_id,users_gym.gym_name,is_all_day_open,gym_logo,gym_address,users_gym.status FROM users_gym 
                INNER JOIN gymcategories AS gc ON users_gym.id = gc.user_gym_id AND gc.plan_category_id  = $plan_id
                WHERE users_gym.status = 1"));
        }

        return $data;
    }


}
