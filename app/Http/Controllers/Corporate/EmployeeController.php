<?php

namespace App\Http\Controllers\Corporate;

use App\API\Subscription;
use App\API\Usersubscription;
use App\Autorenewalfaild;
use App\City;
use App\CorporateSubscription;
use App\CorporateUsers;
use App\Http\Controllers\Controller;
use App\Mail\SendMailable;
use App\User;
use App\Usercard;
use App\Userusecoupon;
use App\Uservisitedgym;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Session;

class EmployeeController extends Controller
{

    function __construct()
    {
//        $this->country = new Country;
        $this->user_visit_gym = new Uservisitedgym;

    }

    public function employeelist()
    {
        return view('corporate.corporate_users.employeelist');
    }

    public function create()
    {

//        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['city'] = City::select('id', 'name')->where('country', 'Pakistan')->get();
//        $data['plan'] = Plancategory::select('id', 'plan_cat_name')->where('id', '!=', '4')->get();
//        $data['subscription'] = Subscription::select('id', 'description')->get();

        return view('corporate.corporate_users.add_employee', $data);
    }

    public function getSubscription()
    {
        $v = $_REQUEST['plan'];
        $subscription = Subscription::select('id', 'visit_pass', 'plan_duration', 'plan_time', 'description')->where('plan_name', $v)->get();

        return $subscription;
    }

    public function store(Request $request)
    {


        $r = $request->all();
        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
//            'phone_number' => 'required',
//            'country' => 'required|integer',
//            'password' => 'required|string|min:8',
//            'user_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $already_employeed = User::join('corporate_users as c', 'c.corporate_id', '=', 'users.id')->where('users.is_delete', 1)->where('c.is_delete', 1)->where('c.corporate_id', $r['corporate_id'])->get()->count();
        $CorporateSubscription = CorporateSubscription::where('corporate_id', $r['corporate_id'])->first();
        $no_of_employees = $CorporateSubscription->no_of_employees;
        if ($no_of_employees == 0) {
            Session::flash('msg', 'You cannot add employees.');
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        }
        if ($no_of_employees <= $already_employeed) {
            Session::flash('msg', 'Max no of employees already added.');
            Session::flash('message', 'alert-danger');
            return redirect()->back();
        }

        $password = mt_rand(10000000,99999999);
        $r['password'] = Hash::make($password);
        $r['plain_password'] = $password;
        $r['user_type'] = 3;
        $r['partner_first_login'] = 1;

//        $imageName = date('Ymdhis') . '.' . request()->user_image->getClientOriginalExtension();
//        request()->user_image->move(public_path('image/user_image'), $imageName);
        if (isset($r['firebase_token'])) {
            $firebase_token = $r['firebase_token'];
        } else {
            $firebase_token = "";
        }
        $r['firebase_token'] = $firebase_token;
        $r['user_ufp_id'] = mt_rand(100000, 999999);//$input['country'].rand(pow(10, 3-1), pow(10, 3)-1);
        $r['user_token'] = $this->generateRandomString();
        $r['user_token_time'] = new \DateTime();

        $r['user_image'] ="/public/image/user_image/default.png";

        $user = User::Create($r);

        if ($user) {
            $message = ['user_name' => $r['first_name'] . ' ' . $r['last_name'], 'email' => $r['email'], 'password' => $password];
            \Mail::to($r['email'])->send(new SendMailable($message, 2));

            $subscription['user_id'] = $user->id;
            $subscription['corporate_id'] = auth()->user()->id;
            $corporateUser = CorporateUsers::create($subscription);

            $subscription_id = CorporateSubscription::select('subscription_id')->where('corporate_id', auth()->user()->id)->orderBy('id', 'desc')->first();

            $subscription_data = Usersubscription::where('user_id',auth()->user()->id)->where('status',1)->first();
            if ($subscription_data) {
                $subscribed['user_id'] = $user->id;
                $subscribed['customer_id'] = $user->id;
                $subscribed['subscription_id'] = $subscription_data->subscription_id;
                $subscribed['transaction_id'] = $subscription_data->transaction_id;
                $subscribed['purchased_at'] = $subscription_data->purchased_at;

                $subscribed['plan_name'] = $subscription_data->plan_name;
                $subscribed['visit_pass'] = $subscription_data->visit_pass;
                $subscribed['plan_duration'] = $subscription_data->plan_duration;
                $subscribed['plan_time'] = $subscription_data->plan_time;
                $subscribed['amount'] = $subscription_data->amount;
                $subscribed['description'] = $subscription_data->description;
                $subscribed['expired_at'] = $subscription_data->expired_at;


                $subscribed['payment_method'] = $subscription_data->payment_method;
                $subscribed['currency'] = $subscription_data->currency;
                $subscribed['charge_id'] = $subscription_data->charge_id;
                $subscribed['total_payable_amount'] = $subscription_data->total_payable_amount;
                $corporateSubscriptionAdded = Usersubscription::create($subscribed);
            }
            $msg = "Employee added successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }


    function generateRandomString($length = 200)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function employee_list(Request $r, $corporate_name = null)
    {
        $id = auth()->user()->id;

        $where = ($id) ? $id : ''; // based on tab request 2 for pending user and 0,1 for active user
        $order_by = $_GET['order'][0]['dir'];
        $columnIndex = $_GET['order'][0]['column'];
        $columnName = ($_GET['columns'][$columnIndex]['data'] == 'first_name') ? 'first_name' : 'users.created_at';

        $offset = $_GET['start'] ? $_GET['start'] : "0";
        $limit_t = ($_GET['length'] != '-1') ? $_GET['length'] : "";
        $status = $_GET['searchdata'] == 'All' ? '' : ($_GET['searchdata']=='Active'?'active':'inactive');
        $startdate = ($_GET['startdate']) ? date('Y-m-d', strtotime($_GET['startdate'])) : '';
        $enddate = ($_GET['enddate']) ? date('Y-m-d', strtotime($_GET['enddate'])) : '';
        $search = $_GET['search']['value'];
        $filter = $_GET['filter_type'];

        $select = array('users.id','users.user_image', 'first_name', 'email', 'phone_number', 'users.status as uStatus', 'users.created_at');
        $get_data = $this->select_user_data($id, $corporate_name, $select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName , $status , $filter);

        $totalRecord = $this->selectdata_count($search, $where, $startdate, $enddate , $status, $filter);

        $results = ["draw" => intval($_GET['draw']),
            "iTotalRecords" => $totalRecord,
            "iTotalDisplayRecords" => $totalRecord,
            "aaData" => $get_data];
        echo json_encode($results);


    }

    public function select_user_data($id, $corporate_name, $select, $search, $offset, $limit_t, $where, $startdate, $enddate, $order_by, $columnName, $status , $filter)
    {
        $query = User::query($select);
        $query->where('corporate_users.corporate_id', $where);
        $query->where(['users.is_delete' => '1']);
        $query->join('corporate_users', 'corporate_users.user_id', '=', 'users.id');

        if(!empty($status)){
            if($status == 'active'){
                $query->where('users.status' , 1);
            }else{
                $query->where('users.status' , 0);
            }
        }

        if($filter == 1){
            if (!empty($startdate) && !empty($enddate)) {
                $query->whereDate('users.created_at', '>=', $startdate);
                $query->whereDate('users.created_at', '<=', $enddate);
            }
        }elseif($filter == 3){
            if (!empty($startdate) && !empty($enddate)) {
                $query->whereHas('userCheckIns' , function ($q) use ($startdate , $enddate){
                    $q->whereBetween('user_check_in', [$startdate.' 00:00:00', $enddate.' 23:59:59']);
                });
            }
        }

        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

        $query->with('countryname');
        $query->with('cityname');
        $query->skip($offset);
        $query->take($limit_t);
        $query->orderBy($columnName, $order_by);

        $user_data = $query->get();

        $data = array();
        $i = 0;
        $j = 1;

        foreach ($user_data as $key => $value) {
            if ($value->status == 1) {
                $status = "<p class='text text-success' style='font-weight: 900'>Active</p>";
//                $status = "Active";
            } else {
                $status = "<p class='text text-danger' style='font-weight: 900'>Inactive</p>";
//                $status = "Inactive";
            }
//            $user_visit_gym = $this->user_visit_gym->check_in_details($value->user_id , $startdate , $enddate)->count();
            if($filter == 1 || $filter == 2 || $filter == 3){
                $total_checkin = DB::table('user_visited_gym')->where('user_id' , $value->user_id)->whereDate('user_check_in' , '>=' , $startdate)->whereDate('user_check_in' ,'<=', $enddate)->count();
                $user_view = "<a href='" . route('employee_detail', ['id' => $value->user_id,'name'=>$value->first_name . ' ' . $value->last_name]) . "' class='btn btn-info btn-xs bg-olive'>$total_checkin</a>";
            }else{
                $total_checkin = DB::table('user_visited_gym')->where('user_id' , $value->user_id)->count();
                $user_view = "<a href='" . route('employee_detail', ['id' => $value->user_id,'name'=>$value->first_name . ' ' . $value->last_name]) . "' class='btn btn-info btn-xs bg-olive'>$total_checkin</a>";
            }
//            if ($total_checkin > 0) {
//                $user_view = "<a href='" . route('employee_detail', ['id' => $value->user_id,'name'=>$value->first_name . ' ' . $value->last_name]) . "' class='btn btn-info btn-xs bg-olive'>$total_checkin</a>";
//            } else {
//                $user_view = "<a href='javascript:void(0)' class='btn btn-info btn-xs bg-olive'>0</a>";
//            }
//            $delete = " <a href='" . route('delete_employee', ['id' => $value->user_id, 'cId' => $id, 'cName' => $corporate_name]) . "' class='btn btn-danger btn-xs' id='inactive' onclick='return confirm(\"Are you sure to delete this employee\")'>Delete</a>";
            $edit = "<a href='" . route('edit_employee', ['id' => $value->user_id]) . "' class='btn btn-xs bg-orange'>Edit</a>";
            $sendEmail = "<a href='" . route('send_email_employee', ['id' => $value->user_id]) . "' class='btn btn-xs bg-black'>Send</a>";

//            $data[$i]['reg_owner'] = $value->first_name . ' ' . $value->last_name;

            $date = date('d/m/Y', strtotime($value->created_at));

            $image =  $value->user_image ? \Config::get('values.app_url') . $value->user_image : asset('image/user_image/default.png');
            $data[$i]['user_image'] = '<img src="' . $image . '" class="user_image" alt="' . $value->first_name . '">';
            $data[$i]['username'] = $value->first_name . ' ' . $value->last_name;
            $data[$i]['email'] = $value->email;
            $data[$i]['phone_number'] = $value->phone_number;
            $data[$i]['address'] = $value->address;
            $data[$i]['plain_password'] = $value->plain_password;
            $data[$i]['country'] = ($value->countryname) ? $value->countryname->nicename : '';
            $data[$i]['city'] = ($value->cityname) ? $value->cityname->name : '';
            $data[$i]['status'] = $status;
            $data[$i]['view_info'] = $user_view;
            $data[$i]['edit'] = $edit;
//            $data[$i]['delete'] = $delete;
            $data[$i]['send_email'] = $sendEmail;

            $i++;
            // }
        }
        return $data;
    }

    public function employee_status($id, $status)
    {
        //echo $id.' staus '.$status;die;
        $update['status'] = ($status == 1) ? 0 : 1;
        $input['status'] = ($status == 1) ? 0 : 1;
        $input['user_token'] = null;
        $input['user_token_time'] = null;
        $updateStatus = User::whereid($id)->update($input);

        if ($updateStatus) {
            CorporateUsers::where('user_id', $id)->update($update);
            Session::flash('msg', 'Employee Status Changed Successfully');
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
        return redirect()->back();
    }

    public function selectdata_count($search, $where, $startdate, $enddate, $status, $filter)
    {

        $query = User::query();
        $query->where('users.is_delete', '=', '1');
        $query->where('corporate_users.corporate_id', $where);
        $query->join('corporate_users', 'corporate_users.user_id', '=', 'users.id');

        if(!empty($status)){
            if($status == 'active'){
                $query->where('users.status' , 1);
            }else{
                $query->where('users.status' , 0);
            }
        }

        if($filter == 1){
            if (!empty($startdate) && !empty($enddate)) {
                $query->whereDate('users.created_at', '>=', $startdate);
                $query->whereDate('users.created_at', '<=', $enddate);
            }
        }elseif($filter == 3){
            if (!empty($startdate) && !empty($enddate)) {
                $query->whereHas('userCheckIns' , function ($q) use ($startdate , $enddate){
                    $q->whereBetween('user_check_in', [$startdate.' 00:00:00', $enddate.' 23:59:59']);
                });
            }
        }

        $query->Where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
                ->orWhere('phone_number', 'like', '%' . $search . '%');
        });

//        $query->with('cityname');
        $query->with('countryname');
        //$query->with('cityname');
        $query->orderBy('users.created_at', 'DESC');

        $gym_data = $query->get();
        return $gym_data->count();
    }


    public function edit($id)
    {
        $data['user'] = User::where('id', $id)->first();
//        $data['country'] = $this->country->select('id', 'nicename')->where('id', 162)->get();
        $data['city'] = City::select('id', 'name')->where('country', 'Pakistan')->get();

        return view('corporate.corporate_users.edit_employee', $data);

    }


    public function user_info($id)
    {

        $data['check_in_details'] = $this->user_visit_gym->check_in_details($id);

        $data['user_id'] = $id;
        return view('corporate.corporate_users.employee_profile_detail', $data);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrfail($id);


        $r = $request->all();


        $validatedData = $request->validate([
//            'first_name' => 'required|string|max:255',
//            'last_name' => 'required|string|max:255',
            'plain_password' => 'required|string|min:8',
//            'email' => ['required', 'string', 'max:255', Rule::unique('users')->ignore($user->id)],

//            'email' => 'string|email|max:255|unique:users',
//            'phone_number' => 'required',
//            'country' => 'required|integer',
//            'user_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if($r['plain_password']){
            $user->plain_password = $r['plain_password'];
            $user->password = Hash::make($r['plain_password']);
            $user = $user->save();
        }


        if ($user) {
            $msg = "Corporate Employee updated successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }

        return redirect()->back();
    }

    public function delete_employee($employee_id, $corporate_id, $corporate_name)
    {

//        $userCard = Usercard::where('user_id', $employee_id)->delete();
        $corporateUsers = CorporateUsers::where('user_id', $employee_id)->update(['is_delete' => '0']);
//        $userVisitedGym = Uservisitedgym::where('user_id', $employee_id)->delete();
//        $autoRenewalFaild = Autorenewalfaild::where('user_id', $employee_id)->delete();
//        $userUseCoupon = Userusecoupon::where('user_id', $employee_id)->delete();
//        if (DB::table('user_bank_detail')->where('user_id', $employee_id)->get()) {
//            DB::table('user_bank_detail')->where('user_id', $employee_id)->delete();
//        }
//        $userSubscription = Usersubscription::where('user_id', $employee_id)->delete();
        $user = User::where('id', $employee_id)->update(['is_delete' => '0']);
        if ($user) {
            Session::flash('msg', 'Employee Deleted Successfully');
            Session::flash('message', 'alert-success');
        } else {
            Session::flash('msg', 'Cannot delete this employee right now!');
            Session::flash('message', 'alert-danger');
        }

        return redirect()->route('employee-list', [$corporate_id, $corporate_name]);

    }

    public function changeCorporatePassword()
    {
        return view('corporate.change_password');
    }

    public function updateCorporatePassword(Request $r)
    {
        $user_id = session()->get('user_id');
        $validatedData = $r->validate([
            'email' => ['required', 'string', 'max:255', Rule::unique('users')->ignore($user_id)],
            'current_password' => 'required',
            'new_password' => 'nullable|min:8',
            'confirm_password' => 'nullable|min:8|required_with:new_password|same:new_password',
        ]);

        $input = $r->all();
        if (isset($input['new_password'])) {
            $input['password'] = Hash::make($r->new_password);
        }
        unset($input['_token']);
        unset($input['current_password']);
        unset($input['new_password']);
        unset($input['confirm_password']);

        $data = User::where('id', $user_id)->first();
        if (Hash::check($r->current_password, $data->password)) {
            if (Hash::check($r->new_password, $data->password)) {
                $msg = "Current Password and New Password must be different";
                Session::flash('message', 'alert-danger');
            } else {
                if (isset($input['new_password'])) {
                    $input['password'] = Hash::make($r->new_password);

                }
                $update = User::whereid($user_id)->update($input);
                $msg = "Profile updated successfully";
                Session::flash('message', 'alert-success');
            }
        } else {
            $msg = "Current Password is Incorrect";
            Session::flash('message', 'alert-danger');
        }
        Session::flash('msg', $msg);

        return redirect()->back();
    }

    public function sendCredentials($id)
    {
        $r = User::find($id);
        $message = ['user_name' => $r->first_name . ' ' . $r->last_name, 'email' => $r->email, 'password' => $r->plain_password];

        \Mail::to($r->email)->send(new SendMailable($message, 2));
        $msg = "Email has been sent successfully";
        Session::flash('msg', $msg);
        Session::flash('message', 'alert-success');


        return redirect()->back();

    }
}
