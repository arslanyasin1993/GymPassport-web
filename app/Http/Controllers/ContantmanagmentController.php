<?php

namespace App\Http\Controllers;

use App\API\Contantmanagment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
class ContantmanagmentController extends Controller
{
    function __construct(){
        $this->c_m = new Contantmanagment;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data['about_us'] = Contantmanagment::where('type','1')->first();
       $data['privacy'] = Contantmanagment::where('type','2')->first();
       $data['refund'] = Contantmanagment::where('type','3')->first();
       $data['terms_conditions'] = Contantmanagment::where('type','5')->first();
       $data['hiw'] = Contantmanagment::where('type','4')->first();
       $data['usege_rule'] = Contantmanagment::where('type','6')->first();
       $data['faq'] = Contantmanagment::where('type','7')->first();
       return view('admin.content_management.content',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input['type'] = $request->type;
        if($request->type==1){
            $input['description'] = trim($request->about_us);
            $input['address_1'] = trim($request->address_1);
            $input['address_2'] = trim($request->address_2);
                $validatedData = $request->validate([
                    'about_us' => 'required',
                    'address_1' => 'required',
                    'address_2' => 'required',
                ]);
        }elseif ($request->type==2) {
           $input['description'] = trim($request->privacy_policy);
           $validatedData = $request->validate([
                    'privacy_policy' => 'required',
            ]);

        }elseif ($request->type==3) {
            $input['description'] = trim($request->refund_policy);
            $validatedData = $request->validate([
                'refund_policy' => 'required',
            ]);
        }elseif ($request->type==5) {
          $input['description'] = trim($request->terms_conditions);
           $validatedData = $request->validate([
                    'terms_conditions' => 'required',
            ]);
        }elseif($request->type==4){
            $input['description'] = trim($request->how_its_work);
             $validatedData = $request->validate([
                    'how_its_work' => 'required',
            ]);
        }elseif($request->type==6){
            $input['description'] = trim($request->use_rule);
             $validatedData = $request->validate([
                    'use_rule' => 'required',
            ]);
        }else{
            $input['description'] = trim($request->faq_data);
             $validatedData = $request->validate([
                    'faq_data' => 'required',
            ]);
        }
        $insertdata = Contantmanagment::create($input);
        if($insertdata){
            $msg = trans('lang_data.content');
            Session::flash('msg',$msg);
            Session::flash('message','alert-success');
        }else{
            $msg = trans('lang_data.error');
            Session::flash('msg',$msg);
            Session::flash('message','alert-danger');
        }

        return redirect()->route('content');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\API\Contantmanagment  $contantmanagment
     * @return \Illuminate\Http\Response
     */
    public function show(Contantmanagment $contantmanagment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\API\Contantmanagment  $contantmanagment
     * @return \Illuminate\Http\Response
     */
    public function edit(Contantmanagment $contantmanagment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\API\Contantmanagment  $contantmanagment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $input['type'] = $request->type;
        if($request->type==1){
            $input['description'] = trim($request->about_us);
            $input['address_1'] = trim($request->address_1);
            $input['address_2'] = trim($request->address_2);
                $validatedData = $request->validate([
                    'about_us' => 'required',
                    'address_1' => 'required',
                    'address_2' => 'required',
                ]);
        }elseif ($request->type==2) {
           $input['description'] = trim($request->privacy_policy);
           $validatedData = $request->validate([
                    'privacy_policy' => 'required',
            ]);
        }elseif ($request->type==3) {
            $input['description'] = trim($request->refund_policy);
            $validatedData = $request->validate([
                'refund_policy' => 'required',
            ]);
        }elseif ($request->type==5) {
          $input['description'] = trim($request->terms_conditions);
           $validatedData = $request->validate([
                    'terms_conditions' => 'required',
            ]);
        }elseif($request->type==4){
            $input['description'] = trim($request->how_its_work);
             $validatedData = $request->validate([
                    'how_its_work' => 'required',
            ]);
        }elseif($request->type==6){
            $input['description'] = trim($request->use_rule);
             $validatedData = $request->validate([
                    'use_rule' => 'required',
            ]);
        }else{
            $input['description'] = trim($request->faq_data);
             $validatedData = $request->validate([
                    'faq_data' => 'required',
            ]);
        }
        $updatedata = Contantmanagment::whereid($id)->update($input);
        if($updatedata){
            $msg = trans('lang_data.content_update');
            Session::flash('msg',$msg);
            Session::flash('message','alert-success');
        }else{
            $msg = trans('lang_data.error');
            Session::flash('msg',$msg);
            Session::flash('message','alert-danger');
        }

        return redirect()->route('content');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\API\Contantmanagment  $contantmanagment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contantmanagment $contantmanagment)
    {
        //
    }
    
    public function getterms_conditions(){
        $data['terms_conditions'] = Contantmanagment::where('type','5')->first();
        return view('web.terms_conditions',$data);
    }
    public function getrefund_policy(){
        $data['refund'] = Contantmanagment::where('type','3')->first();
        return view('web.refund_policy',$data);
    }
    public function getprivacy_policy(){
        $data['privacy'] = Contantmanagment::where('type','2')->first();
        return view('web.privacy_policy',$data);
    }
    
    public function get_how_its_work(){
        $data['h_i_w'] = Contantmanagment::where('type','4')->first();
        return view('web.how_its_work',$data);
    }
    
    public function get_about_us(){
        $data['about_us'] = Contantmanagment::where('type','1')->first();
        return view('web.about_us',$data);
    }
    
    public function get_faq(){
        $data['faq'] = Contantmanagment::where('type','7')->first();
        return view('web.faq',$data);
    }
}
