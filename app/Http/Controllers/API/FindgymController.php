<?php

namespace App\Http\Controllers\API;

use App\API\Subscription;
use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\ApiCommanFunctionController;
use App\Country;
use App\User;
use App\Usergym;
use App\Gymimage;
use App\Gymcategory;
use App\Uservisitedgym;
use App\Gym_facilities;
use App\Facilities;
use App\Gymtiming;
use App\API\Usersubscription;
use App\API\Plancategory;
use Validator;
use DB;

class FindgymController extends Controller
{

    public $successStatus = 200;

    function __construct()
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $this->PostData = (array)$data;
        $this->return = new ApiCommanFunctionController;
        $this->gym_mod = new Usergym;
        $this->gym_plan_cat = new Gymcategory;
        $this->gym_facilities = new Gym_facilities;
        $this->user_sub = new Usersubscription; //05-11-2019
        $this->plan_cat = new Plancategory; //23-01-2020
        //date_default_timezone_set("Asia/Kolkata");
    }

    public function gymlist()
    {
        $array = $this->PostData;
//        echo '<pre>';print_r($array);die();
        $lat = $array['gym_latitude'];
        $lng = $array['gym_longitude'];
        $search = ($array['search']) ? $array['search'] : '';
        $distance = $array['distance'];
        $user_id = $array['user_id'];
        $status = 1;
        //05-11-2019
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean)1 : (boolean)0; //user subscription data
        //end


        $searchCity = City::where('name', $search)->first();

        if ($searchCity) {
            $search = $searchCity->id;
        }

        $user_plan = Usersubscription::select('id', 'plan_name', 'visit_pass')
            ->where('status', 1)->where('user_id', $user_id)->first();
        if (empty($user_plan)) {
            $user_plan_category_id = '';
        } else {
            $user_plan_category_id = ($user_plan->plan_name) ? $user_plan->plan_name : '';
        }

        $getGymData = Usergym::getByDistance($lat, $lng, $distance, $status, $search);
//        dd($getGymData);
        $data = [];
        if (!empty($getGymData)) {
            foreach ($getGymData as $key => $value) {
                $plan_cat_name = $this->gym_mod->plan_name_by_gym_id($value->gym_id);
                foreach ($plan_cat_name as $item) {
                    if ($user_plan_category_id) {
                        if ($user_plan_category_id == 4 && $item->plan_category_id == 3) {
                            continue 2;
                        }
                    }

                }

                $data[$key]['gym_plan_category'] = '';
                if (!empty($plan_cat_name)) {
                    $data[$key]['gym_plan_category'] = trim(str_replace('Fit Pass', '', $plan_cat_name[0]->plan_cat_name));
                    $gym_plan_category = trim(str_replace('Fit', '', $plan_cat_name[0]->plan_cat_name));
                    $data[$key]['gym_plan_category'] = trim(str_replace('Gym', '', $gym_plan_category));
                    $data[$key]['plan_cat_id'] = (string)$plan_cat_name[0]->plan_category_id;
                    $data[$key]['gym_id'] = (string)$value->gym_id;
                    $data[$key]['gym_name'] = $value->gym_name;
                    $data[$key]['gym_logo'] = trans('constants.image_url') . $value->gym_logo;
                    $data[$key]['gym_address'] = $value->gym_address;
                    $data[$key]['gym_latitude'] = $value->gym_latitude;
                    $data[$key]['gym_longitude'] = $value->gym_longitude;
                    $data[$key]['is_all_day_open'] = ($value->is_all_day_open == 1) ? true : false;
                    $data[$key]['gym_city'] = $value->city_name;
                    $data[$key]['distance'] = (string)round($value->distance, 2) . " kms";
                }
                //print_r($plan_cat_name);
            }
            //die;
            $gym_data = array_values($data);
            return $this->return->json_sendResponse(1, $gym_data, 'Gym Listing', $is_sub_val);
        } else {
            $allData = [];
            return $this->return->json_sendResponse(0, $allData, 'Gym not found at this location', $is_sub_val);
        }
    }

    public function gymdetails()
    {
        $array = $this->PostData;
        $gym['gym_id'] = $array['gym_id'];
        $user_id = $array['user_id'];
        $lat = $array['latitute'] ? $array['latitute'] : "";
        $lng = $array['longitude'] ? $array['longitude'] : "";
        $rules = [
            'gym_id' => 'required',
        ];
        $message = [
            'gym_id.required' => 'Gym id is required',
        ];
        $validator = Validator::make($gym, $rules, $message);
        if ($validator->fails()) {
            return $this->return->sendError_obj($validator->errors()->first());
        }
        $gym_id = $array['gym_id'];
        $plan_cat_id = $this->gym_mod->cat_id_by_gym_id($gym_id);
        // print_r($plan_cat_id);die;
        $select = array('id', 'users_id', 'gym_name', 'gym_logo', 'phone_number', 'gym_activities', 'about_gym', 'gym_address',
            'gym_latitude', 'gym_longitude', 'country', 'accept_terms_condition', 'status', 'created_at', 'is_all_day_open', 'is_staff_hours', 'staff_hours');
        $Gymdetails = $this->gym_mod->gym_details($gym_id, $select);
        //echo '<pre>'; print_r($Gymdetails); die;
        //05-11-2019
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean)1 : (boolean)0; //user subscription data
        //end
        $plan_cat_name = $this->gym_mod->plan_name_by_gym_id($gym_id);
        $distance = "";
        if (!empty($lat) && !empty($lng)) {
            $get_distance = $this->gym_mod->get_distance_by_gym_id($lat, $lng, $gym_id);
            $distance = (string)round($get_distance[0]->distance, 2);
        }
        // echo '<pre>';print_r($get_distance);die;
        if (!empty($Gymdetails)) {
            $data['gym_id'] = (string)$Gymdetails->id;
            $data['gym_name'] = ($Gymdetails->gym_name) ? $Gymdetails->gym_name : '';
            if (!empty($plan_cat_name)) {
                $gym_plan_category = trim(str_replace('Fit', '', $plan_cat_name[0]->plan_cat_name));
            } else {
                $gym_plan_category = '';
            }

            if (count($plan_cat_id) > 1) {
                $plan_cat_id_filter = collect($plan_cat_id)->where('plan_category_id', '<', 4)->sortBy(['plan_cat_id', 'DESC']);
                if (count($plan_cat_id_filter)) {
                    $plan_cat_id = $plan_cat_id_filter->values()->all();
                }

            }

            $data['plan_cat_id'] = $plan_cat_id ? (string)$plan_cat_id[0]->plan_category_id : '';
            $data['gym_plan_category'] = trim(str_replace('Gym', '', $gym_plan_category));
            $data['gym_logo'] = ($Gymdetails->gym_logo) ? trans('constants.image_url') . $Gymdetails->gym_logo : '';
            $data['phone_number'] = ($Gymdetails->phone_number) ? (string)$Gymdetails->phone_number : '';
            $data['gym_activities'] = ($Gymdetails->gym_activities) ? $Gymdetails->gym_activities : '';
            $data['about_gym'] = ($Gymdetails->about_gym) ? $Gymdetails->about_gym : '';
            $data['gym_distance'] = $distance;
            $data['gym_address'] = ($Gymdetails->gym_address) ? $Gymdetails->gym_address : '';
            $data['gym_latitude'] = ($Gymdetails->gym_latitude) ? $Gymdetails->gym_latitude : '';
            $data['gym_longitude'] = ($Gymdetails->gym_longitude) ? $Gymdetails->gym_longitude : '';
            $data['country_id'] = ($Gymdetails->countryname->id) ? (string)$Gymdetails->countryname->id : '';
            $data['country'] = ($Gymdetails->countryname->nicename) ? (string)$Gymdetails->countryname->nicename : '';
            $data['created_at'] = date('d/m/Y', strtotime($Gymdetails->created_at));
            $data['is_all_day_open'] = ($Gymdetails->is_all_day_open == 1) ? true : false;
            $data['is_all_day_open_staff'] = ($Gymdetails->is_staff_hours == 1) ? true : false;
            $data['gym_staff_time'] = $Gymdetails->staff_hours ? $Gymdetails->staff_hours : '';
            $data['gym_owner_detail']['gym_owner_id'] = (string)$Gymdetails->gym_owner_detail->id;
            $data['gym_owner_detail']['gym_owner_name'] = $Gymdetails->gym_owner_detail->first_name . ' ' . $Gymdetails->gym_owner_detail->last_name;
            $data['gym_owner_detail']['owner_email'] = $Gymdetails->gym_owner_detail->email;
            $data['gym_image'] = [];
            $data['gym_time'] = [];
            $data['silver_total_gym'] = DB::table('gymcategories')->where('plan_category_id', '1')->get()->count();
            $data['standard_total_gym'] = DB::table('gymcategories')->where('plan_category_id', '1')->get()->count() + DB::table('gymcategories')->where('plan_category_id', '2')->get()->count();
            $data['gold_total_gym'] = DB::table('users_gym')->where('status', '1')->get()->count();

            $data['facilities'] = [];
            if (!empty($Gymdetails->gym_image)) {
                foreach ($Gymdetails->gym_image as $key => $value) {
                    $data['gym_image'][$key]['image_id'] = (string)$value->id;
                    $data['gym_image'][$key]['gym_image'] = trans('constants.image_url') . $value->gym_image;
                }
            }
            if (isset($Gymdetails->gym_time) && !empty($Gymdetails->gym_time)) {
                foreach ($Gymdetails->gym_time as $key => $value) {
                    $data['gym_time'][$key]['gym_open_days'] = (string)$value->gym_open_days;
                    $data['gym_time'][$key]['gym_open_timing'] = date('h:i A', strtotime($value->gym_open_timing));
                    $data['gym_time'][$key]['gym_close_time'] = date('h:i A', strtotime($value->gym_close_time));
                }
            }
//            if (isset($Gymdetails->staff_timing) && !empty($Gymdetails->staff_timing)) {
//                foreach ($Gymdetails->staff_timing as $key => $value) {
//                    $data['gym_staff_time'][$key]['gym_open_days'] = (string) $value->gym_open_days;
//                    $data['gym_staff_time'][$key]['gym_open_timing'] = date('h:i A', strtotime($value->gym_open_timing));
//                    $data['gym_staff_time'][$key]['gym_close_time'] = date('h:i A', strtotime($value->gym_close_time));
//                }
//            }
            if (!empty($Gymdetails->facilities)) {
                foreach ($Gymdetails->facilities as $key => $value) {
                    if (!empty($value->gym_facilities)) {
                        $data['facilities'][$key]['facilities_id'] = (string)$value->gym_facilities->id;
                        $data['facilities'][$key]['facilities_name'] = $value->gym_facilities->facilities ? $value->gym_facilities->facilities : '';
                        $data['facilities'][$key]['facilities_image'] = $value->gym_facilities->imag_url ? $value->gym_facilities->imag_url : '';
                    }
                }
            }
            return $this->return->json_sendResponse(1, $data, 'Gym Details', $is_sub_val);
        } else {
            $allData = (object)[];
            return $this->return->json_sendResponse(0, $allData, 'Gym details not available', $is_sub_val);
        }
    }

    public function check_in_gymlist()
    {
        $array = $this->PostData;
        //echo '<pre>';print_r($array);die();
        $lat = $array['gym_latitude'];
        $lng = $array['gym_longitude'];
        $user_id = $array['user_id'];
        //05-11-2019
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean)1 : (boolean)0; //user subscription data
        //end
        $user_plan = Usersubscription::select('id', 'plan_name', 'visit_pass')
            ->where('status', 1)->where('user_id', $user_id)->first();
        if ($user_plan) {
            $user_visits = Uservisitedgym::select('id', 'user_id', 'usersubscription_id', 'gym_id')
                ->with('gym_name')
                ->where('user_id', $user_id)
                ->where('usersubscription_id', $user_plan->id)
                ->orderBy('id', 'DESC')
                ->limit('5')
                ->get();
            // return $user_visits;
            foreach ($user_visits as $user_visit) {
                $user_total_visit = Uservisitedgym::where('user_id', $user_visit->user_id)
                    ->where('usersubscription_id', $user_visit->usersubscription_id)
                    ->where('gym_id', $user_visit->gym_id)
                    ->count();
                $user_visit->user_total_visit = $user_total_visit;
            }
        } else {
            $user_visits = '';
        }


        // return $user_visits;


        if (empty($user_plan)) {
            $allData = [];
            return $this->return->json_sendResponse(0, $allData, 'You have not any active plan', $is_sub_val);
        }
        $user_plan_category_id = ($user_plan->plan_name) ? $user_plan->plan_name : '';
        $user_subscription_id = ($user_plan->id) ? $user_plan->id : '';
        $user_have_visit_pass = ($user_plan->visit_pass) ? $user_plan->visit_pass : '';
        $status = 1;

        $getGymData = Usergym::check_in_gymlist($lat, $lng, $status, $user_plan_category_id);
        // dd($getGymData);
        $total_visit = $this->gym_mod->usertotal_visit($user_id, $user_subscription_id, $user_have_visit_pass);

        //echo '<pre>';print_r($getGymData);die();
        $total_visit = ($total_visit) ? (string)$total_visit : '0';
        $get_plan_wise_gym = $this->gym_plan_cat->get_gym_plan_id($user_plan_category_id);
        $gym_array = array_column($get_plan_wise_gym, 'user_gym_id');
        if (!empty($getGymData)) {
            foreach ($getGymData as $key => $value) {
                //if (in_array($value->gym_id, $gym_array)) {
                $plan_cat_name = $this->gym_mod->plan_name_by_gym_id($value->gym_id);


                if (!empty($plan_cat_name)) {
                    foreach ($plan_cat_name as $item) {
                        if ($user_plan_category_id == 4 && $item->plan_category_id == 3) {
                            continue 2;
                        }
                    }
                    $gym_plan_category = trim(str_replace('Fit', '', $plan_cat_name[0]->plan_cat_name));
                    $data['check_in_gymlist'][$key]['gym_plan_category'] = trim(str_replace('Gym', '', $gym_plan_category));
                    $data['check_in_gymlist'][$key]['plan_cat_id'] = (string)$plan_cat_name[0]->plan_category_id;
                } else {
                    //print_r($plan_cat_name);die();
                    $data['check_in_gymlist'][$key]['gym_plan_category'] = '';
                    $data['check_in_gymlist'][$key]['plan_cat_id'] = '';

                }


                $data['check_in_gymlist'][$key]['gym_id'] = (string)$value->gym_id;
                $data['check_in_gymlist'][$key]['gym_name'] = $value->gym_name;
                $data['check_in_gymlist'][$key]['gym_logo'] = trans('constants.image_url') . $value->gym_logo;
                $data['check_in_gymlist'][$key]['gym_address'] = $value->gym_address;
                $data['check_in_gymlist'][$key]['gym_latitude'] = $value->gym_latitude;
                $data['check_in_gymlist'][$key]['gym_longitude'] = $value->gym_longitude;
                $data['check_in_gymlist'][$key]['distance'] = (string)round($value->distance, 2) . " kms";
                //$data['check_in_gymlist'][$key]['distance'] = (string) round(0, 2) . " kms";
                //}
            }
            $data['check_in_gymlist'] = array_values($data['check_in_gymlist']);

            $data['last_five_visit'] = $user_visits;
            return $this->return->checkIngym_sendResponse(1, $data, 'Gym Listing', 0, $is_sub_val);
        } else {
            $allData = [];
            return $this->return->checkIngym_sendResponse(0, $allData, 'No Gyms found within 500m of your location. You must be at a  gym location to check in.', $total_visit, $is_sub_val);
        }
    }

    //10-10-2019

    public function gym_check_in()
    {
        try {
            $response['flag'] = '';
            $response['message'] = '';
            $array = $this->PostData;
            $gym_id = $array['gym_id'];
            $user_id = $array['user_id'];
            $time = strtotime("now");
            $rules = [
                'gym_id' => 'required',
            ];
            $message = [
                'gym_id.required' => 'Gym is required',
            ];
            $validator = Validator::make($array, $rules, $message);
            if ($validator->fails()) {
                return $this->return->sendError_obj($validator->errors()->first());
            }

            $user_subscription_id = Usersubscription::select('subscription_id')->where('user_id', $user_id)->orderBy('created_at', 'DESC')->first();

            $user_plan_id = Subscription::select('id', 'plan_name')->where('id', $user_subscription_id['subscription_id'])->first();
            $is_corporate_user = User::find($user_id)->is_corporate_user;

            if ($user_plan_id->plan_name == 4) {
                $gym_plan_id = Gymcategory::select('plan_category_id')->where('user_gym_id', $gym_id)->whereIn('plan_category_id', [1, 2])->first();


            } else {
                $gym_plan_id = Gymcategory::select('plan_category_id')->where('user_gym_id', $gym_id)->whereIn('plan_category_id', [1, 2,3])->first();

            }
            if ($user_plan_id->plan_name == 4 && !$gym_plan_id) {
                $response['flag'] = 0;
                $response['message'] = "Sorry you cannot checkin to the gym with free trial.";
                return $this->return->json_sendResponse($response['flag'], (object)[], $response['message'], 0);
            }
            if ($user_plan_id && $gym_plan_id) {


                if ($user_plan_id->plan_name < $gym_plan_id->plan_category_id) {
                    if ($user_plan_id->plan_name == 1) {
                        $response['flag'] = 0;
                        $response['message'] = "Sorry! you can not checkin in with Gym Passport - Lite pass.";
                        return $this->return->json_sendResponse($response['flag'], (object)[], $response['message'], 0);
                    }
                    if ($user_plan_id->plan_name == 2) {
                        $response['flag'] = 0;
                        $response['message'] = "Sorry! you can not checkin in with Gym Passport - Standard pass.";
                        return $this->return->json_sendResponse($response['flag'], (object)[], $response['message'], 0);
                    }
                }
            }


            $gym_price_per_visit = Usergym::select('id', 'gym_price_per_visit')->where('id', $gym_id)->first();
            if(empty($gym_price_per_visit)){
                return $this->return->json_sendResponse(0, (object)[], 'Gym not Found', 0);
            }

            $user_plan = Usersubscription::select('id', 'plan_name', 'expired_at','subscription_id',  'visit_pass', 'plan_duration', 'plan_time', 'created_at', 'total_payable_amount', 'amount')->where('status', 1)->where('user_id', $user_id)->first();
            if (!empty($user_plan)) {

                // insert array
                // echo $user_plan->id;die;
                $last_check_in_date = Uservisitedgym::select('user_check_in')
                    ->where('user_id', $user_id)
                    ->where('usersubscription_id', $user_plan->id)
                    ->orderBy('id', 'DESC')->limit(1)->first();


                $insert['user_id'] = $user_id;
                $insert['unique_id'] = $user_id . '_' . $time;
                $insert['gym_id'] = $gym_id;
                $insert['usersubscription_id'] = $user_plan->id;
                $insert['check_in_status'] = 1;
                $insert['user_check_in'] = date('Y-m-d H:i:s' , $time);
                $insert['gym_earn_amount'] = $gym_price_per_visit->gym_price_per_visit;

                $allData = (object)[];
                $is_all_day_open = Usergym::select('is_all_day_open')->where('id', $gym_id)->first(); //select 24* 7 open gym
                $gym_timing = $this->gym_open_timing($gym_id); //checking open and close gym timing
                $lag = session()->get('map_lat');
                $long = session()->get('map_long');
                //$gym_distance = $this->gym_mod->check_in_user_distance($lag, $long, $gym_id);
                $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
                //dd($is_subscribe_data);
                $is_sub_val = ($is_subscribe_data) ? (boolean)1 : (boolean)0; //user subscription data
                //end
                //return $is_sub_val;

                /* if ($is_all_day_open->is_all_day_open != 1) {
                     if ($gym_timing == '0' || $gym_timing == '2') {
                         $msg = ($gym_timing == '0') ? 'Currently gym is closed.You can not check-in at this time' : 'Gym is closed today';
                         $response['flag'] = 0;
                         $response['message'] = $msg; //'You need to upgrade your plan to access this gym';
                         return $this->return->json_sendResponse($response['flag'], (object) [], $response['message'], $is_sub_val);
                     }
                 }*/

                $total_visit = Uservisitedgym::where('user_id', $user_id)->where('usersubscription_id', $user_plan->id)->get()->count();
                $total_visit_in_a_gym = Uservisitedgym::where('user_id', $user_id)->where('gym_id', $gym_id)->where('usersubscription_id', $user_plan->id)->get()->count();

                $last_ggym_check = (isset($last_check_in_date->user_check_in)) ? date('Y-m-d H:i:s', strtotime($last_check_in_date->user_check_in)) : '';
                $current_date = ' ' . date('Y-m-d H:i:s',$time);
                $to_time = strtotime($current_date);
                $from_time = strtotime($last_ggym_check);
                $check_time = round(abs($to_time - $from_time) / 60, 2);

                $I8ActiveCount = Uservisitedgym::
                where('user_id', $user_id)
                    ->where('gym_id', 118)
                    ->where('usersubscription_id', $user_plan->id)
                    ->whereMonth('user_check_in', date('m',$time))
                    ->orderBy('id', 'DESC')->count();
                if ($I8ActiveCount >= 5 && $gym_id == 118) {
                    $response['flag'] = 0;
                    $response['message'] = "Sorry! You can not checkin in this Gym more than 5 time in a month.";
                    return $this->return->json_sendResponse($response['flag'], (object)[], $response['message'], 0);
                } else {
                    if ($user_plan_id->plan_name == 4) {
                        if ($total_visit < 1) {
                            if ($check_time > 480.00) {
                                //if($total_visit_in_a_gym < 8){
                                $unique_id = Uservisitedgym::where('unique_id' , $insert['unique_id'])->delete();
                                // if(!empty($unique_id)){
                                //     $wait_msg = 'Please Wait';
                                //     return $this->return->json_sendResponse(0, (object)[], $wait_msg, $is_sub_val);
                                // }
                                $add = Uservisitedgym::create($insert);
                                $response['flag'] = 1;
                                $total_visit_in_a_gym = Uservisitedgym::where('user_id', $user_id)->where('gym_id', $gym_id)->where('usersubscription_id', $user_plan->id)->get()->count();
                                //    if($total_visit_in_a_gym == 8){
                                //        $response['message'] = 'You have used your 8th check-in to this Gym under your current pass. You will not be able to check-in to this Gym again until your pass renews';
                                //     }else{
                                $response['message'] = 'Your Check-in was successful';
                                //     }
                                $total_visit = Uservisitedgym::where('user_id', $user_id)->where('usersubscription_id', $user_plan->id)->get()->count();

                                // }else{
                                //     $response['flag'] = 0;
                                //     $response['message'] = 'You have used your 8th check-in to this Gym under your current pass. You will not be able to check-in to this Gym again until your pass renews';
                                // }
                                if ($total_visit == 1) {
                                    $user_id = $user_id;
                                    $subcription_id = $user_plan->id;
                                    $update['status'] = '0';
                                    $update['expired_at'] = date('Y-m-d H:i:s');//->where('user_id',$user_id)->where('status','1')
                                    $update = Usersubscription::where('id', $subcription_id)->update($update);
                                }
                            } else {
                                $wait_msg = 'You must wait 8 hours after your previous check-in to be able to check-in again';
                                return $this->return->json_sendResponse(0, (object)[], $wait_msg, $is_sub_val);
                            }
                        } else {
                            $response['flag'] = 3;
                            $response['message'] = 'You have used all of your check-ins under this trail pass.Please purchase our premium passes.';
                            //  $response['message'] = 'You have completed all your visit to continue please renew your plan';
                        }
                    } else {

                        $last_check_in_count = Uservisitedgym::where('user_id', $user_id)
                            ->where('usersubscription_id', $user_plan->id)
                            ->whereDate('user_check_in', '=', date('Y-m-d'))
                            ->count();
                        if ($last_check_in_count < 2) {
                            if ($user_plan->subscription_id >= 5 && $user_plan->subscription_id <= 10) {
                                if ($check_time > 480.00) {
                                    $unique_id = Uservisitedgym::where('unique_id' , $insert['unique_id'])->delete();
                                    // if(!empty($unique_id)){
                                    //     $wait_msg = 'Please Wait';
                                    //     return $this->return->json_sendResponse(0, (object)[], $wait_msg, $is_sub_val);
                                    // }

                                    $add = Uservisitedgym::create($insert);
                                    $response['flag'] = 1;
                                    $response['message'] = 'Your Check-in was successful';
                                }else {
                                    $wait_msg = 'You must wait 8 hours after your previous check-in to be able to check-in again';
                                    return $this->return->json_sendResponse(0, (object)[], $wait_msg, $is_sub_val);
                                }
                            } else {
                                if ($is_corporate_user) {
                                    if ($check_time > 480.00) {
                                        $unique_id = Uservisitedgym::where('unique_id', $insert['unique_id'])->delete();
                                        // if(!empty($unique_id)){
                                        //     $wait_msg = 'Please Wait';
                                        //     return $this->return->json_sendResponse(0, (object)[], $wait_msg, $is_sub_val);
                                        // }

                                        $add = Uservisitedgym::create($insert);
                                        $response['flag'] = 1;
                                        $response['message'] = 'Your Check-in was successful';
                                    } else {
                                        $wait_msg = 'You must wait 8 hours after your previous check-in to be able to check-in again';
                                        return $this->return->json_sendResponse(0, (object)[], $wait_msg, $is_sub_val);
                                    }
                                }else{
                                    if ($total_visit < 20) {
                                        if ($check_time > 480.00) {
                                            //if($total_visit_in_a_gym < 8){
                                            $unique_id = Uservisitedgym::where('unique_id', $insert['unique_id'])->delete();
                                            // if(!empty($unique_id)){
                                            //     $wait_msg = 'Please Wait';
                                            //     return $this->return->json_sendResponse(0, (object)[], $wait_msg, $is_sub_val);
                                            // }
                                            $add = Uservisitedgym::create($insert);
                                            $response['flag'] = 1;
                                            $total_visit_in_a_gym = Uservisitedgym::where('user_id', $user_id)->where('gym_id', $gym_id)->where('usersubscription_id', $user_plan->id)->get()->count();
                                            //    if($total_visit_in_a_gym == 8){
                                            //        $response['message'] = 'You have used your 8th check-in to this Gym under your current pass. You will not be able to check-in to this Gym again until your pass renews';
                                            //     }else{
                                            $response['message'] = 'Your Check-in was successful';
                                            //     }
                                            $total_visit = Uservisitedgym::where('user_id', $user_id)->where('usersubscription_id', $user_plan->id)->get()->count();

                                            // }else{
                                            //     $response['flag'] = 0;
                                            //     $response['message'] = 'You have used your 8th check-in to this Gym under your current pass. You will not be able to check-in to this Gym again until your pass renews';
                                            // }
                                        } else {
                                            $wait_msg = 'You must wait 8 hours after your previous check-in to be able to check-in again';
                                            return $this->return->json_sendResponse(0, (object)[], $wait_msg, $is_sub_val);
                                        }
                                    } else {
                                        $response['flag'] = 3;
                                        $response['message'] = 'You have used all of your check-ins under this pass. Please renew your pass to continue visiting gyms';
                                        //  $response['message'] = 'You have completed all your visit to continue please renew your plan';
                                    }
                                }
                            }
                        } else {
                            $response['flag'] = 4;
                            $response['message'] = 'You have used your 2 check-ins for today. Please wait until tomorrow to continue visiting gyms';

                        }
                    }
                }
                return $this->return->json_sendResponse($response['flag'], (object)[], $response['message'], $is_sub_val);

//
            } else {
                $response['flag'] = 0;
                $response['message'] = 'Your plan has been expired';
                return $this->return->json_sendResponse($response['flag'], (object)[], $response['message'], 0);
            }
        } catch (\Throwable $th) {
            return $this->return->json_sendResponse(0, (object)[], $th->getMessage(), 0);
        }
    }

    public function gym_check_in_old()
    {

        $array = $this->PostData;
        $user_id = $array['user_id'];
        $gym_id = $array['gym_id'];
        $rules = [
            'gym_id' => 'required',
        ];
        $message = [
            'gym_id.required' => 'Gym is required',
        ];
        $validator = Validator::make($array, $rules, $message);
        if ($validator->fails()) {
            return $this->return->sendError_obj($validator->errors()->first());
        }
        //05-11-2019
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean)1 : (boolean)0; //user subscription data
        // return $is_sub_val;
        //end
        $user_plan = Usersubscription::select('id', 'plan_name', 'amount', 'expired_at', 'visit_pass', 'plan_duration', 'plan_time', 'created_at', 'total_payable_amount')->where('status', 1)->where('user_id', $user_id)->first();

        //echo '<pre>';print_r($user_plan);die;
        $allData = (object)[];
        $is_all_day_open = Usergym::select('is_all_day_open')->where('id', $gym_id)->first(); //select 24* 7 open gym
        //echo '<pre>';print_r($is_all_day_open);die;
        if ($is_all_day_open->is_all_day_open != 1) { //if not open 24 *7
            $gym_timing = $this->gym_open_timing($gym_id); //checking open and close gym timing
            if ($gym_timing == '0' || $gym_timing == '2') {
                $msg = ($gym_timing == '0') ? 'Currently gym is closed. You can not check-in at this time' : 'Gym is closed today';
                return $this->return->json_sendResponse(0, $allData, $msg, $is_sub_val);
            }//end time here
        }//end 24*7 if not open

        if (empty($user_plan)) {
            return $this->return->json_sendResponse(0, $allData, 'You have not any active plan', $is_sub_val);
        }
        $final_divided_amount = $this->given_amount_patner_after_visit($user_plan);
        //echo '<pre>';print_r($final_divided_amount);die;
        $user_plan_category_id = ($user_plan->plan_name) ? $user_plan->plan_name : '';
        $user_subscription_id = ($user_plan->id) ? $user_plan->id : '';
        $user_have_visit_pass = ($user_plan->visit_pass) ? $user_plan->visit_pass : '';
        // $purchase_date = date('Y/m/d',strtotime($user_plan->created_at)); //purchase date
        // $add_date  = $user_plan->plan_duration.' '.$user_plan->plan_time; // plan valid duration
        // $plan_end_date = date('Y/m/d', strtotime($purchase_date. " + $add_date"));// plan ending on
        $plan_end_date = date('Y/m/d', strtotime($user_plan->expired_at)); //plan_end date
        $currentdate = date('Y/m/d'); //current date
        $get_plan_wise_gym = $this->gym_plan_cat->get_gym_plan_id($user_plan_category_id);
        $gym_array = array_column($get_plan_wise_gym, 'user_gym_id');
        //echo '<pre>'; print_r($gym_array);die;
        if (!in_array($gym_id, $gym_array)) {
            $get_user_current_plan_name = $this->plan_cat->web_get_subscription_data($user_plan->plan_name);
            $get_gym_plan_name = $this->gym_plan_cat->get_gym_plan($gym_id);
            $gym_msg = 'This gym is available to the ' . $get_gym_plan_name[0]->plan_cat_name . ' your plan is the ' . $get_user_current_plan_name->plan_cat_name . '. If you wish to visit this gym, please upgrade your membership plan.';
            return $this->return->json_sendResponse(135, $allData, $gym_msg, $is_sub_val);
            // return $this->return->json_sendResponse(135, $allData, 'This gym is not added any subscription plan please contact your admin', $is_sub_val);
        }

        if (strtotime($currentdate) <= strtotime($plan_end_date)) { //check user expired plan
            $total_visit = $this->gym_mod->usertotal_visit($user_id, $user_subscription_id, $user_have_visit_pass);
            $total_visit = ($total_visit) ? $total_visit : '';
            $insert['user_id'] = $user_id;
            $insert['gym_id'] = $gym_id;
            $insert['usersubscription_id'] = $user_subscription_id;
            $insert['check_in_status'] = 1;
            $insert['user_check_in'] = date('Y-m-d H:i:s');
            $insert['gym_earn_amount'] = $final_divided_amount;
            $amount_update['gym_earn_amount'] = $final_divided_amount;

            $last_check_in_date = Uservisitedgym::select('user_check_in')->where('user_id', $user_id)->where('usersubscription_id', $user_subscription_id)->orderBy('id', 'DESC')->limit(1)->first();
            if (empty($last_check_in_date)) {  //check if user have not any visit with current plan
                $add = Uservisitedgym::create($insert);
                $total_visit = $this->gym_mod->usertotal_visit($user_id, $user_subscription_id, $user_have_visit_pass);
                $total_visit = ($total_visit) ? $total_visit : '';
                // echo $user_have_visit_pass.'  dfjkfkj '.$total_visit;die;
                if ($user_have_visit_pass == 'month') { // if user monthly plan
                    Uservisitedgym::where('usersubscription_id', $user_subscription_id)->update($amount_update);
                    $arr['is_subscribed'] = (boolean)1;
                    return $this->return->json_sendResponse(1, $arr, 'Your Check-in was successful', $arr['is_subscribed']);
                } else {
                    if (($user_have_visit_pass - $total_visit) == '0') {
                        $arr['is_subscribed'] = (boolean)0;
                        $arr['is_last_visit'] = (boolean)1;
                        $update['status'] = '0';
                        //$msg = "Your Check-in was successful Now you have not any remaining visits left, you need to renew your subscription to access the Gyms";
                        $msg = "Your Check-in was successful! You have now used your last remaining visit.You will need to renew your subscription to continue your membership plan.";
                        Usersubscription::where('user_id', $user_id)->update($update);
                    } else {
                        $arr['is_subscribed'] = (boolean)1;
                        $arr['is_last_visit'] = (boolean)0;
                        $msg = "Your Check-in was successful";
                    }
                }
                return $this->return->json_sendResponse(1, $arr, $msg, $arr['is_subscribed']);
            } else {
                $last_ggym_check = $last_check_in_date->user_check_in ? date('Y-m-d h:i:s', strtotime($last_check_in_date->user_check_in)) : '';
                $current_date = ' ' . date('Y-m-d h:i:s');
                $to_time = strtotime($current_date);
                $from_time = strtotime($last_ggym_check);
                $check_time = round(abs($to_time - $from_time) / 60, 2);
                $timediff = strtotime(date('Y-m-d h:i:s')) - strtotime($last_ggym_check);
                // echo $check_time;die;
                if ($check_time > 240.00) { // check user check after 10 minute last visit
                    //'more than 10 minute';
                    if ($user_have_visit_pass == 'month') { // if user monthly plan
                        $add = Uservisitedgym::create($insert);
                        $arr['is_subscribed'] = (boolean)1;
                        $arr['is_last_visit'] = (boolean)0;
                        Uservisitedgym::where('usersubscription_id', $user_subscription_id)->update($amount_update);
                        return $this->return->json_sendResponse(1, $arr, 'Your Check-in was successful', $arr['is_subscribed']);
                    } else {
                        if ($total_visit < $user_have_visit_pass) { //check total user visit current subscription
                            $add = Uservisitedgym::create($insert);
                            $total_visit = $this->gym_mod->usertotal_visit($user_id, $user_subscription_id, $user_have_visit_pass);
                            //echo $user_have_visit_pass.'    d d '.$total_visit;die;
                            if (($user_have_visit_pass - $total_visit) == '0') {
                                $arr['is_subscribed'] = (boolean)0;
                                $arr['is_last_visit'] = (boolean)1;
                                $update['status'] = '0';
                                //$msg = "Your Check-in was successful Now you have not any remaining visits left, you need to renew your subscription to access the Gyms";
                                $msg = "Your Check-in was successful! You have now used your last remaining visit.You will need to renew your subscription to continue your membership plan.";
                                Usersubscription::where('user_id', $user_id)->update($update);
                            } else {
                                $arr['is_subscribed'] = (boolean)1;
                                $arr['is_last_visit'] = (boolean)0;
                                $msg = "Your Check-in was successful";
                            }
                            return $this->return->json_sendResponse(1, $arr, $msg, $arr['is_subscribed']);
                        } else {
                            return $this->return->json_sendResponse(0, $allData, 'You have completed all your visited', $is_sub_val);
                        }
                    }
                } else {
                    $wait_msg = 'You must wait 10 minutes after your previous check-in to be able to check-in again';
                    return $this->return->json_sendResponse(0, $allData, $wait_msg, $is_sub_val);
                    //return $this->return->json_sendResponse(0, $allData, 'Please wait 10 minutes to check-in again', $is_sub_val);
                }
            }
        } else {
            return $this->return->json_sendResponse(0, $allData, 'You plan has been expired', $is_sub_val);
        }
    }

    public function gym_open_timing($gym_id)
    {
        if (date('l') == 'Sunday') {
            $day = '0';
        }
        if (date('l') == 'Monday') {
            $day = '1';
        }
        if (date('l') == 'Tuesday') {
            $day = '2';
        }
        if (date('l') == 'Wednesday') {
            $day = '3';
        }
        if (date('l') == 'Thursday') {
            $day = '4';
        }
        if (date('l') == 'Friday') {
            $day = '5';
        }
        if (date('l') == 'Saturday') {
            $day = '6';
        }
        $gym_time_data = Gymtiming::where('users_gym_id', $gym_id)->where('gym_open_days', $day)->first();
        if (empty($gym_time_data)) {
            $data = '2';
        } else {
            $open_time = $gym_time_data->gym_open_timing;
            $close_time = $gym_time_data->gym_close_time;
            $gym_open_days = $gym_time_data->gym_open_days;
            $current_time = date('H:i:s');
            if ($current_time >= $open_time && $current_time <= $close_time && $gym_open_days == $day) {
                //  19:22:28           13:30        19:22:28        21:30           1              1
                $data = '1';
            } else {
                $data = '0';
            }
        }
        return $data;
    }

    public function given_amount_patner_after_visit($user_subscription)
    {
        //dd($user_subscription);die;
        $for_patner_percentage = 81;
        //$user_payable_amount = $user_subscription->total_payable_amount;
        $user_payable_amount = $user_subscription->amount;
        $total_visit_pass = $user_subscription->visit_pass;
        if ($total_visit_pass != 'month') {
            $for_final_distribute_amount = ($for_patner_percentage / 100) * $user_payable_amount;
            $amount_for_patner = ($for_final_distribute_amount / $total_visit_pass);
            $final_amount_for_gym_patner = number_format((float)$amount_for_patner, 2, '.', '');
        } else {
            $visit = Uservisitedgym::select('id')->where('usersubscription_id', $user_subscription->id)->get();
            $total_visit_pass = $visit->count();
            if ($total_visit_pass > 0) {
                $total_visit_pass = ($total_visit_pass + 1);
                $for_final_distribute_amount = ($for_patner_percentage / 100) * $user_payable_amount;
                $amount_for_patner = ($for_final_distribute_amount / $total_visit_pass);
            } else {
                $total_visit_pass = 1;
                $for_final_distribute_amount = ($for_patner_percentage / 100) * $user_payable_amount;
                $amount_for_patner = ($for_final_distribute_amount / $total_visit_pass);
            }
            $final_amount_for_gym_patner = number_format((float)$amount_for_patner, 2, '.', '');
        }

        return $final_amount_for_gym_patner;
    }

}
