<?php

namespace App\Http\Controllers\API;

use App\Mail\SendUserTransactionMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\ApiCommanFunctionController;
use Validator;
use DB;
use App\Jazz\jazzcash;
use App\API\Subscription;
use App\API\Usersubscription;
use App\API\Plancategory;
use App\Usercard;
use App\Userusecoupon;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;


class PaymentController extends Controller
{

    function __construct()
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $this->form = $data;
        $this->return = new ApiCommanFunctionController;
        $this->plan_category = new Plancategory;
        $this->user_sub = new Usersubscription;
    }

    public function save_payment()
    {
        if (isset($this->form->client_token)) {
            $input['user_id'] = trim($this->form->user_id);
            $input['plan_id'] = trim($this->form->plan_id);
            $input['client_token'] = trim($this->form->client_token);
            $input['payable_amount'] = trim($this->form->payable_amount);
            $input['plan_description'] = trim($this->form->plan_description);
            $input['card_number'] = trim($this->form->card_number);
            $input['exp_month'] = trim($this->form->exp_month);
            $input['exp_year'] = trim($this->form->exp_year);
            $input['gst'] = trim($this->form->gst);
            //28-11-2019
            $input['coupon_id'] = trim($this->form->coupon_id);
            $input['coupon_code'] = trim($this->form->coupon_code);
            $input['counpon_discount'] = trim($this->form->counpon_discount);//end

            $rules = [
                'plan_id' => 'required',
                'user_id' => 'required',
                'client_token' => 'required',
                'payable_amount' => 'required',
                'card_number' => 'required',
                'exp_month' => 'required',
                'exp_year' => 'required',
            ];
            $message = [
                'user_id.required' => 'User ID is required',
                'plan_id.required' => 'Plan ID is required',
                'client_token.required' => 'Client Token is required',
                'payable_amount.required' => 'Amount is required',
                'card_number.required' => 'Card number is required',
                'exp_month.required' => 'Expiry month is required',
                'exp_year.required' => 'Expiry year is required',
            ];
            $validator = Validator::make($input, $rules, $message);
            if ($validator->fails()) {
                return $this->return->sendError_obj($validator->errors()->first());
            }

            $user = User::select('email')->where('id', $input['user_id'])->first();
            $user_email = '';
            $user_name = '';
            if (!empty($user)) {
                $user_email = $user->email;
                $user_name = $user->first_name . $user->last_name;

            }
            $date = date('Y-m-d H:i:s');
            $charge_id = $input['client_token'] ? $input['client_token'] : '';
            $currency = 'PKR';
            //$charge_object = $charge->object ? $charge->object : '';
            //$charge_amount = $charge->amount ? $charge->amount : '';
            $balance_transaction_id = $input['client_token'] ? $input['client_token'] : '';
            $balance_created = $date ? $date : '';

            $balance_currency = $currency ? $currency : '';
            $balance_description = 'Gym Passport pass fee';
            $payment_method = 'Foree';

            $get_subscription_data = Subscription::with('plan_category')->whereid($input['plan_id'])->first();
            //User Subscription Data
            $insert['user_id'] = $input['user_id'];
            $insert['subscription_id'] = $input['plan_id'];
            $insert['customer_id'] = $input['user_id']; //customer id
            $insert['transaction_id'] = $balance_transaction_id;
            $insert['purchased_at'] = $balance_created;
            $insert['plan_name'] = $get_subscription_data->plan_name;
            $insert['visit_pass'] = $get_subscription_data->visit_pass;
            $insert['plan_duration'] = $get_subscription_data->plan_duration;
            $insert['plan_time'] = $get_subscription_data->plan_time;
            $insert['amount'] = $get_subscription_data->amount;
            $insert['description'] = $input['plan_description']; //$get_subscription_data->description;
            $insert['payment_method'] = $payment_method;
            $insert['status'] = 1;
            $insert['currency'] = $balance_currency;
            $insert['charge_id'] = $charge_id;
            $insert['integrated_gst'] = $input['gst'];
            //add 23-12-2019
            $insert['failure_code'] = '';
            $insert['failure_message'] = '';
            //end 23-12-2019
            $insert['total_payable_amount'] = $input['payable_amount'];
            $plan_duration = $insert['plan_duration'];
            $plan_time = $insert['plan_time'];
            $insert['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($insert['purchased_at'])));
            // User Card Data
            $card['card_brand'] = 'Foree';
            $card['card_country'] = 'PAK';
            $card['card_number'] = $input['card_number'];
            $card['card_exp_month'] = $input['exp_month'];
            $card['card_exp_year'] = $input['exp_year'];
            $card['user_card_brand'] = $card['card_brand'];
            $card['user_card_number'] = $input['card_number'];
            $card['user_card_exp_month'] = $input['exp_month'];
            $card['user_card_exp_year'] = $input['exp_year'];
            $card['status'] = 1;
            $card['user_id'] = $input['user_id'];
            $status['status'] = '0';
            $update = Usersubscription::where('user_id', $input['user_id'])->update($status);
            $add_payment = Usersubscription::create($insert);
            if (isset($add_payment->id) && $add_payment->id > 0) {
                $card['usersubscription_id'] = $add_payment->id;
                $add_card = Usercard::create($card);
                //28-11-2019 for coupon code
                if (!empty($input['coupon_id']) && $input['coupon_code']) {
                    $coupon['usersubscription_id'] = $add_payment->id;
                    $coupon['coupon_id'] = $input['coupon_id'];
                    $coupon['coupon_code'] = $input['coupon_code'];
                    $coupon['counpon_discount'] = $input['counpon_discount'];
                    $coupon['user_id'] = $input['user_id'];
                    Userusecoupon::create($coupon);
                }

                $res['user_id'] = $insert['user_id'];
                $res['expired_at'] = date('d/m/Y H:i:s', strtotime($insert['expired_at']));
                $is_sub_val = (boolean)1;
                $msg = "Your payment has been successfully done";
                if ($user_email) {

                    $message = ['user_name' => $user_name, 'email' => $user_email, 'amount' => $input['payable_amount'], 'package_name' => $get_subscription_data->plan_category->plan_cat_name];

                    \Mail::to($user_email)->send(new SendUserTransactionMail($message));
                }
                //$msg = 'Your payment for the '.$get_subscription_data->visit_pass.' visit pass has been received. You are now subscribed to the '.$get_subscription_data->plan_category->plan_cat_name.' plan.';
                return $this->return->json_sendResponse(1, $res, $msg, $is_sub_val);

            } else {
                return json_encode([
                    'error' => 'Something went wrong payment saved failed'
                ]);
            }

        }
    }

    public function save_payment_without_card()
    { // for 100% free subscription
        if (isset($this->form->client_token)) {
            $input['user_id'] = trim($this->form->user_id);
            $input['plan_id'] = trim($this->form->plan_id);
            $input['client_token'] = trim($this->form->client_token);
            $input['payable_amount'] = trim($this->form->payable_amount);
            $input['plan_description'] = trim($this->form->plan_description);
            // $input['card_number'] = trim($this->form->card_number);
            // $input['exp_month'] = trim($this->form->exp_month);
            // $input['exp_year'] = trim($this->form->exp_year);
            $input['gst'] = trim($this->form->gst);
            //28-11-2019
            $input['coupon_id'] = trim($this->form->coupon_id);
            $input['coupon_code'] = trim($this->form->coupon_code);
            $input['counpon_discount'] = trim($this->form->counpon_discount);//end

            $rules = [
                'plan_id' => 'required',
                'user_id' => 'required',
                'client_token' => 'required',
                'payable_amount' => 'required',
            ];
            $message = [
                'user_id.required' => 'User ID is required',
                'plan_id.required' => 'Plan ID is required',
                'client_token.required' => 'Client Token is required',
                'payable_amount.required' => 'Amount is required',
            ];
            $validator = Validator::make($input, $rules, $message);
            if ($validator->fails()) {
                return $this->return->sendError_obj($validator->errors()->first());
            }
            $user = User::select('email')->where('id', $input['user_id'])->first();
            $user_email = '';
            $user_name = '';
            if (!empty($user)) {
                $user_email = $user->email;
                $user_name = $user->first_name . $user->last_name;

            }
            $date = date('Y-m-d H:i:s');
            $charge_id = $input['client_token'] ? $input['client_token'] : '';
            $currency = 'PKR';
            //$charge_object = $charge->object ? $charge->object : '';
            //$charge_amount = $charge->amount ? $charge->amount : '';
            $balance_transaction_id = $input['client_token'] ? $input['client_token'] : '';
            $balance_created = $date ? $date : '';

            $balance_currency = $currency ? $currency : '';
            $balance_description = 'Gym Passport pass fee';
            $payment_method = 'No Payment Method';

            $get_subscription_data = Subscription::with('plan_category')->whereid($input['plan_id'])->first();
            //User Subscription Data
            $insert['user_id'] = $input['user_id'];
            $insert['subscription_id'] = $input['plan_id'];
            $insert['customer_id'] = $input['user_id']; //customer id
            $insert['transaction_id'] = $balance_transaction_id;
            $insert['purchased_at'] = $balance_created;
            $insert['plan_name'] = $get_subscription_data->plan_name;
            $insert['visit_pass'] = $get_subscription_data->visit_pass;
            $insert['plan_duration'] = $get_subscription_data->plan_duration;
            $insert['plan_time'] = $get_subscription_data->plan_time;
            $insert['amount'] = $get_subscription_data->amount;
            $insert['description'] = $input['plan_description']; //$get_subscription_data->description;
            $insert['payment_method'] = $payment_method;
            $insert['status'] = 1;
            $insert['currency'] = $balance_currency;
            $insert['charge_id'] = $charge_id;
            $insert['integrated_gst'] = $input['gst'];
            //add 23-12-2019
            $insert['failure_code'] = '';
            $insert['failure_message'] = '';
            //end 23-12-2019
            $insert['total_payable_amount'] = $input['payable_amount'];
            $plan_duration = $insert['plan_duration'];
            $plan_time = $insert['plan_time'];
            $insert['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($insert['purchased_at'])));
            // User Card Data
            // $card['card_brand'] = 'Foree';
            // $card['card_country'] = 'PAK';
            // $card['card_number'] = $input['card_number'];
            // $card['card_exp_month'] = $input['exp_month'];
            // $card['card_exp_year'] = $input['exp_year'];
            // $card['user_card_brand'] = $card['card_brand'];
            // $card['user_card_number'] = $input['card_number'];
            // $card['user_card_exp_month'] = $input['exp_month'];
            // $card['user_card_exp_year'] = $input['exp_year'];
            // $card['status'] = 1;
            // $card['user_id'] = $input['user_id'];
            $status['status'] = '0';
            $update = Usersubscription::where('user_id', $input['user_id'])->update($status);
            $add_payment = Usersubscription::create($insert);
            if (isset($add_payment->id) && $add_payment->id > 0) {
                // $card['usersubscription_id'] = $add_payment->id;
                // $add_card = Usercard::create($card);
                //28-11-2019 for coupon code
                if (!empty($input['coupon_id']) && $input['coupon_code']) {
                    $coupon['usersubscription_id'] = $add_payment->id;
                    $coupon['coupon_id'] = $input['coupon_id'];
                    $coupon['coupon_code'] = $input['coupon_code'];
                    $coupon['counpon_discount'] = $input['counpon_discount'];
                    $coupon['user_id'] = $input['user_id'];
                    Userusecoupon::create($coupon);
                }

                $res['user_id'] = $insert['user_id'];
                $res['expired_at'] = date('d/m/Y H:i:s', strtotime($insert['expired_at']));
                $is_sub_val = (boolean)1;
                $msg = "Your payment has been successfully done";
                if ($user_email) {

                    $message = ['user_name' => $user_name, 'email' => $user_email, 'amount' => $input['payable_amount'], 'package_name' => $get_subscription_data->plan_category->plan_cat_name];

                    \Mail::to($user_email)->send(new SendUserTransactionMail($message));
                }
                //$msg = 'Your payment for the '.$get_subscription_data->visit_pass.' visit pass has been received. You are now subscribed to the '.$get_subscription_data->plan_category->plan_cat_name.' plan.';
                return $this->return->json_sendResponse(1, $res, $msg, $is_sub_val);

            } else {
                return json_encode([
                    'error' => 'Something went wrong payment saved failed'
                ]);
            }

        }
    }

    public function enableTrail()
    {
        $input['user_id'] = trim($this->form->user_id);
        $input['plan_id'] = 4;

        $date = date('Y-m-d H:i:s');
        $charge_id = '4';
        $currency = 'PKR';
        $balance_transaction_id = '4';
        $balance_created = $date ? $date : '';
        $balance_currency = $currency ? $currency : '';
        $balance_description = 'Gym Passport Trail';
        $payment_method = 'Trail';

        $get_subscription_data = Subscription::with('plan_category')->whereid($input['plan_id'])->first();
        //User Subscription Data
        $insert['user_id'] = $input['user_id'];
        $insert['subscription_id'] = $input['plan_id'];
        $insert['customer_id'] = $input['user_id']; //customer id
        $insert['transaction_id'] = $balance_transaction_id;
        $insert['purchased_at'] = $balance_created;
        $insert['plan_name'] = $get_subscription_data->plan_name;
        $insert['visit_pass'] = $get_subscription_data->visit_pass;
        $insert['plan_duration'] = $get_subscription_data->plan_duration;
        $insert['plan_time'] = $get_subscription_data->plan_time;
        $insert['amount'] = $get_subscription_data->amount;
        $insert['description'] = 'Gym Passport Trail Pass'; //$get_subscription_data->description;
        $insert['payment_method'] = $payment_method;
        $insert['status'] = 1;
        $insert['currency'] = $balance_currency;
        $insert['charge_id'] = $charge_id;
        $insert['integrated_gst'] = '';
        //add 23-12-2019
        $insert['failure_code'] = '';
        $insert['failure_message'] = '';
        //end 23-12-2019
        $insert['total_payable_amount'] = "00.00";
        $plan_duration = $insert['plan_duration'];
        $plan_time = $insert['plan_time'];
        $insert['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($insert['purchased_at'])));
        // User Card Data
        $card['card_brand'] = 'Trail';
        $card['card_country'] = 'PAK';
        $card['card_number'] = '';
        $card['card_exp_month'] = '';
        $card['card_exp_year'] = '';
        $card['user_card_brand'] = '';
        $card['user_card_number'] = '';
        $card['user_card_exp_month'] = '';
        $card['user_card_exp_year'] = '';
        $card['status'] = 1;
        $card['user_id'] = $input['user_id'];
        $status['status'] = '0';

        $update = Usersubscription::where('user_id', $input['user_id'])->update($status);
        $data['partner_first_login'] = '1';
        $updated = User::whereid($input['user_id'])->update($data);
        $add_payment = Usersubscription::create($insert);
        if (isset($add_payment->id) && $add_payment->id > 0) {
            $card['usersubscription_id'] = $add_payment->id;


            $res['user_id'] = $insert['user_id'];
            $res['expired_at'] = date('d/m/Y H:i:s', strtotime($insert['expired_at']));
            $is_sub_val = (boolean)1;
            $msg = "Your free trail is now active.";
            //$msg = 'Your payment for the '.$get_subscription_data->visit_pass.' visit pass has been received. You are now subscribed to the '.$get_subscription_data->plan_category->plan_cat_name.' plan.';
            return $this->return->json_sendResponse(1, $res, $msg, $is_sub_val);

        } else {
            return json_encode([
                'error' => 'Something went wrong payment saved failed'
            ]);
        }

    }

    public function save_payment_jazzcash()
    {
        $input['user_id'] = trim($this->form->user_id);
        $input['plan_id'] = trim($this->form->plan_id);
        $input['client_token'] = trim($this->form->client_token);
        $input['payable_amount'] = trim($this->form->payable_amount);
        $input['plan_description'] = trim($this->form->plan_description);
        $input['phone_number'] = trim($this->form->client_token);
        $input['gst'] = trim($this->form->gst);
        //28-11-2019
        $input['coupon_id'] = trim($this->form->coupon_id);
        $input['coupon_code'] = trim($this->form->coupon_code);
        $input['counpon_discount'] = trim($this->form->counpon_discount);//end
        $rules = [
            'plan_id' => 'required',
            'user_id' => 'required',
            'payable_amount' => 'required',
            'phone_number' => 'required',
        ];
        $message = [
            'user_id.required' => 'User ID is required',
            'plan_id.required' => 'Plan ID is required',
            'client_token.required' => 'Client Token is required',
            'payable_amount.required' => 'Amount is required',
            'phone_number.required' => 'Phone number is required',
        ];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            return $this->return->sendError_obj($validator->errors()->first());
        }
        $user = User::select('email')->where('id', $input['user_id'])->first();
        $user_email = '';
        if (!empty($user)) {
            $user_email = $user->email;
            $user_name = $user->first_name . ' ' . $user->last_name;
        }

        //echo $user_email; die;


        $jc = new Jazzcash();
        $jc->reqType = 'WALLET';
        $jc->set_data([
            'pp_Version' => "1.1",
            'pp_TxnType' => "MWALLET",
            "pp_Language" => "EN",
            'pp_MerchantID' => "MC12576",
            "pp_SubMerchantID" => "",
            'pp_Password' => "sh1gh20xwa",
            "pp_BankID" => "",
            "pp_ProductID" => "",
            'pp_TxnRefNo' => 'T' . 'x' . rand(1111111111, 9999999999),
            'pp_Amount' => $input['payable_amount'] * 100,
            'pp_TxnCurrency' => "PKR",
            'pp_TxnDateTime' => date('YmdHis'),
            'pp_BillReference' => "billRef",
            'pp_Description' => "Payment to gympassport pass",
            'pp_TxnExpiryDateTime' => date('YmdHis', strtotime(date('YmdHis') . ' + 2 days')),
            "pp_ReturnURL" => "https://gympassport.pk/",
            'pp_SecureHash' => '',
            "ppmpf_1" => $input['phone_number'],
            "ppmpf_2" => "",
            "ppmpf_3" => "",
            "ppmpf_4" => "",
            "ppmpf_5" => ""
        ]);
        $result = $jc->send();
        $map_data = json_decode($result, true);
        //print_r($map_data);
        //return;
        //return $map_data;
        if ($map_data['pp_ResponseCode'] == 000) {
            $charge_id = $map_data['pp_TxnRefNo'] ? $map_data['pp_TxnRefNo'] : '';
            $charge_amount = $map_data['pp_Amount'] ? $map_data['pp_Amount'] : '';
            $balance_transaction_id = $map_data['pp_TxnRefNo'] ? $map_data['pp_TxnRefNo'] : '';
            $balance_created = $map_data['pp_TxnDateTime'] ? $map_data['pp_TxnDateTime'] : '';
            $balance_currency = $map_data['pp_TxnCurrency'] ? $map_data['pp_TxnCurrency'] : '';
            $balance_description = "payment with jazzcash";
            $payment_method = $map_data['pp_TxnType'] ? $map_data['pp_TxnType'] : '';
            // add 23-12-2019
            //$failure_code = $charge->failure_code ? $charge->failure_code : '';
            //$failure_message = $charge->failure_message ? $charge->failure_message : '';
            // end 23-12-2019
            $get_subscription_data = Subscription::with('plan_category')->whereid($input['plan_id'])->first();
            //User Subscription Data
            $insert['user_id'] = $input['user_id'];
            $insert['subscription_id'] = $input['plan_id'];
            $insert['customer_id'] = $input['user_id']; //customer id
            $insert['transaction_id'] = $balance_transaction_id;
            $insert['purchased_at'] = date('Y-m-d H:i:s', strtotime($balance_created));
            $insert['plan_name'] = $get_subscription_data->plan_name;
            $insert['visit_pass'] = $get_subscription_data->visit_pass;
            $insert['plan_duration'] = $get_subscription_data->plan_duration;
            $insert['plan_time'] = $get_subscription_data->plan_time;
            $insert['amount'] = $get_subscription_data->amount;
            $insert['description'] = $input['plan_description']; //$get_subscription_data->description;
            $insert['payment_method'] = $payment_method;
            $insert['status'] = ($map_data['pp_ResponseCode'] == 000) ? 1 : 0;
            $insert['currency'] = $balance_currency;
            $insert['charge_id'] = $charge_id;
            $insert['integrated_gst'] = $input['gst'];
            //add 23-12-2019
            $insert['failure_code'] = '';
            $insert['failure_message'] = '';
            //end 23-12-2019
            $insert['total_payable_amount'] = $input['payable_amount'];
            $plan_duration = $insert['plan_duration'];
            $plan_time = $insert['plan_time'];
            $insert['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($insert['purchased_at'])));
            // User Card Data
            $card['card_brand'] = "jazzcash";
            $card['card_country'] = 'pk';
            $card['card_number'] = "4242";
            $card['card_exp_month'] = "06";
            $card['card_exp_year'] = "22";
            $card['user_card_brand'] = "jazzcash";
            $card['user_card_number'] = "4242";
            $card['user_card_exp_month'] = "06";
            $card['user_card_exp_year'] = "22";
            $card['code'] = "123";

            $card['status'] = 1;
            $card['user_id'] = $input['user_id'];
            $status['status'] = '0';
            $update = Usersubscription::where('user_id', $input['user_id'])->update($status);
            $data['partner_first_login'] = '1';
            $updated = User::whereid($input['user_id'])->update($data);
            $add_payment = Usersubscription::create($insert);
            if (isset($add_payment->id) && $add_payment->id > 0) {
                $card['usersubscription_id'] = $add_payment->id;
                $add_card = Usercard::create($card);
                //28-11-2019 for coupon code
                if (!empty($input['coupon_id']) && $input['coupon_code']) {
                    $coupon['usersubscription_id'] = $add_payment->id;
                    $coupon['coupon_id'] = $input['coupon_id'];
                    $coupon['coupon_code'] = $input['coupon_code'];
                    $coupon['counpon_discount'] = $input['counpon_discount'];
                    $coupon['user_id'] = $input['user_id'];
                    Userusecoupon::create($coupon);
                }
                //end coupon code
                if ($map_data['pp_ResponseCode'] != 000) {
                    $allData = (object)[];
                    $message = $map_data['pp_ResponseMessage'];
                    return $this->return->sendResponse(0, $allData, $message);
                } else {

                    $res['user_id'] = $insert['user_id'];
                    $res['expired_at'] = date('d/m/Y H:i:s', strtotime($insert['expired_at']));
                    $is_sub_val = (boolean)1;
                    $msg = "Your payment has been successfully done";
                    if ($user_email) {

                        $message = ['user_name' => $user_name, 'email' => $user_email, 'amount' => $input['payable_amount'], 'package_name' => $get_subscription_data->plan_category->plan_cat_name];

                        \Mail::to($user_email)->send(new SendUserTransactionMail($message));
                    }
                    //$msg = 'Your payment for the '.$get_subscription_data->visit_pass.' visit pass has been received. You are now subscribed to the '.$get_subscription_data->plan_category->plan_cat_name.' plan.';
                    return $this->return->json_sendResponse(1, $res, $msg, $is_sub_val);
                }
            }
        } else if ($map_data['pp_ResponseCode'] == 404) {
            $allData = (object)[];
            return $this->return->json_sendResponse(0, $allData, $map_data['pp_ResponseMessage'], true);
        } else if ($map_data['pp_ResponseCode'] == 999) {
            $allData = (object)[];
            return $this->return->json_sendResponse(0, $allData, "Insufficient balance", true);
        } else {
            $allData = (object)[];
            return $this->return->json_sendResponse(0, $allData, 'Something went wrong payment saved failed', true);
        }

    }

    public function payfast(Request $request)
    {
        $data['merchant_id'] = 3827;
        $data['secured_key'] = 'Qd5H2DVBQSShrNddYL77lH';
        $data['basket_id'] = $request->user_id . '-' . time();
        $data['trans_amount'] = $request->amount;
        $data['user_id'] = $request->user_id;
        $data['email'] = $request->email;
        $data['mobile'] = $request->mobile;
        $data['subscription_id'] = $request->subscription_id;
        $data['plan_id'] = $request->plan_id;
        $data['gst'] = $request->gst;
        if($request->coupon_id){
            $deductedAmount = $request->amount * ($request->counpon_discount / 100);
            $data['trans_amount'] = $request->amount - $deductedAmount;
        }
        $data['coupon_id'] = $request->coupon_id;
        $data['coupon_code'] = $request->coupon_code;
        $data['counpon_discount'] = $request->counpon_discount;
        $data['token'] = $this->getAccessToken($data['merchant_id'], $data['secured_key'], $data['basket_id'], $data['trans_amount']);
        $user = User::find($request->user_id);
        if ($user) {

            $get_subscription_data = Subscription::with('plan_category')->whereid($request->plan_id)->first();

            $insert['user_id'] = $user->id;
            $insert['subscription_id'] = $request->plan_id;
            $insert['customer_id'] = $user->id;
            $insert['transaction_id'] = '';
            $insert['purchased_at'] = date('Y-m-d H:i:s');
            $insert['plan_name'] = $get_subscription_data->plan_name;
            $insert['visit_pass'] = $get_subscription_data->visit_pass;
            $insert['plan_duration'] = $get_subscription_data->plan_duration;
            $insert['plan_time'] = $get_subscription_data->plan_time;
            $insert['amount'] = $get_subscription_data->amount;
            $insert['description'] = $get_subscription_data->plan_description;
            $insert['payment_method'] = 'Payfast';
            $insert['status'] = 3;
            $insert['currency'] = 'PKR';
            $insert['charge_id'] = null;
            $insert['integrated_gst'] = null;
            $insert['total_payable_amount'] = $data['trans_amount'];
            $insert['basket_id'] = $data['basket_id'];

            $plan_time = $insert['plan_time'];
            $plan_duration = $insert['plan_duration'];
            $insert['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($insert['purchased_at'])));

            $user_subscription = Usersubscription::create($insert);
            $data['user_subscription_id'] = $user_subscription->id;
            return view('payfast' , $data);
        } else {
            return response()->json(['status' => 0, 'message' => 'user does not exits']);
        }
    }

    function getAccessToken($merchant_id, $secured_key, $basket_id, $trans_amount)
    {
        $tokenApiUrl = 'https://ipg1.apps.net.pk/Ecommerce/api/Transaction/GetAccessToken';

        $urlPostParams = sprintf(
            'MERCHANT_ID=%s&SECURED_KEY=%s&TXNAMT=%s&BASKET_ID=%s',
            $merchant_id,
            $secured_key,
            $trans_amount,
            $basket_id
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $tokenApiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $urlPostParams);
        curl_setopt($ch, CURLOPT_USERAGENT, 'CURL/PHP PayFast Example');
        $response = curl_exec($ch);
        curl_close($ch);
        $payload = json_decode($response);
        $token = isset($payload->ACCESS_TOKEN) ? $payload->ACCESS_TOKEN : '';

        return $token;
    }

    public function payfastprocessed()
    {
        try {
            header('HTTP/1.0 200 OK');
            flush();
            $input = $_GET;
            $err_code = $_GET['err_code'];
            $err_msg = $_GET['err_msg'];
            $transaction_id = $_GET['transaction_id'];
            $basket_id = $_GET['basket_id'];
            $order_date = $_GET['order_date'];
            $Rdv_Message_Key = $_GET['Rdv_Message_Key'];
            $PaymentType = $_GET['PaymentType'];
            $PaymentName = $_GET['PaymentName'];
            $validation_hash = $_GET['validation_hash'];
            $transaction_amount = $_GET['transaction_amount'];
            $merchant_amount = $_GET['merchant_amount'];
            $discounted_amount = $_GET['discounted_amount'];
            $issuer_name = $_GET['issuer_name'];
            $transaction_currency = $_GET['transaction_currency'];
            /**
             * following parameters sent from PayFast after success/failed transaction
             *
             */
            // Notify PayFast that information has been received - this is required

            $trans_id = $_GET['transaction_id'];
            $err_code = $_GET['err_code'];
            $err_msg = $_GET['err_msg'];
            $basket_id = $_GET['basket_id'];
            $order_date = $_GET['order_date'];
            $response_key = $_GET['Response_Key'];
            $payment_name = $_GET['PaymentName'];
            $secretword = ''; // No secret code defined for merchant id 102,  secret code can be entered in merchant portal.
            $response_string = sprintf("%s%s%s%s%s", $_GET['merchant_id'], $basket_id, $secretword, $transaction_amount, $err_code);
            $response_hash = hash('MD5', $response_string);
            $user_new_subscription = Usersubscription::where('user_id' , $input['user_id'])->orderBy('id' , 'desc')->first();
            $user_new_subscription->charge_id = $transaction_id;
            $user_new_subscription->transaction_id = $transaction_id;
            $user_new_subscription->payment_status = $err_code;
            $user_new_subscription->save();

            if (strtolower($response_hash) != strtolower($response_key)) {
                return response()->json("Transaction could not be varified");

            }
            if ($err_code == '000' || $err_code == '00') {

                $get_subscription_data = Subscription::with('plan_category')->whereid($input['plan_id'])->first();

                // User Subscription Data
                // $insert['user_id'] = $input['user_id'];
                // $insert['subscription_id'] = $input['plan_id'];
                // $insert['customer_id'] = $input['user_id']; //customer id
                // $insert['transaction_id'] = $trans_id;
                // $insert['purchased_at'] = date('Y-m-d H:i:s', strtotime($order_date));
                // $insert['plan_name'] = $get_subscription_data->plan_name;
                // $insert['visit_pass'] = $get_subscription_data->visit_pass;
                // $insert['plan_duration'] = $get_subscription_data->plan_duration;
                // $insert['plan_time'] = $get_subscription_data->plan_time;
                // $insert['amount'] = $get_subscription_data->amount;
                // $insert['description'] = $get_subscription_data->plan_description; //$get_subscription_data->description;
                // $insert['payment_method'] = 'Payfast';
                // $insert['status'] = 1;
                // $insert['currency'] = $transaction_currency;
                // $insert['charge_id'] = $transaction_id;
                // $insert['integrated_gst'] = $input['gst'];
                // $insert['total_payable_amount'] = $transaction_amount;
                // $plan_duration = $insert['plan_duration'];
                // $plan_time = $insert['plan_time'];
                // $insert['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($insert['purchased_at'])));
                // add 23-12-2019
                // $insert['failure_code'] = '';
                // $insert['failure_message'] = '';
                $status['status'] = '0';
                $update = Usersubscription::where('user_id', $input['user_id'])->where('status' , '!=' , 3)->update($status);
                $data['partner_first_login'] = '1';
                $updated = User::whereid($input['user_id'])->update($data);

                $user_new_subscription->status = 1;
                $user_new_subscription->charge_id = $transaction_id;
                $user_new_subscription->transaction_id = $trans_id;
                $user_new_subscription->integrated_gst = $input['gst'];
                $user_new_subscription->failure_code = '';
                $user_new_subscription->failure_message = '';
                $user_new_subscription->save();

                //end 23-12-2019
                if ($user_new_subscription) {
                    // $card['usersubscription_id'] = $add_payment->id;
                    // $add_card = Usercard::create($card);
                    //28-11-2019 for coupon code
                    if (!empty($input['coupon_id']) && $input['coupon_code']) {
                        $coupon['usersubscription_id'] = $user_new_subscription->id;
                        $coupon['coupon_id'] = $input['coupon_id'];
                        $coupon['coupon_code'] = $input['coupon_code'];
                        $coupon['counpon_discount'] = $input['counpon_discount'];
                        $coupon['user_id'] = $input['user_id'];
                        Userusecoupon::create($coupon);
                    }

                    $res['user_id'] = $input['user_id'];
                    $res['expired_at'] = date('d/m/Y H:i:s', strtotime($user_new_subscription->expired_at));
                    $is_sub_val = (boolean)1;
                    $msg = "Your payment has been successfully done";
                    $user = User::select('email')->where('id', $input['user_id'])->first();
                    $user_email = '';
                    $user_name = '';
                    if (!empty($user)) {
                        $user_email = $user->email;
                        $user_name = $user->first_name . $user->last_name;

                    }
                    if ($user_email) {

                        $message = ['user_name' => $user_name, 'email' => $user_email, 'amount' => $transaction_amount, 'package_name' => $get_subscription_data->plan_category->plan_cat_name];

                        \Mail::to($user_email)->send(new SendUserTransactionMail($message));
                    }
                    //$msg = 'Your payment for the '.$get_subscription_data->visit_pass.' visit pass has been received. You are now subscribed to the '.$get_subscription_data->plan_category->plan_cat_name.' plan.';
                    $data = ['status'=>1,'response'=> $res,'message'=> $msg,'is_subscribed'=> $is_sub_val];
                    return view('payFastResponse' , $data);
                    // return response()->json(['status'=>1,'response'=> $res,'message'=> $msg,'is_subscribed'=> $is_sub_val]);

                } else {
                    $message = 'Something went wrong payment saved failed';
                    return view('payFastResponse' , compact('message'));
                }

                $message = 'Transaction Successfully Completed.';
                return view('payFastResponse' , compact('message'));
//                return response()->json(array('message' => "Transaction Successfully Completed. ", 'TransactionID' => $trans_id, 'Date:' => $order_date));
            }
            return response()->json(array('message' => $err_msg));

        } catch (\Exception $e) {
            $user_new_subscription = Usersubscription::where('user_id' , $input['user_id'])->orderBy('id' , 'desc')->first();
            $user_new_subscription->failure_message = $e->getMessage();
            $user_new_subscription->payment_status = $e->getCode();
            $user_new_subscription->save();
            return response()->json(array('message' => "Something went wrong"));
        }
    }

    public function payFastFail(){
        Usersubscription::where('user_id', request()->user_id)->where('status' , '!=' , 3)->update([
            'status' => 0
        ]);

        $user_new_subscription = Usersubscription::where('user_id' , request()->user_id)->orderBy('id' , 'desc')->first();
        $user_new_subscription->status = 3;
        $user_new_subscription->charge_id = request()->transaction_id;
        $user_new_subscription->transaction_id = isset(request()->transaction_id) ? request()->transaction_id : '';
        $user_new_subscription->integrated_gst = request()->gst;
        $user_new_subscription->failure_code = request()->err_code;
        $user_new_subscription->failure_message = request()->err_msg;
        $user_new_subscription->save();

        $message = request()->err_msg;
        return view('payFastResponse', compact('user_new_subscription' , 'message'));
    }

    /*    public function save_payment() {
            $input['user_id'] = trim($this->form->user_id);
            $input['plan_id'] = trim($this->form->plan_id);
            $input['client_token'] = trim($this->form->client_token);
            $input['payable_amount'] = trim($this->form->payable_amount);
            $input['plan_description'] = trim($this->form->plan_description);
            $input['card_number'] = trim($this->form->card_number);
            $input['exp_month'] = trim($this->form->exp_month);
            $input['exp_year'] = trim($this->form->exp_year);
            $input['card_csv'] = trim($this->form->csv);
            $input['card_brand'] = trim($this->form->card_brand);
            $input['gst'] = trim($this->form->gst);
            //28-11-2019
            $input['coupon_id'] = trim($this->form->coupon_id);
            $input['coupon_code'] = trim($this->form->coupon_code);
            $input['counpon_discount'] = trim($this->form->counpon_discount);//end
            $rules = [
                'plan_id' => 'required',
                'user_id' => 'required',
                'payable_amount' => 'required',
                'card_number' => 'required',
                'exp_month' => 'required',
                'exp_year' => 'required',
            ];
            $message = [
                'user_id.required' => 'User ID is required',
                'plan_id.required' => 'Plan ID is required',
                'client_token.required' => 'Client Token is required',
                'payable_amount.required' => 'Amount is required',
                'card_number.required' => 'Card number is required',
                'exp_month.required' => 'Expiry month is required',
                'exp_year.required' => 'Expiry year is required',
            ];
            $validator = Validator::make($input, $rules, $message);
            if ($validator->fails()) {
                return $this->return->sendError_obj($validator->errors()->first());
            }
            $user = User::select('email')->where('id',$input['user_id'])->first();
            $user_email = '';
            if(!empty($user)){
                $user_email = $user->email;
            }

            //echo $user_email; die;

            $jc = new Jazzcash();
            $jc->reqType = 'PAY';
            $jc->set_data([
                'pp_Version' => "1.1",
                'pp_InstrToken' => "",
                'pp_TxnType'   => "MPAY",
                'pp_TxnRefNo' =>  'T'. 'x' . rand(1111111111 , 9999999999),
                'pp_MerchantID' => "MC12576",
                'pp_Password' => "sh1gh20xwa",
                'pp_Amount' => $input['payable_amount'] * 100,
                'pp_TxnCurrency' => "PKR",
                'pp_TxnExpiryDateTime' => date('YmdHis' , strtotime(date('YmdHis'). ' + 2 days')),
                'pp_BillReference' => "billRef",
                'pp_Description' => $input['plan_description'],
                'pp_CustomerCardNumber' => $input['card_number'],
                'pp_CustomerCardExpiry' => $input['exp_month'].$input['exp_year'],
                'pp_CustomerCardCvv' => $input['card_csv'],
                'pp_SecureHash' => '',
                'pp_Frequency' => "SINGLE",
                'pp_TxnDateTime' => date('YmdHis')
            ]);
            $result =  $jc->send();
            $map_data = json_decode($result,true);
            if ($map_data['responseCode'] == 000) {
                $charge_id = $map_data['pp_TxnRefNo'] ? $map_data['pp_TxnRefNo'] : '';
                $charge_amount = $map_data['pp_Amount'] ? $map_data['pp_Amount'] : '';
                $balance_transaction_id = $map_data['pp_TxnRefNo'] ? $map_data['pp_TxnRefNo'] : '';
                $balance_created = $map_data['pp_TxnDateTime'] ? $map_data['pp_TxnDateTime'] : '';
                $balance_currency = $map_data['pp_TxnCurrency'] ? $map_data['pp_TxnCurrency'] : '';
                $balance_description = $map_data['pp_Description'] ? $map_data['pp_Description'] : '';
                $payment_method = $map_data['pp_TxnType'] ? $map_data['pp_TxnType'] : '';
                // add 23-12-2019
                //$failure_code = $charge->failure_code ? $charge->failure_code : '';
                //$failure_message = $charge->failure_message ? $charge->failure_message : '';
                // end 23-12-2019
                $get_subscription_data = Subscription::with('plan_category')->whereid($input['plan_id'])->first();
                //User Subscription Data
                $insert['user_id'] = $input['user_id'];
                $insert['subscription_id'] = $input['plan_id'];
                $insert['customer_id'] = $input['user_id']; //customer id
                $insert['transaction_id'] = $balance_transaction_id;
                $insert['purchased_at'] = date('Y-m-d H:i:s', strtotime($balance_created));
                $insert['plan_name'] = $get_subscription_data->plan_name;
                $insert['visit_pass'] = $get_subscription_data->visit_pass;
                $insert['plan_duration'] = $get_subscription_data->plan_duration;
                $insert['plan_time'] = $get_subscription_data->plan_time;
                $insert['amount'] = $get_subscription_data->amount;
                $insert['description'] = $input['plan_description']; //$get_subscription_data->description;
                $insert['payment_method'] = $payment_method;
                $insert['status'] = ($map_data['responseCode'] == 000) ? 1 : 0;
                $insert['currency'] = $balance_currency;
                $insert['charge_id'] = $charge_id;
                $insert['integrated_gst'] = $input['gst'];
                //add 23-12-2019
                $insert['failure_code'] = '';
                $insert['failure_message'] = '';
                //end 23-12-2019
                $insert['total_payable_amount'] = $input['payable_amount'];
                $plan_duration = $insert['plan_duration'];
                $plan_time = $insert['plan_time'];
                $insert['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($insert['purchased_at'])));
                // User Card Data
                $card['card_brand'] = $input['card_brand'];
                $card['card_country'] = 'pk';
                $card['card_number'] = substr($input['card_number'], 12);
                $card['card_exp_month'] = Crypt::encryptString($input['exp_month']);
                $card['card_exp_year'] = Crypt::encryptString($input['exp_year']);
                $card['user_card_brand'] = $card['card_brand'];
                $card['user_card_number'] = Crypt::encryptString($input['card_number']);
                $card['user_card_exp_month'] = Crypt::encryptString($input['exp_month']);  ;
                $card['user_card_exp_year'] = Crypt::encryptString($input['exp_year']);
                $card['code'] = Crypt::encryptString($input['card_csv']);

                $card['status'] = 1;
                $card['user_id'] = $input['user_id'];
                $status['status'] = '0';
                $update = Usersubscription::where('user_id', $input['user_id'])->update($status);
                $add_payment = Usersubscription::create($insert);
                if (isset($add_payment->id) && $add_payment->id > 0) {
                    $card['usersubscription_id'] = $add_payment->id;
                    $add_card = Usercard::create($card);
                    //28-11-2019 for coupon code
                    if(!empty($input['coupon_id']) && $input['coupon_code']){
                        $coupon['usersubscription_id'] = $add_payment->id;
                        $coupon['coupon_id'] = $input['coupon_id'];
                        $coupon['coupon_code'] = $input['coupon_code'];
                        $coupon['counpon_discount'] = $input['counpon_discount'];
                        $coupon['user_id'] = $input['user_id'];
                        Userusecoupon::create($coupon);
                    }
                    //end coupon code
                    if ($map_data['responseCode'] != 000) {
                        $allData = (object) [];
                        $message = $map_data['responseMessage'];
                        return $this->return->sendResponse(0, $allData, $message);
                    } else {

                        $res['user_id'] = $insert['user_id'];
                        $res['expired_at'] = date('d/m/Y H:i:s', strtotime($insert['expired_at']));
                        $is_sub_val = (boolean) 1;
                        $msg = "Your payment has been successfully done";
                        //$msg = 'Your payment for the '.$get_subscription_data->visit_pass.' visit pass has been received. You are now subscribed to the '.$get_subscription_data->plan_category->plan_cat_name.' plan.';
                        return $this->return->json_sendResponse(1, $res, $msg, $is_sub_val);
                    }
                }
            } else if ($map_data['responseCode'] == 404){
                $allData = (object) [];
                return $this->return->json_sendResponse(0, $allData, $map_data['responseMessage'], true);
            } else {
                $allData = (object) [];
                return $this->return->json_sendResponse(0, $allData, 'Something went wrong payment saved failed', true);
            }

        }*/


}