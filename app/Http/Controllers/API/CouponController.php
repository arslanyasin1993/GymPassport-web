<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Coupon;
use App\Couponplan;
use App\Userusecoupon;
use App\API\Plancategory;
use App\API\Usersubscription;
use Session;
use DB;
use App\Http\Controllers\API\ApiCommanFunctionController;

class CouponController extends Controller {

    public $successStatus = 200;

    function __construct() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $this->form = $data;
        $this->return = new ApiCommanFunctionController;
        $this->user_sub = new Usersubscription;
        $this->coupon = new Coupon;
        $this->couponplan = new Couponplan;
        //date_default_timezone_set("Asia/Kolkata");
    }

    public function coupon() {
        $plan['coupon_code'] = $this->form->coupon_code;
        $rules = [
            'coupon_code' => 'required',
        ];
        $message = [
            'coupon_code.required' => 'coupon code is required',
        ];
        $validator = Validator::make($plan, $rules, $message);
        if ($validator->fails()) {
            return $this->return->sendError_arr($validator->errors()->first());
        }

        //28-11-2019
        $plan_cat_id = $this->form->plan_cat_id;
        $user_id = $this->form->user_id;
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
        //end 
        $coupon_code = trim($this->form->coupon_code);
        $currentDate = date('Y-m-d');
        $is_valid_coupon = Coupon::where('coupon_code', $coupon_code)->where('valid_to', '>', "$currentDate")->where('is_delete', '1')->where('status', '1')->first();
        if ($is_valid_coupon) {
            $coupon_id = $is_valid_coupon->id;
            $is_user_plan_valid = Couponplan::where('coupon_id', $coupon_id)->where('plan_cat_id', $plan_cat_id)->first();
            if ($is_user_plan_valid) {
                if(!empty($is_valid_coupon->corporate_id)){
                    $corporate_users = DB::table('users')->where('id' , $is_valid_coupon->corporate_id)->first(['email']);
                    $user = DB::table('users')->where('id' , $user_id)->first();
                    if(explode('@' , $corporate_users->email)[1] != explode('@' , $user->email)[1]){
                        return $this->return->json_sendResponse(0, [], 'You are not eligible for this coupon.', $is_sub_val);
                    }
                }
                // valid one time use coupon
                if($is_valid_coupon->is_one_time_use_coupon == 1){
                   	$onetimecoupon = Userusecoupon::where('coupon_code', $coupon_code)->get();
                   	if(count($onetimecoupon) > 0){
                        $allData = (object) [];
                        return $this->return->json_sendResponse(0, $allData, 'This coupon has already been used.', $is_sub_val);
                   	}
                }
                if ($is_valid_coupon->valid_for == 1) {
                    $res['coupon_id'] = (string) $is_valid_coupon->id;
                    $res['discount'] = (string) $is_valid_coupon->discount;
                    $res['coupon_code'] = $is_valid_coupon->coupon_code;
                    $res['valid_from'] = date('d-m-Y', strtotime($is_valid_coupon->valid_from));
                    $res['valid_to'] = date('d-m-Y', strtotime($is_valid_coupon->valid_to));
                    $res['description'] = $is_valid_coupon->description;
                    return $this->return->json_sendResponse(1, $res, 'Coupon', $is_sub_val);
                } else {
                    $total_use_coupon = Userusecoupon::where('coupon_code', $coupon_code)->where('user_id', $user_id)->get();
                    $total_num_of_use = $total_use_coupon->count();
                    if ($is_valid_coupon->total_use_coupon > $total_num_of_use) {
                        $res['coupon_id'] = (string) $is_valid_coupon->id;
                        $res['discount'] = (string) $is_valid_coupon->discount;
                        $res['coupon_code'] = $is_valid_coupon->coupon_code;
                        $res['valid_from'] = date('d-m-Y', strtotime($is_valid_coupon->valid_from));
                        $res['valid_to'] = date('d-m-Y', strtotime($is_valid_coupon->valid_to));
                        $res['description'] = $is_valid_coupon->description;
                        return $this->return->json_sendResponse(1, $res, 'Coupon', $is_sub_val);
                    } else {
                        $allData = (object) [];
                        return $this->return->json_sendResponse(0, $allData, "You have already used this coupon", $is_sub_val);
                    }
                }
//                        $already_use_coupon = Userusecoupon::where('coupon_code',$coupon_code)->where('user_id',$user_id)->first();
//                        if(empty($already_use_coupon)){
//                                $res['coupon_id'] = (string)$is_valid_coupon->id;
//                                $res['discount'] = (string)$is_valid_coupon->discount;
//                                $res['coupon_code'] = $is_valid_coupon->coupon_code;
//                                $res['valid_from'] = date('d-m-Y',strtotime($is_valid_coupon->valid_from));
//                                $res['valid_to'] = date('d-m-Y',strtotime($is_valid_coupon->valid_to));
//                                $res['description'] = $is_valid_coupon->description;
//                                return $this->return->json_sendResponse(1, $res, 'Coupon',$is_sub_val);
//                        }else{
//                             $allData = (object)[];
//                             return $this->return->json_sendResponse(0, $allData, "You have already used this coupon",$is_sub_val);
//                        }
            } else {
                $allData = (object) [];
                return $this->return->json_sendResponse(0, $allData, "You are not eligible for this coupon", $is_sub_val);
            }
        } else {
            $allData = (object) [];
            return $this->return->json_sendResponse(0, $allData, 'Invalid coupon code', $is_sub_val);
        }
    }

//    public function coupon(){
//        $plan['plan_id'] = $this->form->plan_cat_id;
//        $plan['page'] = $this->form->page;
//        $rules = [
//            'plan_id' => 'required',
//            'page' => 'required',
//        ];
//        $message = [
//            'plan_id.required' => 'Plan id is required',
//            'page.required' => 'Page id is required',
//        ];
//        $validator = Validator::make($plan, $rules, $message);
//        if ($validator->fails()) {
//            return $this->return->sendError_arr($validator->errors()->first());
//        }
//        
//        //28-11-2019
//            $plan_cat_id = $this->form->plan_cat_id;
//            $user_id = $this->form->user_id; 
//            $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
//            $is_sub_val = ($is_subscribe_data) ? (boolean)1: (boolean)0; //user subscription data
//        //end 
//          $search =$this->form->search;
//          $page =$this->form->page; 
//          $limit = 10;
//          $offset = $page * $limit;
//          $data = $this->coupon->api_coupon($plan_cat_id,$search,$offset,$limit);
//         // echo'<pre>';print_r($data);die;
//          if(!$data->isEmpty()){
//             // $res = [];
//              foreach ($data as $key => $value) {
//                  $already_use_coupon = Userusecoupon::select('coupon_id')->where('user_id',$user_id)->get()->toArray();
//                        $already_use_coupon_id = array_column($already_use_coupon, 'coupon_id');
//                       // echo '<pre>';print_r($already_use_coupon_id);die;
//                        if(!in_array($value->coupon_id, $already_use_coupon_id)){
//                            $res[$key]['coupon_id'] = (string)$value->coupon_id;
//                            $res[$key]['discount'] = (string)$value->discount;
//                            $res[$key]['coupon_code'] = $value->coupon_code;
//                            $res[$key]['valid_from'] = date('d-m-Y',strtotime($value->valid_from));
//                            $res[$key]['valid_to'] = date('d-m-Y',strtotime($value->valid_to));
//                            $res[$key]['description'] = $value->description;
//                            $res[$key]['plan_cat_name'] = $value->plan_cat_name;
//                            //
//                        }
//                           
//                  
//              }
//              $res = array_values($res);
//              return $this->return->json_sendResponse(1, $res, 'Coupon',$is_sub_val);
//          }else{
//               $allData = [];
//               return $this->return->json_sendResponse(0, $allData, 'Coupon unavailable',$is_sub_val);
//          }
//    }
}
