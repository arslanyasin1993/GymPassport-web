<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\API\Subscription;
use App\API\Usersubscription;
use App\API\Plancategory;
use Validator;
use DB;
use App\Usergym;
use App\Uservisitedgym;
use App\Http\Controllers\API\ApiCommanFunctionController;
use App\Autorenewalfaild;
use App\Usercard;
use App\Userusecoupon;
use Illuminate\Support\Facades\Crypt;
use App\CorporateUsers;


class SubscriptionController extends Controller {

    public $successStatus = 200;

    function __construct() { //json_sendResponse
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $this->form = $data;
        $this->return = new ApiCommanFunctionController;
        $this->plan_category = new Plancategory;
        $this->user_sub = new Usersubscription;
        $this->user_gym = new Usergym;
        $this->user_visit_gym = new Uservisitedgym;
    }

    public function getSubscription() {
        //$getdata = Subscription::orderBy('id','ASC')->where('status',1)->get();

        //echo '<pre>';print_r($getdata);die();
        //05-11-2019
        $user_id = $this->form->user_id;
        $is_subscribe_data = $this->user_sub->is_subscribe_trail($user_id);
//        if($is_subscribe_data){
//            $getdata = $this->plan_category->get_subscription_data_flag($is_subscribe_data);
//        }else{
//            $getdata = $this->plan_category->get_subscription_data();
//        }
        $getdata = $this->plan_category->get_subscription_data();
        //$is_sub_val = isset($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
        $is_sub_val=0;
        //end
        if (!empty($getdata)) {
            foreach ($getdata as $key => $value) {
                $cat_id = (string) $value->id;
                $res[$key]['plan_cat_id'] = (string) $value->id;
                $res[$key]['plan_cat_name'] = $value->plan_cat_name;
                if ($value->id == 1) {
//                    $result = DB::table('gymcategories')->where('plan_category_id', $value->id)->get()->count();

                    $result = DB::table('gymcategories')->join('users_gym', 'users_gym.id', '=', 'gymcategories.user_gym_id')
                        ->where(function ($query) {
                            $query->where([['gymcategories.status', 1], ['users_gym.status', 1], ['users_gym.is_delete', 1]])->whereIN('gymcategories.plan_category_id', [1]);
                        })->groupBy('gymcategories.user_gym_id')->get()->count();
                } else if ($value->id == 2) {
//                    $result1 = DB::table('gymcategories')->where('plan_category_id', 1)->get()->count();
//                    $result2 = DB::table('gymcategories')->where('plan_category_id', $value->id)->get()->count();
//                    $result = $result1 + $result2;

                    $result = DB::table('gymcategories')->join('users_gym', 'users_gym.id', '=', 'gymcategories.user_gym_id')
                        ->where(function ($query) {
                            $query->where([['gymcategories.status', 1], ['users_gym.status', 1], ['users_gym.is_delete', 1]])->whereIN('gymcategories.plan_category_id', [1, 2]);
                        })->groupBy('gymcategories.user_gym_id')->get()->count();
                }
                elseif ($value->id == 4) {
//                    $result1 = DB::table('gymcategories')->where('plan_category_id', 1)->get()->count();
//                    $result2 = DB::table('gymcategories')->where('plan_category_id', 2)->get()->count();
//                    $result = $result1 + $result2;
                    $result = DB::table('gymcategories')->join('users_gym', 'users_gym.id', '=', 'gymcategories.user_gym_id')
                        ->where(function ($query) {
                            $query->where([['gymcategories.status', 1], ['users_gym.status', 1], ['users_gym.is_delete', 1]])->whereIN('gymcategories.plan_category_id', [1, 2]);
                        })->groupBy('gymcategories.user_gym_id')->get()->count();

                }else {
//                    $result = DB::table('gymcategories')->where('plan_category_id','<', 4)->get()->count();
                    $result = DB::table('gymcategories')->join('users_gym', 'users_gym.id', '=', 'gymcategories.user_gym_id')
                        ->where(function ($query) {
                            $query->where([['gymcategories.status', 1], ['users_gym.status', 1], ['users_gym.is_delete', 1]])->whereIN('gymcategories.plan_category_id', [1, 2, 3, 4]);
                        })->groupBy('gymcategories.user_gym_id')->get()->count();


                }
                if($value->id == 4){
                    $res[$key]['category_description'] = "Click here to activate " . $value->plan_cat_name . " for access to " . $result . " Gyms. \nFree trial is only valid for Lite and Standard packages.";
                }else {
                    $res[$key]['category_description'] = "Click here to buy " . $value->plan_cat_name . " for access to " . $result . " Gyms.";
                }
                $res[$key]['plan_detail'] = [];
                if (isset($value->plan_detail) && !empty($value->plan_detail)) {
                    foreach ($value->plan_detail as $key1 => $value) {
                        $res[$key]['plan_detail'][$key1]['plan_id'] = (string) $value->id;
                        $visit_duration = '';
                        if($value->visit_pass == 'Monthly') { $visit_duration = 'Single Month';}
                        if($value->visit_pass == '6 Month') { $visit_duration = 'Six Months';}
                        if($value->visit_pass == '3 Month') { $visit_duration = 'Three Months';}
                        $res[$key]['plan_detail'][$key1]['visit_pass'] = $visit_duration;
                        $res[$key]['plan_detail'][$key1]['plan_cat_id'] = $cat_id;
//                        if ($value->visit_pass == 'month') {
//                            $duration = $value->plan_duration . ' ' . $value->plan_time . ' - Unlimited Visitsts';
//                        } else {
//                            $duration = $value->plan_duration . ' ' . $value->plan_time;
//                        }
                        $duration = $value->description;
                        $res[$key]['plan_detail'][$key1]['plan_duration'] = $duration;
                        // $gst= "10";
                        // $percentage = trans('lang_data.gst');
                        // $amount = $value->amount;
                        // $gst = (string)(($percentage / 100) * $amount);
                        // $final_gst = number_format((float) $gst, 2, '.', '');
                        $res[$key]['plan_detail'][$key1]['amount'] = number_format((float) $value->amount, 2, '.', ''); //(string)($value->amount + $final_gst);
                    }
                }

                //$res[$key]['plan_detail'] = array_values($res[$key]['plan_detail']);
            }
            return $this->return->json_sendResponse(1, $res, 'Subscription Plan List', $is_sub_val);
        } else {
            $allData = [];
            return $this->return->json_sendResponse(0, $allData, 'Subscription not available', $is_sub_val);
        }
    }

    public function SubscriptionDetail() {
        $plan['plan_id'] = $this->form->plan_id;
        $rules = [
            'plan_id' => 'required',
        ];
        $message = [
            'plan_id.required' => 'Plan id is required',
        ];
        $validator = Validator::make($plan, $rules, $message);
        if ($validator->fails()) {
            return $this->return->sendError_obj($validator->errors()->first());
        }
        //05-11-2019
        $user_id = $this->form->user_id;
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
        //end
        $getdata = Subscription::whereid($plan['plan_id'])->first();
        if (!empty($getdata)) {
            $res['id'] = (string) $getdata->id;
            $res['plan_name'] = $getdata->plan_name;
            $res['plan_duration'] = (string) $getdata->plan_duration;
            $res['plan_time'] = $getdata->plan_time;
            $res['amount'] = (string) $getdata->amount;
            $res['description'] = $getdata->description;
            return $this->return->json_sendResponse(1, $res, 'Subscription Package Details', $is_sub_val);
        } else {
            $allData = (object) [];
            return $this->return->json_sendResponse(0, $allData, 'Package Details not available', $is_sub_val);
        }
    }

    public function account_over_view() {
        $user_id = $this->form->user_id;
        $getdata = Usersubscription::where('user_id', $user_id)->with('plan_category')->with('user_card')->where('status', 1)->first();
        //echo '<pre>';        print_r($getdata);die;
        //05-11-2019
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
        //end
        if (!empty($getdata)) {
            $res['id'] = (string) $getdata->id;
            $res['plan_name'] = $getdata->plan_category->plan_cat_name;
            $res['message'] = "You are currently using the " . $getdata->plan_duration . " " . $getdata->plan_time . " Subscription Plan";
            $res['purchased_on'] = date('d/m/Y', strtotime($getdata->purchased_at));
            $res['renewal_date'] = date('d/m/Y', strtotime("+$getdata->plan_duration $getdata->plan_time", strtotime($getdata->purchased_at)));
            $res['renewal_charge'] = "10";
            if (empty($getdata->user_card)) {
                $res['card_detail'] = (object) [];
            } else {

                foreach ($getdata->user_card as $key => $value) {
                    $res['card_detail']['card_name'] = $value->user_card_brand ? ucfirst($value->user_card_brand) : '';
                    $res['card_detail']['card_number'] = $value->user_card_number ? (string) $value->user_card_number : '';
                    $res['card_detail']['card_exp_month'] = $value->user_card_exp_month ? (string) $value->user_card_exp_month : '';
                    $res['card_detail']['card_exp_year'] = $value->user_card_exp_year ? (string) $value->user_card_exp_year : '';
                    $res['card_detail']['card_country'] = $value->card_country ? (string) ucfirst($value->card_country) : '';
                }
            }
//            $res['card_detail'] = [];
//            foreach ($getdata->user_card as $key => $value) {
//                $res['card_detail']['card_name'] = $value->card_brand;
//                $res['card_detail']['card_number'] = (string) $value->card_number;
//                $res['card_detail']['card_exp_month'] = (string) $value->card_exp_month;
//                $res['card_detail']['card_exp_year'] = (string) $value->card_exp_year;
//                $res['card_detail']['card_country'] = (string) $value->card_country;
//            }
            return $this->return->json_sendResponse(1, $res, 'Current Subscription Plan', $is_sub_val);
        } else {
            $allData = (object) [];
            return $this->return->json_sendResponse(0, $allData, 'Subscription Details not available', $is_sub_val);
        }
    }

    public function subscription() {
        $user_id = $this->form->user_id;
        $getdata = Usersubscription::with('plan_category')->with('user_card')->where('user_id', $user_id)->where('status', 1)->first();
        $allData = (object) [];
        //05-11-2019
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
        //end
        if (empty($getdata)) {
            return $this->return->json_sendResponse(0, $allData, 'You have not any active plan', $is_sub_val);
        }

        $user_total_visit = $is_sub_val == 1 ? Uservisitedgym::where('user_id', $user_id)->where('Usersubscription_id' , $is_subscribe_data->id)->count() : 0;
        $isCorporateUser = CorporateUsers::where('user_id', $user_id)->first();
        if(!empty($isCorporateUser)){
            $remaining_visit = 'Unlimited'; //date('i',$diff);
        }else{
            if ($getdata->plan_category->id == 4) {
                $remaining_visit = (string)(trans('lang_data.total_trail_pass') - $user_total_visit); //date('i',$diff);
            } else {
                $remaining_visit = (string)(trans('lang_data.total_visit_pass') - $user_total_visit); //date('i',$diff);
            }
        }
        // echo'<pre>';print_r($getdata);die();
        $user_subscription_id = $getdata->id;
        $user_have_visit_pass = $getdata->visit_pass;
        $my_visit = $this->user_sub->my_visit($user_id, $user_subscription_id);
        $home_data = $this->user_visit_gym->homescreendata($user_id, $user_subscription_id);
        $total_visit = $this->user_gym->usertotal_visit($user_id, $user_subscription_id, $user_have_visit_pass);
        $res['id'] = (string) $getdata->id;
        $res['total_gyms'] = Usergym::where('status' , 1)->where('is_delete' , 1)->count();
        $res['plan_name'] = $getdata->plan_category->plan_cat_name;
        $res['visit_pass'] = ($getdata->visit_pass == 'month') ? 'Monthly ∞ Pass' : $getdata->visit_pass . ' Visit Pass';
        $res['purchased_date'] = ($getdata->purchased_at) ? date('d/m/Y', strtotime($getdata->purchased_at)) : '';
        $res['subscription_amount'] = ($getdata->amount) ? $getdata->amount : '';
        $res['expired_date'] = ($getdata->expired_at) ? date('d/m/Y', strtotime($getdata->expired_at)) : '';
        $res['total_visit'] = (string) $total_visit;
        $res['remaining_visit'] = $remaining_visit;
        if (!empty($home_data)) {
            $date = ($home_data[0]->user_check_in) ? $home_data[0]->user_check_in : '';
        } else {
            $date = '';
        }

        $delta_time = strtotime(date('Y-m-d H:i:s')) - strtotime($date);
        $hours = floor($delta_time / 3600);
        $delta_time %= 3600;
        $minutes = floor($delta_time / 60);
        $time_diff = $hours . "." . $minutes;
//        $diff=strtotime(date('Y-m-d H:i:s'))-strtotime($date);//time returns current time in seconds
//        $fullDays    = floor($diff/(60*60*24));
//        $fullHours   = floor(($diff-($fullDays*60*60*24))/(60*60));
//        $fullMinutes = floor(($diff-($fullDays*60*60*24)-($fullHours*60*60))/60);
//        $remaining = $fullHours.'.'.$fullMinutes;
        $res['remaining_hour'] = $time_diff > 24 ? '00:00' : (string) round(24 - $time_diff, 2); //(string)($time_diff > 24 ? 0 : 24-$time_diff);
        if ($date == '') {
            $res['remaining_hour'] = '00:00';
        }
//        if ($user_have_visit_pass == 'month') {
//            $remaining_visit = (string) (30 - $total_visit);
//        } else {
//            $remaining_visit = (string) ($user_have_visit_pass - $total_visit);
//        }
        $res['is_remaining_visit'] = (string) (10 - $total_visit); //$remaining_visit;
        $res['about_plan'] = ($getdata->description) ? $getdata->description : '';
        if (empty($my_visit)) {
            $arr = [];
        } else {
            //echo'<pre>';  print_r($my_visit);die;
            $num = $total_visit; //($getdata->visit_pass == 'month') ? 30 : $getdata->visit_pass;
            foreach ($my_visit as $key => $value) {
                $arr[$key]['gym_name'] = $value->gym_name;
                $arr[$key]['gym_id'] = $value->gym_id;
                $arr[$key]['check_in_date'] = $value->check_in_date; //date('d/m/Y',strtotime($value->check_in_date));
                $arr[$key]['visit_number'] = (string) $num--;
            }
        }

        $res['my_visit'] = $arr;
        if (empty($getdata->user_card)) {
            $res['card_detail'] = (object) [];
        } else {

            foreach ($getdata->user_card as $key => $value) {
                $res['card_detail']['card_name'] = ucfirst($value->user_card_brand);
                $res['card_detail']['card_number'] = $value->user_card_number;
                $res['card_detail']['card_exp_month'] = $value->user_card_exp_month;
                $res['card_detail']['card_exp_year'] = $value->user_card_exp_year;
                $res['card_detail']['card_country'] = (string) ucfirst($value->card_country);
            }
        }
        return $this->return->json_sendResponse(1, $res, 'Current Subscription Plan', $is_sub_val);
//        $getdata = Usersubscription::where('user_id',$user_id)->where('status',1)->first();
//        if(!empty($getdata)){
//                $res['id'] = (string)$getdata->id;
//                $res['message'] = "You are currently using the ".$getdata->plan_duration." ".$getdata->plan_time." Subscription Plan";
//                $day_date = $getdata->plan_duration.' '.$getdata->plan_time;
//                $date = date('d/m/Y', strtotime("+$day_date", strtotime($getdata->purchased_at)));
//                $res['renewal_date'] = $date;//"Your plan will be  automatically renewed on ".$date;
//                $res['renewal_charge'] = ($getdata->amount) ? $getdata->amount :"";
//                $res['description'] = ($getdata->description) ? $getdata->description :"";
//            return $this->return->sendResponse(1, $res, 'Current Subscription Plan');
//        }else{
//           $allData = (object)[];
//           return $this->return->sendResponse(0, $allData, 'Subscription not available');
//        }
    }

    public function CancelSubscription() {
        $user_id = $this->form->user_id;
        $date = date('Y-m-d H:i:s');
        $getdata = Usersubscription::where('user_id', $user_id)->where('status', 1)->update(['status' => 0, 'expired_at' => $date]);
        //05-11-2019
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
        //end
        if ($getdata) {
            $res = (object) [];
            return $this->return->json_sendResponse(1, $res, 'You have successfully cancelled your subscription plan', $is_sub_val);
        } else {
            $allData = (object) [];
            return $this->return->json_sendResponse(0, $allData, 'Something went wrong to cancel subscription', $is_sub_val);
        }
    }

    public function booking_detail() {
        $plan['plan_id'] = $this->form->plan_id;
        $rules = [
            'plan_id' => 'required',
        ];
        $message = [
            'plan_id.required' => 'Plan id is required',
        ];

        $validator = Validator::make($plan, $rules, $message);
        if ($validator->fails()) {
            return $this->return->sendError_obj($validator->errors()->first());
        }
        $getdata = Subscription::whereid($plan['plan_id'])->first();
        //05-11-2019
        $user_id = $this->form->user_id;
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
        //end
        if (!empty($getdata)) {
            $base_amount = (string) $getdata->amount;
            $percentage = trans('lang_data.gst');
            //  $gst = ($percentage / 100) * $base_amount;
            $gst = 0;
            //  $base_amount_cal = ($base_amount - $gst);
            $base_amount_cal = ($base_amount);
            $res['base_amount'] = number_format((float) $base_amount_cal, 2, '.', '');
            $res['gst'] = number_format((float) $gst, 2, '.', '');
            $res['total_payable'] = number_format((float) $base_amount, 2, '.', '');
            // $res['base_amount'] = (string) $getdata->amount;
            // $percentage = trans('lang_data.gst');
            // $amount = $getdata->amount;
            // $gst = (string)(($percentage / 100) * $amount);
            // $res['gst'] = number_format((float) $gst, 2, '.', '');
            // $total = ($getdata->amount + $res['gst']);
            // $total_amt = number_format((float) $total, 2, '.', '');
            // $res['total_payable'] = (string) $total_amt;
            return $this->return->json_sendResponse(1, $res, 'Booking Details', $is_sub_val);
        } else {
            $allData = (object) [];
            return $this->return->json_sendResponse(0, $allData, 'Details not available', $is_sub_val);
        }
    }

    public function BillingDetails() {
        $user_id = $this->form->user_id;
        $PastPlan = Usersubscription::with('plan_category')
            ->where('user_id', $user_id)->where('status', '0')->orderBy('status', 'DSC')->orderBy('purchased_at', 'DSC')->get();
        $currentPlan = Usersubscription::with('plan_category')->where('user_id', $user_id)
            ->where('status', '1')->first();
        $coupoun_details = Userusecoupon::where('usersubscription_id',$currentPlan->id)->first();
        //05-11-2019
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
        //end
        if (!empty($currentPlan)) {
            $res['id'] = (string) $currentPlan->id;
            $res['plan_name'] = $currentPlan->plan_category->plan_cat_name;
            if($coupoun_details != null) {
                $res['coupon_name'] = $coupoun_details->coupon_code;
                $res['coupon_amount'] = $coupoun_details->counpon_discount;
            }else{
                $res['coupon_name'] = "";
                $res['coupon_amount'] = "0";
            }
            //$res['visit_pass'] = ($currentPlan->visit_pass == 'month') ? 'Monthly ∞ Pass' : $currentPlan->visit_pass . ' Visit Pass';
            $res['purchased_date'] = ($currentPlan->purchased_at) ? date('d/m/Y', strtotime($currentPlan->purchased_at)) : '';
            $res['subscription_amount'] = ($currentPlan->amount) ? $currentPlan->amount : '';
            $res['expired_date'] = ($currentPlan->expired_at) ? date('d/m/Y', strtotime($currentPlan->expired_at)) : '';
            //$res['about_plan'] = ($currentPlan->description) ? $currentPlan->description :'';
        } else {
            $res['message'] = "Oh no! Looks like you are not subscribed";
        }
        $res['past_subscription'] = [];
        if (!empty($PastPlan)) {

            foreach ($PastPlan as $key => $value) {
                $res['past_subscription'][$key]['id'] = (string) $value->id;
                $res['past_subscription'][$key]['plan_name'] = $value->plan_category->plan_cat_name;
                // $res['past_subscription'][$key]['visit_pass'] = ($value->visit_pass == 'month') ? 'Monthly' : $value->visit_pass . ' Visit Pass';
                $res['past_subscription'][$key]['purchased_date'] = ($value->purchased_at) ? date('d/m/Y', strtotime($value->purchased_at)) : '';
                $res['past_subscription'][$key]['subscription_amount'] = ($value->amount) ? $value->amount : '';
                $res['past_subscription'][$key]['expired_date'] = ($value->expired_at) ? date('d/m/Y', strtotime($value->expired_at)) : '';
                //$res['past_subscription'][$key]['about_plan'] = ($value->description) ? $value->description :'';
            }
        }
        return $this->return->json_sendResponse(1, $res, 'Billing Details', $is_sub_val);
    }

    public function homescreen() {
        $user_id = $this->form->user_id;
        $is_corporate_user = CorporateUsers::where('user_id', $user_id)->first();
        $getdata = Usersubscription::with('plan_category')->with('user_card')->where('user_id', $user_id)->where('status', 1)->first();
        // return $getdata;
        $carddata = Usersubscription::with('plan_category')->with('user_card')->where('user_id', $user_id)->where('status', 0)->orderBy('id', 'desc')->first();
        $last_check_in = Uservisitedgym::with('gym_name')->where('user_id', $user_id)->orderBy('id', 'DESC')->first();
        //echo print_r($last_check_in->gym_name);die;
        if ($last_check_in) {
            $gym_id = Usergym::select('id', 'gym_name')->where('id', $last_check_in->gym_id)->first();
            $gym_id_new = $gym_id->id;
        } else {
            $gym_id_new = '0';
        }

        if ($getdata) {
            if ($is_corporate_user) {
                $total_check_in = Uservisitedgym::where('user_id', $user_id)->where('Usersubscription_id', $getdata->id)->whereMonth('created_at', Carbon::now()->month)->where('gym_id', $gym_id_new)->get()->count();
            } else {
                $total_check_in = Uservisitedgym::where('user_id', $user_id)->where('Usersubscription_id', $getdata->id)->where('gym_id', $gym_id_new)->get()->count();
            }
            $user_total_visit = Uservisitedgym::where('user_id', $user_id)->where('Usersubscription_id', $getdata->id)->get()->count();
        } else {
            $total_check_in = 0;
            $user_total_visit = 0;
        }
        //echo'<pre>';print_r($last_check_in);die;
        //05-11-2019
        // return $user_total_visit;
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
        //end
        $allData = (object) [];
        if (empty($getdata)) {
            $arr['is_subscribed'] = (boolean) 0;
            $arr['total_gyms'] = Usergym::where('status' , 1)->where('is_delete' , 1)->count();
            // $arr['is_subscribed'] = '';
            $arr['total_visit_pass'] = '';
            if (empty($last_check_in)) {
                $arr['is_visited'] = (boolean) 0;

                $arr['gym_name'] = '';
                $arr['gym_address'] = '';
                $arr['date'] = '';
                $arr['time'] = '';
                $arr['i8Active_remaining'] = '';
            } else {
                $arr['is_visited'] = (boolean) 1;

                $arr['gym_name'] = $last_check_in->gym_name->gym_name;
                $arr['gym_address'] = $last_check_in->gym_name->gym_address;
                $arr['date'] = date('d/m/Y', strtotime($last_check_in->user_check_in));
                $arr['time'] = $last_check_in->user_check_in;
                $arr['i8Active_remaining'] = '';
                if ($last_check_in->gym_name->gym_id == 118) {
                    $I8ActiveCount = Uservisitedgym::
                    where('user_id', $user_id)
                        ->where('gym_id', 118)
                        ->whereMonth('user_check_in', date('m'))
                        ->orderBy('id', 'DESC')->count();
                    $arr['i8Active_remaining'] = 5 - $I8ActiveCount;
                }

            }
            $arr['plan_cat_id'] = "";
            $arr['current_time'] = date('Y-m-d H:i:s');
            $arr['re_check_in_time'] = '';
            if ($carddata) {
                foreach ($carddata->user_card as $key => $value) {
                    $arr['card_detail']['card_name'] = ucfirst($value->user_card_brand);
                    $arr['card_detail']['card_number'] = $value->user_card_number;
                    $arr['card_detail']['card_exp_month'] = $value->user_card_exp_month;
                    $arr['card_detail']['card_exp_year'] = $value->user_card_exp_year;
                    $arr['card_detail']['card_country'] = (string) ucfirst($value->card_country);
                }
            } else {
                $arr['card_detail'] = (object) [];
            }
            return $this->return->json_sendResponse(1, $arr, 'You have not any active plan', $is_sub_val);
        }
        //echo'<pre>';print_r($getdata);die();
        $user_subscription_id = $getdata->id;
        $user_have_visit_pass = $getdata->visit_pass;
        $my_visit = $this->user_sub->my_visit($user_id, $user_subscription_id);
        $total_visit = $this->user_gym->usertotal_visit($user_id, $user_subscription_id, $user_have_visit_pass);
        $home_data = $this->user_visit_gym->homescreendata($user_id, $user_subscription_id);
        $is_subscription = $this->user_sub->is_subscription($user_id);
        //echo'<pre>';print_r($home_data);die();
        if (!empty($is_subscription)) {
            $arr['total_gyms'] = Usergym::where('status' , 1)->where('is_delete' , 1)->count();
            $arr['is_subscribed'] = ($is_subscription['status'] == '1') ? (boolean) 1 : (boolean) 0;
            $arr['expired_at'] = ($is_subscription['expired_at'] == '') ? '' : date('d/m/Y', strtotime($is_subscription['expired_at']));
            $arr['total_visit_pass'] = ($is_subscription['visit_pass'] == 'month') ? 'Monthly ∞ Pass' : $is_subscription['visit_pass'] . ' Visit Pass';
            if (empty($home_data)) {
                $arr['gym_name'] = '';
                if (empty($last_check_in)) {
                    $arr['is_visited'] = (boolean) 0;
                    $arr['gym_id'] = '';
                    $arr['gym_name'] = '';
                    $arr['gym_address'] = '';
                    $arr['date'] = '';
                    $arr['time'] = '';
                    $arr['i8Active_remaining'] = '';
                } else {
                    $arr['is_visited'] = (boolean) 1;
                    $arr['gym_id'] = $gym_id->id;
                    $arr['gym_name'] = $last_check_in->gym_name->gym_name;
                    $arr['gym_address'] = $last_check_in->gym_name->gym_address;
                    $arr['date'] = date('d/m/Y', strtotime($last_check_in->user_check_in));
                    $arr['time'] = $last_check_in->user_check_in;
                    $arr['i8Active_remaining'] = '';
                    if ($last_check_in->gym_name->gym_id == 118) {
                        $I8ActiveCount = Uservisitedgym::
                        where('user_id', $user_id)
                            ->where('gym_id', 118)
                            ->where('usersubscription_id', $is_subscription['id'])
                            ->whereMonth('user_check_in', date('m'))
                            ->orderBy('id', 'DESC')->count();
                        $arr['i8Active_remaining'] = 5 - $I8ActiveCount;
                    }
                }
                $arr['current_time'] = date('Y-m-d H:i:s');
                $arr['plan_cat_id'] = $getdata->plan_category->id;
                $arr['re_check_in_time'] = $is_subscription['purchased_at'] ? $is_subscription['purchased_at'] : '';
            }
            if ($home_data) {
                $arr['is_visited'] = (boolean) 1;
                $arr['gym_id'] = $gym_id->id ? $gym_id->id : '';
                $arr['gym_name'] = ($home_data[0]->gym_name) ? $home_data[0]->gym_name : '';
                $arr['gym_address'] = ($home_data[0]->gym_address) ? $home_data[0]->gym_address : '';
                $date = ($home_data[0]->user_check_in) ? $home_data[0]->user_check_in : '';
                $arr['date'] = date('d/m/Y', strtotime($date));
                $arr['current_time'] = date('Y-m-d H:i:s');
                $arr['time'] = $date; //date('H:i',strtotime($date));
                $arr['i8Active_remaining'] = '';
                if ($last_check_in->gym_name->id == 118) {
                    $I8ActiveCount = Uservisitedgym::
                    where('user_id', $user_id)
                        ->where('gym_id', 118)
                        ->where('usersubscription_id', $is_subscription['id'])
                        ->whereMonth('user_check_in', date('m'))
                        ->orderBy('id', 'DESC')->count();
                    $arr['i8Active_remaining'] = 5 - $I8ActiveCount;
                }
                $arr['re_check_in_time'] = date('Y-m-d H:i:s', strtotime('+480 minutes', strtotime($date))); //$is_subscription['purchased_at'] ? $is_subscription['purchased_at'] :'';
            }
            $arr['selected_plan'] = $getdata->plan_category->plan_cat_name;
            $arr['plan_cat_id'] = (string) $getdata->plan_category->id;
//             if ($user_have_visit_pass == 'month') {
//                 $remaining_visit = (string) 4500;
//             } else {
//                 $remaining_visit = (string) ($user_have_visit_pass - $total_visit);
//             }
            $isCorporateUser = CorporateUsers::where('user_id', $user_id)->first();
            if($isCorporateUser){
                $arr['remaining_visit'] = 'Unlimited'; //date('i',$diff);
            }else{
                if ($getdata->plan_category->id == 4) {
                    $arr['remaining_visit'] = (string)(trans('lang_data.total_trail_pass') - $user_total_visit); //date('i',$diff);
                } else {
                    $arr['remaining_visit'] = (string)(trans('lang_data.total_visit_pass') - $user_total_visit); //date('i',$diff);
                }
            }
            $arr['monthly_check_in'] = $total_check_in;

            //$arr['card_detail'] = (object)[];
            foreach ($getdata->user_card as $key => $value) {
                $arr['card_detail']['card_name'] = ucfirst($value->user_card_brand);
                $arr['card_detail']['card_number'] = (string) $value->user_card_number;
                $arr['card_detail']['card_exp_month'] = (string) $value->user_card_exp_month;
                $arr['card_detail']['card_exp_year'] = (string) $value->user_card_exp_year;
                $arr['card_detail']['card_country'] = (string) ucfirst($value->card_country);
            }
            return $this->return->json_sendResponse(1, $arr, 'Home Screen Data', $is_sub_val);
        } else {
            return $this->return->json_sendResponse(0, $allData, 'Data unavailable', $is_sub_val);
        }
    }

    public function ManualRenewPlan(){
        try{
            $user_id = $this->form->user_id;
            $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
                $is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
            // $subscription_id = $this->form->subscription_id;
            $date = date('Y-m-d');
            $status['status'] = '0';
            $today_end_plan = $this->user_sub->getmanualrenewaldata($user_id);
            //dd($today_end_plan);
            include_once(app_path() . '/Stripe/init.php');
            \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
            //echo '<pre>';print_r($value);die;
            $card_number = $today_end_plan->user_card[0]->user_card_number;
            //echo $card_number;die;
            $amount = $today_end_plan->amount;
            try {
                $amount = trim($amount * 100);
                $curr = 'pkr';
                $customer_id = $today_end_plan->customer_id;
                $charge = \Stripe\Charge::create([
                    'amount' => $amount,
                    'currency' => $curr,
                    'customer' => $customer_id,
                ]);
            } catch (\Stripe\Exception\CardException $e) {
                // Since it's a decline, \Stripe\Exception\CardException will be caught
                echo 'Status is:' . $e->getHttpStatus() . '\n';
                echo 'Type is:' . $e->getError()->type . '\n';
                echo 'Code is:' . $e->getError()->code . '\n';
                // param is '' in this case
                echo 'Param is:' . $e->getError()->param . '\n';
                echo 'Message is:' . $e->getError()->message . '\n';
                $failed['failed_message'] = $e->getError()->message;
                $failed['user_id'] = $today_end_plan->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $today_end_plan->user_id)->update($status);
                return $this->return->json_sendResponse(0, (object)[], $e->getError()->message, $is_sub_val);
            } catch (\Stripe\Exception\RateLimitException $e) {
                $failed['failed_message'] = "Too many requests made to the API too quickly";
                $failed['user_id'] = $today_end_plan->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $today_end_plan->user_id)->update($status);
                return $this->return->json_sendResponse(0, (object)[], $failed['failed_message'], $is_sub_val);
            } catch (\Stripe\Exception\InvalidRequestException $e) {
                $failed['failed_message'] = "Invalid card parameters";
                $failed['user_id'] = $today_end_plan->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $today_end_plan->user_id)->update($status);
                return $this->return->json_sendResponse(0, (object)[], $failed['failed_message'], $is_sub_val);
            } catch (\Stripe\Exception\AuthenticationException $e) {
                $failed['failed_message'] = "Authentication with Stripe's API failed";
                $failed['user_id'] = $today_end_plan->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $today_end_plan->user_id)->update($status);
                return $this->return->json_sendResponse(0, (object)[], $failed['failed_message'], $is_sub_val);
            } catch (\Stripe\Exception\ApiConnectionException $e) {
                $failed['failed_message'] = "Network communication with Stripe failed";
                $failed['user_id'] = $today_end_plan->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $today_end_plan->user_id)->update($status);
                return $this->return->json_sendResponse(0, (object)[], $failed['failed_message'], $is_sub_val);
            } catch (\Stripe\Exception\ApiErrorException $e) {
                $failed['failed_message'] = "Something went wrong please try again";
                $failed['user_id'] = $today_end_plan->user_id;
                Usersubscription::where('user_id', $today_end_plan->user_id)->update($status);
                Autorenewalfaild::create($failed);
                return $this->return->json_sendResponse(0, (object)[], $failed['failed_message'], $is_sub_val);
            } catch (Exception $e) {
                $failed['failed_message'] = "Something went wrong please try again";
                $failed['user_id'] = $today_end_plan->user_id;
                Autorenewalfaild::create($failed);
                Usersubscription::where('user_id', $today_end_plan->user_id)->update($status);
                return $this->return->json_sendResponse(0, (object)[], $failed['failed_message'], $is_sub_val);
            }

            // save payment here
            if (!empty($charge)) {
                $charge_id = $charge->id ? $charge->id : '';
                $charge_object = $charge->object ? $charge->object : '';
                $charge_amount = $charge->amount ? $charge->amount : '';
                $balance_transaction_id = $charge->balance_transaction ? $charge->balance_transaction : '';
                $balance_created = $charge->created ? $charge->created : '';
                $balance_currency = $charge->currency ? $charge->currency : '';
                $balance_description = $charge->description ? $charge->description : '';
                $payment_method = $charge->payment_method ? $charge->payment_method : '';
                $get_subscription_data = Subscription::whereid($today_end_plan->subscription_id)->first();
                //User Subscription Data
                $insert['user_id'] = $today_end_plan->user_id;
                $insert['subscription_id'] = $today_end_plan->subscription_id;
                $insert['customer_id'] = $customer_id; //customer id
                $insert['transaction_id'] = $balance_transaction_id;
                $insert['purchased_at'] = date('Y-m-d H:i:s', $balance_created);
                $insert['plan_name'] = $get_subscription_data->plan_name;
                $insert['visit_pass'] = $get_subscription_data->visit_pass;
                $insert['plan_duration'] = $get_subscription_data->plan_duration;
                $insert['plan_time'] = $get_subscription_data->plan_time;
                $insert['amount'] = $get_subscription_data->amount;
                        $insert['description'] = $get_subscription_data->description ? $get_subscription_data->description :'';
                $insert['payment_method'] = $payment_method;
                $insert['status'] = ($charge->status == 'succeeded') ? 1 : 0;
                $insert['currency'] = $balance_currency;
                $insert['charge_id'] = $charge_id;
                $insert['integrated_gst'] = $today_end_plan->integrated_gst;
                $insert['total_payable_amount'] = $today_end_plan->amount;//$today_end_plan->total_payable_amount;
                // add 23-12-2019
                $insert['failure_code'] = $charge->failure_code ? $charge->failure_code : '';
                $insert['failure_message'] = $charge->failure_message ? $charge->failure_message : '';
                //end 23-12-2019
                $plan_duration = $insert['plan_duration'];
                $plan_time = $insert['plan_time'];
                $insert['expired_at'] = date('Y-m-d H:i:s', strtotime("+$plan_duration $plan_time", strtotime($insert['purchased_at'])));
                // User Card Data
                $card['card_brand'] = $charge->payment_method_details->card->brand;
                $card['card_country'] = $charge->payment_method_details->card->country;
                $card['card_number'] = $charge->payment_method_details->card->last4;
                $card['card_exp_month'] = $charge->payment_method_details->card->exp_month;
                $card['card_exp_year'] = $charge->payment_method_details->card->exp_year;
                $card['user_card_brand'] = $charge->payment_method_details->card->brand;
                $card['user_card_number'] = $card_number; //$charge->payment_method_details->card->last4; //here full card
                $card['user_card_exp_month'] = $charge->payment_method_details->card->exp_month;
                $card['user_card_exp_year'] = $charge->payment_method_details->card->exp_year;
                $card['status'] = 1;
                $card['user_id'] = $today_end_plan->user_id;
                $status['status'] = '0';
                $update = Usersubscription::where('user_id', $today_end_plan->user_id)->update($status);
                $add_payment = Usersubscription::create($insert);
                if (isset($add_payment->id) && $add_payment->id > 0) {
                    $card['usersubscription_id'] = $add_payment->id;
                    $add_card = Usercard::create($card);
                    return $this->return->json_sendResponse(1, (object)[], 'SUCCESS', $is_sub_val);
                } else {
                    $failed['failed_message'] = "Something went wrong payment saved failed";
                    $failed['user_id'] = $today_end_plan->user_id;
                    Autorenewalfaild::create($failed);
                    return $this->return->json_sendResponse(0, (object)[], $failed['failed_message'], $is_sub_val);

                }
            }
        }catch (Throwable $th) {
            return $this->return->json_sendResponse(0, (object)[], $th->getMessage(), $is_sub_val = false);
        }
    }

}
