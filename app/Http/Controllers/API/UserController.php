<?php

namespace App\Http\Controllers\API;

use App\API\Usersubscription;
use App\Appversion;
use App\CorporateUsers;
use App\Http\Controllers\API\ApiCommanFunctionController as ApiCommanFunctionController;
use App\Mail\SendMailable;
use App\Resetpassword;
use App\User;
use DB;
use Illuminate\Support\Str;
use App\Mail\VerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;


class UserController extends ApiCommanFunctionController
{

    public $successStatus = 200;

    function __construct()
    {
        //parent::__construct();is_subscribe
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $this->form = $data;
        $this->return = new ApiCommanFunctionController;
        $this->user_sub = new Usersubscription;
        //echo '<pre>'; print_r($data);die;
    }

    public function registration(Request $r)
    {
        $input = $r->all();
        $user = User::where(function ($query) use ($input){
            $query->where('email' , $input['email'])->orWhere('phone_number' , $input['phone_number']);
        })->where('is_delete' , 0)->first();

        if (!empty($user)) {
            $rules = [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'phone_number' => 'required',
                'password' => 'required',
//            'country' => 'required',
//            'postal_code' => 'required',
//            'd_o_b' => 'required',
                'user_type' => 'required',
                'user_image' => 'required',
            ];
        } else {
            $rules = [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users',
                'phone_number' => 'required|unique:users',
                'password' => 'required',
//            'country' => 'required',
//            'postal_code' => 'required',
//            'd_o_b' => 'required',
                'user_type' => 'required',
                'user_image' => 'required',
            ];
        }

        $message = [
            'first_name.required' => 'First Name is required',
            'last_name.required' => 'Last Name is required',
            'email.required' => 'Email is required',
            'phone_number.required' => 'Phone number is required',
            'd_o_b.required' => 'DOB is required',
            'password.required' => 'Password is required',
            'country.required' => 'Country is required',
            'postal_code.required' => 'Postal code is required',
            'user_type.required' => 'User Type required',
            'user_image.required' => 'Image required',
        ];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            return app('App\Http\Controllers\API\ApiCommanFunctionController')->sendError_obj($validator->errors()->first());
        } else {
            //echo '<pre>'; print_r($r->all()); die;
            $obj = new User;
            if ($r->user_image) {
                if (!empty($user)) {
                    if (file_exists($user->image)) {
                        unlink($user->image);
                    }
                }
                $image_64 = $r->user_image; //your base64 encoded data
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
                $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                $image = str_replace($replace, '', $image_64);
                $image = str_replace(' ', '+', $image);
                $imageName = time() . '.' . $extension;

                file_put_contents('image/user_image/' . $imageName, base64_decode($image));
                $input['user_image'] = '/public/image/user_image/' . $imageName;

            }
//            $imageName = date('Ymdhis') . '.' . request()->user_image->getClientOriginalExtension();
//            request()->user_image->move(public_path('image/user_image'), $imageName);
            unset($input['d_o_b']);
            unset($input['password']);
            if (isset($r['firebase_token'])) {
                $firebase_token = $r['firebase_token'];
            } else {
                $firebase_token = "";
            }

            $input['status'] = 1;
            $input['is_delete'] = 1;
            $input['is_email_verified'] = 1;
            $input['firebase_token'] = $firebase_token;
            $input['password'] = Hash::make($r['password']);
            $input['user_ufp_id'] = !empty($user) ? $user->user_ufp_id : mt_rand(10000, 99999);//$input['country'].rand(pow(10, 3-1), pow(10, 3)-1);
            $input['user_token'] = $this->generateRandomString();
            $input['user_token_time'] = new \DateTime();

            //echo '<pre>'; print_r($input); die;
            $insert = User::where(function ($query) use ($input){
                $query->where('original_email' , $input['email'])->orWhere('original_phone_number' , $input['phone_number']);
            })->where('is_delete' , 0)->first();

            if($insert){
                $insert->update($input);
            }else{
                $insert = User::create($input);
            }

//            $insert = !empty($user) ? $user->update($input) : User::create($input);
//            $lastID = !empty($user) ? $user->id : $insert->id;
            $lastID = $insert->id;
            $is_subscribed = (boolean)0;//05-11-2019
            if ($lastID > 0) {
                $update = User::whereid($lastID)->update([
                    'user_image' => '/image/user_image/' . $imageName,
                    'd_o_b' => date('Y-m-d'),
                ]);
                //$tempArr = array();
                $tempArr = $this->getUserData($lastID);
                //\Mail::to($input['email'])->send(new SendMailabletemplate($lastID));
                $token = $this->generateRandomString();
//                DB::table('users_verify')->insert([
//                    'user_email' => $input['email'],
//                    'user_id' => $lastID,
//                    'token' => $token
//                ]);


//                $data = ['verification_link' => url('account/verify/' . $token)];
//                try{
//                    \Mail::to($input['email'])->send(new VerifyEmail($data));
//                }catch (\Exception $e){
//
//                }

                return app('App\Http\Controllers\API\ApiCommanFunctionController')->json_sendResponse(1, $tempArr, 'User Registration successfully', $is_subscribed);
            } else {
                $arr = (object)[];
                return $this->return->json_sendResponse(0, $arr, 'Something went wrong please try again', $is_subscribed);
            }
        }
    }

    public function validate_registration(Request $r)
    {
        //echo '<pre>'; print_r($r->all()); die;
        $input = $r->all();
        $user = User::where(function ($query) use ($input){
            $query->where('email' , $input['email'])->orWhere('phone_number' , $input['phone_number']);
        })->where('is_delete' , 0)->first();

        if (!empty($user)) {
            $rules = [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'phone_number' => 'required',
                'password' => 'required',
                'user_type' => 'required',
            ];
        } else {
            $rules = [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users',
                'phone_number' => 'required|unique:users',
                'password' => 'required',
                'user_type' => 'required',
            ];
        }


        $message = [
            'first_name.required' => 'First Name is required',
            'last_name.required' => 'Last Name is required',
            'email.required' => 'Email is required',
            'phone_number.required' => 'Phone number is required',
            'password.required' => 'Password is required',
            'user_type.required' => 'User Type required',
        ];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            return app('App\Http\Controllers\API\ApiCommanFunctionController')->sendError_obj($validator->errors()->first());
        } else {
            $arr = (object)[];
            return $this->return->json_sendResponse(1, $arr, '', '0');
        }
    }

    public function country()
    {
        $is_subscribed = (boolean)0;//05-11-2019
        $country = DB::table('country')->select('id', 'nicename')->where('id', 162)->orderBy('nicename', 'ASC')->get();
        if ($country) {
            foreach ($country as $key => $value) {
                $res[$key]['id'] = (string)$value->id;
                $res[$key]['nicename'] = $value->nicename;
            }
            return app('App\Http\Controllers\API\ApiCommanFunctionController')->json_sendResponse(1, $res, 'Country List', $is_subscribed);
        } else {
            $allData = [];
            return app('App\Http\Controllers\API\UserController')->json_sendResponse(0, $allData, 'Country Unavailable', $is_subscribed);
        }
    }

    public function state()
    {
        $country_id = 162;
        $is_subscribed = (boolean)0;
        $country = DB::table('state')->select('id', 'state')->where('country_id', $country_id)->orderBy('state', 'ASC')->get();
        if ($country) {
            foreach ($country as $key => $value) {
                $res[$key]['id'] = (string)$value->id;
                $res[$key]['nicename'] = $value->state;
            }
            return app('App\Http\Controllers\API\ApiCommanFunctionController')->json_sendResponse(1, $res, 'State List', $is_subscribed);
        } else {
            $allData = [];
            return app('App\Http\Controllers\API\UserController')->json_sendResponse(0, $allData, 'State Unavailable', $is_subscribed);
        }
    }

    public function forgot_password()
    {

        $email['email'] = $this->form->email;
        $email_id = $email['email'];
        $rules = [
            'email' => 'required',
        ];
        $message = [
            'email.required' => 'Email is required',
        ];
        $validator = Validator::make($email, $rules, $message);
        if ($validator->fails()) {
            return app('App\Http\Controllers\API\ApiCommanFunctionController')->sendError_obj($validator->errors()->first());
        }
        $is_subscribed = (boolean)0;//05-11-2019
        $data = User::where('email', $email_id)->first();
        if (!empty($data)) {
            //$name = 'Hello User';
            $user_id = $data['id'];
            $pin = $this->generatePIN();
            $email['token'] = $pin;
            $get_email_otp = Resetpassword::where('email', $email['email'])->first(); // For resend OTP  condition
            if ($get_email_otp) {
                Resetpassword::where('email', $email['email'])->update(['token' => $pin]);
            } else {
                Resetpassword::create($email);
            }
            $tempArr = array('email' => $email['email'], 'otp' => (string)$pin);
            //$email = (string)$email;
            \Mail::to($email_id)->send(new SendMailable($pin));
            $msg = "A one time pin was sent to your registered email address. Please enter this for verification. If you have not received the OTP check your spam folder!";
            return app('App\Http\Controllers\API\ApiCommanFunctionController')->json_sendResponse(1, $tempArr, $msg, $is_subscribed);
        } else {
            $allData = (object)[];
            return app('App\Http\Controllers\API\UserController')->json_sendResponse(0, $allData, 'This email does not exist', $is_subscribed);
        }
    }

    public function verify_otp()
    {

    }

    public function update_password()
    {
        $input['email'] = $this->form->email;
        $input['confirm_password'] = $this->form->confirm_password;
        $rules = [
            'email' => 'required',
            'confirm_password' => 'required',
        ];
        $message = [
            'email.required' => 'Email is required',
            'confirm_password.required' => 'Password is required',
        ];
        $is_subscribed = (boolean)0;//05-11-2019
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            return $this->return->sendError_arr($validator->errors()->first());
        } else {
            //echo '<pre>';print_r($input);die;
            $update = User::where('email', $input['email'])->update(['password' => Hash::make($input['confirm_password'])]);
            $arr = array();
            return $this->return->json_sendResponse(1, $arr, 'Password reset successfully', $is_subscribed);
        }
    }

//    public function email_send_opt()
//    {
//        $input['email'] = $this->form->email;
//        $input['opt_code'] = $this->form->opt_code;
//        $rules = [
//            'email' => 'required',
//            'opt_code' => 'required'
//        ];
//        $message = [
//            'email.required' => 'Email is required',
//            'opt_code.required' => 'Otp code is required'
//        ];
//        $is_subscribed = (boolean)0;//05-11-2019
//        $validator = Validator::make($input, $rules, $message);
//        if ($validator->fails()) {
//            return $this->return->sendError_arr($validator->errors()->first());
//        }
//        $opt_message = 'Hi, Please verify your email by entering otp code. Thanks';
//        $opt_message .= ' Your otp code is ' . $input['opt_code'];
//        \Mail::to($input['email'])->send(new SendMailable($opt_message, 1));
//
//        $arr = array();
//        $msg = "A otp code was sent to your email address. Please enter this for verification. If you have not received the OTP check your spam folder!";
//        return $this->return->json_sendResponse(1, $arr, $msg, $is_subscribed);
//    }

    public function email_send_opt()
    {
        $input['phone_no'] = isset($this->form->phone_no) ? $this->form->phone_no : '';
        $input['email'] = isset($this->form->email) ? $this->form->email : '';
        $input['opt_code'] = isset($this->form->opt_code) ? $this->form->opt_code : '';
        $rules = [
            'phone_no' => 'required',
            'email' => 'required',
            'opt_code' => 'required'
        ];
        $message = [
            'email.required' => 'Email is required',
            'phone_no.required' => 'Mobile number is required',
            'opt_code.required' => 'Otp code is required'
        ];
        $is_subscribed = (boolean)0;//05-11-2019
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            return $this->return->sendError_arr($validator->errors()->first());
        }

        $opt_message1 = 'Hi, Please verify your email by entering otp code. Thanks';
        $opt_message1 .= ' Your otp code is ' . $input['opt_code'];
        \Mail::to($input['email'])->send(new SendMailable($opt_message1, 1));

        $opt_message = 'Hi, Please verify your phone number by entering otp code. Thanks \n';
        $opt_message .= 'Your otp code is ' . $input['opt_code'] . ' \n Gym Passport';

        // $this->sendSMS($opt_message, $input['phone_no']);
        $arr = array();
        $msg = "A otp code was sent to your email. Please enter this for verification. If you have not received the OTP resend it after a while!";
        return $this->return->json_sendResponse(1, $arr, $msg, $is_subscribed);
    }

    public function sendSMS($message, $PhoneNo)
    {
        $message = strtr($message, array('<br>' => PHP_EOL));
        $message = strip_tags($message);
        $message = urlencode($message);
        $phn = '92' . substr(strstr($PhoneNo, "0"), 1);
//        $_url = 'http://api.bizsms.pk/api-send-branded-sms.aspx?username=gympassport@bizsms.pk&pass=gympas386&text=' . $message . '&masking=GymPassport&destinationnum=' . $phn . '&language=English';
        $_url = "https://secure.m3techservice.com/GenericService/webservice_4_0.asmx/SendSMS?UserId=TotalSoft@Gym%20Passport&Password=G%23M9@$$90RT&MobileNo=$phn&MsgId=1234&SMS=$message&MsgHeader=939397&SMSType=0&HandsetPort=0&SMSChannel=info&Telco=";
        if ($_result = file_get_contents($_url)) {
            $_result_f = json_decode($_result);
            return 1;
        }
        return 0;
    }

    public function login()
    {
        //echo '<pre>';print_r($this->form);die;
        $email['email'] = $this->form->email;
        $password = $this->form->password;
        $user_type = $this->form->user_type;
        $device_token = $this->form->device_token;
        if (isset($this->form->firebase_token)) {
            $firebase_token = $this->form->firebase_token;
        } else {
            $firebase_token = "";
        }
        $rules = [
            'email' => 'required',
        ];
        $message = [
            'email.required' => 'Email is required',
        ];
        $validator = Validator::make($email, $rules, $message);
        if ($validator->fails()) {
            return app('App\Http\Controllers\API\ApiCommanFunctionController')->sendError_obj($validator->errors()->first());
        }
        $data = User::where('email', $email['email'])->where('user_type', $user_type)->where('status', '1')->where('is_delete', '1')->first();
        //05-11-2019
        $is_sub_val = (boolean)0; //user subscription data
        //end

        if (!empty($data)) {
            if ($data->status == '0') {
                $allData = (object)[];
                return $this->return->json_sendResponse(0, $allData, 'Your access to Gym Passport has been restricted. Please contact your company or HR team', $is_sub_val);
            }
            if (Hash::check($password, $data->password)) {
               $corporateUser = CorporateUsers::where('user_id',$data->id)->first();
               if($corporateUser) {
                   $checkCorporate = User::find($corporateUser->corporate_id);
                   if(!$checkCorporate->status){
                       $allData = (object)[];
                       return $this->return->json_sendResponse(0, $allData, 'Your account has been blocked please contact GymPassport Admin', $is_sub_val);
                   }
               }
                $user_id = $data->id;
                //generete refresh token for user
                $current_time = date('Ymdhis');
                $generateRefreshToken = $this->generateRandomString();
                $data->user_token = $generateRefreshToken;
                $data->user_token_time = new \DateTime();
                $data->firebase_token = $firebase_token;
                $data->update();
                // end refresh token
                $getdata = $this->getUserData($user_id);
                //echo '<pre>';print_r($getdata);die;
                //05-11-2019
                $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
                //return $is_subscribe_data;
                $is_sub_val = ($is_subscribe_data) ? (boolean)1 : (boolean)0; //user subscription data
                //end
                if ($getdata) {
                    if($getdata['is_email_verified'] == 0){
                        $token = $this->generateRandomString();
                        DB::table('users_verify')->insert([
                            'user_email' => $getdata['email'],
                            'token' => $token
                        ]);

                        $data = ['verification_link' => url('account/verify/' . $token)];
                        \Mail::to($getdata['email'])->send(new VerifyEmail($data));
                        return $this->return->json_sendResponse(0 , [] , 'We have sent you an email, Please verify your Account', $is_sub_val);
                    }

                    if ($getdata['is_corporate'] && !$getdata['is_subscribed']) {
                        $allData = (object)[];
                        return $this->return->json_sendResponse(0, $allData, 'Your subscription is expired please contact your corporate representative.', $is_sub_val);
                    }
                }

                return $this->return->json_sendResponse(1, $getdata, 'Login Successfully', $is_sub_val);
            } else {
                $allData = (object)[];
                return $this->return->json_sendResponse(0, $allData, 'Invalid Password', $is_sub_val);
            }
        } else {
            $allData = (object)[];
            return $this->return->json_sendResponse(0, $allData, 'User does not exist please check login credentials', $is_sub_val);
        }
    }

    public function getUserData($user_id, $flag = null)
    {
        $user = User::where('id', $user_id)->first();
        $sql = "SELECT users.id,users.is_email_verified,users.first_name,users.user_ufp_id,users.last_name,users.email,users.d_o_b,partner_first_login,users.user_image,users.user_token,users.country as usercountry,users.state as state_id,state.state,users.city,users.house_no,users.street,users.district,users.address as useraddress,users.postal_code,uvg.user_check_in,DAYNAME(uvg.user_check_in) as day"
            . ",WEEK(CURDATE(), 1) as current_week,(SELECT gym_address FROM users_gym WHERE id=uvg.gym_id ORDER BY uvg.id DESC LIMIT 1) as address,"
            . "country.nicename as country,(SELECT usersubscriptions.status from usersubscriptions WHERE usersubscriptions.user_id=users.id AND usersubscriptions.status=1) as is_subscribed,(SELECT usersubscriptions.plan_name from usersubscriptions WHERE usersubscriptions.user_id=users.id AND usersubscriptions.status=1) as plan_cat_id FROM users LEFT JOIN user_visited_gym uvg ON uvg.user_id=users.id AND (YEARWEEK(uvg.user_check_in, 1) = YEARWEEK(CURDATE(), 1)) LEFT JOIN users_gym ug ON ug.id=uvg.gym_id "
            . "LEFT JOIN country ON users.country=country.id LEFT JOIN usersubscriptions ON usersubscriptions.user_id=users.id LEFT JOIN state ON state.id=users.state WHERE users.id='" . $user_id . "' order by usersubscriptions.id desc";
        $userdata = DB::select($sql);

        if (!empty($userdata)) {
            $res['user_id'] = (string)$userdata[0]->id;
            $res['is_email_verified'] = $userdata[0]->is_email_verified;
            $res['user_ufp_id'] = (string)$userdata[0]->user_ufp_id;
            $res['is_subscribed'] = ($userdata[0]->is_subscribed == '') ? (boolean)0 : (boolean)1;
            $res['plan_cat_id'] = ($userdata[0]->plan_cat_id == '') ? '' : (string)$userdata[0]->plan_cat_id;
            $res['first_name'] = $userdata[0]->first_name;
            $res['last_name'] = ($userdata[0]->last_name) ? $userdata[0]->last_name : "";
            $res['email'] = $userdata[0]->email;
            $res['d_o_b'] = ($userdata[0]->d_o_b) ? date('d/m/Y', strtotime($userdata[0]->d_o_b)) : "";
            $res['country_id'] = ($userdata[0]->usercountry) ? $userdata[0]->usercountry : "";
            $res['country'] = ($userdata[0]->country == null || $userdata[0]->country == '') ? "" : $userdata[0]->country;
            $res['state'] = ($userdata[0]->state) ? $userdata[0]->state : "";
            $res['state_id'] = ($userdata[0]->state_id) ? $userdata[0]->state_id : "";
            $res['city'] = ($userdata[0]->city) ? $userdata[0]->city : "";
            $res['house_no'] = ($userdata[0]->house_no) ? $userdata[0]->house_no : "";
            $res['street'] = ($userdata[0]->street) ? $userdata[0]->street : "";
            $res['district'] = ($userdata[0]->district) ? $userdata[0]->district : "";
            $res['address'] = ($userdata[0]->useraddress == '') ? "" : $userdata[0]->useraddress;
            $res['postal_code'] = ($userdata[0]->postal_code) ? $userdata[0]->postal_code : "";
            $res['partner_first_login'] = ($userdata[0]->partner_first_login) ? $userdata[0]->partner_first_login : "0";
            $res['user_image'] = \Config::get('values.app_url') . $userdata[0]->user_image;

            $is_corporate = CorporateUsers::where('user_id', $user_id)->count();

            $res['is_corporate'] = $is_corporate;

            if ($flag != 1) {
                $res['user_token'] = $userdata[0]->user_token;
                $res['current_week'] = (string)$userdata[0]->current_week;
                $res['last_check_in_address'] = ($userdata[0]->address == null) ? "" : $userdata[0]->address;
                $res['check_in_detail'] = [];
                foreach ($userdata as $key => $value) {
                    if (!empty($value->user_check_in)) {
                        $res['check_in_detail'][$key]['user_check_in_date'] = $value->user_check_in ? $value->user_check_in : "";
                        $res['check_in_detail'][$key]['user_check_in_day'] = $value->day ? $value->day : "";
                    }
                }
            }
        } else {
            $res = array();
        }
        return $res;
    }

    public function generatePIN($digits = 4)
    {
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while ($i < $digits) {
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }

    function generateRandomString($length = 200)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function ChangePassword()
    {
        $input['user_id'] = $this->form->user_id;
        $input['current_password'] = $this->form->current_password;
        $input['confirm_password'] = $this->form->confirm_password;
        $rules = [
            'user_id' => 'required',
            'confirm_password' => 'required',
            'current_password' => 'required',
        ];
        $message = [
            'user_id.required' => 'User ID is required',
            'confirm_password.required' => 'Confirm Password is required',
            'current_password.required' => 'Current Password is required',
        ];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            return $this->return->sendError_obj($validator->errors()->first());
        } else {
            $data = User::where('id', $input['user_id'])->first();
            //05-11-2019
            $is_subscribe_data = $this->user_sub->is_subscribe($input['user_id']);
            $is_sub_val = ($is_subscribe_data) ? (boolean)1 : (boolean)0; //user subscription data
            //end
            if (!empty($data)) {
                if (Hash::check($input['current_password'], $data->password)) {
                    if (Hash::check($input['confirm_password'], $data->password)) {
                        $allData = (object)[];
                        return $this->return->json_sendResponse(0, $allData, 'Current Password and New Password must be different', $is_sub_val);
                    } else {
                        $update = User::whereid($input['user_id'])->update(['password' => Hash::make($input['confirm_password']), 'plain_password' => '']);
                        $arr = (object)array();
                        return $this->return->json_sendResponse(1, $arr, 'Password changed successfully', $is_sub_val);
                    }
                } else {
                    $allData = (object)[];
                    return $this->return->json_sendResponse(0, $allData, 'Current Password is Incorrect', $is_sub_val);
                }
            } else {
                $allData = (object)[];
                return $this->return->json_sendResponse(0, $allData, 'Something went wronge', $is_sub_val);
            }

        }

    }

    public function UpdateProfile(Request $request)
    {
        $user_id = $this->form->user_id;
        //$form_email = $this->form->email;
        //$name = $this->form->name;
        //$array = explode(' ', $name);
        // return $request->all();

//        $rules = [
//            'user_image' => 'image|mimes:jpeg,png,jpg,gif',
//        ];

//        $validator = Validator::make($request, $rules);

//        if ($this->form->has('user_image')) {
//            $imageName = date('Ymdhis') . '.' . $this->form->user_image->getClientOriginalExtension();
//            $this->form->user_image->move(public_path('image/user_image'), $imageName);
//            $input['user_image'] = $this->form->user_image;
//        }
        if ($this->form->user_image) {

            $image_64 = $this->form->user_image; //your base64 encoded data
            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
            $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
            $image = str_replace($replace, '', $image_64);
            $image = str_replace(' ', '+', $image);
            $imageName = time() . '.' . $extension;

            file_put_contents('image/user_image/' . $imageName, base64_decode($image));
            $input['user_image'] = '/image/user_image/' . $imageName;

        }

        $input['first_name'] = $this->form->first_name;//$array[0];
        $input['last_name'] = $this->form->last_name;//$array[1];
//        $date = str_replace('/', '-', $this->form->d_o_b);
//        $input['d_o_b'] = date('Y-m-d', strtotime($date));
//        $input['country'] = $this->form->country;
        $input['state'] = $this->form->state;
        $input['city'] = $this->form->city;
//        $input['house_no'] = $this->form->house_no;
//        $input['street'] = $this->form->street;
//        $input['district'] = $this->form->district;
//        $input['address'] = $this->form->address;
//        $input['postal_code'] = $this->form->postal_code;
        $updated = User::whereid($user_id)->update($input);
        //05-11-2019
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean)1 : (boolean)0; //user subscription data
        //end
        if ($updated) {
            $flag = 1;
            $getdata = $this->getUserData($user_id, $flag);
            return $this->return->json_sendResponse(1, $getdata, 'Profile Updated successfully', $is_sub_val);
        } else {
            $allData = (object)[];
            return $this->return->json_sendResponse(2, $allData, 'Nothing to update', $is_sub_val);
        }
    }

    public function app_version()
    {
        $type = $this->form->device_type;
        $data = Appversion::select('ios_force_upgrade', 'android_force_upgrade', 'status', 'android_version', 'ios_version')->first();
        //echo'<pre>';        print_r($data);die;
        if (!empty($data)) {
            if ($type == 'android') {
                $res['android_update'] = (boolean)$data->android_force_upgrade;
                $res['android_version'] = $data->android_version ? $data->android_version : '';
            } else {
                $res['ios_update'] = (boolean)$data->ios_force_upgrade;
                $res['ios_version'] = $data->ios_version ? $data->ios_version : '';
            }
            $res['reason'] = '';
        } else {
            $res = [];
        }

        return $this->return->json_sendResponse(1, $res, 'get app version', 0);
    }

    public function delteSelfAccount(Request $request)
    {
        $user_id = $this->form->user_id;
        $user = User::find($user_id);
        $user->is_delete = 0;
        $user->original_email = $user->email;
        $user->original_phone_number = $user->phone_number;
        $user->email = Str::random(8) . '_' . $user->email;
        $user->phone_number = $this->generatePIN() . $user->phone_number;
        $user->save();

        if ($user) {

            return $this->return->sendResponse(1, [], 'You have Successfuly Deleted Your Account');
        } else {

            return $this->return->sendResponse(0, [], 'Something went wrong');
        }

    }

}
