<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\API\Usersubscription;
use App\User;

class CanclesubscriptionController extends Controller
{
    function __construct()
    {
        $this->user_sub = new Usersubscription;
    }

    public function auto_cancle_subscription()
    {
        $date = date('Y-m-d');
        $fetch_end_subscription_user = $this->user_sub->select_subscription($date);
        foreach ($fetch_end_subscription_user as $key => $value) {

            $user_id = $value->user_id;
            $subcription_id = $value->id;
            $updateRecord['status'] = 0;
            $updateRecord['expired_at'] = date('Y-m-d H:i:s');//->where('user_id',$user_id)->where('status','1')
            $update = Usersubscription::where('id', $subcription_id)->update($updateRecord);
            $user = User::find($user_id);
            if ($update) {
                if ($value->subscription_id == 4) {
                    $this->fire_single_notification("Trial Pass Expired", "Your's GymPassport - Trial has been expired.", '', $user->firebase_token);
                } else if ($value->subscription_id == 1) {
                    $this->fire_single_notification("Lite Pass Expired", "Your's GymPassport - Lite has been expired.", '', $user->firebase_token);
                } else if ($value->subscription_id == 2) {
                    $this->fire_single_notification("Standard Pass Expired", "Your's GymPassport - Standard has been expired.", '', $user->firebase_token);
                } else if ($value->subscription_id == 3) {
                    $this->fire_single_notification("Premium Pass Expired", "Your's GymPassport - Premium has been expired.", '', $user->firebase_token);
                } else {
                    $this->fire_single_notification("GymPassport Pass Expired", "Your's GymPassport - Pass has been expired.", '', $user->firebase_token);
                }
            } else {

            }
            //die();
        }
        //echo '<pre>';print_r($fetch_end_subscription_user);die();
    }

    public function fire_single_notification($title = '', $description = '', $url = '', $firebase_token)
    {

        $fields = '{
            "to" : "' . $firebase_token . '",
            "notification" : {
				"title" : "' . $title . '",
				"body" : "' . $description . '",
				"url" : "' . $url . '",
				"type" : "simple",
				"sound" : "default",
            },
            "data" : {
				"title" : "' . $title . '",
				"body" : "' . $description . '",
				"url" : "' . $url . '",
				"type" : "simple",
				"sound" : "default",
             }
        }';

        $url = 'https://fcm.googleapis.com/fcm/send';
        $setting = "AAAAYJ_iTiA:APA91bH8yDT-0do3nTZNawiSpkhV50t_XJgU4Tbysd8T51T7KaqXdFkdIfG5x-5Ws-WEDX_4Whn-xOGPbr1tL2exXUPmwzMT_kufTBuYaru8R-6UULusKBBaWfE7rEtQCYhgABKK07Hj";
        $headers = array(
            'Authorization: key=' . $setting,
            'Content-Type: application/json',
        );

        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);
        curl_close($ch);
        //return response()->json(array('msg' => 'success'));
    }

}
