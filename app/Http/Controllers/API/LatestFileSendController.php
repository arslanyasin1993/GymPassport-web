<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\LatestFileSend;
use App\Mail\SendMailabletemplate;

class LatestFileSendController extends Controller {

    public function send_latest_file() {
        $files = scandir(public_path() . '/backup/', SCANDIR_SORT_DESCENDING);
        $newest_file = $files[0];
        \Mail::to('gympassport@gmail.com')->send(new LatestFileSend($newest_file));
    }

}
