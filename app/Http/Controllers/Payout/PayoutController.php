<?php

namespace App\Http\Controllers\Payout;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\ApiCommanFunctionController;
use Illuminate\Support\Facades\Validator;
use App\Bankdetail;
use App\Uservisitedgym;
use App\PayoutTransfer;
use App\Payoutdate;
use App\PayoutError;
use Session;

class PayoutController extends Controller {

    function __construct() {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $this->form = $data;
        $this->return = new ApiCommanFunctionController;
        $this->bank_detail = new Bankdetail;
        $this->uvg = new Uservisitedgym;
    }

    public function index(Request $r) {
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        $email = '';
        $account = $this->CreateAccount($r);
        echo '<pre>';
        print_r($account);
        die();
    }

    public function SaveAccount(Request $r) {
        //echo'<pre>'; print_r($r->all());die;
        $validatedData = $r->validate([
            //'first_name' => 'required|max:255',
            //'maiden_name' => 'required|max:255',
            //'last_name' => 'required|max:255',
            //'email' => 'required|max:255',
            //'phone' => 'required|max:15',
            //'dob' => 'required',
            //'country' => 'required',
            //'state' => 'required',
            //'city' => 'required',
            //'postal_code' => 'required|max:6',
            //'line1' => 'required',
            // 'line2' => 'required',
            //'document_front' => 'required|image|mimes:jpeg,png,jpg,pdf|max:4096',
            //'document_back' => 'required|image|mimes:jpeg,png,jpg,pdf|max:4096',
            // 'additional_document_front' => 'required|image|mimes:jpeg,png,jpg,pdf|max:2048',
            //'additional_document_back' => 'required|image|mimes:jpeg,png,jpg,pdf|max:2048',
            'account_holder_name' => 'required|max:255',
            //'account_holder_type' => 'required|max:255',
            'routing_number' => 'required|max:20',
            'account_number' => 'required|max:20',
            //'currency' => 'required|max:20',
            // 'name' => 'required|max:255',
           // 'company_name' => 'required|max:255',
            //'role_in_company' => 'required|max:255',
            //'tax_id' => 'required|max:13',
            //'business_website' => 'required',
                //'mcc' => 'required|max:10',
                //'support_email' => 'required|max:255',
                //'support_phone' => 'required|max:15',
                //'support_url' => 'required|max:255',
                //'url' => 'required|max:255',
                //'product_description' => 'required',
        ]);

        $CreateAccount = $this->CreateAccountStripe($r);
        return redirect()->back();
    }

    public function CreateAccountStripe($r) {
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        $purpose = 'identity_document';
        $input = $r->all();
        $phone = '+61 ' . trim($input['phone']);
        $document_front = date('Ymdhis') . '_' . request()->document_front->getClientOriginalName();
        $document_back = date('Ymdhis') . '_' . request()->document_back->getClientOriginalName();
        // $additional_document_front = date('Ymdhis') . '_' . request()->additional_document_front->getClientOriginalName();
        //$additional_document_back = date('Ymdhis') . '_' . request()->additional_document_back->getClientOriginalName();
        //&& request()->additional_document_front->move(public_path('image/payout_file'), $additional_document_front) && request()->additional_document_back->move(public_path('image/payout_file'), $additional_document_back)
        try {

            if (request()->document_front->move(public_path('image/payout_file'), $document_front) && request()->document_back->move(public_path('image/payout_file'), $document_back)) {
                $image_document_front = 'http://' . $_SERVER['HTTP_HOST'] . '/ultimateFitness/public/image/payout_file/' . $document_front;
                $image_document_back = 'http://' . $_SERVER['HTTP_HOST'] . '/ultimateFitness/public/image/payout_file/' . $document_back;
                // $image_additional_document_front = 'http://' . $_SERVER['HTTP_HOST'] . '/ultimateFitness/public/image/payout_file/' . $additional_document_front;
                //$image_additional_document_back = 'http://' . $_SERVER['HTTP_HOST'] . '/ultimateFitness/public/image/payout_file/' . $additional_document_back;
                $filePath_document_front = $_SERVER['DOCUMENT_ROOT'] . strstr($image_document_front, '/public');
                $filePath_document_back = $_SERVER['DOCUMENT_ROOT'] . strstr($image_document_back, '/public');
                //$filePath_additional_document_front = $_SERVER['DOCUMENT_ROOT'] . strstr($image_additional_document_front, '/public');
                //$filePath_additional_document_back = $_SERVER['DOCUMENT_ROOT'] . strstr($image_additional_document_back, '/public');

                $front_doc = \Stripe\File::create([
                            'purpose' => $purpose,
                            'file' => fopen($filePath_document_front, 'rw'),
                ]);
                $back_doc = \Stripe\File::create([
                            'purpose' => $purpose,
                            'file' => fopen($filePath_document_back, 'rw'),
                ]);
                $additonal_front_doc = \Stripe\File::create([
                            'purpose' => $purpose,
                            'file' => fopen($filePath_document_back, 'rw'),
                ]);
                $additonal_back_doc = \Stripe\File::create([
                            'purpose' => $purpose,
                            'file' => fopen($filePath_document_back, 'rw'),
                ]);

                if ($front_doc && $back_doc && $additonal_front_doc && $additonal_back_doc) {
                    $account_create = \Stripe\Account::create([
                                'type' => 'custom',
                                'country' => trim($input['country']),
                                'email' => trim($input['email']),
                                'requested_capabilities' => [
                                    'card_payments',
                                    'transfers',
                                ],
                                'business_type' => 'company', //trim($input['account_holder_type']), //"individual",
                                'business_profile' => [
                                    'mcc' => '5734', //trim($input['mcc']),
                                    'name' => trim($input['company_name']),
                                    'product_description' => 'Gyms where you grow your body and show your fitness to world', //trim($input['product_description']),
//                                    'support_email' => trim($input['support_email']),
//                                    'support_phone' => trim($input['support_phone']),
//                                    'support_url' => trim($input['support_url']),
                                    'url' => trim($input['business_website']),
                                ],
                                'default_currency' => 'aud', //trim($input['currency']), //'aud',
                                'external_account' => [
                                    'object' => 'bank_account',
                                    'country' => trim($input['country']),
                                    'currency' => 'aud', //trim($input['currency']),
                                    'account_holder_name' => trim($input['account_holder_name']),
                                    'account_holder_type' => 'company', //trim($input['account_holder_type']),
                                    'routing_number' => trim($input['routing_number']),
                                    'account_number' => trim($input['account_number']),
                                ],
                                'company' => [
                                    'name' => trim($input['company_name']),
                                    'directors_provided' => true,
                                    'executives_provided' => true,
                                    'owners_provided' => true,
                                    'phone' => $phone,
                                    'tax_id' => trim($input['tax_id']),
                                    'address' => [
                                        'city' => trim($input['city']),
                                        'country' => trim($input['country']),
                                        'line1' => trim($input['line1']),
                                        'postal_code' => trim($input['postal_code']),
                                        'state' => trim($input['state']),
                                    ],
                                    'verification' => [
                                        'document' => [
                                            'front' => $front_doc->id,
                                            'back' => $back_doc->id,
                                        ],
                                    ],
                                ],
                                'tos_acceptance' => [
                                    'date' => time(),
                                    'ip' => $_SERVER['REMOTE_ADDR'],
                                    'user_agent' => 'Mozilla/5.0',
                                ],
                    ]);

                    $create_person = \Stripe\Account::createPerson(
                                    $account_create->id, [
                                'first_name' => trim($input['first_name']),
                                'last_name' => trim($input['last_name']),
                                'email' => trim($input['email']),
                                'phone' => $phone,
                                'dob' => [
                                    'day' => date('d', strtotime($input['dob'])),
                                    'month' => date('m', strtotime($input['dob'])),
                                    'year' => date('Y', strtotime($input['dob'])),
                                ],
                                'address' => [
                                    'city' => trim($input['city']),
                                    'country' => trim($input['country']),
                                    'line1' => trim($input['line1']),
                                    'postal_code' => trim($input['postal_code']),
                                    'state' => trim($input['state']),
                                ],
                                'relationship' => [
                                    'director' => true,
                                    'executive' => true,
                                    'owner' => true,
                                    'representative' => true,
                                    'title' => 'Manager',//trim($input['role_in_company']),
                                ],
                                'verification' => [
                                    'additional_document' => [
                                        'front' => $additonal_front_doc->id,
                                        'back' => $additonal_back_doc->id,
                                    ],
                                    'document' => [
                                        'front' => $front_doc->id,
                                        'back' => $back_doc->id,
                                    ],
                                ],
                                    ]
                    );
                    $this->bank_detail->SaveUserAccountDetail($account_create);
                    $this->bank_detail->updateUserAccountDetail($create_person);
                    $this->bank_detail->updateUserUserDetail($r);
                } else {
                    Session::flash('msg', 'Document verification on stripe failed!');
                    Session::flash('message', 'alert-danger');
                    return;
                }
            } else {
                Session::flash('msg', 'Document uploading failed!');
                Session::flash('message', 'alert-danger');
                return;
            }
        } catch (\Stripe\Exception\CardException $e) {
            Session::flash('msg', $e->getError()->message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (\Stripe\Exception\RateLimitException $e) {
            $Message = 'Too many requests made to the API too quickly';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $Message = 'Invalid parameters were supplied to Stripes API';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $Message = 'Authentication with Stripes API failed';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $Message = 'Network communication with Stripe failed';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $Message = 'Display a very generic error to the user, and maybe send yourself an email';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (Exception $e) {
            $Message = 'Something else happened, completely unrelated to Stripe';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        }
    }

    public function TransferToPayout() {
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        $GetDataForPayoutTransfer = $this->bank_detail->selecttoPayout(); //select bank detail where gym patner create own account on stripe
        //$month = date('m', strtotime("- 1 month")); // (last month based on current month)
        $month = date('m', strtotime("- 1 month")); // (last month based on current month)
        $FullMonthName = date('F', strtotime("- 1 month"));
        $msg = 'Transfer fund from ' . $FullMonthName . ' which is based on all visited users on your gyms';
        $get_payou_date = Payoutdate::select('payout_date')->first();
        $payout_date = $get_payou_date->payout_date;
        // echo '<pre>';        print_r($GetDataForPayoutTransfer);die;
        foreach ($GetDataForPayoutTransfer as $value) {
            $gym_owner_id = $value->user_id;
            $Patner_account_id = trim($value->account_id); //external_accounts_bank_id
            $AmountForTransfer_visit = $this->uvg->get_for_TarnsferPayout($gym_owner_id, $month, $payout_date); // select previous month amount on based on month and partner id
            $AmountForTransfer_month = $this->uvg->get_for_TarnsferPayout_month($gym_owner_id, $month, $payout_date); // select previous month amount on based on month and partner id
           // echo '<pre>';print_r($AmountForTransfer_visit);die;
            $TotalAmountForGymPatner = ($AmountForTransfer_visit[0]->total + $AmountForTransfer_month[0]->total);
           // echo 'total='. $TotalAmountForGymPatner.' user_id='.$gym_owner_id.'\n';
           // if(isset($TotalAmountForGymPatner))
           // $TotalAmountForGymPatner = $AmountForTransfer[0]->total;
            if (isset($TotalAmountForGymPatner)) {
                if ($TotalAmountForGymPatner >= '1') {
                    $FinalAmopuntToTransfer = (int) ($TotalAmountForGymPatner * 100);
                    //$FinalAmopuntToTransfer = 200;
                    try {
//                        $arr = array('gym_owner_id'=>$gym_owner_id, 'TotalAmountForGymPatner'=>$TotalAmountForGymPatner);
//                        echo '<pre>';print_r($arr);
                        $transfers = \Stripe\Transfer::create([
                                    'amount' => $FinalAmopuntToTransfer,
                                    'currency' => 'aud',
                                    'destination' => $Patner_account_id,
                                    'transfer_group' => $msg, //'TEST_ORDER_01',
                        ]);

                        if ($transfers) {
                            PayoutTransfer::create([
                                'gym_partner_id' => $gym_owner_id,
                                'transfer_id' => $transfers->id,
                                'balance_transaction_id' => $transfers->balance_transaction,
                                'amount_transfer' => $TotalAmountForGymPatner,
                                'transfer_destination' => $transfers->destination,
                                'destination_payment' => $transfers->destination_payment,
                                'reversals_url' => $transfers->reversals->url,
                                'transfer_group' => $transfers->transfer_group,
                            ]);
                             $next_pay_out_date['payout_date'] = date('Y-m-d', strtotime("+1 months", strtotime($payout_date)));
                             $next_pay_out_date['period_from'] = date('Y-m-01', strtotime($payout_date));
                             $next_pay_out_date['period_to'] = date("Y-m-t", strtotime($payout_date));
                             Payoutdate::where('id','1')->update($next_pay_out_date); 
                        }
                        //echo '<pre>';print_r($transfers);die();
                    } catch (\Stripe\Exception\CardException $e) {
                        $error_msg = $e->getError()->message;
                    } catch (\Stripe\Exception\RateLimitException $e) {
                        $error_msg = 'Too many requests made to the API too quickly';
                    } catch (\Stripe\Exception\InvalidRequestException $e) {
                        $error_msg = 'Invalid parameters were supplied to Stripes API';
                    } catch (\Stripe\Exception\AuthenticationException $e) {
                        $error_msg = 'Authentication with Stripes API failed';
                    } catch (\Stripe\Exception\ApiConnectionException $e) {
                        $error_msg = 'Network communication with Stripe failed';
                    } catch (\Stripe\Exception\ApiErrorException $e) {
                        $error_msg = 'Display a very generic error to the user, and maybe send yourself an email';
                    } catch (Exception $e) {
                        $error_msg = 'Something else happened, completely unrelated to Stripe';
                    }
                    if (isset($error_msg) && !empty($error_msg)) {
                        $amount = $AmountForTransfer[0]->total;
                        $msg_eror = '$' . $amount . ' amount of ' . $FullMonthName . ' month has been failed! this amount is not transfer to gym owner account';
                        PayoutError::create([
                            'gym_partner_id' => $gym_owner_id,
                            'error_msg' => $error_msg,
                            'amount_on_month' => $msg_eror,
                        ]);
                    }

                    //   
                }
            }
            //echo '<pre>';print_r($AmountForTransfer);die;
        }
        //echo '<pre>';print_r($GetDataForPayoutTransfer);die;
    }

    private function CreateAccount($r) {
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        $purpose = $r->purpose;
        $file_name = date('Ymdhis') . '_' . request()->file->getClientOriginalName();
        if (request()->file->move(public_path('image/payout_file'), $file_name)) {
            $image = 'http://localhost/ultimateFitness/public/image/payout_file/' . $file_name;
            //$image = 'https://ufitpass.com/public/image/payout_file/'.$file_name;
            $filePath = $_SERVER['DOCUMENT_ROOT'] . strstr($image, '/public');
            $fp = fopen($filePath, 'rw');
            $front_doc = \Stripe\File::create([
                        'purpose' => $purpose,
                        'file' => $fp,
            ]);

            $back_doc = \Stripe\File::create([
                        'purpose' => $purpose,
                        'file' => $fp,
            ]);

            $additonal_front_doc = \Stripe\File::create([
                        'purpose' => $purpose,
                        'file' => $fp,
            ]);

            $additonal_back_doc = \Stripe\File::create([
                        'purpose' => $purpose,
                        'file' => $fp,
            ]);
            if ($front_doc && $back_doc) {
                $account = \Stripe\Account::create([
                            'type' => 'custom',
                            'country' => 'AU',
                            'email' => 'rohit_test@example.com',
                            'requested_capabilities' => [
                                'card_payments',
                                'transfers',
                            ],
                            'business_type' => "company",
                            'business_profile' => [
                                'mcc' => '5734',
                                'name' => 'File Upload Testing',
                                'product_description' => 'SaaS for the Fitne sdfkjsk fsfjks fsdfjf ',
                                'support_email' => 'abx@gmail.com',
                                'support_phone' => '0401431864',
                                'support_url' => 'https://ufitpass.com/',
                                'url' => 'https://ufitpass.com/',
                            ],
                            //'charge_enabled'=>true,
                            'default_currency' => 'aud',
                            'external_account' => [
                                'object' => 'bank_account',
                                'country' => 'AU',
                                'currency' => 'aud',
                                'account_holder_name' => 'Rohit Test',
                                'account_holder_type' => 'company',
                                'routing_number' => '110000',
                                'account_number' => '000123456',
                            ],
                            'company' => [
                                'name' => 'Rohit Mishra',
                                'directors_provided' => true,
                                'executives_provided' => true,
                                'owners_provided' => true,
                                'phone' => '+61 401431864',
                                'tax_id' => '000000019',
                                'address' => [
                                    'city' => 'Bondi Junction',
                                    'country' => 'AU',
                                    'line1' => '2022 Bondi Junction Australia',
                                    //'line1' => 'sdg hsd f sdh sdhgsdhjfsdfgsdh sd',
                                    'postal_code' => '2022',
                                    'state' => 'NSW',
                                ],
                                'verification' => [
                                    'document' => [
                                        'front' => $front_doc->id,
                                        'back' => $back_doc->id,
                                    ],
                                ],
                            ],
                            'tos_acceptance' => [
                                'date' => time(),
                                'ip' => $_SERVER['REMOTE_ADDR'], // Assumes you're not using a proxy
                                'user_agent' => 'Mozilla/5.0',
                            ],
                ]);

                $create_person = \Stripe\Account::createPerson(
                                $account->id, [
                            'first_name' => 'rohit',
                            'last_name' => 'mishra',
                            'phone' => '+61 401431864',
                            'email' => 'rohit@gmail.com',
                            'dob' => [
                                'day' => '07',
                                'month' => '07',
                                'year' => '1993',
                            ],
                            'address' => [
                                'city' => 'Bondi Junction',
                                'country' => 'AU',
                                'line1' => '2022 Bondi Junction Australia',
                                //'line1' => 'sdg hsd f sdh sdhgsdhjfsdfgsdh sd',
                                'postal_code' => '2022',
                                'state' => 'NSW',
                            ],
                            'relationship' => [
                                'director' => true,
                                'executive' => true,
                                'owner' => true,
                                'representative' => true,
                                'title' => 'CEO',
                            ],
                            'verification' => [
                                'additional_document' => [
                                    'front' => $additonal_front_doc->id,
                                    'back' => $additonal_back_doc->id,
                                ],
                                'document' => [
                                    'front' => $front_doc->id,
                                    'back' => $back_doc->id,
                                ],
                            ],
                                ]
                );

                echo'<pre>';
                print_r($create_person);
                die;
            } else {
                echo 'file not upload';
                die;
            }
        } else {
            echo'error';
        }die;
    }

    public function payoutcharge(Request $r) {
        //echo 'hello'; 
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        // $check_balnace = \Stripe\Balance::retrieve();
        //   echo '<pre>';print_r($check_balnace);die();
        //  $transfers = \Stripe\Transfer::create([
        //      'amount' => 500,
        //      'currency' => 'aud',
        //      'destination' => 'acct_1G71VzJFXvir6DNz',
        //      'transfer_group' => 'TEST_ORDER_01',
        //    ]);   
        // echo '<pre>';print_r($transfers);die();       
        // $bankToken = \Stripe\Token::create([
        //     'bank_account' => [
        //         'country' => 'AU',
        //         'currency' => 'aud',
        //         'account_holder_name' => 'Rohit Test',
        //         'account_holder_type' => 'individual',
        //         'routing_number' => '110000',
        //         'account_number' => '000123456'
        //     ]
        // ]);
        // $createExternalAccount = \Stripe\Account::createExternalAccount(
        //   'acct_1G71VzJFXvir6DNz',
        //   [
        //     'external_account' => $bankToken->id,
        //   ]
        // );
        // echo '<pre>';print_r($createExternalAccount);die();
        // $retrieveExternalAccount = \Stripe\Account::retrieveExternalAccount(
        //       'acct_1G71VzJFXvir6DNz',
        //       'ba_1G71VzJFXvir6DNzZosJXe8U'
        //     );
        // echo '<pre>';print_r($retrieveExternalAccount);die();

        $payout = \Stripe\Payout::create([
                    'amount' => 500,
                    'currency' => 'aud',
                    'description' => 'first payout payment transfer on stripe',
                    'destination' => 'ba_1G497bAoBoRegJgCC1jj2UE2', //'ba_1G71VzJFXvir6DNzZosJXe8U',
                    'method' => 'instant',
                    'source_type' => 'bank_account',
                    'statement_descriptor' => 'first payout payment transfer on stripe veerendermishra@gmail.vom',
        ]);
        echo '<pre>';
        print_r($payout);
        die();
    }

    public function createPerson(Request $r) {
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        $createPerson = \Stripe\Account::createPerson(
                        'acct_1G71VzJFXvir6DNz', ['first_name' => 'veerender', 'last_name' => 'Mishra ']
        );
    }

    public function update_account() {
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        //echo 'update';die;
        $update = \Stripe\Account::update(
                        'acct_1GGM68DyWTBUuRVe', [
                    'metadata' => ['order_id' => '6735'],
                    'email' => 'rohit_test@example.com',
                    'requested_capabilities' => [
                        'card_payments',
                        'transfers',
                    ],
                    'business_type' => "company",
                    'business_profile' => [
                        'mcc' => '5734',
                        'name' => 'Veerender Update gyms',
                        'product_description' => 'veerender gyms update gyms details for users',
                        'url' => 'https://ufitpass.com/',
                    ],
                    'default_currency' => 'aud',
                    'external_account' => [
                        'object' => 'bank_account',
                        'country' => 'AU',
                        'currency' => 'aud',
                        'account_holder_name' => 'Rohit Test',
                        'account_holder_type' => 'company',
                        'routing_number' => '110000',
                        'account_number' => '000123456',
                    ],
                    'company' => [
                        'directors_provided' => true,
                        'executives_provided' => true,
                        'owners_provided' => true,
                        'phone' => '+61 401431864',
                        'tax_id' => '000000019',
                        'address' => [
                            'city' => 'Bondi Junction',
                            'country' => 'AU',
                            'line1' => '2022 mumbai maharastra india',
                            'postal_code' => '2022',
                            'state' => 'NSW',
                        ],
                    ],
                    'tos_acceptance' => [
                        'date' => time(),
                        'ip' => $_SERVER['REMOTE_ADDR'], // Assumes you're not using a proxy
                        'user_agent' => 'Mozilla/5.0',
                    ],
                        ]
        );

        $UpdatePerson = \Stripe\Account::updatePerson(
                        'acct_1GGM68DyWTBUuRVe', 'person_Gny2HQLhykHbMN', [
                    'metadata' => ['order_id' => '6735'],
                    'phone' => '+61 401431864',
                    'email' => 'pramod@gmail.com',
                    'address' => [
                        'city' => 'Bondi Junction',
                        'country' => 'AU',
                        'line1' => '2022 mumbai Junction Australia',
                        'postal_code' => '2022',
                        'state' => 'NSW',
                    ],
                    'relationship' => [
                        'director' => true,
                        'executive' => true,
                        'owner' => true,
                        'representative' => true,
                        'title' => 'CEO',
                    ],
                        ]
        );

        echo '<pre>';
        print_r($update);
        die;
    }

    public function update_add_account(Request $r, $user_id) {
         //echo'<pre>';print_r($r->all());die;
        $input = $r->all();
        $validatedData = $r->validate([
           // 'first_name' => 'required|max:255',
            //'last_name' => 'required|max:255',
            'email' => 'required|max:255',
            'phone' => 'required|max:15',
           // 'dob' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'postal_code' => 'required|max:6',
            'line1' => 'required',
            'account_holder_name' => 'required|max:255',
            'routing_number' => 'required|max:20',
            'account_number' => 'required|max:20',
            'company_name' => 'required|max:255',
            //'role_in_company' => 'required|max:255',
            'tax_id' => 'required|max:13',
            'business_website' => 'required',
        ]);
        $updateAccount = $this->UpdateAccountStripe($r);
        return redirect()->back();
    }

    public function UpdateAccountStripe($r) {
        $input = $r->all();
        include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        //echo 'update';die;
        $phone = '+61 ' . trim($input['phone']);
        try {
            $update = \Stripe\Account::update(
                            trim($input['account_id']), [
                        'metadata' => ['order_id' => '6735'],
                        'email' => trim($input['email']),
                        'requested_capabilities' => [
                            'card_payments',
                            'transfers',
                        ],
                        'business_type' => "company",
                        'business_profile' => [
                            'mcc' => '5734',
                            'name' => trim($input['company_name']),
                            'product_description' => 'Gyms where you grow your body and show your fitness to world',
                            'url' => trim($input['business_website'])
                        ],
                        'default_currency' => 'aud',
                        'external_account' => [
                            'object' => 'bank_account',
                            'country' => trim($input['country']),
                            'currency' => 'aud', //trim($input['currency']),
                            'account_holder_name' => trim($input['account_holder_name']),
                            'account_holder_type' => 'company', //trim($input['account_holder_type']),
                            'routing_number' => trim($input['routing_number']),
                            'account_number' => trim($input['account_number']),
                        ],
                        'company' => [
                            'directors_provided' => true,
                            'executives_provided' => true,
                            'owners_provided' => true,
                            'phone' => $phone,
                            'tax_id' => trim($input['tax_id']),
                            'address' => [
                                'city' => trim($input['city']),
                                'country' => trim($input['country']),
                                'line1' => trim($input['line1']),
                                'postal_code' => trim($input['postal_code']),
                                'state' => trim($input['state']),
                            ],
                        ],
                        'tos_acceptance' => [
                            'date' => time(),
                            'ip' => $_SERVER['REMOTE_ADDR'], // Assumes you're not using a proxy
                            'user_agent' => 'Mozilla/5.0',
                        ],
                            ]
            );

            $UpdatePerson = \Stripe\Account::updatePerson(
                        trim($input['account_id']), trim($input['person_id']), [
                        'metadata' => ['order_id' => '6735'],
                        'email' => trim($input['email']),
                        'phone' => $phone,
                        'address' => [
                            'city' => trim($input['city']),
                            'country' => trim($input['country']),
                            'line1' => trim($input['line1']),
                            'postal_code' => trim($input['postal_code']),
                            'state' => trim($input['state']),
                        ],
                        'relationship' => [
                            'director' => true,
                            'executive' => true,
                            'owner' => true,
                            'representative' => true,
                            'title' => 'Manager',//trim($input['role_in_company']),
                        ],
                            ]
            );
                $this->bank_detail->SaveUserAccountDetail($update);
                $this->bank_detail->updateUserAccountDetail($UpdatePerson);
                $this->bank_detail->updateUserUserDetail($r);
        } catch (\Stripe\Exception\CardException $e) {
            Session::flash('msg', $e->getError()->message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (\Stripe\Exception\RateLimitException $e) {
            $Message = 'Too many requests made to the API too quickly';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $Message = 'Invalid parameters were supplied to Stripes API';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $Message = 'Authentication with Stripes API failed';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $Message = 'Network communication with Stripe failed';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $Message = 'Display a very generic error to the user, and maybe send yourself an email';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        } catch (Exception $e) {
            $Message = 'Something else happened, completely unrelated to Stripe';
            Session::flash('msg', $Message);
            Session::flash('message', 'alert-danger');
            return;
        }
    }
    
    
    public function account_delete($owner_id){
       // echo $owner_id;die;
         include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        
        $get_account_id = Bankdetail::select('account_id')->where('user_id',$owner_id)->first();
       // echo '<pre>';        print_r($get_account_id);die;
        $account = \Stripe\Account::retrieve(
            $get_account_id->account_id
          );
        $account->delete();
        
        if(isset($account->deleted) && $account->deleted==1){
            Bankdetail::where('user_id',$owner_id)->delete();
            $msg = "Stripe account deleted successfully!";
               Session::flash('msg',$msg);
               Session::flash('message','alert-success');
            return redirect()->route('gymlist');
        }else{
             $msg = trans('lang_data.error');
                Session::flash('msg',$msg);
                Session::flash('message','alert-danger');
            return redirect()->route('gymlist');
        }
        //echo '<pre>';        print_r($account);die;
    }
    
    
    public function retrive_stripe_balance(){
         include_once(app_path() . '/Stripe/init.php');
        \Stripe\Stripe::setApiKey(trans('lang_data.Secret_key'));
        
        $available_amount = \Stripe\Balance::retrieve();
        $total_amount = ($available_amount->available[0]->amount/100);
       // echo '<pre>';print_r($total_amount);die;
        //return $total_amount;
        echo date('Y-m-d H:i:s');
    }
    
   

}
