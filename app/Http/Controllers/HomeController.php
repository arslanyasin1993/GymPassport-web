<?php

namespace App\Http\Controllers;

use App\User;
use App\Usergym;
use App\Uservisitedgym;
use App\CorporateUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $user = Landinguser::orderBy('id','DESC')->get();
        $data['total_gym_user'] = User::where('user_type', '3')->where('status', '1')->get()->count();
        $data['total_gym_owner'] = User::where('user_type', '2')->where('status', '1')->get()->count();
        $data['total_visit'] = Uservisitedgym::get()->count();
        $data['total_gym'] = Usergym::where('status', '1')->get()->count();

//        $active_corporate_users = User::query();
        $active_corporates = DB::table('users')->where('is_corporate' , 1)->where('status' , 1)->where('is_delete' , 1)->count();
        $active_corporate_users = DB::table('users')->where('is_corporate_user' , 1)->where('status' , 1)->where('is_delete' , 1)->count();
        return view('home', $data , get_defined_vars());
    }

    public function show()
    {
        $id = auth()->user()->id;
//        2489
        $data['total_active_user'] = User::join('corporate_users', 'corporate_users.user_id', '=', 'users.id')->where('corporate_users.corporate_id', $id)->where(['users.is_delete' => '1'])->where(['users.status' => '1'])->get()->count();
        $data['total_inactive_user'] = User::join('corporate_users', 'corporate_users.user_id', '=', 'users.id')->where('corporate_users.corporate_id', $id)->where(['users.is_delete' => '1'])->where(['users.status' => '0'])->get()->count();
        $data['total_users'] = User::join('corporate_users', 'corporate_users.user_id', '=', 'users.id')->where('corporate_users.corporate_id', $id)->where(['users.is_delete' => '1'])->get()->count();

        $data['corporate_name'] = User::join('corporate_subscription as c', 'c.corporate_id', '=', 'users.id')->where('c.corporate_id', $id)->pluck("corporate_name")->get(0);

        $result = DB::table('users')->select(DB::raw("p.plan_cat_name,c.expired_at"))
            ->join('usersubscriptions as c', 'c.user_id', '=', 'users.id')->join('subscriptions as s', 's.id', '=', 'c.subscription_id')
            ->join('plan_category as p', 'p.id', '=', 's.plan_name')
            ->where('users.id', $id)
            ->ORDERBy('c.id', 'DESC')->limit(1)->get();
        if (isset($result[0])) {
            $data['corporate_plan'] = $result[0]->plan_cat_name;
            $data['expired_at'] = date_create_from_format('Y-m-d H:i:s', $result[0]->expired_at)->format('d-m-Y H:i:s');
        } else {
            $data['corporate_plan'] = '';
            $data['expired_at'] = '';
        }
        return view('corporate-home', $data);
    }

    public function login_account($id){
        $user = User::find($id);
        if(session()->has('last_url') == false){
            session()->put('last_url' , url()->previous());
        }
        session()->put('super_admin' , auth()->user()->id);

        if($user->user_type == 5){
            Auth::loginUsingId($id);
            return redirect()->route('corporate-home');
        }
        if($user->user_type == 3){
            Auth::loginUsingId($id);
            return redirect()->route('userhome');
        }
        if($user->user_type == 2){
            Auth::loginUsingId($id);
            return redirect()->route('ufppatnerhome');
        }
        if($user->user_type == 1){
            Auth::loginUsingId($id);
            $previous_url = session()->get('last_url');

            session()->forget('last_url');
            session()->forget('super_admin');
            return redirect()->to($previous_url);
        }
    }
}
