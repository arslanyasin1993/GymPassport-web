<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  DB;
use App\Landinguser;
use Illuminate\Support\Facades\Validator;
use App\Country;
use App\User;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
class BecomepatnerController extends Controller
{
	function __construct(){
		$this->country = new Country;
		//$this->user = new Landinguser;
		$this->user = new User;
	}

    public function index(){
    	//echo 'sdkfjjfhg';
    	$db['country'] = DB::table('country')->select('id','nicename')->where('id',162)->first();
        $db['state'] = DB::table('state')->select('id','state')
        				->where('country_id',162)->orderBy('state','ASC')->get();
    	return view('web.become_patner',$db);
    }

    public function store(Request $r){
    	$insert = $r->all();
        //unset($insert['form_source'])
    	$insert['password'] = Hash::make('123456789');
    	$insert['user_type'] = '2';
    	$insert['country'] = 162;
        $insert['partner_first_login'] = '1';
        $insert['user_ufp_id'] = mt_rand(10000, 99999);//$input['country'].rand(pow(10, 3-1), pow(10, 3)-1);
    	unset($insert['_token']);
    	//$validator = Validator::make($r->all(), [
    	 $validatedData = $r->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'state' => 'required',
            'address' => 'required|string',
            //'postal_code' => 'required|string|max:10',
            'phone_number' => 'required',
             'registration_fees'=>'required',
             'monthly_fees'=>'required',
            'fitness_facility' => 'required',
//            'country' => 'required',
        ]);
    	 $data = User::create($insert);
    	 $LastId = $data->id;
		 if($LastId > 0){
            if(isset($r->created_from)){
                 $msg = "Gym Partner Added Successfully!";
            }else{
                 $msg = "Thank you for registering your interest. A member of our team will contact you within 24 hours to continue the partnership process";
            }
               
                Session::flash('msg',$msg);
                Session::flash('message','alert-success');
        }else{
                $msg = trans('lang_data.error');
                Session::flash('msg',$msg);
                Session::flash('message','alert-danger');
        }
         return redirect()->back();
		//if ($validator->passes()) {
    		// $emailfound = User::where('email',$input['email'])->first();
    		// if(empty($emailfound)){
    		// 		 $useradd = User::create($insert);
					 // if($useradd > 0){
					 // 	echo 'add';
					 // }else{
					 // 	echo 'notadd';
					 // }
    		// }else{
    		// 	echo 'email';
    		// }
		// }else{
		// 	//echo 'uniqemail';
		// 	//The email has already been taken.
		//   //return response()->json(['error'=>$validator->errors()->all()]);
		// }
    }
}
