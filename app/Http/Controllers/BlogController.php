<?php

namespace App\Http\Controllers;

use App\Blog;
use Session;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $blogs = Blog::where(['is_deleted' => 0, 'is_active' => 1])->orderBy('updated_at', 'desc')->orderBy('id', 'desc')->paginate('3');
        $recentBlogs = Blog::where(['is_deleted' => 0, 'is_active' => 1])->orderBy('id', 'desc')->limit(5)->get();

        return view('blogs-list', compact('blogs', 'recentBlogs'));
    }


    public function detail($slug)
    {
        $blog = Blog::where('slug' , 'LIKE' , '%' . $slug . '%')->where(['is_deleted' => 0, 'is_active' => 1])->first();
        $recentBlogs = Blog:: where(['is_deleted' => 0, 'is_active' => 1])->limit(5)->get();
        return view('blog-details', compact('blog', 'recentBlogs'));
    }

}
