<?php

namespace App\Http\Controllers;

use App\News;
use Session;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $news = News::where(['is_deleted' => 0, 'is_active' => 1])->orderBy('updated_at', 'desc')->orderBy('id', 'desc')->paginate('3');
        $recentNews = News::where(['is_deleted' => 0, 'is_active' => 1])->orderBy('id', 'desc')->limit(5)->get();

        return view('news-list', compact('news', 'recentNews'));
    }


    public function detail($slug)
    {
        $news = News::where('slug' , 'LIKE' , '%' . $slug . '%')->where(['is_deleted' => 0, 'is_active' => 1])->first();
        $recentNews = News:: where(['is_deleted' => 0, 'is_active' => 1])->limit(5)->get();
        return view('news-details', compact('news', 'recentNews'));
    }

}
