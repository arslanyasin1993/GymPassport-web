<?php

namespace App\Http\Controllers\WebGymUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Usercard;
use App\Uservisitedgym;
use App\API\Usersubscription;
use Auth;
use Session;
use DB;
use Illuminate\Support\Facades\Hash;
//use Request;
use Validator;
use App\Http\Controllers\API\UserController as ApiUserController;
class UsergymhomeController extends Controller
{
	function __construct() {
        $this->api_user_controller = new ApiUserController;
        $this->user_sub = new Usersubscription;
        /*$this->plan_category = new Plancategory;
        $this->user_gym = new Usergym;
        $this->user_visit_gym = new Uservisitedgym;*/
    }

    public function index(){
    	$GymUser = Auth::User();
		$current_time = date('Ymdhis');
		$generateRefreshToken = $this->api_user_controller->generateRandomString();
		$GymUser->user_token = $generateRefreshToken;
		$GymUser->user_token_time = new \DateTime();
		$GymUser->update();
    	$user_id = $GymUser->id;
    	session()->put('user_id',$user_id);
    	session()->put('user_token',$GymUser->user_token);
    	$data['current_plan'] = $this->Current_Plan($user_id);
        $data['card_detail']  = Usercard::where('user_id',$user_id)->orderBy('id','desc')->first();
    	$data['user_data'] = (object)$this->api_user_controller->getUserData($user_id); 
    	$data['country'] = DB::table('country')->select('id', 'nicename')->where('id',162)->orderBy('nicename', 'ASC')->get();
   // echo'<pre>';print_r($data['current_plan']);die();
    	return view('web.web_user.home',$data);
    }

      public function Current_Plan($user_id) {
        $currentPlan = Usersubscription::with('plan_category')->where('user_id', $user_id)
                        ->where('status', '1')->first();
        //05-11-2019
            $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
            $is_sub_val = ($is_subscribe_data) ? (boolean)1: (boolean)0; //user subscription data
        //end 
        $res = array();
        if (!empty($currentPlan)) {
            $res['id'] = (string) $currentPlan->id;
            $res['is_subscribe'] = $is_sub_val;
            $res['plan_name'] = $currentPlan->plan_category->plan_cat_name;
            $res['visit_pass'] = ($currentPlan->visit_pass == 'month') ? '&#8734; Pass' : $currentPlan->visit_pass . ' visit Pass';
            $res['purchased_date'] = ($currentPlan->purchased_at) ? date('d/m/Y', strtotime($currentPlan->purchased_at)) : '';
            $res['subscription_amount'] = ($currentPlan->total_payable_amount) ? $currentPlan->total_payable_amount : '';
            $res['expired_date'] = ($currentPlan->expired_at) ? date('d/m/Y', strtotime($currentPlan->expired_at)) : '';
        }
        return $res;
    }

    public function billing_histroy(Request $request){
			if(isset($_GET['bill_val'])){$num = $_GET['bill_val'];}else{ $num= '';}
			if (!empty($num) && $num != null) {
			$data['page'] = $num;
			} else {
			$data['page'] = (request()->segment(2)) ? request()->segment(2) : 1;
			}
			$data['limit'] = 10;
			$offset = ($data['page'] - 1) * $data['limit'];
			$user_id = session()->get('user_id');
			$data['billing_detail'] = Usersubscription::with('plan_category')
										->where('user_id', $user_id)
										->where('status', '0')
										->orderBy('purchased_at', 'DSC')
										->skip($offset)->take($data['limit'])
										->get();//->toArray();
			$total_rows = Usersubscription::with('plan_category')
										->where('user_id', $user_id)
										->where('status', '0')
										->orderBy('purchased_at', 'DSC')
										->get();
			$data['total_rows'] = $total_rows->count();
			//echo "<pre>"; print_r($data['billing_detail']);die();
			return view('web.web_ajax_page.billing_detail',$data);

    } 

    public function check_in_histroy(Request $request){
    	//print_r(Request::segment(2));die;
			if(isset($_GET['check_in_val'])){$num = $_GET['check_in_val'];}else{ $num= '';}
			if (!empty($num) && $num != null) {
			$data['page'] = $num;
			} else {
			$data['page'] = (request()->segment(2)) ? request()->segment(2) : 1;
			}
			$data['limit'] = 10;
			$offset = ($data['page'] - 1) * $data['limit'];
			$user_id = session()->get('user_id');
			$data['check_in_detail'] = Uservisitedgym::with('gym_name')
										->where('user_id', $user_id)
										->orderBy('user_check_in', 'DSC')
										->skip($offset)->take($data['limit'])
										->get();//->toArray();
			$total_rows = Uservisitedgym::where('user_id', $user_id)->get();
			$data['total_rows'] = $total_rows->count();
			//echo "<pre>"; print_r($data['check_in_detail']);die();
			return view('web.web_ajax_page.check_in_details',$data);

    }

    public function update_profile(Request $r){
		$validatedData = $r->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'city' => 'required',
            //'d_o_b' => 'required',
        ]);
        $input = $r->all();
        unset($input['d_o_b']);
        unset($input['_token']);
        //unset($input['email']);
        $input['d_o_b'] = date('Y-m-d',strtotime($r->d_o_b));
        $user_id = session()->get('user_id');
        //echo'<pre>';print_r($input);die();
        $update = User::where('id',$user_id)->update($input);
        //$userdata = User::where('email',$r->email)->first();
        if ($update) {
            $msg = "Profile Update successfully";
            Session::flash('msg',$msg);
            Session::flash('message','alert-success');
        } else {
            $msg = "Nothing to update";//trans('lang_data.error');
            Session::flash('msg',$msg);
            Session::flash('message','alert-danger');
        }
        return redirect()->back();
    }

    public function change_password(Request $r){
		return view('web.web_user.change_password');
    }

    public function password(Request $r){
    	$validatedData = $r->validate([
            'current_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|min:8|required_with:new_password|same:new_password',
        ]);
    	 $user_id = session()->get('user_id');
         $data = User::where('id', $user_id)->first();
            if (Hash::check($r->current_password, $data->password)) {
                if(Hash::check($r->confirm_password, $data->password)){
                    $msg = "Current Password and New Password must be different";
                    Session::flash('message','alert-danger');
                }else{
                    $update = User::whereid($user_id)->update(['password' => Hash::make($r->confirm_password)]);
                    $msg = "Password changed successfully";
                    Session::flash('message','alert-success');
                }
            } else {
                $msg = "Current Password is Incorrect";
                Session::flash('message','alert-danger');
            }
            Session::flash('msg',$msg);

          return redirect()->back();
    }
}
