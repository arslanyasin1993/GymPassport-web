<?php

namespace App\Http\Controllers\WebGymUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Usergym;
use App\Currentlocation;
use App\Uservisitedgym;
use App\Gymcategory;
use App\Gymtiming;
use App\API\Usersubscription;
use App\API\Plancategory;
use Auth;
use Session;
use DB;
use Validator;
use FarhanWazir\GoogleMaps\GMaps;

class FindgymController extends Controller
{
    function __construct()
    {
        //$this->api_user_controller = new ApiUserController;
        $this->user_sub = new Usersubscription;
        $this->gym_mod = new Usergym;
        $this->gym_plan_cat = new Gymcategory;
        $this->plan_cat = new Plancategory; //23-01-2020
    }

    public function index()
    {   
        $map_data = $this->create_map();
        $map = $map_data['map'];
        $gym_data = $map_data['gym_data'];
        $arr['side_gym'] = [];
        foreach ($gym_data as $key => $value) {
            $plan_cat_name = $this->gym_mod->plan_name_by_gym_id($value->gym_id);
            //  if (!empty($plan_cat_name) && isset($plan_cat_name[0]->plan_category_id)) {
            $arr['side_gym'][$key]['gym_cat_id'] = ""; //$plan_cat_name[0]->plan_category_id;
            $arr['side_gym'][$key]['gym_id'] = $value->gym_id;
            $arr['side_gym'][$key]['gym_name'] = $value->gym_name;
            $arr['side_gym'][$key]['gym_logo'] = $value->gym_logo;
            $arr['side_gym'][$key]['gym_address'] = substr($value->gym_address, 0, 30);
            $arr['side_gym'][$key]['distance'] = number_format((float) $value->distance, 1, '.', '');
            // }
        }
        $side_gym_data = $arr['side_gym'];
        //echo'<pre>';print_r($side_gym_data);die();
        return view('web.find_gym.find_gym', compact('map', 'side_gym_data'));
    }

    public function index_new()
    {   
        $map_data = $this->create_map();
        $map = $map_data['map'];
        $gym_data = $map_data['gym_data'];
        $arr['side_gym'] = [];
        foreach ($gym_data as $key => $value) {
            $plan_cat_name = $this->gym_mod->plan_name_by_gym_id($value->gym_id);
            //  if (!empty($plan_cat_name) && isset($plan_cat_name[0]->plan_category_id)) {
            $arr['side_gym'][$key]['gym_cat_id'] = ""; //$plan_cat_name[0]->plan_category_id;
            $arr['side_gym'][$key]['gym_id'] = $value->gym_id;
            $arr['side_gym'][$key]['gym_name'] = $value->gym_name;
            $arr['side_gym'][$key]['gym_logo'] = $value->gym_logo;
            $arr['side_gym'][$key]['gym_address'] = substr($value->gym_address, 0, 30);
            $arr['side_gym'][$key]['full_gym_address'] = $value->gym_address;
            $arr['side_gym'][$key]['distance'] = number_format((float) $value->distance, 1, '.', '');
            $arr['side_gym'][$key]['about'] = $value->about_gym;
            $arr['side_gym'][$key]['staffhours'] = $value->staffhours;
            $arr['side_gym'][$key]['gymcity'] = $value->gymcity;
            // }
        }
        $side_gym_data = $arr['side_gym'];
        //echo'<pre>';print_r($side_gym_data);die();
        return view('web.find_gym.find_gym_new', compact('map', 'side_gym_data'));
    }

    public function ajax_find_gym(Request $r)
    {
        $input = $r->all();
        $search = $input['search'];
        $plan = null;
        if(isset($input['plan'])){
            $plan = $input['plan'];
        }
        $map_data = $this->create_map($search,$plan);
        $map = $map_data['map'];
        $gym_data = $map_data['gym_data'];
        //echo'<pre>';print_r($map);die();
        return view('web.find_gym.ajax_find_gym', compact('map', 'gym_data'));
    }

    public function find_sidebar(Request $r)
    {
        $input = $r->all();

        $search = $input['search'];
        $map_data = $this->create_map($search);
        $gym_data = $map_data['gym_data'];
        $arr['side_gym'] = [];
        foreach ($gym_data as $key => $value) {
            $plan_cat_name = $this->gym_mod->plan_name_by_gym_id($value->gym_id);
            //if (!empty($plan_cat_name) && isset($plan_cat_name[0]->plan_category_id)) {
            $arr['side_gym'][$key]['gym_cat_id'] = ""; //$plan_cat_name[0]->plan_category_id;
            $arr['side_gym'][$key]['gym_id'] = $value->gym_id;
            $arr['side_gym'][$key]['gym_name'] = $value->gym_name;
            $arr['side_gym'][$key]['gym_logo'] = $value->gym_logo;
            $arr['side_gym'][$key]['gym_address'] = substr($value->gym_address, 0, 30);
            $arr['side_gym'][$key]['distance'] = number_format((float) $value->distance, 1, '.', '');
            //}
        }
        $side_gym_data = $arr['side_gym'];
        return view('web.find_gym.ajax_find_sidebar_gym', compact('side_gym_data'));
    }

    public function find_gym_by_plan(Request $r)
    {   
        return $r->all();
        $map_data = $this->create_map();
        $map = $map_data['map'];
        $gym_data = $map_data['gym_data'];
        $arr['side_gym'] = [];
        foreach ($gym_data as $key => $value) {
            $plan_cat_name = $this->gym_mod->plan_name_by_gym_id($value->gym_id);
            //  if (!empty($plan_cat_name) && isset($plan_cat_name[0]->plan_category_id)) {
            $arr['side_gym'][$key]['gym_cat_id'] = ""; //$plan_cat_name[0]->plan_category_id;
            $arr['side_gym'][$key]['gym_id'] = $value->gym_id;
            $arr['side_gym'][$key]['gym_name'] = $value->gym_name;
            $arr['side_gym'][$key]['gym_logo'] = $value->gym_logo;
            $arr['side_gym'][$key]['gym_address'] = substr($value->gym_address, 0, 30);
            $arr['side_gym'][$key]['distance'] = number_format((float) $value->distance, 1, '.', '');
            // }
        }
        $side_gym_data = $arr['side_gym'];
        //echo'<pre>';print_r($side_gym_data);die();
        return view('web.find_gym.find_gym', compact('map', 'side_gym_data'));
    }    

    public function find_gym_detail(Request $r)
    {
        $input = $r->all();
        $gym_id = $input['gym_id'];
        $authuser = Auth::User();
        if (!empty($authuser)) {
            $user_id = $authuser->id;
        }

        $plan_cat_id = $this->gym_mod->cat_id_by_gym_id($gym_id);
        $select = array(
            'id', 'users_id', 'gym_name', 'gym_logo', 'phone_number', 'gym_activities', 'about_gym', 'gym_address',
            'gym_latitude', 'gym_longitude', 'country', 'accept_terms_condition', 'status', 'created_at', 'is_all_day_open', 'is_staff_hours', 'staff_hours'
        );
        $Gymdetails = $this->gym_mod->gym_details($gym_id, $select);
        // echo '<pre>'; print_r($Gymdetails); die; 
        //$is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        //$is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; 
        $plan_cat_name = $this->gym_mod->plan_name_by_gym_id($gym_id);
        $visite = Uservisitedgym::where('gym_id', $gym_id)->get();
        $total_visit = $visite->count();

        if (!empty($Gymdetails)) {
            $data['total_check_in'] = $total_visit;
            $data['gym_id'] = (string) $Gymdetails->id;
            $data['gym_name'] = ($Gymdetails->gym_name) ? $Gymdetails->gym_name : '';
            //$gym_plan_category= trim(str_replace('Fit', '', $plan_cat_name[0]->plan_cat_name));
            $data['plan_cat_id'] = $plan_cat_id ? (string)$plan_cat_id[0]->plan_category_id : '';
            //$data['gym_plan_category'] = trim(str_replace('Gym', '', $gym_plan_category));
            $data['gym_logo'] = ($Gymdetails->gym_logo) ? trans('constants.image_url') . $Gymdetails->gym_logo : '';
            $data['phone_number'] = ($Gymdetails->phone_number) ? (string) $Gymdetails->phone_number : '';
            $data['gym_activities'] = ($Gymdetails->gym_activities) ? $Gymdetails->gym_activities : '';
            $data['about_gym'] = ($Gymdetails->about_gym) ? $Gymdetails->about_gym : '';
            $data['gym_address'] = ($Gymdetails->gym_address) ? $Gymdetails->gym_address : '';
            $data['is_all_day_open'] = ($Gymdetails->is_all_day_open) ? $Gymdetails->is_all_day_open : '';
            $data['is_all_day_open_staff'] = ($Gymdetails->is_staff_hours == 1) ? true : false;
            $data['gym_latitude'] = ($Gymdetails->gym_latitude) ? $Gymdetails->gym_latitude : '';
            $data['gym_longitude'] = ($Gymdetails->gym_longitude) ? $Gymdetails->gym_longitude : '';
            $data['country_id'] = ($Gymdetails->countryname->id) ? (string) $Gymdetails->countryname->id : '';
            $data['country'] = ($Gymdetails->countryname->nicename) ? (string) $Gymdetails->countryname->nicename : '';
            $data['created_at'] = date('d/m/Y', strtotime($Gymdetails->created_at));
            $data['gym_owner_detail']['gym_owner_id'] = (string) $Gymdetails->gym_owner_detail->id;
            $data['gym_owner_detail']['gym_owner_name'] = $Gymdetails->gym_owner_detail->first_name . ' ' . $Gymdetails->gym_owner_detail->last_name;
            $data['gym_owner_detail']['owner_email'] = $Gymdetails->gym_owner_detail->email;
            $data['gym_image'] = [];
            $data['gym_time'] = [];
            $data['facilities'] = [];
            $data['gym_staff_time'] = $Gymdetails->staff_hours ? $Gymdetails->staff_hours : "";
            if (!empty($Gymdetails->gym_image)) {
                foreach ($Gymdetails->gym_image as $key => $value) {
                    $data['gym_image'][$key]['image_id'] = (string) $value->id;
                    $data['gym_image'][$key]['gym_image'] = trans('constants.image_url') . $value->gym_image;
                }
            }
            if (!empty($Gymdetails->gym_time)) {
                $is_open =  array();
                foreach ($Gymdetails->gym_time as $key => $value) {
                    $data['gym_time'][$key]['gym_open_days'] = (string) $value->gym_open_days;
                    $data['gym_time'][$key]['gym_open_timing'] = date('h:i A', strtotime($value->gym_open_timing));
                    $data['gym_time'][$key]['gym_close_time'] = date('h:i A', strtotime($value->gym_close_time));
                    array_push($is_open, $value->gym_open_days);
                }
                $data['is_gym_open'] = $is_open; //array_column($Gymdetails->gym_time,'gym_open_days');
            }
            if (!empty($Gymdetails->facilities)) {
                foreach ($Gymdetails->facilities as $key => $value) {
                    $data['facilities'][$key]['facilities_id'] = (string) $value->gym_facilities->id;
                    $data['facilities'][$key]['facilities_name'] = $value->gym_facilities->facilities ? $value->gym_facilities->facilities : '';
                    $data['facilities'][$key]['facilities_image'] = $value->gym_facilities->imag_url ? $value->gym_facilities->imag_url : '';
                }
            }

            //             if (isset($Gymdetails->staff_timing) && !empty($Gymdetails->staff_timing)) {
            //                  $is_staff_open =  array();
            //                foreach ($Gymdetails->staff_timing as $key => $value) {
            //                    $data['gym_staff_time'][$key]['gym_open_days'] = (string) $value->gym_open_days;
            //                    $data['gym_staff_time'][$key]['gym_open_timing'] = date('h:i A', strtotime($value->gym_open_timing));
            //                    $data['gym_staff_time'][$key]['gym_close_time'] = date('h:i A', strtotime($value->gym_close_time));
            //                    array_push($is_staff_open,$value->gym_open_days);
            //                }
            //                 $data['is_staff_open'] = $is_staff_open;
            //            }


        }
        $gym_detail['gym_detail'] = $data;
        session()->put('before_login_gym_data', '400'); //for login user 
        //echo '<pre>';print_r($data);die();
        return view('web.find_gym.ajax_find_gym_detail', $gym_detail);
    }

    public function create_map($serach = NULL,$plan = null)
    {
        $authuser = Auth::User();
        $user_id = '';
        if (!empty($authuser)) {
            $user_id = $authuser->id;
        }
        $current = Currentlocation::where('user_id', $user_id)->first();
        if ($current) {
            $config['center'] = $current->latitude . ',' . $current->longitude;
        } else {
            $lat_default = '31.582045';
            $long_default = '74.329376';
            $config['center'] = (session()->get('default_lag_long')) ? session()->get('default_lag_long') : $lat_default . ',' . $long_default;
        }
        $config['zoom'] = '11';
        $config['map_height'] = '100%';
        $config['map_width'] = '100%';
        $config['geocodeCaching'] = true;
        $config['scrollwheel'] = false;
        $config['styles'] = array(
            array("name" => "Black Roads", "definition" => array(
                array("featureType" => "all", "stylers" => array(array("saturation" => "-10"))),
                array("elementType" => "geometry", "stylers" => array(array("color" => "#242f3e"))),
                array("elementType" => "labels.text.fill", "stylers" => array(array("color" => "#746855"))),
                array("elementType" => "labels.text.stroke", "stylers" => array(array("color" => "#242f3e"))),
                array("featureType" => "administrative.locality", "elementType" => "labels.text.fill", "stylers" => array(array("color" => "#d59563"))),
                array("featureType" => "poi", "elementType" => "labels.text.fill", "stylers" => array(array("color" => "#d59563"))),
                array("featureType" => "poi.park", "elementType" => "geometry", "stylers" => array(array("color" => "#263c3f"))),
                array("featureType" => "poi.park", "elementType" => "labels.text.fill", "stylers" => array(array("color" => "#6b9a76"))),
                array("featureType" => "road", "elementType" => "geometry", "stylers" => array(array("color" => "#38414e"))),
                array("featureType" => "road", "elementType" => "geometry.stroke", "stylers" => array(array("color" => "#212a37"))),
                array("featureType" => "road", "elementType" => "labels.text.fill", "stylers" => array(array("color" => "#9ca5b3"))),
                array("featureType" => "road.highway", "elementType" => "geometry", "stylers" => array(array("color" => "#746855"))),
                array("featureType" => "road.highway", "elementType" => "geometry.stroke", "stylers" => array(array("color" => "#1f2835"))),
                array("featureType" => "road.highway", "elementType" => "labels.text.fill", "stylers" => array(array("color" => "#f3d19c"))),
                array("featureType" => "transit", "elementType" => "geometry", "stylers" => array(array("color" => "#2f3948"))),
                array("featureType" => "transit.station", "elementType" => "labels.text.fill", "stylers" => array(array("color" => "#d59563"))),
                array("featureType" => "water", "elementType" => "geometry", "stylers" => array(array("color" => "#17263c"))),
                array("featureType" => "water", "elementType" => "labels.text.fill", "stylers" => array(array("color" => "#515c6d"))),
                array("featureType" => "water", "elementType" => "labels.text.stroke", "stylers" => array(array("color" => "#17263c"))),
            ))
        );
        $config['stylesAsMapTypes'] = true;
        $config['stylesAsMapTypesDefault'] = "Black Roads";
        $gmap = new GMaps();
        $gmap->initialize($config);
        //$gym = Usergym::orderBy('id','desc')->get();

        /**************************/
        if (!empty($current)) {
            $lat = session()->get('map_lat') ? session()->get('map_lat') : $current->latitude;
            $lng = session()->get('map_long') ? session()->get('map_long') : $current->longitude;
        } else {
            $lat = session()->get('map_lat') ? session()->get('map_lat') : '28.6505005'; //$current->latitude;
            $lng = session()->get('map_long') ? session()->get('map_long') : '70.6723565326'; //$current->longitude;
        }

        $search = $serach;
        $distance = '100000';
        $status = 1;
        $gym = Usergym::getByDistance($lat, $lng, $distance, $status, $search,$plan);
        // echo '<pre>';print_r($gym);die;
        foreach ($gym as $key => $value) {
            $plan_cat_name = $this->gym_mod->plan_name_by_gym_id($value->gym_id);
            //print_r($plan_cat_name);die;
            //if (!empty($plan_cat_name) && isset($plan_cat_name[0]->plan_category_id)) {
            $logo = trans('constants.image_url') . $value->gym_logo;
            $marker['position'] = $value->gym_latitude . ',' . $value->gym_longitude; //$value->gym_address;
            $html  = '<div class="main_gym">';
            $html .= '<h3 style="margin-top: 0px;">' . $value->gym_name . '</h3>';
            $html .= '<p >' . $value->gym_address . '</p>';
            $html .= '<div class="row">';
            $html .= '<div class="col-sm-7 col-lg-7">';
            $html .= '<img src="' . $logo . '" class="map_image">';
            $html .= '</div>';
            $html .= '<div class="col-sm-4 col-lg-4">';
            $html .= '<button class="btn btn-info map_button" onclick="getGymdetail(' . $value->gym_id . ')">View Detail</button>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $marker['infowindow_content'] = $html;
            $marker['icon'] = trans('constants.image_url') . 'public/website_file/marker_icon/gym-icon2.png';
            $gmap->add_marker($marker);
            //  }
            // $marker['icon_size'] = '';
        }

        $data['map'] = $gmap->create_map();
        $data['gym_data'] = $gym;
        return $data;
    }

    public function direction()
    {
        $config['center'] = 'Noida,India';
        $config['zoom'] = '18';
        $config['map_height'] = '500px';
        $config['geocodeCaching'] = true;
        $config['scrollwheel'] = false;

        $config['directions'] = true;
        $config['directionsStart'] = 'Unnamed Road, Arya Nagar, Sewa Nagar, Ghaziabad, Uttar Pradesh 201003, India';
        $config['directionsEnd'] = 'RipenApps Technologies, A Block, Sector 63, Noida, Uttar Pradesh';
        $config['directionsDivID'] =  'directionsDiv';

        $gmap = new GMaps();
        $gmap->initialize($config);
        $map = $gmap->create_map();
        return view('web.find_gym.get_direction_find_gym', compact('map'));
    }

    public function getlatlong(Request $r)
    {
        $input = $r->all();
        $user_id = '';
        if (isset(Auth::User()->id)) {
            $authuser = Auth::User();
            $user_id = $authuser->id;
        }
        $before_login_lat_long = $input['latitude'] . ',' . $input['longitude'];
        session()->put('default_lag_long', $before_login_lat_long);
        session()->put('map_lat', $input['latitude']);
        session()->put('map_long', $input['longitude']);
        unset($input['_token']);

        if (!empty($user_id)) {
            $data = Currentlocation::where('user_id', $user_id)->first();
            if ($data) {
                Currentlocation::where('user_id', $user_id)->update($input);
            } else {
                $input['user_id'] = $user_id;
                Currentlocation::create($input);
            }
        }

        //return $input;
    }

    public function gym_check_in(Request $r)
    {

        $response = array();
        $input = $r->all();

        $authuser = Auth::User();
        $user_id = $authuser->id;
        $gym_id = $input['gym_id'];
        //dd($user_id);
        $gym_price_per_visit=Usergym::select('id','gym_price_per_visit')->where('id',$gym_id)->first();
        $user_plan = Usersubscription::select('id', 'plan_name', 'expired_at', 'visit_pass', 'plan_duration', 'plan_time', 'created_at', 'total_payable_amount', 'amount')->where('status', 1)->where('user_id', $user_id)->first();
        //echo'<pre>';print_r($user_plan);die;
         //dd($gym_price_per_visit->gym_price);
        
        if (!empty($user_plan)) {
            $insert['user_id'] = $user_id;
            $insert['gym_id'] = $gym_id;
            $insert['usersubscription_id'] = $user_plan->id;
            $insert['check_in_status'] = 1;
            $insert['user_check_in'] = date('Y-m-d H:i:s');
            $insert['gym_earn_amount'] =$gym_price_per_visit->gym_price_per_visit;
            $allData = (object) [];
            $is_all_day_open = Usergym::select('is_all_day_open')->where('id', $gym_id)->first(); //select 24* 7 open gym 
            $gym_timing = $this->gym_open_timing($gym_id); //checking open and close gym timing
            $lag = session()->get('map_lat');
            $long = session()->get('map_long');
            $gym_distance = $this->gym_mod->check_in_user_distance($lag, $long, $gym_id);
            if (empty($gym_distance)) {
                $response['flag'] = false;
                $response['message'] = 'This Gym is not in 500m range from your location.You must be at a UFP gym location to check in.';
                return $response;
            } elseif ($is_all_day_open->is_all_day_open != 1) {
                if ($gym_timing == '0' || $gym_timing == '2') {
                    $msg = ($gym_timing == '0') ? 'Currently gym is closed.You can not check-in at this time' : 'Gym is closed today';
                    $response['flag'] = false;
                    $response['message'] = $msg; //'You need to upgrade your plan to access this gym';
                    return $response;
                }
            } elseif (empty($user_plan)) {
                $response['flag'] = false;
                $response['message'] = 'You have not any active plan';
                return $response;
            } else {
                $last_check_in_date = Uservisitedgym::select('user_check_in')->where('user_id', $user_id)->where('usersubscription_id', $user_plan->id)->orderBy('id', 'DESC')->limit(1)->first();
                if($last_check_in_date){
                $last_ggym_check = $last_check_in_date->user_check_in ? date('Y-m-d h:i:s', strtotime($last_check_in_date->user_check_in)) : '';
            }else{
                $last_ggym_check ='';
            }
                    $current_date = ' ' . date('Y-m-d h:i:s');
                    $to_time = strtotime($current_date);
                    $from_time = strtotime($last_ggym_check);
                    $check_time = round(abs($to_time - $from_time) / 60);
                    $timediff = strtotime(date('Y-m-d h:i:s')) - strtotime($last_ggym_check);
                   //dd($check_time);
                $total_visit = Uservisitedgym::where('user_id', $user_id)->where('usersubscription_id', $user_plan->id)->get()->count();
                $total_visit_on_Gym = Uservisitedgym::where('user_id', $user_id)->where('gym_id', $gym_id)->where('usersubscription_id', $user_plan->id)->get()->count();
                //echo $total_visit;die;
                 if($check_time > 10){
                if ($total_visit < 15) {
                     
                    $add = Uservisitedgym::create($insert);
                    $response['flag'] = true;
                    $response['message'] = 'Your Check-in was successful';
                    if ($total_visit_on_Gym+1 == 8) {
                        $response['flag'] = true;
                        $response['message'] = 'You have used your 8th check-in to this Gym under your current pass. You will not will be able to check-in to this Gym again until your pass renews';
                        //Usersubscription::where('user_id', $user_id)->update(['status' => 0]);
                    }
                } else {
                    $response['flag'] = false;
                    $response['message'] = 'You have used all of your check-ins under this pass. Please renew your pass to continue visiting gyms';
                }
            }else{
                    $response['flag'] = false;
                    $response['message'] = 'Please wait 10 minutes to check-in again';

                }
            }
        } else {
            $response['flag'] = false;
            $response['message'] = 'You plan has been expired';
        }
        return $response;
    }

    public function gym_check_in_old(Request $r)
    {
        $response = array();
        $input = $r->all();
        $authuser = Auth::User();
        $user_id = $authuser->id;
        $gym_id = $input['gym_id'];
        $user_plan = Usersubscription::select('id', 'plan_name', 'expired_at', 'visit_pass', 'plan_duration', 'plan_time', 'created_at', 'total_payable_amount', 'amount')->where('status', 1)->where('user_id', $user_id)->first();
        //echo'<pre>';print_r($user_plan);die;
        $allData = (object) [];
        $is_all_day_open = Usergym::select('is_all_day_open')->where('id', $gym_id)->first(); //select 24* 7 open gym 
        $gym_timing = $this->gym_open_timing($gym_id); //checking open and close gym timing
        $lag = session()->get('map_lat');
        $long = session()->get('map_long');
        $gym_distance = $this->gym_mod->check_in_user_distance($lag, $long, $gym_id);
        if (empty($gym_distance)) {
            $response['flag'] = false;
            $response['message'] = 'This Gym is not in 500m range from your location.You must be at a UFP gym location to check in.';
            return $response;
        } elseif ($is_all_day_open->is_all_day_open != 1) {
            if ($gym_timing == '0' || $gym_timing == '2') {
                $msg = ($gym_timing == '0') ? 'Currently gym is closed.You can not check-in at this time' : 'Gym is closed today';
                $response['flag'] = false;
                $response['message'] = $msg; //'You need to upgrade your plan to access this gym';
                return $response;
            }
        } elseif (empty($user_plan)) {
            $response['flag'] = false;
            $response['message'] = 'You have not any active plan';
            return $response;
        } else {
            $final_divided_amount = $this->given_amount_patner_after_visit($user_plan);
            $user_plan_category_id = ($user_plan->plan_name) ? $user_plan->plan_name : '';
            $user_subscription_id = ($user_plan->id) ? $user_plan->id : '';
            $user_have_visit_pass = ($user_plan->visit_pass) ? $user_plan->visit_pass : '';
            $plan_end_date = date('Y/m/d', strtotime($user_plan->expired_at)); //plan_end date
            $currentdate = date('Y/m/d'); //current date
            $get_plan_wise_gym = $this->gym_plan_cat->get_gym_plan_id($user_plan_category_id);
            $gym_array = array_column($get_plan_wise_gym, 'user_gym_id');
            if (!in_array($gym_id, $gym_array)) {
                $get_user_current_plan_name = $this->plan_cat->web_get_subscription_data($user_plan->plan_name);
                $get_gym_plan_name = $this->gym_plan_cat->get_gym_plan($gym_id);
                $response['flag'] = false;
                //$gym_msg = 'This gym is available to '.$get_gym_plan_name[0]->plan_cat_name.' your plan is '.$get_user_current_plan_name->plan_cat_name.'. If you wish to visit this gym, please upgrade your membership plan.';
                $gym_msg = "This club is not available in your current plan. If you wish to check-in to this club, please upgrade your membership plan and get access to more clubs like this!";
                $response['message'] = $gym_msg; //'This gym is not added any subscription plan please contact your admin';
            } elseif (strtotime($currentdate) <= strtotime($plan_end_date)) {
                $total_visit = $this->gym_mod->usertotal_visit($user_id, $user_subscription_id, $user_have_visit_pass);
                $total_visit = ($total_visit) ? $total_visit : '';
                $insert['user_id'] = $user_id;
                $insert['gym_id'] = $gym_id;
                $insert['usersubscription_id'] = $user_subscription_id;
                $insert['check_in_status'] = 1;
                $insert['user_check_in'] = date('Y-m-d H:i:s');
                $insert['gym_earn_amount'] = $final_divided_amount;
                $last_check_in_date = Uservisitedgym::select('user_check_in')->where('user_id', $user_id)->where('usersubscription_id', $user_subscription_id)->orderBy('id', 'DESC')->limit(1)->first();
                if (empty($last_check_in_date)) {  //check if user have not any visit with current plan
                    $add = Uservisitedgym::create($insert);
                    $total_visit = $this->gym_mod->usertotal_visit($user_id, $user_subscription_id, $user_have_visit_pass);
                    $total_visit = ($total_visit) ? $total_visit : '';
                    if ($user_have_visit_pass == 'month') { // if user monthly plan
                        // $add = Uservisitedgym::create($insert);
                        $arr['is_subscribed'] = (bool) 1;
                        $response['flag'] = true;
                        $response['message'] = 'Your Check-in was successful';
                    } else {
                        if (($user_have_visit_pass - $total_visit) == '0') {
                            $arr['is_subscribed'] = (bool) 0;
                            $arr['is_last_visit'] = (bool) 1;
                            $update['status'] = '0';
                            $update['expired_at'] = date('Y-m-d H:i:s');
                            $response['flag'] = true;
                            //$response['message'] = 'Your Check-in was successful Now you have not any remaining visits left, you need to renew your subscription to access the Gyms';
                            $response['message'] = "Your Check-in was successful! You have now used your last remaining visit.You will need to renew your subscription to continue your membership plan.";
                            Usersubscription::where('user_id', $user_id)->update($update);
                        } else {
                            $arr['is_subscribed'] = (bool) 1;
                            $arr['is_last_visit'] = (bool) 0;
                            $response['flag'] = true;
                            $response['message'] = 'Your Check-in was successful';
                        }
                    }
                } else {
                    $last_ggym_check = $last_check_in_date->user_check_in ? date('Y-m-d h:i:s', strtotime($last_check_in_date->user_check_in)) : '';
                    $current_date = ' ' . date('Y-m-d h:i:s');
                    $to_time = strtotime($current_date);
                    $from_time = strtotime($last_ggym_check);
                    $check_time = round(abs($to_time - $from_time) / 60);
                    $timediff = strtotime(date('Y-m-d h:i:s')) - strtotime($last_ggym_check);
                    if ($check_time > 10) { // check user check after 24 hours last visit
                        //'more than 24 hours';
                        if ($user_have_visit_pass == 'month') { // if user monthly plan
                            $add = Uservisitedgym::create($insert);
                            $arr['is_subscribed'] = (bool) 1;
                            $arr['is_last_visit'] = (bool) 0;
                            $response['flag'] = true;
                            $response['message'] = 'Your Check-in was successful';
                        } else {
                            if ($total_visit < $user_have_visit_pass) { //check total user visit current subscription
                                $add = Uservisitedgym::create($insert);
                                $total_visit = $this->gym_mod->usertotal_visit($user_id, $user_subscription_id, $user_have_visit_pass);
                                if (($user_have_visit_pass - $total_visit) == '0') {
                                    $arr['is_subscribed'] = (bool) 0;
                                    $arr['is_last_visit'] = (bool) 1;
                                    $update['status'] = '0';
                                    $update['expired_at'] = date('Y-m-d H:i:s');
                                    $response['flag'] = true;
                                    //$response['message'] = 'Your Check-in was successful Now you have not any remaining visits left, you need to renew your subscription to access the Gyms';
                                    $response['message'] =  "Your Check-in was successful! You have now used your last remaining visit.You will need to renew your subscription to continue your membership plan.";
                                    Usersubscription::where('user_id', $user_id)->update($update);
                                } else {
                                    $arr['is_subscribed'] = (bool) 1;
                                    $arr['is_last_visit'] = (bool) 0;
                                    $response['flag'] = true;
                                    $response['message'] = 'Your Check-in was successful';
                                }
                            } else {
                                $response['flag'] = false;
                                $response['message'] = 'You have completed all your visited';
                            }
                        }
                    } else {
                        $response['flag'] = false;
                        $response['message'] = 'Please wait 10 minutes to check-in again';
                    }
                }
            } else {
                $response['flag'] = false;
                $response['message'] = 'You plan has been expired';
            }
        }
        return $response;
    }

    public function gym_open_timing($gym_id)
    {
        if (date('l') == 'Sunday') {
            $day = '0';
        }
        if (date('l') == 'Monday') {
            $day = '1';
        }
        if (date('l') == 'Tuesday') {
            $day = '2';
        }
        if (date('l') == 'Wednesday') {
            $day = '3';
        }
        if (date('l') == 'Thursday') {
            $day = '4';
        }
        if (date('l') == 'Friday') {
            $day = '5';
        }
        if (date('l') == 'Saturday') {
            $day = '6';
        }
        $gym_time_data = Gymtiming::where('users_gym_id', $gym_id)->where('gym_open_days', $day)->first();
        if (empty($gym_time_data)) {
            $data = '2';
        } else {
            $open_time = $gym_time_data->gym_open_timing;
            $close_time = $gym_time_data->gym_close_time;
            $gym_open_days = $gym_time_data->gym_open_days;
            $current_time = date('H:i:s');
            if ($current_time >= $open_time && $current_time <= $close_time && $gym_open_days == $day) {
                //  19:22:28           13:30        19:22:28        21:30           1              1
                $data = '1';
            } else {
                $data = '0';
            }
        }
        return $data;
    }

    public function given_amount_patner_after_visit($user_subscription)
    {
        //dd($user_subscription);die;
        $for_patner_percentage = 81;
        $user_payable_amount = $user_subscription->amount;
        $total_visit_pass = 10;//$user_subscription->visit_pass;
        if ($total_visit_pass != 'month') {
            $for_final_distribute_amount = ($for_patner_percentage / 100) * $user_payable_amount;
            $amount_for_patner = ($for_final_distribute_amount / $total_visit_pass);
            $final_amount_for_gym_patner = number_format((float) $amount_for_patner, 2, '.', '');
        } else {
            $final_amount_for_gym_patner = '00.00';
        }

        return $final_amount_for_gym_patner;
    }
}
