<?php

namespace App\Http\Controllers\WebGymUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\API\Subscription;
use App\API\Usersubscription;
use App\API\Plancategory;
use Validator;
use DB;
use Session;
use App\Usergym;
use App\Gymcategory;
use App\Uservisitedgym;
use App\Coupon;
use App\Couponplan;
use App\Userusecoupon;
use Auth;
use App\Usercard;
class SubscriptionController extends Controller {

    function __construct() {
        $this->plan_category = new Plancategory;
        $this->user_sub = new Usersubscription;
        $this->user_gym = new Usergym;
        $this->user_card = new Usercard;
        $this->Gymcategory = new Gymcategory;
        $this->user_visit_gym = new Uservisitedgym;
    }

    public function index() {
        $getdata = $this->plan_category->get_subscription_data();
        if (!empty($getdata)) {
            foreach ($getdata as $key => $value) {
                $totalgym = $this->Gymcategory->count_total_gym($value->id);//Gymcategory::where('plan_category_id','<=', $value->id)->get();
                $res[$key]['plan_cat_id'] = (string) $value->id;
                $res[$key]['plan_cat_name'] = $value->plan_cat_name;
                $res[$key]['category_description'] = $value->description;
                $res[$key]['total_gym'] = $totalgym[0]->total_gym;//$totalgym->count();
                $res[$key]['plan_detail'] = [];
                if (isset($value->plan_detail) && !empty($value->plan_detail)) {
                    foreach ($value->plan_detail as $key1 => $value) {
                        $res[$key]['plan_detail'][$key1]['plan_id'] = (string) $value->id;
                        $visit_duration = ($value->visit_pass == 'month') ? 'Monthly ∞' : $value->visit_pass . ' Visit Pass';
                        $res[$key]['plan_detail'][$key1]['visit_pass'] = $visit_duration;
                        $duration = $value->description;
                        $res[$key]['plan_detail'][$key1]['plan_duration'] = $duration;
                            // $percentage = '10';
                            // $gst = (($percentage / 100) * $value->amount);
                            // $resss['gst'] = number_format((float) $gst, 2, '.', '');
                            // $total = ($value->amount + $resss['gst']);
                            // $total_amt = number_format((float) $total, 2, '.', '');
                        $total_amt = number_format((float) $value->amount, 2, '.', '');
                       // $gst = "10";
                        $res[$key]['plan_detail'][$key1]['amount'] = (string) $total_amt;//($value->amount + $gst);
                    }
                }
            }
        }
        $data['membership'] = $res;
        //echo'<pre>';print_r($data['membership']);die();
        return view('web.subscription_plan.subscription_plan', $data);
    }

    public function plan_wise_gym($plan_id) {
        $plan_name = Plancategory::where('id', $plan_id)->first();
        $data['plan_id'] = $plan_id;
        if ($plan_name) {
            $data['plan_name'] = $plan_name->plan_cat_name;
        } else {
            $data['plan_name'] = '';
        }
        return view('web.subscription_plan.plan_wise_gym_list', $data);
    }

    public function plan_wise_gym_list(Request $r) {
        $input = $r->all();
        $plan_id = $input['plan_id'];
        $search = $input['search'];
        $data = $this->Gymcategory->get_gymname_plan_wise($plan_id, $search);
        $gym_data = $this->gym_list($data);
       
        echo $gym_data;
    }

    public function gym_list($data) {
        $val = '';
         //echo '<pre>';print_r($data);die();
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $val .= '<a href="#" onclick="open_find_gym('.$value->gym_id.')"><div class="card-box col-md-3 col-lg-3 col-sm-12 col-12">';
                $val .= "<img src='" . trans('constants.image_url') . $value->gym_logo . "' alt='image' class='img-fluid img-fluid-gym'>";
                $val .= '<h3 class="gym-name ">' . $value->gym_name . '</h3>';
                $val .= '<div class="address-box">';
                $val .= '<a href="#" onclick="open_find_gym('.$value->gym_id.')"><p>' . mb_strimwidth($value->gym_address, 0, 25, "...") . '</p></a>';
                $val .= '</div>';
                $val .= '</div></a>';
            }
        } else {
            $val .= '<p class="msgcolor">Gym not available on this membership plan</p>';
        }
        return $val;
    }

    public function subscription_plan($plan_id) {
        $getdata = $this->plan_category->web_get_subscription_data($plan_id);
        if (!empty($getdata)) {
            $cat_id = (string) $getdata->id;
            $res['plan_cat_id'] = (string) $getdata->id;
            $res['plan_cat_name'] = $getdata->plan_cat_name;
            $res['category_description'] = $getdata->description;
            $res['plan_detail'] = [];
            if (isset($getdata->plan_detail) && !empty($getdata->plan_detail)) {
                foreach ($getdata->plan_detail as $key1 => $getdata) {
                    $res['plan_detail'][$key1]['plan_id'] = (string) $getdata->id;
                    $visit_duration = ($getdata->visit_pass == 'month') ? 'Monthly ∞' : $getdata->visit_pass . ' Visit Pass';
                    $res['plan_detail'][$key1]['visit_pass'] = $visit_duration;
                    $res['plan_detail'][$key1]['plan_cat_id'] = $cat_id;
                    $duration = $getdata->description;
                    $res['plan_detail'][$key1]['plan_duration'] = $duration;
                    $gst = "10";
                    $res['plan_detail'][$key1]['amount'] = (string) ($getdata->amount + $gst);
                }
            }
        } else {
            $res = [];
        }

        $data['membership_detail'] = $res;
        // echo '<pre>';print_r($data['membership_detail']);die();
        return view('web.subscription_plan.select_subscription_plan', $data);
    }

    public function subscription_plan_detail(Request $r) {
        //  print_r($r->all());die();
        $userid = Auth::user();
        if(!isset($userid->id)){
            session()->put('before_login_data','404');
        }
        $plan_id = $r->plan_id;
        $plan_cat_id = $r->plan_cat_id;
        $getdata = Subscription::whereid($plan_id)->first();
        if (!empty($getdata)) {
            $res['plan_cat_id'] = $plan_cat_id;
            $res['plan_id'] = (string) $plan_id;
            $base_amount = (string) $getdata->amount;
            $percentage = 10;
            $gst = ($percentage / 100) * $base_amount;
            $res['gst'] = number_format((float) $gst, 2, '.', '');
            $base_amount_cal = ($base_amount - $gst);
            $res['base_amount'] = number_format((float) $base_amount_cal, 2, '.', '');
            $res['total_payable'] = number_format((float) $base_amount, 2, '.', '');
            // $res['base_amount'] = (string) $getdata->amount;
            // $percentage = '10';
            // $gst = (($percentage / 100) * $res['base_amount']);
            // $res['gst'] = number_format((float) $gst, 2, '.', '');
            // $total = ($getdata->amount + $res['gst']);
            // $total_amt = number_format((float) $total, 2, '.', '');
            // $res['total_payable'] = (string) $total_amt; //($getdata->amount + $res['integrated_gst']);
        } else {
            $res = [];
        }
        $data['plan_detail'] = $res;
        // echo '<pre>';print_r($res);die();
        return view('web.subscription_plan.subscription_plan_detail', $data);
    }

    public function card_detail() {
        // echo decrypt($plan_id);die; 
        // echo decrypt($base_amount);
        // echo decrypt($gst); 
        // echo decrypt($total_amount); die;
       // echo $plan_id.' '.$base_amount.' '.$gst.' '.$total_amount;die();
        $userid = Auth::user();
        $user_id = $userid->id;
        $user_card_detail = $this->user_card->usercard($user_id);
        $data['card_number'] = '';
        $data['exp_month'] = '';
        $data['exp_year'] = '';
        if(!empty($user_card_detail)){
            $data['card_number'] = $user_card_detail->user_card_number;
            $data['exp_month'] = $user_card_detail->user_card_exp_month;
            $data['exp_year'] = $user_card_detail->user_card_exp_year;
        }
       //echo '<pre>';print_r($data);die;
       $data['plan_id'] =1;
       $data['payable_amount'] =7500;
        // $data['plan_id'] = decrypt($plan_id);
        // $data['base_amount'] = decrypt($base_amount);
        // $data['gst'] = decrypt($gst);
        // $data['total_amount'] = decrypt($total_amount);
       // $data['coupon_discount_amount'] = $coupon_discount_amount ? decrypt($coupon_discount_amount) : '';
       // $data['total_payable_amount'] = decrypt($total_payable_amount);
      //  $data['coupon_id'] = $coupon_id ? decrypt($coupon_id) : '';
      //  $data['coupon_code'] = $coupon_code ? decrypt($coupon_code) : '';
       // $data['counpon_discount'] = $counpon_discount ? decrypt($counpon_discount) : '';
        // echo'<pre>';print_r($data);die;
        return view('web.subscription_plan.card_detail', $data);
    }

    public function ajax_price(Request $r) {
        $plan_id = $r->plan_id;
        $data = Subscription::select('amount', 'description')->whereid($plan_id)->first();
        if ($data) {
           // $gst = '10';
            /*$percentage = '10';
            $gst = (($percentage / 100) * $data->amount);
            $resss['gst'] = number_format((float) $gst, 2, '.', '');
            $total = ($data->amount + $resss['gst']);
            $total_amt = number_format((float) $total, 2, '.', '');*/
            $total_amt = number_format((float) $data->amount, 2, '.', '');
            //$amount = ($data->amount + $gst);
            $description = $data->description;
            $array['amount'] = '$' . $total_amt;
            $array['description'] = $description;
            return json_encode($array);
        }
    }

    public function apply_promo_code(Request $r) {
        $response = array();
        $input = $r->all();
        unset($input['_token']);
        // print_r($input);
        $GymUser = Auth::User();
        $user_id = $GymUser->id;
        $plan_id = trim($input['plan_id']);
        $base_amount = trim($input['base_amount']);
        $gst = trim($input['gst']);
        $coupon_code = trim($input['coupon_code']);
        $plan_cat_id = trim($input['plan_cat_id']);
        $payable_amount = trim($input['payable_amount']);
        $is_valid_coupon = Coupon::where('coupon_code', $coupon_code)->where('is_delete','1')->where('status','1')->first();
        if ($is_valid_coupon) {
            $coupon_id = $is_valid_coupon->id;
            $is_user_plan_valid = Couponplan::where('coupon_id', $coupon_id)->where('plan_cat_id', $plan_cat_id)->first();
            if ($is_user_plan_valid) {
                if ($is_valid_coupon->valid_for == 1) {
                    $coupon_id = $is_valid_coupon->id;
                    $discount = $is_valid_coupon->discount;
                    $coupon_code = $is_valid_coupon->coupon_code;
                    $final_payable = ($payable_amount - ($payable_amount * $discount) / 100);
                    $final_payable_amount = number_format((float) $final_payable, 2, '.', '');
                    $discount_amount = ($payable_amount - $final_payable_amount);
                    $discount_amount = number_format((float) $discount_amount, 2, '.', '');
                    $response['flag'] = true;
                    $response['message'] = 'Coupon code apply successfully';
                    $response['result'] = array('coupon_code' => $coupon_code, 'discount' => $discount, 'final_amt' => $final_payable_amount, 'discount_amount' => $discount_amount);
                    $response['submit_button'] = "<a href='" . route('apply_coupon_card_detail', ['plan_id' => encrypt($plan_id), 'base_amount' => encrypt($base_amount), 'gst' => encrypt($gst), 'total_payable' => encrypt($payable_amount), 'total_payable_amount' => encrypt($final_payable_amount), 'coupon_id' => encrypt($coupon_id), 'coupon_code' => encrypt($coupon_code), 'coupon_discount' => encrypt($discount), 'coupon_discount_amount' => encrypt($discount_amount)]) . "'><button type='button' class='btn btn-danger pay-btn'>Pay $ " . $final_payable_amount . "</button></a>";
                } else {
                    $total_use_coupon = Userusecoupon::where('coupon_code', $coupon_code)->where('user_id', $user_id)->get();
                    $total_num_of_use = $total_use_coupon->count();
                    if ($is_valid_coupon->total_use_coupon > $total_num_of_use) {
                        $coupon_id = $is_valid_coupon->id;
                        $discount = $is_valid_coupon->discount;
                        $coupon_code = $is_valid_coupon->coupon_code;
                        $final_payable = ($payable_amount - ($payable_amount * $discount) / 100);
                        $final_payable_amount = number_format((float) $final_payable, 2, '.', '');
                        $discount_amount = ($payable_amount - $final_payable_amount);
                        $discount_amount = number_format((float) $discount_amount, 2, '.', '');
                        $response['flag'] = true;
                        $response['message'] = 'Coupon code apply successfully';
                        $response['result'] = array('coupon_code' => $coupon_code, 'discount' => $discount, 'final_amt' => $final_payable_amount, 'discount_amount' => $discount_amount);
                        $response['submit_button'] = "<a href='" . route('apply_coupon_card_detail', ['plan_id' => encrypt($plan_id), 'base_amount' => encrypt($base_amount), 'gst' => encrypt($gst), 'total_payable' => encrypt($payable_amount), 'total_payable_amount' => encrypt($final_payable_amount), 'coupon_id' => encrypt($coupon_id), 'coupon_code' => encrypt($coupon_code), 'coupon_discount' => encrypt($discount), 'coupon_discount_amount' => encrypt($discount_amount)]) . "'><button type='button' class='btn btn-danger pay-btn'>Pay $ " . $final_payable_amount . "</button></a>";
                    } else {
                        $response['flag'] = false;
                        $response['message'] = 'You have already used this coupon';
                    }
                }
//                        $already_use_coupon = Userusecoupon::where('coupon_code',$coupon_code)->where('user_id',$user_id)->first();
//                        if(empty($already_use_coupon)){
//                            $coupon_id= $is_valid_coupon->id;
//                            $discount = $is_valid_coupon->discount;
//                            $coupon_code = $is_valid_coupon->coupon_code;
//                            $final_payable= ($payable_amount-($payable_amount*$discount)/100);
//                            $final_payable_amount = number_format((float) $final_payable, 2, '.', '');
//                            $discount_amount = ($payable_amount - $final_payable_amount);
//                            $discount_amount = number_format((float) $discount_amount, 2, '.', '');
//                            $response['flag'] = true;
//                            $response['message'] = 'Coupon code apply successfully';
//                            $response['result'] = array('coupon_code'=>$coupon_code,'discount'=>$discount,'final_amt'=>$final_payable_amount, 'discount_amount'=>$discount_amount);
//                            $response['submit_button'] = "<a href='".route('apply_coupon_card_detail',['plan_id'=>encrypt($plan_id),'base_amount'=>encrypt($base_amount),'gst'=>encrypt($gst),'total_payable'=>encrypt($payable_amount),'total_payable_amount'=>encrypt($final_payable_amount),'coupon_id'=>encrypt($coupon_id),'coupon_code'=>encrypt($coupon_code),'coupon_discount'=>encrypt($discount),'coupon_discount_amount'=>encrypt($discount_amount)])."'><button type='button' class='btn btn-danger pay-btn'>Pay $ ".$final_payable_amount."</button></a>";
//                            
//                        }else{
//                            $response['flag'] = false;
//                            $response['message'] = 'You are already use this coupon';
//                        }
            } else {
                $response['flag'] = false;
                $response['message'] = 'You are not eligible for this coupon';
            }
        } else {
            $response['flag'] = false;
            $response['message'] = 'Invalid coupon code';
        }

        return $response;
    }

    public function CancelSubscription() {
        $response = array();
        $GymUser = Auth::User();
        $user_id = $GymUser->id;
        $date = date('Y-m-d H:i:s');
        $getdata = Usersubscription::where('user_id', $user_id)->where('status', 1)->update(['status' => 0,'expired_at'=>$date]);
        //05-11-2019
        $is_subscribe_data = $this->user_sub->is_subscribe($user_id);
        $is_sub_val = ($is_subscribe_data) ? (boolean) 1 : (boolean) 0; //user subscription data
        //end 
        if ($getdata) {
            $response['flag'] = true;
            $response['message'] = 'You have successfully cancelled your subscription plan';
        } else {
            $response['flag'] = false;
            $response['message'] = 'Something went wrong to cancel subscription';
        }

        return $response;
    }

}
