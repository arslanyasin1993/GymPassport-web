<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Gymimage;
use App\Gymtiming;
use App\Stafftiming;
use App\City;
use Session;
use DB;
use App\Gym_facilities;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class Usergym extends Model
{

    protected $table = "users_gym";
    protected $fillable = ['users_id', 'gym_name', 'gym_price_per_visit', 'gym_logo', 'phone_number', 'gym_activities', 'about_gym', 'gym_address', 'gym_latitude', 'gym_longitude', 'country', 'city', 'qr_code_image', 'accept_terms_condition', 'is_all_day_open', 'gym_pin_code', 'staff_hours', 'is_staff_hours'];

    public function user_gym_registerd($r)
    {

//        $user['first_name'] = $r->first_name;
//        $user['last_name'] = $r->last_name;
//        $user['email'] = $r->email;
//        $user['user_type'] = $r->user_type;
//        $user['user_ufp_id'] = mt_rand(10000, 99999);
        if (isset($r->password) && $r->password != '') {
            $user['password'] = Hash::make($r->password);
        }

//        $user['country'] = $r->country;
        $gym['gym_name'] = $r->gym_name;
        $gym['gym_price_per_visit'] = $r->gym_price_per_visit;
        $gym['phone_number'] = $r->phone_number;
        if (isset($r->gym_activities)) {
            $gym['gym_activities'] = $r->gym_activities;
        } else {
            $gym['gym_activities'] = '';
        };
        if (isset($r->about_gym)) {
            $gym['about_gym'] = $r->about_gym;
        } else {
            $gym['about_gym'] = '';
        };
        $gym['gym_address'] = $r->gym_address;
        $gym['gym_pin_code'] = $r->gym_pin_code;
        $gym['gym_latitude'] = $r->gym_latitude;
        $gym['gym_longitude'] = $r->gym_longitude;
        $gym['country'] = $r->country;
        $gym['city'] = $r->city;
        if (isset($r->staff_hours) && $r->staff_hours != '') //if not open 24 hr
        {
            $gym['is_staff_hours'] = 1;
            $gym['staff_hours'] = $r->staff_hours;
        } else {
            $gym['is_staff_hours'] = 0;
        }

        $gym['accept_terms_condition'] = 1;
        if (isset($r->is_all_day_open) && $r->is_all_day_open == 1) {
            $gym['is_all_day_open'] = $r->is_all_day_open;

        }
//         if(isset($r->is_all_day_open_staff) && $r->is_all_day_open_staff == 1){
//             $gym['is_staff_hours'] = $r->is_all_day_open_staff;
//
//        }
//echo '<pre>';print_r($gym);die();
        $user_id = $r->gym_owner;//User::create($user); //insert user from user table
        if (isset($r->password) && $r->password != '') {
            User::where('id', $user_id)->update($user);
        }
        $gym['users_id'] = $user_id;//$user_id->id ? $user_id->id : '';
        $logo = preg_replace('/\s+/', '', $gym['gym_name']);
        if ($r->hasFile('gym_logo')) {
            $sDirPath = public_path('gym_images/gym_logo') . '/' . $logo;
            $path = 'public/gym_images/gym_logo' . '/' . $logo;
            if (!$sDirPath) {
                $oldmask = umask(0);
                mkdir($sDirPath, 0777, true);
                umask($oldmask);
            }
            $gym_logo = date('Ymdhis') . '.' . $r->gym_logo->getClientOriginalExtension();
            $r->gym_logo->move($sDirPath, $gym_logo);
            $gym['gym_logo'] = $gym_logo ? $path . '/' . $gym_logo : "";
        }
//echo '<pre>';print_r($gym);die();
        $gym_id = self::create($gym); //insert gym data

        if (!isset($r->is_all_day_open) && $r->is_all_day_open != 1) //if not open 24 hr
        {
            if ($r->gym_open_days) {
                foreach ($r->gym_open_days as $key => $val) {
                    Gymtiming::create([
                        'users_gym_id' => $gym_id->id,
                        'gym_open_days' => $val,
                        'gym_open_timing' => date("H:i", strtotime($r->gym_open_timing[$val])),
                        'gym_close_time' => date("H:i", strtotime($r->gym_close_time[$val])),
                    ]); //insert gym time and open days
                }
            }
            $allday['is_all_day_open'] = '0'; //after click open some days
            self::whereid($gym_id->id)->update($allday);
        }
//if(!isset($r->is_all_day_open) && $r->is_all_day_open != 1) //if not open 24 hr
//    {
//        if ($r->gym_open_days) {
//            $totalday = count($r->gym_open_days);
//            for ($i = 0; $i < $totalday; $i++) {
//                Gymtiming::create([
//                    'users_gym_id' => $gym_id->id,
//                    'gym_open_days' => $r->gym_open_days[$i],
//                    'gym_open_timing' => date("H:i", strtotime($r->gym_open_timing[$i])),
//                    'gym_close_time' => date("H:i", strtotime($r->gym_close_time[$i])),
//                ]); //insert gym time and open days
//            }
//        }
//    }

//if(isset($r->is_all_day_open_staff) && $r->is_all_day_open_staff == 1) //if not open 24 hr
//    {
//        if ($r->staff_open_days) {
//            $totalday = count($r->staff_open_days);
//            for ($i = 0; $i < $totalday; $i++) {
//                Gymtiming::create([
//                    'users_gym_id' => $gym_id->id,
//                    'gym_open_days' => $r->staff_open_days[$i],
//                    'gym_open_timing' => date("H:i", strtotime($r->staff_open_timing[$i])),
//                    'gym_close_time' => date("H:i", strtotime($r->staff_close_time[$i])),
//                ]); //insert gym time and open days
//            }
//        }
//    }
        $image = preg_replace('/\s+/', '', $gym['gym_name']);
        if ($r->hasFile('gym_image')) {
            $sDirPath = public_path('gym_images/gym_image') . '/' . $image;
            $Path = 'public/gym_images/gym_image' . '/' . $image;
            if (!$sDirPath) {
                $oldmask = umask(0);
                mkdir($sDirPath, 0777, true);
                $oldmask = umask(0);
            }
            foreach ($r->file('gym_image') as $image) {

                $gym_image = $image->getClientOriginalName();
                $image->move($sDirPath, $gym_image);
                $gym_img['gym_image'] = $Path . '/' . $gym_image;
                $gym_img['users_gym_id'] = $gym_id->id;
                Gymimage::create($gym_img);
            }
        }

        // add gym facilities

        foreach ($r->facilities_id as $key => $value) {
            $facilities['gym_id'] = $gym_id->id;
            $facilities['facilities_id'] = $value;
            Gym_facilities::create($facilities);
        }

        if ($gym_id->id) {
            $file_formate = 'svg';
            $qr_code_image["qr_code_image"] = $gym_id->id . '.svg';

            QrCode::format($file_formate)->size(250)
                ->generate($gym_id->id, public_path('image/qr_codes/' . $qr_code_image["qr_code_image"]));

            if (file_exists(base_path() . '/public/image/qr_codes/' . $qr_code_image["qr_code_image"])) {
                self::whereid($gym_id->id)->update($qr_code_image);
            } else {
                QrCode::format($file_formate)->size(250)
                    ->generate($gym_id->id, public_path('/image/qr_codes/' . $qr_code_image["qr_code_image"]));
                self::whereid($gym_id->id)->update($qr_code_image);
            }

            $msg = trans('lang_data.gymadd');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
    }

    public function user_gym_update($r, $id)
    {
        if (isset($r->first_name)) {
            $user['first_name'] = $r->first_name;
            $user['last_name'] = $r->last_name;
            $user['email'] = $r->email;
            $user['user_type'] = $r->user_type;
            $user['country'] = $r->country;
            $gym['country'] = $r->country;
            $gym['city'] = $r->city;
        }
        $gym['gym_name'] = $r->gym_name;
        $gym['gym_price_per_visit'] = $r->gym_price_per_visit;
        $gym['phone_number'] = $r->phone_number;
        if (isset($r->gym_activities)) {
            $gym['gym_activities'] = $r->gym_activities;
        } else {
            $gym['gym_activities'] = '';
        };
        if (isset($r->about_gym)) {
            $gym['about_gym'] = $r->about_gym;
        } else {
            $gym['about_gym'] = '';
        };
        //$gym['gym_activities'] = $r->gym_activities;
        //$gym['about_gym'] = $r->about_gym;
        if (isset($r->gym_address) && !empty($r->gym_address)) {
            $gym['gym_address'] = $r->gym_address;
            $gym['gym_pin_code'] = $r->gym_pin_code;
            $gym['gym_latitude'] = $r->gym_latitude;
            $gym['gym_longitude'] = $r->gym_longitude;
        }

        $gym['accept_terms_condition'] = 1;
        // echo '<pre>';print_r($gym);die;
        if (isset($r->is_all_day_open) && ($r->is_all_day_open == 1)) {
            $gym['is_all_day_open'] = $r->is_all_day_open;
            Gymtiming::where('users_gym_id', $id)->delete();
        }

        if (isset($r->staff_hours) && $r->staff_hours != '' && ($r->is_staff_hours == 1 || $r->is_all_day_open_staff == 1)) //if not open 24 hr
        {
            $gym['is_staff_hours'] = 1;
            $gym['staff_hours'] = $r->staff_hours;
        } else {
            $gym['is_staff_hours'] = 0;
            $gym['staff_hours'] = '';
            // Stafftiming::where('users_gym_id', $id)->delete();
        }
        // if(!isset($r->is_all_day_open_staff) && $r->is_all_day_open_staff != 1){
        //     $gym['is_staff_hours'] = 0;
        //     Stafftiming::where('users_gym_id', $id)->delete();
        // }
        if (isset($r->first_name)) {
            $userid = self::select('users_id')->whereid($id)->first();
            $user_id = $userid->users_id;
            User::whereid($user_id)->update($user); //update user from user table
            $gym['users_id'] = $user_id;
        }
        if ($r->hasFile('gym_logo')) {
            $sDirPath = public_path('gym_images/gym_logo') . '/' . preg_replace('/\s+/', '', $gym['gym_name']);
            $path = 'public/gym_images/gym_logo' . '/' . preg_replace('/\s+/', '', $gym['gym_name']);
            if (!$sDirPath) {
                $oldmask = umask(0);
                mkdir($sDirPath, 0777, true);
                umask($oldmask);
            }
            $gym_logo = date('Ymdhis') . '.' . $r->gym_logo->getClientOriginalExtension();
            $r->gym_logo->move($sDirPath, $gym_logo);
            $gym['gym_logo'] = $gym_logo ? $path . '/' . $gym_logo : "";
        }
// echo '<pre>';print_r($gym);die();
        self::whereid($id)->update($gym); //update gym data
        if (!isset($r->is_all_day_open) && $r->is_all_day_open != 1) //if not open 24 hr
        {
            if ($r->gym_open_days) {
                Gymtiming::where('users_gym_id', $id)->delete();
                foreach ($r->gym_open_days as $key => $val) {
                    Gymtiming::create([
                        'users_gym_id' => $id,
                        'gym_open_days' => $val,
                        'gym_open_timing' => date("H:i", strtotime($r->gym_open_timing[$val])),
                        'gym_close_time' => date("H:i", strtotime($r->gym_close_time[$val])),
                    ]); //insert gym time and open days
                }
//                    $totalday = count($r->gym_open_days);
//                    Gymtiming::where('users_gym_id', $id)->delete(); //before insert delete all time from tables
//                    for ($i = 0; $i < $totalday; $i++) {
//                        Gymtiming::create([
//                            'users_gym_id' => $id,
//                            'gym_open_days' => $r->gym_open_days[$i],
//                            'gym_open_timing' => date("H:i", strtotime($r->gym_open_timing[$i])),
//                            'gym_close_time' => date("H:i", strtotime($r->gym_close_time[$i])),
//                        ]); //insert gym time and open days
//                    }
            }
            $allday['is_all_day_open'] = '0'; //after click open some days
            self::whereid($id)->update($allday);
        }

//        if(isset($r->is_all_day_open_staff) && $r->is_all_day_open_staff = 1) //if  open 24 hr
//            {
//                if ($r->staff_open_days) {
//                    $totalday = count($r->staff_open_days);
//                    Stafftiming::where('users_gym_id', $id)->delete(); //before insert delete all time from tables
//                    for ($i = 0; $i < $totalday; $i++) {
//                        Stafftiming::create([
//                            'users_gym_id' => $id,
//                            'gym_open_days' => $r->staff_open_days[$i],
//                            'gym_open_timing' => date("H:i", strtotime($r->staff_open_timing[$i])),
//                            'gym_close_time' => date("H:i", strtotime($r->staff_close_time[$i])),
//                        ]); //insert gym time and open days
//                    }
//                }
//                $allday['is_staff_hours'] = '1'; //after click open some days
//                self::whereid($id)->update($allday);
//            }

        if ($r->hasFile('gym_image')) {
            $sDirPath = public_path('gym_images/gym_image') . '/' . preg_replace('/\s+/', '', $gym['gym_name']);
            $Path = 'public/gym_images/gym_image' . '/' . preg_replace('/\s+/', '', $gym['gym_name']);
            if (!$sDirPath) {
                $oldmask = umask(0);
                mkdir($sDirPath, 0777, true);
                $oldmask = umask(0);
            }
            //Gymimage::where('users_gym_id', $id)->delete(); //before insert delete all from tables
            foreach ($r->file('gym_image') as $image) {
                $total_image = Gymimage::where('users_gym_id', $id)->count();
                if ($total_image < 5) {
                    $gym_image = $image->getClientOriginalName();
                    $image->move($sDirPath, $gym_image);
                    $gym_img['gym_image'] = $Path . '/' . $gym_image;
                    $gym_img['users_gym_id'] = $id;
                    Gymimage::create($gym_img);
                }

            }
        }

        // add gym facilities
        if (!empty($r->facilities_id)) {
            Gym_facilities::where('gym_id', $id)->delete();
            foreach ($r->facilities_id as $key => $value) {
                $facilities['gym_id'] = $id;
                $facilities['facilities_id'] = $value;
                Gym_facilities::create($facilities);
            }
        }

        if ($id) {
            $msg = "Gym updated successfully";
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-success');
        } else {
            $msg = trans('lang_data.error');
            Session::flash('msg', $msg);
            Session::flash('message', 'alert-danger');
        }
    }

    public function gym_img_path()
    {
        $image_path = public_path('gym_images/gym_image') . '/' . $this->gym_name;
    }

    public function gym_logo_path()
    {
        $logo_path = public_path('gym_images/gym_logo') . '/' . $this->gym_name;
    }

    public function gym_owner_detail()
    {
        return $this->hasOne('App\User', 'id', 'users_id')->select(['id', 'first_name', 'last_name', 'email', 'created_at as registered_date']);
    }

    public function countryname()
    {
        return $this->belongsTo(Country::class, 'country', 'id')->select(['id', 'nicename']);
    }


    public function cityname()
    {
        return $this->belongsTo(City::class, 'city', 'id');
    }


    public function gym_details($gym_id, $select)
    {
        $data = self::select($select)
            ->with('gym_owner_detail')
            ->with('countryname')
            ->with('gym_image')
            ->with('facilities', 'facilities.gym_facilities')
            ->with('gym_time')
            ->with('staff_timing')
            ->whereid($gym_id)
            ->first(); //->toArray();
        return $data;
    }

    public function gym_time()
    {
        return $this->hasMany('App\Gymtiming', 'users_gym_id', 'id')->select(['users_gym_id', 'gym_open_days', 'gym_open_timing', 'gym_close_time']);
    }

    public function staff_timing()
    {
        return $this->hasMany('App\Stafftiming', 'users_gym_id', 'id')->select(['users_gym_id', 'gym_open_days', 'gym_open_timing', 'gym_close_time']);
    }

    public function gym_image()
    {

        return $this->hasMany('App\Gymimage', 'users_gym_id', 'id');
    }

    public static function getByDistance($lat, $lng, $distance, $status, $search = null, $plan = null)
    {
        //phone_number,gym_activities,about_gym,country
        if ($plan == null) {

            $results = DB::select(DB::raw('SELECT users_gym.id as gym_id,city.name as city_name,gym_name,is_all_day_open,gym_logo,gym_address,status,gym_latitude,gym_longitude, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( gym_latitude ) ) * cos( radians( gym_longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(gym_latitude) ) ) ) AS distance, about_gym , staff_hours AS staffhours , city AS gymcity FROM users_gym left join city on city.id = users_gym.city WHERE status ="' . $status . '" AND (gym_name  LIKE "%' . $search . '%" OR gym_pin_code  LIKE "%' . $search . '%" OR city  LIKE "%' . $search . '%") HAVING distance < ' . $distance . '  ORDER BY distance'));
            //$results = DB::select(DB::raw('SELECT id as gym_id,gym_name,is_all_day_open,gym_logo,gym_address,status,gym_latitude,gym_longitude, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( gym_latitude ) ) * cos( radians( gym_longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(gym_latitude) ) ) ) AS distance FROM users_gym WHERE status ="' . $status . '" AND (gym_name  LIKE "%' . $search . '%" OR gym_pin_code  LIKE "%' . $search . '%")'));

        } else {

            if ($search == 1) {
                $search = "AND gc.plan_category_id = '" . $search . "'";
            } else if ($search == 2) {
                $search = "AND (gc.plan_category_id = 1 OR gc.plan_category_id = '" . $search . "')";
            } else if ($search == 3) {
                $search = "AND (gc.plan_category_id = 1 OR gc.plan_category_id = 2 OR gc.plan_category_id = '" . $search . "')";
            } else {
                $search = '';
            }

//            $results = DB::select(DB::raw('SELECT users_gym.id as gym_id,users_gym.gym_name,is_all_day_open,gym_logo,gym_address,users_gym.status,gym_latitude,gym_longitude, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( gym_latitude ) ) * cos( radians( gym_longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(gym_latitude) ) ) ) AS distance FROM users_gym
//                INNER JOIN gymcategories AS gc ON users_gym.id = gc.user_gym_id '. $search  .'
//                WHERE users_gym.status = 1 HAVING distance < ' . $distance . '  ORDER BY distance LIMIT 0, 200'));

            $results = DB::select(DB::raw('SELECT users_gym.id as gym_id,users_gym.gym_name,is_all_day_open,gym_logo,gym_address,users_gym.status,gym_latitude,gym_longitude, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( gym_latitude ) ) * cos( radians( gym_longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(gym_latitude) ) ) ) AS distance , about_gym ,staff_hours AS staffhours , city AS gymcity FROM users_gym 
                INNER JOIN gymcategories AS gc ON users_gym.id = gc.user_gym_id ' . $search . '
                WHERE users_gym.status = 1'));
        }

        return $results;
    }

    public static function check_in_gymlist($lat, $lng, $status, $user_plan_category_id)
    {
        $distance = 300.0;
        $search = '';
        //old query ufp
        //$sql = 'SELECT ug.id as gym_id,ug.gym_name,ug.gym_logo,ug.gym_address,ug.gym_latitude,ug.gym_longitude, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( ug.gym_latitude ) ) * cos( radians( ug.gym_longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(ug.gym_latitude) ) ) ) AS distance FROM users_gym ug LEFT JOIN gymcategories gc ON ug.id = gc.user_gym_id WHERE gc.plan_category_id <= "' . $user_plan_category_id . '" AND ug.status ="' . $status . '" AND ug.gym_name  LIKE "%' . $search . '%" HAVING distance < ' . $distance . '  ORDER BY distance LIMIT 0, 200';
        if ($user_plan_category_id == 4) {
            $sql = 'SELECT gc.id,ug.id as gym_id,ug.gym_name,ug.gym_logo,ug.gym_address,ug.gym_latitude,ug.gym_longitude, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( ug.gym_latitude ) ) * cos( radians( ug.gym_longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(ug.gym_latitude) ) ) ) AS distance FROM users_gym ug INNER JOIN gymcategories gc ON ug.id = gc.user_gym_id WHERE gc.plan_category_id = "1" or gc.plan_category_id = "2" AND ug.status ="' . $status . '" AND ug.gym_name  LIKE "%' . $search . '%" HAVING distance < ' . $distance . '  ORDER BY distance LIMIT 0, 200';
        } else {
            $sql = 'SELECT gc.id,ug.id as gym_id,ug.gym_name,ug.gym_logo,ug.gym_address,ug.gym_latitude,ug.gym_longitude, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( ug.gym_latitude ) ) * cos( radians( ug.gym_longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(ug.gym_latitude) ) ) ) AS distance FROM users_gym ug INNER JOIN gymcategories gc ON ug.id = gc.user_gym_id WHERE gc.plan_category_id <= "' . $user_plan_category_id . '" AND ug.status ="' . $status . '" AND ug.gym_name  LIKE "%' . $search . '%" HAVING distance < ' . $distance . '  ORDER BY distance LIMIT 0, 200';
        }
//        $sql = 'SELECT ug.id as gym_id,ug.gym_name,ug.gym_logo,ug.gym_address,ug.gym_latitude,ug.gym_longitude, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( ug.gym_latitude ) ) * cos( radians( ug.gym_longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(ug.gym_latitude) ) ) ) AS distance FROM users_gym ug WHERE ug.status ="' . $status . '" AND ug.gym_name  LIKE "%' . $search . '%" HAVING distance < ' . $distance . '  ORDER BY distance LIMIT 0, 200';
        //$sql = 'SELECT ug.id as gym_id,ug.gym_name,ug.gym_logo,ug.gym_address,ug.gym_latitude,ug.gym_longitude FROM users_gym ug WHERE ug.status ="' . $status . '" AND ug.gym_name  LIKE "%' . $search . '%" LIMIT 0, 200';

        $results = DB::select(DB::raw($sql));
        return $results;
    }

    public function usertotal_visit($user_id, $user_subscription_id, $user_have_visit_pass)
    {
        $sql = 'SELECT COUNT(id) as totalvisit FROM user_visited_gym WHERE user_id="' . $user_id . '" AND usersubscription_id="' . $user_subscription_id . '"';
        $results = DB::select(DB::raw($sql));
        if (isset($results[0]->totalvisit)) {
            if ($user_have_visit_pass == 'month') {
                $totalvisit = $results[0]->totalvisit;
            } else {
                //$totalvisit = ($user_have_visit_pass - $results[0]->totalvisit);
                $totalvisit = $results[0]->totalvisit;
            }
        }
        return $totalvisit;
    }

//18-10-2019
    public function plan_name_by_gym_id($gym_id)
    {
        $sql = 'SELECT plan_category.plan_cat_name,gymcategories.plan_category_id FROM plan_category LEFT JOIN gymcategories ON gymcategories.plan_category_id=plan_category.id WHERE gymcategories.user_gym_id="' . $gym_id . '"';
        $data = DB::select($sql);
        return $data;
    }

//21-10-2019

    public function facilities()
    {
        return $this->hasMany('App\Gym_facilities', 'gym_id', 'id')->select(['id as g_f_id', 'gym_id', 'facilities_id']);
    }

    //18-11-2019
    public function cat_id_by_gym_id($gym_id)
    {
        $sql = 'SELECT `plan_category_id` FROM `gymcategories` WHERE user_gym_id="' . $gym_id . '"';
        $data = DB::select($sql);
        return $data;
    }


//15-01-2020
    public function partner_gym_detail($user_id)
    {
        $query = self::query();
        $query->with('gym_owner_detail');
        $query->with('countryname');
        $query->with('gym_image');
        $query->with('facilities', 'facilities.gym_facilities');
        $query->with('gym_time');
        $query->where('users_id', $user_id);
        $query->where('status', '1');
        $data = $query->get();//->toArray();
        return $data;
    }

//22-01-2020
    public static function get_distance_by_gym_id($lat, $lng, $gym_id)
    {
        //phone_number,gym_activities,about_gym,country
        $results = DB::select(DB::raw('SELECT id as gym_id,gym_name, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( gym_latitude ) ) * cos( radians( gym_longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(gym_latitude) ) ) ) AS distance FROM users_gym WHERE id ="' . $gym_id . '" '));

        return $results;
    }

    //28-02-2020
    public static function check_in_user_distance($lat, $lng, $gym_id)
    {
        $distance = 50;
        $search = '';
        $sql = 'SELECT ug.id as gym_id,ug.gym_name,ug.gym_logo,ug.gym_address,ug.gym_latitude,ug.gym_longitude, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( ug.gym_latitude ) ) * cos( radians( ug.gym_longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(ug.gym_latitude) ) ) ) AS distance FROM users_gym ug LEFT JOIN gymcategories gc ON ug.id = gc.user_gym_id WHERE ug.id ="' . $gym_id . '"  HAVING distance < ' . $distance . '';
        $results = DB::select(DB::raw($sql));
        return $results;
    }

    //29-09-2020

    public function get_gym_name($user_id)
    {
        $query = self::query();
        $query->select('gym_name');
        $query->where('users_id', $user_id);
        return $query->get()->toArray();
    }

    public function gymCategories(){
        return $this->hasMany('App\GymCategory' , 'user_gym_id' , 'id');
    }

}
