<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appversion extends Model
{
    protected $table='app_version';
    
    protected $fillable = ['android_version','ios_version','ios_force_upgrade','android_force_upgrade','status'];
}
