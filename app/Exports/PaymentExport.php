<?php

namespace App\Exports;

use Session;
use App\Usergym;
use App\Uservisitedgym;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromCollection;

class PaymentExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use Exportable;
    function __construct($data = NULL) {
        $this->gym = new Usergym;
        $this->uvg = new Uservisitedgym;
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'Sr No',
            'Gym Name',
            'Transaction ID',
            'Date',
            'Earnings'
        ];
    }

}