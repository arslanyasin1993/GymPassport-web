<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Country;
use App\State;
use App\Uservisitedgym;
use DB;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
         'email', 'password','first_name','last_name','plain_password','d_o_b','country','state','house_no','street','district','city','address','postal_code',
         'user_type','user_image','user_token','user_token_time','firebase_token','status','phone_number','suburb','fitness_facility',
         'user_ufp_id','created_from','partner_first_login','registration_fees','monthly_fees', 'gym_user_id','is_corporate','is_delete','is_email_verified','is_corporate_user','status_updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function visited_gym()
    {
        return $this->hasMany('App\Uservisitedgym');
    }

    public function countryname()
    {
        return $this->belongsTo(Country::class, 'country','id')->select(array('id', 'nicename'));
    }
    public function cityname()
    {
        return $this->belongsTo(City::class, 'city','id')->select(array('id', 'name'));
    }

     public function state_name()
    {
        return $this->belongsTo(State::class, 'state','id')->select(array('id', 'state','state_code'));
    }

    public function user_details($id){
        $data = self::select('id','user_ufp_id','email','first_name','last_name','d_o_b','country','state','city','address','postal_code','user_image','status')->with('countryname')->with('state_name')->with('user_subscription')->where('id',$id)->first()->toArray();
        return $data;
    }
    public function user_subscription(){
        return $this->hasMany('App\API\Usersubscription');
    }
    
    
    //24-12-2019
     public function total_visit()
    {
        return $this->belongsTo(Uservisitedgym::class, 'id','user_id')->select(array('count(id) as total_visit'));
    }
    
    //02-1-2020
     public function active_subscription(){
        return $this->hasOne('App\API\Usersubscription')->where('status',1)->where('visit_pass','month');
    }
    
     public function last_subscription(){
        return $this->hasOne('App\API\Usersubscription')->orderBy('id','DESC');
    }
    
    public function select_plan_from_ufpid($ufp_id){
        $data = self::select('id','user_ufp_id','first_name','last_name','email')
                ->with('active_subscription')->where('user_ufp_id',$ufp_id)->first()->toArray();
        return $data;
    }
    
    public function userCheckIns(){
        return $this->hasMany('App\Uservisitedgym' , 'user_id' , 'id');
    }

    public function get_current_plan(){
        $data = self::select('id','user_ufp_id','first_name','last_name','email')
                ->with('last_subscription.plan_category')->where('user_type',3)->orderBy('first_name','asc')->get();
        return $data;
    }
}
