<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $for = 0;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $for = 0)
    {
        $this->name = $name;
        $this->for = $for;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->for == 0) {
            return $this->view('emails.name')->subject('Gym Passport Forgot Password');
        } elseif ($this->for == 1) { // for email verification
            return $this->view('emails.email_otp')->subject('Gym Passport Registration email verification');
        } elseif ($this->for == 2) {/// for corporate employee
            return $this->view('emails.corporate_user_email_template')->subject('Gym Passport Corporate Login credentials');
        }
    }
}
