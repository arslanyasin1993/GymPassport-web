<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LatestFileSend extends Mailable
{
    use Queueable, SerializesModels;

    public $newest_file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($newest_file)
    {
        $this->newest_file = $newest_file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        Mail::send('mail.Test_mail', $data, function($message)use($data, $files) {
//            $message->to($data["email"])
//                ->subject($data["title"]);
//
//            foreach ($files as $file){
//                $message->attach($file);
//            }
//        });

        return $this->view('emails.latest_file_send')->subject('Latest Email Backup '.date('d-m-Y'))
            ->attach(public_path('/backup/') . $this->newest_file);
    }
}
