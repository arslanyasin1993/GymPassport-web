<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gymtiming extends Model
{
    protected $table ="gym_timing";
    
    protected $fillable = ['users_gym_id','gym_open_days','gym_open_timing','gym_close_time'];
}
