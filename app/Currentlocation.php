<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currentlocation extends Model
{
    protected $table = 'user_current_lat_long';

    protected $fillable = ['user_id','latitude','longitude'];
}
